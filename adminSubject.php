<?php
    include_once 'sessionAdmin.php';
    include_once 'dbconnect.php';

    function check($con)
    {
        $query = mysqli_num_rows(mysqli_query($con, "SELECT * from yearlevel"));
        if($query > 0){
            return true;
        }
        else
        {
            return false;
        }
    }
?>
<!DOCTYPE html>
    <!-- HEAD -->
    <?php include_once 'head.php'; ?>
    <!-- HEAD   -->
    <body>
        <!-- HEADER -->
        <?php include_once 'header.php'; ?>
        <!-- HEADER -->

        <section id="main">
            <ol class="breadcrumb">
                <li><a href="adminHome.php">Home</a></li>
                <li class="active">Subjects</li>
            </ol>
            <?php
                $toggle = 'adminSubject';
                include_once 'sidebar.php';
            ?>

            <section id="content">
                <div class="container">
                    <!-- Colored Headers -->
                    <div class="block-header">
                        <h1><i class="zmdi zmdi-collection-text"></i> SUBJECTS
                        </h1>

                        <div class="actions">
                            <div class="btn-demo">

                                <button type="button" class="btn btn-danger btn-lg dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><b><i class='zmdi zmdi-collection-pdf'></i> PRINT</b>
                                    <span class="caret"></span>
                                    <span class="sr-only"></span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="LIST-SUBJECT.php?level=Grade7" target="_blank">GRADE 7</a></li>
                                    <li><a href="LIST-SUBJECT.php?level=Grade8" target="_blank">GRADE 8</a></li>
                                    <li><a href="LIST-SUBJECT.php?level=Grade9" target="_blank">GRADE 9</a></li>
                                    <li><a href="LIST-SUBJECT.php?level=Grade10" target="_blank">GRADE 10</a></li>
                                    <li class="divider"></li>
                                    <li><a href="LIST-SUBJECT.php?level=ALL">ALL SUBJECT</a></li>
                                </ul>

                            <?php
                                if(check($con)){
                                echo '<button  href="#modalColor" data-toggle="modal" class="btn btn-default btn-lg"><i class="zmdi zmdi-collection-text"></i><b> New Subject</b></button>';
                                }

                                else
                                {
                                echo "<button href='#' data-toggle='tooltip' data-placement='top' title='All Level must be complete in order to add subjects. Tips: * Go to Enrollment Configuration and click Year Levels' class='btn btn-default btn-lg'><i class='zmdi zmdi-collection-text'></i><b> New Subject</b></button>";
                                }
                            ?>
                            </div>
                        </div>
                        <div class="modal fade" id="modalColor" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
                            <div class="modal-dialog ">
                                <div class="modal-content">
                                    <form id="myForm" method="post">
                                        <div class="modal-header">
                                            <h2 class="modal-title"><b>CREATE NEW SUBJECT</b></h2>
                                        </div>

                                        <div class="modal-body">
                                            <div class='col-sm-8'>
                                                <div class='form-group fg-float'>
                                                    <div class="fg-line">
                                                        <input type='text' name='subj_title' id='subj_title' class='form-control fg-input' required>
                                                        <label class="fg-label">Title </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class='form-group fg-float'>

                                                    <select class="selectpicker form-control" name="year_lvl_title" id="year_lvl_title" required>
                                                        <option value="">LEVEL</option>
                                            <?php
                                                $query = mysqli_query($con, "SELECT * from yearlevel");
                                                while($row = mysqli_fetch_array($query)){
                                                    echo "
                                                        <option>".$row['year_lvl_title']."</option>
                                                    ";
                                                }
                                            ?>

                                                    </select>

                                                </div>
                                            </div>
                                            <br />
                                            <br /><br />
                                            <br />
                                            <div class="panel panel-collapse">
                                                <div class="panel-heading" role="tab" id="headingOne">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"
                                                           aria-expanded="false" aria-controls="collapseOne">
                                                            <b>More Information</b> (Optional)
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseOne" class="collapse out" role="tabpanel"
                                                     aria-labelledby="headingOne">
                                                    <div class="panel-body">
                                                        <div class='col-sm-12'>
                                                            <div class='form-group fg-float'>
                                                                <div class="fg-line">
                                                                    <input type='text' name='subj_description' id='subj_description' class='form-control fg-input '>
                                                                    <label class="fg-label">Description (Optional)</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class='col-sm-6'>
                                                            <div class='form-group fg-float'>
                                                                <div class="fg-line">
                                                                    <input type='text' name='subj_unit' id='subj_unit' class='form-control fg-input '>
                                                                    <label class="fg-label">Unit(Optional) </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class='col-sm-6'>
                                                            <div class='form-group fg-float'>
                                                                <div class="fg-line">
                                                                    <input type='text' name='subj_hrs_per_week' id='subj_hrs_per_week' class='form-control fg-input '>
                                                                    <label class="fg-label">Hours/week(Optional) </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">

                                            <button type="submit" name="insert1" id="insert1" class="btn btn-success">CREATE SUBJECT</button>
                                            <button type="button" class="btn btn-warning" data-dismiss="modal">CANCEL
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                         <div class="modal fade" id="modalColor1" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
                            <div class="modal-dialog ">
                                <div class="modal-content">
                                    <form id="myForm1" method="post" role='form'>
                                        <div class="modal-header">
                                            <h2 class="modal-title"><b>EDIT SUBJECT</b></h2>
                                        </div>

                                        <div class="modal-body">
                                            <div class='col-sm-8'>
                                                <div class='form-group fg-float'>
                                                    <div class="fg-line">
                                                        <input type='text' name='subj_title1' id='subj_title1' class='form-control fg-input' required>
                                                        <label class="fg-label">Title </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group fg-line">
                                                    <select class="selectpicker form-control" name="year_lvl_title1" id="year_lvl_title1" required>

                                            <?php
                                                $query = mysqli_query($con, "SELECT * from yearlevel");
                                                while($row = mysqli_fetch_array($query)){
                                                    echo "
                                                        <option>".$row['year_lvl_title']."</option>
                                                    ";
                                                }
                                            ?>

                                                    </select>
                                                </div>
                                            </div>
                                            <br />
                                            <br /><br />
                                            <br />
                                            <div class="panel panel-collapse">
                                                <div class="panel-heading" role="tab" id="headingTwo">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"
                                                           aria-expanded="false" aria-controls="collapseTwo">
                                                            <b>More Information</b> (Optional)
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseTwo" class="collapse out" role="tabpanel"
                                                     aria-labelledby="headingTwo">
                                                    <div class="panel-body">
                                                        <div class='col-sm-12'>
                                                            <div class='form-group fg-float'>
                                                                <div class="fg-line">
                                                                    <input type='text' name='subj_description1' id='subj_description1' class='form-control fg-input '>
                                                                    <label class="fg-label">Description </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class='col-sm-4'>
                                                            <div class='form-group fg-float'>
                                                                <div class="fg-line">
                                                                    <input type='number' name='subj_unit1' id='subj_unit1' class='form-control fg-input '>
                                                                    <label class="fg-label">Unit(Optional) </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class='col-sm-4'>
                                                            <div class='form-group fg-float'>
                                                                <div class="fg-line">
                                                                    <input type='number' name='subj_hrs_per_week1' id='subj_hrs_per_week1' class='form-control fg-input '>
                                                                    <label class="fg-label">Hrs/Week(Optional) </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                
                                        </div>
                                        <div class="modal-footer">
                                            <input type="hidden" name="subj_id" id="subj_id">
                                            <button type="submit" name="insert1" id="insert1" class="btn btn-success">EDIT SUBJECT</button>
                                            <button type="button" class="btn btn-warning" data-dismiss="modal">CANCEL
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div id="dataModal" class="modal fade" data-backdrop="static" data-keyboard="false">
                            <div class="modal-dialog">
                               <div class="modal-content">
                                    <div class="modal-header">
                                         <button type="button" class="close" data-dismiss="modal">&times;</button>
                                         <h4 class="modal-title"><b>SUBJECT INFORMATION</b></h4>
                                    </div>
                                    <div class="modal-body" id="Details">
                                    </div>
                                    <div class="modal-footer">
                                         <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
                                    </div>
                               </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="action-header clearfix">
                            <div class="ah-label hidden-xs" data-ma-action="action-header-open"><b>Click here to search for a subject</b></div>

                            <div class="ah-search">
                                <input type="text" name='search1' id="search1" placeholder="Start typing..." class="ahs-input" autofocus>
                                <i class="ahs-close" data-ma-action="action-header-close">&times;</i>
                            </div>

                           <ul class="actions">
                                <li>
                                    <a href="#" data-ma-action="action-header-open">
                                        <i class="zmdi zmdi-search"></i>
                                    </a>
                                </li>


                            </ul>
                        </div>

                        <div class="card-body" id="hehe">

                            
                            
                            <?php
                                $query = mysqli_query($con, "CALL selectSubjectDetails()");
                                include_once 'ajaxJQuery/displaySubject.php'; 
                            ?>
                            
                        </div>
                    </div>


                    <br/>
                    <br/>
                </div>
                <button href='#modalColor' data-toggle="modal" class="btn btn-float bgm-green m-btn"><i class="zmdi zmdi-plus"></i></button>
            </section>
        </section>

        <!-- FOOTER -->
        <?php include_once 'footer.php' ?>
        <!-- FOOTER -->

        <!-- Javascript Libraries -->
        <?php include_once 'scripts.php'; ?>
        <!-- Javascript Libraries -->
        <script type="text/javascript" src="js/Notification.js"></script>

        <script type="text/javascript">
            $(document).ready(function(){
                $('#data-table-basic').DataTable();

                $('#modalColor').on('shown.bs.modal', function() {
                  $('#subj_title').focus();
                });
                $('#modalColor1').on('shown.bs.modal', function() {
                  $('#subj_title1').focus();
                  $('#subj_description1').focus();
                  $('#subj_unit1').focus();
                  $('#subj_hrs_per_week1').focus();
                });

                $('[data-dismiss=modal]').on('click', function (e) {
                    $('#myForm')[0].reset();
                    $('#myForm1')[0].reset();
                });

                $('#search1').keyup(function(objEvent){
                    var txt = $('#search1').val();


                    $.ajax({
                        url:"ajaxJQuery/searchSubject.php",
                        method:"POST",
                        data:{search:txt},
                        dataType:"text",
                        success:function(data)
                        {
                            if(!data)
                            {
                                $('#hehe').html(
                                    '<div class="card-body card-padding" id="hehe">'
                                        +'Your search - <b>'+txt+'</b> - did not match any subjects.'
                                        +'<br />'
                                        +'Suggestions:<br />'
                                        +'<ul style="margin-left:1.3em;margin-bottom:2em"><li>Make sure that all words are spelled correctly.</li><li>Try different keywords.</li><li>Try more general keywords.</li></ul>'
                                    +'</div>'
                                );
                            }
                            else
                            {
                                $('#hehe').html(data);
                            }
                        }
                    });


                });
                $('#globalSearch').keyup(function(objEvent){
                    var txt = $('#globalSearch').val();


                    $.ajax({
                        url:"ajaxJQuery/searchSubject.php",
                        method:"POST",
                        data:{search:txt},
                        dataType:"text",
                        success:function(data)
                        {
                            if(!data)
                            {
                                $('#hehe').html(
                                    '<div class="card-body card-padding" id="hehe">'
                                        +'Your search - <b>'+txt+'</b> - did not match any subjects.'

                                        +'<br />'
                                        +'Suggestions:<br />'
                                        +'<ul style="margin-left:1.3em;margin-bottom:2em"><li>Make sure that all words are spelled correctly.</li><li>Try different keywords.</li><li>Try more general keywords.</li></ul>'
                                    +'</div>'
                                );
                            }
                            else
                            {
                                $('#hehe').html(data);
                            }
                        }
                    });


                });
                $(document).on('click', '.details', function(){
                    var subj_id = $(this).attr("id");

                    $.ajax({
                         url:"ajaxJQuery/viewSubject.php",
                         method:"POST",
                         data:{subj_id:subj_id},

                         success:function(data){
                              $('#Details').html(data);
                              $('#dataModal').modal('show');
                         }
                    });
                });


                $(document).on('click', '.edit_subject', function(){
                    var subj_id = $(this).attr("id");
                    $('#myForm1')[0].reset();
                    $('#myForm')[0].reset();
                    $.ajax({
                         url:"ajaxJQuery/selectSubject.php",
                         method:"POST",
                         data:{subj_id:subj_id},
                         dataType:"json",
                         success:function(data){
                              $('#subj_title1').val(data.subj_title);
                              $('#subj_description1').val(data.subj_description);
                              $('#subj_unit1').val(data.subj_unit);
                              $('#subj_hrs_per_week1').val(data.subj_hrs_per_week);
                              $('#subj_id').val(data.subj_id);

                              var result = $('#year_lvl_title1 option[value!=""]').first().html();
                              $("#year_lvl_title1 option[value='"+result+"']").remove();

                              $('#year_lvl_title1').prepend('<option value="'+data.Level+'" selected="">'+data.Level+'</option>');
                               $('#year_lvl_title1').selectpicker('refresh');
                              $('#modalColor1').modal('show');

                              //$('.title-modal').text('asdas');

                         }
                    });
                });


                $(document).on('click', '.delete_subject', function(){
                    var tae = $(this).attr("id");
                    swal({
                        title: "Are you sure?",
                        text: "You will not be able to recover this data!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonText: "Yes, delete it!",
                    }).then(function(){

                        $.ajax({
                            url:"ajaxJQuery/deleteSubject.php",
                            method:"POST",
                            data:{tae:tae},

                            success:function(data){
                               if(data == '0'){
                                    var message = "Error on deleting subject.";
                                    executeNotif(message,"danger");



                                }else{

                                    var message = "Successfully deleted subject.";
                                    executeNotif(message,"success");
                                    $('#hehe').html(data);
                                }
                                   //$('#modalColor1').modal('hide');

                              }


                        });
                    });

                });

                $('#myForm').on("submit", function(event){
                    event.preventDefault();

                     $.ajax({
                          url:"ajaxJQuery/insertSubject.php",
                          method:"POST",
                          data:$('#myForm').serialize(),
                          beforeSend:function(){
                               $('#insert1').val("Inserting..");
                          },
                          success:function(data){


                            if(data == '0')
                            {
                                var message = "Error on adding subject. Subject may exist or something";
                                executeNotif(message,"danger");
                            }
                            else
                            {
                                var message = "Successfully added subject";
                                executeNotif(message,"success");
                                $('#year_lvl_title1').empty();
                                $('#year_lvl_title').empty();
                              $('#myForm')[0].reset();
                              $('#modalColor').modal('hide');
                              $('#hehe').html(data);
                            }
                          }

                     });
                    
                });

                $('#myForm1').on("submit", function(event){


                    event.preventDefault();
                    
                     $.ajax({
                          url:"ajaxJQuery/updateSubject.php",
                          method:"POST",
                          data:$('#myForm1').serialize(),
                          beforeSend:function(){
                               $('#insert1').val("Inserting..");
                          },
                          success:function(data){
                            if(data != '0'){
                                var message = "Successfully edited subject";
                                executeNotif(message,"success");
                               $('#modalColor1').modal('hide');
                                $('#myForm1')[0].reset();
                                $('#hehe').html(data);
                            }
                            else
                            {
                                var message = "Error on editing subject";
                                executeNotif(message,"danger");

                            }
                          }

                     });
                    

                });
            });



        </script>
    </body>

</html>
