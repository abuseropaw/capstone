<?php
	include_once 'sessionTeacher.php';
	include_once 'dbconnect.php';
    include_once 'ajaxJQuery/globalFunction.php';


	$id = mysqli_real_escape_string($con, $_GET['id']);
	$fid = mysqli_real_escape_string($con,$_GET['fid']);
	$sid = mysqli_real_escape_string($con,$_GET['sid']);
	$sectionName =mysqli_real_escape_string($con,$_GET['sname']);
	$subjID = mysqli_real_escape_string($con,$_GET['subjid']);
	$subjTitle = mysqli_real_escape_string($con,$_GET['subjTitle']);
	$subjDescription = mysqli_real_escape_string($con,$_GET['subjDesc']);

    $query = mysqli_query($con, "SELECT sy_id from section where section_id='".$sid."'");
    $temp = mysqli_fetch_row($query);
    $schoolYear = $temp[0];


    function setGradesofQuarter($con, $name, $countGrade, $grade,$quarter,$controlID,$id,$schoolYear)
    {
        $getControlID = mysqli_query($con, "SELECT control_id,control_remarks from studentinfo1 where Name='".$name."' and sy_year='".$schoolYear."'");
        $temporary = mysqli_fetch_row($getControlID);
        $controlID = $temporary[0];

        $getMaxID = mysqli_fetch_row(mysqli_query($con, "SELECT max(grade_id) from grade"));
        // if($temporary[1] == "DRP" || $temporary[1] == "T/O")
        // {
        //     alert("DROP");
        // }
        // else
        // {
        if($countGrade > 0){
            if(mysqli_query($con, "UPDATE grade set grade_value='".$grade."', grade_dateUpdate=NOW(), faculty_id='".$_SESSION['faculty_id']."' WHERE quarters_id='".$quarter."' and control_id='".$controlID."' and so_id='".$id."' and sy_id='".$schoolYear."'"))
            {

            }
            else if( mysqli_query($con, "INSERT INTO GRADE VALUES('".$grade."',null,NOW(),NOW(),'".$quarter."','".$_SESSION['faculty_id']."','".$controlID."','".$id."','".$schoolYear."')")){
                

            }
            else
            {
                echo "<script>
                                    
                                    alert('Error sa pag save!!');
                                </script>";
            }
        }else{
        
            if( mysqli_query($con, "INSERT INTO GRADE VALUES('".$grade."',null,NOW(),NOW(),'".$quarter."','".$_SESSION['faculty_id']."','".$controlID."','".$id."','".$schoolYear."')")){
                
            }
           
        }
          
    }

    function setGrades($con,$schoolYear,$id,$fid,$sid,$sectionName,$subjID,$subjTitle,$subjDescription){
        
        if(isset($_POST['addQuarter'])){
            require_once 'PHPExcel-1.8.1/Classes/PHPExcel.php';

            $quarter = mysqli_real_escape_string($con, $_POST['quarter1']);
            $file =$_FILES['files']['tmp_name'];
            $fileName= $_FILES['files']['name'];
            $fileError= $_FILES['files']['error'];

            $fileExtension = explode('.', $fileName);
            $fileActualExt = strtolower(end($fileExtension));
            $allowed = array('xlsx','xls','csv');
            if(in_array($fileActualExt, $allowed))
            {
                if($fileError === 0)
                {
                    $excelReader = PHPExcel_IOFactory::createReaderForFile($file);
                    $excelObj = $excelReader->load($file);
                    $worksheet = $excelObj->getActiveSheet();

                    $lastRow = $worksheet->getHighestRow();
                
                    $temp = explode('/', $quarter);
                    $quarter = $temp[0];
                    $quarterDesc = $temp[1];

                    $countGrade = mysqli_query($con, "SELECT count(*) from grade");
                    $temp1 = mysqli_fetch_row($countGrade);
                    $countGrade = $temp1[0];


                        switch ($quarterDesc) {
                            case '1st Quarter':
                            
                                for($row = 13; $row<=$lastRow; $row++){
                        
                                    $name = $worksheet->getCell('B'.$row)->getValue();
                                    $first = $worksheet->getCell('F'.$row)->getValue();
                                    $second = $worksheet->getCell('J'.$row)->getValue();
                                    $third = $worksheet->getCell('N'.$row)->getValue();
                                    $fourth = $worksheet->getCell('R'.$row)->getValue();
                                    
                                    setGradesofQuarter($con, $name, $countGrade, $first,$quarter,$controlID,$id,$schoolYear);
                                    
                                }
                                echo "<script>
                                    
                                    alert('Done!!');
                                </script>";
                                break;
                            case '2nd Quarter':
                                for($row = 13; $row<=$lastRow; $row++){
                        
                                    $name = $worksheet->getCell('B'.$row)->getValue();
                                    $first = $worksheet->getCell('F'.$row)->getValue();
                                    $second = $worksheet->getCell('J'.$row)->getValue();
                                    $third = $worksheet->getCell('N'.$row)->getValue();
                                    $fourth = $worksheet->getCell('R'.$row)->getValue();
                                    

                                    setGradesofQuarter($con, $name, $countGrade, $second,$quarter,$controlID,$id,$schoolYear);
                                }
                                echo "<script>alert('Done!!');</script>";
                                break;
                            case '3rd Quarter':
                                for($row = 13; $row<=$lastRow; $row++){
                        
                                    $name = $worksheet->getCell('B'.$row)->getValue();
                                    $first = $worksheet->getCell('F'.$row)->getValue();
                                    $second = $worksheet->getCell('J'.$row)->getValue();
                                    $third = $worksheet->getCell('N'.$row)->getValue();
                                    $fourth = $worksheet->getCell('R'.$row)->getValue();
                                    

                                    setGradesofQuarter($con, $name, $countGrade, $third,$quarter,$controlID,$id,$schoolYear);
                                }
                                echo "<script>alert('Done!!');</script>";
                                break;
                            case '4th Quarter':
                                for($row = 13; $row<=$lastRow; $row++){
                        
                                    $name = $worksheet->getCell('B'.$row)->getValue();
                                    $first = $worksheet->getCell('F'.$row)->getValue();
                                    $second = $worksheet->getCell('J'.$row)->getValue();
                                    $third = $worksheet->getCell('N'.$row)->getValue();
                                    $fourth = $worksheet->getCell('R'.$row)->getValue();
                                    

                                    setGradesofQuarter($con, $name, $countGrade, $fourth
                                        ,$quarter,$controlID,$id,$schoolYear);
                                }
                                echo "<script>alert('Done!!');</script>";

                                break;
                            default:
                                echo "<script>alert('Error on uploading the ECR file');</script>";
                                break;
                        }
                         
                    header("Location: teacherClassDetails.php?id=".$id."&subjid=".$subjID."&fid=".$fid."&sid=".$sid."&sname=".$sectionName."&subjTitle=".$subjTitle."&subjDesc=".$subjDescription."");
                }
                else
                {
                    echo "<script>
                                    
                                    alert('Error!!');
                                </script>";
                }
            }
            else
            {
                echo "<script>alert('The file is unsupported!!!');</script>";
            }

            
        }
        
        
    }

?>
<!DOCTYPE html>
       
    <!-- HEAD -->
    <?php include_once 'head.php'; ?> 
    <!-- HEAD   -->

    <body>
        <!-- HEADER -->
        <?php include_once 'header.php'; ?>
        <!-- HEADER -->

        <section id="main">
            <ol class="breadcrumb">
                <li><a href="teacherHome.php">Home</a></li>
                <li><a href="teacherClass.php">Classes</a></li>
                <li class="active">Class Information</li>
            </ol>
            <?php 
                $toggle = 'teacherClass';
                include_once 'sidebarTeacher.php'; 
            ?>

            <section id="content">

                <div class="container">

                    <div class="block-header">
                        <h1><i class="zmdi zmdi-account-box"></i> <?php echo $sectionName; ?></h1>
                        <h3><?php echo $subjTitle .": ".$subjDescription ?></h3>


                        <div class="actions btn-demo">
                            <?php 
                                if(getCurrentSY1($con) == $schoolYear)
                                {
                                    echo '<button type="submit" data-toggle="modal"  href="#modalImportGrade" name="imports" name="imports" class="btn btn-default btn-lg" ><i class="zmdi zmdi-upload"></i> <b>ECR Grades</b></button>';
                                }

                            ?> 
                        </div>
                    </div>
                    <div class="card">
                        <div class="action-header clearfix" >
                            <div class="ah-label hidden-xs" data-ma-action="action-header-open"><b>Click here to search for a learner</b></div>

                            <div class="ah-search" >
                                <input type="text" name='geSearch' id="geSearch" placeholder="Start typing..." class="ahs-input" autofocus>
                                <i class="ahs-close" data-ma-action="action-header-close">&times;</i>
                            </div>

                           <ul class="actions">
                                <li>
                                    <a href="#" data-ma-action="action-header-open">
                                        <i class="zmdi zmdi-search"></i>
                                    </a>
                                </li>


                            </ul>
                        </div>

                        <div class="card-body" id="hehe">


                            
                                <?php
                                    $query = mysqli_query($con, "SELECT * FROM controlDetails where section_id='".$sid."' order by Gender DESC,Student ASC");
                                    include_once 'ajaxJQuery/displayClassDetails.php'; 
                                 ?>
                            
                        </div>
                    </div>
                    
                </div>
            </section>
        </section>
        <div class="modal fade" id="modalGrade" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <form id="addGradeForm" method="post">
                        <div class="modal-header">
                            <h4 class="modal-title">ADD GRADE</h4>
                        </div>


                        <div class="modal-body">
                            
                            <div class='col-sm-12'>
                                <div class='form-group fg-line'>
                                    <input type="text" name="student" id="student" class="form-control" disabled>
                                </div>
                            </div>


                            <div class='col-sm-12'>
                                <div class='form-group fg-line'>
                                    <input type="number" name="grade" id="grade" class="form-control" placeholder="Grade Percentage" min="0" max="100" required>
                                    <input type="hidden" name="important_id" id="important_id">
                                    <input type="hidden" name="soID" id="soID" value="<?php echo $id ?>">
                                    <input type="hidden" name="sid" id="sid" value="<?php echo $sid ?>">

                                </div>
                            </div>
                            
                        </div>
                        <div class="modal-footer">
                            
                            <button type="submit" name="addQuarter" id="addQuarter" class="btn btn-success">START</button>
                            
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>


        <div class="modal fade" id="modalEditGrade" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <form id="editGradeForm" method="post">
                        <div class="modal-header">
                            <h4 class="modal-title"><b>EDIT GRADE</b></h4>
                        </div>


                        <div class="modal-body">
                            <div class='col-sm-12'>
                                <div class='form-group fg-line'>
                                    <input type="text" name="student1" id="student1" class="form-control" disabled>
                                </div>
                            </div>
                            <div class='col-sm-12'>
                                <div class='form-group fg-line'>
                                    <select class="selectpicker form-control" name="quarter" id="quarter" required>
                                        <option value="">QUARTER</option>
                                        <?php
                                            $queryQuarter = mysqli_query($con, "SELECT * from quarters where sy_id='".$schoolYear."'");
                                            while($row = mysqli_fetch_array($queryQuarter)){
                                                if($row[2] == "Closed")
                                                {
                                                    echo "<option value='".$row[0]."' disabled>".$row[1]."</option>";
                                                }
                                                else
                                                {
                                                    echo "<option value='".$row[0]."'>".$row[1]."</option>";
                                                }
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>


                            <div class='col-sm-12'>
                                <div class='form-group fg-line'>
                                    <input type="number" name="grade1" id="grade1" class="form-control" placeholder="Grade Percentage" min="0" max="100" required>
                                    <input type="hidden" name="important_id1" id="important_id1">
                                    <input type="hidden" name="soID" id="soID" value="<?php echo $id ?>">
                                    <input type="hidden" name="sid" id="sid" value="<?php echo $sid ?>">

                                </div>
                            </div>
                            
                        </div>
                        <div class="modal-footer">
                            
                            <button type="submit" name="addQuarter" id="addQuarter" class="btn btn-success">START</button>
                            
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modalImportGrade" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                <?php echo "
                    <form method='POST' id='uploadECR' enctype='multipart/form-data'>
                        <div class='modal-header'>
                            <h4 class='modal-title'><b>IMPORT ECR</b></h4>
                        </div>


                        <div class='modal-body'>
                           
                            <div class='col-sm-12'>
                                <div class='form-group fg-line'>
                                    <select class='selectpicker form-control' name='quarter1' id='quarter1' required>
                                        <option value=''>QUARTER</option>";
                                        
                                            $queryQuarter = mysqli_query($con, "SELECT * from quarters where sy_id='".$schoolYear."'");
                                            while($row = mysqli_fetch_array($queryQuarter)){
                                                if($row[2] == "Closed")
                                                {
                                                    echo "<option value='".$row[0].'/'.$row[1]."' disabled>".$row[1]." - CLOSED</option>";
                                                }else
                                                {
                                                echo "<option value='".$row[0].'/'.$row[1]."'>".$row[1]."</option>";
                                                }
                                            }
                                    echo "    
                                    </select>
                                </div>
                            </div>


                            <div class='col-sm-12'>
                                <div class='form-group fg-line'>
                                    <input type='file' name='files' id='files' class='form-control' required>
                                    <input type='hidden' name='sectionID' id='sectionID' value='".$sid."'>
                                    <input type='hidden' name='soid' id='soid' value='".$id."'>
                                </div>
                            </div>
                            
                        </div>
                        <div class='modal-footer'>
                            
                            <button type='submit' name='addQuarter' id='addQuarter' class='btn btn-success'>START</button>
                            
                            <button type='button' class='btn btn-danger' data-dismiss='modal'>Close
                            </button>
                        </div>
                    </form>"; ?>
                </div>
            </div>
        </div>

        <div id="loading" class="modal fade" data-backdrop="static" data-keyboard="false">  
            <div class="modal-dialog modal-sm">  
               <div class="modal-content">
               <center>   
                    <div class="modal-body" id="Details">
                         
                        <div class="preloader pl-xxl">
                            
                                <svg class="pl-circular" viewBox="25 25 50 50">
                                    <circle class="plc-path" cx="50" cy="50" r="20"/>
                                </svg>
                            
                        </div> 
                       <center><h4>Processing...Please wait</h4></center>
                    </div>  
                </center>
               </div>  
            </div>  
        </div>
        <!-- FOOTER -->
        <?php include_once 'footer.php' ?>
        <!-- FOOTER -->

        <!-- Javascript Libraries -->
        <?php include_once 'scripts.php'; ?>
        <!-- Javascript Libraries -->
        <script type="text/javascript" src="js/Notification.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                
                //$('#asd').DataTable();
                 
                $('#modalGrade').on('shown.bs.modal', function() {
                  $('#grade').focus();
                });

                $('#uploadECR').on('submit', function(e){
                    e.preventDefault();
                    var sid="<?php echo $sid; ?>";
                    var id="<?php echo $id; ?>";
                    $.ajax({
                        url: "ajaxJQuery/uploadECRGrades.php",
                        method: "post",
                        data: new FormData(this),
                        contentType:false,
                        cache: false,
                        processData:false,
                        beforeSend:function(){
                        $('#loading').modal('show');
                            $('#modalImportGrade').modal('hide');
                        },
                        success:function(data){
                            $('#loading').modal('hide');

                            if(data == 'close')
                            {
                                var message = "The quarter is closed";
                                executeNotif(message,"danger");
                            }
                            else if(data == "not allowed")
                            {
                                var message = "Accept only csv format of excel";
                                executeNotif(message,"danger");
                            }
                            else
                            {
                                $.ajax({
                                    url:"ajaxJQuery/getClassDetails.php",
                                    method:"get",
                                    data:{sid:sid,id:id},
                                    success:function(response){

                                        var message = "Successfully import grade";
                                        executeNotif(message,"success");
                                        $('#hehe').html(response);
                                        
                                        
                                        
                                    }
                                });


                                
                            }

                        }
                    });

                });


                $('#geSearch').keyup(function(){
                    var txt = $('#geSearch').val();
                    var secID = <?php echo $sid; ?>;
                    var soid = <?php echo $id; ?>;
                    
                    search(txt,secID,soid);
                        
                    
                });
                

                function search(txt,secID,soid)
                {
                    $.ajax({
                        url:"ajaxJQuery/searchStudentinClass.php",
                        method:"POST",
                        data:{search:txt,secID:secID,soid:soid},
                        dataType:"text",
                        success:function(data)
                        {
                            if(data=="")
                            {
                                $('#hehe').html(
                                    '<div class="card-body card-padding" id="hehe">'
                                        +'Your search - <b>'+txt+'</b> - did not match any learner.'
                                        +'<br />'
                                        +'Suggestions:<br />'
                                        +'<ul style="margin-left:1.3em;margin-bottom:2em"><li>Make sure that all words are spelled correctly.</li><li>Try different keywords.</li><li>Try more general keywords.</li></ul>'
                                    +'</div>'
                                );
                            }
                            else
                            {
                                $('#hehe').html(data);
                            }
                            //$('#asd').DataTable();
                        }
                    });
                }

                //ADD GRADE
                $('#addGradeForm').on("submit", function(event){
                    event.preventDefault();
                    var txt = $('#geSearch').val();
                    var secID = <?php echo $sid; ?>;
                    var soid = <?php echo $id; ?>;
                    
                    
                    if($('#grade').val() == '')
                    {
                        $('#type').focus();    
                         alert("You must enter a grade");
                    }
                   
                    else 
                    {
                        $.ajax({  
                              url:"ajaxJQuery/insertGrade.php",  
                              method:"POST",  
                              data:$('#addGradeForm').serialize(),  
                              success:function(data){ 
                                if(data == '0')
                                {
                                    var message = "Error on adding grade to learner";
                                    executeNotif(message,"danger");
                                    $('#modalGrade').modal('hide');
                                }
                                else if(data == "less" | data == "big")
                                {
                                    var message = "Grade must be valid";
                                    executeNotif(message,"danger");
                                }
                                else
                                {
                                      
                                   $('#addGradeForm')[0].reset();   
                                   $('#modalGrade').modal('hide');
                                   if(!txt)
                                   {
                                        search(txt,secID,soid);var message = "Successfully added grade to learner";
                                    executeNotif(message,"success");
                                    }
                                   
                                   
                                }
                              }  

                         });  
                    }
                });
                
                //UPDATE GRADE
                $('#editGradeForm').on("submit", function(event){
                    event.preventDefault();
                    var txt = $('#geSearch').val();
                    var secID = <?php echo $sid; ?>;
                    var soid = <?php echo $id; ?>;
                    if($('#grade1').val() == '')
                    {
                        $('#grade1').focus();    
                         alert("You must enter a grade");
                    }
                    else if($('#quarter').val() == 'QUARTER'){
                        $('#quarter').focus();    
                         alert("You must select a quarter");
                    }
                    else
                    {
                        $.ajax({  
                              url:"ajaxJQuery/updateGrade.php",  
                              method:"POST",  
                              data:$('#editGradeForm').serialize(),  
                              success:function(response){ 
                                    
                                   if(response == 0){
                                      
                                        var message = "Unable to update grade";
                                        executeNotif(message,"danger");
                                        $('#modalEditGrade').modal('hide');
                                       
                                   }
                                   else if(response == "less" | response == "big")
                                   {
                                        var message = "Grade must be valid";
                                        executeNotif(message,"danger"); 
                                   }
                                   else if(response == "close")
                                   {
                                    
                                        var message = "Unable to update grade, quarter is closed";
                                        executeNotif(message,"danger");
                                        $('#modalEditGrade').modal('hide');
                                   }
                                   else
                                   {
                                        
                                        
                                       
                                       $('#editGradeForm')[0].reset();
                                       $('#modalEditGrade').modal('hide');
                                       search(txt,secID,soid);
                                        var message = "Successfully updated grade to learner";
                                        executeNotif(message,"success");  
                                   }
                                       
                              }  

                         });  
                    }
                });

                
                $(document).on('click', '.add_grade', function(){
                    var important_id = $(this).attr("id");
                    $.ajax({
                        url:"ajaxJQuery/selectControl.php",
                        method:"POST",
                        data:{important_id:important_id},
                        dataType:"json",
                        success:function(data){
                            $('#student').val(data.Student);
                            $('#important_id').val(important_id);
                            $('#modalGrade').modal('show');

                        }

                    });
                });
                $(document).on('click', '.edit_grade', function(){
                    var important_id1 = $(this).attr("id");
                    $.ajax({
                        url:"ajaxJQuery/selectControl1.php",
                        method:"POST",
                        data:{important_id1:important_id1},
                        dataType:"json",
                        success:function(data){
                            $('#student1').val(data.Student);
                            $('#important_id1').val(important_id1);
                            $('#modalEditGrade').modal('show');
                        }

                    });
                });
            } );
        </script>
    </body>
  
<!-- Mirrored from byrushan.com/projects/ma/1-7-1/jquery/light/widget-templates.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 01 May 2017 06:35:54 GMT -->
</html>