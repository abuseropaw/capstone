?php
session_start();
    include_once 'dbconnect.php';
    $countAdmin = mysqli_num_rows(mysqli_query($con, "SELECT * from faculty_account where faculty_type='Administrator'"));
    if(isset($_POST['login'])){
        $username = mysqli_real_escape_string($con, $_POST['username']);
        $password = mysqli_real_escape_string($con, $_POST['password']);   
        

        $query = "CALL loginTeacher('".$username."','".$password."')";
        $result = mysqli_query($con, $query);
        if($row = mysqli_fetch_array($result)){
            $type = $row['faculty_type'];
            if($type == 'Administrator'){
                $_SESSION['faculty_type'] = $type;
                $_SESSION['faculty_id'] = $row['faculty_id'];
                $_SESSION['Name'] = $row['Name'];
                header("Location: adminHome.php");
            }else if($type == 'Teacher'){
                $_SESSION['faculty_type'] = $type;
                $_SESSION['faculty_id'] = $row['faculty_id'];
                $_SESSION['Name'] = $row['Name'];
                header("Location: teacherHome.php");
            }
            else if($type == 'Head Teacher' | $type == 'Registrar'){
                $_SESSION['faculty_type'] = $type;
                $_SESSION['faculty_id'] = $row['faculty_id'];
                $_SESSION['Name'] = $row['Name'];
                header("Location: registrarHome.php");
            }

        }else{
            $errormsg = "Incorrect Email or Password!!!";
        }

    }

    if(isset($_POST['createAdmin'])){
        $fname = mysqli_real_escape_string($con, $_POST['fname']);
        $mname = mysqli_real_escape_string($con, $_POST['mname']);
        $lname = mysqli_real_escape_string($con, $_POST['lname']);
        $password = mysqli_real_escape_string($con, $_POST['password']);
        $id=1000;
        if(mysqli_query($con, "INSERT into faculty_account values('".$id."','".$fname."','".$mname."','".$lname."','','','','','','','','Administrator',null,'".$password."',NOW(),'1000')")){
            
        }

        $query = "INSERT into yearlevel VALUES('10','Grade 7',NOW(),'".$id."');";
            $query.="INSERT into yearlevel VALUES('11','Grade 8',NOW(),'".$id."');";
            $query.="INSERT into yearlevel VALUES('12','Grade 9',NOW(),'".$id."');";
            $query.="INSERT into yearlevel VALUES('13','Grade 10',NOW(),'".$id."')";
        if(mysqli_multi_query($con, $query)){
            $checkmsg = "Welcome Administrator!!";
        }
    }
?>

<!DOCTYPE html>
<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>LNHS | HOME</title>

        <!-- Vendor CSS -->
         <link href="vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css" rel="stylesheet">
        <link rel="stylesheet" href="assets/css/main.css" />
        <!-- CSS -->
        <link href="css/app_1.min.css" rel="stylesheet">
    </head>

    <body class="loading">
        <div id="wrapper">
        <div id="bg"></div>
        <div id="overlay"></div>
        <div class="main">
            <header id="header" class="c-white">
                        <h1 class="c-white">Lugait National High School</h1>
                        <p>A School &nbsp;&bull;&nbsp; Home &nbsp;&bull;&nbsp; Future</p>
            </header>
                </div>
        <div class="login-content bgm-green">
            <!-- Login -->
            <div class="lc-block toggled" id="l-login">
                <div class="lcb-form">
                <form id="myForm" method="POST" action="index.php">
                    <div class="input-group m-b-20">
                        <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
                        <div class="fg-line">
                            <input type="text" class="form-control input-lg" name='username' id='username' placeholder="Username" required>
                        </div>
                    </div>

                    <div class="input-group m-b-20">
                        <span class="input-group-addon"><i class="zmdi zmdi-male"></i></span>
                        <div class="fg-line">
                            <input type="password" class="form-control input-lg" name='password' name='password' placeholder="Password" required>
                        </div>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" value="">
                            <i class="input-helper"></i>
                            Keep me signed in
                        </label>
                    </div>

                    
                    <button type="submit" class="btn btn-login btn-success btn-float" name='login' id='login'><i class="zmdi zmdi-arrow-forward"></i></button>
                </form>
                <span class="text-danger" id="result"><?php if (isset($checkmsg)) { echo $checkmsg; } ?></span>
                <span class="text-danger" id="result"><?php if (isset($errormsg)) { echo $errormsg; } ?></span>
                </div>

                
                <?php
                    
                    if($countAdmin != 0){
                        echo '
                        <div class="lcb-navigation">
                            <a href="#" data-ma-action="login-switch" data-ma-block="#l-register"><i class="zmdi zmdi-plus"></i> <span>Register</span></a>
                            
                        </div>';
                    }
                ?>
            </div>

            <!-- Register -->
            <div class="lc-block" id="l-register">

                <div class="lcb-form">
                    <h2>Administrator Account</h2>
                    <form id="myForm" method="POST" action="index.php">
                    <div class="input-group m-b-20">
                        <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
                        <div class="fg-line">
                            <input type="text" id="fname" name="fname" class="form-control" placeholder="First Name" required>
                        </div>
                    </div>
                    <div class="input-group m-b-20">
                        <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
                        <div class="fg-line">
                            <input type="text" id="mname" name="mname" class="form-control" placeholder="Middle Name">
                        </div>
                    </div>
                    <div class="input-group m-b-20">
                        <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
                        <div class="fg-line">
                            <input type="text" id="lname" name="lname" class="form-control" placeholder="Last Name" required>
                        </div>
                    </div>
                    <div class="input-group m-b-20">
                        <span class="input-group-addon"><i class="zmdi zmdi-key"></i></span>
                        <div class="fg-line">
                            <input type="password" id="password" name="password" class="form-control" placeholder="Password" required>
                        </div>
                    </div>


                    <button type="submit" name="createAdmin" class="btn btn-login btn-success btn-float"><i class="zmdi zmdi-arrow-forward"></i></button>
                    </form>
                </div>

                <div class="lcb-navigation">
                    <a href="#" data-ma-action="login-switch" data-ma-block="#l-login"><i class="zmdi zmdi-long-arrow-right"></i> <span>Sign in</span></a>
                    <a href="#" data-ma-action="login-switch" data-ma-block="#l-forget-password"><i>?</i> <span>Forgot Password</span></a>
                </div>
            </div>

            
    </div>
    </div>

        <!-- Javascript Libraries -->
        <script src="vendors/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

        <script src="vendors/bower_components/Waves/dist/waves.min.js"></script>

        <!-- Placeholder for IE9 -->
        <!--[if IE 9 ]>
            <script src="vendors/bower_components/jquery-placeholder/jquery.placeholder.min.js"></script>
        <![endif]-->

        <script src="js/app.min.js"></script>
        <script>
            window.onload = function() { document.body.className = ''; }
            window.ontouchmove = function() { return false; }
            window.onorientationchange = function() { document.body.scrollTop = 0; }
        </script>
        
    </body>
</html>
