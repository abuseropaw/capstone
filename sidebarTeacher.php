<?php 
    include_once 'dbconnect.php';
    $getPic=  mysqli_fetch_row(mysqli_query($con, "SELECT faculty_picture from faculty_account where faculty_id='".$_SESSION['faculty_id']."'"));
    $p =$getPic[0];
    echo " 
            <aside id='sidebar' class='sidebar c-overflow'>
                <div class='s-profile'>
                    <a href='#' data-ma-action='profile-menu-toggle'>
                        <div class='sp-pic'>";
                    if($p == '')
                    {
                        echo "<img src='img/default-profile.png' alt=''>";

                    }else
                    {
                        echo "<img src='profile/".$p."' alt=''>";

                    }
                    echo "
                        </div>

                        <div class='sp-info'>";
                            echo $_SESSION['Name'];
                        echo "    
                            <i class='zmdi zmdi-caret-down'></i>
                        </div>
                    </a>

                    <ul class='main-menu'>
                        <li>
                            <a href='teacherProfile.php'><i class='zmdi zmdi-account'></i> View Profile</a>
                        </li>
                        <li>
                            <a href='teacherChangePass.php'><i class='zmdi zmdi-lock'></i> Privacy Settings</a>
                        </li>
                        <li>
                            <a href='logout.php'><i class='zmdi zmdi-power'></i> Logout</a>
                        </li>
                    </ul>
                </div>

                <ul class='main-menu'>";
                    if($toggle == 'teacherHome'){
                        echo "
                        <li class='active toggled'>
                            <a href='teacherHome.php'><i class='zmdi zmdi-home'></i> Home</a>
                        </li>";
                    }else{
                        echo "
                        <li>
                            <a href='teacherHome.php'><i class='zmdi zmdi-home'></i> Home</a>
                        </li>";
                    }
                    
                    if($toggle == 'teacherClass'){
                        echo "
                        <li class='active toggled'>
                            <a href='teacherClass.php'><i class='zmdi zmdi-calendar-note'></i> Class</a>
                        </li>";
                    }else{
                        echo "
                        <li>
                            <a href='teacherClass.php'><i class='zmdi zmdi-calendar-note'></i> Class</a>
                        </li>";
                    }

                    if($toggle == 'teacherAdvisory'){
                        echo "
                        <li class='active toggled'>
                            <a href='teacherAdvisory.php'><i class='zmdi zmdi-calendar-note'></i> Advisory</a>
                        </li>";
                    }else{
                        echo "
                        <li>
                            <a href='teacherAdvisory.php'><i class='zmdi zmdi-calendar-note'></i> Advisory</a>
                        </li>";
                    }

                    
            echo "         
                </ul>
            </aside>";

?>
<!-- 
<li class="sub-menu">
    <a href="#" data-ma-action="submenu-toggle"><i class="zmdi zmdi-chart"></i> Reports</a>

    <ul>
        <li><a href="SF1.php"><i class="zmdi zmdi-file"> </i>  School Register (SF1)</a></li>
        <li><a href="SF2.php"><i class="zmdi zmdi-file"></i>  Daily Attendance Report of Learners (SF2)</a></li>
        <li><a href="SF3.php"><i class="zmdi zmdi-file"></i>  Books Issued and Returned (SF3)</a></li>
        <li><a href="SF4.php"><i class="zmdi zmdi-file"></i>  Monthly Learner's Movement and Attendance (SF4)</a></li>
        <li><a href="SF5.php"><i class="zmdi zmdi-file"></i>  Report on Promotion & Level of Proficiency (SF5)</a></li>
        
        
    </ul>
</li> -->