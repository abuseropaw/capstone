<?php
echo '
<div role="tabpanel" class="tab-pane" id="profile11">
    <div class="card">
        
        <div class="card-body card-padding">
            <div class="form-wizard-basic fw-container">
                <ul class="tab-nav text-center" data-tab-color="green">';
                   
                        $query123 = mysqli_query($con, "SELECT * from month where sy_id='".getSYofSection($con, $id)."' order by month_id DESC");
                        $iter = 1;
                        while ($row = mysqli_fetch_array($query123)) {
                            if($iter == 2){
                            echo '<li><a href="#tab'.$iter.'" aria-controls="tab'.$iter.'" data-toggle="tab">'.$row[1].'</a></li>';
                            }
                            else
                            {
                                echo '<li><a href="#tab'.$iter.'" aria-controls="tab'.$iter.'" data-toggle="tab">'.$row[1].'</a></li>';
                            }
                            $iter++;
                        }
                

            echo '
                </ul>

                <div class="tab-content">';
                   
                        $query123 = mysqli_query($con, "SELECT * from month where sy_id='".getSYofSection($con, $id)."' order by month_id DESC");
                        $i = 1;
                        
                        while ($row = mysqli_fetch_array($query123)) {
                            $totalPresentM = 0;
                            $totalPresentF = 0;
                            $totalDays = $row[2];
                            echo '
                            <div class="row tab-pane fade" id="tab'.$i.'">
                                <div class="col-sm-12">
                                    <div class="action-header">
                                        <div class="btn-demo">
                                        ';
                                        if($row[3] == 'Open')
                                            {
                                                echo '<button id="'.$row[0].'" class="btn btn-default btn-lg add_attendance"><i class="zmdi zmdi-plus-square"> </i> <b>Record Attendance</b></button>';
                                            }
                                        echo '
                                            <button id="'.$row[0].'" onclick=window.open("SF2-Excel.php?month='.$row[0].'&section='.$id.'") class="btn btn-default btn-lg generate_SF2"><i class="zmdi zmdi-download"></i> <b>School Form 2 - '.$row[1].'</b></button>
                                            

                                            ';
                                            
                                            
                                    echo '
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-7">
                                    <div class="card">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>';
                                                    if($row[3] == 'Open')
                                                    {
                                                        echo '<th>Edit</th>';
                                                    }
                                                    else
                                                    {
                                                       
                                                    }

                                                    echo '
                                                        <th>Learner</th>
                                                        <th>Remarks</th>
                                                        <th>Present</th>
                                                        <th>Absent</th>
                                                        <th>Tardy</th>
                                                        <th>Consecutive<br /> Absent</th>
                                                    </tr>
                                                </thead>
                                                <tbody>';
                                                    $query = mysqli_query($con, "SELECT * FROM controldetails where section_id='".$id."' order by Gender DESC,Student ASC");

                                                    while ($item = mysqli_fetch_array($query)) {
                                                        
                                                        $getAttendance = mysqli_query($con, "SELECT * from attendance where control_id='".$item[0]."' and month_id='".$row[0]."'");
                                                        $temp = mysqli_fetch_row($getAttendance);
                                                        
                                                        echo "
                                                        <tr>";
                                                        if( $row[3] == 'Open')
                                                        {
                                                            if($item[1] == "DRP" | $item[1] == "T/O")
                                                            {
                                                                echo "<td></td>";
                                                            }
                                                            else if($temp[0] != '')
                                                            {
                                                                echo "<td><button type='submit' name='edit' id=".$temp[5].'/'.$temp[7]." class='btn btn-default btn-sm edit_attendance'><i class='zmdi zmdi-edit'></i></button></td>";    
                                                            }
                                                            else
                                                            {
                                                                echo "<td><button type='submit' name='edit' class='btn btn-default btn-sm' disabled><i class='zmdi zmdi-edit'></i></button></td>";
                                                            }
                                                        }
                                                        else
                                                        {
                                                            
                                                        }

                                                        $getRemarks = mysqli_fetch_row(mysqli_query($con, "SELECT remarks,reqInformation from remarks_history where control_id='".$item[0]."' and month_desc='".$row[1]."'"));
                                                        echo "
                                                            <td>".$item[5]."</td>";
                                                            if(mysqli_num_rows(mysqli_query($con, "SELECT remarks,reqInformation from remarks_history where control_id='".$item[0]."' and month_desc='".$row[1]."'")) != 0)
                                                            {
                                                                echo "<td>".$getRemarks[0]." - ".$getRemarks[1]."</td>";
                                                            }
                                                            else
                                                            {
                                                                echo "<td>".$item[1]."</td>";
                                                            }
                                                        echo "
                                                            <td>".$temp[0]."</td>
                                                            <td>".$temp[1]."</td>
                                                            <td>".$temp[2]."</td>
                                                            <td>".$temp[3]."</td>
                                                        </tr>


                                                            ";
                                                    }

                                            echo '
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-5">
                                    <div class="card">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-hover">    
                                                <thead>
                                                    <tr>
                                                        <th rowspan="2" class="text-center"><b>Month: '.$row[1].'</b></th>
                                                        <th rowspan="2" class="text-center"><b>No. Days of Classes: '.$row[2].'</b></th>
                                                        <th colspan="3" class="text-center"><b>Summary</b></th>

                                                    </tr>
                                                    <tr>
                                                        <th class="text-center"><b>M</b></th>
                                                        <th class="text-center"><b>F</b></th>
                                                        <th class="text-center"><b>TOTAL</b></th>

                                                    </tr>
                                                </thead>
                                                <tbody>';

                                                    $rowNumber1M = mysqli_fetch_row(mysqli_query($con, "SELECT Total from Enrolmentasof1stFridayofJune where Gender = 'Male' and section_id='".$id."' and month_no IN ('5', '6')"));
                                                    $rowNumber1F = mysqli_fetch_row(mysqli_query($con, "SELECT Total from Enrolmentasof1stFridayofJune where Gender = 'Female' and section_id='".$id."' and month_no IN ('5', '6')"));
                                                    $totalrowNumber1 = $rowNumber1M[0] + $rowNumber1F[0];

                                                echo '
                                                    <tr>
                                                        <td colspan="2" class="text-center"><b><small>* Enrolment as of (1st Friday of June)</small></b></td>
                                                        <td>'.$rowNumber1M[0].'</td>
                                                        <td>'.$rowNumber1F[0].'</td>
                                                        <td>'.$totalrowNumber1.'</td>
                                                    </tr>';

                                                    $rowNumber2M = mysqli_fetch_row(mysqli_query($con, "SELECT Total from LateEnrollmentduringthemonth where Gender='Male' and section_id='".$id."' and Month='".$row[1]."'"));
                                                    $rowNumber2F = mysqli_fetch_row(mysqli_query($con, "SELECT Total from LateEnrollmentduringthemonth where Gender='Female' and section_id='".$id."' and Month='".$row[1]."'"));
                                                    $totalrowNumber2 = $rowNumber2M[0] + $rowNumber2F[0];

                                                echo '    
                                                    <tr>
                                                        <td colspan="2" class="text-center"><b><small>Late Enrollment during the month (beyond cut-off)</small></b></td>
                                                        <td>'.(($rowNumber2M[0] == '')? 0: $rowNumber2M[0]).'</td>
                                                        <td>'.(($rowNumber2F[0] == '')? 0: $rowNumber2F[0]).'</td>
                                                        <td>'.$totalrowNumber2.'</td>
                                                    </tr>';

                                                    $rowNumber10M = mysqli_fetch_row(mysqli_query($con, "SELECT * from TransferredInt where section_id='".$id."' and Gender='Male' and Month='".$row[1]."'"));
                                                    $rowNumber10F = mysqli_fetch_row(mysqli_query($con, "SELECT * from TransferredInt where section_id='".$id."' and Gender='Female' and Month='".$row[1]."'"));

                                                    $rowNumber3M = mysqli_fetch_row(mysqli_query($con, "SELECT count(*) from remarks_historydetails where Gender='Male' and section_id='".$id."' and month_desc='June'")); 
                                                    $rowNumber3F =mysqli_fetch_row(mysqli_query($con, "SELECT count(*) from remarks_historydetails where Gender='Female' and section_id='".$id."' and month_desc='June'")); 


                                                    $get1stMonth = mysqli_fetch_row(mysqli_query($con, "SELECT min(month_id) as month_id from month where sy_id='".getSYofSection($con, $id)."'"));
                                                    $getlastMonth = mysqli_fetch_row(mysqli_query($con, "SELECT max(month_id) as month_id from month where sy_id='".getSYofSection($con, $id)."'"));

                                                    $totalunregisteredMale = 0;
                                                    $totalunregisteredFemale = 0;

                                                    $totalDeductionMale = 0;
                                                    $totalDeductionFemale = 0;
                                                    for($iterLoop = $row[0]; $iterLoop >= $get1stMonth[0]; $iterLoop--)
                                                    {
                                                        $getDescriptionforMonth = mysqli_fetch_row(mysqli_query($con, "SELECT month_description from month where month_id='".$iterLoop."'"));
                                                        echo '';
                                                        $getUnregisterMale = mysqli_fetch_row(mysqli_query($con, "SELECT Total from totalunregistered where section_id='".$id."' and Gender='Male' and Month='".$getDescriptionforMonth[0]."'"));
                                                        $getUnregisterFemale = mysqli_fetch_row(mysqli_query($con, "SELECT Total from totalunregistered where section_id='".$id."' and Gender='Female' and Month='".$getDescriptionforMonth[0]."'"));

                                                        $totalunregisteredMale = $totalunregisteredMale + $getUnregisterMale[0];
                                                        $totalunregisteredFemale = $totalunregisteredFemale + $getUnregisterFemale[0];
                                                    }

                                                    for($iterLoop1 = ($row[0] +1) ; $iterLoop1 <= $getlastMonth[0]; $iterLoop1++)
                                                    {
                                                        $getDescriptionforMonth1 = mysqli_fetch_row(mysqli_query($con, "SELECT month_description from month where month_id='".$iterLoop1."'"));
                                                        
                                                        ///DIRI PAKO
                                                        $getdeductionMale = mysqli_fetch_row(mysqli_query($con, "SELECT count(*) from controldetails1 where section_id='".$id."' and Gender='Male' and Month='".$getDescriptionforMonth1[0]."' and control_remarks='T/I' and control_remarks='LE'"));
                                                        $getdeductionFemale = mysqli_fetch_row(mysqli_query($con, "SELECT count(*) from controldetails1 where section_id='".$id."' and Gender='Female' and Month='".$getDescriptionforMonth1[0]."' and control_remarks='T/I' and control_remarks='LE'"));

                                                        $totalDeductionMale = $totalDeductionMale + $getdeductionMale[0];
                                                        $totalDeductionFemale = $totalDeductionFemale + $getdeductionMale[0];
                                                    }


                                                    if($row[0] != $get1stMonth[0])
                                                    {
                                                       

                                                        $totalRegisteredMale = $rowNumber3M[0] - $totalunregisteredMale - $totalDeductionMale + $rowNumber10M[4] + $rowNumber2M[0];
                                                        $totalRegisteredFemale = $rowNumber3F[0] - $totalunregisteredFemale - $totalDeductionFemale + $rowNumber10F[4] + $rowNumber2F[0];
                                                        $totalrowNumber3 = $totalRegisteredMale + $totalRegisteredFemale;

                                                    }
                                                    else
                                                    {

                                                    $totalRegisteredMale = $rowNumber3M[0] - $totalunregisteredMale;
                                                    $totalRegisteredFemale = $rowNumber3F[0] - $totalunregisteredFemale;
                                                    $totalrowNumber3 = $totalRegisteredMale + $totalRegisteredFemale;

                                                    }


                                                 echo '   
                                                    
                                                    <tr>
                                                        <td colspan="2" class="text-center"><b><small>Registered Learners as of end of the month</small></b></td>
                                                        <td>'.$totalRegisteredMale.'</td>
                                                        <td>'.$totalRegisteredFemale.'</td>
                                                        <td>'.$totalrowNumber3.'</td>
                                                    </tr>';

                                                    $rowNumber4M = ($totalRegisteredMale / $rowNumber1M[0]) * 100;
                                                    $rowNumber4F = ($totalRegisteredFemale / $rowNumber1F[0]) * 100;
                                                    $totalrowNumber4 = ($rowNumber4M + $rowNumber4F) /2;

                                                echo '
                                                    <tr>
                                                        <td colspan="2" class="text-center"><b><small>Percentage of Enrolment as of end of the month</small></b></td>
                                                        <td>'.$rowNumber4M.'%</td>
                                                        <td>'.$rowNumber4F.'%</td>
                                                        <td>'.$totalrowNumber4.'%</td>
                                                    </tr>
                                                    <tr>';
                                                    $rowNumber5M = mysqli_fetch_row(mysqli_query($con, "SELECT * from TotalDailyAttendance where section_id='".$id."' and Gender='Male' and month_id='".$row[0]."'"));
                                                    $rowNumber5F = mysqli_fetch_row(mysqli_query($con, "SELECT * from TotalDailyAttendance where section_id='".$id."' and Gender='Female' and month_id='".$row[0]."'"));
                                                    $rowNumber5Ms = $rowNumber5M[4] / $totalDays;
                                                    $rowNumber5Fs= $rowNumber5F[4] / $totalDays;
                                                    $totalrowNumber5 = $rowNumber5Ms + $rowNumber5Fs;

                                                echo '
                                                        <td colspan="2" class="text-center"><b><small>Average Daily Attendance</small></b></td>
                                                        <td>'.$rowNumber5Ms.'</td>
                                                        <td>'.$rowNumber5Fs.'</td>
                                                        <td>'.$totalrowNumber5.'</td>
                                                    </tr>';
                                                    $rowNumber6M = 0;
                                                    $rowNumber6F = 0;
                                                    if($totalRegisteredMale != 0 && $totalRegisteredFemale != 0){
                                                        $rowNumber6M = ($rowNumber5Ms / $totalRegisteredMale) * 100;
                                                        $rowNumber6F = ($rowNumber5Fs / $totalRegisteredFemale) * 100;
                                                    }

                                                    $totalrowNumber6 = ($rowNumber6M + $rowNumber6F) / 2;

                                                echo '
                                                    <tr>
                                                        <td colspan="2" class="text-center"><b><small>Percentage of Attendance for the month</small></b></td>
                                                        <td>'.$rowNumber6M.'%</td>
                                                        <td>'.$rowNumber6F.'%</td>
                                                        <td>'.$totalrowNumber6.'%</td>
                                                    </tr>';


                                                    $rowNumber7M = mysqli_fetch_row(mysqli_query($con, "SELECT * from NumberofStudentsAbsentfor5ConsecutiveDays where section_id='".$id."' and Gender='Male' and month_id='".$row[0]."'"));
                                                    $rowNumber7F = mysqli_fetch_row(mysqli_query($con, "SELECT * from NumberofStudentsAbsentfor5ConsecutiveDays where section_id='".$id."' and Gender='Female' and month_id='".$row[0]."'"));
                                                    $totalrowNumber7 = $rowNumber7M[4] + $rowNumber7F[4];
                                                echo '
                                                    <tr>
                                                        <td colspan="2" class="text-center"><b><small>Number of students absent for 5 consecutive days:</small></b></td>
                                                        <td>'.(($rowNumber7M[4] == '')? 0:$rowNumber7M[4]).'</td>
                                                        <td>'.(($rowNumber7F[4] == '')? 0:$rowNumber7F[4]).'</td>
                                                        <td>'.$totalrowNumber7.'</td>
                                                    </tr>';

                                                    $rowNumber8M = mysqli_fetch_row(mysqli_query($con, "SELECT * from DropOut where section_id='".$id."' and Gender='Male' and Month='".$row[1]."'"));
                                                    $rowNumber8F = mysqli_fetch_row(mysqli_query($con, "SELECT * from DropOut where section_id='".$id."' and Gender='Female' and Month='".$row[1]."'"));
                                                    $totalrowNumber8 = $rowNumber8M[4] + $rowNumber8F[4];
                                                echo '
                                                    <tr>
                                                        <td colspan="2" class="text-center"><b><small>Drop out</small></b></td>
                                                        <td>'.(($rowNumber8M[4] == '')? 0:$rowNumber8M[4]).'</td>
                                                        <td>'.(($rowNumber8F[4] == '')? 0:$rowNumber8F[4]).'</td>
                                                        <td>'.$totalrowNumber8.'</td>
                                                    </tr>';


                                                    $rowNumber9M = mysqli_fetch_row(mysqli_query($con, "SELECT * from TransferredOut where section_id='".$id."' and Gender='Male' and Month='".$row[1]."'"));
                                                    $rowNumber9F = mysqli_fetch_row(mysqli_query($con, "SELECT * from TransferredOut where section_id='".$id."' and Gender='Female' and Month='".$row[1]."'"));
                                                    $totalrowNumber9 = $rowNumber9M[4] + $rowNumber9F[4];
                                                echo '
                                                    <tr>
                                                        <td colspan="2" class="text-center"><b><small>Transferred out</small></b></td>
                                                        <td>'.(($rowNumber9M[4] == '')? 0:$rowNumber9M[4]).'</td>
                                                        <td>'.(($rowNumber9F[4] == '')? 0:$rowNumber9F[4]).'</td>
                                                        <td>'.$totalrowNumber9.'</td>
                                                    </tr>';

                                                    $rowNumber10M = mysqli_fetch_row(mysqli_query($con, "SELECT * from TransferredInt where section_id='".$id."' and Gender='Male' and Month='".$row[1]."'"));
                                                    $rowNumber10F = mysqli_fetch_row(mysqli_query($con, "SELECT * from TransferredInt where section_id='".$id."' and Gender='Female' and Month='".$row[1]."'"));

                                                    $totalrowNumber10 = $rowNumber10M[4] + $rowNumber10F[4];

                                                echo '
                                                    <tr>
                                                        <td colspan="2" class="text-center"><b><small>Transferred in</small></b></td>
                                                        <td>'.(($rowNumber10M[4] == '')? 0:$rowNumber10M[4]).'</td>
                                                        <td>'.(($rowNumber10F[4] == '')? 0:$rowNumber10F[4]).'</td>
                                                        <td>'.$totalrowNumber10.'</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <img src="img/SF2-Guidlines.png" style="width:450px;height:300px">
                                    </div>
                                </div>
                            </div>';
                            $i++;
                        }

             


                echo '
                    <ul class="fw-footer pagination wizard">
                        <li class="previous first"><a class="a-prevent" href="#"><i
                                class="zmdi zmdi-more-horiz"></i></a></li>
                        <li class="previous"><a class="a-prevent" href="#"><i
                                class="zmdi zmdi-chevron-left"></i></a></li>
                        <li class="next"><a class="a-prevent" href="#"><i
                                class="zmdi zmdi-chevron-right"></i></a></li>
                        <li class="next last"><a class="a-prevent" href="#"><i
                                class="zmdi zmdi-more-horiz"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>';
?>