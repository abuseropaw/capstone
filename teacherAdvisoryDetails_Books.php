<?php
echo '
<div role="tabpanel" class="tab-pane" id="messages11">
    <div id="listofStudents">
        <div>
            <div class="btn-demo actions">';

               
                    if(checkifCanIssueOrReturn($con, $level))
                    {
                        if(getSYofSection($con, $id) == getCurrentSY($con))
                        {
                            echo '<button class="btn btn-default btn-lg" data-toggle="modal" href="#book"><i class="zmdi zmdi-plus-square"> </i> <b>Issue/Return Book</b></button>';
                        }
                    }
                    else
                    {
                        
                    }
                    echo "<button class='btn btn-default btn-lg' onclick=window.open('SF3-Excel.php?id=".$id."') data-toggle='tooltip' data-placement='top' title='' data-original-title='Generate School Form 3 (Books Issues and Returned) in excel file'><i class='zmdi zmdi-download'> </i>  <b>School Form 3</b></button>
                        

                    ";
        echo '     

            </div>
        </div>
        <br />
        <div class="card table-responsive">
        <table class="table table-bordered table-hover">
            <thead>
                <tr>';
                if(getSYofSection($con, $id) == getCurrentSY($con))
                {
                    echo '<th rowspan="2" class="text-center"><b><small>Edit/Delete</small></b></th>';
                }
               echo '
                <th rowspan="2" class="text-center" width="30%"><b><b>Learners Full Name</b></b></th>';

                $queryBooks = mysqli_query($con, "SELECT * from bookdetails where Level='".$level."' order by book_id ASC");
                $countBooks = mysqli_num_rows(mysqli_query($con, "SELECT * from bookdetails where Level='".$level."'"));
                $iter = 0;
                while($row = mysqli_fetch_array($queryBooks)){
                    $b[$iter] = $row[1];
                    $bID[$iter] = $row[0];
                    $iter++;
                }
                $arrlength = 0;
                //KUNG NAAY BOOKS
                if($countBooks > 0){
                    $arrlength = count($b);
                    for($i = 0; $i < $arrlength; $i++){
                        echo "
                            <th colspan='2' class='text-center'><b>".$b[$i]."</b></th>
                        ";
                    }
                    echo "
                        <th rowspan='2' class='text-center'><b>Remarks</b></th>
                    </tr>

                    <tr>";
                        for($i = 0; $i < $arrlength; $i++){
                            echo "<th class='text-center'><b><small><small><small>Issue</small></small></small></th>
                            <th class='text-center'><small><small><small>Return</small></small></small></b></th>";
                        }

                    echo "
                    </tr>";
                }
                //KUNG WALAY BOOKS
                else{
                    for($i = 0; $i < 8; $i++){
                        echo "
                            <th colspan='2' class='text-center'><b>Empty</b></th>
                        ";
                    }
                    echo "
                        <th rowspan='2' class='text-center'><b>Remarks</b></th>
                    </tr>

                    <tr>";
                        for($i = 0; $i < 8; $i++){
                            echo "<th class='text-center'><b><small><small><small>Issue</small></small></small></th>
                            <th class='text-center'><small><small><small>Return</small></small></small></b></th>";
                        }

                    echo "
                    </tr>";
                }
            




        echo "
            </thead>
            <tbody>";
               
                $remakrsOfBooksAcquired = '';
                    $query = mysqli_query($con, "SELECT * FROM controldetails where section_id='".$id."' order by Gender DESC,Student ASC");
                    while($row = mysqli_fetch_array($query)){
                        $temp = $row[0];
                    if($row[1] == 'DRP' | $row[1] == 'T/O')
                    {
                        echo "<tr class='c-red'>";
                    }
                    else
                    {
                        echo "<tr>";
                    }
                    
                    
                        if(getSYofSection($con, $id) == getCurrentSY($con))
                        {

                            echo "
                        <td>";
                            echo "
                            <div class='btn-demo'>
                                <button type='submit' name='editBookAcquired' id='".$temp."' class='btn btn-default btn-sm edit_booksacquired'><i class='zmdi zmdi-edit'></i></button>
                                <button type='submit' name='deleteBookAcquired' id='".$temp."' class='btn btn-default btn-sm delete_booksacquired'><i class='zmdi zmdi-delete'></i></button>
                            </div>
                            </td>";
                        }
                        echo "
                        <td width='15%'>".$row[5]."</td>
                    ";
                    if($arrlength != 0){
                        for($i = 0; $i < $arrlength; $i++){
                            $queryDateIssued =mysqli_query($con, "SELECT control_id,book_id, booksAcquired_remarks, DATE_FORMAT(date_acquired, '%d/%m/%y'),DATE_FORMAT(date_returned, '%d/%m/%y') from booksacquired where control_id='".$temp."' and book_id='".$bID[$i]."'");
                            if(mysqli_num_rows($queryDateIssued) == 0){
                                
                                    echo "<td></td><td></td>";
                                
                            }else{

                                if($row1 = mysqli_fetch_array($queryDateIssued)){
                                    if(!empty($row1[2]))
                                    {

                                        echo "<td>".$row1[3]."</td><td class='c-red'>".$row1[2]."</td>";
                                        switch ($row1[2]) {
                                            case 'FM':
                                                $remakrsOfBooksAcquired = 'LLTR';
                                                break;
                                            case 'TDO':
                                                $remakrsOfBooksAcquired = 'TLTR';
                                                break;
                                            case 'NEG':
                                                $remakrsOfBooksAcquired = 'PLT';
                                                break;
                                            default:
                                                # code...
                                                break;
                                        }

                                    }
                                    else
                                    {
                                        echo "<td>".$row1[3]."</td><td>".$row1[4]."</td>


                                        ";
                                    }
                                }

                            }

                        }


                    echo "<td><h3>".$remakrsOfBooksAcquired."</h3></td>
                    </tr>";

                       $remakrsOfBooksAcquired = '';
                    }
                    //KUNG WLAY BOOKS
                    else
                    {
                        for($i = 0; $i<$arrlength; $i++)
                        {
                            echo "<td></td><td></td>";
                        }
                        echo "<td><h3></h3></td>
                    </tr>";
                    }
                }
            echo "
            </tbody>
        </table>
    </div>
    </div>
</div>";

?>