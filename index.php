<?php
session_start();
    include_once 'dbconnect.php';
    $countAdmin = mysqli_num_rows(mysqli_query($con, "SELECT * from faculty_account where faculty_type='Administrator'"));
    if(isset($_POST['login'])){
        $username = mysqli_real_escape_string($con, $_POST['username']);
        $password = mysqli_real_escape_string($con, $_POST['password']);


        $query = "CALL loginTeacher('".$username."','".$password."')";
        $result = mysqli_query($con, $query);
        if($row = mysqli_fetch_array($result)){
            $type = $row['faculty_type'];
            if($type == 'Administrator'){
                $_SESSION['faculty_type'] = $type;
                $_SESSION['faculty_id'] = $row['faculty_id'];
                $_SESSION['Name'] = $row['Name'];
                header("Location: adminHome.php");
            }else if($type == 'Teacher'){
                $_SESSION['faculty_type'] = $type;
                $_SESSION['faculty_id'] = $row['faculty_id'];
                $_SESSION['Name'] = $row['Name'];
                header("Location: teacherHome.php");
            }
            else if($type == 'Head Teacher' | $type == 'Registrar'){
                $_SESSION['faculty_type'] = $type;
                $_SESSION['faculty_id'] = $row['faculty_id'];
                $_SESSION['Name'] = $row['Name'];
                header("Location: registrarHome.php");
            }

        }else{
            $errormsg = "Incorrect Email or Password!!!";
        }

    }

    if(isset($_POST['createAdmin'])){
        $fname = mysqli_real_escape_string($con, $_POST['fname']);
        $mname = mysqli_real_escape_string($con, $_POST['mname']);
        $lname = mysqli_real_escape_string($con, $_POST['lname']);
        $password = mysqli_real_escape_string($con, $_POST['password']);
        $id=1000;
        if(mysqli_query($con, "INSERT into faculty_account values('".$id."','".$fname."','".$mname."','".$lname."','','','','','','','','Administrator',null,'".$password."',NOW(),'1000')")){

        }

        $query = "INSERT into yearlevel VALUES('10','Grade 7',NOW(),'".$id."');";
            $query.="INSERT into yearlevel VALUES('11','Grade 8',NOW(),'".$id."');";
            $query.="INSERT into yearlevel VALUES('12','Grade 9',NOW(),'".$id."');";
            $query.="INSERT into yearlevel VALUES('13','Grade 10',NOW(),'".$id."')";
        if(mysqli_multi_query($con, $query)){
            $checkmsg = "Welcome Administrator!!";
        }
    }
?>

<!DOCTYPE html>
<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" type="image/x-icon" href="img/lnhs.ico" alt="logo" />

        <title>LNHS | HOME</title>

        <!-- Vendor CSS -->
        <link href="vendors/bower_components/animate.css/animate.min.css" rel="stylesheet">
        <link href="vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css" rel="stylesheet">
        
        <!-- CSS -->
        <link href="css/app_1.min.css" rel="stylesheet">

        <style>
            #bg {
              position: fixed; 
              top: -50%; 
              left: -50%; 
              width: 200%; 
              height: 200%;
              background-size: cover;

            }
            #bg img {
              position: absolute; 
              top: 0; 
              left: 0; 
              right: 0; 
              bottom: 0; 
              margin: auto; 
              min-width: 50%;
              min-height: 50%;
              max-width: 50%;
              max-height: 50%;
            }
        </style>
</head>

<body>
    
    
        <!-- <div id="bg"></div> -->
    <div id="bg">
        <img src="assets/css/images/asdsdasdsadas.jpg" alt="">
    </div>

   <div class="login-content col" >

        <div class="lc-block lc-block-alt toggled" id="l-lockscreen" >

            <div class="lcb-form b-0" style="filter:alpha(opacity=75); opacity:1;">
                <h3 class="c-black"><b>Lugait National High School</b></h3>
            <p>A School &nbsp;&bull;&nbsp; Home &nbsp;&bull;&nbsp; Future</p>
                <img class="lcb-user pull-right" src="img/lnhs.ico" alt="" style="font-size: calc(2vw + 1vh);">
                <div class="btn-demo">

                    <button class="btn btn-success" onclick="mywork()"><i class="zmdi zmdi-account"></i>  My Work</button>&nbsp;
                    <button class="btn btn-success" onclick="myschool()"><i class="zmdi zmdi-graduation-cap"></i>  My School</button>

                </div>
            </div>

        </div>

    
    </div>
    

    <!-- Javascript Libraries -->
    <script src="vendors/bower_components/jquery/dist/jquery.min.js"></script>
    <script src="vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <script src="vendors/bower_components/Waves/dist/waves.min.js"></script>

    <!-- Placeholder for IE9 -->
    <!--[if IE 9 ]>
        <script src="vendors/bower_components/jquery-placeholder/jquery.placeholder.min.js"></script>
    <![endif]-->

    <script src="js/app.min.js"></script>
    <script>
        function myschool(){
            window.location.assign("Student/studentLogin.php")
        }
        function mywork()
        {
            window.location.assign("teacherLogin.php");
        }
        window.onload = function() { document.body.className = ''; }
        window.ontouchmove = function() { return false; }
        window.onorientationchange = function() { document.body.scrollTop = 0; }
    </script>

</body>
</html>
