<?php
	session_start();
	include_once 'dbconnect.php';
	require('Report/fpdf.php');

	$pdf = new FPDF('P','in',array(8.27,11.69));
	$pdf->AddPage();

	$sy = mysqli_real_escape_string($con, $_GET['sy']);
	$level = mysqli_real_escape_string($con, $_GET['level']);
	$remarks = mysqli_real_escape_string($con, $_GET['remarks']);

	$getSY = mysqli_fetch_row(mysqli_query($con, "SELECT sy_year from schoolyear where sy_id='".$sy."'"));

	$pdf->SetFont("Arial","B","15");
	$pdf->Ln(.5);
	$pdf->SetLeftMargin(.2);
	switch ($remarks) {
		case 'T/I':
			$pdf->Cell(0,0.20,"Report on Tranferred In   ",0,1,"C"); 
			setParentHeader($pdf,$getSY[0],$level);
			$query = "SELECT * from controldetails where SchoolYear='".$getSY[0]."' and gradelevel='".$level."' and control_remarks='T/I' order by Student ASC";
			setData($con,$pdf,$getSY[0],$level,$remarks,$query);
			setFooter($pdf);
			break;
		case 'T/O':
			$pdf->Cell(0,0.20,"Report on Transferred Out",0,1,"C");
			setParentHeader($pdf,$getSY[0],$level); 
			$query = "SELECT * from controldetails where SchoolYear='".$getSY[0]."' and gradelevel='".$level."' and control_remarks='T/O' order by Student ASC";
			setData($con,$pdf,$getSY[0],$level,$remarks,$query);
			setFooter($pdf);
			break;
		case 'DRP':
			$pdf->Cell(0,0.20,"Report on Dropout",0,1,"C"); 
			setParentHeader($pdf,$getSY[0],$level);
			$query = "SELECT * from controldetails where SchoolYear='".$getSY[0]."' and gradelevel='".$level."' and control_remarks='DRP' order by Student ASC";
			setData($con,$pdf,$getSY[0],$level,$remarks,$query);
			setFooter($pdf);
			break;
		case 'LE':
			$pdf->Cell(0,0.20,"Report on Late Enrollment",0,1,"C"); 
			setParentHeader($pdf,$getSY[0],$level);
			$query = "SELECT * from controldetails where SchoolYear='".$getSY[0]."' and gradelevel='".$level."' and control_remarks='LE' order by Student ASC";
			setData($con,$pdf,$getSY[0],$level,$remarks,$query);
			setFooter($pdf);
			break;
		case 'Pending':
			$pdf->Cell(0,0.20,"Report on Pending Transferred In",0,1,"C"); 
			setParentHeader($pdf,$getSY[0],$level);
			$query = "SELECT * from controldetails where SchoolYear='".$getSY[0]."' and gradelevel='".$level."' and control_remarks='Pending TI' order by Student ASC";
			setData($con,$pdf,$getSY[0],$level,$remarks,$query);
			setFooter($pdf);
			break;
		default:
			# code...
			break;
	}
	$pdf->Output();



	function setData($con,$pdf,$sy,$level,$remarks,$query)
	{

		$getTI = mysqli_query($con, $query);
		$i=1;
        while ($row = mysqli_fetch_array($getTI)) {
           $pdf->Ln();
			$pdf->SetFont("Arial","","9");
		 	$pdf->Cell(.5,.20,"",0,0,"C");
		 	$pdf->Cell(.4,.20,$i,1,0,"C");
		 	$pdf->Cell(1.3,.20,$row['student_id'],1,0,"L");
		 	$pdf->Cell(4,.20,characterConverter($row['Student']),1,0,"L");
		 	$pdf->Cell(1.4,.20,characterConverter($row['SectionName']),1,0,"L");
		 	$i++; 
        }
	}

	function setParentHeader($pdf,$sy,$level)
	{
		$pdf->Ln(.0001);
		$pdf->SetFont("Arial","","12");
		$pdf->Cell(0,0.15,"Lugait National High School",0,1,"C");
		
		
		$pdf->Ln(.0001);
		$pdf->SetFont("Arial","","11");
		$pdf->Cell(0,0.15,"School Year: ".$sy,0,1,"C");
		$pdf->Ln(.0001);
		$pdf->SetFont("Arial","","11");
		$pdf->Cell(0,0.15,"Level: ".$level,0,1,"C");
		
		$pdf->Ln(.0001);
		$pdf->SetFont("Arial","","11");
		$pdf->Cell(0,0.15,date('M-d-Y'),0,1,"C");


		$pdf->Image('Report/kne.png',1,.75,.9,.9);
		$pdf->Image('Report/deped.png',6,.82,1.8,.8);

		$pdf->Ln(.3);
		$pdf->Ln(.1);
		
		setTableHeader($pdf);
		
	}

	function setTableHeader($pdf)
	{
		$pdf->SetFont("Arial","B","10");
	 	$pdf->Cell(.5,.45,"",0,0,"C");
	 	$pdf->Cell(.4,.45,"NO",1,0,"C");
	 	$pdf->Cell(1.3,.45,"LRN",1,0,"C");
	 	$pdf->Cell(4,.45,"LEARNER'S NAME",1,0,"C");
	 	$pdf->Cell(1.4,.45,"SECTION",1,0,"C");
	 	
	}
	function setFooter($pdf)
	{
		$pdf->Ln(1);
		$pdf->SetFont("Arial","B","12");
		 	$pdf->Cell(.7,.30,"",0,0,"C");
		 	$pdf->Cell(1.7,.30,"",0,0,"L");
		 	$pdf->Cell(3.9,.30,"PREPARED BY:",0,0,"R");
		 $pdf->Ln();

		$pdf->SetFont("Arial","U","12");
		 $pdf->Cell(13,.30,"          ".characterConverter($_SESSION['Name'])."         ",0,0,"C");
		 	
	}

	function characterConverter($string)
	{
		$padparan = stripslashes($string);
		$econvert = iconv('UTF-8', 'windows-1252', $padparan);

		return $econvert;
	}
?>