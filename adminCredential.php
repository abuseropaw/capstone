<?php
    include_once 'sessionAdmin.php';
    include_once 'dbconnect.php';
?>
<!DOCTYPE html>
    <!-- HEAD -->
    <?php include_once 'head.php'; ?>
    <!-- HEAD   -->
    <body>
        <!-- HEADER -->
        <?php include_once 'header.php'; ?>
        <!-- HEADER -->

        <section id="main">
            <ol class="breadcrumb">
                <li><a href="adminHome.php">Home</a></li>
                <li class="active">Credentials</li>
            </ol>
            <?php
                $toggle = 'adminCredential';
                include_once 'sidebar.php';
            ?>
            <section id="content">
                <div class="container">
                    <!-- Colored Headers -->
                    <div class="block-header">
                        <h1><i class="zmdi zmdi-layers"></i> CREDENTIALS
                        </h1>

                        <div class="actions">
                            <button  href="#modalColor" data-toggle="modal" class="btn btn-default btn-lg"><i class="zmdi zmdi-layers"></i> New credential</button>
                        </div>
                        <div class="modal fade" id="modalColor" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <form id="myForm" method="post" action="insertCredential.php" >
                                        <div class="modal-header">
                                            <h1 class="modal-title">NEW CREDENTIAL</h1>
                                        </div>

                                        <div class="modal-body">
                                            <div class='col-sm-12'>

                                                <div class='form-group fg-float'>
                                                    <div class="fg-line">
                                                        <input type='text' name='cr_title' id='cr_title' class='form-control fg-input'>
                                                        <label class="fg-label">Title ex. Form 137</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class='col-sm-12'>

                                                <div class='form-group fg-float'>
                                                    <div class="fg-line">
                                                        <input type='text' name='cr_description' id='cr_description' class='form-control fg-input'>
                                                        <label class="fg-label">Description</label>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="modal-footer">

                                            <button type="submit" name="insert1" id="insert1" class="btn btn-success">CREATE CREDENTIAL</button>
                                            <button type="button" class="btn btn-warning" data-dismiss="modal">CANCEL
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="modal fade" id="modalColor1" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <form id="myForm1" method="post"" >
                                        <div class="modal-header">
                                            <h1 class="modal-title">UPDATE CREDENTIAL</h1>
                                        </div>

                                        <div class="modal-body">
                                            <div class='col-sm-12'>

                                                <div class='form-group fg-float'>
                                                    <div class="fg-line">
                                                        <input type='text' name='cr_title1' id='cr_title1' class='form-control fg-input'>
                                                        <label class="fg-label">Title ex. Form 137</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class='col-sm-12'>

                                                <div class='form-group fg-float'>
                                                    <div class="fg-line">
                                                        <input type='text' name='cr_description1' id='cr_description1' class='form-control fg-input'>
                                                        <label class="fg-label">Description</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <input type="hidden" name="cr_id" id="cr_id">
                                            <button type="submit" name="insert1" id="insert1" class="btn btn-success">CREATE CREDENTIAL</button>
                                            <button type="button" class="btn btn-warning" data-dismiss="modal">CANCEL
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div id="dataModal" class="modal fade" data-backdrop="static" data-keyboard="false">
                            <div class="modal-dialog">
                               <div class="modal-content">
                                    <div class="modal-header">
                                         <button type="button" class="close" data-dismiss="modal">&times;</button>
                                         <h4 class="modal-title">CREDENTIAL INFORMATION</h4>
                                    </div>
                                    <div class="modal-body" id="Details">
                                    </div>
                                    <div class="modal-footer">
                                         <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
                                    </div>
                               </div>
                            </div>
                        </div>

                    </div>
                    <div class="card">
                        <div class="action-header clearfix" data-ma-action="action-header-open">
                            <div class="ah-label hidden-xs">Click here to search</div>

                            <div class="ah-search">
                                <input type="text" name='search1' id="search1" placeholder="Start typing..." class="ahs-input" autofocus>
                                <i class="ahs-close" data-ma-action="action-header-close">&times;</i>
                            </div>

                           <ul class="actions">
                                <li>
                                    <a href="#" data-ma-action="action-header-open">
                                        <i class="zmdi zmdi-search"></i>
                                    </a>
                                </li>


                            </ul>
                        </div>

                        <div class="card-body">


                            <div id="hehe">
                                <?php include_once 'ajaxJQuery/displayCredential.php'; ?>
                            </div>
                        </div>
                    </div>

                </div>
                <button href='#modalColor' data-toggle="modal" class="btn btn-float bgm-green m-btn"><i class="zmdi zmdi-layers"></i></button>
            </section>
        </section>

        <!-- FOOTER -->
        <?php include_once 'footer.php' ?>
        <!-- FOOTER -->

        <!-- Javascript Libraries -->
        <?php include_once 'scripts.php'; ?>
        <!-- Javascript Libraries -->

        <script type="text/javascript" src="js/Notification.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){

                $('#modalColor').on('shown.bs.modal', function() {
                  $('#cr_title').focus();
                })
                $('#modalColor1').on('shown.bs.modal', function() {
                  $('#cr_title1').focus();
                  $('#cr_description1').focus();

                })


                $('[data-dismiss=modal]').on('click', function (e) {
                    $('#myForm')[0].reset();
                    $('#myForm1')[0].reset();
                })
                $('#search1').keyup(function(objEvent){
                    var txt = $('#search1').val();


                    $.ajax({
                        url:"ajaxJQuery/searchCredential.php",
                        method:"POST",
                        data:{search:txt},
                        dataType:"text",
                        success:function(data)
                        {
                            $('#hehe').html(data);
                        }
                    });


                });
                $('#globalSearch').keyup(function(objEvent){
                    var txt = $('#globalSearch').val();


                    $.ajax({
                        url:"ajaxJQuery/searchCredential.php",
                        method:"POST",
                        data:{search:txt},
                        dataType:"text",
                        success:function(data)
                        {
                            $('#hehe').html(data);
                        }
                    });


                });
                $(document).on('click', '.details', function(){
                    var cr_id = $(this).attr("id");
                    $.ajax({
                        url:"ajaxJQuery/viewCredential.php",
                        method:"POST",
                        data:{cr_id:cr_id},

                        success:function(data){
                            $('#Details').html(data);
                            $('#dataModal').modal('show');
                        }
                    });
                });
                 $(document).on('click', '.delete_credential', function(){
                    var credential_id = $(this).attr("id");
                    swal({
                        title: "Are you sure?",
                        text: "You will not be able to recover this data!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonText: "Yes, delete it!",
                    }).then(function(){

                        $.ajax({
                            url:"ajaxJQuery/deleteCredential.php",
                            method:"POST",
                            data:{credential_id:credential_id},

                            success:function(data){
                                if(data == '0')
                                {
                                    var message = "Error on deleting credential.";
                                        executeNotif(message,"danger");
                                }
                                else
                                {

                                    var message = "Successfully deleted credential.";
                                    executeNotif(message,"success");
                                    $('#hehe').html(data);


                                }

                            }

                        });
                    });

                });

                $(document).on('click', '.edit_credential', function(){
                    var cr_id = $(this).attr("id");
                    $.ajax({
                        url:"ajaxJQuery/selectCredential.php",
                        method:"POST",
                        data:{cr_id:cr_id},
                        dataType: "json",
                        success:function(data){
                            $('#cr_title1').val(data.cr_title);
                            $('#cr_description1').val(data.cr_description);
                            $('#cr_id').val(data.cr_id);
                            $('#modalColor1').modal('show');
                        }
                    });
                });
                $('#myForm1').on("submit", function(event){
                    event.preventDefault();
                    if($('#cr_title1').val() == '')
                    {
                        $('#cr_title1').focus();
                         alert("Title is required");
                    }
                    else if($('#cr_description1').val() == '')
                    {
                         $('#cr_description1').focus();
                         alert("Description is required");
                    }

                    else
                    {
                         $.ajax({
                              url:"ajaxJQuery/updateCredential.php",
                              method:"POST",
                              data:$('#myForm1').serialize(),
                              beforeSend:function(){
                                   $('#insert1').val("Inserting..");
                              },
                              success:function(data){

                                if(data == '0')
                                {
                                    var message = "Error on updating credential.";
                                        executeNotif(message,"danger");
                                }
                                else
                                {

                                    var message = "Successfully updated credential.";
                                        executeNotif(message,"success");
                                   $('#myForm1')[0].reset();
                                   $('#modalColor1').modal('hide');

                                   $('#hehe').html(data);

                                }
                              }

                         });
                    }
                });
                $('#myForm').on("submit", function(event){
                    event.preventDefault();
                    if($('#cr_title').val() == '')
                    {
                        $('#cr_title').focus();
                         alert("Title is required");
                    }
                    else if($('#cr_description').val() == '')
                    {
                         $('#cr_description').focus();
                         alert("Description is required");
                    }

                    else
                    {
                         $.ajax({
                              url:"ajaxJQuery/insertCredential.php",
                              method:"POST",
                              data:$('#myForm').serialize(),
                              beforeSend:function(){
                                   $('#insert1').val("Inserting..");
                              },
                              success:function(data){
                                    if(data == '0')
                                   {
                                        var message = "Error on adding new credential.";
                                        executeNotif(message,"danger");
                                   }
                                   else
                                   {
                                        var message = "Successfully added new credential.";
                                        executeNotif(message,"success");
                                       $('#myForm')[0].reset();

                                       $('#modalColor').modal('hide');
                                        $('#hehe').html(data);
                                    }
                              }

                         });
                    }
                });
            });



        </script>
    </body>


</html>
