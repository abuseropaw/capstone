<?php
    include_once 'sessionAdmin.php';
    include_once 'dbconnect.php';

    $getCurrentSY = mysqli_query($con, "SELECT sy_year from schoolyear where sy_remarks='Open'");
    $temp = mysqli_fetch_row($getCurrentSY);
    $SY = $temp[0];
    function fillSchoolYear($con){
        $output ='';
        $query = mysqli_query($con, "SELECT * from schoolyear order by sy_id DESC");
        if(check($con))
        {

        }
        else
        {
            $output .='<option disabled="disabled" selected>NO CURRENT SY</option>';
        }
        while($row = mysqli_fetch_array($query)){
            if($row[4] == 'Open'){
                $output .='<option value="'.$row[1].'"><b>'.$row[1].' - CURRENT</b></option>';
            }else
            {
                $output .='<option value="'.$row[1].'">'.$row[1].'</option>';
            }
            
        }
        return $output;
    }
    function check($con)
    {
        $query = mysqli_num_rows(mysqli_query($con, "SELECT * from schoolyear where sy_remarks='Open'"));
        if($query > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
?>
<!DOCTYPE html>
    <!-- HEAD -->
    <?php include_once 'head.php'; ?> 
    <!-- HEAD   -->
    <body>
        <!-- HEADER -->
        <?php include_once 'header.php'; ?>
        <!-- HEADER -->

        <section id="main">
            <ol class="breadcrumb">
                <li><a href="adminHome.php">Home</a></li>
                <li class="active">Classes</li>
            </ol>
            <?php 
                $toggle = 'adminClass';
                include_once 'sidebar.php'; 
            ?>
            <section id="content">
                <div class="container">
                    <!-- Colored Headers -->
                    
                    <br />
                    <div class="block-header">
                        <div class="class">
                            <h1><i class="zmdi zmdi-calendar-check"></i> Classes
                            </h1>


                            <div class="actions">
                                <div class="btn-demo row">
                                <?php if(fillSchoolYear($con) == '<option disabled="disabled" selected>NO CURRENT SY</option>'){
                                    echo '
                                    <select class="btn btn-group btn-lg" id="sy" name="sy" >
                                    <optgroup label="School Year"><b>';
                                        
                                        echo fillSchoolYear($con);
                                    echo '</b></select>';
                                }else{
                                    echo '
                                    <select class="btn btn-group btn-lg" id="sy" name="sy" >
                                    <optgroup label="School Year"><b>';
                                        
                                        echo fillSchoolYear($con);
                                    echo '</b></select>
                                    
                                    <button  href="#modalLoad"  data-toggle="modal" class="btn btn-default btn-lg"><i class="zmdi zmdi-calendar-note"></i> <b>Load Class</b></button>

                                   
                                        ';
                                } ?>
                                </div>
                            </div>
                        </div>
                        <div class="modal fade" id="modalColor" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <form id="myForm" method="post" >
                                        <div class="modal-header">
                                            <h3 class="modal-title"><b>NEW CLASS</b></h3>
                                        </div>
                                        
                                        <div class="modal-body">
                                            <div class='col-sm-12'>
                                                <div class='form-group fg-float'>
                                                    <div class="fg-line">
                                                        <input type='text' name='section_title' id='section_title' class='form-control fg-input' required>
                                                        <label class="fg-label">Title </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group fg-line">
                                                    <select class="selectpicker form-control" name="year_lvl_title" id="year_lvl_title">
                                                        
                                
                                                    </select>
                                                </div>
                                            </div>
                                            <div class='col-sm-8'>
                                                <div class='form-group fg-line'>
                                                    <select class='selectpicker form-control' data-live-search="true" name='section_adviser' id='section_adviser'>
                                                        <option>ADVISER</option>
                                            <?php
                                                $query = mysqli_query($con, "SELECT * from facultysmalldetail where faculty_type='Teacher'");
                                                while($row = mysqli_fetch_array($query)){
                                                    $id = $row['faculty_id'];

                                                    $checkExistence = mysqli_query($con, "SELECT * from adviser where faculty_id='".$id."' and sy_id='".$SY."'");

                                                    if($row1 = mysqli_fetch_array($checkExistence)){

                                                    }else{
                                                        echo "
                                                            <option value='".$row[0]."'>".$row['Name']."</option>
                                                        ";
                                                    }

                                                }
                                            ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <br />
                                            <br />
                                            <br />
                                            <br />
                                            <br /><br /><br />
                                            <div class="panel panel-collapse">
                                                <div class="panel-heading" role="tab" id="headingOne">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"
                                                           aria-expanded="false" aria-controls="collapseOne">
                                                            <b>More Information</b> (Optional)
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseOne" class="collapse out" role="tabpanel"
                                                     aria-labelledby="headingOne">
                                                    <div class="panel-body">
                                                        <div class='col-sm-4'>
                                                            <div class='form-group fg-float'>
                                                                <div class="fg-line">
                                                                    <input type='number' name='section_capacity' id='section_capacity' class='form-control fg-input' >
                                                                    <label class="fg-label">Capacity(Optional) </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class='col-sm-4'>
                                                            <div class='form-group fg-float'>
                                                                <div class="fg-line">
                                                                    <input type='number' name='section_maxGrade' id='section_maxGrade' class='form-control fg-input' >
                                                                    <label class="fg-label">Maximum grade(Optional) </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class='col-sm-4'>
                                                            <div class='form-group fg-float'>
                                                                <div class="fg-line">
                                                                    <input type='number' name='section_minGrade' id='section_minGrade' class='form-control fg-input' >
                                                                    <label class="fg-label">Minimum grade(Optional) </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        </div>
                                        <div class="modal-footer">
                                            
                                            <button type="submit" name="insert1" id="insert1" class="btn btn-success">CREATE CLASS</button>
                                            <button type="button" class="btn btn-warning" data-dismiss="modal">CANCEL
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        

                        <div class="modal fade" id="modalColor1" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <form id="myForm1" method="post">
                                        <div class="modal-header">
                                            <h2 class="modal-title"><b>EDIT CLASS</b></h2>
                                        </div>
                                        
                                        <div class="modal-body">
                                            <div class='col-sm-12'>
                                                <div class='form-group fg-line'>
                                                    <input type='text' name='section_title1' id='section_title1' class='form-control' placeholder='Title' autofocus='enable' required>
                                                </div>
                                            </div>
                                            <div class='col-sm-4'>
                                                <div class='form-group fg-line'>
                                                    <input type='number' name='section_capacity1' id='section_capacity1' class='form-control' placeholder='Capacity'>

                                                </div>
                                            </div>
                                            <div class='col-sm-4'>
                                                <div class='form-group fg-line'>
                                                    <input type='number' name='section_maxGrade1' id='section_maxGrade1' class='form-control' placeholder='Maximum grade'>
                                                </div>
                                            </div>
                                            <div class='col-sm-4'>
                                                <div class='form-group fg-line'>
                                                    <input type='number' name='section_minGrade1' id='section_minGrade1' class='form-control' placeholder='Minimum grade'>
                                                </div>
                                            </div>
                                            
                                            <div class='col-sm-8'>
                                                <div class='form-group fg-line'>
                                                    <select class='selectpicker form-control' data-live-search="true" name='section_adviser1' id='section_adviser1'>
                                                        
                                            <?php
                                                $query = mysqli_query($con, "SELECT * from facultysmalldetail where faculty_type='Teacher'");
                                                while($row = mysqli_fetch_array($query)){
                                                    $id = $row['faculty_id'];

                                                    $checkExistence = mysqli_query($con, "SELECT * from adviser where faculty_id='".$id."' and sy_id='".$SY."'");

                                                    if($row1 = mysqli_fetch_array($checkExistence)){

                                                    }else{
                                                        echo "
                                                            <option value='".$row[0]."'>".$row['Name']."</option>
                                                        ";
                                                    }

                                                }
                                            ?>
                                                    </select>
                                                </div>
                                            </div>


                                      
                                        </div>
                                        <div class="modal-footer">
                                            <input type="hidden" name="section_id" id="section_id">
                                            <button type="submit" name="insert1" id="insert1" class="btn btn-success">EDIT CLASS</button>
                                            <button type="button" class="btn btn-warning" data-dismiss="modal">CANCEL
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="modal fade" id="modalLoad" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
                            <div class="modal-dialog modal-sm">
                                <div class="modal-content ">
                                    <form id="load" method="post">
                                        <div class="modal-header">
                                            <h2 class="modal-title"><b>LOAD CLASS</b></h2>
                                        </div>
                                        
                                        <div class="modal-body">
                                            
                                            
                                            <div class='col-sm-12'>
                                                <div class='form-group fg-line'>
                                                    <select class='selectpicker form-control' data-live-search="true" name='sections' id='sections' required>
                                                        <option value=""></option>
                                            <?php
                                                $query = mysqli_query($con, "SELECT * from schoolyear WHERE NOT sy_year='".$SY."'");
                                                while($row = mysqli_fetch_array($query)){
                                                    echo "
                                                        <option>".$row['sy_year']."</option>
                                                    ";
                                                }
                                            ?>
                                                    </select>
                                                </div>
                                            </div>


                                        </div>
                                        <div class="modal-footer">
                                            <input type="hidden" name="section_id" id="section_id">
                                            <button type="submit" name="insert1" id="insert1" class="btn btn-success">LOAD CLASS</button>
                                            <button type="button" class="btn btn-warning" data-dismiss="modal">CANCEL
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div id="hehe">
                    
                    <?php include_once 'ajaxJQuery/displaySection.php'; ?>
                </div>



                    <br/>
                    <br/>
                </div>
            </section>
        </section>
        
        <!-- FOOTER -->
        <?php include_once 'footer.php' ?>
        <!-- FOOTER -->

        <!-- Javascript Libraries -->
        <?php include_once 'scripts.php'; ?>
        <!-- Javascript Libraries -->

        <script type="text/javascript" src="js/Notification.js"></script>
        <script type="text/javascript">
            function show1(){
                var id = '10';
                var name = 'Grade 7';
                execute(id, name);

            }

            function show2()
            {
                var id = '11';
                var name = 'Grade 8';
                execute(id, name);
            }

            function show3()
            {
                var id = '12';
                var name = 'Grade 9';
                execute(id, name);
            }
            function show4()
            {
                var id = '13';
                var name = 'Grade 10';
                execute(id, name);
            }


            function execute(id, name)
            {
                var result = $('#year_lvl_title option[value!=""]').first().html();
                $("#year_lvl_title option[value!='"+result+"']").remove();
                $('#year_lvl_title').prepend('<option selected="" value="'+id+'">'+name+'</option>');
                $('#year_lvl_title').selectpicker('refresh');
                
                $('#modalColor').modal('show');
                


            }




            $(document).ready(function(){



                $('#modalColor').on('shown.bs.modal', function() {
                    $.ajax({
                        url: "ajaxJQuery/getListofTeacher.php",
                        dataType: "json",
                        success:function(data)
                        {
                            var length = data.length;

                            $("#section_adviser").empty();
                            
                            $("#section_adviser").append("<option value=''>ADVISER</option>");
                            if(length != 0)
                            {
                                for(var i = 0; i<length; i++)
                                {
                                    $('#section_adviser').append("<option value='"+data[i]['id']+"'>"+data[i]['name']+"</option>");
                                    $('#section_adviser').selectpicker('refresh');
                                }
                            }

                            else
                            {
                                $("#section_adviser").empty();
                                $("#section_adviser").append("<option value=''>ADVISER</option>");
                                $('#section_adviser').selectpicker('refresh');
                            }


                        }
                    }); 
                });
                
                $('[data-dismiss=modal]').on('click', function (e) {
                    $('#myForm')[0].reset();
                    $('#myForm1')[0].reset();
                });
                $('#globalSearch').keyup(function(){
                    var search = $('#globalSearch').val();
                        $.ajax({
                            url:"ajaxJQuery/searchSection.php",
                            method:"POST",
                            data:{search:search},
                            dataType:"text",
                            success:function(data)
                            {
                                $('#hehe').html(data);
                            }
                        });
                    
                });

                $('[data-dismiss=modal]').on('click', function (e) {
                    $('#myForm')[0].reset();
                })

                $('#sy').change(function(){
                    var sys = $(this).val();
                    $.ajax({
                        url:"ajaxJQuery/loadClass.php",
                        method:"POST",
                        data:{sys:sys},
                        success:function(data){
                            $('#sectionList').html(data);
                        }
                    });
                });

                $(document).on('click', '.delete_data', function(){
                    var tae = $(this).attr("id");
                    swal({   
                        title: "Are you sure?",   
                        text: "You will not be able to recover this data!",   
                        type: "warning",   
                        showCancelButton: true,   
                        confirmButtonText: "Yes, delete it!",
                    }).then(function(){
                        
                        $.ajax({
                            url:"ajaxJQuery/deleteSection.php",
                            method:"POST",
                            data:{tae:tae},
                            
                            success:function(data){
                                if(data != '0'){ 
                                    var message = "Successfully deleted section.";
                                    executeNotif(message,"success");
                                   $('#hehe').html(data);
                               }else
                               {
                                    var message = "Error on deleting section. Section has enrolled learners";
                                    executeNotif(message,"danger");
                               }
                              }


                        });
                    });
                    
                });
                $('#load').on("submit", function(event){
                    event.preventDefault();
                    if($('#sections').val() == '')  
                    {  
                         alert("Select school year");  
                    }  
                     
                    else  
                    {  
                         $.ajax({  
                              url:"ajaxJQuery/insertSection.php",  
                              method:"POST",  
                              data:$('#load').serialize(),  
                              
                              success:function(data){ 
                            
                                        var message = "Successfully exported sections from school year "+$('#sections').val()+"";
                                        executeNotif(message,"success");

                                        
                                        $('#modalLoad').modal('hide'); 
                                        $('#hehe').html(data);
                             
                               
                                    
                              }  

                         });  
                    }
                });



                $(document).on('click', '.edit_data', function(){
                    var section_id = $(this).attr("id");
                    $.ajax({
                        url:"ajaxJQuery/selectSection.php",
                        method:"POST",
                        data:{section_id:section_id},
                        dataType:"json",
                        success:function(data){
                            $('#section_title1').val(data.section_name);
                            $('#section_capacity1').val(data.section_capacity);
                            $('#section_maxGrade1').val(data.section_maxGrade);
                            $('#section_minGrade1').val(data.section_minGrade);
                             var tid = data.adviser_id;
                            var tname =data.Adviser;
                            filterTeacher(tid,tname);
                            $('#year_lvl_title1').val(data.GradeLevel);
                             $('#section_id').val(data.section_id);
                            $('#modalColor1').modal('show');  
                        }

                    });
                });

                function filterTeacher(tid,tname)
                {
                    
                    $("#section_adviser1 option[value='"+tid+"']").remove();
                    if(tname == null)
                    {
                        $('#section_adviser1').prepend('<option value="" selected>ADVISER</option>');
                    }else
                    {
                        $('#section_adviser1').prepend('<option value="'+tid+'">'+tname+'</option>');
                    }
                    $('#section_adviser1').selectpicker('refresh');
                }

                

                $(document).on('click', '.enrol_student', function(){
                    var section_id = $(this).attr("id");
                    $.ajax({
                        
                        success:function(data){
                            
                             $('#section_id1').val(data.section_id);
                            $('#modalColor2').modal('show');
                        }

                    });
                });

                $('#myForm').on("submit", function(event){
                    event.preventDefault();
                    if($('#section_title').val() == 'a')  
                    {  
                         alert("Title is required");  
                    }  
                    
                    
                    else if($('#year_lvl_title').val() == 'LEVEL')  
                    {  
                         alert("You must select year level");  
                    }  
                    else  
                    {  
                         $.ajax({  
                              url:"ajaxJQuery/insertSection.php",  
                              method:"POST",  
                              data:$('#myForm').serialize(),
                               
                              beforeSend:function(){  
                                   $('#insert1').val("Inserting..");   
                              },  
                              success:function(data){ 
                                    if(jQuery.trim(data) == "0"){
                                        var message = "Error on adding section";
                                        executeNotif(message,"danger");
                                   }
                                   else if(jQuery.trim(data) == "1")
                                   {
                                        var message = "Section already in list";
                                        executeNotif(message,"danger");
                                   }
                                   else
                                   {
                                        var message = "Successfully added section.";
                                        executeNotif(message,"success");
                                        $('#myForm')[0].reset();  
                                        $('#modalColor').modal('hide'); 
                                        $('#hehe').html(data);
                                       
                                   }
                                    
                              }  

                         });  
                    }
                });


                $('#myForm1').on("submit", function(event){
                    event.preventDefault();
                    if($('#section_title1').val() == '')  
                    {  
                         alert("Title is required");  
                    }  
                    
                    else if($('#section_adviser1').val() == 'ADVISER')  
                    {  
                         alert("You must select adviser");  
                    } 
                    else  
                    {  
                         $.ajax({  
                              url:"ajaxJQuery/updateSection.php",  
                              method:"POST",  
                              data:$('#myForm1').serialize(),  
                              beforeSend:function(){  
                                   $('#insert1').val("Inserting..");   
                              },  
                              success:function(data){ 
                                if(jQuery.trim(data) =="error" | jQuery.trim(data) == "error1")
                                {
                                    var message = "Error on editing section.";
                                    executeNotif(message,"danger");
                                }
                                else if(jQuery.trim(data)=="duplicate")
                                {
                                    var message = "Section already in list.";
                                    executeNotif(message,"danger");
                                }
                                else if(jQuery.trim(data)=="no changes")
                                {
                                    var message = "Nothing Changes";
                                    executeNotif(message,"warning");
                                    $('#modalColor1').modal('hide');
                                }
                                else
                                {
                                    var message = "Successfully edited section.";
                                    executeNotif(message,"success");
                                   $('#myForm1')[0].reset();
                                   $('#modalColor1').modal('hide');   
                                   $('#hehe').html(data);
                                }  
                              }  

                         });  
                    }
                });
            });



        </script>
    </body>
</html>