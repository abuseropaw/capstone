<?php
    include_once 'sessionTeacher.php';
    include_once 'dbconnect.php';

    $teacherID =mysqli_real_escape_string($con, $_SESSION['faculty_id']);


    $query = mysqli_query($con, "SELECT * from faculty_account where faculty_id='".$teacherID."'");
    if($row = mysqli_fetch_array($query)){
        $id = $row[0];
        $fname=$row[1];
        $mname=$row[2];
        $lname=$row[3];
        $phone=$row[4];
        $address=$row[5];
        $religion=$row[6];
        $major=$row[7];
        $minor=$row[8];
        $dob=$row[9];
        $gender=$row[10];
        $type=$row[11];
        $picture=$row[12];
        $pass=$row[13];
        $dateCreated=$row[14];
        $createdBy=$row[15];
    }
?>
<!DOCTYPE html>
    <!-- HEAD -->
    <?php include_once 'head.php'; ?> 
    <!-- HEAD   -->
    
    <body>
        <!-- HEADER -->
        <?php include_once 'header.php'; ?>
        <!-- HEADER -->

        <section id="main">
            <ol class="breadcrumb">
                <li><a href="teacherHome.php">Home</a></li>
                <li class="active">Profile</li>
            </ol>
            <?php
                $toggle = 'teacherProfile'; 
                include_once 'sidebarTeacher.php'; 
            ?>


            <section id="content">

                        <div class="container">

                            <div class="block-header">
                                <h1><i class="zmdi zmdi-accounts"></i> My Profile
                                </h1>

                                
                            </div>
                                <div class="row">

                                    <div class="col-sm-12">
                                        <div class='card'  id='teacherInfo'>
                                            
                                            <div class="card-body">
                                                <div class="card" id="profile-main">
                                                    <div class="pm-overview c-overflow">
                                                        
                                                        <div class="pmo-pic">
                                                            <div class="p-relative">
                                                                <?php
                                                                    if(empty($picture))
                                                                    {
                                                                        echo '
                                                                        <a href="#">
                                                                            <img class="img-responsive" src="img/default-profile.png" alt="">
                                                                        </a>';
                                                                        
                                                                    }
                                                                    else
                                                                    {
                                                                        echo '
                                                                        <a href="#">
                                                                            <img class="img-responsive" src="profile/'.$picture.'" alt="">
                                                                        </a>';
                                                                    }
                                                                ?>
                                                                
                                                            </div>


                                                                
                                                        </div>

                                                        <div class="pmo-block pmo-contact hidden-xs">
                                                            <h2 class="text-center"><?php echo $fname.' '.$mname.' '.$lname; ?></h2>
                                                            <?php
                                                            echo "<ul>
                                                                <li><i class='zmdi zmdi-phone'></i>".$phone."</li>
                                                                <li>
                                                                    <i class='zmdi zmdi-pin'></i>
                                                                    <address class='m-b-0 ng-binding'>
                                                                        ".$address."
                                                                    </address>
                                                                </li>
                                                            </ul>";
                                                            ?>
                                                        </div>

                                                    
                                                    </div>

                                                    <div class="pm-body clearfix">
                                                        <div role="tabpanel">
                                                            <ul class="tab-nav tn-justified" role="tablist" data-tab-color="green">
                                                                <li class="active"><a href="#acount" aria-controls="acount" role="tab"
                                                                  data-toggle="tab">Account</a></li>
                                                                <li><a href="#class" aria-controls="class" role="tab" data-toggle="tab">Logs</a></li>
                                                                
                                                               
                                                            </ul>
                                                            <div class="tab-content">
                                                                <div role="tabpanel" class="tab-pane active" id="acount">
                                                                    <div class="pmb-block">
                                                                        <div class="pmbb-header">
                                                                            <h2><i class="zmdi zmdi-account m-r-10"></i> Basic Information</h2>

                                                                            
                                                                        </div>
                                                                        <div class="pmbb-body p-l-30">
                                                                            <div class="pmbb-view">
                                                                                <?php
                                                                                echo "
                                                                                <dl class='dl-horizontal'>
                                                                                    <dt>First Name</dt>
                                                                                    <dd>".$fname."</dd>
                                                                                </dl>
                                                                                <dl class='dl-horizontal'>
                                                                                    <dt>Middle Name</dt>
                                                                                    <dd>".$mname."</dd>
                                                                                </dl>
                                                                                <dl class='dl-horizontal'>
                                                                                    <dt>Last Name</dt>
                                                                                    <dd>".$lname."</dd>
                                                                                </dl>
                                                                                <dl class='dl-horizontal'>
                                                                                    <dt>Gender</dt>
                                                                                    <dd>".$gender."</dd>
                                                                                </dl>
                                                                                  
                                                                                <dl class='dl-horizontal'>
                                                                                    <dt>Birthday</dt>
                                                                                    <dd>".$dob."</dd>
                                                                                </dl>
                                                                                <dl class='dl-horizontal'>
                                                                                    <dt>Position</dt>
                                                                                    <dd>".$type."</dd>
                                                                                </dl> 
                                                                                <dl class='dl-horizontal'>
                                                                                    <dt>Minor Subject</dt>
                                                                                    <dd>".$minor."</dd>
                                                                                </dl>
                                                                                <dl class='dl-horizontal'>
                                                                                    <dt>Major Subject</dt>
                                                                                    <dd>".$major."</dd>
                                                                                </dl>

                                                                                <dl class='dl-horizontal'>
                                                                                    <dt>Contant No.</dt>
                                                                                    <dd>".$phone."</dd>
                                                                                </dl>
                                                                                <dl class='dl-horizontal'>
                                                                                    <dt>Home Address</dt>
                                                                                    <dd>".$address."</dd>
                                                                                </dl>
                                                                                ";
                                                                                ?>
                                                                            </div>
                                                                            
                                                                            
                                                                        </div>
                                                                    </div>

                                                                    
                                                                </div>
                                                                <div role="tabpanel" class="tab-pane" id="class">
                                                                    <div class="pmb-block">
                                                                        <div class="pmbb-header">
                                                                            <h2><i class="zmdi zmdi-account m-r-10"></i> Logs</h2>

                                                                            <ul class="actions">
                                                                                <li class="dropdown">
                                                                                    <a href="#" data-toggle="dropdown">
                                                                                        <i class="zmdi zmdi-more-vert"></i>
                                                                                    </a>

                                                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                                                        <li>
                                                                                            <a data-ma-action="profile-edit" href="#">Edit</a>
                                                                                        </li>
                                                                                    </ul>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                        <div class="pmbb-body p-l-30">
                                                                            
                                                                        </div>
                                                                    </div>
                                                                    
                                                                </div>
                                                                
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            
                            </div>
                        
                            </div>
                        </div>
                    </section>
        </section>
        
        <!-- FOOTER -->
        <?php include_once 'footer.php' ?>
        <!-- FOOTER -->

        <!-- Javascript Libraries -->
        <?php include_once 'scripts.php'; ?>
        <!-- Javascript Libraries -->


    </body>
</html>