<?php
session_start();
    include_once 'dbconnect.php';
    if(!empty($_SESSION['faculty_id']))
    {
        header("Location: adminHome.php");
    }
    
    
    $errormsg = false;

    $countAdmin = mysqli_num_rows(mysqli_query($con, "SELECT * from faculty_account where faculty_type='Administrator'"));
    if(isset($_POST['login'])){
        $username = mysqli_real_escape_string($con, $_POST['username']);
        $password = mysqli_real_escape_string($con, $_POST['password']);   
        

        $query = "CALL loginTeacher('".$username."','".$password."')";  
        $result = mysqli_query($con, $query);
        if($row = mysqli_fetch_array($result)){
            $type = $row['faculty_type'];
            if($type == 'Administrator'){
                $_SESSION['faculty_type'] = $type;
                $_SESSION['faculty_id'] = $row['faculty_id'];
                $_SESSION['Name'] = $row['Name'];
                header("Location: adminHome.php");
            }else if($type == 'Teacher'){
                $_SESSION['faculty_type'] = $type;
                $_SESSION['faculty_id'] = $row['faculty_id'];
                $_SESSION['Name'] = $row['Name'];
                header("Location: teacherHome.php");
            }
            else if($type == 'Head Teacher'){
                $_SESSION['faculty_type'] = $type;
                $_SESSION['faculty_id'] = $row['faculty_id'];
                $_SESSION['Name'] = $row['Name'];
                header("Location: registrarHome.php");
            }
            else if($type == 'Registrar')
            {
                 $_SESSION['faculty_type'] = $type;
                $_SESSION['faculty_id'] = $row['faculty_id'];
                $_SESSION['Name'] = $row['Name'];
                header("Location: registrarHome.php");
            }


        }else{
            
            
        }

    }

    
?>

<!DOCTYPE html>
<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1"><link rel="shortcut icon" type="image/x-icon" href="img/lnhs.ico" alt="logo" />
        <title>LNHS | HOME</title>

        <!-- Vendor CSS -->
        <link href="vendors/bower_components/animate.css/animate.min.css" rel="stylesheet">
         <link href="vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css" rel="stylesheet">
        <link href="vendors/bower_components/sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
        <!-- CSS -->
        <link href="css/app_1.min.css" rel="stylesheet">
        <link href="css/app_2.min.css" rel="stylesheet">
        <style>
            #bg {
              position: fixed; 
              top: -50%; 
              left: -50%; 
              width: 200%; 
              height: 200%;
              background-size: cover;

            }
            #bg img {
              position: absolute; 
              top: 0; 
              left: 0; 
              right: 0; 
              bottom: 0; 
              margin: auto; 
              min-width: 50%;
              min-height: 50%;
              max-width: 50%;
              max-height: 50%;
            }
        </style>
</head>

<body>
   
    <div id="bg">
        <img src="assets/css/images/asdsdasdsadas.jpg" alt="">
    </div>
    
   <div class="login-content">
        <?php 
        if($countAdmin > 0)
        {
            echo '
        <div class="lc-block lc-block-alt toggled" id="l-lockscreen">
            <div class="lcb-form" style="filter:alpha(opacity=75); opacity:1;">
                <a href="index.php"><img class="lcb-user pull-right" src="img/lnhs.ico" alt=""></a>
                <h3 class="c-black"><b>Teacher Account</b></h3>
                <p>Online Portal</p>
                <form id="myForm" method="POST" action="teacherLogin.php">
                    <div class="input-group m-b-20">
                        <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
                        <div class="fg-line">
                            <input type="text" class="form-control input-lg" name="username" id="username" placeholder="Username" required>
                        </div>
                    </div>

                    <div class="input-group m-b-20">
                        <span class="input-group-addon"><i class="zmdi zmdi-key"></i></span>
                        <div class="fg-line">
                            <input type="password" class="form-control input-lg" name="password" name="password" placeholder="Password" required>
                        </div>
                    </div>

                    

                    
                    <button type="submit" class="btn btn-login bgm-green btn-float" name="login" id="login"><i class="zmdi zmdi-arrow-forward"></i></button>
                </form>
                <span class="text-danger" id="result"><?php if (isset($checkmsg)) { echo $checkmsg; } ?></span>
                <span class="text-danger" id="result"><?php if ($errormsg)){ echo $errormsg1; } ?></span>
            </div>

        </div>
        ';
        }
        else
        {
        echo '
        <div class="lc-block lc-block-alt toggled" id="l-lockscreen">
            <div class="lcb-form" style="filter:alpha(opacity=75); opacity:0.90;">
                <img class="lcb-user pull-right" src="img/lnhs.ico" alt="">
                <h4 class="c-black"><b>Create Admin Account</b></h4>
                
                <form id="addAdmin" method="POST">
                    <div class="input-group m-b-40">
                        <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
                        <div class="fg-line">
                            <input type="text" id="fname" name="fname" class="form-control" placeholder="First Name" required>
                        </div>
                    </div>
                    <div class="input-group m-b-40">
                        <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
                        <div class="fg-line">
                            <input type="text" id="mname" name="mname" class="form-control" placeholder="Middle Name">
                        </div>
                    </div>
                    <div class="input-group m-b-40">
                        <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
                        <div class="fg-line">
                            <input type="text" id="lname" name="lname" class="form-control" placeholder="Last Name" required>
                        </div>
                    </div>
                    <div class="input-group m-b-70">
                        <span class="input-group-addon"><i class="zmdi zmdi-key"></i></span>
                        <div class="fg-line">
                            <input type="password" id="password" name="password" class="form-control" placeholder="Password" required>
                        </div>
                    </div>
                    <div class="form-footer">
                        <button type="submit" name="createAdmin" class="btn btn-login btn-success btn-float"><i class="zmdi zmdi-arrow-forward"></i></button>
                    </div>
                </form>
                
            </div>
        </div>';
        }
         ?>
    </div>
    </div>
    </div>
</div>

    <!-- Javascript Libraries -->
    <script src="vendors/bower_components/jquery/dist/jquery.min.js"></script>
    <script src="vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <script src="vendors/bower_components/Waves/dist/waves.min.js"></script>

    <!-- Placeholder for IE9 -->
    <!--[if IE 9 ]>
        <script src="vendors/bower_components/jquery-placeholder/jquery.placeholder.min.js"></script>
    <![endif]-->
   <script src="vendors/bower_components/sweetalert2/dist/sweetalert2.min.js"></script>

    <script src="js/app.min.js"></script>
    <script>
        window.onload = function() { document.body.className = ''; }
        window.ontouchmove = function() { return false; }
        window.onorientationchange = function() { document.body.scrollTop = 0; }

        $(function(){

            $('#addAdmin').on('submit', function(e){
                e.preventDefault();

                $.ajax({
                    url:"ajaxJQuery/createAdmin.php",
                    data:$(this).serialize(),
                    method:"POST",
                    success:function(data){
                        if(data == '0')
                        {
                            swal("Oops!", "Error on adding account", "error");
                        }
                        else
                        {
                           swal("Hello Administrator", "You can login your account using "+data+" as Username", "success");
                           $.ajax({
                                url:"ajaxJQuery/createAdmin1.php",
                                data:$(this).serialize(),
                                method:"POST",
                                beforeSend: function()
                                {
                                    setTimeout(function(){// wait for 5 secs(2)
                                       location.reload(); // then reload the page.(3)
                                   }, 4500);
                                },
                                success:function(data){
                                    
                                }
                            });
                           
                        }   

                    }
                });

            });
        });
    </script>
    
</body>
</html>
