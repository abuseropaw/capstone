<div role="tabpanel" class="tab-pane active" id="acount">

<!-- PERSONAL INFORMATION --><!-- PERSONAL INFORMATION --><!-- PERSONAL INFORMATION --><!-- PERSONAL INFORMATION -->
    <div class="pmb-block">
        <div class="pmbb-header">
            <h2><i class="zmdi zmdi-account m-r-10"></i><b> Personal Information</b></h2>

            <ul class="actions">
                <li class="dropdown">
                    <a href="#" data-toggle="dropdown">
                        <i class="zmdi zmdi-more-vert"></i>
                    </a>

                    <ul class="dropdown-menu dropdown-menu-right">
                        <li>
                            <a data-ma-action="profile-edit" href="#">Edit</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        <div class="pmbb-body p-l-30">
            <div class="pmbb-view">
                <?php
                echo "
                
                <dl class='dl-horizontal'>
                    <dt>LRN</dt>
                    <dd>".$lrn."</dd>
                </dl>
                <dl class='dl-horizontal'>
                    <dt>Name</dt>
                    <dd>".$name."</dd>
                </dl>
                <dl class='dl-horizontal'>
                    <dt>Gender</dt>
                    <dd>".$sex."</dd>
                </dl>
                  
                <dl class='dl-horizontal'>
                    <dt>Birthday</dt>
                    <dd>".$BirthDate."</dd>
                </dl>
                <dl class='dl-horizontal'>
                    <dt>Age</dt>
                    <dd>".$age."</dd>
                </dl> 
                <dl class='dl-horizontal'>
                    <dt>Birth Place</dt>
                    <dd>".$birthPlace."</dd>
                </dl>
                <dl class='dl-horizontal'>
                    <dt>Mother Tongue</dt>
                    <dd>".$motherTongue."</dd>
                </dl>
                <dl class='dl-horizontal'>
                    <dt>IP</dt>
                    <dd>".$IP."</dd>
                </dl>
                <dl class='dl-horizontal'>
                    <dt>Religion</dt>
                    <dd>".$Religion."</dd>
                </dl>
                ";
                ?>
            </div>
            <form method="POST" id="updateS">
                <div class="pmbb-edit">
                   
                    <dl class="dl-horizontal">
                        <dt class="p-t-10">First Name</dt>
                        <dd>
                            <div class="fg-line">
                                <input type="text" name='fname' id='fname' class="form-control"
                                       value="<?php echo $fname ?>">
                                        <input type="hidden" name='studentID' id='studentID' class="form-control"
                                       value="<?php echo $studentID ?>">
                            </div>

                        </dd>
                    </dl>

                    <dl class="dl-horizontal">
                        <dt class="p-t-10">Middle Name</dt>
                        <dd>
                            <div class="fg-line">
                                <input type="text" id='mname' name='mname' class="form-control"
                                       value="<?php echo $mname ?>">
                            </div>

                        </dd>
                    </dl>

                    <dl class="dl-horizontal">
                        <dt class="p-t-10">Last Name</dt>
                        <dd>
                            <div class="fg-line">
                                <input type="text" id='lname' name='lname' class="form-control"
                                       value="<?php echo $lname ?>">
                            </div>

                        </dd>
                    </dl>
                    <dl class="dl-horizontal">
                        <dt class="p-t-10">Gender</dt>
                        <dd>
                            <div class="fg-line">   
                                <select class="selectpicker form-control" id='sex' name='sex'>
                                    <option><?php echo $sex ?></option>
                                    <option>Male</option>
                                    <option>Female</option>
                                </select>
                            </div>
                        </dd>
                    </dl>
                    <dl class="dl-horizontal">
                        <dt class="p-t-10">Birthday</dt>
                        <dd>
                            <div class="dtp-container dropdown fg-line">
                                <input type='text' class="form-control" id="dob" name="dob" value="<?php echo $BirthDate ?>">
                            </div>
                        </dd>
                    </dl>
                    <dl class="dl-horizontal">
                        <dt class="p-t-10">Age</dt>
                        <dd>
                            <div class="fg-line">
                                <input type="text" name='age' id='age' class="form-control"
                                       value="<?php echo $age ?> ">
                                
                            </div>

                        </dd>
                    </dl>

                    <dl class="dl-horizontal">
                        <dt class="p-t-10">Monther Tongue</dt>
                        <dd>
                            <div class="fg-line">
                                <select class="selectpicker form-control" id='motherTongue' name='motherTongue'>
                                    <option><?php echo $motherTongue ?></option>
                                    <option>Cebuano</option>
                                    <option>Filipino</option>
                                    <option>Sinugbuanong Binisaya</option>
                                </select>
                            </div>

                        </dd>
                    </dl>

                    <dl class="dl-horizontal">
                        <dt class="p-t-10">Ethnic Group</dt>
                        <dd>
                            <div class="fg-line">
                                <select class="selectpicker form-control" id='ip' name='ip'>
                                    <option><?php echo $IP ?></option>
                                    <option>Cebuano</option>
                                    <option>Filipino</option>
                                    <option>Sinugbuanong Binisaya</option>
                                </select>
                            </div>
                        </dd>
                    </dl>
                    <dl class="dl-horizontal">
                        <dt class="p-t-10">Religion</dt>
                        <dd>
                            <div class="fg-line">
                                <select class="selectpicker form-control" id='religion' name='religion'>
                                    <option><?php echo $Religion ?></option>
                                    <option>Christianity</option>
                                    <option>Muslim</option>
                                </select>
                            </div>
                        </dd>
                    </dl>

                    <div class="m-t-30">

                        <button type="submit" class="btn btn-success btn-lg">Save Changes</button>
                        <button data-ma-action="profile-edit-cancel" class="btn btn-warning btn-lg">Cancel</button>
                    </div>
                    
                </div>
            </form>                                                              
        </div>
    </div>
<!-- PERSONAL INFORMATION --><!-- PERSONAL INFORMATION --><!-- PERSONAL INFORMATION --><!-- PERSONAL INFORMATION -->


<!-- ADDRESS INFORMATION --><!-- ADDRESS INFORMATION --><!-- ADDRESS INFORMATION --><!-- ADDRESS INFORMATION --><!-- ADDRESS INFORMATION -->
    <div class="pmb-block">
        <div class="pmbb-header">
            <h2><i class="zmdi zmdi-phone m-r-10"></i><b> Address Information</b></h2>

            <ul class="actions">
                <li class="dropdown">
                    <a href="#" data-toggle="dropdown">
                        <i class="zmdi zmdi-more-vert"></i>
                    </a>

                    <ul class="dropdown-menu dropdown-menu-right">
                        <li>
                            <a data-ma-action="profile-edit" href="#">Edit</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        <div class="pmbb-body p-l-30">
            <div class="pmbb-view">
                <dl class="dl-horizontal">
                    <dt>House # /Street/Sitio</dt>
                    <dd><?php echo $HSSP; ?></dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>Barangay</dt>
                    <dd><?php echo $Barangay; ?></dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>Municipality/City</dt>
                    <dd><?php echo $Municipality; ?></dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>Province</dt>
                    <dd><?php echo $Province; ?></dd>
                </dl>
                
            </div>
            <form id="updateA" method="post">
                <div class="pmbb-edit">
                    
                    <dl class="dl-horizontal">
                        <dt class="p-t-10">House # /Street/Sitio</dt>
                        <dd>
                            <div class="fg-line">
                                <input type="hidden" name="studentID" id="studentID" value="<?php echo $studentID; ?>">
                                <input type="text" id='hssp' name ='hssp' class="form-control" value="<?php echo $HSSP ?>">
                            </div>
                        </dd>
                    </dl>
                    <dl class="dl-horizontal">
                        <dt class="p-t-10">Barangay</dt>
                        <dd>
                            <div class="fg-line">
                                <input type="text" id='barangay' name ='barangay' class="form-control" value="<?php echo $Barangay ?>">
                            </div>
                        </dd>
                    </dl>
                    <dl class="dl-horizontal">
                        <dt class="p-t-10">Municipality/City</dt>
                        <dd>
                            <div class="fg-line">
                                <select class="selectpicker form-control" data-live-search="true" name='municipality' id='municipality'>
                                    <option><?php echo $Municipality ?></option>
                                    <option>Lugait</option>
                                    <option>Malaybalay</option>
                                    <option>Valencia</option>
                                    <option>Baungon</option>
                                    <option>Cabanglasan</option>
                                    <option>Damulog</option>
                                    <option>Dangcagan</option>
                                    <option>Don Carlos</option>
                                    <option>Impasug-ong</option>
                                    <option>Kadingilan</option>
                                    <option>Kibawe</option>
                                    <option>Kitaotao</option>
                                    <option>Lantapan</option>
                                    <option>Malitbog</option>
                                    <option>Manolo Fortich</option>
                                    <option>Maramag</option>
                                    <option>Pangantucan</option>
                                    <option>Quezon</option>
                                    <option>San Fernando</option>
                                    <option>Sumilao</option>
                                    <option>Talakag</option>
                                    <option>Catarman</option>
                                    <option>Guinsiliban</option>
                                    <option>Mahinog</option>
                                    <option>Mambajao</option>
                                    <option>Sagay</option>
                                    <option>Bacolod</option>
                                    <option>Baloi</option>
                                    <option>Baroy</option>
                                    <option>Kapatagan</option>
                                    <option>Kauswagan</option>
                                    <option>Kolambugan</option>
                                    <option>Lala</option>
                                    <option>Linamon</option>
                                    <option>Magsaysay</option>
                                    <option>Maigo</option>
                                    <option>Matungao</option>
                                    <option>Munai</option>
                                    <option>Nunungan</option>
                                    <option>Pantao Ragat</option>
                                    <option>Pantar</option>
                                    <option>Poona Piagapo</option>
                                    <option>Salvador</option>
                                    <option>Sapad</option>
                                    <option>Tagoloan</option>
                                    <option>Tangkal</option>
                                    <option>Tubod</option>
                                    <option>Iligan</option>
                                    <option>Oroquieta</option>
                                    <option>Ozamiz</option>
                                    <option>Tangub</option>
                                    <option>Aloran</option>
                                    <option>Baliangao</option>
                                    <option>Bonifacio</option>
                                    <option>Calamba</option>
                                    <option>Clarin</option>
                                    <option>Concepcion</option>
                                    <option>Don Victoriano</option>
                                    <option>Jimenez</option>
                                    <option>Lopez Jaena</option>
                                    <option>Panaon</option>
                                    <option>Plaridel</option>
                                    <option>Sapang Dalaga</option>
                                    <option>Sinacaban</option>
                                    <option>Tudela</option>
                                    <option>El Salvador</option>
                                    <option>Gingoog</option>
                                    <option>Alubijid</option>
                                    <option>Balingasag</option>
                                    <option>Baligoan</option>
                                    <option>Claveria</option>
                                    <option>Gitagum</option>
                                    <option>Initao</option>
                                    <option>Jasaan</option>
                                    <option>Kinoguitan</option>
                                    <option>Lagonglong</option>
                                    <option>Laguindingan</option>
                                    
                                    <option>Libertad</option>
                                    <option>Magsaysay(Linugos)</option>
                                    <option>Manticao</option>
                                    <option>Medina</option>
                                    <option>Naawan</option>
                                    <option>Opol</option>
                                    <option>Salay</option>
                                    <option>Sugbongcogon</option>
                                    <option>Tagoloan</option>
                                    <option>Talisayan</option>
                                    <option>Villanueva</option>
                                    <option>Cagayan de Oro</option>

                                </select>
                            </div>
                        </dd>
                    </dl>
                    <dl class="dl-horizontal">
                        <dt class="p-t-10">Province</dt>
                        <dd>
                            <div class="fg-line">
                                <select class="selectpicker form-control" data-live-search="true" name='province' id='province'>
                                    <option><?php echo $Province; ?></option>
                                    <option>Misamis Oriental</option>
                                    <option>Province</option>
                                    <option>Bukidnon</option>
                                    <option>Camiguin</option>
                                    <option>Lanao Del Norte</option>
                                   
                                    <option>Misamis Occidental</option>

                                </select>
                                
                            </div>
                        </dd>
                    </dl>
                    <div class="m-t-30">
                        <button type="submit" name="updateAddress" id="updateAddress" class="btn btn-success btn-lg">Save Changes</button>
                        <button data-ma-action="profile-edit-cancel" id="closebutton" class="btn btn-warning btn-lg">Cancel</button>
                    </div>
                    
                </div>
            </form>                                                                                                                             
        </div>
    </div>
<!-- ADDRESS INFORMATION --><!-- ADDRESS INFORMATION --><!-- ADDRESS INFORMATION --><!-- ADDRESS INFORMATION -->

<!-- Family Background --><!-- Family Background --><!-- Family Background --><!-- Family Background --><!-- Family Background -->
    <div class="pmb-block">
        <div class="pmbb-header">
            <h2><i class="zmdi zmdi-phone m-r-10"></i><b> Parent & Guardian Information</b></h2>

            <ul class="actions">
                <li class="dropdown">
                    <a href="#" data-toggle="dropdown">
                        <i class="zmdi zmdi-more-vert"></i>
                    </a>

                    <ul class="dropdown-menu dropdown-menu-right">
                        <li>
                            <a data-ma-action="profile-edit" href="#">Edit</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        <div class="pmbb-body p-l-30">
            <div class="pmbb-view">
                <dl class="dl-horizontal">
                    <dt>Father's Name</dt>
                    <dd><?php echo $Father; ?></dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>Mother's Name</dt>
                    <dd><?php echo $Mother; ?></dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>Guardian's Name</dt>
                    <dd><?php echo $GuardianName; ?></dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>Contact No.</dt>
                    <dd><?php echo $GContact.'/'.$PContact; ?></dd>
                </dl>
                
            </div>
            <form method="POST" id="updateP">
                <div class="pmbb-edit">
                    
                    <dl class="dl-horizontal">
                        <dt class="p-t-10">Father's First Name</dt>
                        <dd>
                            <div class="fg-line">
                                <input type="hidden" name="studentID" id="studentID" value="<?php echo $studentID; ?>">
                                <input type="text" id='ffname' name ='ffname' class="form-control"
                                       value="<?php echo $ffname ?>">
                            </div>
                        </dd>
                        <dt class="p-t-10">Father's Middle Name</dt>
                        <dd>
                            <div class="fg-line">
                                <input type="text" id='fmname' name ='fmname' class="form-control"
                                       value="<?php echo $fmname ?>">
                            </div>
                        </dd>
                        <dt class="p-t-10">Father's Last Name</dt>
                        <dd>
                            <div class="fg-line">
                                <input type="text" id='flname' name ='flname' class="form-control"
                                       value="<?php echo $flname ?>">
                            </div>
                        </dd>
                    </dl>
                    <dl class="dl-horizontal">
                        <dt class="p-t-10">Mother's First Name</dt>
                        <dd>
                            <div class="fg-line">
                                <input type="text" id='mfname' name ='mfname' class="form-control"
                                       value="<?php echo $mfname ?>">
                            </div>
                        </dd>
                        <dt class="p-t-10">Mother's Middle Name</dt>
                        <dd>
                            <div class="fg-line">
                                <input type="text" id='mmname' name ='mmname' class="form-control"
                                       value="<?php echo $mmname ?>">
                            </div>
                        </dd>
                        <dt class="p-t-10">Mother's Last Name</dt>
                        <dd>
                            <div class="fg-line">
                                <input type="text" id='mlname' name ='mlname' class="form-control"
                                       value="<?php echo $mlname ?>">
                            </div>
                        </dd>
                    </dl>


                    <dl class="dl-horizontal">
                        <dt class="p-t-10">Parent's Contact No.</dt>
                        <dd>
                            <div class="fg-line">
                                <input type="text" id='pphone' name ='pphone' class="form-control"
                                       value="<?php echo $PContact1 = $PContact ?>">
                            </div>
                        </dd>
                    </dl>


                    <dl class="dl-horizontal">
                        <dt class="p-t-10">Guardian Name</dt>
                        <dd>
                            <div class="fg-line">
                                <input type="text" id='gname' name ='gname' class="form-control"
                                       value="<?php echo $GuardianName1 = $GuardianName ?>">
                            </div>
                        </dd>
                        <dt class="p-t-10">Relationship</dt>
                        <dd>
                            <div class="fg-line">
                                <select class="selectpicker form-control" id='relationship' name='relationship'>
                                    <option><?php echo $Relationship1 = $Relationship ?></option>
                                    <option>Aunt</option>
                                    <option>Uncle</option>
                                </select>
                            </div>
                        </dd>

                        <dt class="p-t-10">Parent's Contact No.</dt>
                        <dd>
                            <div class="fg-line">
                                <input type="text" id='gphone' name ='gphone' class="form-control"
                                       value="<?php echo $GContact1 = $GContact ?>">
                            </div>
                        </dd>
                        
                    </dl>




                    <div class="m-t-30">
                        <button type="submit" class="btn btn-success btn-lg">Save Changes</button>
                        <button data-ma-action="profile-edit-cancel" class="btn btn-warning btn-lg">Cancel</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
<!-- Family Background --><!-- Family Background --><!-- Family Background --><!-- Family Background --><!-- Family Background -->
</div>