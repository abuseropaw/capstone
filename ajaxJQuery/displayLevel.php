<?php
    $output ='';
$output.= '<div class="row m-t-25">';

      $query = mysqli_query($con, "CALL selectYearLevelDetails()");
    while($row = mysqli_fetch_array($query)){
        $output .= '
            <div class="col-sm-3">
                <div class="row-sm-3">
                <div class="card pt-inner">';
                switch ($row[1]) {
                    case 'Grade 7':
                        $output .= '<div class="pti-header bgm-green">';
                        $output .= '
                        <h2>'.$row[1].'

                        </h2>
                        <div class="ptih-title">Freshmen</div>
                        </div>
                        ';
                        break;
                    case 'Grade 8':
                        $output .= '<div class="pti-header bgm-yellow">';
                        $output .= '
                        <h2>'.$row[1].'

                        </h2>
                        <div class="ptih-title">Sophomore</div>
                        </div>
                        ';
                        break;
                    case 'Grade 9':
                        $output .= '<div class="pti-header bgm-red">';
                        $output .= '
                        <h2>'.$row[1].'

                        </h2>
                        <div class="ptih-title">Junior</div>
                        </div>
                        ';
                        break;
                    case 'Grade 10':
                        $output .= '<div class="pti-header bgm-blue">';
                        $output .= '
                        <h2>'.$row[1].'

                        </h2>
                        <div class="ptih-title">Senior</div>
                        </div>
                        ';
                        break;

                    default:
                        $output .= '<div class="pti-header bgm-blue">';
                        $output .= '
                        <h2>'.$row[1].'

                        </h2>
                        <div class="ptih-title">'.$row[1].'</div>
                        </div>
                        <div class="pti-footer">
                            <a href="#" id="'.$row[0].'" class="bgm-blue edit_level"><i class="zmdi zmdi-edit"></i></a>
                            <a href="#" id="'.$row[0].'" class="bgm-blue delete_level"><i class="zmdi zmdi-delete"></i></a>
                        </div>';
                        break;
                }
                $output .= '
                </div>
                </div>
            </div>
        ';
    }
    $output.=
    '
    </div>';
    echo $output;
?>
