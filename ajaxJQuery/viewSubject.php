<?php
	session_start();
	include_once 'dbconnect.php';
	$output = '';
	$query = mysqli_query($con, "CALL viewSubjectDetails('".$_POST['subj_id']."')");

	$output .= '
	  <div class="table-responsive">  
	       <table class="table table-bordered">
	       <tbody>';
	  while($row = mysqli_fetch_array($query))
	  {
	       $output .= '
	            <tr>
	                 <td width="20%"><label>ID</label></td>
	                 <td width="80%">'.$row[0].'</td>
	            </tr>
	            <tr>
	                 <td width="20%"><label>TITLE</label></td>
	                 <td width="80%">'.$row[1].'</td>
	            </tr>
	            <tr>
	                 <td width="20%"><label>DESCRIPTION</label></td>
	                 <td width="80%">'.$row[2].'</td>
	            </tr>
	            <tr>
	                 <td width="20%"><label>UNIT</label></td>
	                 <td width="80%">'.$row[3].'</td>
	            </tr>
	            <tr>
	                 <td width="20%"><label>HOURS/WEEK</label></td>
	                 <td width="80%">'.$row[4].'</td>
	            </tr>
	            <tr>
	                 <td width="20%"><label>YEAR LEVEL</label></td>
	                 <td width="80%">'.$row[5].'</td>
	            </tr>
	            <tr>
	                 <td width="20%"><label>DATE CREATED</label></td>
	                 <td width="80%">'.$row[6].'</td>
	            </tr>
	            <tr>
	                 <td width="20%"><label>CREATED BY</label></td>
	                 <td width="80%">'.$row[7].'</td>
	            </tr>
	       ';
	  }
	  $output .= '
	       </tbody></table>
	  </div>
	  ';
	  echo $output;
?>
