<?php
    if($level == "Overall")
    {
        if($countQuarter == 4)
        {
            $getQuarters = mysqli_query($con,"SELECT * from yearlevel");

            while ($row = mysqli_fetch_array($getQuarters)) {
                $getaveragedetails = mysqli_query($con, "SELECT * from averagedetails where sy_id='".$SY."' and gradelevel='".$row[1]."' order by average desc,Student ASC limit 20");
                echo '

                    <div class="col-sm-3">
                        <div class="card">     
                            <div class="card-header">
                                <h2>'.$row[1].'<small>Top 20 Learner</small></h2>
                                <div class="actions">
                                    <button class="btn btn-danger" onclick=window.open("rankingReport.php?type=average&syID='.$SY.'&level='.$row[0].'")><i class="zmdi zmdi-collection-pdf"></i><b> PRINT</b></button>
                                </div>
                            </div>        
                            <div class="card-body card-padding lcb-form">
                                <div>
                                   
                                       

                                            <ol>
                ';
                while ($item = mysqli_fetch_array($getaveragedetails)) {
                echo '
                                    
                                        <li>'.$item['Student'].' - <b>'.$item['average'].'</b></li>
                                   
                                ';
                }
                echo '
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                ';

            }
        }
    }
    else
    {
        $getQuarters = mysqli_query($con,"SELECT * from quarters where sy_id='".$SY."'");

        while ($row = mysqli_fetch_array($getQuarters)) {
            $getaverageperquarterdetails = mysqli_query($con, "SELECT * from averageperquarterdetails where quarters_id='".$row[0]."' and gradelevel='".$level."' order by average desc,Name ASC limit 20");
            echo '

                <div class="col-sm-3">
                    <div class="card">     
                        <div class="card-header">
                            <h2>'.$row[1].'<small>Top 20 Learner</small></h2>
                            <div class="actions">
                                <button class="btn btn-danger" onclick=window.open("rankingReport.php?type=perQuarter&qID='.$row[0].'&level='.$getLevel[0].'")><i class="zmdi zmdi-collection-pdf"></i><b> PRINT</b></button>
                            </div>
                        </div>        
                        <div class="card-body card-padding lcb-form">
                            <div>
                               
                                   

                                        <ol>
            ';
            while ($item = mysqli_fetch_array($getaverageperquarterdetails)) {
            echo '
                                
                                    <li>'.$item['Name'].' - <b>'.$item[0].'</b></li>
                               
                            ';
            }
            echo '
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            ';

        }
    }
?>