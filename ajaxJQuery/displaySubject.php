<?php
$output='';

	$output.='
	<div class="table-responsive">
        <table class="table table-bordered table-nowrap">
            <thead>
                <th><b>Title</b></th>
                <th><b>Description</b></th>
                <th><b>Unit</b></th>
                <th><b>Hrs/Week</b></th>
                <th><b>Level</b></th>
                <th width="20%"><b>Action</b></th>
            </thead>
            <tbody>';

                    
                    while($row = mysqli_fetch_array($query)){
                        $temp = $row[0];
                        $output.= "
                        <tr>
                            <td>".$row[1]."</td>
                            <td>".$row[2]."</td>
                            <td>".$row[3]."</td>
                            <td>".$row[4]."</td>
                            <td>".$row[5]."</td>
                            <td>
								<div class='btn-demo'>
									<button type='submit' name='details' id='".$temp."' data-toggle='tooltip' data-placement='top' title='Details' class='btn btn-default btn-sm details'><i class='zmdi zmdi-info'></i></button>
                                    <button type='submit' name='edit' id='".$temp."' data-toggle='tooltip' data-placement='top' title='Edit' class='btn btn-default btn-sm edit_subject'><i class='zmdi zmdi-edit'></i></button>

                                    <button type='submit' name='delete' id='".$temp."' data-toggle='tooltip' data-placement='top' title='Delete' class='btn btn-default btn-sm delete_subject'><i class='zmdi zmdi-delete'></i></button>
								</div>
                            </td>
                        </tr>
                        ";
                    }
               $output.='
            </tbody>
        </table>
    </div>  ';

    echo $output;
?>
