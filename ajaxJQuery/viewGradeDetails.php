<?php
	session_start();
	include_once 'dbconnect.php';
	$output = '';	
	

	$querycs = mysqli_query($con, "SELECT * from controldetails where control_id='".$_POST['s_id']."'");

    while ($row4 = mysqli_fetch_array($querycs)) {
        $controlID = $row4[0];
        $sectionID = $row4[8];
        $sy_year = $row4[10];

    $output.= '
        
            <div class=" card table-responsive">
                <table class="table table-bordered">
                   
                <thead>
                    <tr>

                    <th rowspan="2" class="text-center" width="50%"><b>Subject Title</b></th>
                    <th colspan="5" class="text-center" width="70%"><b>'.$row4[11].' - Grade</b></th>

                    </tr>

                    <tr>
                    <th width="15%" class="text-center"><small><small><small>1st</small></small></small></th>
                    <th width="15%" class="text-center"><small><small><small>2nd</small></small></small></th>
                    <th width="15%" class="text-center"><small><small><small>3rd</small></small></small></th>
                    <th width="15%" class="text-center"><small><small><small>4th</small></small></small></th>
                    <th width="15%" class="text-center"><small><small><small><b>Final</b></small></small></small></th>

                    </tr>
                </thead>
                    <tbody>';
                        
                            $getSO = mysqli_query($con, "SELECT * from subjectofferingdetails where section_id='".$sectionID."'");
                            $countgetSO = mysqli_num_rows(mysqli_query($con, "SELECT * from subjectofferingdetails where section_id='".$sectionID."'"));
                            if($countgetSO > 0){
                            while ($row1 = mysqli_fetch_array($getSO)) {
                                $soID = $row1[0];
                                $output.= '
                                    <tr>
                                    <td>'.$row1[6].'</td>';
                                     $getQuarters = mysqli_query($con, "SELECT * from quarters where sy_id='".$sy_year."'");
                                    $countQ = mysqli_num_rows($getQuarters);

                                    if(mysqli_num_rows($getQuarters) != 0)
                                    {
                                        while ($item = mysqli_fetch_array($getQuarters)) {
                                   
                                            if($row2 = mysqli_fetch_row(mysqli_query($con, "SELECT * from grade where control_id='".$controlID."' and so_id='".$soID."' and quarters_id='".$item['quarters_id']."'"))){
                                                if($row2[0] < 75)
                                                {
                                                    $output.= "<td class='c-red'>".$row2[0]."</td>";
                                                }
                                                else
                                                {
                                                    $output.= "<td class='c-green'>".$row2[0]."</td>";
                                                    
                                                }
                                            }
                                            else
                                            {
                                                $output.= "<td></td>";
                                            }

                                        }
                                    }
                                    else{
                                        $output.= "<td></td><td></td><td></td><td></td><td></td>";
                                    }

                                    if($countQ == 1){
                                    $output .='<td></td><td></td><td></td>';  
                                    }else if($countQ == 2){
                                        $output.= '<td></td><td></td>';

                                    }else if($countQ == 3){
                                        $output.= '<td></td>';

                                    }else if($countQ == 4){

                                    }

                                    $queryfinal = mysqli_query($con, "SELECT round(average, 3) from soaveragegrade where so_id = '".$soID."' and control_id='".$controlID."'");
                                    if ($row3 = mysqli_fetch_row($queryfinal)) {
                                        if($row3[0] < 75)
                                        {
                                            $output.= "<td class='c-red'>".$row3[0]."</td>";
                                        }
                                        else
                                        {
                                            $output.= "<td class='c-green'>".$row3[0]."</td>";
                                            
                                        }
                                    }
                                    else
                                    {
                                        $output.='<td></td>';
                                    }

                                $output.= '
                                    </tr>
                                ';
                            }}
                            else{
                                $output.= '<td></td><td></td><td></td><td></td><td></td><td></td>';
                            }
                        }                       

                $output.= '
                        
                        </tbody>
                       
                </table>
                 <table class="table table-bordered">
                    <thead>';
                    $getAverage = mysqli_query($con, "SELECT average from average where control_id='".$controlID."'");
                    if(mysqli_num_rows($getAverage) > 0)
                    {
                        $temp = mysqli_fetch_row($getAverage);
                        $output.= '<th rowspan="2" class="text-center" width="30%"><b>Total GPA: '.$temp[0].'</b></th>';
                    }
                    $output.= '
                    </thead>
                    <tbody>
                    
                    
                    
                    </tbody>
                </table>
            </div>
        ';
    echo $output;
?>