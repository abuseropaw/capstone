<?php
function getCurrentSY($con)
{
	$query = mysqli_query($con, "SELECT max(sy_id) from schoolyear where sy_remarks='Open'");
	$temp = mysqli_fetch_row($query);
	return $temp[0];
}

function getCurrentSY1($con)
{
	$query = mysqli_query($con, "SELECT sy_year from schoolyear where sy_remarks='Open'");
	$temp = mysqli_fetch_row($query);
	return $temp[0];
}


function getSYofMonth($con, $monthID)
{
	$query = mysqli_fetch_row(mysqli_query($con, "SELECT sy_id from month where month_id='".$monthID."'"));
	return $query[0];
}

function getSYofYear($con, $year)
{
	$query = mysqli_fetch_row(mysqli_query($con, "SELECT sy_id from schoolyear where sy_year='".$year."'"));
	return $query[0];
}

function getSYofSection($con, $sectionID)
{
	$query = mysqli_query($con, "SELECT sy_id from section where section_id='".$sectionID."'");
	$temp = mysqli_fetch_row($query);

	$q = mysqli_fetch_row(mysqli_query($con, "SELECT sy_id from schoolyear where sy_year='".$temp[0]."'"));
	return $q[0];
}
function getSYofSection1($con, $sectionID)
{
	$query = mysqli_query($con, "SELECT sy_id from section where section_id='".$sectionID."'");
	$temp = mysqli_fetch_row($query);

	
	return $temp[0];
}

function getMonthID($con)
{
	$query = mysqli_query($con, "SELECT max(month_id) from month");
	$temp = mysqli_fetch_row($query);
	$id = $temp[0];
	$start = 100000;
	return ($id == null) ? $start:$id = $id+1;
}

function checkMonthifExist($con,$SY,$month)
{
	$query = mysqli_num_rows(mysqli_query($con, "SELECT * from month where sy_id='".$SY."' and month_description='".$month."'"));
	return ($query > 0) ? false:true;
}

function getMonthTotalDays($con, $monthID)
{
	$query = mysqli_query($con, "SELECT month_totalDays from month where month_id='".$monthID."'");
	$temp = mysqli_fetch_row($query);
	return $temp[0];
}
function checkifTotalDaysisQaulified($totalDays, $present,$absent,$tardy)
{
	return ($present > $totalDays) ? false :
           ((($present + $absent ) > $totalDays)? false :true);
}
function checkifCanIssueOrReturn($con, $level)
{
    $query = mysqli_num_rows(mysqli_query($con, "SELECT * from bookDetails where Level='".$level."'"));
    return ($query > 0)? true:false;
    
}


function checkifOpen($con,$monthID)
{
	$query = mysqli_fetch_row(mysqli_query($con, "SELECT month_status from month where month_id='".$monthID."'"));
	return ($query[0] == 'Open')? true:false;
}



  function deplication($con, $title,$SY){
    $query = mysqli_query($con, "SELECT count(*) from section where section_name='".$title."' and sy_id='".$SY."'");
    $count = mysqli_fetch_row($query);

    return ($count[0] == 0)? false:true;
  }

  function checkifNoChanges($con,$id,$SY,$name,$adviser)
  {
  	$query = mysqli_fetch_row(mysqli_query($con, "SELECT section_name from section where section_id='".$id."'"));
  	$getAdviser = mysqli_fetch_row(mysqli_query($con, "SELECT faculty_id,section_id,adviser_id from adviser where section_id='".$id."'"));

  	return ($name == $query[0] && $adviser == $getAdviser[0])? true:false;

  }
  function countLearner($con, $id)
    {
        $countLearner = mysqli_num_rows(mysqli_query($con, "SELECT * from controlDetails where section_id='".$id."'"));
        return ($countLearner == 0)? false:true;
    }
  
?>