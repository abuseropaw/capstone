<div class="col-sm-5">    
    <div class="card">
        
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th colspan="2" class="text-center"><b>Subject</b></th>
                    <th rowspan="2" class="text-center"><b>Assigned Teacher</b></th>
                    <th colspan="2" class="text-center"><b>Schedule</b></th>
                    <th rowspan="2" class="text-center"><b>Action</b></th>
                </tr>
                <tr>
                    <th class="text-center"><small><small><b>Title</b></small></small></th>
                    <th class="text-center"><small><small><b>Description</b></small></small></th>

                    <th class="text-center"><small><small><b>Day</b></small></small></th>
                    <th class="text-center"><small><small><b>Time</b></small></small></th>
                </tr>
                </thead>
                <tbody>
                    
                        <?php
                            $getSubjectOffered = mysqli_query($con, "SELECT * from subjectofferingdetails where section_id='".$sectionId."'");

                            while($row = mysqli_fetch_array($getSubjectOffered)){
                                echo "<tr><td>".$row['SubjectTitle']."</td>
                                <td>".$row['SubjecDescription']."</td>
                                <td>".$row['Teacher']."</td>
                                <td>".$row['so_days']."</td>
                                <td>" .$row['so_startTime']."-".$row['so_endTime']."</td>


                                <td>
                                    <div class='btn-demo'>
                                    <button type='submit' name='edit' id='".$row['so_id']."' data-toggle='tooltip' data-placement='top' title='Edit' class='btn btn-default btn-sm edit_Level'><i class='zmdi zmdi-edit'></i></button>
                                    <button class='btn btn-default monitor' id='".$row[0]."' data-toggle='tooltip' data-placement='top' title='Monitor Class' ><i class='zmdi zmdi-zoom-in'></i></button>
                                    </div>
                                </td>
                                </tr>";
                            }

                        ?>
                        
                    
                </tbody>
            </table>
        </div>
    </div>
</div>