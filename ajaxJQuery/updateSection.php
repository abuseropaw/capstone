<?php  
  session_start();
  include_once 'dbconnect.php';
  include 'globalFunction.php';
  $output = '';  
  $id = mysqli_real_escape_string($con, $_POST['section_id']);
  $title = mysqli_real_escape_string($con, $_POST['section_title1']);  
  $capacity = mysqli_real_escape_string($con, $_POST['section_capacity1']);  
  $maxGrade= mysqli_real_escape_string($con, $_POST['section_maxGrade1']);  
  $minGrade = mysqli_real_escape_string($con, $_POST['section_minGrade1']);
  $adviser = mysqli_real_escape_string($con, $_POST['section_adviser1']);
  


  $getSY = mysqli_query($con, "SELECT sy_year from schoolyear where sy_remarks='Open'");
  $tempSY = mysqli_fetch_row($getSY);
  $SY = $tempSY[0];
  $queryUpdateAdviser = '';
  $queryAddAdviser = '';

  $queryAdviser = mysqli_query($con, "SELECT max(adviser_id) FROM adviser");
  $numberofAdviser = mysqli_fetch_row($queryAdviser);
  $numberofAdvisers = $numberofAdviser[0] + 1;



 
  $query = "CALL updateSection('".$id."','".$title."','".$capacity."','".$maxGrade."','".$minGrade."','".$_SESSION['faculty_id']."',NOW(),'".$SY."')";
 
  $queryUpdateAdviser = "UPDATE adviser set faculty_id='".$adviser."', adviser_dateCreated = NOW(), adviser_createdBy = '".$_SESSION['faculty_id']."' where section_id='".$id."'";
  $queryAddAdviser = "INSERT into adviser values('".$adviser."','".$id."','".$numberofAdvisers."',NOW(),'".$_SESSION['faculty_id']."','".$SY."')";
    

    
    if(mysqli_query($con, $query)){ 
      //UPDATE
      if(checkifSectionhasAdviser($con,$id,$SY))
      {
        if(!checkifAdviserExist($con,$adviser,$id,$SY))
        {
          
            if(mysqli_query($con, $queryUpdateAdviser)){ }
        }
        else
        {
          echo 'error';
        }
      }
      //ADD
      else
      {
        if(!checkifAdviserExist($con,$adviser,$id,$SY))
        {
          if(!checkifAdviserhasAlreadyAssigned($con,$adviser,$SY))
          {
            if(!empty($adviser)){
              if(mysqli_query($con, $queryAddAdviser)){  }
                include_once 'displaySection.php';
            }
          }
          else
          {
            echo "error1";
          }
        }
        else
        {
          echo "error";
        }
      }
      include_once 'displaySection.php';
    }
  
  
  
  function checkifAdviserhasAlreadyAssigned($con,$adviser,$SY)
  {
    $count = mysqli_num_rows(mysqli_query($con, "SELECT * from adviser where sy_id='".$SY."' and faculty_id='".$adviser."'"));
    return ($count != 0)? true:false;
    
  }
  function checkifAdviserExist($con,$adviser,$id,$SY)
  {
    $query = mysqli_num_rows(mysqli_query($con, "SELECT * from adviser where faculty_id='".$adviser."' and section_id='".$id."' and sy_id='".$SY."'"));
    return ($query != 0)? true:false;
    
  }

  function checkifSectionhasAdviser($con,$id,$SY)
  {
    $count = mysqli_num_rows(mysqli_query($con, "SELECT * from adviser where section_id='".$id."' and sy_id='".$SY."'"));
    return ($count != 0)? true:false;
    
  }  

 ?>