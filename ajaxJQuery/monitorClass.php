<?php

	include_once 'dbconnect.php';
	$sid = mysqli_real_escape_string($con, $_GET['sid']);
	$id = mysqli_real_escape_string($con, $_GET['soid']);
	$temp = mysqli_fetch_row(mysqli_query($con, "SELECT * from subjectofferingdetails where so_id='".$id."'"));

	$output='';
    $output.='
<div class="card-body" id="listofStudents">
		
    <div class="table-responsive"><center><h1>'.$temp[6].'</h1></center>
    <table class="table table-bordered" id="asd">
        <thead>
            <tr>
            <th rowspan="2" class="text-center"><b>LRN</b></th>
            <th rowspan="2" class="text-center"><b>Name</b></th>
            <th rowspan="2" class="text-center"><b>Remarks</b></th>
            <th colspan="5" class="text-center"><b>Grade</b></th>
            <th rowspan="2" class="text-center"><b>History</b></th>
           
            </tr>

            <tr>
            <th class="text-center"><small><small>1st</small></small></th>
            <th class="text-center"><small><small>2nd</small></small></th>
            <th class="text-center"><small><small>3rd</small></small></th>
            <th class="text-center"><small><small>4th</small></small></th>
            <th class="text-center"><small><small>Average</small></small></th>
            
            </tr>
        </thead>
        <tbody>';

                $query = mysqli_query($con, "SELECT * FROM controlDetails where section_id='".$sid."' order by Gender DESC,Student ASC");
                while($row = mysqli_fetch_array($query)){
                    $controlID = $row[0];
                    $studentID = $row[4];
                    if($row[1] == 'DRP')
                    {
                        $output.= "
                        <tr class='c-red'>";
                    }
                    else
                    {
                        $output.= "
                        <tr>";
                    }
                    $output.= "
                        <td class='text-center'>".$row[7]."</td>
                        <td class='text-center'>".$row[5]."</td>
                        <td class='text-center'>".$row[1]."</td>";
                        
                        $getYear = "SELECT sy_id from section where section_id='".$sid."'";
                        $tempYear = mysqli_fetch_row(mysqli_query($con, $getYear));
                        $year = $tempYear[0];


                        $getQuarters = mysqli_query($con, "SELECT * from quarters where sy_id='".$year."'");

                        $countQuarters = mysqli_num_rows(mysqli_query($con, "SELECT * from quarters where sy_id='".$year."'"));
                        $getQuarters1 = mysqli_query($con, "SELECT * from quarters where sy_id='".$year."'");
                        if($row0 = mysqli_fetch_array($getQuarters)){
                        while($row1 = mysqli_fetch_array($getQuarters1)){
                            $qID = $row1[0];
                            $getGrades = mysqli_query($con, "SELECT * from gradeDetails where control_id = '".$controlID."' and quarters_id='".$qID."' and so_id='".$id."'");

                           
                            if($row2 = mysqli_fetch_array($getGrades)){
                                $grade = $row2[0];
                                $period = $row2['Period'];
                                
            

                                if($period = "1st Quarter"){
                                    $output.= "<td class='text-center'>".$grade."%</td>";
                                }else if($period = "2nd Quarter"){
                                    $output.= "<td class='text-center'>".$grade."%</td>";
                                }else if($period = "3rd Quarter"){
                                    $output.= "<td class='text-center'>".$grade."%</td>";
                                }else if($period = "4th Quarter"){
                                    $output.= "<td class='text-center'>".$grade."%</td>";
                                }
                            }else{
                                if($row[1] != 'T/O' && $row[1] != 'DRP')
                                {
                                    if(checker($con,$qID)){
                                        $output.= "<td class='text-center'></td>";
                                    }else{
                                        $output.= "<td class='text-center'></td>";    
                                    }
                                }
                                else
                                {
                                    $output.="<td></td>";
                                }
                                
                                
                            }
                        }
                        
                        if($countQuarters == 1){
                            $output.= "<td class='text-center'></td>";
                            $output.= "<td class='text-center'></td>";
                            $output.= "<td class='text-center'></td>";
                            
                        }else if($countQuarters == 2){
                            $output.= "<td class='text-center'></td>";
                            $output.= "<td class='text-center'></td>";
                            
                        }else if($countQuarters == 3){
                            $output.= "<td class='text-center'></td>";
                            
                        }
                        }else{
                            $output.= "<td class='text-center'></td>";
                            $output.= "<td class='text-center'></td>";
                            $output.= "<td class='text-center'></td>";
                            $output.= "<td class='text-center'></td>";
                            
                        }
                        if($av = mysqli_fetch_array(mysqli_query($con, "SELECT round(average) from SOAverageGrade where control_id='".$controlID."' and so_id='".$id."'"))){
                            $output.= "<td class='text-center'><b><h4>".$av[0]."</h4></b></td>";
                        }else{
                            $output.= "<td></td>";
                        }
                        $output.= "
    
                        <td width='5%'>
                                ";
                            if(checkCurrentSY($con,$year))
                            {
                                $output.= " 
                            <button type='button' onclick='openModal()' id='".$controlID."' name='".$id."' data-toggle='tooltip' data-placement='top' title='Details' class='btn btn-default btn-sm history_grade'><i class='zmdi zmdi-info'></i></button>";
                            }
                           $output.= "     
                        </td>
                    </tr>    
                    ";
                }

                function checker($con,$qID)
                {   
                    $query = mysqli_query($con, "SELECT * from quarters where quarters_id='".$qID."'");
                    if($row = mysqli_fetch_array($query)){
                        if($row[2] == 'Closed')
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
                function checkCurrentSY($con,$year)
                {
                    $query = mysqli_fetch_row(mysqli_query($con, "SELECT sy_year from schoolyear where sy_remarks='Open'"));
                    $current = $query[0];

                    if($current == $year)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
  
        $output.='</tbody>
    </table>
</div>                            
</div>';

    echo $output;

?>