<div class="row">
    <div class="col-sm-4">
        <div class='card'>
            <div class="card-header">
                <h4>Promoted</h4>
                <ul class="actions">
                    
                    <button type="button" onclick='window.open("summaryReport.php?type=promoted&level=<?php echo $level; ?>&sy=<?php echo $SY; ?>")' class="btn btn-danger btn-sm waves-effect"><i class="zmdi zmdi-collection-pdf"></i> <b>PRINT</b></button>
                      
                </ul>
            </div>                                  
            <div class="card-body card-padding lcb-form">
                <div>
                <ul>
                <?php
                    $getLearners = mysqli_query($con, "SELECT * from controlDetails where SchoolYear='".$SY."' and gradelevel='".$level."' order by Gender DESC,Student ASC");
                    while($row = mysqli_fetch_array($getLearners))
                    {
                        $id = $row['section_id'];
                        $controlId = $row[0];
                        $getAverageDetails = mysqli_query($con, "SELECT * from averageDetails where control_id='".$row[0]."'");
                        if($row1 = mysqli_fetch_array($getAverageDetails))
                        {
                            if(($row[1] == 'DRP'))
                            {
                            }   
                            else{
                                //$ans = checkLearnersProficiency($con,$id,$controlId);
                                if(checkLearnersProficiency($con,$id,$controlId) == "irregular")
                                {
                                    
                                }
                                else if(checkLearnersProficiency($con,$id,$controlId) == "promote")
                                {
                                    if(($row1[2] < 75))
                                    {
                                        
                                    }
                                    else
                                    {
                                        echo "
                                            <li>
                                                ".$row['Student']." - ".$row['SectionName']."
                                            </li>
                                            ";
                                    }
                                }
                                else if(checkLearnersProficiency($con,$id,$controlId) == "retain")
                                {
                                    
                                    
                                }
                            }
                            
                        }
                        else if($row[1] == 'DRP')
                        {
                            
                        }
                       
                    }

                ?>
                </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-4">
        <div class='card'>
            <div class="card-header">
                <h4>Irregular</h4>
                <ul class="actions">
                    
                    <button type="button" onclick='window.open("summaryReport.php?type=irregular&level=<?php echo $level; ?>&sy=<?php echo $SY; ?>")' class="btn btn-danger btn-sm waves-effect"><i class="zmdi zmdi-collection-pdf"></i> <b>PRINT</b></button>
                      
                </ul>
            </div>                                  
            <div class="card-body card-padding lcb-form">
                <div>
                <ul>
                <?php
                    $getLearners = mysqli_query($con, "SELECT * from controlDetails where SchoolYear='".$SY."' and gradelevel='".$level."' order by Gender DESC,Student ASC");
                    while($row = mysqli_fetch_array($getLearners))
                    {
                        $controlId = $row[0];
                        $getAverageDetails = mysqli_query($con, "SELECT * from averageDetails where control_id='".$row[0]."'");
                        if($row1 = mysqli_fetch_array($getAverageDetails))
                        {
                            if(($row[1] == 'DRP'))
                            {
                                
                            }   
                            else{
                                //$ans = checkLearnersProficiency($con,$id,$controlId);
                                if(checkLearnersProficiency($con,$id,$controlId) == "irregular")
                                {
                                    echo 
                                    "   
                                        <li>
                                            ".$row['Student']." - ".$row['SectionName']."
                                        </li>
                                    ";
                                }
                                else if(checkLearnersProficiency($con,$id,$controlId) == "promote")
                                {
                                    if(($row1[2] < 75))
                                    {
                                        
                                    }
                                    else
                                    {
                                        
                                    }
                                }
                                else if(checkLearnersProficiency($con,$id,$controlId) == "retain")
                                {
                                    
                                    
                                }
                            }
                            
                        }
                        else if($row[1] == 'DRP')
                        {
                          
                        }
                       
                    }

                ?>
                </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class='card'>
            <div class="card-header">
                <h4>Retained</h4>
                <ul class="actions">
                    
                    <button type="button" onclick='window.open("summaryReport.php?type=retained&level=<?php echo $level; ?>&sy=<?php echo $SY; ?>")' class="btn btn-danger btn-sm waves-effect"><i class="zmdi zmdi-collection-pdf"></i> <b>PRINT</b></button>
                      
                </ul>
            </div>                                  
            <div class="card-body card-padding lcb-form">
                <div>
                <ul>
                <?php
                    $getLearners = mysqli_query($con, "SELECT * from controlDetails where SchoolYear='".$SY."' and gradelevel='".$level."' order by Gender DESC,Student ASC");
                    while($row = mysqli_fetch_array($getLearners))
                    {
                        $controlId = $row[0];
                        $getAverageDetails = mysqli_query($con, "SELECT * from averageDetails where control_id='".$row[0]."'");
                        if($row1 = mysqli_fetch_array($getAverageDetails))
                        {
                            if(($row[1] == 'DRP'))
                            {
                                echo 
                                    "   
                                        <li>
                                            ".$row['Student']." - ".$row['SectionName']."
                                        </li>
                                    ";
                            }
                            else{
                                //$ans = checkLearnersProficiency($con,$id,$controlId);
                                if(checkLearnersProficiency($con,$id,$controlId) == "irregular")
                                {
                                                                
                                }
                                else if(checkLearnersProficiency($con,$id,$controlId) == "promote")
                                {
                                    if(($row1[2] < 75))
                                    {
                                        
                                        echo 
                                    "   
                                        <li>
                                            ".$row['Student']." - ".$row['SectionName']."
                                        </li>
                                    ";
                                    }
                                    else
                                    {
                                        
                                        
                                    }
                                }
                                else if(checkLearnersProficiency($con,$id,$controlId) == "retain")
                                {
                                    echo 
                                    "   
                                        <li>
                                            ".$row['Student']." - ".$row['SectionName']."
                                        </li>
                                    ";
                                    
                                }
                            }
                            
                        }
                        else if($row[1] == 'DRP')
                        {
                            echo 
                                    "   
                                        <li>
                                            ".$row['Student']." - ".$row['SectionName']."
                                        </li>
                                    ";
                        }
                        
                    }

                ?>
                </ul>
                </div>
            </div>
        </div>
    </div>

</div>