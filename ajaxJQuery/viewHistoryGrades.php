<?php
	session_start();
	include_once 'dbconnect.php';
	$soid = mysqli_real_escape_string($con, $_GET['soid']);
	$controlID = mysqli_real_escape_string($con, $_GET['controlID']);
	$temp = mysqli_fetch_row(mysqli_query($con, "SELECT * from subjectofferingdetails where so_id='".$soid."'"));

	$output = '';	
	$query = mysqli_query($con, "SELECT Period,grade_value,(DATE_FORMAT(grade_dateSubmit, '%M %e, %Y')),(DATE_FORMAT(grade_dateUpdate, '%M %e, %Y')),SubmittedBy from gradedetails where so_id='".$soid."' and control_id='".$controlID."' order by Period ASC");

	$output .= '  
	  <div class="card table-responsive">  
	       <table class="table table-bordered">
	       <thead>
	       		<tr><td colspan="5"><center><b>'.$temp[6].'</b></center></th></tr>
	       		<tr>
	       			<th>Period</th>
	       			<th>Grade</th>
	       			<th>Date Submitted</th>
	       			<th>Date Updated</th>
	       			<th>Submitted By</th>
	       		</tr>
	       </thead>

	 ';  
	 if(mysqli_num_rows($query) != 0)
	 { 
		  while($row = mysqli_fetch_array($query))  
		  {  
		       $output .= '  
		            <tr>  
		                 <td>'.$row[0].'</td>  
		                 <td >'.$row[1].'</td>
		                 <td >'.$row[2].'</td>  
		                 <td >'.$row[3].'</td>    
		                 <td >'.$row[4].'</td>  
		            </tr>  
		             		
		       ';  
		  }

	}  
	  $output .= '  
	       </table>  
	  </div>  
	  ';  
	  echo $output;   
?>