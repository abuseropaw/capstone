<?php
	
    session_start();
    

	include_once 'dbconnect.php';

    $allowed = array('Administrator','Registrar','Head Teacher','Teacher');

    if(in_array($_SESSION['faculty_type'], $allowed)){
        
	    $fname_ = mysqli_real_escape_string($con, $_POST['fname']);
        $mname_ = mysqli_real_escape_string($con, $_POST['mname']);
        $lname_ = mysqli_real_escape_string($con, $_POST['lname']);
        $sex_ = mysqli_real_escape_string($con, $_POST['sex']);
        $dob_ = mysqli_real_escape_string($con, $_POST['dob']);
        $age_ = mysqli_real_escape_string($con, $_POST['age']);
        $ip_ = mysqli_real_escape_string($con, $_POST['ip']);
        $religion_ = mysqli_real_escape_string($con, $_POST['religion']);
        $mt_ = mysqli_real_escape_string($con, $_POST['motherTongue']);
        $studentID_ = mysqli_real_escape_string($con, $_POST['studentID']);



        $getMaxControl_id = mysqli_fetch_row(mysqli_query($con, "SELECT * from controlinformation where control_id=(SELECT max(control_id) from control where student_id='".mysqli_real_escape_string($con, $studentID)."');"));
        $enrolledSY = $getMaxControl_id[6];
        $yrlevel =  $getMaxControl_id[19];
        $rem = $getMaxControl_id[1];

        $query = mysqli_query($con, "SELECT * from studentinfo1 where student_id='".$studentID."'");
        if($row = mysqli_fetch_array($query)){
            $id = $row['student_id'];
            $lrn = $row['student_LRN'];
            $name=$row['Name'];
            $sex = $row['sex'];

        //$BirthDate=$row['BirthDate'];
            $age = $row['Age'];
            $birthPlace=$row['BirthPlace'];
            $motherTongue=$row['MotherTongue'];
            $IP=$row['IP'];
            $Religion=$row['Religion'];
            $sy_id=$row['sy_id'];
            $sy_year=$row['sy_year'];
            $remarks = $row['control_remarks'];
            $sectionID = $row['section_id'];


            $HSSP=$row['student_addressHSSP'];
            $Barangay=$row['student_addressBarangay'];
            $Municipality=$row['student_addressMunicipality'];
            $Province=$row['student_addressProvince'];
            $Father=$row['Father'];
            $Mother=$row['Mother'];
            $GuardianName=$row['GuardianName'];
            $Relationship=$row['Relationship'];
            $GContact=$row['GContact'];
            $PContact=$row['PContact'];

        }
        $query1 = mysqli_query($con, "SELECT * from student_account where student_id='".$studentID."'");
        if($row = mysqli_fetch_array($query1)){
            $picture = $row['student_picture'];
            $fname = $row[2];
            $mname = $row[3];
            $lname = $row[4];
            $sex = $row[5];
            $age=$row[7];
            $BirthDate = $row['student_birthDate'];


        }
        $query2 = mysqli_query($con, "SELECT * from parent where student_id='".$studentID."'");
        if($row = mysqli_fetch_array($query2)){
            $ffname = $row[2];
            $fmname = $row[3];
            $flname = $row[4];
            $mfname = $row[5];
            $mmname = $row[6];
            $mlname = $row[7];

               
        }

        if(mysqli_query($con, "UPDATE student_account SET student_fname='".$fname_."', student_mname='".$mname_."', student_lname='".$lname_."', student_sex='".$sex_."', student_birthDate='".$dob_."', student_age='".$age_."', student_IP='".$ip_."', student_religion='".$religion_."', student_motherTongue='".$mt_."' where student_id='".$studentID_."'"))
        {
           
           include_once '../studentinformation_profile.php';
        }
        else
        {
            echo "0";
        }

    }
?>