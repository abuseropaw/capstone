<?php
	session_start();
	include_once 'dbconnect.php';
	$output = '';

	$hssp = mysqli_real_escape_string($con, $_POST['hssp']);
	$barangay = mysqli_real_escape_string($con, $_POST['barangay']);
	$municipality = mysqli_real_escape_string($con, $_POST['municipality']);
	$province = mysqli_real_escape_string($con, $_POST['province']);
	$student_id =  mysqli_real_escape_string($con, $_POST['studentID']);
	$SY = currentSY($con);

	$allowed = array('Administrator','Registrar','Head Teacher');

    if(in_array($_SESSION['faculty_type'], $allowed)){
	
		if(!check($con, $student_id, $SY))
		{
			$output= '0';
		}
		else
		{
			if(mysqli_query($con, "UPDATE address set student_addressHSSP='".$hssp."',student_addressBarangay='".$barangay."',student_addressMunicipality='".$municipality."',student_addressProvince='".$province."',student_addressdateCreated=NOW(), student_addresscreatedBy='".$_SESSION['faculty_id']."' where student_id='".$student_id."' and sy_id='".currentSY($con)."'"))
			{
				$output=  '1';
			}
			else
			{
				$output=  '0';
			}
		}
	}
	echo $output;
	function currentSY($con)
	{
		$query = mysqli_query($con, "SELECT sy_id from schoolyear where sy_remarks='Open'");
		$temp = mysqli_fetch_row($query);
		return $temp[0];
	}

	function check($con, $student_id, $SY)
	{
		$query = mysqli_fetch_row(mysqli_query($con, "SELECT count(*) from address where student_id='".$student_id."' and sy_id='".$SY."'"));
		if($query[0] == 0){return false;}
		else{return true;}
	}

?>