<?php
    session_start();
	include_once 'dbconnect.php';


	$output = '';
    $sectionID = mysqli_real_escape_string($con, $_POST['section']);

    if($_POST['search'] != ''){
        $geSearch = mysqli_real_escape_string($con, $_POST['search']);

    	$query = "SELECT * FROM studentinfo1view WHERE Name LIKE '%".$geSearch."%' or student_LRN LIKE '%".$geSearch."%'";
    	$result1 = mysqli_query($con, $query);
        $countResult = mysqli_num_rows($result1);
        if($countResult != 0)
        {
    		while($row = mysqli_fetch_array($result1))
            {


                $q = mysqli_fetch_row(mysqli_query($con, "SELECT student_picture from student_account where student_id='".$row[0]."'"));
                $pic = $q[0];
                $output .='
                <div class="col-md-2 col-sm-4 col-xs-6">
                    <div class="c-item">';
                        if($_SESSION['faculty_type'] == 'Head Teacher' | $_SESSION['faculty_type'] == 'Registrar'){

                        }else{
                            $output.= '<a href="studentInformation.php?sID='.$row[0].'" target="_blank" class="ci-avatar">';
                                if(empty($pic)){
                                    $output .='<img src="img/default-profile.png" alt="">';
                                }
                                else
                                {
                                    $output .='<img src="profile/'.$pic.'" alt="">';
                                }
                                $output .='
                            </a>';
                        }
                        $output .='
                        <div class="c-info">
                            <h5><b>'.$row[2].'</b></h5>
                            <small><b>'.$row[1].'</b></small>
                        </div>

                        <div class="c-footer">
                            <button type="button" onclick=window.open("studentinformation1.php?sID='.$row[0].'") name="details" id='.$row[0].' class="btn btn-info btn-xs details"> Info </button>';
                            if(checkifCurrentlyEnrolled($con,$row[1]))
                            {
                            $output.= '<button type="button" onclick=window.open("studentinformation1.php?sID='.$row[0].'") name="details" id='.$row[0].' class="btn btn-default btn-xs details" disabled> Enrolled </button>';
                            }
                            else
                            {
                                if(checkifCanbeEnroll($con,$row[1],$sectionID))
                                {
                                    $output.= '<button type="submit" id="'.$row[1].'" name="'.$row[2].'" value="'.$sectionID.'" class="btn bgm-green btn-success btn-xs details enroll_learnertoSection"> Enroll </button>';
                                }
                                else
                                {
                                    $output.= '<button type="button" onclick=window.open("studentinformation1.php?sID='.$row[0].'") name="details" id='.$row[0].' class="btn btn-default btn-xs details" disabled> RETAINED</button>';
                                }
                            }
                     $output.='
                        </div>

                    </div>
                </div>
                ';
            }
        }
        else
        {
            
        }

            echo $output;
	}
    function checkifCanbeEnroll($con, $lrn,$sectionID)
    {
        $count = 0;
        $level = getSectionLevel($con, $sectionID);//SECTION LEVEL
        if($row = mysqli_fetch_array(mysqli_query($con, "SELECT max(control_id) from controlinformation where student_LRN='".$lrn."' order by control_id ASC")))
        {
            $id = $row[0];
            if($row1 = mysqli_fetch_array(mysqli_query($con, "SELECT * from controlinformation where control_id ='".$id."'")))
            {
                $sectionID = $row1['section_id'];

                $countSubjectOffer = mysqli_num_rows(mysqli_query($con, "SELECT * from subjectoffering where section_id='".$sectionID."'"));
                $getAverage = mysqli_fetch_row(mysqli_query($con, "SELECT average from average where control_id='".$id."'"));
                $countAverage = mysqli_num_rows(mysqli_query($con, "SELECT * from averagepersubjectoffering where control_id='".$id."'"));

                $query = mysqli_query($con, "SELECT * from averagepersubjectoffering where control_id='".$id."'");



                    $t = explode(' ', $row1['control_remarks']);
                    $level1 = getLevelAble($con, $level);
                    if($row1['gradelevel'] == $level)// kung ang result kay pareha ug level sa section
                    {
                        if(($t[0] == 'DRP') | $t[0] == 'RETAINED'){
                            return true;
                        }else
                        {
                            if($countAverage == $countSubjectOffer){
                                
                                while($row = mysqli_fetch_array($query)){
                                    if($row[0] < 75)
                                    {
                                        $count++;
                                    }
                                }
                                if($count > 2)
                                {
                                    return true;
                                }
                                else if($count <= 2 && $count !=0)
                                {
                                    if($countSubjectOffer <= $count)
                                    {
                                        return true;
                                    }
                                    else
                                    {
                                        return false;
                                    }
                                    
                                }
                                else if($count == 0)
                                {
                                    
                                    if($getAverage[0] < 75)
                                    {
                                        return true;
                                    }
                                    else
                                    {
                                        return false;
                                    }
                                }
                            }
                            else{
                                return true;
                            }
                        }
                    }

                    else if(($row1['gradelevel'] == $level1))
                    {
                        if(($t[0] == 'DRP') | $t[0] == 'RETAINED'){
                            return false;
                        }else
                        {
                            if($countAverage == $countSubjectOffer){
                                
                                while($row = mysqli_fetch_array($query)){
                                    if($row[0] < 75)
                                    {
                                        $count++;
                                    }
                                }
                                if($count > 2)
                                {
                                    return false;
                                }
                                else if($count <= 2 && $count !=0)
                                {
                                    if($countSubjectOffer <= $count)
                                    {
                                        return false;
                                    }
                                    else
                                    {
                                        return false;
                                    }
                                    
                                }
                                else if($count == 0)
                                {
                                    
                                    if($getAverage[0] < 75)
                                    {
                                        return false;
                                    }
                                    else
                                    {
                                        return true;
                                    }
                                }
                            }
                            else{
                                return false;
                            }
                        }
                    }
                    else
                    {
                        return false;
                    }
                
            }
        }
    }
    function getLevelAble($con, $level)
    {
        $tmp = mysqli_fetch_row(mysqli_query($con, "SELECT year_lvl_id from yearlevel where year_lvl_title='".$level."'"));
        $id = $tmp[0];
        $id--;
        $tmp = mysqli_fetch_row(mysqli_query($con, "SELECT year_lvl_title from yearlevel where year_lvl_id='".$id."'"));
        $level = $tmp[0];
        return $level;
    }
    function getSectionLevel($con, $sectionID)
    {
        $tmp = mysqli_fetch_row(mysqli_query($con, "SELECT Gradelevel from classinformation where section_id='".$sectionID."'"));
        $level = $tmp[0];
        return $level;
    }
    function checkifCurrentlyEnrolled($con,$lrn)
    {
        $SY = currentSY($con);
        $count = mysqli_num_rows(mysqli_query($con, "SELECT * from controlDetails where LRN='".$lrn."' and SchoolYear='".$SY."'"));

        if($count == 1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    function currentSY($con)
    {
        if($row = mysqli_fetch_array(mysqli_query($con, "SELECT * from schoolyear where sy_remarks='Open'"))){
            return $row[1];
        }
    }
?>
