<?php
	$output='';
    $output .='
	<div class="table-responsive">
        <table class="table table-bordered table-nowrap">
            <thead>
                <th width="30%">Title</th>
                <th width="55%">Description</th>
                <th width="15%">Action</th>
            </thead>
            <tbody>';
                
                    $query = mysqli_query($con, "CALL selectCredentialDetails()");
                    while($row = mysqli_fetch_array($query)){
                        $temp = $row[0];
                        $output.= "
                        <tr>
                            <td>".$row[1]."</td>
                            <td>".$row[2]."</td>
                            <td>
                            <div class='btn-demo'>
                                <button type='submit' name='details' id='".$temp."' data-toggle='tooltip' data-placement='top' title='Details' class='btn btn-success btn-sm details'><i class='zmdi zmdi-info'></i></button>
                                <button type='submit' name='edit' id='".$temp."' data-toggle='tooltip' data-placement='top' title='Edit' class='btn bgm-orange btn-sm edit_credential'><i class='zmdi zmdi-edit'></i></button>
                                <button type='submit' name='delete' id='".$temp."' data-toggle='tooltip' data-placement='top' title='Delete' class='btn bgm-red btn-sm delete_credential'><i class='zmdi zmdi-delete'></i></button>
                               
                            </div>
                            </td>
                        </tr>    
                        ";
                    }
            $output .='    
            </tbody>
        </table>
    </div>';
    echo $output;
?>	