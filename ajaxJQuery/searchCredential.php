<?php

	include_once 'dbconnect.php';
	$search = mysqli_real_escape_string($con, $_POST['search']);

    if(empty($search))
    {
        $query = mysqli_query($con, "SELECT * from credentialdetails");
    }
    else
    {
        $query = mysqli_query($con, "SELECT * from credentialdetails where cr_title LIKE '%".$search."%' OR cr_description LIKE '%".$search."%'");
    }
$output='';

    $output .='
	<div class="table-responsive">
        <table class="table table-bordered table-nowrap">
            <thead>
                <th width="30%">Title</th>
                <th width="55%">Description</th>
                <th width="15%">Action</th>
            </thead>
            <tbody>';
                
                    
                    while($row = mysqli_fetch_array($query)){
                        $temp = $row[0];
                        $output.= "
                        <tr>
                            <td>".$row[1]."</td>
                            <td>".$row[2]."</td>
                            <td><button type='submit' name='details' id='".$temp."' class='btn btn-success btn-sm details'><i class='zmdi zmdi-info'></i></button>
                                <button type='submit' name='edit' id='".$temp."' class='btn bgm-orange btn-sm edit_credential'><i class='zmdi zmdi-edit'></i></button>
                                <button type='submit' name='delete' id='".$temp."' class='btn bgm-red btn-sm delete_credential'><i class='zmdi zmdi-delete'></i></button>
                            </td>
                        </tr>    
                        ";
                    }
            $output .='    
            </tbody>
        </table>
    </div>';

    echo $output;
?>