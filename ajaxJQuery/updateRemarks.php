<?php
    session_start();
    
    include_once 'dbconnect.php';
    //STUDENT INFORMATION
    $output='';
    $id = mysqli_real_escape_string($con, $_POST['sid2']);
    $controlID = mysqli_real_escape_string($con, $_POST['important_id2']);
    $studentID = mysqli_real_escape_string($con, $_POST['student_id2']);
    $level = mysqli_real_escape_string($con, $_POST['level2']);
    $remarks = mysqli_real_escape_string($con, $_POST['remarks1']);
    $date = mysqli_real_escape_string($con, $_POST['tiDate']);
    $school = mysqli_real_escape_string($con, $_POST['school']);
    $reason = mysqli_real_escape_string($con, $_POST['reason']);
    $other = mysqli_real_escape_string($con, $_POST['others']);


    function month($con, $date)
    {
        $result = mysqli_fetch_row(mysqli_query($con, "SELECT month('".$date."')"));
        return $result[0];
    }
    function monthDesc($con,$date)
    {
        $result = mysqli_fetch_row(mysqli_query($con, "SELECT monthname('".$date."')"));
        return $result[0];
    }
    if(!empty($date)){
        $concat = explode("/", $date);
        $date = $concat[2]."-".$concat[1]."-".$concat[0];
    }

    $allowed = array('T/I','T/O','DRP','LE', 'Pending TI');

    if(in_array($remarks, $allowed)){

        if($remarks == 'DRP')
        {
            $countDRP = mysqli_num_rows(mysqli_query($con, "SELECT * from remarks_history where remarks='DRP' and control_id='".$controlID."'"));
            if($reason == 'Others')
            {
                $query1 = "";
                $query2 = "";
                switch ($countDRP) {
                    case 0:
                        $query1 ="INSERT into remarks_history values('". month($con,$date). "','".$controlID."','".$remarks."','".$other."','".$date."','".monthDesc($con,$date)."')"; 
                        $query2 = "UPDATE remarks_history set remarks='".$remarks."', dateUpdate=NOW(), reqInformation='".$other."' where  month_no='".month($con,$date)."' and control_id='".$controlID."' ";
                        break;
                    case 1:
                        $query2 = "UPDATE remarks_history set month_no='".month($con,$date)."', month_desc='".monthDesc($con,$date)."', dateUpdate=NOW(), reqInformation='".$other."' where remarks='".$remarks."' and control_id='".$controlID."' ";
                        break;
                    
                    default:
                        # code...
                        break;
                }
                $query = "UPDATE control SET control_remarks='".$remarks."',control_dateUpdated='".$date."', control_createdBy='".$_SESSION['faculty_id']."', control_reqInformation='".$other."' where control_id='".$controlID."'"; 
                  
                
            }
            else
            {
                $query1 = "";
                $query2 = "";
                switch ($countDRP) {
                    case 0:
                        $query1 ="INSERT into remarks_history values('". month($con,$date). "','".$controlID."','".$remarks."','".$reason."','".$date."','".monthDesc($con,$date)."')";
                        $query2 = "UPDATE remarks_history set remarks='".$remarks."', dateUpdate=NOW(), reqInformation='".$reason."' where  month_no='".month($con,$date)."' and control_id='".$controlID."' "; 
                        break;
                    case 1:
                        $query2 = "UPDATE remarks_history set month_no='".month($con,$date)."', month_desc='".monthDesc($con,$date)."', dateUpdate=NOW(), reqInformation='".$reason."' where remarks='".$remarks."' and control_id='".$controlID."' ";
                        break;
                    
                    default:
                        # code...
                        break;
                }
                $query = "UPDATE control SET control_remarks='".$remarks."',control_dateUpdated='".$date."', control_createdBy='".$_SESSION['faculty_id']."', control_reqInformation='".$reason."' where control_id='".$controlID."'"; 
            }
        }
        else if($remarks == 'Pending TI')
        {
            $query = "UPDATE control SET control_remarks='".$remarks."',control_dateUpdated='".$date."', control_createdBy='".$_SESSION['faculty_id']."', control_reqInformation='' where control_id='".$controlID."'";  
            $query1 ="INSERT into remarks_history values('". month($con,$date). "','".$controlID."','".$remarks."','','".$date."','".monthDesc($con,$date)."')";
            $query2 = "UPDATE remarks_history set remarks='".$remarks."', dateUpdate=NOW() where month_no='".month($con,$date)."' and control_id='".$controlID."' ";
        }

        else if($remarks == 'T/I')
        {
            $countTI = mysqli_num_rows(mysqli_query($con, "SELECT * from remarks_history where remarks='T/I' and control_id='".$controlID."'"));
            $query1 = "";
            $query2 = "";
            switch ($countTI) {
                case 0:
                    $query1 ="INSERT into remarks_history values('". month($con,$date). "','".$controlID."','".$remarks."','".$school."','".$date."','".monthDesc($con,$date)."')"; 
                    $query2 = "UPDATE remarks_history set remarks='".$remarks."', dateUpdate=NOW(), reqInformation='".$school."' where  month_no='".month($con,$date)."' and control_id='".$controlID."' ";
                    break;
                case 1:
                    $query2 = "UPDATE remarks_history set month_no='".month($con,$date)."', month_desc='".monthDesc($con,$date)."', dateUpdate=NOW(), reqInformation='".$school."' where remarks='".$remarks."' and control_id='".$controlID."' ";
                    break;
                
                default:
                    # code...
                    break;
            }
            $query = "UPDATE control SET control_remarks='".$remarks."',control_dateUpdated='".$date."', control_createdBy='".$_SESSION['faculty_id']."', control_reqInformation='".$school."' where control_id='".$controlID."'"; 
        }

        else
        {
            $query = "UPDATE control SET control_remarks='".$remarks."',control_dateUpdated='".$date."', control_createdBy='".$_SESSION['faculty_id']."', control_reqInformation='".$school."' where control_id='".$controlID."'";  
            $query1 ="INSERT into remarks_history values('". month($con,$date). "','".$controlID."','".$remarks."','".$school."','".$date."','".monthDesc($con,$date)."')";
            $query2 = "UPDATE remarks_history set remarks='".$remarks."', dateUpdate=NOW(), reqInformation='".$school."' where month_no='".month($con,$date)."' and control_id='".$controlID."' ";
        }

        if(mysqli_query($con, $query)){


            $count = mysqli_num_rows(mysqli_query($con, "SELECT * from remarks_history where control_id='".$controlID."' and month_no='".month($con,$date)."'"));
            if($count == 0)
            {
                if(mysqli_query($con, $query1)){}
            }
            else
            {
                if(mysqli_query($con, $query2)){}
            }
               

            if($_SESSION['faculty_type'] == 'Head Teacher' | $_SESSION['faculty_type'] == 'Registrar')
            {
                include_once 'displayregistrarStudentDetails.php';
            }
            else
            {
                include_once '../teacherAdvisoryDetails_Learners.php';
            }

            
            
             
        }
        else
        {
            $output = '1';
            echo $output;
        }
    
    }
    else
    {
        $output = '1';
        echo $output;
    }
    

?>