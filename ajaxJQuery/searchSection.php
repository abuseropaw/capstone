<?php  
    include_once 'dbconnect.php';

    $search = mysqli_real_escape_string($con, $_POST['search']);
    $getCurrentSY = mysqli_query($con, "SELECT sy_year from schoolyear where sy_remarks='Open'");
    $temp = mysqli_fetch_row($getCurrentSY);
    $SY = $temp[0];


        $output = '';  
        
           $output .='
                    <div class="row" id="sectionList">
                        <div class="col-sm-3">
                            <div class="card">
                                <div class="card-header bgm-lightgreen"><br />
                                    <h2> Grade 7
                                    </h2>

                                    <ul class="actions actions-alt">
                                        <li class="dropdown">
                                            <a href="#" onclick="show1()" data-toggle="tooltip" data-placement="top" title="Add">
                                                <i class="zmdi zmdi-plus"></i>
                                            </a>

                                            <a href="adminClassDetails.php?level=Grade 7" data-toggle="tooltip" data-placement="top" title="Schedule">
                                                        <i class="zmdi zmdi-calendar-note"></i>
                                                    </a>
                                            <a href="adminClass.php" data-toggle="tooltip" data-placement="top" title="Refresh">
                                                <i class="zmdi zmdi-refresh-alt"></i>
                                            </a>
                                        </li>
                                    </ul>
                                    
                                </div>
                            </div>

                        ';
                        $query = mysqli_query($con, "SELECT * from classinformation where GradeLevel='Grade 7' and sy_id='".$SY."' and section_name LIKE '%".$search."%'");
                        while($row = mysqli_fetch_array($query)){
                            $id = $row['section_id'];
                            $name = $row['section_name'];
                            $capacity = $row['section_capacity'];
                            $max = $row['section_maxGrade'];
                            $min = $row['section_minGrade'];
                            $createdby = $row['Cretedby'];
                            $dateCreated = $row['section_dateCreated'];
                            $level = $row['GradeLevel'];
                            $adviser = $row['Adviser'];
                            $countGirls = mysqli_num_rows(mysqli_query($con, "SELECT * from controldetails where gender='Female' and section_id='".$id."'"));
                            $countBoys = mysqli_num_rows(mysqli_query($con, "SELECT * from controldetails where gender='Male' and section_id='".$id."'"));
                            $output .='
                            <div class="card">
                                <div class="card-header">
                                    <h2>'.$name.'<small>'.$adviser.'</small>
                                    </h2>
                                    <h2>
                                        <i class="zmdi zmdi-female"> '.$countGirls.' </i>  &nbsp&nbsp <i class="zmdi zmdi-male"> '.$countBoys.'</i> 
                                    </h2>
                                    <ul class="actions">
                                        <li class="dropdown">

                                            <a href="#" name="edit" id="'.$id.'" class="edit_data" data-toggle="tooltip" data-placement="top" title="Edit section">
                                                <i class="zmdi zmdi-edit"></i>
                                            </a>
                                            <a href="#" name="edit" id="'.$id.'" class="delete_data" data-toggle="tooltip" data-placement="top" title="Delete section">
                                                <i class="zmdi zmdi-delete"></i>
                                            </a>
                                            <a href="classSchedule.php?sID='.$id.'" data-toggle="tooltip" data-placement="top" title="Class schedule">
                                                <i class="zmdi zmdi-calendar-note"></i>
                                            </a>
                                            <a href="classLearner.php?sID='.$id.'" data-toggle="tooltip" data-placement="top" title="Learners for this section">
                                                <i class="zmdi zmdi-graduation-cap"></i>
                                            </a>
                                        </li>
                                        
                                    </ul>
                                   
                                </div>
                            </div>';
                        }
                    $output .='
                        </div>
                        <div class="col-sm-3">
                            <div class="card">
                                <div class="card-header bgm-amber"><br />
                                    <h2> Grade 8
                                    </h2>

                                    <ul class="actions actions-alt">
                                        <li class="dropdown">
                                            <a href="#" onclick="show2()" data-toggle="tooltip" data-placement="top" title="Add">
                                                <i class="zmdi zmdi-plus"></i>
                                            </a>

                                            <a href="adminClassDetails.php?level=Grade 8" data-toggle="tooltip" data-placement="top" title="Schedule">
                                                        <i class="zmdi zmdi-calendar-note"></i>
                                                    </a>
                                            <a href="adminClass.php" data-toggle="tooltip" data-placement="top" title="Refresh">
                                                <i class="zmdi zmdi-refresh-alt"></i>
                                            </a>
                                        </li>
                                    </ul>
                                    
                                </div>
                            </div>
                        ';
                            $query = mysqli_query($con, "SELECT * from classinformation where GradeLevel='Grade 8' and sy_id='".$SY."' and section_name LIKE '%".$search."%'");
                            while($row = mysqli_fetch_array($query)){
                                $id = $row['section_id'];
                                $name = $row['section_name'];
                                $capacity = $row['section_capacity'];
                                $max = $row['section_maxGrade'];
                                $min = $row['section_minGrade'];
                                $createdby = $row['Cretedby'];
                                $dateCreated = $row['section_dateCreated'];
                                $level = $row['GradeLevel'];
                                $adviser = $row['Adviser'];
                                $countGirls = mysqli_num_rows(mysqli_query($con, "SELECT * from controldetails where gender='Female' and section_id='".$id."'"));
                            $countBoys = mysqli_num_rows(mysqli_query($con, "SELECT * from controldetails where gender='Male' and section_id='".$id."'"));
                                $output .='
                                <div class="card">
                                    <div class="card-header">
                                        <h2>'.$name.'
                                            <small>
                                                '.$adviser.'
                                            </small>
                                        </h2>
                                        <h2>
                                        <i class="zmdi zmdi-female"> '.$countGirls.' </i>  &nbsp&nbsp <i class="zmdi zmdi-male"> '.$countBoys.'</i> 
                                    </h2>
                                        <ul class="actions">
                                            <li class="dropdown">

                                            <a href="#" name="edit" id="'.$id.'" class="edit_data" data-toggle="tooltip" data-placement="top" title="Edit section">
                                                <i class="zmdi zmdi-edit"></i>
                                            </a>
                                            <a href="#" name="edit" id="'.$id.'" class="delete_data" data-toggle="tooltip" data-placement="top" title="Delete section">
                                                <i class="zmdi zmdi-delete"></i>
                                            </a>
                                            <a href="classSchedule.php?sID='.$id.'" data-toggle="tooltip" data-placement="top" title="Class schedule">
                                                <i class="zmdi zmdi-calendar-note"></i>
                                            </a>
                                            <a href="classLearner.php?sID='.$id.'" data-toggle="tooltip" data-placement="top" title="Learners for this section">
                                                <i class="zmdi zmdi-graduation-cap"></i>
                                            </a>
                                        </li>
                                            
                                        </ul>
                                    </div>

                                        
                                </div>';
                            }
                    $output .='
                        </div>
                        <div class="col-sm-3">
                            <div class="card">
                                <div class="card-header bgm-red"><br />
                                    <h2> Grade 9
                                    </h2>

                                    <ul class="actions actions-alt">
                                        <li class="dropdown">
                                            <a href="#" onclick="show3()" data-toggle="tooltip" data-placement="top" title="Add">
                                                <i class="zmdi zmdi-plus"></i>
                                            </a>

                                            <a href="adminClassDetails.php?level=Grade 9" data-toggle="tooltip" data-placement="top" title="Schedule">
                                                        <i class="zmdi zmdi-calendar-note"></i>
                                                    </a>
                                            <a href="adminClass.php" data-toggle="tooltip" data-placement="top" title="Refresh">
                                                <i class="zmdi zmdi-refresh-alt"></i>
                                            </a>
                                        </li>
                                    </ul>
                                    
                                </div>
                            </div>
                        ';
                            $query = mysqli_query($con, "SELECT * from classinformation where GradeLevel='Grade 9' and sy_id='".$SY."' and section_name LIKE '%".$search."%'");
                            while($row = mysqli_fetch_array($query)){
                                $id = $row['section_id'];
                                $name = $row['section_name'];
                                $capacity = $row['section_capacity'];
                                $max = $row['section_maxGrade'];
                                $min = $row['section_minGrade'];
                                $createdby = $row['Cretedby'];
                                $dateCreated = $row['section_dateCreated'];
                                $level = $row['GradeLevel'];
                                $adviser = $row['Adviser'];
                                $countGirls = mysqli_num_rows(mysqli_query($con, "SELECT * from controldetails where gender='Female' and section_id='".$id."'"));
                            $countBoys = mysqli_num_rows(mysqli_query($con, "SELECT * from controldetails where gender='Male' and section_id='".$id."'"));
                                $output .='
                                <div class="card">
                                    <div class="card-header">
                                        <h2>'.$name.'<small>'.$adviser.'</small>
                                        </h2>
                                        <h2>
                                        <i class="zmdi zmdi-female"> '.$countGirls.' </i>  &nbsp&nbsp <i class="zmdi zmdi-male"> '.$countBoys.'</i> 
                                    </h2>
                                        <ul class="actions">
                                            <li class="dropdown">

                                            <a href="#" name="edit" id="'.$id.'" class="edit_data" data-toggle="tooltip" data-placement="top" title="Edit section">
                                                <i class="zmdi zmdi-edit"></i>
                                            </a>
                                            <a href="#" name="edit" id="'.$id.'" class="delete_data" data-toggle="tooltip" data-placement="top" title="Delete section">
                                                <i class="zmdi zmdi-delete"></i>
                                            </a>
                                            <a href="classSchedule.php?sID='.$id.'" data-toggle="tooltip" data-placement="top" title="Class schedule">
                                                <i class="zmdi zmdi-calendar-note"></i>
                                            </a>
                                            <a href="classLearner.php?sID='.$id.'" data-toggle="tooltip" data-placement="top" title="Learners for this section">
                                                <i class="zmdi zmdi-graduation-cap"></i>
                                            </a>
                                        </li>
                                            
                                        </ul>
                                    </div>

                                        
                                </div>';
                            }
                    $output .='
                        </div>
                        <div class="col-sm-3">
                            <div class="card">
                                <div class="card-header bgm-blue"><br />
                                    <h2> Grade 10
                                    </h2>

                                    <ul class="actions actions-alt">
                                        <li class="dropdown">
                                            <a href="#" onclick="show4()" data-toggle="tooltip" data-placement="top" title="Add">
                                                <i class="zmdi zmdi-plus"></i>
                                            </a>

                                            <a href="adminClassDetails.php?level=Grade 10" data-toggle="tooltip" data-placement="top" title="Schedule">
                                                        <i class="zmdi zmdi-calendar-note"></i>
                                                    </a>
                                            <a href="adminClass.php" data-toggle="tooltip" data-placement="top" title="Refresh">
                                                <i class="zmdi zmdi-refresh-alt"></i>
                                            </a>
                                        </li>
                                    </ul>
                                    
                                </div>
                            </div>
                        ';
                            $query = mysqli_query($con, "SELECT * from classinformation where GradeLevel='Grade 10' and sy_id='".$SY."' and section_name LIKE '%".$search."%'");
                            while($row = mysqli_fetch_array($query)){
                                $id = $row['section_id'];
                                $name = $row['section_name'];
                                $capacity = $row['section_capacity'];
                                $max = $row['section_maxGrade'];
                                $min = $row['section_minGrade'];
                                $createdby = $row['Cretedby'];
                                $dateCreated = $row['section_dateCreated'];
                                $level = $row['GradeLevel'];
                                $adviser = $row['Adviser'];
                                $countGirls = mysqli_num_rows(mysqli_query($con, "SELECT * from controldetails where gender='Female' and section_id='".$id."'"));
                            $countBoys = mysqli_num_rows(mysqli_query($con, "SELECT * from controldetails where gender='Male' and section_id='".$id."'"));
                                $output .='
                                <div class="card">
                                    <div class="card-header">
                                        <h2>'.$name.'
                                            <small>'.$adviser.'</small>
                                        </h2>
                                        <h2>
                                        <i class="zmdi zmdi-female"> '.$countGirls.' </i>  &nbsp&nbsp <i class="zmdi zmdi-male"> '.$countBoys.'</i> 
                                    </h2>
                                        <ul class="actions">
                                            <li class="dropdown">

                                            <a href="#" name="edit" id="'.$id.'" class="edit_data" data-toggle="tooltip" data-placement="top" title="Edit section">
                                                <i class="zmdi zmdi-edit"></i>
                                            </a>
                                            <a href="#" name="edit" id="'.$id.'" class="delete_data" data-toggle="tooltip" data-placement="top" title="Delete section">
                                                <i class="zmdi zmdi-delete"></i>
                                            </a>
                                            <a href="classSchedule.php?sID='.$id.'" data-toggle="tooltip" data-placement="top" title="Class schedule">
                                                <i class="zmdi zmdi-calendar-note"></i>
                                            </a>
                                            <a href="classLearner.php?sID='.$id.'" data-toggle="tooltip" data-placement="top" title="Learners for this section">
                                                <i class="zmdi zmdi-graduation-cap"></i>
                                            </a>
                                        </li>
                                            
                                        </ul>
                                    </div>

                                        
                                </div>';
                            }
                    $output .='
                        </div>
                    </div>
                    ';
          echo $output;


 ?>