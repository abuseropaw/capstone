<?php 
    session_start();
	include_once 'dbconnect.php';
    include 'globalFunction.php';
	include_once 'PHPExcel-1.8.1/Classes/PHPExcel.php';

	$sid = mysqli_real_escape_string($con, $_POST['sectionID']);
    $query = mysqli_query($con, "SELECT * FROM controlDetails where section_id='".$sid."' order by Gender DESC,Student ASC");
    $quarter = mysqli_real_escape_string($con, $_POST['quarter1']);
    $file  = $_FILES['files'];
    $id = mysqli_real_escape_string($con, $_POST['soid']);


    $file =$_FILES['files']['tmp_name'];
    $fileName= $_FILES['files']['name'];
    $fileError= $_FILES['files']['error'];

    $fileExtension = explode('.', $fileName);
    $fileActualExt = strtolower(end($fileExtension));
    $allowed = array('csv');
    if(in_array($fileActualExt, $allowed))
    {
        if($fileError === 0)
        {
            $objReader = PHPExcel_IOFactory::createReader('CSV');
            $objReader->setInputEncoding('ISO-8859-1');
            $objPHPExcel = $objReader->load($file);
            $worksheet = $objPHPExcel->getActiveSheet();
            

            $lastRow = $worksheet->getHighestRow();
           
            $temp = explode('/', $quarter);
            $quarter = $temp[0];
            $quarterDesc = $temp[1];

            $countGrade = mysqli_query($con, "SELECT count(*) from grade");
            $temp1 = mysqli_fetch_row($countGrade);
            $countGrade = $temp1[0];
            if(check($con,$quarter))
            {

                switch ($quarterDesc) {
                    case '1st Quarter':
                        for($row = 13; $row<=$lastRow; $row++){
                
                            $name = $worksheet->getCell('B'.$row)->getValue();
                            $first = $worksheet->getCell('F'.$row)->getValue();
                            $second = $worksheet->getCell('J'.$row)->getValue();
                            $third = $worksheet->getCell('N'.$row)->getValue();
                            $fourth = $worksheet->getCell('R'.$row)->getValue();
                            
                            setGradesofQuarter($con, $name, $countGrade, $first,$quarter,$sid,$id);
                            
                        }
                        include_once 'displayClassDetails.php';
                        break;
                    case '2nd Quarter':
                        for($row = 13; $row<=$lastRow; $row++){
                
                            $name = $worksheet->getCell('B'.$row)->getValue();
                            $first = $worksheet->getCell('F'.$row)->getValue();
                            $second = $worksheet->getCell('J'.$row)->getValue();
                            $third = $worksheet->getCell('N'.$row)->getValue();
                            $fourth = $worksheet->getCell('R'.$row)->getValue();
                            

                            setGradesofQuarter($con, $name, $countGrade, $second,$quarter,$sid,$id);
                        }
                        include_once 'displayClassDetails.php';
                        break;
                    case '3rd Quarter':
                        for($row = 13; $row<=$lastRow; $row++){
                
                            $name = $worksheet->getCell('B'.$row)->getValue();
                            $first = $worksheet->getCell('F'.$row)->getValue();
                            $second = $worksheet->getCell('J'.$row)->getValue();
                            $third = $worksheet->getCell('N'.$row)->getValue();
                            $fourth = $worksheet->getCell('R'.$row)->getValue();
                            

                            setGradesofQuarter($con, $name, $countGrade, $third,$quarter,$sid,$id);
                        }
                        include_once 'displayClassDetails.php';
                        break;
                    case '4th Quarter':
                        for($row = 13; $row<=$lastRow; $row++){
                
                            $name = $worksheet->getCell('B'.$row)->getValue();
                            $first = $worksheet->getCell('F'.$row)->getValue();
                            $second = $worksheet->getCell('J'.$row)->getValue();
                            $third = $worksheet->getCell('N'.$row)->getValue();
                            $fourth = $worksheet->getCell('R'.$row)->getValue();
                            

                            setGradesofQuarter($con, $name, $countGrade, $fourth
                                ,$quarter,$sid,$id);
                        }
                        
                        include_once 'displayClassDetails.php';
                        break;
                    default:
                        include_once 'displayClassDetails.php';
                        break;
                }
                 
                 
            }
            else
            {
                echo "close";
            }
        }
    }
    else
    {
        echo "not allowed";
    }

    function setGradesofQuarter($con, $name, $countGrade, $grade,$quarter,$sid,$id)
    {
        // $newName = str_ireplace('Ñ', 'N', $name);
        
        $getControlID = mysqli_query($con, "SELECT control_id,control_remarks from controldetails where Student='".$name."' and section_id='".$sid."'");
        $temporary = mysqli_fetch_row($getControlID);
        $controlID = $temporary[0]; 

    
        if($temporary[1] == "DRP" | $temporary[1] == "T/O")
        {
        }
        else
        {
     
            $query1 = "UPDATE grade set grade_value='".$grade."', grade_dateUpdate=NOW(), faculty_id='".$_SESSION['faculty_id']."' WHERE quarters_id='".$quarter."' and control_id='".$controlID."' and so_id='".$id."' and sy_id='".getCurrentSY1($con)."'";
            $query2 = "INSERT into grade VALUES('".$grade."','',NOW(),NOW(),'".$quarter."','".$_SESSION['faculty_id']."','".$controlID."','".$id."','".getCurrentSY1($con)."')";


            

            if(check($con, $quarter)){

                if($grade > 100)
                {
                    
                }
                else if($grade > -1)
                {
                    if( mysqli_query($con, $query2)){
                        
                    }
                    else
                    {
                        if( mysqli_query($con, $query1)){
                           
                        }
                    }
                }
                else
                {
                    
                } 
            }
            else
            {
                
            }

        }
 
    }
    function characterConverter($string)
    {
        $padparan = stripslashes($string);
        $econvert = iconv('UTF-8', 'windows-1252', $padparan);

        return $econvert;
    }
    

    function check($con,$quarterID)
    {
        $query = mysqli_fetch_row(mysqli_query($con, "SELECT quarters_remarks from quarters where quarters_id='".$quarterID."'"));
        $rem=$query[0];

        if($rem != 'Open')
        {
            return false;
        }
        else
        {
            return true;
        }
    }
?>