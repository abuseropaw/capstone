<div class="card">
    <div class="card-header">
        

       
    </div>

    <div class="card-body card-padding">
        <div class="row">
            <div class="col-sm-4 m-b-25">
                <p class="c-black"><b>School Register - (SF 1)</b></p>

                <ul>
                    <?php
                    $getSectionsforSF1 = mysqli_query($con, "SELECT * from classinformation where sy_id='".$SY."'");
                    while($row = mysqli_fetch_array($getSectionsforSF1))
                    {
                        echo '<li><b><a href="SF1-Excel.php?id='.$row[0].'">'.$row[1].' - School Form 1.xls</a></b></li>';
                    }
                    ?>
                </ul>
            </div>

            <div class="col-sm-4 m-b-25">
                <p class="c-black"><b>Daily Attendance Report of Learners - (SF 2)</b></p>

                <div class="panel-group" data-collapse-color="red" id="accordionRed" role="tablist"
                                         aria-multiselectable="true">
                    <div class="panel panel-collapse">
                        <ul class="clist clist-angle">
                        <?php
                        $getSectionsforSF1 = mysqli_query($con, "SELECT * from classinformation where sy_id='".$SY."'");
                        while($row = mysqli_fetch_array($getSectionsforSF1))
                        {
                            echo '
                            
                            <li><b>
                                <a data-toggle="collapse" data-parent="#accordionRed"
                                   href="#'.$row[0].'" aria-expanded="true">
                                  '.$row[1].'
                                </a></b>
                            </li>  

                          
                            <div id="'.$row[0].'" class="collapse out" role="tabpanel">
                                <div class="panel-body">
                                <ul>
                                ';
                                $getMonth = mysqli_query($con, "SELECT * from month where sy_id='".$sy_id."'");
                                while ($rows = mysqli_fetch_array($getMonth)) {
                                    echo '

                                    <li><a href="SF2-Excel.php?month='.$rows[0].'&section='.$row[0].'">
                                        '.$rows[1].' - School Form 2.xls
                                        </a>
                                    </li>
                                    ';
                                }
                            echo '
                                </ul>
                                </div>
                            </div>';
                        }
                        ?>
                    </ul>
                    </div>
                </div>
            </div>

            <div class="col-sm-4 m-b-25">
                <p class="c-black"><b>Books Issued and Returned - (SF 3)</b></p>

                <ul>
                    <?php
                        $getSectionforBook = mysqli_query($con, "SELECT * from classinformation where sy_id='".$SY."'");
                        while ($rowss = mysqli_fetch_array($getSectionforBook)) {
                            echo '

                            <li><b><a href="SF3-Excel.php?id='.$rowss[0].'">
                                '.$rowss[1].' - School Form 3.xls
                                </a></b>
                            </li>
                            ';
                        }

                    ?>
                </ul>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4 m-b-25">
                <p class="c-black"><b>Monthly Learner's Movement and Attendance - (SF 4)</b></p>

                <ul>
                    <?php
                        $getMonthforSF4 = mysqli_query($con, "SELECT * from month where sy_id='".$sy_id."'");
                        while ($rowsss = mysqli_fetch_array($getMonthforSF4)) {
                            echo '

                            <li><b><a href="SF4-Excel.php?id='.$rowsss[0].'">
                                '.$rowsss[1].' - School Form 4.xls
                                </a></b>
                            </li>
                            ';    
                        }

                    ?>
                </ul>
            </div>

            <div class="col-sm-4 m-b-25">
                <p class="c-black"><b>Report on Promotion & Level of Proficiency - (SF 5)</b></p>

                <ul>
                    <?php 
                        $getSectionforSF5 = mysqli_query($con, "SELECT * from classinformation where sy_id='".$SY."'");
                        while ($rowssss = mysqli_fetch_array($getSectionforSF5)) {
                            echo '

                            <li><b><a href="SF5-Excel.php?id='.$rowssss[0].'">
                                '.$rowssss[1].' - School Form 5.xls
                                </a></b>
                            </li>
                            ';   
                        }

                    ?>
                </ul>
            </div>

            <div class="col-sm-4 m-b-25">
                <p class="c-black"><b>Summarized Report on Promotion and Level of Proficiency - (SF 6)</b></p>

                <ul>
                    <?php
                        $query = mysqli_query($con, "SELECT * from schoolyear where sy_year='".$SY."'");
                        if(mysqli_num_rows($query) == 1){
                            echo '

                        <li><b><a href="SF6-Excel.php?id='.$sy_id.'">
                            '.$SY.' - School Form 6.xls
                            </a></b>
                        </li>
                        ';    
                        } 
                          

                    ?>
                </ul>
            </div>
        </div>
    </div>
</div>