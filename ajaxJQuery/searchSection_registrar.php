<?php
    include_once 'dbconnect.php';

    $search = mysqli_real_escape_string($con, $_POST['search']);
    $getCurrentSY = mysqli_query($con, "SELECT sy_year from schoolyear where sy_remarks='Open'");
    $temp = mysqli_fetch_row($getCurrentSY);
    $SY = $temp[0];

    $output = "";

    $output.="
<div class='row' id='sectionList'>

    <div class='col-sm-3'>
        <div class='card'>
            <div class='card-header bgm-lightgreen'>
                <h2> Grade 7
                </h2>
            </div>
        </div>";

        
            $query = mysqli_query($con, "SELECT * from classinformation where GradeLevel='Grade 7' and sy_id='".$SY."' and section_name LIKE '%".$search."%'");
            while($row = mysqli_fetch_array($query)){
                $id = $row['section_id'];
                $name = $row['section_name'];
                $capacity = $row['section_capacity'];
                $max = $row['section_maxGrade'];
                $min = $row['section_minGrade'];
                $createdby = $row['Cretedby'];
                $dateCreated = $row['section_dateCreated'];
                $level = $row['GradeLevel'];
                $adviser = $row['Adviser'];


                $countGirls = mysqli_num_rows(mysqli_query($con, "SELECT * from controldetails where gender='Female' and section_id='".$id."'"));
                $countBoys = mysqli_num_rows(mysqli_query($con, "SELECT * from controldetails where gender='Male' and section_id='".$id."'"));

                $output.= "
                <div class='card'>
                    <div class='card-header'>
                        <h2>".$name."
                        </h2>
                        <h2 class='actions'>
                            <i class='zmdi zmdi-female'> ".$countGirls." </i>  &nbsp&nbsp <i class='zmdi zmdi-male'> ".$countBoys."</i> 
                        </h2>
                        
                        <div class='btn-demo'>
                            <button onclick=window.open('registrarStudentDetails.php?sID=".$row['section_id']."') type='submit' name='edit' id='".$row['section_id']."' class='btn bgm-green btn-xs waves-effect enrol_student pull-right'></i>Enrollment</button>
                            <button onclick=window.open('registrarSectionClasses.php?sID=".$row['section_id']."') type='submit' name='edit' id='".$row['section_id']."' class='btn bgm-green btn-xs waves-effect enrol_student pull-right'></i>Classes</button>
                        </div>
                    </div>
                </div>";
            }
 $output.="
    </div>

    <div class='col-sm-3'>
        <div class='card'>
            <div class='card-header bgm-yellow'>
                <h2> Grade 8
                </h2>
            </div>
        </div>";
        
            $query = mysqli_query($con, "SELECT * from classinformation where GradeLevel='Grade 8' and sy_id='".$SY."' and section_name LIKE '%".$search."%'");
            while($row = mysqli_fetch_array($query)){
                $id = $row['section_id'];
                $name = $row['section_name'];
                $capacity = $row['section_capacity'];
                $max = $row['section_maxGrade'];
                $min = $row['section_minGrade'];
                $createdby = $row['Cretedby'];
                $dateCreated = $row['section_dateCreated'];
                $level = $row['GradeLevel'];
                $adviser = $row['Adviser'];
                $countGirls = mysqli_num_rows(mysqli_query($con, "SELECT * from controldetails where gender='Female' and section_id='".$id."'"));
                $countBoys = mysqli_num_rows(mysqli_query($con, "SELECT * from controldetails where gender='Male' and section_id='".$id."'"));
                $output.= "
                <div class='card'>
                    <div class='card-header'>
                        <h2>".$name."
                            
                        </h2>
                        <h2 class='actions'>
                            <i class='zmdi zmdi-female'> ".$countGirls." </i>  &nbsp&nbsp <i class='zmdi zmdi-male'> ".$countBoys."</i> 
                        </h2>
                        
                        <div class='btn-demo'>
                            <button onclick=window.open('registrarStudentDetails.php?sID=".$row['section_id']."') type='submit' name='edit' id='".$row['section_id']."' class='btn bgm-green btn-xs waves-effect enrol_student pull-right'></i>Enrollment</button>
                            <button onclick=window.open('registrarSectionClasses.php?sID=".$row['section_id']."') type='submit' name='edit' id='".$row['section_id']."' class='btn bgm-green btn-xs waves-effect enrol_student pull-right'></i>Classes</button>
                        </div>
                    </div>

                    
                </div>";
            }
  $output.="      
    </div>

    <div class='col-sm-3'>
        <div class='card'>
            <div class='card-header bgm-red'>
                <h2> Grade 9
                </h2>
            </div>
        </div>";
        
            $query = mysqli_query($con, "SELECT * from classinformation where GradeLevel='Grade 9' and sy_id='".$SY."' and section_name LIKE '%".$search."%'");
            while($row = mysqli_fetch_array($query)){
                $id = $row['section_id'];
                $name = $row['section_name'];
                $capacity = $row['section_capacity'];
                $max = $row['section_maxGrade'];
                $min = $row['section_minGrade'];
                $createdby = $row['Cretedby'];
                $dateCreated = $row['section_dateCreated'];
                $level = $row['GradeLevel'];
                $adviser = $row['Adviser'];
                $countGirls = mysqli_num_rows(mysqli_query($con, "SELECT * from controldetails where gender='Female' and section_id='".$id."'"));
                $countBoys = mysqli_num_rows(mysqli_query($con, "SELECT * from controldetails where gender='Male' and section_id='".$id."'"));
                $output.= " 
                <div class='card'>
                    <div class='card-header'>
                        <h2>".$name."
                            
                        </h2>
                        <h2 class='actions'>
                            <i class='zmdi zmdi-female'> ".$countGirls." </i>  &nbsp&nbsp <i class='zmdi zmdi-male'> ".$countBoys."</i> 
                        </h2>
                        
                       <div class='btn-demo'>
                            <button onclick=window.open('registrarStudentDetails.php?sID=".$row['section_id']."') type='submit' name='edit' id='".$row['section_id']."' class='btn bgm-green btn-xs waves-effect enrol_student pull-right'></i>Enrollment</button>
                            <button onclick=window.open('registrarSectionClasses.php?sID=".$row['section_id']."') type='submit' name='edit' id='".$row['section_id']."' class='btn bgm-green btn-xs waves-effect enrol_student pull-right'></i>Classes</button>
                        </div>
                    </div>

                    
                </div>";
            }
        $output.="
    </div>
    <div class='col-sm-3'>
        <div class='card'>
            <div class='card-header bgm-blue'>
                <h2> Grade 10
                </h2>
            </div>
        </div>";
        
            $query = mysqli_query($con, "SELECT * from classinformation where GradeLevel='Grade 10' and sy_id='".$SY."' and section_name LIKE '%".$search."%'");
            while($row = mysqli_fetch_array($query)){
                $id = $row['section_id'];
                $name = $row['section_name'];
                $capacity = $row['section_capacity'];
                $max = $row['section_maxGrade'];
                $min = $row['section_minGrade'];
                $createdby = $row['Cretedby'];
                $dateCreated = $row['section_dateCreated'];
                $level = $row['GradeLevel'];
                $adviser = $row['Adviser'];
                $countGirls = mysqli_num_rows(mysqli_query($con, "SELECT * from controldetails where gender='Female' and section_id='".$id."'"));
                $countBoys = mysqli_num_rows(mysqli_query($con, "SELECT * from controldetails where gender='Male' and section_id='".$id."'"));
                $output.= "
                <div class='card'>
                    <div class='card-header'>
                        <h2>".$name."
                            
                        </h2>
                        <h2 class='actions'>
                            <i class='zmdi zmdi-female'> ".$countGirls." </i>  &nbsp&nbsp <i class='zmdi zmdi-male'> ".$countBoys."</i> 
                        </h2>
                        
                        <div class='btn-demo'>
                            <button onclick=window.open('registrarStudentDetails.php?sID=".$row['section_id']."') type='submit' name='edit' id='".$row['section_id']."' class='btn bgm-green btn-xs waves-effect enrol_student pull-right'></i>Enrollment</button>
                            <button onclick=window.open('registrarSectionClasses.php?sID=".$row['section_id']."') type='submit' name='edit' id='".$row['section_id']."' class='btn bgm-green btn-xs waves-effect enrol_student pull-right'></i>Classes</button>
                        </div>
                    </div>
                </div>";
            }
 $output.="       
    </div>
</div>
";

    echo $output;
?>