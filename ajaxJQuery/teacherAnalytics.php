<?php
	include_once 'dbconnect.php';
    include 'globalFunction.php';
	$type = mysqli_real_escape_string($con, $_GET['type']);
	$id = mysqli_real_escape_string($con, $_GET['id']);
	if($type == "status")
	{
		$items = mysqli_fetch_array(mysqli_query($con, "SELECT * from sectionoverall where section_id='".$id."'"));
	}
	else if($type == "summary")
	{
		$getLearners = mysqli_query($con, "SELECT * from controlDetails where section_id='".$id."' order by Gender DESC,Student ASC");
		$maleRetained = 0;
        $femaleRetained = 0;
        $femalePromoted = 0;
        $femaleIrregular = 0;
        $malePromoted = 0;
        $maleIrregular = 0;




        while($row = mysqli_fetch_array($getLearners))
        {
            $controlId = $row[0];
            $getAverageDetails = mysqli_query($con, "SELECT * from averageDetails where control_id='".$row[0]."'");
            if($row[6] == 'Male')
            {
                
                if($row1 = mysqli_fetch_array($getAverageDetails))
                {
                    
                    if(($row[1] == 'DRP'))
                    {
                        $maleRetained++;
                    }
                    else{
                        //$ans = checkLearnersProficiency($con,$id,$controlId);
                        if(checkLearnersProficiency($con,$id,$controlId) == "irregular")
                        {
                            
                            $maleIrregular++;
                            
                        }
                        else if(checkLearnersProficiency($con,$id,$controlId) == "promote")
                        {
                            if(($row1[2] < 75))
                            {
                                
                                $maleRetained++;
                            }
                            else
                            {
                                
                                $malePromoted++;
                            }
                        }
                        else if(checkLearnersProficiency($con,$id,$controlId) == "retain")
                        {
                            
                            $maleRetained++;
                        }
                    }
                    
                }
                else if($row[1] == 'DRP')
                {
                    
                    $maleRetained++;
                }
               
            
                
            }
            else
            {
               
                if($row1 = mysqli_fetch_array($getAverageDetails))
                {
                    
                   

                    if(($row[1] == 'DRP'))
                    {
                       
                        $femaleRetained++;
                    }
                    else{
                        //$ans = checkLearnersProficiency($con,$id,$controlId);
                        if(checkLearnersProficiency($con,$id,$controlId) == "irregular")
                        {
                            
                                
                                $femaleIrregular++;
                            
                        }
                        else if(checkLearnersProficiency($con,$id,$controlId) == "promote")
                        {
                            if(($row1[2] < 75))
                            {
                                
                                $femaleRetained++;
                            }
                            else
                            {
                                
                                $femalePromoted++;
                            }
                        }
                        else if(checkLearnersProficiency($con,$id,$controlId) == "retain")
                        {
                            
                            $femaleRetained++;
                        }
                    }
                     
                }
                else if($row[1] == 'DRP')
                {
                   
                    $femaleRetained++;
                }

            }
        }
        
        $irregular = $femaleIrregular+$maleIrregular;
        $retained = $maleRetained+$femaleRetained;
        $promoted = $femalePromoted+$malePromoted;
        $items = array('irregular' => $irregular,'retained' => $retained, 'promoted' => $promoted );

	}

    else if($type= 'attendance')
    {
        $query123 = mysqli_query($con, "SELECT * from month where sy_id='".getSYofSection($con, $id)."' order by month_id DESC");

        while ($row = mysqli_fetch_array($query123)) {
            $totalPresentM = 0;
            $totalPresentF = 0;
            $totalDays = $row[2];

            $rowNumber2M = mysqli_fetch_row(mysqli_query($con, "SELECT Total from LateEnrollmentduringthemonth where Gender='Male' and section_id='".$id."' and Month='".$row[1]."'"));
            $rowNumber2F = mysqli_fetch_row(mysqli_query($con, "SELECT Total from LateEnrollmentduringthemonth where Gender='Female' and section_id='".$id."' and Month='".$row[1]."'"));
            $totalrowNumber2 = $rowNumber2M[0] + $rowNumber2F[0];

            $rowNumber10M = mysqli_fetch_row(mysqli_query($con, "SELECT * from TransferredInt where section_id='".$id."' and Gender='Male' and Month='".$row[1]."'"));
            $rowNumber10F = mysqli_fetch_row(mysqli_query($con, "SELECT * from TransferredInt where section_id='".$id."' and Gender='Female' and Month='".$row[1]."'"));

            $rowNumber3M = mysqli_fetch_row(mysqli_query($con, "SELECT count(*) from remarks_historydetails where Gender='Male' and section_id='".$id."' and month_desc='June'")); 
            $rowNumber3F =mysqli_fetch_row(mysqli_query($con, "SELECT count(*) from remarks_historydetails where Gender='Female' and section_id='".$id."' and month_desc='June'")); 


            $get1stMonth = mysqli_fetch_row(mysqli_query($con, "SELECT min(month_id) as month_id from month where sy_id='".getSYofSection($con, $id)."'"));
            $getlastMonth = mysqli_fetch_row(mysqli_query($con, "SELECT max(month_id) as month_id from month where sy_id='".getSYofSection($con, $id)."'"));

            $totalunregisteredMale = 0;
            $totalunregisteredFemale = 0;

            $totalDeductionMale = 0;
            $totalDeductionFemale = 0;
            for($iterLoop = $row[0]; $iterLoop >= $get1stMonth[0]; $iterLoop--)
            {
                $getDescriptionforMonth = mysqli_fetch_row(mysqli_query($con, "SELECT month_description from month where month_id='".$iterLoop."'"));
                echo '';
                $getUnregisterMale = mysqli_fetch_row(mysqli_query($con, "SELECT Total from totalunregistered where section_id='".$id."' and Gender='Male' and Month='".$getDescriptionforMonth[0]."'"));
                $getUnregisterFemale = mysqli_fetch_row(mysqli_query($con, "SELECT Total from totalunregistered where section_id='".$id."' and Gender='Female' and Month='".$getDescriptionforMonth[0]."'"));

                $totalunregisteredMale = $totalunregisteredMale + $getUnregisterMale[0];
                $totalunregisteredFemale = $totalunregisteredFemale + $getUnregisterFemale[0];
            }

            for($iterLoop1 = ($row[0] +1) ; $iterLoop1 <= $getlastMonth[0]; $iterLoop1++)
            {
                $getDescriptionforMonth1 = mysqli_fetch_row(mysqli_query($con, "SELECT month_description from month where month_id='".$iterLoop1."'"));
                
                ///DIRI PAKO
                $getdeductionMale = mysqli_fetch_row(mysqli_query($con, "SELECT count(*) from controldetails1 where section_id='".$id."' and Gender='Male' and Month='".$getDescriptionforMonth1[0]."' and control_remarks='T/I' and control_remarks='LE'"));
                $getdeductionFemale = mysqli_fetch_row(mysqli_query($con, "SELECT count(*) from controldetails1 where section_id='".$id."' and Gender='Female' and Month='".$getDescriptionforMonth1[0]."' and control_remarks='T/I' and control_remarks='LE'"));

                $totalDeductionMale = $totalDeductionMale + $getdeductionMale[0];
                $totalDeductionFemale = $totalDeductionFemale + $getdeductionMale[0];
            }


            if($row[0] != $get1stMonth[0])
            {
               

                $totalRegisteredMale = $rowNumber3M[0] - $totalunregisteredMale - $totalDeductionMale + $rowNumber10M[4] + $rowNumber2M[0];
                $totalRegisteredFemale = $rowNumber3F[0] - $totalunregisteredFemale - $totalDeductionFemale + $rowNumber10F[4] + $rowNumber2F[0];
                $totalrowNumber3 = $totalRegisteredMale + $totalRegisteredFemale;

            }
            else
            {

            $totalRegisteredMale = $rowNumber3M[0] - $totalunregisteredMale;
            $totalRegisteredFemale = $rowNumber3F[0] - $totalunregisteredFemale;
            $totalrowNumber3 = $totalRegisteredMale + $totalRegisteredFemale;

            }


            $rowNumber5M = mysqli_fetch_row(mysqli_query($con, "SELECT * from TotalDailyAttendance where section_id='".$id."' and Gender='Male' and month_id='".$row[0]."'"));
            $rowNumber5F = mysqli_fetch_row(mysqli_query($con, "SELECT * from TotalDailyAttendance where section_id='".$id."' and Gender='Female' and month_id='".$row[0]."'"));
            $rowNumber5Ms = $rowNumber5M[4] / $totalDays;
            $rowNumber5Fs= $rowNumber5F[4] / $totalDays;
            $totalrowNumber5 = $rowNumber5Ms + $rowNumber5Fs;


            $rowNumber6M = 0;
            $rowNumber6F = 0;
            if($totalRegisteredMale != 0 && $totalRegisteredFemale != 0){
                $rowNumber6M = ($rowNumber5Ms / $totalRegisteredMale) * 100;
                $rowNumber6F = ($rowNumber5Fs / $totalRegisteredFemale) * 100;
            }

            $totalrowNumber6 = ($rowNumber6M + $rowNumber6F) / 2;
            $items[] = array('Month' => $row[1],'adaM' => $rowNumber5Ms, 'adaF' => $rowNumber5Fs, 'pamM' => $rowNumber6M,'pamF' => $rowNumber6M, );
        }
    }








	function checkLearnersProficiency($con,$id,$controlId)
    {
        
        $countSubjectOffer = mysqli_num_rows(mysqli_query($con, "SELECT * from subjectoffering where section_id='".$id."'"));
        $countAverage = mysqli_num_rows(mysqli_query($con, "SELECT * from averagepersubjectoffering where control_id='".$controlId."'"));
        $query = mysqli_query($con, "SELECT * from averagepersubjectoffering where control_id='".$controlId."'");
        $count = 0;
        if($countAverage == $countSubjectOffer){
            while($row = mysqli_fetch_array($query)){
                if($row[0] < 75)
                {
                    $count++;
                }
            }
            if($count > 2)
            {
                return "retain";
            }
            else if($count <= 2 && $count != 0)
            {
                if($countSubjectOffer <= $count)
                {
                    return "retain";
                }
                else
                {
                    return "irregular";
                }
            }
            else
            {
                return "promote";
            }
           
        } else{
            return "retain";
        }
        

    }

	echo json_encode($items,JSON_NUMERIC_CHECK);
?>