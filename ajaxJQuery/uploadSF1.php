<?php
	session_start();
	include_once "PHPExcel-1.8.1/Classes/PHPExcel.php";
	include_once 'dbconnect.php';
    
    if(!checkifDateisCorrect($con))
    {
        echo 'incorrectMonth';
    }
    else
    {
        $sectionID = mysqli_real_escape_string($con, $_POST['sectionId']);
        $file  = $_FILES['files'];
       
        $file =$_FILES['files']['tmp_name'];
        $fileName= $_FILES['files']['name'];
        $fileError= $_FILES['files']['error'];

        $fileExtension = explode('.', $fileName);
        $fileActualExt = strtolower(end($fileExtension));
        $allowed = array('xlsx','xls');

        if(in_array($fileActualExt, $allowed))
        {
            if($fileError === 0)
            {
                $excelReader = PHPExcel_IOFactory::load($file);
                $worksheet = $excelReader->getActiveSheet();
                $lastRow = $worksheet->getHighestRow();




                $SY = getSY($con);

                for($row = 7; $row <=$lastRow; $row++)
                {
                    $LRN = $worksheet->getCell('A'.$row)->getValue();
                    if(LRN_valid_checker($LRN)){
                        $name = $worksheet->getCell('C'.$row)->getValue();

                        $lname = "";
                        $fname = "";
                        $mname = "";
                        $explodeName1 = explode(',', $name);
                        switch (count($explodeName1)) {
                            case 1:
                                $lname = $explodeName1[0];
                                break;
                            case 2:
                                $lname = $explodeName1[0];
                                $fname = $explodeName1[1];
                                break;
                            case 3:
                                $lname = $explodeName1[0];
                                $fname = $explodeName1[1];
                                $mname = $explodeName1[2];
                                break;
                            
                            default:
                                # code...
                                break;
                        }
                        

                        $sex = $worksheet->getCell('G'.$row)->getValue();
                        if($sex == 'M'){
                            $sex = 'Male';
                        }
                        else{
                            $sex = 'Female';
                        }

                        $bday = $worksheet->getCell('H'.$row)->getValue();
                        $explodebday = explode('-', $bday);
                        $bday1 = $explodebday[2].'-'.$explodebday[0].'-'.$explodebday[1];

                        $age = $worksheet->getCell('J'.$row)->getValue();
                        $motherTongue = $worksheet->getCell('L'.$row)->getValue();
                        $IP = $worksheet->getCell('N'.$row)->getValue();
                        $religion = $worksheet->getCell('O'.$row)->getValue();


                        $hssp = $worksheet->getCell('P'.$row)->getValue();
                        $barangay = $worksheet->getCell('Q'.$row)->getValue();
                        $municipality = $worksheet->getCell('U'.$row)->getValue();
                        $province = $worksheet->getCell('X'.$row)->getValue();

                        $parent_Ffname = "";
                        $parent_Fmname = "";
                        $parent_Flname = "";
                        $parent_Mfname="";
                        $parent_Mmname="";
                        $parent_Mlname="";

                        $father = $worksheet->getCell('AB'.$row)->getValue();
                        if($father == ''){

                        }else{
                            $explodefather = explode(',', $father);
                            switch (count($explodefather)) {
                                case 1:
                                    $parent_Flname = $explodefather[0];
                                    break;
                                case 2:
                                    $parent_Ffname = $explodefather[1];
                                
                                    $parent_Flname = $explodefather[0];
                                    break;
                                case 3:
                                    $parent_Ffname = $explodefather[1];
                                    $parent_Fmname = $explodefather[2];
                                    $parent_Flname = $explodefather[0];
                                    break;
                                
                                default:
                                   $parent_Ffname = $explodefather[1];
                                    $parent_Fmname = $explodefather[2];
                                    $parent_Flname = $explodefather[0];
                                    break;
                            }
                            
                        }
                        $mother = $worksheet->getCell('AF'.$row)->getValue();
                        if($mother == ''){

                        }else{
                            $explodemother = explode(',', $mother);
                            switch (count($explodemother)) {
                                case 1:
                                    $parent_Mlname=$explodemother[0];
                                    break;
                                case 2:
                                    $parent_Mfname=$explodemother[1];
                                    
                                    $parent_Mlname=$explodemother[0];
                                    break;
                                case 3:
                                    $parent_Mfname=$explodemother[1];
                                    $parent_Mmname=$explodemother[2];
                                    $parent_Mlname=$explodemother[0];
                                    break;
                                
                                default:
                                    $parent_Mfname=$explodemother[1];
                                    $parent_Mmname=$explodemother[2];
                                    $parent_Mlname=$explodemother[0];
                                    break;
                            }
                            
                        }
                        

                        $guardian = $worksheet->getCell('AH'.$row)->getValue();
                        $relation = $worksheet->getCell('AK'.$row)->getValue();
                        $contact = $worksheet->getCell('AL'.$row)->getValue();

                        $remarks = $worksheet->getCell('AN'.$row)->getValue();



                        if(!LRN_Checker($con, $LRN))
                        {

                            insertStudent($con,$LRN,$fname,$mname,$lname,$sex,$bday1,$age,$motherTongue,$IP,$religion);
                            insertAddress($con, $LRN,$hssp,$barangay,$municipality,$province,$SY);
                            insertParent($con, $LRN,$parent_Ffname,$parent_Fmname,$parent_Flname,$parent_Mfname,$parent_Mmname,$parent_Mlname,$contact,$guardian,$relation,$SY);
                             $CID = $LRN ."-". $SY;
                            insertControl($con, $CID,$LRN,$remarks,$sectionID,$SY);

                        }
                        else
                        {
                            $existStudentID = $LRN;
                            if(check_if_enrolled($con, $existStudentID,$SY))
                            {
                                
                            }else
                            {

                                $CID = $LRN ."-". $SY;
                                insertAddress($con, $LRN,$hssp,$barangay,$municipality,$province,$SY);
                                insertParent($con, $LRN,$parent_Ffname,$parent_Fmname,$parent_Flname,$parent_Mfname,$parent_Mmname,$parent_Mlname,$contact,$guardian,$relation,$SY);
                                insertControl($con, $CID,$LRN,$remarks,$sectionID,$SY);
                               
                            }
                        }
                    }
                    else
                    {
                       
                    }
                }
                $sectionID = mysqli_real_escape_string($con, $_POST['sectionId']);
                include_once 'displayClassLearner.php';

            }
            else
            {
                
            }
        }
        else
        {
            
        }
        
    }


    
    // function getStudentID($con, $LRN)
    // {
    //     $q = mysqli_query($con, "SELECT student_id from student_account where student_LRN='".$LRN."'");
    //     $temp = mysqli_fetch_row($q);

    //     return $temp[0];
    // }
 
        
    

    function insertStudent($con,$LRN,$explodeName1,$explodeName2,$explodeName3,$sex,$bday1,$age,$motherTongue,$IP,$religion)
    {
        if(mysqli_query($con, "CALL insertStudent('".$LRN."','".$LRN."','".$explodeName1."','".$explodeName2."','".$explodeName3."','".$sex."','".$bday1."','".$age."','".$motherTongue."','".$IP."','".$religion."',null,'".$LRN."',NOW(),'".$_SESSION['faculty_id']."')")){

        }

    }
    function insertAddress($con,$LRN,$hssp,$barangay,$municipality,$province,$SY)
    {
        if(mysqli_query($con, "CALL insertAddress('".$LRN."','".$hssp."','".$barangay."','".$municipality."','".$province."',NOW(),'".$_SESSION['faculty_id']."','".$SY."')")){

        }
    }
    function insertParent($con,$LRN,$parent_Ffname,$parent_Fmname,$parent_Flname,$parent_Mfname,$parent_Mmname,$parent_Mlname,$contact,$guardian,$relation,$SY)
    {
        if(mysqli_query($con, "CALL insertParent('".$LRN."','".$parent_Ffname."','".$parent_Fmname."','".$parent_Flname."','".$parent_Mfname."','".$parent_Mmname."','".$parent_Mlname."','".$contact."','".$guardian."','".$contact."','".$relation."',NOW(),'".$_SESSION['faculty_id']."','".$SY."')")){

        }
    }


    function checkifDateisCorrect($con)
    {
        if(year($con) == yearStarted($con))
        {
            if(monthDesc($con) == 'May' | monthDesc($con) == 'June')
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    function yearStarted($con)
    {
        $getSY = mysqli_fetch_row(mysqli_query($con, "SELECT year(sy_dateStarted) from schoolyear where sy_id=(SELECT max(sy_id) from schoolyear)"));

        return $getSY[0];
    }

    function year($con)
    {
        $result = mysqli_fetch_row(mysqli_query($con, "SELECT year(NOW())"));
        return $result[0];
    }
    function month($con)
    {
        $result = mysqli_fetch_row(mysqli_query($con, "SELECT month(NOW())"));
        return $result[0];
    }
    function monthDesc($con)
    {
        $result = mysqli_fetch_row(mysqli_query($con, "SELECT monthname(NOW())"));
        return $result[0];
    }

    function insertControl($con, $CID,$LRN,$remarks,$sectionID,$SY)
    {
        if($remarks == '')
        {
            if(mysqli_query($con, "CALL insertControl('".$CID."','',NOW(),NOW(),'".$_SESSION['faculty_id']."','".$LRN."','".$sectionID."','".$SY."',null)")){
                if(mysqli_query($con, "INSERT into remarks_history values('".month($con)."','".$CID."','','',NOW(),'".monthDesc($con)."')")){}
            }
            
        }
        else
        {
            $temp = explode(' ', $remarks);
            $rem = $temp[0];
            $date = $temp[1];
            $defaultRemarks = array('T/O','T/I','LE','DRP','CCT','B/A','LWD','ACL');
            if(in_array($rem, $defaultRemarks))
            {
                $temp1 = explode(':',$date);
                $date = $temp1[1];
                if($rem == 'LE')
                {
                     if(mysqli_query($con, "CALL insertControl('".$CID."','".$remarks."',NOW(),NOW(),'".$_SESSION['faculty_id']."','".$LRN."','".$sectionID."','".$SY."',null)"))
                    {

                    }
                }
                else if($rem == 'B/A')
                {
                     if(mysqli_query($con, "CALL insertControl('".$CID."','".$remarks."',NOW(),NOW(),'".$_SESSION['faculty_id']."','".$LRN."','".$sectionID."','".$SY."',null)"))
                    {

                    }
                }
                else if($rem == 'LWD')
                {
                     if(mysqli_query($con, "CALL insertControl('".$CID."','".$remarks."',NOW(),NOW(),'".$_SESSION['faculty_id']."','".$LRN."','".$sectionID."','".$SY."',null)"))
                    {

                    }
                }else{
                    if(mysqli_query($con, "CALL insertControl('".$CID."','".$rem."',NOW(),NOW(),'".$_SESSION['faculty_id']."','".$LRN."','".$sectionID."','".$SY."',null)"))
                    {

                    }
                }
                if(mysqli_query($con, "INSERT into remarks_history values('".month($con)."','".$CID."','".$rem."','',NOW(),'".monthDesc($con)."')")){}
            }
            else
            {
                if(mysqli_query($con, "CALL insertControl('".$CID."','".$remarks."',null,null,'".$_SESSION['faculty_id']."','".$LRN."','".$sectionID."','".$SY."',null)"))
                {
                    if(mysqli_query($con, "INSERT into remarks_history values('".month($con)."','".$CID."','".$remarks."','',NOW(),'".monthDesc($con)."')")){}
                }
            }

        }

    }
    function getSY($con)
    {
        $q = mysqli_query($con, "SELECT sy_id from schoolyear where sy_remarks='Open'");
        $temp = mysqli_fetch_row($q);
        $temp1 = $temp[0];
        return $temp1;
    }
    function LRN_valid_checker($LRN)
    {
        return (strlen($LRN) == 12) ? true:false;
    }
    function LRN_Checker($con,$LRN)
    {
        $q = mysqli_num_rows(mysqli_query($con, "SELECT * from student_account where student_LRN='".$LRN."'"));
        return ($q == 1) ? true:false;

    }
    function check_if_enrolled($con,$existStudentID,$SY)
    {
        $q = mysqli_num_rows(mysqli_query($con, "SELECT * from control where student_id='".$existStudentID."' and sy_id='".$SY."'"));

        if($q == 1){

            return true;
        }
        else
        {
            return false;
        }
    }
?>