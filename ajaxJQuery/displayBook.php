<?php
	$output='';

	$output.='
	<div class="table-responsive">
        <table class="table table-bordered table-nowrap" id="booksearching">
            <thead>
                <th width="15%"><b>Title</b></th>
                <th width="20%"><b>Description</b></th>
                <th width="15%"><b>Subject</b></th>
                <th width="10%"><b>Level</b></th>
                
                <th width="20%"><b>Created by</b></th>
                <th width="20%"><b>Action</b></th>
            </thead>
            <tbody>';

                     
                    while($row = mysqli_fetch_array($query1)){
                        $temp = $row['book_id'];
                        $output.= "
                        <tr>
                            <td>".$row[1]."</td>
                            <td>".$row[2]."</td>
                            <td>".$row[5]."</td>
                            <td>".$row[6]."</td>
                            <td>".$row[4]."</td>
                            <td>
							    <div class='btn-demo'>
								<button type='submit' name='details' id='".$temp."' data-toggle='tooltip' data-placement='top' title='Details' class='btn btn-default btn-sm details'><i class='zmdi zmdi-info'></i></button>
                                <button type='submit' name='edit' id='".$temp."' data-toggle='tooltip' data-placement='top' title='Edit' class='btn btn-default btn-sm edit_book'><i class='zmdi zmdi-edit'></i></button>
                                <button type='submit' name='delete' id='".$temp."' data-toggle='tooltip' data-placement='top' title='Details' class='btn btn-default btn-sm delete_book'><i class='zmdi zmdi-delete'></i></button>
								</div>
                            </td>
                        </tr>
                        ";
                    }

                $output .='
            </tbody>
        </table>
    </div>';
    echo $output;
?>
