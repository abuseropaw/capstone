<div class="row">

    <div class="col-sm-12">
        <div class='card'  id='teacherInfo'>
            
            <div class="card-body">
                <div class="card" id="profile-main">
                    <div class="pm-overview c-overflow">
                        
                        <div class="pmo-pic">
                            <div class="p-relative">


                                <a href="#changepic" data-toggle='modal'>
                                    <?php
                                        if($picture == '')
                                        {
                                           echo '<img class="img-responsive" src="img/default-profile.png" alt="">';
                                        }
                                        else
                                        {
                                            echo '<img class="img-responsive" src="profile/'.$picture.'" alt="">';
                                        }
                                    
                                    ?>
                                </a>
                                <a href="#changepic" data-toggle='modal' class="pmop-edit">
                                   <i class="zmdi zmdi-camera"></i> <span class="hidden-xs">Update Profile Picture</span>
                                </a>
                                
                            </div>


                                
                        </div>

                        <div class="pmo-block pmo-contact hidden-xs">
                            <h2>Contact</h2>
                            <?php
                            echo "<ul>
                                <li><i class='zmdi zmdi-phone'></i>".$fname.' '.$mname.' '.$lname."</li>
                                <li>
                                    <i class='zmdi zmdi-pin'></i>
                                    <address class='m-b-0 ng-binding'>
                                        ".$address."
                                    </address>
                                </li>
                            </ul>";
                            ?>
                        </div>

                    
                    </div>

                    <div class="pm-body clearfix">
                        <div role="tabpanel">
                            <ul class="tab-nav tn-justified" role="tablist" data-tab-color="green">
                                <li class="active"><a href="#acount" aria-controls="acount" role="tab"
                                  data-toggle="tab">Account</a></li>
                                <li><a href="#class" aria-controls="class" role="tab" data-toggle="tab">Logs</a></li>
                                
                               
                            </ul>
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="acount">
                                    <div class="pmb-block">
                                        <div class="pmbb-header">
                                            <h2><i class="zmdi zmdi-account m-r-10"></i> Basic Information</h2>

                                            <ul class="actions">
                                                <li class="dropdown">
                                                    <a href="#" data-toggle="dropdown">
                                                        <i class="zmdi zmdi-more-vert"></i>
                                                    </a>

                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                        <li>
                                                            <a data-ma-action="profile-edit" href="#">Edit</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="pmbb-body p-l-30">
                                            <div class="pmbb-view">
                                                <?php
                                                echo "
                                                <dl class='dl-horizontal'>
                                                    <dt>ID #</dt>
                                                    <dd>".$id."</dd>
                                                </dl>
                                                <dl class='dl-horizontal'>
                                                    <dt>Status</dt>
                                                    <dd>".$status."</dd>
                                                </dl>

                                                <dl class='dl-horizontal'>
                                                    <dt>First Name</dt>
                                                    <dd>".$fname."</dd>
                                                </dl>
                                                <dl class='dl-horizontal'>
                                                    <dt>Middle Name</dt>
                                                    <dd>".$mname."</dd>
                                                </dl>
                                                <dl class='dl-horizontal'>
                                                    <dt>Last Name</dt>
                                                    <dd>".$lname."</dd>
                                                </dl>
                                                <dl class='dl-horizontal'>
                                                    <dt>Gender</dt>
                                                    <dd>".$gender."</dd>
                                                </dl>
                                                  
                                                <dl class='dl-horizontal'>
                                                    <dt>Birthday</dt>
                                                    <dd>".$dob."</dd>
                                                </dl>
                                                <dl class='dl-horizontal'>
                                                    <dt>Position</dt>
                                                    <dd>".$type."</dd>
                                                </dl> 
                                                <dl class='dl-horizontal'>
                                                    <dt>Minor Subject</dt>
                                                    <dd>".$minor."</dd>
                                                </dl>
                                                <dl class='dl-horizontal'>
                                                    <dt>Major Subject</dt>
                                                    <dd>".$major."</dd>
                                                </dl>

                                                <dl class='dl-horizontal'>
                                                    <dt>Contant No.</dt>
                                                    <dd>".$phone."</dd>
                                                </dl>
                                                <dl class='dl-horizontal'>
                                                    <dt>Home Address</dt>
                                                    <dd>".$address."</dd>
                                                </dl>
                                                ";
                                                ?>
                                            </div>
                                            
                                            <div class="pmbb-edit">
                                                <form id="editForm" method="post">
                                                <?php 
                                                if($type !='Administrator')
                                                {
                                                    echo '
                                                    <dl class="dl-horizontal">
                                                        <dt class="p-t-10">Status</dt>
                                                        <dd>
                                                            <div class="fg-line">
                                                                <select class="form-control selectpicker" id="status" name="status">
                                                                    <option>'.$status .'</option>
                                                                    <option>Active</option>
                                                                    <option>Inactive</option>
                                                                </select>
                                                            </div>

                                                        </dd>
                                                    </dl>';
                                                }
                                                ?>
                                                <dl class="dl-horizontal">
                                                    <dt class="p-t-10">First Name</dt>
                                                    <dd>
                                                        <div class="fg-line">
                                                            <input type="text" name='fname' id='fname' class="form-control"
                                                                   value ="<?php echo $fname ?> " required>
                                                        </div>

                                                    </dd>
                                                </dl>

                                                <dl class="dl-horizontal">
                                                    <dt class="p-t-10">Middle Name</dt>
                                                    <dd>
                                                        <div class="fg-line">
                                                            <input type="text" id='mname' name='mname' class="form-control"
                                                                   value ="<?php echo $mname ?>">
                                                        </div>

                                                    </dd>
                                                </dl>

                                                <dl class="dl-horizontal">
                                                    <dt class="p-t-10">Last Name</dt>
                                                    <dd>
                                                        <div class="fg-line">
                                                            <input type="text" id='lname' name='lname' class="form-control"
                                                                   value ="<?php echo $lname ?>" required>
                                                        </div>

                                                    </dd>
                                                </dl>
                                                <dl class="dl-horizontal">
                                                    <dt class="p-t-10">Gender</dt>
                                                    <dd>
                                                        <div class="fg-line">
                                                            <select class="form-control selectpicker" id='gender' name='gender'>
                                                                <option><?php echo $gender ?></option>
                                                                <option>Male</option>
                                                                <option>Female</option>
                                                            </select>
                                                        </div>
                                                    </dd>
                                                </dl>
                                                <dl class="dl-horizontal">
                                                    <dt class="p-t-10">Birthday</dt>
                                                    <dd>
                                                        <div class="dtp-container dropdown fg-line">
                                                            <input type='text' name="dob" id="dob" class="form-control"
                                                                   value ="<?php echo $dob ?>">
                                                        </div>
                                                    </dd>
                                                </dl>
                                                <?php
                                                    if($type != 'Administrator')
                                                    {
                                                        echo '

                                                            <dl class="dl-horizontal">
                                                                <dt class="p-t-10">Position</dt>
                                                                <dd>
                                                                    <div class="fg-line">
                                                                        <select class="form-control selectpicker" name="position" id="position">

                                                                            <option>'.$type.'</option>
                                                                            <option>Teacher</option>
                                                                            
                                                                            <option>Head Teacher</option>
                                                                            <option>Principal</option>
                                                                            <option>Registrar</option>
                                                                        </select>
                                                                    </div>
                                                                </dd>
                                                            </dl>

                                                        ';
                                                    }
                                                ?>
                                                

                                                <dl class="dl-horizontal">
                                                    <dt class="p-t-10">Minor Subject</dt>
                                                    <dd>
                                                        <div class="fg-line">
                                                            <select class="form-control selectpicker" name="minor" id="minor">

                                                                <option><?php echo $minor; ?></option>
                                                                <?php $query= mysqli_query($con, "SELECT distinct(subj_title) from subjects");
                                                                    while($row = mysqli_fetch_array($query)){
                                                                        echo "<option>".$row[0]."</option>";
                                                                    }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </dd>
                                                </dl>

                                                <dl class="dl-horizontal">
                                                    <dt class="p-t-10">Major Subject</dt>
                                                    <dd>
                                                        <div class="fg-line">
                                                            <select class="form-control selectpicker" name="major" id="major"> 

                                                                <option><?php echo $major; ?></option>
                                                                <?php $query= mysqli_query($con, "SELECT distinct(subj_title) from subjects");
                                                                    while($row = mysqli_fetch_array($query)){
                                                                        echo "<option>".$row[0]."</option>";
                                                                    }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </dd>
                                                </dl>
                                                <dl class="dl-horizontal">
                                                    <dt class="p-t-10">Contact No.</dt>
                                                    <dd>
                                                        <div class="fg-line">
                                                    

                                                               <input type='text' name="phone" id="phone" class="form-control" value ="<?php echo $phone ?>">
                                                        </div>
                                                    </dd>
                                                </dl>
                                                <dl class="dl-horizontal">
                                                    <dt class="p-t-10">Home Address</dt>
                                                    <dd>
                                                        <div class="fg-line">
                                                    

                                                               <input type='text' name="address" id="address" class="form-control" value ="<?php echo $address ?>">
                                                               <input type="hidden" name="faculty_id" id="faculty_id" value="<?php echo $teacherID ?>">
                                                        </div>
                                                    </dd>
                                                </dl>
                                                <div class="m-t-30">
                                                    <button type="submit" name="editBasic" id="editBasic" class="btn btn-success btn-lg">SAVE CHANGES</button>

                                                    <button data-ma-action="profile-edit-cancel" class="btn btn-warning btn-lg">CANCEL</button>
                                                </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>

                                    
                                </div>
                                <div role="tabpanel" class="tab-pane" id="class">
                                    <div class="pmb-block">
                                        <div class="pmbb-header">
                                            <h2><i class="zmdi zmdi-account m-r-10"></i> Logs</h2>

                                            <ul class="actions">
                                                <li class="dropdown">
                                                    <a href="#" data-toggle="dropdown">
                                                        <i class="zmdi zmdi-more-vert"></i>
                                                    </a>

                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                        <li>
                                                            <a data-ma-action="profile-edit" href="#">Edit</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="pmbb-body p-l-30">
                                            
                                        </div>
                                    </div>
                                    
                                </div>
                                
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>