<div class="card-body card-padding">
                                
    <div class="card">
        
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th colspan="2" class="text-center">Subject</th>
                    <th rowspan="2" class="text-center">Assigned Teacher</th>
                    <th colspan="2" class="text-center">Schedule</th>
                    <th rowspan="2" class="text-center">Action</th>
                </tr>
                <tr>
                    <th class="text-center"><small><small>Title</small></small></th>
                    <th class="text-center"><small><small>Description</small></small></th>

                    <th class="text-center"><small><small>Day</small></small></th>
                    <th class="text-center"><small><small>Time</small></small></th>
                </tr>
                </thead>
                <tbody>
                    
                        <?php
                            $getSubjectOffered = mysqli_query($con, "SELECT * from subjectofferingdetails where section_id='".$_GET['sID']."'");

                            while($row = mysqli_fetch_array($getSubjectOffered)){
                                echo "<tr><td>".$row['SubjectTitle']."</td>
                                <td>".$row['SubjecDescription']."</td>
                                <td>".$row['Teacher']."</td>
                                <td>".$row['so_days']."</td>
                                <td>" .$row['so_startTime']."-".$row['so_endTime']."</td>


                                <td><button type='submit' name='edit' id='".$row['so_id']."' class='btn btn-warning btn-sm edit_Level'><i class='zmdi zmdi-edit'></i></button>
                                </td>
                                </tr>";
                            }

                        ?>
                        
                    
                </tbody>
            </table>
        </div>
    </div>
</div>