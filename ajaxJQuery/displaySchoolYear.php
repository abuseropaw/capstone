<?php
    $output= '';
    $output.='
    <div class="block-header">
        <h1>School Year
        </h1>

        <div class="actions">
            <div class="btn-demo"  id="btn-color-targets">';
            
                $query = mysqli_query($con, "SELECT * from schoolyear where sy_remarks='Open'");
                if($row = mysqli_fetch_array($query)){
                    
                }else{
                    

                    $output.= '<button  href="#modalColor" data-toggle="modal" class="btn btn-default btn-lg"><i class="zmdi zmdi-bookmark"></i> Start School Year</button>';
                }
        
    $output .='
            </div>
        </div>
    </div>';




        $query = mysqli_query($con, "SELECT * from schoolyear order by sy_id DESC");

        while($row = mysqli_fetch_array($query))
        {
            $id = $row[0];
            $sy = $row[1];
            $dateStarted = $row[2];
            $dateEnd = $row[3];
            $remarks = $row[4];
            $output.= "<div class='row'>";
            if($remarks == 'Closed'){
            $output.= "
                <div class='col-sm-8 pull-right'>";
            }else
            {
                $output.= "
                <div class='col-sm-8'>";
            }
                    $output.= "<div class='card'>";
                    if($remarks == 'Closed'){
                        $output.= "<div class='card-header bgm-gray'>";
                            $output.= "<h2><b>".$sy."</b></h2>

                        "; }
                    else { $output.= "<div class='card-header bgm-lightgreen'>";
                    $output.= "<h1 class='c-white'><b>".$sy."</b></h1>

                            <ul class='actions actions-alt'>
                                <li class='dropdown'>
                                <a href='#' class='schoolYears' id='".$id."' data-toggle='tooltip' data-placement='top' title='Close school year'>
                                    <h1 class='c-white'><i class='zmdi zmdi-close-circle-o'></h1></i> <b></b>
                                </a>
                                <a href='#' class='deleteSchoolYear' id='".$id."'  data-toggle='tooltip' data-placement='top' title='Delete school year'>
                                    <h1 class='c-white'><i class='zmdi zmdi-delete'></h1></i> <b></b>
                                </a>
                                </li>
                                    
                            </ul>"; }
                            $output.= "
                        </div>

                        <div class='card-body card-padding'>
                            

                            
                            <div class='block-header'>
                            <h4><i class='zmdi zmdi-time-interval'></i><b> QUARTERS</b></h4>
                                ";
                                    if($remarks != 'Closed')
                                    {
                                           $output.= " <div class='actions'><button  href='#modalQuarter' data-toggle='modal' class='btn btn-default btn-sm'><i class='zmdi zmdi-plus'></i> <b> QUARTER</b></button></div>";
                                    }
                                $output.= "
                            </div>
                            
                            <div class='table-responsive'>

                                <table class='table table-bordered table-nowrap dataTable'>
                                    <thead>
                                        <th width='20%'><b>QRTR</b></th>
                                        <th width='30%'><b>Started</b></th>
                                        <th width='30%'><b>End</b></th>
                                        <th width='15%'><b>Remarks</b></th>
                                        ";
                                        if($remarks == 'Open')
                                        {
                                            $output.= "<th width='10%'><b>Edit</b></th>";
                                        }
                                    $output.= "
                                    </thead>
                                    <tbody>";
                                        $getQuarterDetails = mysqli_query($con, "SELECT * from quarterdetails where sy_id='".$sy."' order by QUARTERS_ID ASC");
                                        $getQuarterDetails1 = mysqli_query($con, "SELECT * from quarterdetails where sy_id='".$sy."' order by QUARTERS_ID ASC");
                                        if($row0 = mysqli_fetch_array($getQuarterDetails)){
                                        while($row = mysqli_fetch_array($getQuarterDetails1)){
                                            $temp = explode(' ',$row[1]);

                                            $output.= "

                                            <tr>
                                                <td><small>".$temp[0]."</small></td>
                                                <td><small>".$row[3]."</small></td>
                                                <td><small>".$row[4]."</small></td>
                                                <td><small>".$row[2]."</small></td>
                                                ";
                                                if($remarks == 'Closed'){

                                                }else
                                                {
                                                    $output.= "<td><button type='submit' name='edit' id='".$row[0]."' data-toggle='tooltip' data-placement='top' title='Edit' class='btn btn-default btn-xs edit_quarter'><i class='zmdi zmdi-edit'></i></button></td>";
                                                }
                                                $output.= "

                                            </tr>

                                            ";
                                        }}
                                        else{

                                        }
                    $output.= "          </tbody>
                                </table>
                            </div>
                            <div class='block-header'>
                                    <h4><i class='zmdi zmdi-calendar'></i> <b>MONTHS</b></h4>
                                <div class='actions'>";
                                    if($remarks != 'Closed')
                                    {
                                       $output.= "<button href='#modalMonth' data-toggle='modal' class='btn btn-default btn-sm addMonthButton'><i class='zmdi zmdi-plus'></i> <b> MONTH</b></button>";
                                    }

                                $output.= "</div>
                            </div>
                            
                            <div class='card table-responsive'>

                                <table class='table table-bordered table-nowrap dataTable'>
                                    <thead>
                                        <th width='20%'><b>Month</b></th>
                                        <th width='30%'><b>Total Days</b></th>
                                        <th width='30%'><b>Status</b></th>";
                                        if($remarks == 'Open')
                                        {
                                            $output.= "<th width='30%'><b>Edit</b></th>";
                                        }

                                    $output.= "
                                    </thead>
                                    <tbody>";
                                        $getMonths = mysqli_query($con, "SELECT * from month where sy_id='".$id."'");
                                        while ($item = mysqli_fetch_array($getMonths)) {
                                            $output.="
                                            <tr>
                                                <td>".$item[1]."</td>
                                                <td>".$item[2]."</td>
                                                <td>".$item[3]."</td>
                                                ";
                                                if($remarks == 'Closed'){

                                                }else
                                                {
                                                    $output.= "<td><button type='submit' name='edit' id='".$item[0]."' data-toggle='tooltip' data-placement='top' title='Edit' class='btn btn-default btn-xs edit_month'><i class='zmdi zmdi-edit'></i></button></td>";
                                                }
                                            $output.="
                                                
                                            </tr>

                                            ";
                                        }
                    $output.= "     </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            ";

        }

        echo $output;
?>
