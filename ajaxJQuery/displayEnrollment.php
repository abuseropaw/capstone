<div class="row">
    <div class="col-sm-3">
        <div class='card'>
            <div class="card-header">
                <h4>Transferred In</h4>
                <ul class="actions">
                    <div class="btn-group">
                        <button type="button" class="btn btn-danger btn-xs waves-effect"><b><i class='zmdi zmdi-collection-pdf'></i> PRINT</b></button>
                        <button type="button" class="btn btn-danger btn-xs dropdown-toggle waves-effect" data-toggle="dropdown" aria-expanded="false">
                            <span class="caret"></span>
                            <span class="sr-only"></span>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="enrollmentReport.php?sy=<?php echo $sy_id?>&level=Grade 7&remarks=T/I" target="_blank">Grade 7</a></li>
                            <li><a href="enrollmentReport.php?sy=<?php echo $sy_id?>&level=Grade 8&remarks=T/I" target="_blank">Grade 8</a></li>
                            <li><a href="enrollmentReport.php?sy=<?php echo $sy_id?>&level=Grade 9&remarks=T/I" target="_blank">Grade 9</a></li>
                            <li><a href="enrollmentReport.php?sy=<?php echo $sy_id?>&level=Grade 10&remarks=T/I" target="_blank">Grade 10</a></li>
                        </ul>
                    </div>
                </ul>
            </div>                                  
            <div class="card-body card-padding lcb-form">
                <div>
                <ul>
                <?php
                    $getTI = mysqli_query($con, "SELECT * from controlinformation where sy_id='".$sy_id."' and control_remarks='T/I'");
                    while ($row = mysqli_fetch_array($getTI)) {
                        echo '
                        <li>
                            <b>'.$row['student_lname'].', '.$row['student_fname'].', '.$row['student_mname'].'</b> - '.$row['gradelevel'].'    
                        </li>';
                    }

                ?>
                </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-2">
        <div class='card'>
            <div class="card-header">
                <h4>Transferred Out</h4>
                <ul class="actions">
                    <div class="btn-group">
                        <button type="button" class="btn btn-danger btn-xs waves-effect"><b><i class='zmdi zmdi-collection-pdf'></i> PRINT</b></button>
                        <button type="button" class="btn btn-danger btn-xs dropdown-toggle waves-effect" data-toggle="dropdown" aria-expanded="false">
                            <span class="caret"></span>
                            <span class="sr-only"></span>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="enrollmentReport.php?sy=<?php echo $sy_id?>&level=Grade 7&remarks=T/O" target="_blank">Grade 7</a></li>
                            <li><a href="enrollmentReport.php?sy=<?php echo $sy_id?>&level=Grade 8&remarks=T/O" target="_blank">Grade 8</a></li>
                            <li><a href="enrollmentReport.php?sy=<?php echo $sy_id?>&level=Grade 9&remarks=T/O" target="_blank">Grade 9</a></li>
                            <li><a href="enrollmentReport.php?sy=<?php echo $sy_id?>&level=Grade 10&remarks=T/O" target="_blank">Grade 10</a></li>
                        </ul>
                    </div>
                </ul>
            </div>                                  
            <div class="card-body card-padding lcb-form">
                <div>
                <ul>
                <?php
                    $getTI = mysqli_query($con, "SELECT * from controlinformation where sy_id='".$sy_id."' and control_remarks='T/O'");
                    while ($row = mysqli_fetch_array($getTI)) {
                        echo '
                        <li>
                            <b>'.$row['student_lname'].', '.$row['student_fname'].', '.$row['student_mname'].'</b> - '.$row['gradelevel'].'    
                        </li>';
                    }

                ?>
                </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-2">
        <div class='card'>
            <div class="card-header">
                <h4>Dropout</h4>
                <ul class="actions">
                    <div class="btn-group">
                        <button type="button" class="btn btn-danger btn-xs waves-effect"><b><i class='zmdi zmdi-collection-pdf'></i> PRINT</b></button>
                        <button type="button" class="btn btn-danger btn-xs dropdown-toggle waves-effect" data-toggle="dropdown" aria-expanded="false">
                            <span class="caret"></span>
                            <span class="sr-only"></span>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="enrollmentReport.php?sy=<?php echo $sy_id?>&level=Grade 7&remarks=DRP" target="_blank">Grade 7</a></li>
                            <li><a href="enrollmentReport.php?sy=<?php echo $sy_id?>&level=Grade 8&remarks=DRP" target="_blank">Grade 8</a></li>
                            <li><a href="enrollmentReport.php?sy=<?php echo $sy_id?>&level=Grade 9&remarks=DRP" target="_blank">Grade 9</a></li>
                            <li><a href="enrollmentReport.php?sy=<?php echo $sy_id?>&level=Grade 10&remarks=DRP" target="_blank">Grade 10</a></li>
                        </ul>
                    </div>
                </ul>
            </div>                                  
            <div class="card-body card-padding lcb-form">
                <div>
                <ul>
                <?php
                    $getTI = mysqli_query($con, "SELECT * from controlinformation where sy_id='".$sy_id."' and control_remarks='DRP'");
                    while ($row = mysqli_fetch_array($getTI)) {
                        echo '
                        <li>
                            <b>'.$row['student_lname'].', '.$row['student_fname'].', '.$row['student_mname'].'</b> - '.$row['gradelevel'].'    
                        </li>';
                    }

                ?>
                </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-3">
        <div class='card'>
            <div class="card-header">
                <h4>Late Enrollment</h4>

                <ul class="actions">
                    <div class="btn-group">
                        <button type="button" class="btn btn-danger btn-xs waves-effect"><b><i class='zmdi zmdi-collection-pdf'></i> PRINT</b></button>
                        <button type="button" class="btn btn-danger btn-xs dropdown-toggle waves-effect" data-toggle="dropdown" aria-expanded="false">
                            <span class="caret"></span>
                            <span class="sr-only"></span>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="enrollmentReport.php?sy=<?php echo $sy_id?>&level=Grade 7&remarks=LE" target="_blank">Grade 7</a></li>
                            <li><a href="enrollmentReport.php?sy=<?php echo $sy_id?>&level=Grade 8&remarks=LE" target="_blank">Grade 8</a></li>
                            <li><a href="enrollmentReport.php?sy=<?php echo $sy_id?>&level=Grade 9&remarks=LE" target="_blank">Grade 9</a></li>
                            <li><a href="enrollmentReport.php?sy=<?php echo $sy_id?>&level=Grade 10&remarks=LE" target="_blank">Grade 10</a></li>
                        </ul>
                    </div>
                </ul>
                 
            </div>                                  
            <div class="card-body card-padding lcb-form">
                <div>
                <ul>
                <?php
                    $getTI = mysqli_query($con, "SELECT * from controlinformation where sy_id='".$sy_id."' and control_remarks='LE'");
                    while ($row = mysqli_fetch_array($getTI)) {
                        echo '
                        <li>
                            <b>'.$row['student_lname'].', '.$row['student_fname'].', '.$row['student_mname'].'</b> - '.$row['gradelevel'].'    
                        </li>';
                    }

                ?>
                </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-2">
        <div class='card'>
            <div class="card-header">
                <h4>Peding T.I.</h4>
                <ul class="actions">
                    <div class="btn-group">
                        <button type="button" class="btn btn-danger btn-xs waves-effect"><b><i class='zmdi zmdi-collection-pdf'></i> PRINT</b></button>
                        <button type="button" class="btn btn-danger btn-xs dropdown-toggle waves-effect" data-toggle="dropdown" aria-expanded="false">
                            <span class="caret"></span>
                            <span class="sr-only"></span>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="enrollmentReport.php?sy=<?php echo $sy_id?>&level=Grade 7&remarks=Pending" target="_blank">Grade 7</a></li>
                            <li><a href="enrollmentReport.php?sy=<?php echo $sy_id?>&level=Grade 8&remarks=Pending" target="_blank">Grade 8</a></li>
                            <li><a href="enrollmentReport.php?sy=<?php echo $sy_id?>&level=Grade 9&remarks=Pending" target="_blank">Grade 9</a></li>
                            <li><a href="enrollmentReport.php?sy=<?php echo $sy_id?>&level=Grade 10&remarks=Pending" target="_blank">Grade 10</a></li>
                        </ul>
                    </div>
                </ul>
            </div>                                  
            <div class="card-body card-padding lcb-form">
                <div>
                <ul>
                <?php
                    $getTI = mysqli_query($con, "SELECT * from controlinformation where sy_id='".$sy_id."' and control_remarks='Pending TI'");
                    while ($row = mysqli_fetch_array($getTI)) {
                        echo '
                        <li>
                            <b>'.$row['student_lname'].', '.$row['student_fname'].', '.$row['student_mname'].'</b> - '.$row['gradelevel'].'    
                        </li>';
                    }

                ?>
                </ul>
                </div>
            </div>
        </div>
    </div>
</div>