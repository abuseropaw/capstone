<?php
	session_start();
	
	include_once 'dbconnect.php';
	//STUDENT INFORMATION
	$output='';
	$student_LRN = mysqli_real_escape_string($con, $_POST['student_LRN']);
	$student_fname = mysqli_real_escape_string($con, $_POST['student_fname']);
	$student_mname = mysqli_real_escape_string($con, $_POST['student_mname']);
	$student_lname = mysqli_real_escape_string($con, $_POST['student_lname']);
	$student_sex = mysqli_real_escape_string($con, $_POST['student_gender']);
	$student_birthDate = mysqli_real_escape_string($con, $_POST['student_birthDate']);
	$student_motherTongue = mysqli_real_escape_string($con, $_POST['student_motherTongue']);
	$student_IP = mysqli_real_escape_string($con, $_POST['student_IP']);
	$student_religion = mysqli_real_escape_string($con, $_POST['student_religion']);
	$student_addressHSSP = mysqli_real_escape_string($con, $_POST['student_addressHSSP']);
	$student_addressBarangay = mysqli_real_escape_string($con, $_POST['student_addressBarangay']);
	$student_addressMunicipality = mysqli_real_escape_string($con, $_POST['student_addressMunicipality']);
	$student_addressProvince = mysqli_real_escape_string($con, $_POST['student_addressProvince']);
	$student_age = mysqli_real_escape_string($con, $_POST['student_age']);
	//FAMILY BACKGROUND
	$parent_Ffname = mysqli_real_escape_string($con, $_POST['parent_Ffname']);
	$parent_Fmname = mysqli_real_escape_string($con, $_POST['parent_Fmname']);
	$parent_Flname = mysqli_real_escape_string($con, $_POST['parent_Flname']);

	$parent_Mfname = mysqli_real_escape_string($con, $_POST['parent_Mfname']);
	$parent_Mmname = mysqli_real_escape_string($con, $_POST['parent_Mmname']);
	$parent_Mlname = mysqli_real_escape_string($con, $_POST['parent_Mlname']);
	$parent_phone = mysqli_real_escape_string($con, $_POST['parent_phone']);

	//GUARDIAN BACKGROUND
	$guardian_name = mysqli_real_escape_string($con, $_POST['guardian_name']);
	$guardian_phone = mysqli_real_escape_string($con, $_POST['guardian_Phone']);
	$guardian_relationship = mysqli_real_escape_string($con, $_POST['guardian_relationship']);


	$numberofStudent = mysqli_num_rows(mysqli_query($con, "SELECT * from student_account"));
	$getMaxID = mysqli_fetch_row(mysqli_query($con, "SELECT max(student_id) from student_account"));
	$maxID = $getMaxID[0];

	$exploder = explode('/', $student_birthDate);
	$tempDate = "$exploder[2]-$exploder[1]-$exploder[0]";
	$newbirthDate = date('Y-m-d', strtotime($tempDate));
	$id = mysqli_real_escape_string($con, $_POST['section_id']);


	function getSY($con)
    {
        $q = mysqli_query($con, "SELECT sy_id from schoolyear where sy_remarks='Open'");
        $temp = mysqli_fetch_row($q);
        $temp1 = $temp[0];
        return $temp1;
    }
	function getControlID($con)
    {
        $q = mysqli_query($con, "SELECT max(control_id) from control");
        $temp = mysqli_fetch_row($q);
        if($temp[0] == null){
            $ID = 50000000000;
        }
        else
        {
            $ID = $temp[0] + 1;
        }
        
        return $ID;
    }
    function getParentID($con)
    {
        $q = mysqli_query($con, "SELECT max(parent_id) from parent");
        $temp = mysqli_fetch_row($q);
        if($temp[0] == null){
            $ID = 2000000000;
        }
        else
        {
            $ID = $temp[0] + 1;
        }
        
        return $ID;
    }

    function getAddressID($con)
    {
        $q = mysqli_query($con, "SELECT max(address_id) from address");
        $temp = mysqli_fetch_row($q);
        if($temp[0] == null){
            $ID = 3000000000;
        }
        else
        {
            $ID = $temp[0] + 1;
        }
        
        return $ID;
    }
    function month($con)
    {
        $result = mysqli_fetch_row(mysqli_query($con, "SELECT month(NOW())"));
        return $result[0];
    }
    function monthDesc($con)
    {
        $result = mysqli_fetch_row(mysqli_query($con, "SELECT monthname(NOW())"));
        return $result[0];
    }

	//KUNG GI ENROLL
	
		if($numberofStudent < 1){
			
			$getsyID = mysqli_fetch_row(mysqli_query($con, "SELECT sy_id from schoolyear where sy_remarks='Open'"));
			$syID = $getsyID[0];
			
			// $queryStudent = "CALL insertStudent('".$student_LRN."','".$student_LRN."','".$student_fname."','".$student_mname."','".$student_lname."','".$student_sex."','".$student_birthDate."','".$student_age."','".$student_motherTongue."','".$student_IP."','".$student_religion."',null,'".$student_LRN."',NOW(),'".$_SESSION['faculty_id']."')";
 
			$queryStudent = "CALL insertStudent('".$student_LRN."','".$student_LRN."','".$student_fname."','".$student_mname."','".$student_lname."','".$student_sex."','".$newbirthDate."','".$student_age."','".$student_motherTongue."','".$student_IP."','".$student_religion."',null,'".$student_LRN."',NOW(),'".$_SESSION['faculty_id']."')";


			
			$queryParent = "CALL insertParent('".$student_LRN."','".$parent_Ffname."','".$parent_Fmname."','".$parent_Flname."','".$parent_Mfname."','".$parent_Mmname."','".$parent_Mlname."','".$parent_phone."','".$guardian_name."','".$guardian_phone."','".$guardian_relationship."',NOW(),'".$_SESSION['faculty_id']."','".$syID."')";


			$queryAddress = "CALL insertAddress('".$student_LRN."','".$student_addressHSSP."','".$student_addressBarangay."','".$student_addressMunicipality."','".$student_addressProvince."',NOW(),'".$_SESSION['faculty_id']."','".$syID."')";

			$CID = $student_LRN ."-". $syID;
			$queryControl = "CALL insertControl('".$CID."','T/I',NOW(),NOW(),'".$_SESSION['faculty_id']."','".$student_LRN."','".$id."','".$syID."',null)";
			if(mysqli_query($con, $queryStudent)){
				if(mysqli_query($con, $queryParent)){}
				if(mysqli_query($con, $queryAddress)){}
				
				if($_POST['section_id'] != ''){
					if(mysqli_query($con, $queryControl)){
						if(mysqli_query($con, "INSERT into remarks_history values('".month($con)."','".$CID."','T/I','',NOW(),'".monthDesc($con)."')")){}
						if($_SESSION['faculty_type'] == 'Teacher')
						{
							include_once '../teacherAdvisoryDetails_Learners.php';
						}
						else
						{
							include_once 'displayregistrarStudentDetails.php';
						}
					}
				}
			}
			else
			{
				$output.='1';echo $output;
			}

		}else{
			
			
			$getsyID = mysqli_fetch_row(mysqli_query($con, "SELECT sy_id from schoolyear where sy_remarks='Open'"));
			$syID = $getsyID[0];

			$queryStudent = "CALL insertStudent('".$student_LRN."','".$student_LRN."','".$student_fname."','".$student_mname."','".$student_lname."','".$student_sex."','".$newbirthDate."','".$student_age."','".$student_motherTongue."','".$student_IP."','".$student_religion."',null,'".$student_LRN."',NOW(),'".$_SESSION['faculty_id']."')";
				//INSERT ADDRESS, PARENT and GUARDIAN
			$queryParent = "CALL insertParent('".$student_LRN."','".$parent_Ffname."','".$parent_Fmname."','".$parent_Flname."','".$parent_Mfname."','".$parent_Mmname."','".$parent_Mlname."','".$parent_phone."','".$guardian_name."','".$guardian_phone."','".$guardian_relationship."',NOW(),'".$_SESSION['faculty_id']."','".$syID."')";

			$queryAddress = "CALL insertAddress('".$student_LRN."','".$student_addressHSSP."','".$student_addressBarangay."','".$student_addressMunicipality."','".$student_addressProvince."',NOW(),'".$_SESSION['faculty_id']."','".$syID."')";

			$CID = $student_LRN ."-". $syID;
			$queryControl = "CALL insertControl('".$CID."','T/I',NOW(),NOW(),'".$_SESSION['faculty_id']."','".$student_LRN."','".$id."','".$syID."',null)";

			if(mysqli_query($con, $queryStudent)){
				if(mysqli_query($con, $queryParent)){}
				if(mysqli_query($con, $queryAddress)){}
				
				if($_POST['section_id'] != ''){
					if(mysqli_query($con, $queryControl)){
						if(mysqli_query($con, "INSERT into remarks_history values('".month($con)."','".$CID."','T/I','',NOW(),'".monthDesc($con)."')")){}
						if($_SESSION['faculty_type'] == 'Teacher')
						{
							include_once '../teacherAdvisoryDetails_Learners.php';
						}
						else
						{
							include_once 'displayregistrarStudentDetails.php';
						}
					}
				}
			}
			else
			{
				$output.='0';echo $output;
			}	
		}
	
	
?>