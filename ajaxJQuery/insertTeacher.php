<?php

	session_start();
	include_once 'dbconnect.php';
	
	//GET the values in my form
	$fname = mysqli_real_escape_string($con, $_POST['faculty_fname']);
	$mname = mysqli_real_escape_string($con, $_POST['faculty_mname']);
	$lname = mysqli_real_escape_string($con, $_POST['faculty_lname']);
	$phone = mysqli_real_escape_string($con, $_POST['faculty_phone']);

	$hssp = mysqli_real_escape_string($con, $_POST['faculty_addressHSSP']);
	$barangay = mysqli_real_escape_string($con, $_POST['faculty_addressBarangay']);
	$municipality = mysqli_real_escape_string($con, $_POST['faculty_addressMunicipality']);
	$province = mysqli_real_escape_string($con, $_POST['faculty_addressProvince']);
	$religion = mysqli_real_escape_string($con, $_POST['faculty_religion']);
	$major = mysqli_real_escape_string($con, $_POST['faculty_major']);
	$minor = mysqli_real_escape_string($con, $_POST['faculty_minor']);
	$dob = mysqli_real_escape_string($con, $_POST['faculty_dob']);
	$gender = mysqli_real_escape_string($con, $_POST['faculty_gender']);
	$type = mysqli_real_escape_string($con, $_POST['faculty_type']);

	$address = $hssp .' '. $barangay .' '. $municipality .' '. $province;
	$picture = null;

	$numberofTeacher = mysqli_num_rows(mysqli_query($con, "SELECT * from faculty_account"));

	//$numberofTeacher1 = mysqli_num_rows(mysqli_query($con, "CALL selectTeacher()"));

	////$numberofTeacher2 = mysqli_fetch_row(mysqli_query($con, "CALL maxTeacher()"));
	//$maxID = $numberofTeacher2[0];
	$defaultID = 1000;


		$tempNumberofTeacher = "SELECT * from faculty_account";
		$tempNumberofTeacher1 = mysqli_query($con, $tempNumberofTeacher);


		if(mysqli_num_rows($tempNumberofTeacher1) < 1){
			$query = "CALL insertTeacher('1000','".$fname."','".$mname."','".$lname."','".$phone."','".$address."','".$religion."','".$major."','".$minor."','".$dob."','".$gender."','".$type."',null, '1000', NOW(), '".$_SESSION['faculty_id']."','Active')";
		}else{
			$maxID = mysqli_fetch_row(mysqli_query($con, "SELECT max(faculty_id) as id from faculty_account"));
			$maxID1 = $maxID[0] + 1;

			
			$query = "CALL insertTeacher('".$maxID1."','".$fname."','".$mname."','".$lname."','".$phone."','".$address."','".$religion."','".$major."','".$minor."','".$dob."','".$gender."','".$type."',null, '".$maxID1."', NOW(), '".$_SESSION['faculty_id']."','Active')";
		}
	if($fname != '' && $mname != '')
	{

		if(mysqli_query($con, $query)){
			include_once 'displayTeacher.php';
		}
	}
	else
	{
		echo "0";
	}



?>