<?php
		include_once 'dbconnect.php';
		$output = '';
	$SY = mysqli_real_escape_string($con, $_GET['sys']);
	$query = mysqli_query($con, "SELECT total, round((totalMale/total) * 100), round((totalFemale/total) * 100) from TotalLearners where sy_year='".$SY."'");

    $query1 = mysqli_query($con, "SELECT sum(Total) from TotalLearners where sy_year='".$SY."'");
    $temp1 = mysqli_fetch_row($query1);

    $TotalforSchoolYear = $temp1[0];






    $i = 1;

    $total[1] = 0;
    $male[1] = 0;
    $female[1] = 0;

    $total[2] =0;
    $male[2] = 0;
    $female[2] = 0;

    $total[3] = 0;
    $male[3] = 0;
    $female[3] = 0;
    $total[4] = 0;
    $male[4] = 0;
    $female[4] = 0;


    while($row = mysqli_fetch_array($query))
    {
        $total[$i] = $row[0];
        $male[$i] = $row[1];
        $female[$i] = $row[2];
        $i++;
    }

    if(isset($_POST['sys'])){
    	$output .='

    	<div class="mini-charts">
            <div class="row">
                <div class="col-sm-6 col-md-3">
                    <div class="mini-charts-item bgm-lightgreen">
                        <div class="clearfix">
                            <div class="chart stats-bar"></div>
                            <div class="count">
                                <small>Learners Grade 7</small>
                                <h2>'.$total[1].'</h2>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-3">
                    <div class="mini-charts-item bgm-yellow">
                        <div class="clearfix">
                            <div class="chart stats-bar-2"></div>
                            <div class="count">
                                <small>Learners Grade 8</small>
                                <h2>'.$total[2].'</h2>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-3">
                    <div class="mini-charts-item bgm-red">
                        <div class="clearfix">
                            <div class="chart stats-line"></div>
                            <div class="count">
                                <small>Learners Grade 9</small>
                                <h2>'.$total[3].'</h2>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-3">
                    <div class="mini-charts-item bgm-blue">
                        <div class="clearfix">
                            <div class="chart stats-line-2"></div>
                            <div class="count">
                                <small>Learners Grade 10</small>
                                <h2>'.$total[4].'</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="dash-widgets">
            <div class="row">
                
                <div class="col-md-3 col-sm-6">

                    <div id="pie-charts" class="dw-item bgm-lightgreen c-white">
                        
                        <div class="dw-item">
                        
                            <div class="clearfix"></div>

                            <div class="text-center p-20 m-t-25">
                                <div class="easy-pie main-pie" data-percent="'.(($total[1]/$TotalforSchoolYear) * 100).'">
                                    <div class="percent">'.(($total[1]/$TotalforSchoolYear) * 100).'</div>
                                    <div class="pie-title">Grade 7</div>
                                </div>
                            </div>

                            <div class="p-t-25 p-b-20 text-center">
                                <div class="easy-pie sub-pie-1" data-percent="'.$male[1].'">
                                    <div class="percent">'.$male[1].'</div>
                                    <div class="pie-title">Total Male</div>
                                </div>
                                <div class="easy-pie sub-pie-2" data-percent="'.$female[1].'">
                                    <div class="percent">'.$female[1].'</div>
                                    <div class="pie-title">Total Female</div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="col-md-3 col-sm-6">

                    <div id="pie-charts" class="dw-item bgm-yellow c-white">
                        
                        <div class="dw-item">
                        
                            <div class="clearfix"></div>

                            <div class="text-center p-20 m-t-25">
                                <div class="easy-pie main-pie" data-percent="'.(($total[2]/$TotalforSchoolYear) * 100).'">
                                    <div class="percent">'.(($total[2]/$TotalforSchoolYear) * 100).'</div>
                                    <div class="pie-title">Grade 7</div>
                                </div>
                            </div>

                            <div class="p-t-25 p-b-20 text-center">
                                <div class="easy-pie sub-pie-1" data-percent="'.$male[2].'">
                                    <div class="percent">'.$male[2].'</div>
                                    <div class="pie-title">Total Male</div>
                                </div>
                                <div class="easy-pie sub-pie-2" data-percent="'.$female[2].'">
                                    <div class="percent">'.$female[2].'</div>
                                    <div class="pie-title">Total Female</div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-md-3 col-sm-6">

                    <div id="pie-charts" class="dw-item bgm-red c-white">
                        
                        <div class="dw-item">
                        
                            <div class="clearfix"></div>

                            <div class="text-center p-20 m-t-25">
                                <div class="easy-pie main-pie" data-percent="'.(($total[3]/$TotalforSchoolYear) * 100).'">
                                    <div class="percent">'.(($total[3]/$TotalforSchoolYear) * 100).'</div>
                                    <div class="pie-title">Grade 7</div>
                                </div>
                            </div>

                            <div class="p-t-25 p-b-20 text-center">
                                <div class="easy-pie sub-pie-1" data-percent="'.$male[3].'">
                                    <div class="percent">'.$male[3].'</div>
                                    <div class="pie-title">Total Male</div>
                                </div>
                                <div class="easy-pie sub-pie-2" data-percent="'.$female[3].'">
                                    <div class="percent">'.$female[3].'</div>
                                    <div class="pie-title">Total Female</div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-md-3 col-sm-6">

                    <div id="pie-charts" class="dw-item bgm-blue c-white">
                        
                        <div class="dw-item">
                        
                            <div class="clearfix"></div>

                            <div class="text-center p-20 m-t-25">
                                <div class="easy-pie main-pie" data-percent="'.(($total[4]/$TotalforSchoolYear) * 100).'">
                                    <div class="percent">'.(($total[4]/$TotalforSchoolYear) * 100).'</div>
                                    <div class="pie-title">Grade 7</div>
                                </div>
                            </div>

                            <div class="p-t-25 p-b-20 text-center">
                                <div class="easy-pie sub-pie-1" data-percent="'.$male[4].'">
                                    <div class="percent">'.$female[4].'</div>
                                    <div class="pie-title">Total Male</div>
                                </div>
                                <div class="easy-pie sub-pie-2" data-percent="'.$female[4].'">
                                    <div class="percent">'.$female[4].'</div>
                                    <div class="pie-title">Total Female</div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>';
    }
    echo $output;
?>