<?php
	session_start();
	include_once 'dbconnect.php';


	$id = mysqli_real_escape_string($con, $_POST['section']);
	$sectionId = mysqli_real_escape_string($con, $_POST['section_id']);


	$query = mysqli_query($con, "SELECT * from control where section_id='".$id."'");
	if(empty($id)){
		echo "0";
	}
	else
	{

        $another_result="";
		while($row = mysqli_fetch_array($query))
		{
			$student_id = $row['student_id'];
			$remarks = $row['control_remarks'];
			$tmp = explode(' ', $remarks);
			$allowed = array('DRP','T/O','Retained');
			if(!in_array($tmp[0], $allowed))
			{
				$maxControl = getMaxControlID($con);
				$maxSY = getCurSY($con);
				if(check_if_enrolled($con, $student_id,$maxSY)){
					$another_result="naa";
				}
				else{
					if(checkifCanbeEnroll($con,$student_id,$sectionId))
                    {
						$queryControl = "CALL insertControl('".$student_id.'-'.$maxSY."','',NOW(),NOW(),'".$_SESSION['faculty_id']."','".$student_id."','".$sectionId."','".$maxSY."',null)";

						$maxSY--;
						$getAddress = mysqli_query($con, "SELECT * FROM ADDRESS where student_id='".$student_id."' and sy_id='".$maxSY."'");
						$tempAddress = mysqli_fetch_row($getAddress);

						$getParent = mysqli_query($con, "SELECT * FROM PARENT where student_id='".$student_id."' and sy_id='".$maxSY."'");
						$tempParent = mysqli_fetch_row($getParent);

						if(mysqli_query($con, $queryControl))
						{
							if(mysqli_query($con, "INSERT into remarks_history values('".month($con)."','".$student_id.'-'.$maxSY."','','',NOW(),'".monthDesc($con)."')")){}
							$maxSY++;
							if(mysqli_query($con, "CALL insertParent('".$student_id."','".$tempParent[1]."','".$tempParent[2]."','".$tempParent[3]."','".$tempParent[4]."','".$tempParent[5]."','".$tempParent[6]."','".$tempParent[7]."','".$tempParent[8]."','".$tempParent[9]."','".$tempParent[10]."',NOW(),'".$_SESSION['faculty_id']."','".$maxSY."')")){

					        }
					        if(mysqli_query($con, "CALL insertAddress('".$student_id."','".$tempAddress[1]."','".$tempAddress[2]."','".$tempAddress[3]."','".$tempAddress[4]."',NOW(),'".$_SESSION['faculty_id']."','".$maxSY."')")){

					        }

						}
					}
					else
					{
						
					}
				}

			}
		}
		if(empty($another_result))
		{
			include_once 'displayregistrarStudentDetails.php';
		}
		else
		{
			echo $another_result;
		}
		
	}
	function checkifCanbeEnroll($con, $lrn,$sectionID)
    {
        $count = 0;
        $level = getSectionLevel($con, $sectionID);//SECTION LEVEL
        if($row = mysqli_fetch_array(mysqli_query($con, "SELECT max(control_id) from controlinformation where student_LRN='".$lrn."' order by control_id ASC")))
        {
            $id = $row[0];
            if($row1 = mysqli_fetch_array(mysqli_query($con, "SELECT * from controlinformation where control_id ='".$id."'")))
            {
                $sectionID = $row1['section_id'];

                $countSubjectOffer = mysqli_num_rows(mysqli_query($con, "SELECT * from subjectoffering where section_id='".$sectionID."'"));
                $getAverage = mysqli_fetch_row(mysqli_query($con, "SELECT average from average where control_id='".$id."'"));
                $countAverage = mysqli_num_rows(mysqli_query($con, "SELECT * from averagepersubjectoffering where control_id='".$id."'"));

                $query = mysqli_query($con, "SELECT * from averagepersubjectoffering where control_id='".$id."'");



                    $t = explode(' ', $row1['control_remarks']);
                    $level1 = getLevelAble($con, $level);
                    if($row1['gradelevel'] == $level)// kung ang result kay pareha ug level sa section
                    {
                        if(($t[0] == 'DRP') | $t[0] == 'RETAINED'){
                            return true;
                        }else
                        {
                            if($countAverage == $countSubjectOffer){
                                
                                while($row = mysqli_fetch_array($query)){
                                    if($row[0] < 75)
                                    {
                                        $count++;
                                    }
                                }
                                if($count > 2)
                                {
                                    return true;
                                }
                                else if($count <= 2 && $count !=0)
                                {
                                    if($countSubjectOffer <= $count)
                                    {
                                        return true;
                                    }
                                    else
                                    {
                                        return false;
                                    }
                                    
                                }
                                else if($count == 0)
                                {
                                    
                                    if($getAverage[0] < 75)
                                    {
                                        return true;
                                    }
                                    else
                                    {
                                        return false;
                                    }
                                }
                            }
                            else{
                                return true;
                            }
                        }
                    }

                    else if(($row1['gradelevel'] == $level1))
                    {
                        if(($t[0] == 'DRP') | $t[0] == 'RETAINED'){
                            return false;
                        }else
                        {
                            if($countAverage == $countSubjectOffer){
                                
                                while($row = mysqli_fetch_array($query)){
                                    if($row[0] < 75)
                                    {
                                        $count++;
                                    }
                                }
                                if($count > 2)
                                {
                                    return false;
                                }
                                else if($count <= 2 && $count !=0)
                                {
                                    if($countSubjectOffer <= $count)
                                    {
                                        return false;
                                    }
                                    else
                                    {
                                        return false;
                                    }
                                    
                                }
                                else if($count == 0)
                                {
                                    
                                    if($getAverage[0] < 75)
                                    {
                                        return false;
                                    }
                                    else
                                    {
                                        return true;
                                    }
                                }
                            }
                            else{
                                return false;
                            }
                        }
                    }
                    else
                    {
                        return false;
                    }
                
            }
        }
    }
    function getLevelAble($con, $level)
    {
        $tmp = mysqli_fetch_row(mysqli_query($con, "SELECT year_lvl_id from yearlevel where year_lvl_title='".$level."'"));
        $id = $tmp[0];
        $id--;
        $tmp = mysqli_fetch_row(mysqli_query($con, "SELECT year_lvl_title from yearlevel where year_lvl_id='".$id."'"));
        $level = $tmp[0];
        return $level;
    }
    function getSectionLevel($con, $sectionID)
    {
        $tmp = mysqli_fetch_row(mysqli_query($con, "SELECT Gradelevel from classinformation where section_id='".$sectionID."'"));
        $level = $tmp[0];
        return $level;
    }


	function getParentID($con)
    {
        $q = mysqli_query($con, "SELECT max(parent_id) from parent");
        $temp = mysqli_fetch_row($q);
        if($temp[0] == null){
            $ID = 2000000000;
        }
        else
        {
            $ID = $temp[0] + 1;
        }
        
        return $ID;
    }

    function getAddressID($con)
    {
        $q = mysqli_query($con, "SELECT max(address_id) from address");
        $temp = mysqli_fetch_row($q);
        if($temp[0] == null){
            $ID = 3000000000;
        }
        else
        {
            $ID = $temp[0] + 1;
        }
        
        return $ID;
    }
	function check_if_enrolled($con,$existStudentID,$SY)
    {
        $q = mysqli_num_rows(mysqli_query($con, "SELECT * from control where student_id='".$existStudentID."' and sy_id='".$SY."'"));

        if($q == 1){

            return true;
        }
        else
        {
            return false;
        }
    }
    


	function getMaxControlID($con)
	{
		$query = mysqli_fetch_row(mysqli_query($con, "SELECT max(control_id)+1 from control"));
		return $query[0];
	}
	function getCurSY($con)
	{
		$query = mysqli_fetch_row(mysqli_query($con, "SELECT sy_id from schoolyear where sy_remarks ='Open'"));
		return $query[0];
	}
	function month($con)
    {
        $result = mysqli_fetch_row(mysqli_query($con, "SELECT month(NOW())"));
        return $result[0];
    }
    function monthDesc($con)
    {
        $result = mysqli_fetch_row(mysqli_query($con, "SELECT monthname(NOW())"));
        return $result[0];
    }
?>