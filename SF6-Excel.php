<?php

// Include PHPExcel library and create its object
    session_start();
    include_once 'dbconnect.php';
    require_once "PHPExcel-1.8.1/Classes/PHPExcel.php";


    $sy = mysqli_real_escape_string($con, $_GET['id']);

    $getSY = mysqli_fetch_row(mysqli_query($con, "SELECT * from schoolyear where sy_id='".$sy."'"));
    $sy_year = $getSY[1];
    $phpExcel = PHPExcel_IOFactory::load('School Forms Final/School Form 6.xlsx');
    $sheet = $phpExcel ->getActiveSheet();

   	$sheet ->setCellValue('V7', $sy_year);

   	$totalmaleRetained = 0;
    $totalfemaleRetained = 0;
    $totalfemalePromoted = 0;
    $totalfemaleIrregular = 0;
    $totalmalePromoted = 0;
    $totalmaleIrregular = 0;

    function summaryTable($con,$level,$i,$sheet,$sy)
    {
    	$maleRetained = 0;
        $femaleRetained = 0;
        $femalePromoted = 0;
        $femaleIrregular = 0;
        $malePromoted = 0;
        $maleIrregular = 0;

    	$getLearnersM = mysqli_query($con, "SELECT * from controlinformation where gradelevel='".$level."' and Sex='M' and sy_id='".$sy."'");
		$getLearnersF = mysqli_query($con, "SELECT * from controlinformation where gradelevel='".$level."' and Sex='F' and sy_id='".$sy."'");
		$countM = mysqli_num_rows($getLearnersM);
		$countF = mysqli_num_rows($getLearnersF);
		if($countM == 0)
		{
			// $sheet ->setCellValue('C'.$i.'', 0);
			// $sheet ->setCellValue('C'.($i+1).'', 0);
			// $sheet ->setCellValue('C'.($i+2).'', 0);
		}
		else
		{
			while ($row = mysqli_fetch_array($getLearnersM)) {
				$controlId = $row['control_id'];
				$section_id = $row['section_id'];
				$getAverageDetailsM = mysqli_fetch_row(mysqli_query($con, "SELECT average from averageDetails where control_id='".$controlId."'"));

				// if($getAverageDetailsM[2] == '')
				// {
				// 	$maleIrregular++;
				// }
				// else 
				if($row[1] == 'DRP')
                {
                    $GLOBALS['totalmaleRetained']++;
                    $maleRetained++;
                }
                else if($getAverageDetailsM[0] == '')
                {
                	//$maleIrregular++;
                }
                else
                {
	                //$ans = checkLearnersProficiency($con,$id,$controlId);
	                if(checkLearnersProficiency($con,$section_id,$controlId))
	                {
	                    $maleIrregular++;
	                    $GLOBALS['totalmaleIrregular']++;
	                }
	                else if(!checkLearnersProficiency($con,$section_id,$controlId))
	                {
	                	if($getAverageDetailsM[0] < 75)
	                	{
	                		$GLOBALS['totalmaleRetained']++;
	                		$maleRetained++;
	                	}
	                	else
	                	{
	                		$GLOBALS['totalmalePromoted']++;
	                    	$malePromoted++;
	                    }
	                }
	            }   
			}
		}
		//////////////////////////////////////////////////////////
		if($countF == 0)
		{
			// $sheet ->setCellValue('D'.$i.'', 0);
			// $sheet ->setCellValue('D'.($i+1).'', 0);
			// $sheet ->setCellValue('D'.($i+2).'', 0);
		}
		else
		{
			while ($row = mysqli_fetch_array($getLearnersF)) {
				$controlId = $row['control_id'];
				$section_id = $row['section_id'];
				$getAverageDetails = mysqli_fetch_row(mysqli_query($con, "SELECT * from averageDetails where control_id='".$controlId."'"));

				// if($getAverageDetails[2] == '')
				// {
				// 	$femaleIrregular++;
				// }
				// else
				 if(($row[1] == 'DRP'))
                {
                    $GLOBALS['totalfemaleRetained']++;
                    $femaleRetained++;
                }
                else if($getAverageDetails[2] == '')
                {
                	//$femaleIrregular++;
                }
                else
                {
	                //$ans = checkLearnersProficiency($con,$id,$controlId);
	                if(checkLearnersProficiency($con,$section_id,$controlId))
	                {
	                	$GLOBALS['totalfemaleIrregular']++;
	                    $femaleIrregular++;
	                    
	                }
	                else if(!checkLearnersProficiency($con,$section_id,$controlId))
	                {
	                	if($getAverageDetails[2] < 75)
	                	{
	                		$GLOBALS['totalfemaleRetained']++;
	                		$femaleRetained++;
	                	}
	                	else
	                	{
	                		$GLOBALS['totalfemalePromoted']++;
	                    	$femalePromoted++;
	                    }
	                }
	            }   
			}
		}
		if(($countF + $countM) == 0)
		{
			// $sheet ->setCellValue('E'.$i.'', 0);
			// $sheet ->setCellValue('E'.($i+1).'', 0);
			// $sheet ->setCellValue('E'.($i+2).'', 0);
		}
		switch ($level) {
			case 'Grade 7':
					$sheet ->setCellValue('C'.$i.'', $malePromoted);	$sheet ->setCellValue('D'.$i.'', $femalePromoted);	$sheet ->setCellValue('E'.$i.'', ($malePromoted + $femalePromoted));
					$sheet ->setCellValue('C'.($i+1).'', $maleIrregular);	$sheet ->setCellValue('D'.($i+1).'', $femaleIrregular);	$sheet ->setCellValue('E'.($i+1).'', ($maleIrregular + $femaleIrregular));
					$sheet ->setCellValue('C'.($i+2).'', $maleRetained);	$sheet ->setCellValue('D'.($i+2).'', $femaleRetained);	$sheet ->setCellValue('E'.($i+2).'', ($maleRetained + $femaleRetained));
				break;
			case 'Grade 8':
					$sheet ->setCellValue('F'.$i.'', $malePromoted);	$sheet ->setCellValue('G'.$i.'', $femalePromoted);	$sheet ->setCellValue('H'.$i.'', ($malePromoted + $femalePromoted));
					$sheet ->setCellValue('F'.($i+1).'', $maleIrregular);	$sheet ->setCellValue('G'.($i+1).'', $femaleIrregular);	$sheet ->setCellValue('H'.($i+1).'', ($maleIrregular + $femaleIrregular));
					$sheet ->setCellValue('F'.($i+2).'', $maleRetained);	$sheet ->setCellValue('G'.($i+2).'', $femaleRetained);	$sheet ->setCellValue('H'.($i+2).'', ($maleRetained + $femaleRetained));
				break;
			case 'Grade 9':
					$sheet ->setCellValue('I'.$i.'', $malePromoted);	$sheet ->setCellValue('J'.$i.'', $femalePromoted);	$sheet ->setCellValue('K'.$i.'', ($malePromoted + $femalePromoted));
					$sheet ->setCellValue('I'.($i+1).'', $maleIrregular);	$sheet ->setCellValue('J'.($i+1).'', $femaleIrregular);	$sheet ->setCellValue('K'.($i+1).'', ($maleIrregular + $femaleIrregular));
					$sheet ->setCellValue('I'.($i+2).'', $maleRetained);	$sheet ->setCellValue('J'.($i+2).'', $femaleRetained);	$sheet ->setCellValue('K'.($i+2).'', ($maleRetained + $femaleRetained));
				break;
			case 'Grade 10':
					$sheet ->setCellValue('L'.$i.'', $malePromoted);	$sheet ->setCellValue('M'.$i.'', $femalePromoted);	$sheet ->setCellValue('N'.$i.'', ($malePromoted + $femalePromoted));
					$sheet ->setCellValue('L'.($i+1).'', $maleIrregular);	$sheet ->setCellValue('M'.($i+1).'', $femaleIrregular);	$sheet ->setCellValue('N'.($i+1).'', ($maleIrregular + $femaleIrregular));
					$sheet ->setCellValue('L'.($i+2).'', $maleRetained);	$sheet ->setCellValue('M'.($i+2).'', $femaleRetained);	$sheet ->setCellValue('N'.($i+2).'', ($maleRetained + $femaleRetained));
				break;
			default:
				# code...
				break;
		}
		


    }
    function checkLearnersProficiency($con,$id,$controlId)
    {
        
        $countSubjectOffer = mysqli_num_rows(mysqli_query($con, "SELECT * from subjectoffering where section_id='".$id."'"));
        $countAverage = mysqli_num_rows(mysqli_query($con, "SELECT * from averagepersubjectoffering where control_id='".$controlId."'"));
        $query = mysqli_query($con, "SELECT * from averagepersubjectoffering where control_id='".$controlId."'");

        if($countAverage == $countSubjectOffer){
            while($row = mysqli_fetch_array($query)){
                if($row[0] < 75)
                {
                    return true;
                }
            }
           
        }else{
            return true;
        }
        

    }

    function levelofproficiency($con,$level,$iter,$sheet,$sy_year)
    {
    	
		$getsf5remarks = mysqli_query($con, "SELECT * from sf5remarksperlevel where sy_id='".$sy_year."' and gradelevel='".$level."'");
		$countgetsf5remarks = mysqli_num_rows($getsf5remarks);
		if($countgetsf5remarks == 0)
		{
			switch ($level) {
				case 'Grade 7':
					$sheet ->setCellValue('C'.$iter.'', 0);	$sheet ->setCellValue('D'.$iter.'', 0);	$sheet ->setCellValue('E'.$iter.'', 0);
					$sheet ->setCellValue('C'.($iter+2).'', 0);	$sheet ->setCellValue('D'.($iter+2).'', 0);	$sheet ->setCellValue('E'.($iter+2).'', 0);
					$sheet ->setCellValue('C'.($iter+4).'', 0);	$sheet ->setCellValue('D'.($iter+4).'', 0);	$sheet ->setCellValue('E'.($iter+4).'', 0);
					$sheet ->setCellValue('C'.($iter+7).'', 0);	$sheet ->setCellValue('D'.($iter+7).'', 0);	$sheet ->setCellValue('E'.($iter+7).'', 0);
					$sheet ->setCellValue('C'.($iter+9).'', 0);	$sheet ->setCellValue('D'.($iter+9).'', 0);	$sheet ->setCellValue('E'.($iter+9).'', 0);
					$sheet ->setCellValue('C'.($iter+11).'', 0);	$sheet ->setCellValue('D'.($iter+11).'', 0);	$sheet ->setCellValue('E'.($iter+11).'', 0);
					break;
				case 'Grade 8':
					$sheet ->setCellValue('F'.$iter.'', 0);	$sheet ->setCellValue('G'.$iter.'', 0);	$sheet ->setCellValue('H'.$iter.'', 0);
					$sheet ->setCellValue('F'.($iter+2).'', 0);	$sheet ->setCellValue('G'.($iter+2).'', 0);	$sheet ->setCellValue('H'.($iter+2).'', 0);
					$sheet ->setCellValue('F'.($iter+4).'', 0);	$sheet ->setCellValue('G'.($iter+4).'', 0);	$sheet ->setCellValue('H'.($iter+4).'', 0);
					$sheet ->setCellValue('F'.($iter+7).'', 0);	$sheet ->setCellValue('G'.($iter+7).'', 0);	$sheet ->setCellValue('H'.($iter+7).'', 0);
					$sheet ->setCellValue('F'.($iter+9).'', 0);	$sheet ->setCellValue('G'.($iter+9).'', 0);	$sheet ->setCellValue('H'.($iter+9).'', 0);
					$sheet ->setCellValue('F'.($iter+11).'', 0);	$sheet ->setCellValue('G'.($iter+11).'', 0);	$sheet ->setCellValue('H'.($iter+11).'', 0);
					break;
				case 'Grade 9':
					$sheet ->setCellValue('I'.$iter.'', 0);	$sheet ->setCellValue('J'.$iter.'', 0);	$sheet ->setCellValue('K'.$iter.'', 0);
					$sheet ->setCellValue('I'.($iter+2).'', 0);	$sheet ->setCellValue('J'.($iter+2).'', 0);	$sheet ->setCellValue('K'.($iter+2).'', 0);
					$sheet ->setCellValue('I'.($iter+4).'', 0);	$sheet ->setCellValue('J'.($iter+4).'', 0);	$sheet ->setCellValue('K'.($iter+4).'', 0);
					$sheet ->setCellValue('I'.($iter+7).'', 0);	$sheet ->setCellValue('J'.($iter+7).'', 0);	$sheet ->setCellValue('K'.($iter+7).'', 0);
					$sheet ->setCellValue('I'.($iter+9).'', 0);	$sheet ->setCellValue('J'.($iter+9).'', 0);	$sheet ->setCellValue('K'.($iter+9).'', 0);
					$sheet ->setCellValue('I'.($iter+11).'', 0);	$sheet ->setCellValue('J'.($iter+11).'', 0);	$sheet ->setCellValue('K'.($iter+11).'', 0);
					break;
				case 'Grade 10':
					$sheet ->setCellValue('L'.$iter.'', 0);	$sheet ->setCellValue('M'.$iter.'', 0);	$sheet ->setCellValue('N'.$iter.'', 0);
					$sheet ->setCellValue('L'.($iter+2).'', 0);	$sheet ->setCellValue('M'.($iter+2).'', 0);	$sheet ->setCellValue('N'.($iter+2).'', 0);
					$sheet ->setCellValue('L'.($iter+4).'', 0);	$sheet ->setCellValue('M'.($iter+4).'', 0);	$sheet ->setCellValue('N'.($iter+4).'', 0);
					$sheet ->setCellValue('L'.($iter+7).'', 0);	$sheet ->setCellValue('M'.($iter+7).'', 0);	$sheet ->setCellValue('N'.($iter+7).'', 0);
					$sheet ->setCellValue('L'.($iter+9).'', 0);	$sheet ->setCellValue('M'.($iter+9).'', 0);	$sheet ->setCellValue('N'.($iter+9).'', 0);
					$sheet ->setCellValue('L'.($iter+11).'', 0);	$sheet ->setCellValue('M'.($iter+11).'', 0);	$sheet ->setCellValue('N'.($iter+11).'', 0);
					break;
				
				default:
					# code...
					break;
			}
		}
		else
		{
			switch ($level) {
				case 'Grade 7':
				$item = mysqli_fetch_row($getsf5remarks);
				$sheet ->setCellValue('C'.$iter.'', $item[2]);	$sheet ->setCellValue('D'.$iter.'', $item[3]);	$sheet ->setCellValue('E'.$iter.'', ($item[2] + $item[3]));
				$sheet ->setCellValue('C'.($iter+2).'', $item[4]);	$sheet ->setCellValue('D'.($iter+2).'', $item[5]);	$sheet ->setCellValue('E'.($iter+2).'', ($item[4] + $item[5]));

				$sheet ->setCellValue('C'.($iter+4).'', $item[6]);	$sheet ->setCellValue('D'.($iter+4).'', $item[7]);	$sheet ->setCellValue('E'.($iter+4).'', ($item[6]+$item[7]));
				
				$sheet ->setCellValue('C'.($iter+7).'', $item[8]);	$sheet ->setCellValue('D'.($iter+7).'', $item[9]);	$sheet ->setCellValue('E'.($iter+7).'', ($item[8]+$item[9]));
				$sheet ->setCellValue('C'.($iter+9).'', $item[10]);	$sheet ->setCellValue('D'.($iter+9).'', $item[11]);	$sheet ->setCellValue('E'.($iter+9).'', ($item[10]+$item[11]));
				$sheet ->setCellValue('C'.($iter+11).'', ($item[2]+$item[4]+$item[6]+$item[8]+$item[10]));	$sheet ->setCellValue('D'.($iter+11).'', ($item[3]+$item[5]+$item[7]+$item[9]+$item[11]));	$sheet ->setCellValue('E'.($iter+11).'', (($item[2]+$item[4]+$item[6]+$item[8]+$item[10])+ ($item[3]+$item[5]+$item[7]+$item[9]+$item[11])));
				break;
				case 'Grade 8':
				$item = mysqli_fetch_row($getsf5remarks);
				$sheet ->setCellValue('F'.$iter.'', $item[2]);	$sheet ->setCellValue('G'.$iter.'', $item[3]);	$sheet ->setCellValue('H'.$iter.'', ($item[2] + $item[3]));
				$sheet ->setCellValue('F'.($iter+2).'', $item[4]);	$sheet ->setCellValue('G'.($iter+2).'', $item[5]);	$sheet ->setCellValue('H'.($iter+2).'', ($item[4] + $item[5]));

				$sheet ->setCellValue('F'.($iter+4).'', $item[6]);	$sheet ->setCellValue('G'.($iter+4).'', $item[7]);	$sheet ->setCellValue('H'.($iter+4).'', ($item[6]+$item[7]));
				
				$sheet ->setCellValue('F'.($iter+7).'', $item[8]);	$sheet ->setCellValue('G'.($iter+7).'', $item[9]);	$sheet ->setCellValue('H'.($iter+7).'', ($item[8]+$item[9]));
				$sheet ->setCellValue('F'.($iter+9).'', $item[10]);	$sheet ->setCellValue('G'.($iter+9).'', $item[11]);	$sheet ->setCellValue('H'.($iter+9).'', ($item[10]+$item[11]));
				$sheet ->setCellValue('F'.($iter+11).'', ($item[2]+$item[4]+$item[6]+$item[8]+$item[10]));	$sheet ->setCellValue('G'.($iter+11).'', ($item[3]+$item[5]+$item[7]+$item[9]+$item[11]));	$sheet ->setCellValue('H'.($iter+11).'', (($item[2]+$item[4]+$item[6]+$item[8]+$item[10])+ ($item[3]+$item[5]+$item[7]+$item[9]+$item[11])));
					break;
				case 'Grade 9':
				$item = mysqli_fetch_row($getsf5remarks);
				$sheet ->setCellValue('I'.$iter.'', $item[2]);	$sheet ->setCellValue('J'.$iter.'', $item[3]);	$sheet ->setCellValue('K'.$iter.'', ($item[2] + $item[3]));
				$sheet ->setCellValue('I'.($iter+2).'', $item[4]);	$sheet ->setCellValue('J'.($iter+2).'', $item[5]);	$sheet ->setCellValue('K'.($iter+2).'', ($item[4] + $item[5]));

				$sheet ->setCellValue('I'.($iter+4).'', $item[6]);	$sheet ->setCellValue('J'.($iter+4).'', $item[7]);	$sheet ->setCellValue('K'.($iter+4).'', ($item[6]+$item[7]));
				
				$sheet ->setCellValue('I'.($iter+7).'', $item[8]);	$sheet ->setCellValue('J'.($iter+7).'', $item[9]);	$sheet ->setCellValue('K'.($iter+7).'', ($item[8]+$item[9]));
				$sheet ->setCellValue('I'.($iter+9).'', $item[10]);	$sheet ->setCellValue('J'.($iter+9).'', $item[11]);	$sheet ->setCellValue('K'.($iter+9).'', ($item[10]+$item[11]));
				$sheet ->setCellValue('I'.($iter+11).'', ($item[2]+$item[4]+$item[6]+$item[8]+$item[10]));	$sheet ->setCellValue('J'.($iter+11).'', ($item[3]+$item[5]+$item[7]+$item[9]+$item[11]));	$sheet ->setCellValue('K'.($iter+11).'', (($item[2]+$item[4]+$item[6]+$item[8]+$item[10])+ ($item[3]+$item[5]+$item[7]+$item[9]+$item[11])));
					break;
				case 'Grade 10':
				$item = mysqli_fetch_row($getsf5remarks);
				$sheet ->setCellValue('L'.$iter.'', $item[2]);	$sheet ->setCellValue('M'.$iter.'', $item[3]);	$sheet ->setCellValue('N'.$iter.'', ($item[2] + $item[3]));
				$sheet ->setCellValue('L'.($iter+2).'', $item[4]);	$sheet ->setCellValue('M'.($iter+2).'', $item[5]);	$sheet ->setCellValue('N'.($iter+2).'', ($item[4] + $item[5]));

				$sheet ->setCellValue('L'.($iter+4).'', $item[6]);	$sheet ->setCellValue('M'.($iter+4).'', $item[7]);	$sheet ->setCellValue('N'.($iter+4).'', ($item[6]+$item[7]));
				
				$sheet ->setCellValue('L'.($iter+7).'', $item[8]);	$sheet ->setCellValue('M'.($iter+7).'', $item[9]);	$sheet ->setCellValue('N'.($iter+7).'', ($item[8]+$item[9]));
				$sheet ->setCellValue('L'.($iter+9).'', $item[10]);	$sheet ->setCellValue('M'.($iter+9).'', $item[11]);	$sheet ->setCellValue('N'.($iter+9).'', ($item[10]+$item[11]));
				$sheet ->setCellValue('L'.($iter+11).'', ($item[2]+$item[4]+$item[6]+$item[8]+$item[10]));	$sheet ->setCellValue('M'.($iter+11).'', ($item[3]+$item[5]+$item[7]+$item[9]+$item[11]));	$sheet ->setCellValue('N'.($iter+11).'', (($item[2]+$item[4]+$item[6]+$item[8]+$item[10])+ ($item[3]+$item[5]+$item[7]+$item[9]+$item[11])));
					break;

				default:
	    			# code...
	    			break;
	    	}
		}
    			
    		
    		
    	
    }

    function setTotalSummaryTable($sheet,$totalmaleRetained,$totalfemaleRetained,$totalfemalePromoted,$totalfemaleIrregular,$totalmalePromoted,$totalmaleIrregular)
    {
    	$sheet ->setCellValue('U11', $totalmalePromoted);	$sheet ->setCellValue('V11', $totalfemalePromoted);	$sheet ->setCellValue('W11', ($totalmalePromoted + $totalfemalePromoted));
    	$sheet ->setCellValue('U12', $totalmaleIrregular);	$sheet ->setCellValue('V12', $totalfemaleIrregular);	$sheet ->setCellValue('W12', ($totalmaleIrregular + $totalfemaleIrregular));
    	$sheet ->setCellValue('U13', $totalmaleRetained);	$sheet ->setCellValue('V13', $totalfemaleRetained);	$sheet ->setCellValue('W13', ($totalmaleRetained + $totalfemaleRetained));
    }

    function setTotalLevelProficiency($con,$sheet)
    {
    	$i = mysqli_fetch_row(mysqli_query($con,"SELECT sum(MBEGINNING) as MBEGINNING,sum(FBEGINNING) as FBEGINNING,sum(MDEVELOPING) as MDEVELOPING,sum(FDEVELOPING) as FDEVELOPING,sum(MAPPROACHING) as MAPPROACHING,sum(FAPPROACHING) as FAPPROACHING,sum(MPROFICIENT) as MPROFICIENT,sum(FPROFICIENT) as FPROFICIENT,sum(MADVANCED) as MADVANCED,sum(FADVANCED) as FADVANCED from sf5remarksperlevel group by sy_id"));

    	$sheet ->setCellValue('U15', ($i[0] == 0)? 0:$i[0]);	$sheet ->setCellValue('V15', ($i[1] == 0)? 0:$i[1]);	$sheet ->setCellValue('W15', ($i[0] + $i[1]));
			$sheet ->setCellValue('U17', ($i[2] == 0)? 0:$i[2]);	$sheet ->setCellValue('V17', ($i[3] == 0)? 0:$i[3]);	$sheet ->setCellValue('W17', ($i[2] + $i[3]));

			$sheet ->setCellValue('U19', ($i[4] == 0)? 0:$i[4]);	$sheet ->setCellValue('V19', ($i[5] == 0)? 0:$i[5]);	$sheet ->setCellValue('W19', ($i[4]+$i[5]));
			
			$sheet ->setCellValue('U22', ($i[6] == 0)? 0:$i[6]);	$sheet ->setCellValue('V22', ($i[7] == 0)? 0:$i[7]);	$sheet ->setCellValue('W22', ($i[6]+$i[7]));
			$sheet ->setCellValue('U24', ($i[8] == 0)? 0:$i[8]);	$sheet ->setCellValue('V24', ($i[9] == 0)? 0:$i[9]);	$sheet ->setCellValue('W24', ($i[8]+$i[9]));

			$sheet ->setCellValue('U26', ($i[0]+$i[2]+$i[4]+$i[6]+$i[8]));	$sheet ->setCellValue('V26', ($i[1]+$i[3]+$i[5]+$i[7]+$i[9]));	$sheet ->setCellValue('W26', ($i[0]+$i[2]+$i[4]+$i[6]+$i[8])+($i[1]+$i[3]+$i[5]+$i[7]+$i[9]));
    }
    function setPrincipal($con,$sheet)
    {
        $query = mysqli_query($con, "SELECT * from facultysmalldetail where faculty_type='Principal'");
        if(mysqli_num_rows($query) == 0)
        {
            $sheet ->setCellValue('C30', '');
        }
        else
        {
            $temp = mysqli_fetch_row($query);
            $sheet ->setCellValue('C30', $temp[1]);
        }
    }

    for ($i=1; $i < 5; $i++) { 
    	
    	switch ($i) {
    		case 1:
    			$iter = 11;
    			$level = 'Grade 7';
    			summaryTable($con,$level,$iter,$sheet,$sy);
    			$iters = 15;
    			levelofproficiency($con,$level,$iters,$sheet,$sy_year);

    			break;
    		case 2:
    			$iter = 11;
    			$level = 'Grade 8';
    			summaryTable($con,$level,$iter,$sheet,$sy);
    			$iters = 15;
    			levelofproficiency($con,$level,$iters,$sheet,$sy_year);
    			break;

    		case 3:
    			$iter = 11;
    			$level = 'Grade 9';
    			summaryTable($con,$level,$iter,$sheet,$sy);
    			$iters = 15;
    			levelofproficiency($con,$level,$iters,$sheet,$sy_year);
    			break;
    		case 4:
    			$iter = 11;
    			$level = 'Grade 10';
    			summaryTable($con,$level,$iter,$sheet,$sy);
    			$iters = 15;
    			levelofproficiency($con,$level,$iters,$sheet,$sy_year);
    			break
    			;

    		default:
    			
    			break;
    	}
    

    }

    setTotalSummaryTable($sheet,$totalmaleRetained,$totalfemaleRetained,$totalfemalePromoted,$totalfemaleIrregular,$totalmalePromoted,$totalmaleIrregular);
    setTotalLevelProficiency($con,$sheet);
    setPrincipal($con,$sheet);

    $writer = PHPExcel_IOFactory::createWriter($phpExcel, "Excel2007");
    //
    // $writer->save('C:/Users/MLD/Documents/School Form 1-'.$sectionName.'-'.$SY.'.xlsx'); 


    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="SF6 - '.$sy_year.'.xlsx"');
    header('Cache-Control: max-age=0');
    $writer->save('php://output'); 

?>