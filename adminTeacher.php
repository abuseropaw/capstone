<?php
    include_once 'sessionAdmin.php';
    include_once 'dbconnect.php';
?>
<!DOCTYPE html>
    <!-- HEAD -->
    <?php include_once 'head.php'; ?> 
    <!-- HEAD   -->
    <body>
        <!-- HEADER -->
        <?php include_once 'header.php'; ?>
        <!-- HEADER -->

        <section id="main">
            <ol class="breadcrumb">
                <li><a href="adminHome.php">Home</a></li>
                <li class="active">Teachers</li>
            </ol>
            <?php 
                $toggle = 'adminTeacher';
                include_once 'sidebar.php'; 
            ?>
            <section id="content" >
                
                <div class="container" >

                    <div class="block-header">
                        <h1><i class="zmdi zmdi-accounts"></i> Teacher
                        </h1>

                        <div class="actions">
                            <button  href="#modalColor" data-toggle="modal" class="btn btn-default btn-lg"><i class="zmdi zmdi-accounts-add"></i><b> New Teacher</b></button>
                        </div>
                        
                        <div class="modal fade" id="modalColor" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <form role="form" method="post" id="insert_form" action="insertTeacher.php" data-toggle="md-validator">
                                        <div class="modal-header">
                                            <h1 class="modal-title"><b>TEACHER ACCOUNT</b></h1>
                                        </div>
                                        
                                        <div class="modal-body">
                                            
                                            <div class='col-sm-3'>
                                            
                                                <div class='form-group fg-float'>
                                                    <div class="fg-line">
                                                        <input type='text' name='faculty_fname' id='faculty_fname' class='form-control fg-input' required>
                                                        <label class="fg-label">First name </label>
                                                    </div>
                                                </div>
                                            </div>
                                           
                                            <div class='col-sm-3'>
                                                <div class='form-group fg-float'>
                                                    <div class="fg-line">
                                                        <input type='text' name='faculty_mname' id='faculty_mname' class='form-control fg-input'>
                                                        <label class="fg-label">Middle name </label>
                                                    </div>
                                                </div>
                                            </div>
                                           
                                            <div class='col-sm-3'>
                                                <div class='form-group fg-float'>
                                                    <div class="fg-line">
                                                        <input type='text' name='faculty_lname' id='faculty_lname' class='form-control fg-input' required>
                                                        <label class="fg-label">Last name </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                 <div class='form-group fg-float'>
                                                    <div class="fg-line">
                                                    <select class="selectpicker form-control" data-live-search="true" name='faculty_type' id='faculty_type' required>
                                                        <option value="">Type</option>
                                                        <option>Teacher</option>
                                                        <option>Registrar</option>
                                                        <option>Head Teacher</option>
                                                        <option>Principal</option>
                                                        <option>Administrator</option>
                                                    </select>
                                                </div>
                                                </div>
                                            </div>
                                            <br>
                                            <br />
                                            <br />
                                <div class="panel panel-collapse">
                                    <div class="panel-heading" role="tab" id="headingOne">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"
                                               aria-expanded="false" aria-controls="collapseOne">
                                                <b>More Information</b> (Optional)
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseOne" class="collapse out" role="tabpanel"
                                         aria-labelledby="headingOne">
                                        <div class="panel-body">
                                                
                                                <div class="col-sm-3">
                                                    
                                                    <div class='form-group fg-float'>
                                                        <div class="fg-line">
                                                            <input type='text' name='faculty_phone' id='faculty_phone' class='form-control fg-input'>
                                                            <label class="fg-label">Contact #</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="col-sm-3">
                                            
                                                    <div class='form-group fg-float'>
                                                        <div class="fg-line">
                                                            <input type='text' name='faculty_dob' id='faculty_dob' class='form-control fg-input date-picker'>
                                                            <label class="fg-label">Date of Birth</label>
                                                        </div>
                                                    </div>
                                                </div>
                                               
                                                <div class="col-sm-3">
                                                    
                                                    <div class='form-group fg-float'>
                                                        <div class="fg-line">
                                                            <input type='text' name='faculty_addressHSSP' id='faculty_addressHSSP' class='form-control fg-input'>
                                                            <label class="fg-label">House #/ Street/ Purok</label>
                                                        </div>
                                                    </div>
                                                </div>
                                               
                                                <div class="col-sm-3">
                                                
                                                    <div class='form-group fg-float'>
                                                        <div class="fg-line">
                                                            <input type='text' name='faculty_addressBarangay' id='faculty_addressBarangay' class='form-control fg-input'>
                                                            <label class="fg-label">Barangay</label>
                                                        </div>
                                                    </div>
                                                </div><br />
                                                <div class="col-sm-3">
                                                    <div class="form-group fg-line">
                                                        <select class="selectpicker form-control" data-live-search="true" name='faculty_addressMunicipality' id='faculty_addressMunicipality'>
                                                            <option value=''>Municipality</option>
                                                            <option>Malaybalay</option>
                                                            <option>Valencia</option>
                                                            <option>Baungon</option>
                                                            <option>Cabanglasan</option>
                                                            <option>Damulog</option>
                                                            <option>Dangcagan</option>
                                                            <option>Don Carlos</option>
                                                            <option>Impasug-ong</option>
                                                            <option>Kadingilan</option>
                                                            <option>Kibawe</option>
                                                            <option>Kitaotao</option>
                                                            <option>Lantapan</option>
                                                            <option>Malitbog</option>
                                                            <option>Manolo Fortich</option>
                                                            <option>Maramag</option>
                                                            <option>Pangantucan</option>
                                                            <option>Quezon</option>
                                                            <option>San Fernando</option>
                                                            <option>Sumilao</option>
                                                            <option>Talakag</option>
                                                            <option>Catarman</option>
                                                            <option>Guinsiliban</option>
                                                            <option>Mahinog</option>
                                                            <option>Mambajao</option>
                                                            <option>Sagay</option>
                                                            <option>Bacolod</option>
                                                            <option>Baloi</option>
                                                            <option>Baroy</option>
                                                            <option>Kapatagan</option>
                                                            <option>Kauswagan</option>
                                                            <option>Kolambugan</option>
                                                            <option>Lala</option>
                                                            <option>Linamon</option>
                                                            <option>Magsaysay</option>
                                                            <option>Maigo</option>
                                                            <option>Matungao</option>
                                                            <option>Munai</option>
                                                            <option>Nunungan</option>
                                                            <option>Pantao Ragat</option>
                                                            <option>Pantar</option>
                                                            <option>Poona Piagapo</option>
                                                            <option>Salvador</option>
                                                            <option>Sapad</option>
                                                            <option>Tagoloan</option>
                                                            <option>Tangkal</option>
                                                            <option>Tubod</option>
                                                            <option>Iligan</option>
                                                            <option>Oroquieta</option>
                                                            <option>Ozamiz</option>
                                                            <option>Tangub</option>
                                                            <option>Aloran</option>
                                                            <option>Baliangao</option>
                                                            <option>Bonifacio</option>
                                                            <option>Calamba</option>
                                                            <option>Clarin</option>
                                                            <option>Concepcion</option>
                                                            <option>Don Victoriano</option>
                                                            <option>Jimenez</option>
                                                            <option>Lopez Jaena</option>
                                                            <option>Panaon</option>
                                                            <option>Plaridel</option>
                                                            <option>Sapang Dalaga</option>
                                                            <option>Sinacaban</option>
                                                            <option>Tudela</option>
                                                            <option>El Salvador</option>
                                                            <option>Gingoog</option>
                                                            <option>Alubijid</option>
                                                            <option>Balingasag</option>
                                                            <option>Baligoan</option>
                                                            <option>Claveria</option>
                                                            <option>Gitagum</option>
                                                            <option>Initao</option>
                                                            <option>Jasaan</option>
                                                            <option>Kinoguitan</option>
                                                            <option>Lagonglong</option>
                                                            <option>Laguindingan</option>
                                                            <option>Lugait</option>
                                                            <option>Libertad</option>
                                                            <option>Magsaysay(Linugos)</option>
                                                            <option>Manticao</option>
                                                            <option>Medina</option>
                                                            <option>Naawan</option>
                                                            <option>Opol</option>
                                                            <option>Salay</option>
                                                            <option>Sugbongcogon</option>
                                                            <option>Tagoloan</option>
                                                            <option>Talisayan</option>
                                                            <option>Villanueva</option>
                                                            <option>Cagayan de Oro</option>

                                                        </select>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="col-sm-3">
                                                    <div class="form-group fg-line">
                                                        <select class="selectpicker form-control" data-live-search="true" name='faculty_addressProvince' id='faculty_addressProvince'>
                                                            <option value="">Province</option>
                                                            <option>Bukidnon</option>
                                                            <option>Camiguin</option>
                                                            <option>Lanao Del Norte</option>
                                                            <option>Misamis Oriental</option>
                                                            <option>Misamis Occidental</option>

                                                        </select>
                                                    </div>
                                                </div>
                                                <br>
                                                
                                                <div class="col-sm-3">
                                                    <div class="form-group fg-line">
                                                        <select class="selectpicker form-control" data-live-search="true" name='faculty_religion' id='faculty_religion'>
                                                            <option value="">Religion</option>
                                                            <option>Roman Catholic</option>
                                                            <option>Iglesia ni Cristo</option>
                                                            <option>Seventh Day Adventist</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                
                                                
                                                <div class="col-sm-3">
                                                    <div class="form-group fg-line">
                                                        <select class="selectpicker form-control" data-live-search="true" name='faculty_gender' id='faculty_gender'>
                                                            <option value="">Gender</option>
                                                            <option>Male</option>
                                                            <option>Female</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                
                                                <div class="col-sm-4">
                                                    <div class="form-group fg-line">
                                                        <select class="selectpicker form-control" data-live-search="true" name='faculty_major' id='faculty_major'>
                                                            <option value="">Major Subject</option>
                                                            <option>English</option>
                                                            <option>Filipino</option>
                                                            <option>Science</option>
                                                            <option>TLE</option>
                                                            <option>Mathematics</option>
                                                            <option>Edukasyon sa Pagpapakatao</option>
                                                            <option>MAPEH</option>
                                                            <option>Araling Panlipunan</option>
                                                            <option>Home Economics</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="col-sm-4">
                                                    <div class="form-group fg-line">
                                                        <select class="selectpicker form-control" data-live-search="true" name='faculty_minor' id='faculty_minor'>
                                                            <option value="">Minor Subject</option>
                                                            <option>English</option>
                                                            <option>Filipino</option>
                                                            <option>Science</option>
                                                            <option>TLE</option>
                                                            <option>Mathematics</option>
                                                            <option>Edukasyon sa Pagpapakatao</option>
                                                            <option>MAPEH</option>
                                                            <option>Araling Panlipunan</option>
                                                            <option>Home Economics</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                                           
                                        </div>
                                        <div class="modal-footer">
                                           
                                            <button type="submit" name="insert" id="insert" class="btn btn-success">CREATE ACCOUNT</button>
                                            <button type="button" class="btn btn-warning" data-dismiss="modal">CANCEL
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        
                   

                    <!-- Add button -->
                    
                    </div>
                    <div class="card">
                        <div class="action-header clearfix">

                            <div class="ah-label hidden-xs" data-ma-action="action-header-open"><b>Click here to search for a teacher</b></div>

                            <div class="ah-search" >
                                <input type="text" name="search" id="search" placeholder="Start typing..." class="ahs-input" >

                                <i class="ahs-close" data-ma-action="action-header-close">&times;</i>
                            </div>

                            <ul class="actions">
                                <li>
                                    <a href="#" data-ma-action="action-header-open">
                                        <i class="zmdi zmdi-search"></i>
                                    </a>
                                </li>

                              
                            </ul>
                        </div>

                        <div class="card-body card-padding">

                            <div class="contacts clearfix row" id="resultsaSearch">

                                <?php include_once 'ajaxJQuery/displayTeacher.php' ?>
                                
                            </div>
                        </div>
                    </div>
                </div>
                <button href="#modalColor" data-toggle="modal" class="btn btn-float bgm-green m-btn"><i class="zmdi zmdi-accounts-add"></i></button>
            </section>
        </section>
        
        <div class="modal fade" id="changepic" tabindex="-1" role="dialog" aria-hidden="true"  data-keyboard="false">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <form id='addProfilePic' method="post" enctype='multipart/form-data'>
                        <div class="modal-header">
                            <h1 class="modal-title"><b>PROFILE PICTURE</b></h1>
                        </div>
                        <div class="modal-body">
                            <center>
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-preview thumbnail" data-trigger="fileinput"></div>
                                <div>
                                    <span class="btn btn-info btn-xs btn-file">
                                        <span class="fileinput-new">Select image</span>
                                        
                                        <input type="file" name="file" id="file">
                                    </span>
                                    <button type="submit" name="changeprofilepic" id="changeprofilepic" class="btn btn-success btn-xs">CHANGE PICTURE</button>
                                    
                                </div>
                            </div>
                            </center>
                            <div class="form-group fg-line">
                                <input type='hidden' name="ID" id="ID">
                            </div>
                            
                        </div>
                        
                        
                    </form>
                </div>
            </div>
        </div>
        <!-- FOOTER -->
        <?php include_once 'footer.php' ?>
        <!-- FOOTER -->

        <!-- Javascript Libraries -->
        <?php include_once 'scripts.php'; ?>
        <!-- Javascript Libraries -->
        <script type="text/javascript" src="js/Notification.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $('#data-table-basic').DataTable();
                $('#modalColor').on('shown.bs.modal', function() {
                  $('#faculty_fname').focus();
                });
                $('[data-dismiss=modal]').on('click', function (e) {
                    $('#insert_form')[0].reset();
                });

                $('#addProfilePic').on('submit', function(e){
                    e.preventDefault();
                    $.ajax({
                        url:"ajaxJQuery/addProfilePic.php",
                        method: "post",
                        data: new FormData(this),
                        contentType:false,
                        cache: false,
                        processData:false,
                        success:function(data){
                            if(data == "not allowed")
                            {
                                var message = "Unsupported format for profile";
                                executeNotif(message,"danger");
                            }
                            else
                            {
                                $('#addProfilePic')[0].reset();
                                $('#resultsaSearch').html(data);
                                var message = "Successfully added picture to teacher";
                                executeNotif(message,"success");
                                $('#changepic').modal('hide');
                            }
                        }
                    });
                });

                $(document).on('click', '.change_pic', function(){
                    var tID = $(this).attr("id");
                    $.ajax({
                        url:"ajaxJQuery/searchTeacher.php",
                        method:"POST",
                        data:{tID:tID},
                        success:function(data){
                            $('#ID').val(tID);
                            $('#changepic').modal('show');
                        }

                    });
                });

                //PAG SEARCH
                function search()
                {
                    var txt = $('#search').val();
                    $.ajax({
                        url:"ajaxJQuery/searchTeacher.php",
                        method:"POST",
                        data:{search:txt},
                        dataType:"text",
                        success:function(data)
                        {
                            if(data =='')
                            {
                                $('#resultsaSearch').html(
                                    '<div class="card-body card-padding" id="hehe">'
                                    +'Your search - <b>'+txt+'</b> - did not match any teacher account.'
                                    +'<br />'
                                    +'Suggestions:<br />'
                                    +'<ul style="margin-left:1.3em;margin-bottom:2em"><li>Make sure that all words are spelled correctly.</li><li>Try different keywords.</li><li>Try more general keywords.</li></ul>'
                                +'</div>'
                                    );
                            }
                            else
                            {
                                $('#resultsaSearch').html(data);
                            }
                        }
                    });
                }

                $('#search').keyup(function(){
                    search();
                });
                $('#globalSearch').keyup(function(){
                    var txt = $(this).val();
                    
                    
                        $.ajax({
                            url:"ajaxJQuery/searchTeacher.php",
                            method:"POST",
                            data:{search:txt},
                            dataType:"text",
                            success:function(data)
                            {
                                if(data =='')
                                {
                                    $('#resultsaSearch').html(
                                        '<div class="card-body card-padding" id="hehe">'
                                        +'Your search - <b>'+txt+'</b> - did not match any teacher account.'
                                        +'<br />'
                                        +'Suggestions:<br />'
                                        +'<ul style="margin-left:1.3em;margin-bottom:2em"><li>Make sure that all words are spelled correctly.</li><li>Try different keywords.</li><li>Try more general keywords.</li></ul>'
                                    +'</div>'
                                        );
                                }
                                else
                                {
                                    $('#resultsaSearch').html(data);
                                }
                            }
                        });
                    
                    
                });



                $('#insert_form').on("submit", function(event){
                    event.preventDefault();
                    $('#faculty_fname').focus();
                    if($('#faculty_fname').val() == '')  
                    {
                          alert("First name is required and valid");  
                    }  
                    
                    else if($('#faculty_lname').val() == '')  
                    {
                        $('#faculty_lname').focus();
                         alert("Maximun grade is required");  
                    }
                    
                    else  
                    {  
                         $.ajax({  
                              url:"ajaxJQuery/insertTeacher.php",  
                              method:"POST",  
                              data:$('#insert_form').serialize(),  
                              beforeSend:function(){  
                                   $('#insert1').val("Inserting..");   
                              },  
                              success:function(data){ 
                                     
                                    if(data == 0)
                                    {
                                        var message = "Error on adding teacher account";
                                   
                                        executeNotif(message,"danger");
                                    }
                                    else
                                    {
                                        $('#modalColor').modal('hide');  
                                       $('#resultsaSearch').html(data);
                                       var message = "Successfully added new teacher account "+$('#faculty_fname').val()+" "+$('#faculty_mname').val()+" "+$('#faculty_lname').val()+" ";
                                        executeNotif(message, "success");
                                        $('#insert_form')[0].reset(); 
                                    }
                                   
                              }  

                         });  
                    }
                });
                
            } );
        </script>
    </body>

</html>