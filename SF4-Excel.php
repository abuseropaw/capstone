<?php
    session_start();
    include_once 'dbconnect.php';
    include 'ajaxJQuery/globalFunction.php';
    include 'variables.php';
    require_once "PHPExcel-1.8.1/Classes/PHPExcel.php";

    $id =mysqli_real_escape_string($con, $_GET['id']);
    $totalDays = getMonthTotalDays($con, $id);
    $getMonthDetails = mysqli_fetch_row(mysqli_query($con, "SELECT * from monthdetails where month_id='".$id."'"));
    $month_id = $getMonthDetails[0];
    $month_description = $getMonthDetails[1];
    $month_totalDays = $getMonthDetails[2];
    $month_status = $getMonthDetails[3];
    $sy_id = $getMonthDetails[4];
    $sy_year = $getMonthDetails[5];



    $phpExcel = PHPExcel_IOFactory::load('School Forms Final/School Form 4.xlsx');
    $sheet = $phpExcel ->getActiveSheet();
    $sheet ->setCellValue('Y7', $sy_year);
    $sheet ->setCellValue('AJ7', $month_description);
    
    $startingPoint = 12;

    $startingMonth = 6;


    $getfirstMonth = mysqli_fetch_row(mysqli_query($con, "SELECT min(month_id) as month_id from month where sy_id='".$sy_id."'"));
    $getlastMonth = mysqli_fetch_row(mysqli_query($con, "SELECT max(month_id) as month_id from month where sy_id='".$sy_id."'"));
    

    

    $getSection = mysqli_query($con, "SELECT * from classinformation where sy_id='".$sy_year."' order by section_name ASC");
    $countSection = mysqli_num_rows($getSection);
    $totalDeductedbyOne = $countSection - 2;
    $sheet ->insertNewRowBefore(14,$totalDeductedbyOne);
    $una = $totalDeductedbyOne+$startingPoint+1;
    // for($i = 14; $i<=$una+1; $i++)
    // {
    //     $sheet ->mergeCells('C'.$i.':D'.$i.'');
       
    // }

  

    while ($sectionItem = mysqli_fetch_array($getSection)) {
        $sheet ->setCellValue('A'.$startingPoint.'', $sectionItem['GradeLevel']);
        $sheet ->setCellValue('B'.$startingPoint.'', $sectionItem['section_name']);
        $sheet ->setCellValue('C'.$startingPoint.'', $sectionItem['Adviser']);

        $totalunregisteredMale = 0;
        $totalunregisteredFemale = 0;
        $totalDeductionMale = 0;
        $totalDeductionFemale = 0;
        //REGISTERED LEARNER
            $lateM = mysqli_fetch_row(mysqli_query($con, "SELECT Total from LateEnrollmentduringthemonth where Gender='Male' and section_id='".$sectionItem[0]."' and Month='".$month_description."'"));
            $lateF = mysqli_fetch_row(mysqli_query($con, "SELECT Total from LateEnrollmentduringthemonth where Gender='Female' and section_id='".$sectionItem[0]."' and Month='".$month_description."'"));

            $tiM = mysqli_fetch_row(mysqli_query($con, "SELECT * from TransferredInt where section_id='".$sectionItem[0]."' and Gender='Male' and Month='".$month_description."'"));
            $tiF = mysqli_fetch_row(mysqli_query($con, "SELECT * from TransferredInt where section_id='".$sectionItem[0]."' and Gender='Female' and Month='".$month_description."'"));

            $allMale = mysqli_fetch_row(mysqli_query($con, "SELECT count(*) from remarks_historydetails where Gender='Male' and section_id='".$sectionItem[0]."' and month_desc='June'")); 
            $allFemale =mysqli_fetch_row(mysqli_query($con, "SELECT count(*) from remarks_historydetails where Gender='Female' and section_id='".$sectionItem[0]."' and month_desc='June'"));
            
            for($i = $id; $i >= $getfirstMonth[0]; $i--)
            {
                $getDescriptionforMonth1 = mysqli_fetch_row(mysqli_query($con, "SELECT month_description from month where month_id='".$i."'"));
                $getUnregisterMale = mysqli_fetch_row(mysqli_query($con, "SELECT Total from totalunregistered where section_id='".$sectionItem[0]."' and Gender='Male' and Month='".$getDescriptionforMonth1[0]."'"));
                $getUnregisterFemale = mysqli_fetch_row(mysqli_query($con, "SELECT Total from totalunregistered where section_id='".$sectionItem[0]."' and Gender='Female' and Month='".$getDescriptionforMonth1[0]."'"));

                $totalunregisteredMale = $totalunregisteredMale + $getUnregisterMale[0];
                $totalunregisteredFemale = $totalunregisteredFemale + $getUnregisterFemale[0];
            }
            for($j = ($id +1) ; $j <= $getlastMonth[0]; $j++)
            {
                $getDescriptionforMonth2 = mysqli_fetch_row(mysqli_query($con, "SELECT month_description from month where month_id='".$j."'"));
                $getdeductionMale = mysqli_fetch_row(mysqli_query($con, "SELECT count(*) from controldetails1 where section_id='".$sectionItem[0]."' and Gender='Male' and Month='".$getDescriptionforMonth2[0]."' and control_remarks='T/I' and control_remarks='LE'"));
                $getdeductionFemale = mysqli_fetch_row(mysqli_query($con, "SELECT count(*) from controldetails1 where section_id='".$sectionItem[0]."' and Gender='Female' and Month='".$getDescriptionforMonth2[0]."' and control_remarks='T/I' and control_remarks='LE'"));

                $totalDeductionMale = $totalDeductionMale + $getdeductionMale[0];
                $totalDeductionFemale = $totalDeductionFemale + $getdeductionMale[0];
            }
            if($id != $getfirstMonth[0])
            {
                $totalRegisteredMale = $allMale[0] - $totalunregisteredMale - $totalDeductionMale + $lateM[0] + $tiM[4];
                $totalRegisteredFemale = $allFemale[0] - $totalunregisteredFemale - $totalDeductionFemale + $lateF[0] +$tiF[4];
                $totalRegistered = $totalRegisteredMale + $totalRegisteredFemale;
            }
            else
            {
                $totalRegisteredMale = $allMale[0] - $totalunregisteredMale - $totalDeductionMale;
                $totalRegisteredFemale = $allFemale[0] - $totalunregisteredFemale - $totalDeductionFemale;
                $totalRegistered = $totalRegisteredMale + $totalRegisteredFemale;
            }
            $sheet ->setCellValue('E'.$startingPoint.'', $totalRegisteredMale);
            $sheet ->setCellValue('F'.$startingPoint.'', $totalRegisteredFemale);
            $sheet ->setCellValue('G'.$startingPoint.'', $totalRegistered);
        // REGISTERED LEARNER

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        // ATTENDANCE
            // DAILY ATTENDANCE
                $totalAttendanceMale = mysqli_fetch_row(mysqli_query($con, "SELECT * from TotalDailyAttendance where section_id='".$sectionItem[0]."' and Gender='Male' and month_id='".$id."'"));
                $totalAttendanceFemale = mysqli_fetch_row(mysqli_query($con, "SELECT * from TotalDailyAttendance where section_id='".$sectionItem[0]."' and Gender='Female' and month_id='".$id."'"));

                $averageattendanceMale = $totalAttendanceMale[4] / $totalDays;
                $averageattendanceFemale= $totalAttendanceFemale[4] / $totalDays;
                $totalaverageattendance = $averageattendanceMale + $averageattendanceFemale;
                $sheet ->setCellValue('H'.$startingPoint.'', $averageattendanceMale);
                $sheet ->setCellValue('I'.$startingPoint.'', $averageattendanceFemale);
                $sheet ->setCellValue('J'.$startingPoint.'', $totalaverageattendance);
            // DAILY ATTENDANCE

            // PERCENTAGE FOR THE MONTH
                $percentageattendanceMale = 0;
                $percentageattendanceFemale = 0;
                if($totalRegisteredMale != 0 && $totalRegisteredFemale != 0){
                    $percentageattendanceMale = ($averageattendanceMale / $totalRegisteredMale) * 100;
                    $percentageattendanceFemale = ($averageattendanceFemale / $totalRegisteredFemale) * 100;
                }

                $totalpercentageAttendance = ($percentageattendanceMale + $percentageattendanceFemale) / 2;

                $sheet ->setCellValue('K'.$startingPoint.'', $percentageattendanceMale.'');
                $sheet ->setCellValue('L'.$startingPoint.'', $percentageattendanceFemale.'');
                $sheet ->setCellValue('M'.$startingPoint.'', $totalpercentageAttendance.'');
            // PERCENTAGE FOR THE MONTH


        // ATTENDANCE
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        // DROPPED OUT
            // CUMULATIVE (A)
                $drpcumulativeMale = 0;
                $drpcumulativeFemale = 0;
                for($a = ($id -1); $a >= $getfirstMonth[0]; $a--)
                {
                    $getDescriptionforMonth3 = mysqli_fetch_row(mysqli_query($con, "SELECT month_description from month where month_id='".$a."'"));
                    $drpMale = mysqli_fetch_row(mysqli_query($con, "SELECT * from DropOut where section_id='".$sectionItem[0]."' and Gender='Male' and Month='".$getDescriptionforMonth3[0]."'"));
                    $drpFemale = mysqli_fetch_row(mysqli_query($con, "SELECT * from DropOut where section_id='".$sectionItem[0]."' and Gender='Female' and Month='".$getDescriptionforMonth3[0]."'"));
                    $drpcumulativeMale =$drpcumulativeMale + $drpMale[4];
                    $drpcumulativeFemale = $drpcumulativeFemale + $drpFemale[4];

                }
                $totalcumulative = $drpcumulativeMale + $drpcumulativeFemale;
                $sheet ->setCellValue('N'.$startingPoint.'', ($drpcumulativeMale == '')? 0 : $drpcumulativeMale);
                $sheet ->setCellValue('O'.$startingPoint.'', ($drpcumulativeFemale == '')? 0 : $drpcumulativeFemale);
                $sheet ->setCellValue('P'.$startingPoint.'', ($totalcumulative == '')? 0 : $totalcumulative);

            // CUMULATIVE (A)


            // FOR THE MONTH (B)
                $drpforthemonthMale = mysqli_fetch_row(mysqli_query($con, "SELECT * from DropOut where section_id='".$sectionItem[0]."' and Gender='Male' and Month='".$month_description."'"));
                $drpforthemonthFemale = mysqli_fetch_row(mysqli_query($con, "SELECT * from DropOut where section_id='".$sectionItem[0]."' and Gender='Female' and Month='".$month_description."'"));
                $totaldrpforthemonth = $drpforthemonthMale[4] + $drpforthemonthFemale[4];
                $sheet ->setCellValue('Q'.$startingPoint.'', ($drpforthemonthMale[4] == '')? 0 : $drpforthemonthMale[4]);
                $sheet ->setCellValue('R'.$startingPoint.'', ($drpforthemonthFemale[4] == '')? 0 : $drpforthemonthFemale[4]);
                $sheet ->setCellValue('S'.$startingPoint.'', ($totaldrpforthemonth == '')? 0 : $totaldrpforthemonth);                
            // FOR THE MONTH (B)


            // CUMULATIVE AS OF THE MONTH (A + B)
                $totalCumulativeasoftheMonthMale = $drpcumulativeMale + $drpforthemonthMale[4];
                $totalCumulativeasoftheMonthFemale = $drpcumulativeFemale + $drpforthemonthFemale[4];
                $totalCumulativeasoftheMonth = $totalCumulativeasoftheMonthMale + $totalCumulativeasoftheMonthFemale;

                $sheet ->setCellValue('T'.$startingPoint.'', ($totalCumulativeasoftheMonthMale == '')? 0 : $totalCumulativeasoftheMonthMale);
                $sheet ->setCellValue('U'.$startingPoint.'', ($totalCumulativeasoftheMonthFemale == '')? 0 : $totalCumulativeasoftheMonthFemale);
                $sheet ->setCellValue('V'.$startingPoint.'', ($totalCumulativeasoftheMonth == '')? 0 : $totalCumulativeasoftheMonth);
            // CUMULATIVE AS OF THE MONTH (A + B)

        // DROPPED OUT
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        // TRANSFERED OUT
            // CUMULATIVE (A)
                $tocumulativeMale = 0;
                $tocumulativeFemale = 0;
                for($b = ($id -1); $b >= $getfirstMonth[0]; $b--)
                {
                    $getDescriptionforMonth4 = mysqli_fetch_row(mysqli_query($con, "SELECT month_description from month where month_id='".$b."'"));
                    $toMale = mysqli_fetch_row(mysqli_query($con, "SELECT * from TransferredOut where section_id='".$sectionItem[0]."' and Gender='Male' and Month='".$getDescriptionforMonth4[0]."'"));
                    $toFemale = mysqli_fetch_row(mysqli_query($con, "SELECT * from TransferredOut where section_id='".$sectionItem[0]."' and Gender='Female' and Month='".$getDescriptionforMonth4[0]."'"));
                    $tocumulativeMale =$tocumulativeMale + $toMale[4];
                    $tocumulativeFemale = $tocumulativeFemale + $toFemale[4];

                }
                $totaltocumulative = $tocumulativeMale + $tocumulativeFemale;
                $sheet ->setCellValue('W'.$startingPoint.'', ($tocumulativeMale == '')? 0 : $tocumulativeMale);
                $sheet ->setCellValue('X'.$startingPoint.'', ($tocumulativeFemale == '')? 0 : $tocumulativeFemale);
                $sheet ->setCellValue('Y'.$startingPoint.'', ($totaltocumulative == '')? 0 : $totaltocumulative);
            // CUMULATIVE (A)


            // FOR THE MONTH (B)
                $toforthemonthMale = mysqli_fetch_row(mysqli_query($con, "SELECT * from TransferredOut where section_id='".$sectionItem[0]."' and Gender='Male' and Month='".$month_description."'"));
                $toforthemonthFemale = mysqli_fetch_row(mysqli_query($con, "SELECT * from TransferredOut where section_id='".$sectionItem[0]."' and Gender='Female' and Month='".$month_description."'"));
                $totaltoforthemonth = $toforthemonthMale[4] + $toforthemonthFemale[4];
                $sheet ->setCellValue('Z'.$startingPoint.'', ($toforthemonthMale[4] == '')? 0 : $toforthemonthMale[4]);
                $sheet ->setCellValue('AA'.$startingPoint.'', ($toforthemonthFemale[4] == '')? 0 : $toforthemonthFemale[4]);
                $sheet ->setCellValue('AB'.$startingPoint.'', ($totaltoforthemonth == '')? 0 : $totaltoforthemonth);                
            // FOR THE MONTH (B)


            // CUMULATIVE AS OF THE MONTH (A + B)
                $totaltoCumulativeasoftheMonthMale = $tocumulativeMale + $toforthemonthMale[4];
                $totaltoCumulativeasoftheMonthFemale = $tocumulativeFemale + $toforthemonthFemale[4];
                $totaltoCumulativeasoftheMonth = $totaltoCumulativeasoftheMonthMale + $totaltoCumulativeasoftheMonthFemale;

                $sheet ->setCellValue('AC'.$startingPoint.'', ($totaltoCumulativeasoftheMonthMale == '')? 0 : $totaltoCumulativeasoftheMonthMale);
                $sheet ->setCellValue('AD'.$startingPoint.'', ($totaltoCumulativeasoftheMonthFemale == '')? 0 : $totaltoCumulativeasoftheMonthFemale);
                $sheet ->setCellValue('AE'.$startingPoint.'', ($totaltoCumulativeasoftheMonth == '')? 0 : $totaltoCumulativeasoftheMonth);
            // CUMULATIVE AS OF THE MONTH (A + B)

        // TRANSFERED OUT
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        // TRANSFERED IN
            // CUMULATIVE (A)
                $ticumulativeMale = 0;
                $ticumulativeFemale = 0;
                for($c = ($id -1); $c >= $getfirstMonth[0]; $c--)
                {
                    $getDescriptionforMonth5 = mysqli_fetch_row(mysqli_query($con, "SELECT month_description from month where month_id='".$c."'"));
                    $tiMale = mysqli_fetch_row(mysqli_query($con, "SELECT * from TransferredInt where section_id='".$sectionItem[0]."' and Gender='Male' and Month='".$getDescriptionforMonth5[0]."'"));
                    $tiFemale = mysqli_fetch_row(mysqli_query($con, "SELECT * from TransferredInt where section_id='".$sectionItem[0]."' and Gender='Female' and Month='".$getDescriptionforMonth5[0]."'"));
                    $ticumulativeMale =$ticumulativeMale + $tiMale[4];
                    $ticumulativeFemale = $ticumulativeFemale + $tiFemale[4];

                }
                $titaltocumulative = $ticumulativeMale + $ticumulativeFemale;
                $sheet ->setCellValue('AF'.$startingPoint.'', ($ticumulativeMale == '')? 0 : $ticumulativeMale);
                $sheet ->setCellValue('AG'.$startingPoint.'', ($ticumulativeFemale == '')? 0 : $ticumulativeFemale);
                $sheet ->setCellValue('AH'.$startingPoint.'', ($titaltocumulative == '')? 0 : $titaltocumulative);
            // CUMULATIVE (A)


            // FOR THE MONTH (B)
                $tiforthemonthMale = mysqli_fetch_row(mysqli_query($con, "SELECT * from TransferredInt where section_id='".$sectionItem[0]."' and Gender='Male' and Month='".$month_description."'"));
                $tiforthemonthFemale = mysqli_fetch_row(mysqli_query($con, "SELECT * from TransferredInt where section_id='".$sectionItem[0]."' and Gender='Female' and Month='".$month_description."'"));
                $totaltiforthemonth = $tiforthemonthMale[4] + $tiforthemonthFemale[4];
                $sheet ->setCellValue('AI'.$startingPoint.'', ($tiforthemonthMale[4] == '')? 0 : $tiforthemonthMale[4]);
                $sheet ->setCellValue('AJ'.$startingPoint.'', ($tiforthemonthFemale[4] == '')? 0 : $tiforthemonthFemale[4]);
                $sheet ->setCellValue('AK'.$startingPoint.'', ($totaltiforthemonth == '')? 0 : $totaltiforthemonth);                
            // FOR THE MONTH (B)


            // CUMULATIVE AS OF THE MONTH (A + B)
                $totaltiCumulativeasoftheMonthMale = $ticumulativeMale + $tiforthemonthMale[4];
                $totaltiCumulativeasoftheMonthFemale = $ticumulativeFemale + $tiforthemonthFemale[4];
                $totaltiCumulativeasoftheMonth = $totaltiCumulativeasoftheMonthMale + $totaltiCumulativeasoftheMonthFemale;

                $sheet ->setCellValue('AL'.$startingPoint.'', ($totaltiCumulativeasoftheMonthMale == '')? 0 : $totaltiCumulativeasoftheMonthMale);
                $sheet ->setCellValue('AM'.$startingPoint.'', ($totaltiCumulativeasoftheMonthFemale == '')? 0 : $totaltiCumulativeasoftheMonthFemale);
                $sheet ->setCellValue('AN'.$startingPoint.'', ($totaltiCumulativeasoftheMonth == '')? 0 : $totaltiCumulativeasoftheMonth);
            // CUMULATIVE AS OF THE MONTH (A + B)

        // TRANSFERED IN

        switch ($sectionItem['GradeLevel']) {
            case 'Grade 7':
                $col1M7 = $col1M7 + $totalRegisteredMale;
                $col1F7 = $col1F7 + $totalRegisteredFemale;
                $col2M7 = $col2M7 + $averageattendanceMale;
                $col2F7 = $col2F7 + $averageattendanceFemale;
                $col3M7 = $col3M7 + $percentageattendanceMale;
                $col3F7 = $col3F7 + $percentageattendanceFemale;
                $col4M7 = $col4M7 + $drpcumulativeMale;
                $col4F7 = $col4F7 + $drpcumulativeFemale;
                $col5M7 = $col5M7 + $drpforthemonthMale[4];
                $col5F7 = $col5F7 + $drpforthemonthFemale[4];
                $col6M7 = $col6M7 + $tocumulativeMale;
                $col6F7 = $col6F7 + $tocumulativeFemale;
                $col7M7 = $col7M7 + $toforthemonthMale[4];
                $col7F7 = $col7F7 + $toforthemonthFemale[4];
                $col8M7 = $col8M7 + $ticumulativeMale ;
                $col8F7 = $col8F7 + $ticumulativeFemale;
                $col9M7 = $col9M7 + $tiforthemonthMale[4];
                $col9F7 = $col9F7 + $tiforthemonthFemale[4];
                $counter7++;
                break;
            case 'Grade 8':
                $col1M8 = $col1M8 + $totalRegisteredMale;
                $col1F8 = $col1F8 + $totalRegisteredFemale;
                $col2M8 = $col2M8 + $averageattendanceMale;
                $col2F8 = $col2F8 + $averageattendanceFemale;
                $col3M8 = $col3M8 + $percentageattendanceMale;
                $col3F8 = $col3F8 + $percentageattendanceFemale;
                $col4M8 = $col4M8 + $drpcumulativeMale;
                $col4F8 = $col4F8 + $drpcumulativeFemale;
                $col5M8 = $col5M8 + $drpforthemonthMale[4];
                $col5F8 = $col5F8 + $drpforthemonthFemale[4];
                $col6M8 = $col6M8 + $tocumulativeMale;
                $col6F8 = $col6F8 + $tocumulativeFemale;
                $col7M8 = $col7M8 + $toforthemonthMale[4];
                $col7F8 = $col7F8 + $toforthemonthFemale[4];
                $col8M8 = $col8M8 + $ticumulativeMale ;
                $col8F8 = $col8F8 + $ticumulativeFemale;
                $col9M8 = $col9M8 + $tiforthemonthMale[4];
                $col9F8 = $col9F8 + $tiforthemonthFemale[4];
                    $counter8++;
                break;
            case 'Grade 9':
                $col1M9 = $col1M9 + $totalRegisteredMale;
                $col1F9 = $col1F9 + $totalRegisteredFemale;
                $col2M9 = $col2M9 + $averageattendanceMale;
                $col2F9 = $col2F9 + $averageattendanceFemale;
                $col3M9 = $col3M9 + $percentageattendanceMale;
                $col3F9 = $col3F9 + $percentageattendanceFemale;
                $col4M9 = $col4M9 + $drpcumulativeMale;
                $col4F9 = $col4F9 + $drpcumulativeFemale;
                $col5M9 = $col5M9 + $drpforthemonthMale[4];
                $col5F9 = $col5F9 + $drpforthemonthFemale[4];
                $col6M9 = $col6M9 + $tocumulativeMale;
                $col6F9 = $col6F9 + $tocumulativeFemale;
                $col7M9 = $col7M9 + $toforthemonthMale[4];
                $col7F9 = $col7F9 + $toforthemonthFemale[4];
                $col8M9 = $col8M9 + $ticumulativeMale ;
                $col8F9 = $col8F9 + $ticumulativeFemale;
                $col9M9 = $col9M9 + $tiforthemonthMale[4];
                $col9F9 = $col9F9 + $tiforthemonthFemale[4];
                    $counter9++;
                break;
            case 'Grade 10':
                $col1M10 = $col1M10 + $totalRegisteredMale;
                $col1F10 = $col1F10 + $totalRegisteredFemale;
                $col2M10 = $col2M10 + $averageattendanceMale;
                $col2F10 = $col2F10 + $averageattendanceFemale;
                $col3M10 = $col3M10 + $percentageattendanceMale;
                $col3F10 = $col3F10 + $percentageattendanceFemale;
                $col4M10 = $col4M10 + $drpcumulativeMale;
                $col4F10 = $col4F10 + $drpcumulativeFemale;
                $col5M10 = $col5M10 + $drpforthemonthMale[4];
                $col5F10 = $col5F10 + $drpforthemonthFemale[4];
                $col6M10 = $col6M10 + $tocumulativeMale;
                $col6F10 = $col6F10 + $tocumulativeFemale;
                $col7M10 = $col7M10 + $toforthemonthMale[4];
                $col7F10 = $col7F10 + $toforthemonthFemale[4];
                $col8M10 = $col8M10 + $ticumulativeMale ;
                $col8F10 = $col8F10 + $ticumulativeFemale;
                $col9M10 = $col9M10 + $tiforthemonthMale[4];
                $col9F10 = $col9F10 + $tiforthemonthFemale[4];
                $counter10++;
                break;
            
            default:
                # code...
                break;
        }
        
        $startingPoint++;
    }

    
    $secondStartingpoint = $una + 3;
    for ($j=0; $j < 4; $j++) { 
        if($j == 0)
        {
            $sheet ->setCellValue('E'.$secondStartingpoint.'', $col1M7);
            $sheet ->setCellValue('F'.$secondStartingpoint.'', $col1F7);
            $sheet ->setCellValue('G'.$secondStartingpoint.'', ($col1M7 + $col1F7));

            $sheet ->setCellValue('H'.$secondStartingpoint.'', $col2M7/$counter7);
            $sheet ->setCellValue('I'.$secondStartingpoint.'', $col2F7/$counter7);
            $sheet ->setCellValue('J'.$secondStartingpoint.'', (($col2M7/$counter7) + ($col2F7/$counter7))/2);

            if($counter7 != 0)
            {
                $sheet ->setCellValue('K'.$secondStartingpoint.'', $col3M7/$counter7);
                $sheet ->setCellValue('L'.$secondStartingpoint.'', $col3F7/$counter7);
                $sheet ->setCellValue('M'.$secondStartingpoint.'', (($col3M7/$counter7) + ($col3F7/$counter7))/2);
            }
            else
            {
                $sheet ->setCellValue('K'.$secondStartingpoint.'', 0);
            $sheet ->setCellValue('L'.$secondStartingpoint.'', 0);
            $sheet ->setCellValue('M'.$secondStartingpoint.'', 0);
            }
            

            $sheet ->setCellValue('N'.$secondStartingpoint.'', $col4M7);
            $sheet ->setCellValue('O'.$secondStartingpoint.'', $col4F7);
            $sheet ->setCellValue('P'.$secondStartingpoint.'', $col4M7 + $col4F7);

            $sheet ->setCellValue('Q'.$secondStartingpoint.'', $col5M7);
            $sheet ->setCellValue('R'.$secondStartingpoint.'', $col5F7);
            $sheet ->setCellValue('S'.$secondStartingpoint.'', $col5M7 + $col5F7);

            $sheet ->setCellValue('T'.$secondStartingpoint.'', $col4M7 + $col5M7);
            $sheet ->setCellValue('U'.$secondStartingpoint.'', $col4F7 + $col5F7);
            $sheet ->setCellValue('V'.$secondStartingpoint.'', ($col4M7 + $col5M7) + ($col4F7 + $col5F7));

            $sheet ->setCellValue('W'.$secondStartingpoint.'', $col6M7);
            $sheet ->setCellValue('X'.$secondStartingpoint.'', $col6F7);
            $sheet ->setCellValue('Y'.$secondStartingpoint.'', $col6M7 + $col6F7);

            $sheet ->setCellValue('Z'.$secondStartingpoint.'', $col7M7);
            $sheet ->setCellValue('AA'.$secondStartingpoint.'', $col7F7);
            $sheet ->setCellValue('AB'.$secondStartingpoint.'', $col7M7+ $col7F7);

            $sheet ->setCellValue('AC'.$secondStartingpoint.'', ($col6M7 + $col7M7));
            $sheet ->setCellValue('AD'.$secondStartingpoint.'', ($col6F7 + $col7F7));
            $sheet ->setCellValue('AE'.$secondStartingpoint.'', ($col6M7 + $col7M7)+ ($col6F7 + $col7F7));

            $sheet ->setCellValue('AF'.$secondStartingpoint.'', $col8M7);
            $sheet ->setCellValue('AG'.$secondStartingpoint.'', $col8F7);
            $sheet ->setCellValue('AH'.$secondStartingpoint.'', $col8M7 + $col8F7);

            $sheet ->setCellValue('AI'.$secondStartingpoint.'', $col9M7);
            $sheet ->setCellValue('AJ'.$secondStartingpoint.'', $col9F7);
            $sheet ->setCellValue('AK'.$secondStartingpoint.'', $col9M7 + $col9F7);

            $sheet ->setCellValue('AL'.$secondStartingpoint.'', ($col9M7 + $col8M7));
            $sheet ->setCellValue('AM'.$secondStartingpoint.'', ($col9F7 + $col8F7));
            $sheet ->setCellValue('AN'.$secondStartingpoint.'', ($col9M7 + $col8M7) + ($col9F7 + $col8F7));
        }
        else if($j==1)
        {
            $sheet ->setCellValue('E'.$secondStartingpoint.'', $col1M8);
            $sheet ->setCellValue('F'.$secondStartingpoint.'', $col1F8);
            $sheet ->setCellValue('G'.$secondStartingpoint.'', ($col1M8 + $col1F8));

            $sheet ->setCellValue('H'.$secondStartingpoint.'', $col2M8);
            $sheet ->setCellValue('I'.$secondStartingpoint.'', $col2F8);
            $sheet ->setCellValue('J'.$secondStartingpoint.'', $col2M8 + $col2F8);
            if($counter8 != 0)
            {
                $sheet ->setCellValue('K'.$secondStartingpoint.'', $col3M8/$counter8);
                $sheet ->setCellValue('L'.$secondStartingpoint.'', $col3F8/$counter8);
                $sheet ->setCellValue('M'.$secondStartingpoint.'', (($col3M8/$counter8) + ($col3F8/$counter8))/2);  
            }
            else
            {
                $sheet ->setCellValue('K'.$secondStartingpoint.'', 0);
            $sheet ->setCellValue('L'.$secondStartingpoint.'', 0);
            $sheet ->setCellValue('M'.$secondStartingpoint.'', 0);
            }
            

            $sheet ->setCellValue('N'.$secondStartingpoint.'', $col4M8);
            $sheet ->setCellValue('O'.$secondStartingpoint.'', $col4F8);
            $sheet ->setCellValue('P'.$secondStartingpoint.'', $col4M8 + $col4F8);

            $sheet ->setCellValue('Q'.$secondStartingpoint.'', $col5M8);
            $sheet ->setCellValue('R'.$secondStartingpoint.'', $col5F8);
            $sheet ->setCellValue('S'.$secondStartingpoint.'', $col5M8 + $col5F8);

            $sheet ->setCellValue('T'.$secondStartingpoint.'', $col4M8 + $col5M8);
            $sheet ->setCellValue('U'.$secondStartingpoint.'', $col4F8 + $col5F8);
            $sheet ->setCellValue('V'.$secondStartingpoint.'', ($col4M8 + $col5M8) + ($col4F8 + $col5F8));

            $sheet ->setCellValue('W'.$secondStartingpoint.'', $col6M8);
            $sheet ->setCellValue('X'.$secondStartingpoint.'', $col6F8);
            $sheet ->setCellValue('Y'.$secondStartingpoint.'', $col6M8 + $col6F8);

            $sheet ->setCellValue('Z'.$secondStartingpoint.'', $col7M8);
            $sheet ->setCellValue('AA'.$secondStartingpoint.'', $col7F8);
            $sheet ->setCellValue('AB'.$secondStartingpoint.'', $col7M8+ $col7F8);

            $sheet ->setCellValue('AC'.$secondStartingpoint.'', ($col6M8 + $col7M8));
            $sheet ->setCellValue('AD'.$secondStartingpoint.'', ($col6F8 + $col7F8));
            $sheet ->setCellValue('AE'.$secondStartingpoint.'', ($col6M8 + $col7M8)+ ($col6F8 + $col7F8));

            $sheet ->setCellValue('AF'.$secondStartingpoint.'', $col8M8);
            $sheet ->setCellValue('AG'.$secondStartingpoint.'', $col8F8);
            $sheet ->setCellValue('AH'.$secondStartingpoint.'', $col8M8 + $col8F8);

            $sheet ->setCellValue('AI'.$secondStartingpoint.'', $col9M8);
            $sheet ->setCellValue('AJ'.$secondStartingpoint.'', $col9F8);
            $sheet ->setCellValue('AK'.$secondStartingpoint.'', $col9M8 + $col9F8);

            $sheet ->setCellValue('AL'.$secondStartingpoint.'', ($col9M8 + $col8M8));
            $sheet ->setCellValue('AM'.$secondStartingpoint.'', ($col9F8 + $col8F8));
            $sheet ->setCellValue('AN'.$secondStartingpoint.'', ($col9M8 + $col8M8) + ($col9F8 + $col8F8));
        }
        else if($j == 2)
        {
            $sheet ->setCellValue('E'.$secondStartingpoint.'', $col1M9);
            $sheet ->setCellValue('F'.$secondStartingpoint.'', $col1F9);
            $sheet ->setCellValue('G'.$secondStartingpoint.'', ($col1M9 + $col1F9));

            $sheet ->setCellValue('H'.$secondStartingpoint.'', $col2M9);
            $sheet ->setCellValue('I'.$secondStartingpoint.'', $col2F9);
            $sheet ->setCellValue('J'.$secondStartingpoint.'', $col2M9 + $col2F9);

            if($counter9 != 0)
            {
                $sheet ->setCellValue('K'.$secondStartingpoint.'', $col3M9/$counter9);
                $sheet ->setCellValue('L'.$secondStartingpoint.'', $col3F9/$counter9);
                $sheet ->setCellValue('M'.$secondStartingpoint.'', (($col3M9/$counter9) + ($col3F9/$counter9))/2);
            }
            else
            {
                $sheet ->setCellValue('K'.$secondStartingpoint.'', 0);
                $sheet ->setCellValue('L'.$secondStartingpoint.'', 0);
                $sheet ->setCellValue('M'.$secondStartingpoint.'', 0);
            }
            

            $sheet ->setCellValue('N'.$secondStartingpoint.'', $col4M9);
            $sheet ->setCellValue('O'.$secondStartingpoint.'', $col4F9);
            $sheet ->setCellValue('P'.$secondStartingpoint.'', $col4M9 + $col4F9);

            $sheet ->setCellValue('Q'.$secondStartingpoint.'', $col5M9);
            $sheet ->setCellValue('R'.$secondStartingpoint.'', $col5F9);
            $sheet ->setCellValue('S'.$secondStartingpoint.'', $col5M9 + $col5F9);

            $sheet ->setCellValue('T'.$secondStartingpoint.'', $col4M9 + $col5M9);
            $sheet ->setCellValue('U'.$secondStartingpoint.'', $col4F9 + $col5F9);
            $sheet ->setCellValue('V'.$secondStartingpoint.'', ($col4M9 + $col5M9) + ($col4F9 + $col5F9));

            $sheet ->setCellValue('W'.$secondStartingpoint.'', $col6M9);
            $sheet ->setCellValue('X'.$secondStartingpoint.'', $col6F9);
            $sheet ->setCellValue('Y'.$secondStartingpoint.'', $col6M9 + $col6F9);

            $sheet ->setCellValue('Z'.$secondStartingpoint.'', $col7M9);
            $sheet ->setCellValue('AA'.$secondStartingpoint.'', $col7F9);
            $sheet ->setCellValue('AB'.$secondStartingpoint.'', $col7M9+ $col7F9);

            $sheet ->setCellValue('AC'.$secondStartingpoint.'', ($col6M9 + $col7M9));
            $sheet ->setCellValue('AD'.$secondStartingpoint.'', ($col6F9 + $col7F9));
            $sheet ->setCellValue('AE'.$secondStartingpoint.'', ($col6M9 + $col7M9)+ ($col6F9 + $col7F9));

            $sheet ->setCellValue('AF'.$secondStartingpoint.'', $col8M9);
            $sheet ->setCellValue('AG'.$secondStartingpoint.'', $col8F9);
            $sheet ->setCellValue('AH'.$secondStartingpoint.'', $col8M9 + $col8F9);

            $sheet ->setCellValue('AI'.$secondStartingpoint.'', $col9M9);
            $sheet ->setCellValue('AJ'.$secondStartingpoint.'', $col9F9);
            $sheet ->setCellValue('AK'.$secondStartingpoint.'', $col9M9 + $col9F9);

            $sheet ->setCellValue('AL'.$secondStartingpoint.'', ($col9M9 + $col8M9));
            $sheet ->setCellValue('AM'.$secondStartingpoint.'', ($col9F9 + $col8F9));
            $sheet ->setCellValue('AN'.$secondStartingpoint.'', ($col9M9 + $col8M9) + ($col9F9 + $col8F9));
        }
        else
        {
            $sheet ->setCellValue('E'.$secondStartingpoint.'', $col1M10);
            $sheet ->setCellValue('F'.$secondStartingpoint.'', $col1F10);
            $sheet ->setCellValue('G'.$secondStartingpoint.'', ($col1M10 + $col1F10));

            $sheet ->setCellValue('H'.$secondStartingpoint.'', $col2M10);
            $sheet ->setCellValue('I'.$secondStartingpoint.'', $col2F10);
            $sheet ->setCellValue('J'.$secondStartingpoint.'', $col2M10 + $col2F10);

            if($counter10 == 0)
            {
                $sheet ->setCellValue('K'.$secondStartingpoint.'', 0);
                $sheet ->setCellValue('L'.$secondStartingpoint.'', 0);
                $sheet ->setCellValue('M'.$secondStartingpoint.'', 0);
            }
            else
            {
                $sheet ->setCellValue('K'.$secondStartingpoint.'', $col3M10/$counter10);
                $sheet ->setCellValue('L'.$secondStartingpoint.'', $col3F10/$counter10);
                $sheet ->setCellValue('M'.$secondStartingpoint.'', (($col3M10/$counter10) + ($col3F10/$counter10))/2);
            }
            

            $sheet ->setCellValue('N'.$secondStartingpoint.'', $col4M10);
            $sheet ->setCellValue('O'.$secondStartingpoint.'', $col4F10);
            $sheet ->setCellValue('P'.$secondStartingpoint.'', $col4M10 + $col4F10);

            $sheet ->setCellValue('Q'.$secondStartingpoint.'', $col5M10);
            $sheet ->setCellValue('R'.$secondStartingpoint.'', $col5F10);
            $sheet ->setCellValue('S'.$secondStartingpoint.'', $col5M10 + $col5F10);

            $sheet ->setCellValue('T'.$secondStartingpoint.'', $col4M10 + $col5M10);
            $sheet ->setCellValue('U'.$secondStartingpoint.'', $col4F10 + $col5F10);
            $sheet ->setCellValue('V'.$secondStartingpoint.'', ($col4M10 + $col5M10) + ($col4F10 + $col5F10));

            $sheet ->setCellValue('W'.$secondStartingpoint.'', $col6M10);
            $sheet ->setCellValue('X'.$secondStartingpoint.'', $col6F10);
            $sheet ->setCellValue('Y'.$secondStartingpoint.'', $col6M10 + $col6F10);

            $sheet ->setCellValue('Z'.$secondStartingpoint.'', $col7M10);
            $sheet ->setCellValue('AA'.$secondStartingpoint.'', $col7F10);
            $sheet ->setCellValue('AB'.$secondStartingpoint.'', $col7M10+ $col7F10);

            $sheet ->setCellValue('AC'.$secondStartingpoint.'', ($col6M10 + $col7M10));
            $sheet ->setCellValue('AD'.$secondStartingpoint.'', ($col6F10 + $col7F10));
            $sheet ->setCellValue('AE'.$secondStartingpoint.'', ($col6M10 + $col7M10)+ ($col6F10 + $col7F10));

            $sheet ->setCellValue('AF'.$secondStartingpoint.'', $col8M10);
            $sheet ->setCellValue('AG'.$secondStartingpoint.'', $col8F10);
            $sheet ->setCellValue('AH'.$secondStartingpoint.'', $col8M10 + $col8F10);

            $sheet ->setCellValue('AI'.$secondStartingpoint.'', $col9M10);
            $sheet ->setCellValue('AJ'.$secondStartingpoint.'', $col9F10);
            $sheet ->setCellValue('AK'.$secondStartingpoint.'', $col9M10 + $col9F10);

            $sheet ->setCellValue('AL'.$secondStartingpoint.'', ($col9M10 + $col8M10));
            $sheet ->setCellValue('AM'.$secondStartingpoint.'', ($col9F10 + $col8F10));
            $sheet ->setCellValue('AN'.$secondStartingpoint.'', ($col9M10 + $col8M10) + ($col9F10 + $col8F10));
        }
        $secondStartingpoint++;
    }
    $secondStartingpoint = $secondStartingpoint + 3;

        $sheet ->setCellValue('E'.$secondStartingpoint.'', $col1M7 + $col1M8 + $col1M9 + $col1M10);
        $sheet ->setCellValue('F'.$secondStartingpoint.'', $col1F7 + $col1F8 + $col1F9 + $col1F10);
        $sheet ->setCellValue('G'.$secondStartingpoint.'', ($col1M7 + $col1M8 + $col1M9 + $col1M10) + ($col1F7 + $col1F8 + $col1F9 + $col1F10));

        $sheet ->setCellValue('H'.$secondStartingpoint.'', ($col2M7 + $col2M8 + $col2M9 + $col2M10)/4);
        $sheet ->setCellValue('I'.$secondStartingpoint.'', ($col2F7 + $col2F8 + $col2F9 + $col2F10)/4);
        $sheet ->setCellValue('J'.$secondStartingpoint.'', (($col2M7 + $col2M8 + $col2M9 + $col2M10) + ($col2F7 + $col2F8 + $col2F9 + $col2F10))/8);

        $sheet ->setCellValue('K'.$secondStartingpoint.'', ($col3M7 + $col3M8 + $col3M9 + $col3M10)/4);
        $sheet ->setCellValue('L'.$secondStartingpoint.'', ($col3F7 + $col3F8 + $col3F9 + $col3F10)/4);
        $sheet ->setCellValue('M'.$secondStartingpoint.'', (($col3M7 + $col3M8 + $col3M9 + $col3M10) + ($col3F7 + $col3F8 + $col3F9 + $col3F10))/8);


        $sheet ->setCellValue('N'.$secondStartingpoint.'', ($col4M7 + $col4M8 + $col4M9 + $col4M10));
        $sheet ->setCellValue('O'.$secondStartingpoint.'', ($col4F7 + $col4F8 + $col4F9 + $col4F10));
        $sheet ->setCellValue('P'.$secondStartingpoint.'', (($col4M7 + $col4M8 + $col4M9 + $col4M10) + ($col4F7 + $col4F8 + $col4F9 + $col4F10)));

        $sheet ->setCellValue('Q'.$secondStartingpoint.'', ($col5M7 + $col5M8 + $col5M9 + $col5M10));
        $sheet ->setCellValue('R'.$secondStartingpoint.'', ($col5F7 + $col5F8 + $col5F9 + $col5F10));
        $sheet ->setCellValue('S'.$secondStartingpoint.'', (($col5M7 + $col5M8 + $col5M9 + $col5M10) + ($col5F7 + $col5F8 + $col5F9 + $col5F10)));

        $sheet ->setCellValue('T'.$secondStartingpoint.'', ($col4M7 + $col4M8 + $col4M9 + $col4M10)+($col5M7 + $col5M8 + $col5M9 + $col5M10));
        $sheet ->setCellValue('U'.$secondStartingpoint.'', ($col4F7 + $col4F8 + $col4F9 + $col4F10)+($col5F7 + $col5F8 + $col5F9 + $col5F10));
        $sheet ->setCellValue('V'.$secondStartingpoint.'', (($col4M7 + $col4M8 + $col4M9 + $col4M10) + ($col4F7 + $col4F8 + $col4F9 + $col4F10))+(($col5M7 + $col5M8 + $col5M9 + $col5M10) + ($col5F7 + $col5F8 + $col5F9 + $col5F10)));


        $sheet ->setCellValue('W'.$secondStartingpoint.'', ($col6M7 + $col6M8 + $col6M9 + $col6M10));
        $sheet ->setCellValue('X'.$secondStartingpoint.'', ($col6F7 + $col6F8 + $col6F9 + $col6F10));
        $sheet ->setCellValue('Y'.$secondStartingpoint.'', (($col6M7 + $col6M8 + $col6M9 + $col6M10) + ($col6F7 + $col6F8 + $col6F9 + $col6F10)));
        $sheet ->setCellValue('Z'.$secondStartingpoint.'', ($col7M7 + $col7M8 + $col7M9 + $col7M10));
        $sheet ->setCellValue('AA'.$secondStartingpoint.'', ($col7F7 + $col7F8 + $col7F9 + $col7F10));
        $sheet ->setCellValue('AB'.$secondStartingpoint.'', (($col7M7 + $col7M8 + $col7M9 + $col7M10) + ($col7F7 + $col7F8 + $col7F9 + $col7F10)));

        $sheet ->setCellValue('AC'.$secondStartingpoint.'', ($col6M7 + $col6M8 + $col6M9 + $col6M10)+($col7M7 + $col7M8 + $col7M9 + $col7M10));
        $sheet ->setCellValue('AD'.$secondStartingpoint.'', ($col6F7 + $col6F8 + $col6F9 + $col6F10)+($col7F7 + $col7F8 + $col7F9 + $col7F10));
        $sheet ->setCellValue('AE'.$secondStartingpoint.'', (($col6M7 + $col6M8 + $col6M9 + $col6M10) + ($col6F7 + $col6F8 + $col6F9 + $col6F10))+(($col7M7 + $col7M8 + $col7M9 + $col7M10) + ($col7F7 + $col7F8 + $col7F9 + $col7F10)));


        $sheet ->setCellValue('AF'.$secondStartingpoint.'', ($col8M7 + $col8M8 + $col8M9 + $col8M10));
        $sheet ->setCellValue('AG'.$secondStartingpoint.'', ($col8F7 + $col8F8 + $col8F9 + $col8F10));
        $sheet ->setCellValue('AH'.$secondStartingpoint.'', (($col8M7 + $col8M8 + $col8M9 + $col8M10) + ($col8F7 + $col8F8 + $col8F9 + $col8F10)));
        $sheet ->setCellValue('AI'.$secondStartingpoint.'', ($col9M7 + $col9M8 + $col9M9 + $col9M10));
        $sheet ->setCellValue('AJ'.$secondStartingpoint.'', ($col9F7 + $col9F8 + $col9F9 + $col9F10));
        $sheet ->setCellValue('AK'.$secondStartingpoint.'', (($col9M7 + $col9M8 + $col9M9 + $col9M10) + ($col9F7 + $col9F8 + $col9F9 + $col9F10)));

        $sheet ->setCellValue('AL'.$secondStartingpoint.'', ($col8M7 + $col8M8 + $col8M9 + $col8M10)+($col9M7 + $col9M8 + $col9M9 + $col9M10));
        $sheet ->setCellValue('AM'.$secondStartingpoint.'', ($col8F7 + $col8F8 + $col8F9 + $col8F10)+($col9F7 + $col9F8 + $col9F9 + $col9F10));
        $sheet ->setCellValue('AN'.$secondStartingpoint.'', (($col8M7 + $col8M8 + $col8M9 + $col8M10) + ($col8F7 + $col8F8 + $col8F9 + $col8F10))+(($col9M7 + $col9M8 + $col9M9 + $col9M10) + ($col9F7 + $col9F8 + $col9F9 + $col9F10)));


        $secondStartingpoint = $secondStartingpoint + 3;
        getPrincipal($con,$secondStartingpoint,$sheet);




        function getPrincipal($con,$secondStartingpoint,$sheet)
        {
            $query = mysqli_query($con, "SELECT * from facultysmalldetail where faculty_type='Principal'");
            if(mysqli_num_rows($query) == 0)
            {
                $sheet ->setCellValue('AC'.$secondStartingpoint.'', '');
            }
            else
            {
                $temp = mysqli_fetch_row($query);
                $sheet ->setCellValue('AC'.$secondStartingpoint.'', $temp[1]);
            }
        }
    //PAG PAG DOWNLOAD SA FILE
        $writer = PHPExcel_IOFactory::createWriter($phpExcel, "Excel2007");
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="SF4_'.$sy_year.' - '.$month_description.'.xlsx"');
        header('Cache-Control: max-age=0');
        $writer->save('php://output'); 

?>