<?php

// Include PHPExcel library and create its object
    session_start();
    include_once 'dbconnect.php';
    require_once "PHPExcel-1.8.1/Classes/PHPExcel.php";
    include 'ajaxJQuery/globalFunction.php';
    

    
    $id = mysqli_real_escape_string($con,$_GET['id']);
    $getAdviserID = mysqli_fetch_row(mysqli_query($con, "SELECT faculty_id from adviser where section_id='".$id."'"));
    $AdviserID = $getAdviserID[0];
    
    

    

    // else{

        
        $month = date("m");
        $listofMonthINorOver=array('03','04','05','06');
        if(countLearner($con, $id))
        {
            if(in_array($month, $listofMonthINorOver))
            {
                $flag = 1;
                execute($con, $id, $flag);
            }
            else
            {
                $flag = 0;
                execute($con, $id, $flag);
            }
        }
        else
        {
            echo "
                <script>
                    alert('NO ENROLLED LEARNER');

                    javascript:window.close();
                </script>
            ";
        }
    //}
    function getPrincipal($con)
    {
        $query = mysqli_query($con, "SELECT * from facultysmalldetail where faculty_type='Principal'");
        if(mysqli_num_rows($query) == 0)
        {
            return "";
        }
        else
        {
            $temp = mysqli_fetch_row($query);
            return $temp[1];

        }
    }
    

    function execute($con, $id, $flag)
    {
        global $levels,$SYs,$sectionNames;
        $phpExcel = PHPExcel_IOFactory::load('School Forms Final/School Form 1.xlsx');
        $sheet = $phpExcel ->getActiveSheet();
        $iter = 10;
        $getLearners = mysqli_query($con, "SELECT * from controlDetails where section_id='".$id."' order by Gender DESC,Student ASC");

        $getAdviser = mysqli_fetch_row(mysqli_query($con, "SELECT UPPER(Adviser) as Adviser,GradeLevel from classinformation where section_id='".$id."'"));
        $adviser = $getAdviser[0];
        $level = $getAdviser[1];
        $levels = $level;

        $getSY = mysqli_fetch_row(mysqli_query($con, "SELECT SchoolYear,SectionName from controlDetails where section_id='".$id."'"));
        $SY = $getSY[0];
        $sectionName = $getSY[1];

        $SYs = $SY;
        $sectionNames = $sectionName;

        $countMale = mysqli_num_rows(mysqli_query($con, "SELECT * from controlDetails where section_id='".$id."' and Gender='Male'"));
        $countFemale = mysqli_num_rows(mysqli_query($con, "SELECT * from controlDetails where section_id='".$id."' and Gender='Female'"));
        $total = mysqli_num_rows(mysqli_query($con, "SELECT * from controlDetails where section_id='".$id."'"));

        $sheet ->setCellValue('P6', $SY);
        $sheet ->setCellValue('U6', $level);
        $sheet ->setCellValue('X6', $sectionName);



        $positionMale = $total + 2 + $iter;
        $positionFemale = $total + 3 + $iter;
        $positionTotal = $total + 4 + $iter;



        $totalDeductedbyOne = $total -1;
        if($total == 1)
        {
            
        }
        else
        {
            $sheet ->insertNewRowBefore(11,$totalDeductedbyOne);
        }
        
        

        for($i = 11; $i<=($totalDeductedbyOne+$iter); $i++)
        {
            $sheet ->mergeCells('C'.$i.':F'.$i.'');
            $sheet ->mergeCells('R'.$i.':S'.$i.'');
            $sheet ->mergeCells('T'.$i.':U'.$i.'');
            $sheet ->mergeCells('V'.$i.':W'.$i.'');
        }

        while($row = mysqli_fetch_array($getLearners)){
            $studentID = $row[4];
            $controlID = $row[0];
            $getStudentInfo = mysqli_query($con, "SELECT * from studentinfo1 where control_id='".$controlID."'");
            if($row = mysqli_fetch_array($getStudentInfo)){
                

                $sheet ->setCellValue('B'.$iter.'', $row[2]);//LRN
                $sheet ->setCellValue('C'.$iter.'', $row[3]);//Name
                $sheet ->setCellValue('G'.$iter.'', $row[4]);//Gender
                $sheet ->setCellValue('H'.$iter.'', $row[5]);//Birth Date
                $sheet ->setCellValue('I'.$iter.'', $row[6]);//Age
                $sheet ->setCellValue('L'.$iter.'', $row[8]);//Mother Tongue
                $sheet ->setCellValue('M'.$iter.'', $row[9]);//IP
                $sheet ->setCellValue('N'.$iter.'', $row[10]);//Religion
                $sheet ->setCellValue('O'.$iter.'', $row[15]);//HSSP
                $sheet ->setCellValue('P'.$iter.'', $row[16]);//Barangay
                $sheet ->setCellValue('Q'.$iter.'', $row[17]);//Municipality
                $sheet ->setCellValue('R'.$iter.'', $row[18]);//Province
                $sheet ->setCellValue('T'.$iter.'', $row[19]);//Father's Name
                $sheet ->setCellValue('V'.$iter.'', $row[20]);//Mother's Name
                $sheet ->setCellValue('X'.$iter.'', $row[21]);//Guardian's Name
                $sheet ->setCellValue('Y'.$iter.'', $row[22]);//Relatioship
                $sheet ->setCellValue('Z'.$iter.'', $row[23].' '.$row[24]);//Contact No.
                $defaultRemarks = array('T/O','T/I','DRP','CCT','ACL');
                if(in_array($row[13], $defaultRemarks))
                {
                    $sheet ->setCellValue('AA'.$iter.'', $row[13].' DATE: '.$row[26]);//Remarks
                }
                else if($row[13] == '')
                {
                    $sheet ->setCellValue('AA'.$iter.'', '');//Remarks
                }
                else
                {
                    $sheet ->setCellValue('AA'.$iter.'', $row[13]);//Remarks
                }

            }
            $iter++;
        }

        $sheet ->setCellValue('T'.$positionMale.'', $countMale);
        $sheet ->setCellValue('T'.$positionFemale.'', $countFemale);
        $sheet ->setCellValue('T'.$positionTotal.'', $total);


        if($flag == 1)
        {


            $countMale1 = mysqli_num_rows(mysqli_query($con, "SELECT * from controlDetails where section_id='".$id."' and Gender='Male' and NOT control_remarks='T/O' AND NOT control_remarks='DRP'"));
            $countFemale1 = mysqli_num_rows(mysqli_query($con, "SELECT * from controlDetails where section_id='".$id."' and Gender='Female' and NOT control_remarks='T/O' AND NOT control_remarks='DRP'"));

            
            $sheet ->setCellValue('U'.$positionMale.'', $countMale1);
        $sheet ->setCellValue('U'.$positionFemale.'', $countFemale1);
        $sheet ->setCellValue('U'.$positionTotal.'', $countMale1 + $countFemale1);
        }

        $sheet ->setCellValue('W'.$positionMale.'', $adviser);
        $sheet ->setCellValue('Z'.$positionMale.'', getPrincipal($con));

        $writer = PHPExcel_IOFactory::createWriter($phpExcel, "Excel2007");
        //
        // $writer->save('C:/Users/MLD/Documents/School Form 1-'.$sectionName.'-'.$SY.'.xlsx'); 
        $fileName = "";
        switch ($levels) {
            case 'Grade 7':
                $fileName = "SF1_".$SYs."_".$levels." (Year I) - ".$sectionNames;
                break;
            case 'Grade 8':
                $fileName = "SF1_".$SYs."_".$levels." (Year II) - ".$sectionNames;
                break;
            case 'Grade 9':
                $fileName = "SF1_".$SYs."_".$levels." (Year III) - ".$sectionNames;
                break;
            case 'Grade 10':
                $fileName = "SF1_".$SYs."_".$levels." (Year IV) - ".$sectionNames;
                break;
            default:
                # code...
                break;
        }




        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$fileName.'.xlsx"');
        header('Cache-Control: max-age=0');
        $writer->save('php://output'); 
        //header("Location: index.php");
    }
    
?>