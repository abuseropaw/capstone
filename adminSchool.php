<?php
    include_once 'sessionAdmin.php';
    include_once 'dbconnect.php';
    include 'ajaxJQuery/globalFunction.php';
    function getSY($con)
    {
        $query = mysqli_query($con, "SELECT sy_year from schoolyear where sy_remarks='Open'");
        $temp = mysqli_fetch_row($query);
        return $temp[0];
    }

?>
<!DOCTYPE html>
    <!-- HEAD -->
    <?php include_once 'head.php'; ?>
    <!-- HEAD   -->
    <body>
        <!-- HEADER -->
        <?php include_once 'header.php'; ?>
        <!-- HEADER -->

        <section id="main">
            <ol class="breadcrumb">
                <li><a href="adminHome.php">Home</a></li>
                <li class="active">Lugait National High School</li>
            </ol>
            <?php
                $toggle = 'adminSchool';
                include_once 'sidebar.php';
            ?>



            <section id="content">
                <div class="container">
                    <!-- Colored Headers -->
                    

                        <div class="modal fade" id="modalColor" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog modal-sm">
                                <div class="modal-content">
                                    <form role="form" id="newSY" method="post" name="signupform">
                                        <div class="modal-header">
                                            <h4 class="modal-title"><b>NEW SCHOOL YEAR</b></h4>
                                        </div>
                                        <div class="modal-body">
                                            <div id="sy">
                                                
                                            </div>
                                            <b>NOTE:</b>
                                            <ul><li>You must set your computer date and time correctly</li><li>If you are going to start school year in your preferred year, you must set the computer date</li></ul>
                                        </div>
                                        <div class="modal-footer">

                                            <button type="submit" name="add" id="add" class="btn btn-success">START</button>
                                            <button type="button" class="btn btn-danger" data-dismiss="modal">CANCEL
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="modal fade" id="modalQuarter" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog modal-sm">
                                <div class="modal-content">
                                    <form role="form" id="addQuarterForm" method="post" name="signupform">
                                        <div class="modal-header">
                                            <h4 class="modal-title"><b>NEW QUARTER</b></h4>
                                        </div>
                                        <div class="modal-footer">

                                            <button type="submit" name="addQuarter" id="addQuarter" class="btn btn-success btn-lg">START</button>

                                            <button type="button" class="btn btn-warning btn-lg" data-dismiss="modal">CANCEL
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="modal fade" id="modalMonth" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog modal-sm">
                                <div class="modal-content">
                                    <form role="form" id="addMonthForm" method="post" name="signupform">
                                        <div class="modal-header">
                                            <h4 class="modal-title"><b>OPEN MONTH</b></h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="col-sm-12">
                                                <div class="form-group fg-line">
                                                    <select class="selectpicker form-control" name="month" id="month" required>
                                                        
                                                        
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="form-group fg-line">
                                                    <input type="number" name="total" id="total" class="form-control" placeholder="Total Days" min="0" required>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="modal-footer">

                                            <button type="submit" name="addMonth" id="addMonth" class="btn btn-success btn-lg">START</button>

                                            <button type="button" class="btn btn-warning btn-lg" data-dismiss="modal">CANCEL
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="modal fade" id="modalEditQuarter" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog modal-sm">
                                <div class="modal-content">
                                    <form role="form" id="editQuarterForm" method="post" name="signupform">
                                        <div class="modal-header">
                                            <h4 class="modal-title"><b>EDIT QUARTER</b></h4>
                                        </div>


                                        <div class="modal-body">
                                            <div class='col-sm-12'>

                                                <div class='form-group fg-line'>
                                                    <select class="form-control selectpicker" name='quarter1' id ='quarter1' required>
                                                        <option value=""></option>
                                                        <option>Open</option>
                                                        <option>Closed</option>
                                                    </select>
                                                    <input type="hidden" name="quarterID" id="quarterID">
                                                </div>
                                            </div>

                                        </div>
                                        <div class="modal-footer">

                                            <button type="submit" name="editQuarter" id="editQuarter" class="btn btn-success">EDIT</button>

                                            <button type="button" class="btn btn-danger" data-dismiss="modal">CANCEL
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="modal fade" id="modalEditMonth" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog modal-sm">
                                <div class="modal-content">
                                    <form role="form" id="editMonthForm" method="post" name="signupform">
                                        <div class="modal-header">
                                            <h4 class="modal-title"><b>EDIT MONTH</b></h4>
                                        </div>


                                        <div class="modal-body">
                                            <div class='col-sm-12'>

                                                <div class='form-group fg-line'>
                                                    <input type="number" name="total1" id="total1" class="form-control" placeholder="Total Days" min="0">
                                                </div>
                                            </div>
                                            <div class='col-sm-12'>

                                                <div class='form-group fg-line'>
                                                    <select class="form-control selectpicker" name='status' id ='status' required>
                                                        <option value=""></option>
                                                        <option>Open</option>
                                                        <option>Closed</option>
                                                    </select>
                                                    <input type="hidden" name="monthID" id="monthID">
                                                </div>
                                            </div>

                                        </div>
                                        <div class="modal-footer">

                                            <button type="submit" name="editMonth" id="editMonth" class="btn btn-success">EDIT</button>

                                            <button type="button" class="btn btn-danger" data-dismiss="modal">CANCEL
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    

                        <div id="hehe">
                            <?php
                                include_once 'ajaxJQuery/displaySchoolYear.php';
                            ?>
                        </div>

                    <br/>
                    <br/>
                </div>
            </section>
        </section>

        <!-- FOOTER -->
        <?php include_once 'footer.php' ?>
        <!-- FOOTER -->

        <!-- Javascript Libraries -->
        <?php include_once 'scripts.php'; ?>
        <!-- Javascript Libraries -->
        <script type="text/javascript" src="js/Notification.js"></script>


         <script type="text/javascript">
            $(document).ready(function(){

                $('#modalColor').on('shown.bs.modal', function() {
                    var d = new Date();
                    $('#sy').html('<center><h1><b>'+d.getFullYear()+ ' - ' +(d.getFullYear()+1)+'</b></h1></center>');

                });

                $('#modalMonth').on('shown.bs.modal', function() {
                    $.ajax({
                        url: "ajaxJQuery/getListofMonth.php",
                        success:function(data){
                            
                            $("#month").empty();
                            $("#month").append("<option>"+data+"</option>");
                            $('#month').selectpicker('refresh');
                            $('#modalMonth').modal('show');
                        }
                    });
                });
                $('#newSY').on("submit", function(event){
                    event.preventDefault();
                     $.ajax({
                          url:"ajaxJQuery/insertSY.php",
                          method:"POST",
                          data:$('#newSY').serialize(),
                          success:function(data){


                            if(data == '0')
                            {
                                var message = "Error on starting new School Year.";
                                executeNotif(message,"danger");
                            }
                            else
                            {
                                var message = "Successfully started a new school year!.";
                                executeNotif(message,"success");
                                $('#modalColor').modal('hide');
                                $('#hehe').html(data);

                            }
                          }

                     });

                });
                $('#addMonthForm').on("submit", function(event){
                    event.preventDefault();
                    $.ajax({
                        url:"ajaxJQuery/addMonth.php",
                        method:"POST",
                        data:$('#addMonthForm').serialize(),
                        success:function(response){

                            var message = "Successfully added month";
                            executeNotif(message,"success");
                            $('#modalMonth').modal('hide');
                            $('#addMonthForm')[0].reset();
                            $('#hehe').html(response);
                            
                        }
                    });

                });

                $('#editMonthForm').on("submit", function(event){
                    event.preventDefault();
                    if($('#total1').val() == '')
                    {
                         $('#total1').focus();
                    }

                    else
                    {
                         $.ajax({
                              url:"ajaxJQuery/updateMonth.php",
                              method:"POST",
                              data:$('#editMonthForm').serialize(),
                              success:function(data){


                                if(data == '0')
                                {
                                    var message = "Error on editing quarter.";
                                    executeNotif(message,"danger");
                                }
                                else
                                {
                                    var message = "Successfully edited month";
                                    executeNotif(message,"success");
                                  $('#editMonthForm')[0].reset();
                                  $('#editMonthForm').selectpicker('refresh');
                                  $('#modalEditMonth').modal('hide');
                                  $('#hehe').html(data);
                                  
                                }
                              }

                         });
                    }
                });

                $('#editQuarterForm').on("submit", function(event){
                    event.preventDefault();
                    if($('#quarter1').val() == '')
                    {
                         alert("You must select!");
                    }

                    else
                    {
                         $.ajax({
                              url:"ajaxJQuery/updateQuarter.php",
                              method:"POST",
                              data:$('#editQuarterForm').serialize(),
                              success:function(data){


                                if(data == '0')
                                {
                                    var message = "Error on editing quarter.";
                                    executeNotif(message,"danger");
                                }
                                else
                                {
                                    var message = "Successfully edited quarter";
                                    executeNotif(message,"success");
                                  $('#editQuarterForm')[0].reset();
                                  $('#editQuarterForm').selectpicker('refresh');
                                  $('#modalEditQuarter').modal('hide');
                                  $('#hehe').html(data);
                                 
                                }
                              }

                         });
                    }
                });


                $('#addQuarterForm').on("submit", function(event){
                    event.preventDefault();
                    if($('#quarter').val() == '')
                    {
                         alert("All quarter are added");
                    }

                    else
                    {
                         $.ajax({
                              url:"ajaxJQuery/insertQuarter.php",
                              method:"POST",
                              data:$('#addQuarterForm').serialize(),
                              success:function(data){


                                if(data == '0')
                                {
                                    var message = "Error on adding quarter. Quarter may exceed";
                                    executeNotif(message,"danger");
                                    $('#modalQuarter').modal('hide');
                                }
                                else
                                {
                                    var message = "Successfully added quarter.";
                                    executeNotif(message,"success");
                                  $('#modalQuarter').modal('hide');
                                  $('#hehe').html(data);

                                  
                                }
                              }

                         });
                    }
                });
                $(document).on('click', '.edit_month', function(){
                    var monthID = $(this).attr("id");

                    $.ajax({
                         url:"ajaxJQuery/selectMonth.php",
                         method:"POST",
                         data:{monthID:monthID},
                         dataType:"json",
                         success:function(data){
                              $('#monthID').val(data.month_id);
                              $('#total1').val(data.month_totalDays);
                              $('#modalEditMonth').modal('show');

                              //$('.title-modal').text('asdas');

                         }
                    });
                });

                $(document).on('click', '.edit_quarter', function(){
                    var subj_id = $(this).attr("id");

                    $.ajax({
                         url:"ajaxJQuery/selectSubject.php",
                         method:"POST",
                         data:{subj_id:subj_id},
                         dataType:"json",
                         success:function(data){
                              $('#quarterID').val(subj_id);

                              $('#modalEditQuarter').modal('show');

                              //$('.title-modal').text('asdas');

                         }
                    });
                });
                $(document).on('click', '.schoolYears', function(e){
                    e.preventDefault();
                    var sy_id = $(this).attr("id");
                    swal({
                        title: "Are you sure?",
                        text: "You will not be able to reopen it",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonText: "Yes, close it!",
                    }).then(function(){

                        $.ajax({
                            url:"ajaxJQuery/closeSY.php",
                            method:"POST",
                            data:{sy_id:sy_id},

                            success:function(data){
                                if(data == '0'){
                                    
                                    var message = "You need to end the school year by completing all the school quarter and school month";
                                    executeNotif(message,"danger");
                                }else{

                                    var message = "Successfully close the school year";
                                    executeNotif(message,"success");
                                    $('#hehe').html(data);
                                }
                            }

                        });
                    });

                });

                $(document).on('click', '.deleteSchoolYear', function(e){
                    e.preventDefault();
                    var sy_id = $(this).attr("id");
                    swal({
                        title: "Delete School Year?",
                        text: "You will not be able to reopen it",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonText: "Yes, delete it!",
                    }).then(function(){

                        $.ajax({
                            url:"ajaxJQuery/deleteSY.php",
                            method:"POST",
                            data:{sy_id:sy_id},

                            success:function(data){
                                if(data == '0'){
                                    
                                    var message = "School year already in used!";
                                    executeNotif(message,"danger");
                                }else{

                                    var message = "Successfully delete School year";
                                    executeNotif(message,"success");
                                    $('#hehe').html(data);
                                }
                            }

                        });
                    });

                });
            });
        </script>
    </body>

<!-- Mirrored from byrushan.com/projects/ma/1-7-1/jquery/light/widget-templates.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 01 May 2017 06:35:54 GMT -->
</html>
