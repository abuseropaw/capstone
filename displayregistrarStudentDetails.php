<?php
    $output = '';
    $output.='
    <div role="tabpanel" class="tab-pane active" id="learner">
<div class="card table-responsive">
    <table class="table table-bordered table-hover">
        <thead class="bgm-green">
            <tr>
                <th class="text-center" rowspan="2" width="15"><small><small><b>Info/Edit/Delete</b></small></small></th>


                <th class="text-center" rowspan="2"><b>Remarks</b></th>
                
                <th class="text-center" rowspan="2"><b>LRN</b></th>
                <th class="text-center" colspan="3"><b>Name</b></th>
                <th class="text-center" rowspan="2"><b>Sex</b></th>
                
                <th class="text-center" rowspan="2"><b>DOB</b></th>
                <th class="text-center" rowspan="2"><b>Age</b></th>
                <th class="text-center" rowspan="2"><b>Mother Tongue</b></th>
                <th class="text-center" rowspan="2"><b>Religion</b></th>

               
                
                
                
            </tr>
            <tr>
                <th class="text-center"><small><small><small><b>Last</b></small></small></small></th>
                <th class="text-center"><small><small><small><b>First</b></small></small></small></th>
                <th class="text-center"><small><small><small><b>Middle</b></small></small></small></th>
            </tr>

            
        </thead>
        <tbody>';
            
                $getLearners = mysqli_query($con, "SELECT * from controlDetails where section_id='".$id."' order by Gender DESC,Student ASC");
                while($row = mysqli_fetch_array($getLearners)){
                    $studentID = $row[4];
                    

                    $getStudentInfo = mysqli_query($con, "SELECT * from studentinfo1 where student_id='".$studentID."' order by Name");
                    if($row1 = mysqli_fetch_array($getStudentInfo)){

                        $temp = explode(',', $row1[3]);
                        
                        $output.= "
                        <tr>
                            <td>
                            
                                <button type='submit' onclick=window.open('studentinformation.php?sID=".$row1[1]."') data-toggle='tooltip' data-placement='top' title='Details' name='details' id=".$row1[0]." class='btn btn-default details'><i class='zmdi zmdi-info'></i></button>
                                <button type='submit' name='edit' id='".$row[0]."' data-toggle='tooltip' data-placement='top' title='Edit' class='btn btn-default edit_learner'><i class='zmdi zmdi-edit'></i></button>
                                <button type='submit' name='delete' id='".$row[0]."' data-toggle='tooltip' data-placement='top' title='Delete' class='btn btn-default delete_learner'><i class='zmdi zmdi-delete'></i></button>
                            </td>
                            <td>".$row[1]." ".$row[12]."</td>
                            
                            <td>".$row1[2]."</td>
                            <td>".$temp[0]."</td>
                            <td>".$temp[1]."</td>
                            <td>".$temp[2]."</td>
                            <td>".$row1['sex']."</td>
                            <td>".$row1['BirthDate']."</td>
                            <td>".$row1['Age']."</td>
                            
                            <td>".$row1['MotherTongue']."</td>
                            <td>".$row1['Religion']."</td>
                            



                            
                            
                        </tr>
                        ";
                    }
                }
        $output.='
        </tbody>
    </table>
</div>
</div>';
    echo $output;
?>