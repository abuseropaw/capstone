<?php
    session_start();
   if($_SESSION['faculty_id'] == ''){ header("Location: index.php");}else if( $_SESSION['faculty_type'] != 'Head Teacher' && $_SESSION['faculty_type'] != 'Registrar' ){header("Location: javascript:");}
    include_once 'dbconnect.php';
    include 'ajaxJQuery/globalFunction.php';

    $id =mysqli_real_escape_string($con, $_GET['sID']);

    $query = mysqli_fetch_row(mysqli_query($con, "SELECT section_name,sy_id from section where section_id='".$id."'"));
    $name = $query[0];
    $sy = $query[1];

    if($sy != getCurrentSY1($con))
    {
        echo "
        <script>
           

            javascript:window.close()
        </script>";
    }
    function fillSchoolYear($con,$id){
        $output ='';
        $query = mysqli_query($con, "SELECT * from subjectofferingdetails where section_id='".$id."'");
        if(mysqli_num_rows($query) == 0)
        {
            $output .='<option disabled="disabled" selected>NO CLASSES</option>';
        }
        else
        {
            
            while($row = mysqli_fetch_array($query)){
                
                $output .='<option value="'.$row['so_id'].'">'.$row['SubjectTitle'].' - '.$row['Teacher'].'</option>';
            }
        }
        
        return $output;
    }
    
    
?>

<!DOCTYPE html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" type="image/x-icon" href="img/lnhs.ico" alt="logo" />
<title>SIS | <?php echo $name; ?></title>

<link href="vendors/bower_components/animate.css/animate.min.css" rel="stylesheet">
<link href="vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css" rel="stylesheet">
<link href="vendors/bower_components/sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
<link href="vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet">
<link href="vendors/bower_components/dropzone/dist/min/dropzone.min.css" rel="stylesheet">
<link href="vendors/bower_components/chosen/chosen.css" rel="stylesheet">
<link href="vendors/bower_components/datatables.net-dt/css/jquery.dataTables.min.css" rel="stylesheet">
<link href="vendors/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">
<link href="vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
<link href="vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet">
<link href="vendors/bower_components/lightgallery/dist/css/lightgallery.min.css" rel="stylesheet">

<link href="css/app_1.min.css" rel="stylesheet">
<link href="css/app_2.min.css" rel="stylesheet">
<link href="css/demo.css" rel="stylesheet">
</head>
    <body>
        <!-- HEADER -->
        <?php include_once 'header.php'; ?>
        <!-- HEADER -->

        <section id="main">
           
            
                <div class="container">
                    <div class="block-header">
                        <h1><i class="zmdi zmdi-accounts"></i> <?php echo $name; ?>
                        </h1>
                        
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="card">
                                
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-nowrap">
                                            <thead>
                                                <tr>
                                                    <th colspan='6'><center><b>LIST OF CLASSES</b></center></th>
                                                </tr>
                                                <tr>

                                                    <th><b>Subject</b></th>
                                                    <th><b>Description</b></th>
                                                    <th><b>Time</b></th>
                                                    <th><b>Days</b></th>
                                                    <th><b>Assigned Teacher</b></th>
                                                    <th><b><small><small>Monitor</small></small></b></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                    $query = mysqli_query($con, "SELECT * from subjectofferingdetails where section_id='".$id."'");
                                                    while ($row = mysqli_fetch_array($query)) {
                                                        echo "
                                                            <tr>
                                                                <td>".$row['SubjectTitle']."</td>
                                                                <td>".$row['SubjecDescription']."</td>
                                                                <td>".$row['so_startTime']." - ".$row['so_endTime']."</td>
                                                                <td>".$row['so_days']."</td>
                                                                <td>".$row['Teacher']."</td>
                                                                <td><button class='btn btn-default monitor' id='".$row[0]."'  data-toggle='tooltip' data-placement='top' title='Monitor Class'><i class='zmdi zmdi-arrow-right'></i></button></td>
                                                            </tr>

                                                        ";
                                                    }
                                                ?>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="card">
                                
                                <div class="card-body" id="listofStudents">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-nowrap">
                                            <thead>
                                                <tr>
                                                <th rowspan="2" class="text-center"><b>LRN</b></th>
                                                <th rowspan="2" class="text-center"><b>Name</b></th>
                                                <th rowspan="2" class="text-center"><b>Remarks</b></th>
                                                <th colspan="5" class="text-center"><b>Grade</b></th>
                                                <th rowspan="2" class="text-center"><b>Details/Edit Grade</b></th>
                                               
                                                </tr>

                                                <tr>
                                                <th class="text-center"><small><small>1st</small></small></th>
                                                <th class="text-center"><small><small>2nd</small></small></th>
                                                <th class="text-center"><small><small>3rd</small></small></th>
                                                <th class="text-center"><small><small>4th</small></small></th>
                                                <th class="text-center"><small><small>Average</small></small></th>
                                                
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                
                
            </div>
            
        </section>
        <div id="historyGradeModal" class="modal fade" data-backdrop="static" data-keyboard="false">  
            <div class="modal-dialog">  
               <div class="modal-content">  
                    <div class="modal-header">  
                         <button type="button" class="close" data-dismiss="modal">&times;</button>  
                         <h4 class="modal-title"><b>GRADE HISTORY</b></h4>  
                    </div>  
                    <div class="modal-body" id="Details">  
                    </div>  
                    <div class="modal-footer">  
                         <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>  
                    </div>  
               </div>  
            </div>  
        </div>

        
        <!-- FOOTER -->
        <?php include_once 'footer.php' ?>
        <!-- FOOTER -->

        <!-- Javascript Libraries -->
        <?php include_once 'scripts.php'; ?>
        <script type="text/javascript" src="js/Notification.js"></script>
        <!-- Javascript Libraries -->
        <script type="text/javascript">

            function exec(){
                
            }
            function openModal()
            {
                $('.history_grade').on("click",function(e){
                    e.preventDefault();
                    var soid = $(this).attr("name");
                    var controlID =$(this).attr("id"); 
                    $.ajax({
                        url:"ajaxJQuery/viewHistoryGrades.php",
                        method: "get",
                        data:{controlID:controlID, soid:soid},
                        success:function(data){
                            $('#Details').html(data);
                            $('#historyGradeModal').modal('show');
                        }
                    });
                });
            }
            $(document).ready(function() {
                $('.monitor').on('click', function(e){
                    e.preventDefault();
                    var soid = $(this).attr("id");
                    var sid ="<?php echo $id; ?>"; 
                    $.ajax({
                        url:"ajaxJQuery/monitorClass.php",
                        method: "get",
                        data:{sid:sid, soid:soid},
                        dataType: "html",
                        success:function(data){
                            $('#listofStudents').html(data);
                        }
                    });
                });


                
            } );
        </script>
    </body>

</html>