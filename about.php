<?php
    include_once 'sessionAdmin.php';
    include_once 'dbconnect.php';
    include 'ajaxJQuery/globalFunction.php';
    function fillSchoolYear($con){
        $output ='';
        $query = mysqli_query($con, "SELECT * from schoolyear");
        while($row = mysqli_fetch_array($query)){
            $output .='<option value="'.$row[1].'">'.$row[1].'</option>';
        }
        return $output;
    }


?>
<!DOCTYPE html>
    <!-- HEAD -->
    <?php include_once 'head.php'; ?> 
    <!-- HEAD   -->
    <body>
        <!-- HEADER -->
        <?php include_once 'header.php'; ?>
        <!-- <style>
        

        /* Link the series colors to axis colors */
        .highcharts-color-0 {
            fill: #00BCD4;
            stroke: #90ed7d;
        }
        .highcharts-axis.highcharts-color-0 .highcharts-axis-line {
            stroke: #90ed7d;
        }
        .highcharts-axis.highcharts-color-0 text {
            fill: #90ed7d;
        }

        .highcharts-color-1 {
            fill: #F44336;
            stroke: #FFEB38;
        }
        .highcharts-axis.highcharts-color-1 .highcharts-axis-line {
            stroke: #FFEB38;
        }
        .highcharts-axis.highcharts-color-1 text {
            fill: #F44336;
        }

        .highcharts-color-2 {
            fill: #FFEB38;
            stroke: #F44336;
        }
        .highcharts-axis.highcharts-color-2 .highcharts-axis-line {
            stroke: #F44336;
        }
        .highcharts-axis.highcharts-color-2 text {
            fill: #FFEB38;
        }

        .highcharts-color-3 {
            fill: #7cb5ec;
            stroke: #7cb5ec;
        }
        .highcharts-axis.highcharts-color-3 .highcharts-axis-line {
            stroke: #7cb5ec;
        }
        .highcharts-axis.highcharts-color-3 text {
            fill: #7cb5ec;
        }


        .highcharts-yaxis .highcharts-axis-line {
            stroke-width: 2px;
        }
        </style> -->
        <!-- HEADER -->

        <section id="main">
            
            <?php 
                $toggle = '';
                include_once 'sidebar.php'; 
            ?>
            <section id="content">
              <div class="text-center p-t-25">


                        <h1>PROJECT TEAM</h1>
                        <br />
                    </div>
              <div class="row">
                <div class="col-sm-4">
                  <div class="card profile-view">
                    <div class="pv-header">
                        <img src="img/remon.jpg" class="pv-main" alt="">
                    </div>

                    <div class="pv-body">
                        <h2>Reymond Aljas</h2>
                        <small>
                          Front End Developer and Back End Developer.
                        </small>

                        <ul class="pv-contact">
                            <li><i class="zmdi zmdi-pin"></i> Naawan Misamis Oriental</li>
                            <li><i class="zmdi zmdi-phone"></i> 0909090649289</li>
                        </ul>

                        
                    </div>
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="card profile-view">
                    <div class="pv-header">
                        <img src="img/remon.jpg" class="pv-main" alt="">
                    </div>

                    <div class="pv-body">
                        <h2>Christine Gonzales</h2>
                        <small>
                          Front End Developer and Technical Writer
                        </small>

                        <ul class="pv-contact">
                            <li><i class="zmdi zmdi-pin"></i> Lugait Misamis Oriental</li>
                            <li><i class="zmdi zmdi-phone"></i> 0909090649289</li>
                        </ul>

                        
                    </div>
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="card profile-view">
                    <div class="pv-header">
                        <img src="img/remon.jpg" class="pv-main" alt="">
                    </div>

                    <div class="pv-body">
                        <h2>Melquisedic Ycat</h2>
                        <small>
                          Technical Writer
                        </small>

                        <ul class="pv-contact">
                            <li><i class="zmdi zmdi-pin"></i> Gimangpang, Initao  Misamis  Oriental </li>
                            <li><i class="zmdi zmdi-phone"></i> 0909090649289</li>
                        </ul>

                        
                    </div>
                  </div>
                </div>
              </div>
            </section>
        </section>


        <!-- Javascript Libraries -->
        <?php include_once 'scripts.php'; ?>
        
        <!-- Javascript Libraries -->
        <script src="vendors/bower_components/flot/jquery.flot.js"></script>
        <script src="vendors/bower_components/flot/jquery.flot.resize.js"></script>
        <script src="vendors/bower_components/flot.curvedlines/curvedLines.js"></script>
        <script src="vendors/sparklines/jquery.sparkline.min.js"></script>
        <script src="vendors/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js"></script>
        
    </body>

</html>
