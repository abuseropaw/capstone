-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 01, 2018 at 02:01 AM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wbsislnhs`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `addBooksAcquired` (`cID` VARCHAR(15), `bID` VARCHAR(15), `remarks` VARCHAR(50), `acquired` DATE, `returned` DATE)  BEGIN
   INSERT INTO booksacquired VALUES(cID,bID,remarks,acquired,returned);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `changePassword` (`id` VARCHAR(15), `newPassword` VARCHAR(200))  BEGIN
UPDATE student_account set student_pass=newPassword where student_id=id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `currentSchoolYear` ()  BEGIN
         SELECT sy_year from schoolyear where sy_remarks='Open';
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteClassLearner` (IN `controlID` VARCHAR(15), IN `studentID` VARCHAR(15), IN `syID` VARCHAR(15))  NO SQL
BEGIN
	DELETE from control where control_id=controlID and sy_id=syID;
    DELETE from parent where student_id=studentID and sy_id=syID;
    DELETE from address where student_id=studentID and sy_id=syID;
    DELETE from student_account where student_id=studentID;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getYearLevelID` (IN `title` VARCHAR(25))  BEGIN
   SELECT year_lvl_id
       FROM yearlevel WHERE year_lvl_title = title;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertAddress` (IN `studentID` VARCHAR(15), IN `HSSP` VARCHAR(50), IN `barangay` VARCHAR(50), IN `municipality` VARCHAR(50), IN `province` VARCHAR(50), IN `dateCreated` DATETIME, IN `createdBy` VARCHAR(15), IN `SY` VARCHAR(15))  BEGIN
     INSERT INTO address VALUES(studentID,HSSP,barangay,municipality,province,dateCreated,createdBy,SY);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertAttendance` (IN `present` INT(5), IN `absent` INT(5), IN `tardy` INT(5), IN `consecutive` VARCHAR(25), IN `datecreated` DATETIME, IN `month` VARCHAR(15), IN `faculty` VARCHAR(15), IN `control` VARCHAR(15))  BEGIN
	INSERT into attendance VALUES(present,absent,tardy,consecutive,datecreated,month,faculty,control);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertBook` (`id` VARCHAR(15), `title` VARCHAR(25), `description` VARCHAR(25), `dateCreated` DATETIME, `createdBy` VARCHAR(15), `subjID` VARCHAR(15))  BEGIN
       INSERT INTO book VALUES(id, title, description, dateCreated, createdBy, subjID);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertControl` (IN `id` VARCHAR(15), IN `remarks` VARCHAR(25), IN `dateCreated` DATE, IN `dateUpdated` DATE, IN `createdBy` VARCHAR(50), IN `studentID` VARCHAR(15), IN `sectionID` VARCHAR(15), IN `syID` VARCHAR(15), IN `reqInformation` VARCHAR(100))  BEGIN
          INSERT INTO control VALUES(id,remarks,dateCreated,dateUpdated,createdBy,studentID,sectionID,syID,reqInformation);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertCredential` (`id` VARCHAR(15), `title` VARCHAR(25), `description` VARCHAR(25), `dateCreated` DATETIME, `createdBy` VARCHAR(15))  BEGIN
     INSERT INTO credential VALUES(id, title, description, dateCreated, createdBy);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertGuardian` (`guardianID` VARCHAR(15), `studentID` VARCHAR(15), `name` VARCHAR(25), `phone` VARCHAR(13), `relationship` VARCHAR(25), `dateCreated` DATETIME, `createdBy` VARCHAR(15))  BEGIN
       INSERT INTO guardian VALUES(guardianID,studentID,name,phone,relationship,dateCreated,createdBy);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertLevel` (`id` VARCHAR(15), `title` VARCHAR(25), `dateCreated` DATETIME, `createdBy` VARCHAR(15))  BEGIN
    INSERT INTO yearlevel VALUES(id,title,dateCreated,createdBy);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertParent` (IN `studentID` VARCHAR(15), IN `Ffname` VARCHAR(25), IN `Fmname` VARCHAR(25), IN `Flname` VARCHAR(25), IN `Mfname` VARCHAR(25), IN `Mmname` VARCHAR(25), IN `Mlname` VARCHAR(25), IN `phone` VARCHAR(13), IN `guardianName` VARCHAR(25), IN `guardianPhone` VARCHAR(13), IN `guardianRelation` VARCHAR(25), IN `dateCreated` DATETIME, IN `createdBy` VARCHAR(15), IN `SY` VARCHAR(50))  BEGIN
    INSERT INTO parent VALUES(studentID,Ffname,Fmname,Flname,Mfname,Mmname,Mlname,phone,guardianName,guardianPhone,guardianRelation,dateCreated,createdBy,SY);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertSection` (`id` VARCHAR(15), `name` VARCHAR(25), `capacity` INT(5), `max` DOUBLE(5,2), `min` DOUBLE(5,2), `createdBy` VARCHAR(15), `dateCreated` DATETIME, `level` VARCHAR(15), `sy` VARCHAR(15))  BEGIN
        INSERT INTO section VALUES(id,name,capacity,max,min,createdBy,dateCreated,level,sy);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertStudent` (IN `id` VARCHAR(15), IN `LRN` VARCHAR(20), IN `fname` VARCHAR(50), IN `mname` VARCHAR(50), IN `lname` VARCHAR(50), IN `sex` VARCHAR(25), IN `birthDate` DATE, IN `age` INT(5), IN `motherTongue` VARCHAR(50), IN `IP` VARCHAR(50), IN `religion` VARCHAR(50), IN `picture` VARCHAR(100), IN `pass` VARCHAR(200), IN `dateCreated` DATETIME, IN `createdBy` VARCHAR(15))  BEGIN
     INSERT INTO student_account VALUES(id,LRN,fname,mname,lname,sex,birthDate,age,motherTongue,IP,religion,picture,sha1(pass),dateCreated,createdBy);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertSubject` (`id` VARCHAR(15), `title` VARCHAR(25), `description` VARCHAR(100), `unit` VARCHAR(5), `hrsperWeek` VARCHAR(5), `yearID` VARCHAR(15), `dateCreated` DATETIME, `createdBy` VARCHAR(15))  BEGIN
     INSERT into subjects VALUES(id, title, description, unit, hrsperWeek, yearID, dateCreated, createdBy);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertTeacher` (IN `id` VARCHAR(15), IN `fname` VARCHAR(25), IN `mname` VARCHAR(25), IN `lname` VARCHAR(25), IN `phone` VARCHAR(13), IN `address` VARCHAR(100), IN `religion` VARCHAR(50), IN `major` VARCHAR(25), IN `minor` VARCHAR(25), IN `dob` VARCHAR(20), IN `gender` VARCHAR(10), IN `typeofUser` VARCHAR(20), IN `picture` VARCHAR(200), IN `pass` VARCHAR(200), IN `dateCreated` DATETIME, IN `createdBy` VARCHAR(15), IN `status` VARCHAR(15))  BEGIN
         INSERT into faculty_account VALUES(id, fname, mname, lname, phone, address, religion, major, minor, dob, gender, typeofUser, picture, sha1(pass), dateCreated, createdBy,status);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `loginStudent` (IN `username` VARCHAR(15), IN `pass` VARCHAR(200))  BEGIN
     SELECT * FROM studentsmalldetail1 WHERE student_id=username AND student_pass=sha1(pass);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `loginTeacher` (IN `username` VARCHAR(25), IN `pass` VARCHAR(50))  BEGIN
     SELECT * FROM facultysmalldetail1 WHERE faculty_id=username AND faculty_pass=sha1(pass) AND faculty_status="Active";
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `selectBookDetails` ()  BEGIN
     SELECT * FROM bookdetails;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `selectFacultySmallDetails` ()  BEGIN
        SELECT * FROM facultysmalldetail1 ORDER BY Name;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `selectSchoolYearDetails` ()  BEGIN
      SELECT * FROM schoolyear;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `selectStudentInfo` (`id` VARCHAR(15))  BEGIN
SELECT * from studentInfo1 where student_id=id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `selectSubjectDetails` ()  BEGIN
       SELECT * FROM subjectdetails order by year_lvl_id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `selectTeacher` ()  BEGIN
SELECT * from faculty_account;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `selectYearLevelDetails` ()  BEGIN
  SELECT * FROM yearlevelDetails;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateBook` (`id` VARCHAR(15), `title` VARCHAR(25), `description` VARCHAR(50), `dateCreated` DATETIME, `createdBy` VARCHAR(15), `level` VARCHAR(15))  BEGIN
        UPDATE book SET book_title=title, book_description=description, book_dateCreated=dateCreated, book_createdBy =createdBy  WHERE book_id=id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateBooksAcquired` (IN `cID` VARCHAR(15), IN `bID` VARCHAR(15), IN `remarks` VARCHAR(50), IN `returned` DATE, IN `type` VARCHAR(50))  BEGIN
    IF type = "Return" THEN
    UPDATE booksacquired SET date_returned=returned,
    booksacquired_remarks=remarks WHERE control_id=cID AND book_id=bID;
    
    ELSEIF type = "Issue" THEN
    UPDATE booksacquired SET date_acquired=returned,
    booksacquired_remarks=remarks WHERE control_id=cID AND book_id=bID;
    
    END IF;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateCredential` (`id` VARCHAR(13), `title` VARCHAR(25), `description` VARCHAR(50), `dateCreated` DATETIME, `createdBy` VARCHAR(15))  BEGIN
       UPDATE credential SET cr_title = title, cr_description=description, cr_creationDate = dateCreated, cr_createdBy = createdBy WHERE cr_id=id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateLevel` (`id` VARCHAR(15), `title` VARCHAR(25), `dateCreated` DATETIME, `createdBy` VARCHAR(15))  BEGIN
         UPDATE yearlevel SET year_lvl_title = title, year_lvl_dateCreated = dateCreated, year_lvl_createdBy = createdBy WHERE year_lvl_id=id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateSection` (`id` VARCHAR(15), `name` VARCHAR(25), `capacity` INT(5), `max` DOUBLE(5,2), `min` DOUBLE(5,2), `createdBy` VARCHAR(15), `dateCreated` DATETIME, `sy` VARCHAR(15))  BEGIN
        UPDATE section SET section_name=name, section_capacity= capacity, section_maxGrade = max, section_minGrade=min, section_createdBy= createdBy, section_dateCreated = dateCreated WHERE section_id=id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateStudent` (`id` VARCHAR(15), `fname` VARCHAR(50), `mname` VARCHAR(50), `lname` VARCHAR(50), `sex` VARCHAR(25), `dob` DATE, `age` INT(5), `motherTongue` VARCHAR(25), `ip` VARCHAR(50), `religion` VARCHAR(50))  BEGIN
           UPDATE STUDENT_ACCOUNT set student_fname=fname, student_mname=mname, student_lname=lname, student_sex =sex, student_birthDate=dob, student_age=age,student_motherTongue=motherTongue,student_IP=ip,student_religion=religion where student_id=id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateSubject` (`id` VARCHAR(15), `title` VARCHAR(25), `description` VARCHAR(50), `unit` VARCHAR(5), `hrsperWeek` VARCHAR(5), `level` VARCHAR(15), `dateCreated` DATETIME, `createdBy` VARCHAR(15))  BEGIN
       UPDATE subjects SET subj_title=title, subj_description=description, subj_unit = unit, subj_hrs_per_week = hrsperWeek, year_lvl_id=level, subj_dateCreated = dateCreated,
       subj_createdBy=createdBy WHERE subj_id = id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `viewBookDetails` (`id` VARCHAR(15))  BEGIN
     SELECT * FROM bookdetails WHERE book_id=id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `viewSubjectDetails` (`id` VARCHAR(15))  BEGIN
       SELECT * FROM subjectDetails where subj_id=id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `viewYearLevelDetails` (`id` VARCHAR(15))  BEGIN
  SELECT * FROM yearlevelDetails WHERE year_lvl_id = id;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

CREATE TABLE `address` (
  `student_id` varchar(15) NOT NULL,
  `student_addressHSSP` varchar(50) DEFAULT NULL,
  `student_addressBarangay` varchar(50) NOT NULL,
  `student_addressMunicipality` varchar(50) NOT NULL,
  `student_addressProvince` varchar(50) NOT NULL,
  `student_addressdateCreated` datetime NOT NULL,
  `student_addresscreatedBy` varchar(15) NOT NULL,
  `sy_id` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `adviser`
--

CREATE TABLE `adviser` (
  `faculty_id` varchar(15) NOT NULL,
  `section_id` varchar(15) NOT NULL,
  `adviser_id` varchar(15) NOT NULL,
  `adviser_dateCreated` datetime NOT NULL,
  `adviser_createdBy` varchar(45) NOT NULL,
  `sy_id` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Stand-in structure for view `advisory`
-- (See below for the actual view)
--
CREATE TABLE `advisory` (
`section_id` varchar(15)
,`Adviser` varchar(77)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `alllearnerperlevel`
-- (See below for the actual view)
--
CREATE TABLE `alllearnerperlevel` (
`sy_id` varchar(15)
,`sy_year` varchar(50)
,`gradelevel` varchar(15)
,`sex` varchar(1)
,`Total` bigint(21)
);

-- --------------------------------------------------------

--
-- Table structure for table `attendance`
--

CREATE TABLE `attendance` (
  `attendance_presentDays` int(5) NOT NULL,
  `attendance_absentDays` int(5) DEFAULT NULL,
  `attendance_tardyDays` int(5) DEFAULT NULL,
  `attendance_consecutiveAbsent` varchar(25) NOT NULL,
  `attendance_dateCreated` datetime NOT NULL,
  `month_id` varchar(15) NOT NULL,
  `faculty_id` varchar(15) NOT NULL,
  `control_id` varchar(15) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Stand-in structure for view `attendancedetails`
-- (See below for the actual view)
--
CREATE TABLE `attendancedetails` (
`control_id` varchar(15)
,`Gender` varchar(25)
,`section_id` varchar(15)
,`control_remarks` varchar(25)
,`sy_id` varchar(15)
,`attendance_presentDays` int(5)
,`attendance_absentDays` int(5)
,`attendance_tardyDays` int(5)
,`attendance_consecutiveAbsent` varchar(25)
,`month_id` varchar(15)
,`Student` varchar(153)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `average`
-- (See below for the actual view)
--
CREATE TABLE `average` (
`average` decimal(33,10)
,`control_id` varchar(15)
,`sy_id` varchar(15)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `averagedetails`
-- (See below for the actual view)
--
CREATE TABLE `averagedetails` (
`LRN` varchar(20)
,`Student` varchar(152)
,`average` decimal(27,3)
,`control_id` varchar(15)
,`sy_id` varchar(15)
,`section_id` varchar(15)
,`Gender` varchar(25)
,`GradeLevel` varchar(15)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `averageperquarter`
-- (See below for the actual view)
--
CREATE TABLE `averageperquarter` (
`average` decimal(29,6)
,`quarters_id` varchar(15)
,`control_id` varchar(15)
,`sy_id` varchar(15)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `averageperquarterdetails`
-- (See below for the actual view)
--
CREATE TABLE `averageperquarterdetails` (
`average` decimal(29,6)
,`quarters_id` varchar(15)
,`control_id` varchar(15)
,`sy_id` varchar(15)
,`section_id` varchar(15)
,`Name` varchar(152)
,`gradelevel` varchar(15)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `averagepersubjectoffering`
-- (See below for the actual view)
--
CREATE TABLE `averagepersubjectoffering` (
`average` decimal(29,6)
,`control_id` varchar(15)
,`so_id` varchar(15)
,`sy_id` varchar(15)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `averagepersubjectofferingdetails`
-- (See below for the actual view)
--
CREATE TABLE `averagepersubjectofferingdetails` (
`average` decimal(29,6)
,`control_id` varchar(15)
,`so_id` varchar(15)
,`sy_id` varchar(15)
,`Sex` varchar(1)
,`gradelevel` varchar(15)
,`section_id` varchar(15)
);

-- --------------------------------------------------------

--
-- Table structure for table `book`
--

CREATE TABLE `book` (
  `book_id` varchar(15) NOT NULL,
  `book_title` varchar(25) NOT NULL,
  `book_description` varchar(50) NOT NULL,
  `book_dateCreated` datetime NOT NULL,
  `book_createdBy` varchar(15) NOT NULL,
  `subj_id` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `book`
--

INSERT INTO `book` (`book_id`, `book_title`, `book_description`, `book_dateCreated`, `book_createdBy`, `subj_id`) VALUES
('100', 'English', '', '2018-05-03 09:39:51', '1000', '3000'),
('101', 'Filipino', '', '2018-05-03 09:39:51', '1000', '3001'),
('102', 'Science', '', '2018-05-03 09:39:52', '1000', '3002'),
('103', 'Mathematics', '', '2018-05-03 09:39:52', '1000', '3004'),
('104', 'English', '', '2018-05-03 09:39:52', '1000', '3008'),
('105', 'Filipino', '', '2018-05-03 09:39:52', '1000', '3009'),
('106', 'Science', '', '2018-05-03 09:39:52', '1000', '3010'),
('107', 'Mathematics', '', '2018-05-03 09:39:52', '1000', '3012'),
('108', 'English', '', '2018-05-03 09:39:52', '1000', '3016'),
('109', 'Filipino', '', '2018-05-03 09:39:52', '1000', '3017'),
('110', 'Science', '', '2018-05-03 09:39:52', '1000', '3018'),
('111', 'Mathematics', '', '2018-05-03 09:39:53', '1000', '3020'),
('112', 'English', '', '2018-05-03 09:39:53', '1000', '3024'),
('113', 'Filipino', '', '2018-05-03 09:39:53', '1000', '3025'),
('114', 'Science', '', '2018-05-03 09:39:53', '1000', '3026'),
('115', 'Mathematics', '', '2018-05-03 09:39:53', '1000', '3028');

-- --------------------------------------------------------

--
-- Stand-in structure for view `bookdetails`
-- (See below for the actual view)
--
CREATE TABLE `bookdetails` (
`book_id` varchar(15)
,`book_title` varchar(25)
,`book_description` varchar(50)
,`book_dateCreated` datetime
,`CreatedBy` varchar(77)
,`Subject` varchar(25)
,`Level` varchar(15)
);

-- --------------------------------------------------------

--
-- Table structure for table `booksacquired`
--

CREATE TABLE `booksacquired` (
  `control_id` varchar(15) NOT NULL,
  `book_id` varchar(15) NOT NULL,
  `booksAcquired_remarks` varchar(50) DEFAULT NULL,
  `date_acquired` date DEFAULT NULL,
  `date_returned` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Stand-in structure for view `booksacquireddetails`
-- (See below for the actual view)
--
CREATE TABLE `booksacquireddetails` (
`control_id` varchar(15)
,`Student` varchar(152)
,`sectionName` varchar(20)
,`SchoolYear` varchar(50)
,`date_acquired` date
,`date_returned` date
,`book_id` varchar(15)
,`Subject` varchar(25)
,`book_description` varchar(50)
,`booksAcquired_remarks` varchar(50)
,`Level` varchar(15)
,`Gender` varchar(25)
,`section_id` varchar(15)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `classdetails`
-- (See below for the actual view)
--
CREATE TABLE `classdetails` (
`so_id` varchar(15)
,`subjectTitle` varchar(25)
,`subjecDescription` varchar(100)
,`section_id` varchar(15)
,`control_id` varchar(15)
,`sectionName` varchar(20)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `classinformation`
-- (See below for the actual view)
--
CREATE TABLE `classinformation` (
`section_id` varchar(15)
,`section_name` varchar(20)
,`section_capacity` int(5)
,`section_maxGrade` decimal(5,2)
,`section_minGrade` decimal(5,2)
,`Cretedby` varchar(77)
,`section_dateCreated` datetime
,`GradeLevel` varchar(15)
,`Adviser` varchar(77)
,`sy_id` varchar(15)
,`year_lvl_id` varchar(15)
,`adviser_id` varchar(15)
);

-- --------------------------------------------------------

--
-- Table structure for table `control`
--

CREATE TABLE `control` (
  `control_id` varchar(15) NOT NULL,
  `control_remarks` varchar(25) DEFAULT NULL,
  `control_dateCreated` date DEFAULT NULL,
  `control_dateUpdated` date DEFAULT NULL,
  `control_createdBy` varchar(50) NOT NULL,
  `student_id` varchar(15) NOT NULL,
  `section_id` varchar(15) NOT NULL,
  `sy_id` varchar(15) NOT NULL,
  `control_reqInformation` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Stand-in structure for view `controldetails`
-- (See below for the actual view)
--
CREATE TABLE `controldetails` (
`control_id` varchar(15)
,`control_remarks` varchar(25)
,`control_dateCreated` date
,`CreatedBy` varchar(77)
,`student_id` varchar(15)
,`Student` varchar(152)
,`Gender` varchar(25)
,`LRN` varchar(20)
,`section_id` varchar(15)
,`SectionName` varchar(20)
,`SchoolYear` varchar(50)
,`control_dateUpdated` date
,`gradelevel` varchar(15)
,`control_reqInformation` varchar(100)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `controldetails1`
-- (See below for the actual view)
--
CREATE TABLE `controldetails1` (
`control_id` varchar(15)
,`control_remarks` varchar(25)
,`control_dateCreated` date
,`CreatedBy` varchar(77)
,`student_id` varchar(15)
,`Student` varchar(153)
,`Gender` varchar(25)
,`LRN` varchar(20)
,`section_id` varchar(15)
,`SectionName` varchar(20)
,`SchoolYear` varchar(50)
,`Month` varchar(9)
,`Day` int(2)
,`MonthNumber` int(2)
,`sy_id` varchar(15)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `controlinformation`
-- (See below for the actual view)
--
CREATE TABLE `controlinformation` (
`control_id` varchar(15)
,`control_remarks` varchar(25)
,`control_dateCreated` date
,`control_createdBy` varchar(50)
,`section_id` varchar(15)
,`sy_id` varchar(15)
,`sy_year` varchar(50)
,`student_id` varchar(15)
,`student_LRN` varchar(20)
,`student_fname` varchar(50)
,`student_mname` varchar(50)
,`student_lname` varchar(50)
,`Sex` varchar(1)
,`BirthDate` date
,`Age` int(6)
,`MotherTongue` varchar(50)
,`IP` varchar(50)
,`Religion` varchar(50)
,`student_age` int(5)
,`gradelevel` varchar(15)
,`control_dateUpdated` date
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `dropout`
-- (See below for the actual view)
--
CREATE TABLE `dropout` (
`Gender` varchar(25)
,`section_id` varchar(15)
,`sy_id` varchar(15)
,`Month` varchar(50)
,`count(*)` bigint(21)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `enrolmentasof1stfridayofjune`
-- (See below for the actual view)
--
CREATE TABLE `enrolmentasof1stfridayofjune` (
`Gender` varchar(25)
,`section_id` varchar(15)
,`sy_id` varchar(15)
,`Total` bigint(21)
,`month_no` varchar(15)
,`month_desc` varchar(50)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `facultysmalldetail`
-- (See below for the actual view)
--
CREATE TABLE `facultysmalldetail` (
`faculty_id` varchar(15)
,`Name` varchar(77)
,`faculty_type` varchar(20)
,`faculty_major` varchar(100)
,`faculty_minor` varchar(100)
,`faculty_status` varchar(20)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `facultysmalldetail1`
-- (See below for the actual view)
--
CREATE TABLE `facultysmalldetail1` (
`faculty_id` varchar(15)
,`Name` varchar(51)
,`faculty_type` varchar(20)
,`faculty_major` varchar(100)
,`faculty_minor` varchar(100)
,`faculty_pass` varchar(200)
,`faculty_picture` varchar(200)
,`faculty_status` varchar(20)
);

-- --------------------------------------------------------

--
-- Table structure for table `faculty_account`
--

CREATE TABLE `faculty_account` (
  `faculty_id` varchar(15) NOT NULL,
  `faculty_fname` varchar(25) NOT NULL,
  `faculty_mname` varchar(25) DEFAULT NULL,
  `faculty_lname` varchar(25) NOT NULL,
  `faculty_phone` varchar(13) DEFAULT NULL,
  `faculty_address` varchar(100) NOT NULL,
  `faculty_religion` varchar(50) DEFAULT NULL,
  `faculty_major` varchar(100) NOT NULL,
  `faculty_minor` varchar(100) NOT NULL,
  `faculty_dob` varchar(20) NOT NULL,
  `faculty_gender` varchar(10) NOT NULL,
  `faculty_type` varchar(20) NOT NULL,
  `faculty_picture` varchar(200) DEFAULT NULL,
  `faculty_pass` varchar(200) NOT NULL,
  `faculty_dateCreated` datetime NOT NULL,
  `faculty_createdBy` varchar(15) NOT NULL,
  `faculty_status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `faculty_account`
--

INSERT INTO `faculty_account` (`faculty_id`, `faculty_fname`, `faculty_mname`, `faculty_lname`, `faculty_phone`, `faculty_address`, `faculty_religion`, `faculty_major`, `faculty_minor`, `faculty_dob`, `faculty_gender`, `faculty_type`, `faculty_picture`, `faculty_pass`, `faculty_dateCreated`, `faculty_createdBy`, `faculty_status`) VALUES
('1000', 'Reymond', 'Gomera', 'Aljas', '', '', '', '', '', '', '', 'Administrator', NULL, 'e3cbba8883fe746c6e35783c9404b4bc0c7ee9eb', '2018-05-03 09:39:51', '1000', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `grade`
--

CREATE TABLE `grade` (
  `grade_value` decimal(25,2) NOT NULL DEFAULT '0.00',
  `grade_remarks` varchar(25) DEFAULT NULL,
  `grade_dateSubmit` date NOT NULL,
  `grade_dateUpdate` date NOT NULL,
  `quarters_id` varchar(15) NOT NULL,
  `faculty_id` varchar(15) NOT NULL,
  `control_id` varchar(15) NOT NULL,
  `so_id` varchar(15) NOT NULL,
  `sy_id` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Stand-in structure for view `gradedetails`
-- (See below for the actual view)
--
CREATE TABLE `gradedetails` (
`grade_value` decimal(25,2)
,`grade_remarks` varchar(25)
,`grade_dateSubmit` date
,`grade_dateUpdate` date
,`Period` varchar(25)
,`quarters_id` varchar(15)
,`faculty_id` varchar(15)
,`SubmittedBy` varchar(77)
,`control_id` varchar(15)
,`sy_id` varchar(15)
,`so_id` varchar(15)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `gradeofstudent`
-- (See below for the actual view)
--
CREATE TABLE `gradeofstudent` (
`subjectTitle` varchar(25)
,`subjecDescription` varchar(100)
,`control_id` varchar(15)
,`grade_value` decimal(25,2)
,`so_id` varchar(15)
,`quarters_id` varchar(15)
,`quarters_description` varchar(25)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `lateenrollmentduringthemonth`
-- (See below for the actual view)
--
CREATE TABLE `lateenrollmentduringthemonth` (
`Gender` varchar(25)
,`section_id` varchar(15)
,`sy_id` varchar(15)
,`Month` varchar(50)
,`Total` bigint(21)
,`month_no` varchar(15)
);

-- --------------------------------------------------------

--
-- Table structure for table `month`
--

CREATE TABLE `month` (
  `month_id` varchar(15) NOT NULL,
  `month_description` varchar(25) NOT NULL,
  `month_totalDays` int(5) NOT NULL,
  `month_status` varchar(15) DEFAULT NULL,
  `sy_id` varchar(15) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Stand-in structure for view `monthdetails`
-- (See below for the actual view)
--
CREATE TABLE `monthdetails` (
`month_id` varchar(15)
,`month_description` varchar(25)
,`month_totalDays` int(5)
,`month_status` varchar(15)
,`sy_id` varchar(15)
,`sy_year` varchar(50)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `numberofstudentsabsentfor5consecutivedays`
-- (See below for the actual view)
--
CREATE TABLE `numberofstudentsabsentfor5consecutivedays` (
`Gender` varchar(25)
,`section_id` varchar(15)
,`sy_id` varchar(15)
,`month_id` varchar(15)
,`count(*)` bigint(21)
);

-- --------------------------------------------------------

--
-- Table structure for table `parent`
--

CREATE TABLE `parent` (
  `student_id` varchar(15) NOT NULL,
  `parent_Ffname` varchar(25) DEFAULT NULL,
  `parent_Fmname` varchar(25) DEFAULT NULL,
  `parent_Flname` varchar(25) DEFAULT NULL,
  `parent_Mfname` varchar(25) DEFAULT NULL,
  `parent_Mmname` varchar(25) DEFAULT NULL,
  `parent_Mlname` varchar(25) DEFAULT NULL,
  `parent_phone` varchar(13) DEFAULT NULL,
  `guardian_name` varchar(50) DEFAULT NULL,
  `guardian_phone` varchar(13) DEFAULT NULL,
  `guardian_relationship` varchar(25) DEFAULT NULL,
  `parent_dateCreated` datetime DEFAULT NULL,
  `parent_createdBy` varchar(15) DEFAULT NULL,
  `sy_id` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Stand-in structure for view `quarterdetails`
-- (See below for the actual view)
--
CREATE TABLE `quarterdetails` (
`quarters_id` varchar(15)
,`quarters_description` varchar(25)
,`quarters_remarks` varchar(25)
,`quarters_dateStarted` date
,`quarters_dateEnd` date
,`createdBy` varchar(77)
,`sy_id` varchar(15)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `quarterlygradepersubject`
-- (See below for the actual view)
--
CREATE TABLE `quarterlygradepersubject` (
`average` decimal(29,6)
,`control_id` varchar(15)
,`sy_id` varchar(15)
,`subjectTitle` varchar(25)
,`subjecDescription` varchar(100)
,`Teacher` varchar(77)
,`faculty_id` varchar(15)
,`section_id` varchar(15)
,`section` varchar(20)
,`level` varchar(15)
,`schoolyear` varchar(15)
,`subj_id` varchar(15)
,`so_id` varchar(15)
,`student_id` varchar(15)
);

-- --------------------------------------------------------

--
-- Table structure for table `quarters`
--

CREATE TABLE `quarters` (
  `quarters_id` varchar(15) NOT NULL,
  `quarters_description` varchar(25) NOT NULL,
  `quarters_remarks` varchar(25) NOT NULL,
  `quarters_dateStarted` date NOT NULL,
  `quarters_dateEnd` date DEFAULT NULL,
  `quarters_createdBy` varchar(15) DEFAULT NULL,
  `sy_id` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `remarks_history`
--

CREATE TABLE `remarks_history` (
  `month_no` varchar(15) NOT NULL,
  `control_id` varchar(15) NOT NULL,
  `remarks` varchar(50) NOT NULL,
  `reqInformation` varchar(5) DEFAULT NULL,
  `dateUpdate` date NOT NULL,
  `month_desc` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Stand-in structure for view `remarks_historydetails`
-- (See below for the actual view)
--
CREATE TABLE `remarks_historydetails` (
`control_id` varchar(15)
,`student_id` varchar(15)
,`Student` varchar(153)
,`Gender` varchar(25)
,`LRN` varchar(20)
,`section_id` varchar(15)
,`SectionName` varchar(20)
,`SchoolYear` varchar(50)
,`sy_id` varchar(15)
,`month_no` varchar(15)
,`remarks` varchar(50)
,`reqInformation` varchar(5)
,`dateUpdate` date
,`month_desc` varchar(50)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `report_totalgender`
-- (See below for the actual view)
--
CREATE TABLE `report_totalgender` (
`schoolyear` varchar(50)
,`TotalMale` bigint(21)
,`TotalFemale` bigint(21)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `report_totallearner`
-- (See below for the actual view)
--
CREATE TABLE `report_totallearner` (
`Total` bigint(21)
,`schoolyear` varchar(50)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `report_totalstatus`
-- (See below for the actual view)
--
CREATE TABLE `report_totalstatus` (
`SchoolYear` varchar(50)
,`TI` bigint(21)
,`LE` bigint(21)
,`DO` bigint(21)
,`TO` bigint(21)
,`PTI` bigint(21)
,`OTHERS` bigint(21)
);

-- --------------------------------------------------------

--
-- Table structure for table `schoolyear`
--

CREATE TABLE `schoolyear` (
  `sy_id` varchar(15) NOT NULL,
  `sy_year` varchar(50) NOT NULL,
  `sy_dateStarted` date NOT NULL,
  `sy_dateEnd` date DEFAULT NULL,
  `sy_remarks` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `schoolyear`
--

INSERT INTO `schoolyear` (`sy_id`, `sy_year`, `sy_dateStarted`, `sy_dateEnd`, `sy_remarks`) VALUES
('1', '2018-2019', '2018-05-03', NULL, 'Open');

-- --------------------------------------------------------

--
-- Table structure for table `section`
--

CREATE TABLE `section` (
  `section_id` varchar(15) NOT NULL,
  `section_name` varchar(20) NOT NULL,
  `section_capacity` int(5) NOT NULL,
  `section_maxGrade` decimal(5,2) DEFAULT NULL,
  `section_minGrade` decimal(5,2) DEFAULT NULL,
  `section_createdBy` varchar(50) NOT NULL,
  `section_dateCreated` datetime NOT NULL,
  `year_lvl_id` varchar(15) NOT NULL,
  `sy_id` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `section`
--

INSERT INTO `section` (`section_id`, `section_name`, `section_capacity`, `section_maxGrade`, `section_minGrade`, `section_createdBy`, `section_dateCreated`, `year_lvl_id`, `sy_id`) VALUES
('6000', 'Admirable', 0, '0.00', '0.00', '1000', '2018-05-03 09:40:39', '10', '2018-2019');

-- --------------------------------------------------------

--
-- Stand-in structure for view `sectionoverall`
-- (See below for the actual view)
--
CREATE TABLE `sectionoverall` (
`section_id` varchar(15)
,`TIM` bigint(21)
,`TIF` bigint(21)
,`LEM` bigint(21)
,`LEF` bigint(21)
,`DOM` bigint(21)
,`DOF` bigint(21)
,`TOM` bigint(21)
,`TOF` bigint(21)
,`PTIM` bigint(21)
,`PTIF` bigint(21)
,`M` bigint(21)
,`F` bigint(21)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `sf5remarks`
-- (See below for the actual view)
--
CREATE TABLE `sf5remarks` (
`sy_id` varchar(15)
,`section_id` varchar(15)
,`gradelevel` varchar(15)
,`MBEGINNING` bigint(21)
,`FBEGINNING` bigint(21)
,`MDEVELOPING` bigint(21)
,`FDEVELOPING` bigint(21)
,`MAPPROACHING` bigint(21)
,`FAPPROACHING` bigint(21)
,`MPROFICIENT` bigint(21)
,`FPROFICIENT` bigint(21)
,`MADVANCED` bigint(21)
,`FADVANCED` bigint(21)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `sf5remarksperlevel`
-- (See below for the actual view)
--
CREATE TABLE `sf5remarksperlevel` (
`sy_id` varchar(15)
,`gradelevel` varchar(15)
,`MBEGINNING` decimal(42,0)
,`FBEGINNING` decimal(42,0)
,`MDEVELOPING` decimal(42,0)
,`FDEVELOPING` decimal(42,0)
,`MAPPROACHING` decimal(42,0)
,`FAPPROACHING` decimal(42,0)
,`MPROFICIENT` decimal(42,0)
,`FPROFICIENT` decimal(42,0)
,`MADVANCED` decimal(42,0)
,`FADVANCED` decimal(42,0)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `soaveragegrade`
-- (See below for the actual view)
--
CREATE TABLE `soaveragegrade` (
`average` decimal(29,6)
,`control_id` varchar(15)
,`so_id` varchar(15)
,`sy_id` varchar(15)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `studentinfo`
-- (See below for the actual view)
--
CREATE TABLE `studentinfo` (
`control_id` varchar(15)
,`student_id` varchar(15)
,`student_LRN` varchar(20)
,`Name` varchar(152)
,`sex` varchar(1)
,`BirthDate` date
,`Age` int(6)
,`BirthPlace` varchar(50)
,`MotherTongue` varchar(50)
,`IP` varchar(50)
,`Religion` varchar(50)
,`sy_id` varchar(15)
,`sy_year` varchar(50)
,`control_remarks` varchar(25)
,`section_id` varchar(15)
,`student_addressHSSP` varchar(50)
,`student_addressBarangay` varchar(50)
,`student_addressMunicipality` varchar(50)
,`student_addressProvince` varchar(50)
,`FatherName` varchar(77)
,`Mother` varchar(77)
,`GuardianName` varchar(50)
,`Relationship` varchar(25)
,`GContact` varchar(13)
,`PContact` varchar(13)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `studentinfo1`
-- (See below for the actual view)
--
CREATE TABLE `studentinfo1` (
`control_id` varchar(15)
,`student_id` varchar(15)
,`student_LRN` varchar(20)
,`Name` varchar(152)
,`sex` varchar(1)
,`BirthDate` varchar(10)
,`Age` int(6)
,`BirthPlace` varchar(50)
,`MotherTongue` varchar(50)
,`IP` varchar(50)
,`Religion` varchar(50)
,`sy_id` varchar(15)
,`sy_year` varchar(50)
,`control_remarks` varchar(25)
,`section_id` varchar(15)
,`student_addressHSSP` varchar(50)
,`student_addressBarangay` varchar(50)
,`student_addressMunicipality` varchar(50)
,`student_addressProvince` varchar(50)
,`Father` varchar(77)
,`Mother` varchar(77)
,`GuardianName` varchar(50)
,`Relationship` varchar(25)
,`GContact` varchar(13)
,`PContact` varchar(13)
,`control_dateCreated` date
,`control_dateUpdated` date
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `studentinfo1view`
-- (See below for the actual view)
--
CREATE TABLE `studentinfo1view` (
`student_id` varchar(15)
,`student_LRN` varchar(20)
,`Name` varchar(152)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `studentsmalldetail`
-- (See below for the actual view)
--
CREATE TABLE `studentsmalldetail` (
`student_id` varchar(15)
,`student_LRN` varchar(20)
,`Name` varchar(153)
,`student_sex` varchar(25)
,`DOB` varchar(73)
,`student_age` int(5)
,`student_motherTongue` varchar(50)
,`student_IP` varchar(50)
,`student_religion` varchar(50)
,`student_picture` varchar(100)
,`CreatedBy` varchar(77)
,`student_dateCreated` datetime
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `studentsmalldetail1`
-- (See below for the actual view)
--
CREATE TABLE `studentsmalldetail1` (
`student_id` varchar(15)
,`student_LRN` varchar(20)
,`Name` varchar(152)
,`student_sex` varchar(25)
,`DOB` date
,`student_age` int(5)
,`student_pass` varchar(200)
);

-- --------------------------------------------------------

--
-- Table structure for table `student_account`
--

CREATE TABLE `student_account` (
  `student_id` varchar(15) NOT NULL,
  `student_LRN` varchar(20) NOT NULL,
  `student_fname` varchar(50) NOT NULL,
  `student_mname` varchar(50) DEFAULT NULL,
  `student_lname` varchar(50) NOT NULL,
  `student_sex` varchar(25) DEFAULT NULL,
  `student_birthDate` date NOT NULL,
  `student_age` int(5) NOT NULL,
  `student_motherTongue` varchar(50) NOT NULL,
  `student_IP` varchar(50) DEFAULT NULL,
  `student_religion` varchar(50) NOT NULL,
  `student_picture` varchar(100) DEFAULT NULL,
  `student_pass` varchar(200) DEFAULT NULL,
  `student_dateCreated` datetime NOT NULL,
  `student_createdBy` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Stand-in structure for view `subjectdetails`
-- (See below for the actual view)
--
CREATE TABLE `subjectdetails` (
`subj_id` varchar(15)
,`subj_title` varchar(25)
,`subj_description` varchar(100)
,`subj_unit` varchar(5)
,`subj_hrs_per_week` varchar(5)
,`Level` varchar(15)
,`subj_dateCreated` datetime
,`CreatedBy` varchar(77)
,`year_lvl_id` varchar(15)
);

-- --------------------------------------------------------

--
-- Table structure for table `subjectoffering`
--

CREATE TABLE `subjectoffering` (
  `so_id` varchar(15) NOT NULL,
  `so_startTime` varchar(15) DEFAULT NULL,
  `so_endTime` varchar(15) DEFAULT NULL,
  `so_days` varchar(15) DEFAULT NULL,
  `so_creationDate` datetime NOT NULL,
  `so_createdBy` varchar(50) NOT NULL,
  `subj_id` varchar(15) NOT NULL,
  `faculty_id` varchar(15) DEFAULT NULL,
  `section_id` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Stand-in structure for view `subjectofferingdetails`
-- (See below for the actual view)
--
CREATE TABLE `subjectofferingdetails` (
`so_id` varchar(15)
,`so_startTime` varchar(15)
,`so_endTime` varchar(15)
,`so_days` varchar(15)
,`so_creationDate` datetime
,`CreatedBy` varchar(77)
,`SubjectTitle` varchar(25)
,`SubjecDescription` varchar(100)
,`Teacher` varchar(77)
,`faculty_id` varchar(15)
,`section_id` varchar(15)
,`Section` varchar(20)
,`Level` varchar(15)
,`schoolyear` varchar(15)
,`subj_id` varchar(15)
);

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE `subjects` (
  `subj_id` varchar(15) NOT NULL,
  `subj_title` varchar(25) NOT NULL,
  `subj_description` varchar(100) NOT NULL,
  `subj_unit` varchar(5) NOT NULL,
  `subj_hrs_per_week` varchar(5) NOT NULL,
  `year_lvl_id` varchar(15) NOT NULL,
  `subj_dateCreated` datetime DEFAULT NULL,
  `subj_createdBy` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `subjects`
--

INSERT INTO `subjects` (`subj_id`, `subj_title`, `subj_description`, `subj_unit`, `subj_hrs_per_week`, `year_lvl_id`, `subj_dateCreated`, `subj_createdBy`) VALUES
('3000', 'English', '', '', '', '10', '2018-05-03 09:39:51', '1000'),
('3001', 'Filipino', '', '', '', '10', '2018-05-03 09:39:51', '1000'),
('3002', 'Science', '', '', '', '10', '2018-05-03 09:39:51', '1000'),
('3003', 'TLE', '', '', '', '10', '2018-05-03 09:39:52', '1000'),
('3004', 'Mathematics', '', '', '', '10', '2018-05-03 09:39:52', '1000'),
('3005', 'Edukasyon sa Pagpapakatao', '', '', '', '10', '2018-05-03 09:39:52', '1000'),
('3006', 'MAPEH', '', '', '', '10', '2018-05-03 09:39:52', '1000'),
('3007', 'Araling Panlipunan', '', '', '', '10', '2018-05-03 09:39:52', '1000'),
('3008', 'English', '', '', '', '11', '2018-05-03 09:39:52', '1000'),
('3009', 'Filipino', '', '', '', '11', '2018-05-03 09:39:52', '1000'),
('3010', 'Science', '', '', '', '11', '2018-05-03 09:39:52', '1000'),
('3011', 'TLE', '', '', '', '11', '2018-05-03 09:39:52', '1000'),
('3012', 'Mathematics', '', '', '', '11', '2018-05-03 09:39:52', '1000'),
('3013', 'Edukasyon sa Pagpapakatao', '', '', '', '11', '2018-05-03 09:39:52', '1000'),
('3014', 'MAPEH', '', '', '', '11', '2018-05-03 09:39:52', '1000'),
('3015', 'Araling Panlipunan', '', '', '', '11', '2018-05-03 09:39:52', '1000'),
('3016', 'English', '', '', '', '12', '2018-05-03 09:39:52', '1000'),
('3017', 'Filipino', '', '', '', '12', '2018-05-03 09:39:52', '1000'),
('3018', 'Science', '', '', '', '12', '2018-05-03 09:39:52', '1000'),
('3019', 'TLE', '', '', '', '12', '2018-05-03 09:39:52', '1000'),
('3020', 'Mathematics', '', '', '', '12', '2018-05-03 09:39:52', '1000'),
('3021', 'Edukasyon sa Pagpapakatao', '', '', '', '12', '2018-05-03 09:39:53', '1000'),
('3022', 'MAPEH', '', '', '', '12', '2018-05-03 09:39:53', '1000'),
('3023', 'Araling Panlipunan', '', '', '', '12', '2018-05-03 09:39:53', '1000'),
('3024', 'English', '', '', '', '13', '2018-05-03 09:39:53', '1000'),
('3025', 'Filipino', '', '', '', '13', '2018-05-03 09:39:53', '1000'),
('3026', 'Science', '', '', '', '13', '2018-05-03 09:39:53', '1000'),
('3027', 'TLE', '', '', '', '13', '2018-05-03 09:39:53', '1000'),
('3028', 'Mathematics', '', '', '', '13', '2018-05-03 09:39:53', '1000'),
('3029', 'Edukasyon sa Pagpapakatao', '', '', '', '13', '2018-05-03 09:39:53', '1000'),
('3030', 'MAPEH', '', '', '', '13', '2018-05-03 09:39:53', '1000'),
('3031', 'Araling Panlipunan', '', '', '', '13', '2018-05-03 09:39:53', '1000');

-- --------------------------------------------------------

--
-- Stand-in structure for view `totaldailyattendance`
-- (See below for the actual view)
--
CREATE TABLE `totaldailyattendance` (
`Gender` varchar(25)
,`section_id` varchar(15)
,`month_id` varchar(15)
,`sy_id` varchar(15)
,`Total` decimal(32,0)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `totaldailyattendancedetails`
-- (See below for the actual view)
--
CREATE TABLE `totaldailyattendancedetails` (
`Gender` varchar(25)
,`section_id` varchar(15)
,`month_id` varchar(15)
,`sy_id` varchar(15)
,`Total` decimal(32,0)
,`GradeLevel` varchar(15)
,`year_lvl_id` varchar(15)
,`sy_year` varchar(15)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `totallearners`
-- (See below for the actual view)
--
CREATE TABLE `totallearners` (
`Total` bigint(21)
,`gradelevel` varchar(15)
,`sy_year` varchar(50)
,`TotalMale` bigint(21)
,`TotalFemale` bigint(21)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `totalunregistered`
-- (See below for the actual view)
--
CREATE TABLE `totalunregistered` (
`Gender` varchar(25)
,`section_id` varchar(15)
,`MonthNumber` varchar(15)
,`Month` varchar(50)
,`Total` bigint(21)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `transferredint`
-- (See below for the actual view)
--
CREATE TABLE `transferredint` (
`Gender` varchar(25)
,`section_id` varchar(15)
,`sy_id` varchar(15)
,`Month` varchar(50)
,`count(*)` bigint(21)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `transferredout`
-- (See below for the actual view)
--
CREATE TABLE `transferredout` (
`Gender` varchar(25)
,`section_id` varchar(15)
,`sy_id` varchar(15)
,`Month` varchar(50)
,`count(*)` bigint(21)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `unregistered`
-- (See below for the actual view)
--
CREATE TABLE `unregistered` (
`control_id` varchar(15)
,`student_id` varchar(15)
,`Student` varchar(153)
,`Gender` varchar(25)
,`LRN` varchar(20)
,`section_id` varchar(15)
,`SectionName` varchar(20)
,`SchoolYear` varchar(50)
,`sy_id` varchar(15)
,`month_no` varchar(15)
,`remarks` varchar(50)
,`reqInformation` varchar(5)
,`dateUpdate` date
,`month_desc` varchar(50)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `usangasubjectoffering`
-- (See below for the actual view)
--
CREATE TABLE `usangasubjectoffering` (
`subj_id` varchar(15)
,`SubjectTitle` varchar(25)
,`SubjecDescription` varchar(100)
,`Teacher` varchar(77)
,`faculty_id` varchar(15)
,`Level` varchar(15)
,`schoolyear` varchar(15)
,`section` varchar(20)
);

-- --------------------------------------------------------

--
-- Table structure for table `yearlevel`
--

CREATE TABLE `yearlevel` (
  `year_lvl_id` varchar(15) NOT NULL,
  `year_lvl_title` varchar(15) NOT NULL,
  `year_lvl_dateCreated` datetime NOT NULL,
  `year_lvl_createdBy` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `yearlevel`
--

INSERT INTO `yearlevel` (`year_lvl_id`, `year_lvl_title`, `year_lvl_dateCreated`, `year_lvl_createdBy`) VALUES
('10', 'Grade 7', '2018-05-03 09:39:51', '1000'),
('11', 'Grade 8', '2018-05-03 09:39:51', '1000'),
('12', 'Grade 9', '2018-05-03 09:39:51', '1000'),
('13', 'Grade 10', '2018-05-03 09:39:51', '1000');

-- --------------------------------------------------------

--
-- Stand-in structure for view `yearleveldetails`
-- (See below for the actual view)
--
CREATE TABLE `yearleveldetails` (
`year_lvl_id` varchar(15)
,`year_lvl_title` varchar(15)
,`year_lvl_dateCreated` datetime
,`CreatedBy` varchar(77)
);

-- --------------------------------------------------------

--
-- Structure for view `advisory`
--
DROP TABLE IF EXISTS `advisory`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `advisory`  AS  select `a`.`section_id` AS `section_id`,(select `f`.`Name` from `facultysmalldetail` `f` where (`f`.`faculty_id` = `a`.`faculty_id`)) AS `Adviser` from `adviser` `a` ;

-- --------------------------------------------------------

--
-- Structure for view `alllearnerperlevel`
--
DROP TABLE IF EXISTS `alllearnerperlevel`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `alllearnerperlevel`  AS  select `controlinformation`.`sy_id` AS `sy_id`,`controlinformation`.`sy_year` AS `sy_year`,`controlinformation`.`gradelevel` AS `gradelevel`,`controlinformation`.`Sex` AS `sex`,count(0) AS `Total` from `controlinformation` group by `controlinformation`.`sy_id`,`controlinformation`.`sy_year`,`controlinformation`.`gradelevel`,`controlinformation`.`Sex` ;

-- --------------------------------------------------------

--
-- Structure for view `attendancedetails`
--
DROP TABLE IF EXISTS `attendancedetails`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `attendancedetails`  AS  select `c`.`control_id` AS `control_id`,`c`.`Gender` AS `Gender`,`c`.`section_id` AS `section_id`,`c`.`control_remarks` AS `control_remarks`,`c`.`sy_id` AS `sy_id`,`a`.`attendance_presentDays` AS `attendance_presentDays`,`a`.`attendance_absentDays` AS `attendance_absentDays`,`a`.`attendance_tardyDays` AS `attendance_tardyDays`,`a`.`attendance_consecutiveAbsent` AS `attendance_consecutiveAbsent`,`a`.`month_id` AS `month_id`,`c`.`Student` AS `Student` from (`controldetails1` `c` join `attendance` `a` on((`c`.`control_id` = `a`.`control_id`))) ;

-- --------------------------------------------------------

--
-- Structure for view `average`
--
DROP TABLE IF EXISTS `average`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `average`  AS  select avg(`averageperquarter`.`average`) AS `average`,`averageperquarter`.`control_id` AS `control_id`,`averageperquarter`.`sy_id` AS `sy_id` from `averageperquarter` group by `averageperquarter`.`control_id`,`averageperquarter`.`sy_id` ;

-- --------------------------------------------------------

--
-- Structure for view `averagedetails`
--
DROP TABLE IF EXISTS `averagedetails`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `averagedetails`  AS  select `c`.`LRN` AS `LRN`,`c`.`Student` AS `Student`,round(`a`.`average`,3) AS `average`,`a`.`control_id` AS `control_id`,`a`.`sy_id` AS `sy_id`,`c`.`section_id` AS `section_id`,`c`.`Gender` AS `Gender`,`ca`.`GradeLevel` AS `GradeLevel` from ((`controldetails` `c` join `average` `a` on((`c`.`control_id` = `a`.`control_id`))) join `classinformation` `ca` on(((`c`.`section_id` = `ca`.`section_id`) and (`a`.`sy_id` = `ca`.`sy_id`)))) ;

-- --------------------------------------------------------

--
-- Structure for view `averageperquarter`
--
DROP TABLE IF EXISTS `averageperquarter`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `averageperquarter`  AS  select avg(`grade`.`grade_value`) AS `average`,`grade`.`quarters_id` AS `quarters_id`,`grade`.`control_id` AS `control_id`,`grade`.`sy_id` AS `sy_id` from `grade` group by `grade`.`quarters_id`,`grade`.`control_id`,`grade`.`sy_id` ;

-- --------------------------------------------------------

--
-- Structure for view `averageperquarterdetails`
--
DROP TABLE IF EXISTS `averageperquarterdetails`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `averageperquarterdetails`  AS  select `a`.`average` AS `average`,`a`.`quarters_id` AS `quarters_id`,`a`.`control_id` AS `control_id`,`a`.`sy_id` AS `sy_id`,`c`.`section_id` AS `section_id`,concat(`c`.`student_lname`,',',`c`.`student_fname`,',',`c`.`student_mname`) AS `Name`,`c`.`gradelevel` AS `gradelevel` from (`averageperquarter` `a` join `controlinformation` `c` on((`a`.`control_id` = `c`.`control_id`))) ;

-- --------------------------------------------------------

--
-- Structure for view `averagepersubjectoffering`
--
DROP TABLE IF EXISTS `averagepersubjectoffering`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `averagepersubjectoffering`  AS  select avg(`grade`.`grade_value`) AS `average`,`grade`.`control_id` AS `control_id`,`grade`.`so_id` AS `so_id`,`grade`.`sy_id` AS `sy_id` from `grade` group by `grade`.`control_id`,`grade`.`so_id`,`grade`.`sy_id` ;

-- --------------------------------------------------------

--
-- Structure for view `averagepersubjectofferingdetails`
--
DROP TABLE IF EXISTS `averagepersubjectofferingdetails`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `averagepersubjectofferingdetails`  AS  select `a`.`average` AS `average`,`a`.`control_id` AS `control_id`,`a`.`so_id` AS `so_id`,`a`.`sy_id` AS `sy_id`,`c`.`Sex` AS `Sex`,`c`.`gradelevel` AS `gradelevel`,`c`.`section_id` AS `section_id` from (`averagepersubjectoffering` `a` join `controlinformation` `c` on((`a`.`control_id` = `c`.`control_id`))) ;

-- --------------------------------------------------------

--
-- Structure for view `bookdetails`
--
DROP TABLE IF EXISTS `bookdetails`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `bookdetails`  AS  select `b`.`book_id` AS `book_id`,`b`.`book_title` AS `book_title`,`b`.`book_description` AS `book_description`,`b`.`book_dateCreated` AS `book_dateCreated`,(select `f`.`Name` from `facultysmalldetail` `f` where (`f`.`faculty_id` = `b`.`book_createdBy`)) AS `CreatedBy`,(select `s`.`subj_title` from `subjects` `s` where (`s`.`subj_id` = `b`.`subj_id`)) AS `Subject`,(select `sd`.`Level` from `subjectdetails` `sd` where (`sd`.`subj_id` = `b`.`subj_id`)) AS `Level` from `book` `b` ;

-- --------------------------------------------------------

--
-- Structure for view `booksacquireddetails`
--
DROP TABLE IF EXISTS `booksacquireddetails`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `booksacquireddetails`  AS  select `c`.`control_id` AS `control_id`,`c`.`Student` AS `Student`,`c`.`SectionName` AS `sectionName`,`c`.`SchoolYear` AS `SchoolYear`,`ba`.`date_acquired` AS `date_acquired`,`ba`.`date_returned` AS `date_returned`,`bd`.`book_id` AS `book_id`,`bd`.`Subject` AS `Subject`,`bd`.`book_description` AS `book_description`,`ba`.`booksAcquired_remarks` AS `booksAcquired_remarks`,`bd`.`Level` AS `Level`,`c`.`Gender` AS `Gender`,`c`.`section_id` AS `section_id` from ((`controldetails` `c` join `booksacquired` `ba` on((`c`.`control_id` = `ba`.`control_id`))) join `bookdetails` `bd` on((`bd`.`book_id` = `ba`.`book_id`))) ;

-- --------------------------------------------------------

--
-- Structure for view `classdetails`
--
DROP TABLE IF EXISTS `classdetails`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `classdetails`  AS  select `so`.`so_id` AS `so_id`,`so`.`SubjectTitle` AS `subjectTitle`,`so`.`SubjecDescription` AS `subjecDescription`,`so`.`section_id` AS `section_id`,`c`.`control_id` AS `control_id`,`c`.`SectionName` AS `sectionName` from (`subjectofferingdetails` `so` join `controldetails` `c` on((`so`.`section_id` = `c`.`section_id`))) ;

-- --------------------------------------------------------

--
-- Structure for view `classinformation`
--
DROP TABLE IF EXISTS `classinformation`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `classinformation`  AS  select `s`.`section_id` AS `section_id`,`s`.`section_name` AS `section_name`,`s`.`section_capacity` AS `section_capacity`,`s`.`section_maxGrade` AS `section_maxGrade`,`s`.`section_minGrade` AS `section_minGrade`,(select `f`.`Name` from `facultysmalldetail` `f` where (`f`.`faculty_id` = `s`.`section_createdBy`)) AS `Cretedby`,`s`.`section_dateCreated` AS `section_dateCreated`,(select `y`.`year_lvl_title` from `yearlevel` `y` where (`y`.`year_lvl_id` = `s`.`year_lvl_id`)) AS `GradeLevel`,(select `a`.`Adviser` from `advisory` `a` where (`a`.`section_id` = `s`.`section_id`)) AS `Adviser`,`s`.`sy_id` AS `sy_id`,`s`.`year_lvl_id` AS `year_lvl_id`,(select `a`.`faculty_id` from `adviser` `a` where (`a`.`section_id` = `s`.`section_id`)) AS `adviser_id` from `section` `s` ;

-- --------------------------------------------------------

--
-- Structure for view `controldetails`
--
DROP TABLE IF EXISTS `controldetails`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `controldetails`  AS  select `c`.`control_id` AS `control_id`,`c`.`control_remarks` AS `control_remarks`,`c`.`control_dateCreated` AS `control_dateCreated`,(select `f`.`Name` from `facultysmalldetail` `f` where (`f`.`faculty_id` = `c`.`control_createdBy`)) AS `CreatedBy`,`c`.`student_id` AS `student_id`,(select distinct `s`.`Name` from `studentinfo1` `s` where (`s`.`student_id` = `c`.`student_id`)) AS `Student`,(select `s`.`student_sex` from `studentsmalldetail` `s` where (`s`.`student_id` = `c`.`student_id`)) AS `Gender`,(select `s`.`student_LRN` from `studentsmalldetail` `s` where (`s`.`student_id` = `c`.`student_id`)) AS `LRN`,`c`.`section_id` AS `section_id`,(select `ci`.`section_name` from `classinformation` `ci` where (`ci`.`section_id` = `c`.`section_id`)) AS `SectionName`,(select `sy`.`sy_year` from `schoolyear` `sy` where (`sy`.`sy_id` = `c`.`sy_id`)) AS `SchoolYear`,`c`.`control_dateUpdated` AS `control_dateUpdated`,(select `ci`.`GradeLevel` from `classinformation` `ci` where (`ci`.`section_id` = `c`.`section_id`)) AS `gradelevel`,`c`.`control_reqInformation` AS `control_reqInformation` from `control` `c` ;

-- --------------------------------------------------------

--
-- Structure for view `controldetails1`
--
DROP TABLE IF EXISTS `controldetails1`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `controldetails1`  AS  select `c`.`control_id` AS `control_id`,`c`.`control_remarks` AS `control_remarks`,`c`.`control_dateCreated` AS `control_dateCreated`,(select `f`.`Name` from `facultysmalldetail` `f` where (`f`.`faculty_id` = `c`.`control_createdBy`)) AS `CreatedBy`,`c`.`student_id` AS `student_id`,(select `s`.`Name` from `studentsmalldetail` `s` where (`s`.`student_id` = `c`.`student_id`)) AS `Student`,(select `s`.`student_sex` from `studentsmalldetail` `s` where (`s`.`student_id` = `c`.`student_id`)) AS `Gender`,(select `s`.`student_LRN` from `studentsmalldetail` `s` where (`s`.`student_id` = `c`.`student_id`)) AS `LRN`,`c`.`section_id` AS `section_id`,(select `ci`.`section_name` from `classinformation` `ci` where (`ci`.`section_id` = `c`.`section_id`)) AS `SectionName`,(select `sy`.`sy_year` from `schoolyear` `sy` where (`sy`.`sy_id` = `c`.`sy_id`)) AS `SchoolYear`,monthname(`c`.`control_dateUpdated`) AS `Month`,dayofmonth(`c`.`control_dateUpdated`) AS `Day`,month(`c`.`control_dateUpdated`) AS `MonthNumber`,`c`.`sy_id` AS `sy_id` from `control` `c` ;

-- --------------------------------------------------------

--
-- Structure for view `controlinformation`
--
DROP TABLE IF EXISTS `controlinformation`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `controlinformation`  AS  select `c`.`control_id` AS `control_id`,`c`.`control_remarks` AS `control_remarks`,`c`.`control_dateCreated` AS `control_dateCreated`,`c`.`control_createdBy` AS `control_createdBy`,`sec`.`section_id` AS `section_id`,`sy`.`sy_id` AS `sy_id`,`sy`.`sy_year` AS `sy_year`,`sa`.`student_id` AS `student_id`,`sa`.`student_LRN` AS `student_LRN`,`sa`.`student_fname` AS `student_fname`,`sa`.`student_mname` AS `student_mname`,`sa`.`student_lname` AS `student_lname`,(case when (`sa`.`student_sex` = 'Male') then 'M' when (`sa`.`student_sex` = 'Female') then 'F' end) AS `Sex`,`sa`.`student_birthDate` AS `BirthDate`,((year(curdate()) - year(`sa`.`student_birthDate`)) - (dayofyear(curdate()) < dayofyear(`sa`.`student_birthDate`))) AS `Age`,`sa`.`student_motherTongue` AS `MotherTongue`,`sa`.`student_IP` AS `IP`,`sa`.`student_religion` AS `Religion`,`sa`.`student_age` AS `student_age`,`sec`.`GradeLevel` AS `gradelevel`,`c`.`control_dateUpdated` AS `control_dateUpdated` from (((`student_account` `sa` join `control` `c` on((`sa`.`student_id` = `c`.`student_id`))) join `classinformation` `sec` on((`c`.`section_id` = `sec`.`section_id`))) join `schoolyear` `sy` on((`c`.`sy_id` = `sy`.`sy_id`))) ;

-- --------------------------------------------------------

--
-- Structure for view `dropout`
--
DROP TABLE IF EXISTS `dropout`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `dropout`  AS  select `remarks_historydetails`.`Gender` AS `Gender`,`remarks_historydetails`.`section_id` AS `section_id`,`remarks_historydetails`.`sy_id` AS `sy_id`,`remarks_historydetails`.`month_desc` AS `Month`,count(0) AS `count(*)` from `remarks_historydetails` where (`remarks_historydetails`.`remarks` = 'DRP') group by `remarks_historydetails`.`Gender`,`remarks_historydetails`.`section_id`,`remarks_historydetails`.`sy_id`,`Month` ;

-- --------------------------------------------------------

--
-- Structure for view `enrolmentasof1stfridayofjune`
--
DROP TABLE IF EXISTS `enrolmentasof1stfridayofjune`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `enrolmentasof1stfridayofjune`  AS  select `remarks_historydetails`.`Gender` AS `Gender`,`remarks_historydetails`.`section_id` AS `section_id`,`remarks_historydetails`.`sy_id` AS `sy_id`,count(0) AS `Total`,`remarks_historydetails`.`month_no` AS `month_no`,`remarks_historydetails`.`month_desc` AS `month_desc` from `remarks_historydetails` where (`remarks_historydetails`.`remarks` <> 'LE') group by `remarks_historydetails`.`sy_id`,`remarks_historydetails`.`section_id`,`remarks_historydetails`.`Gender`,`remarks_historydetails`.`month_no`,`remarks_historydetails`.`month_desc` ;

-- --------------------------------------------------------

--
-- Structure for view `facultysmalldetail`
--
DROP TABLE IF EXISTS `facultysmalldetail`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `facultysmalldetail`  AS  select `faculty_account`.`faculty_id` AS `faculty_id`,concat(`faculty_account`.`faculty_fname`,' ',`faculty_account`.`faculty_mname`,' ',`faculty_account`.`faculty_lname`) AS `Name`,`faculty_account`.`faculty_type` AS `faculty_type`,`faculty_account`.`faculty_major` AS `faculty_major`,`faculty_account`.`faculty_minor` AS `faculty_minor`,`faculty_account`.`faculty_status` AS `faculty_status` from `faculty_account` group by `faculty_account`.`faculty_id` ;

-- --------------------------------------------------------

--
-- Structure for view `facultysmalldetail1`
--
DROP TABLE IF EXISTS `facultysmalldetail1`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `facultysmalldetail1`  AS  select `faculty_account`.`faculty_id` AS `faculty_id`,concat(`faculty_account`.`faculty_fname`,' ',`faculty_account`.`faculty_lname`) AS `Name`,`faculty_account`.`faculty_type` AS `faculty_type`,`faculty_account`.`faculty_major` AS `faculty_major`,`faculty_account`.`faculty_minor` AS `faculty_minor`,`faculty_account`.`faculty_pass` AS `faculty_pass`,`faculty_account`.`faculty_picture` AS `faculty_picture`,`faculty_account`.`faculty_status` AS `faculty_status` from `faculty_account` group by `faculty_account`.`faculty_id` ;

-- --------------------------------------------------------

--
-- Structure for view `gradedetails`
--
DROP TABLE IF EXISTS `gradedetails`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `gradedetails`  AS  select `g`.`grade_value` AS `grade_value`,`g`.`grade_remarks` AS `grade_remarks`,`g`.`grade_dateSubmit` AS `grade_dateSubmit`,`g`.`grade_dateUpdate` AS `grade_dateUpdate`,(select `q`.`quarters_description` from `quarters` `q` where (`q`.`quarters_id` = `g`.`quarters_id`)) AS `Period`,`g`.`quarters_id` AS `quarters_id`,`g`.`faculty_id` AS `faculty_id`,(select `f`.`Name` from `facultysmalldetail` `f` where (`f`.`faculty_id` = `g`.`faculty_id`)) AS `SubmittedBy`,`g`.`control_id` AS `control_id`,`g`.`sy_id` AS `sy_id`,`g`.`so_id` AS `so_id` from `grade` `g` ;

-- --------------------------------------------------------

--
-- Structure for view `gradeofstudent`
--
DROP TABLE IF EXISTS `gradeofstudent`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `gradeofstudent`  AS  select `cd`.`subjectTitle` AS `subjectTitle`,`cd`.`subjecDescription` AS `subjecDescription`,`cd`.`control_id` AS `control_id`,`g`.`grade_value` AS `grade_value`,`g`.`so_id` AS `so_id`,`q`.`quarters_id` AS `quarters_id`,`q`.`quarters_description` AS `quarters_description` from ((`grade` `g` join `classdetails` `cd` on((`g`.`so_id` = `cd`.`so_id`))) join `quarters` `q` on((`q`.`quarters_id` = `g`.`quarters_id`))) ;

-- --------------------------------------------------------

--
-- Structure for view `lateenrollmentduringthemonth`
--
DROP TABLE IF EXISTS `lateenrollmentduringthemonth`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `lateenrollmentduringthemonth`  AS  select `remarks_historydetails`.`Gender` AS `Gender`,`remarks_historydetails`.`section_id` AS `section_id`,`remarks_historydetails`.`sy_id` AS `sy_id`,`remarks_historydetails`.`month_desc` AS `Month`,count(0) AS `Total`,`remarks_historydetails`.`month_no` AS `month_no` from `remarks_historydetails` where (`remarks_historydetails`.`remarks` = 'LE') group by `remarks_historydetails`.`sy_id`,`remarks_historydetails`.`section_id`,`remarks_historydetails`.`Gender`,`Month`,`remarks_historydetails`.`month_no` ;

-- --------------------------------------------------------

--
-- Structure for view `monthdetails`
--
DROP TABLE IF EXISTS `monthdetails`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `monthdetails`  AS  select `m`.`month_id` AS `month_id`,`m`.`month_description` AS `month_description`,`m`.`month_totalDays` AS `month_totalDays`,`m`.`month_status` AS `month_status`,`m`.`sy_id` AS `sy_id`,`s`.`sy_year` AS `sy_year` from (`month` `m` join `schoolyear` `s` on((`m`.`sy_id` = `s`.`sy_id`))) ;

-- --------------------------------------------------------

--
-- Structure for view `numberofstudentsabsentfor5consecutivedays`
--
DROP TABLE IF EXISTS `numberofstudentsabsentfor5consecutivedays`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `numberofstudentsabsentfor5consecutivedays`  AS  select `attendancedetails`.`Gender` AS `Gender`,`attendancedetails`.`section_id` AS `section_id`,`attendancedetails`.`sy_id` AS `sy_id`,`attendancedetails`.`month_id` AS `month_id`,count(0) AS `count(*)` from `attendancedetails` where (`attendancedetails`.`attendance_consecutiveAbsent` = 'Yes') group by `attendancedetails`.`Gender`,`attendancedetails`.`section_id`,`attendancedetails`.`sy_id`,`attendancedetails`.`month_id` ;

-- --------------------------------------------------------

--
-- Structure for view `quarterdetails`
--
DROP TABLE IF EXISTS `quarterdetails`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `quarterdetails`  AS  select `q`.`quarters_id` AS `quarters_id`,`q`.`quarters_description` AS `quarters_description`,`q`.`quarters_remarks` AS `quarters_remarks`,`q`.`quarters_dateStarted` AS `quarters_dateStarted`,`q`.`quarters_dateEnd` AS `quarters_dateEnd`,(select `f`.`Name` from `facultysmalldetail` `f` where (`f`.`faculty_id` = `q`.`quarters_createdBy`)) AS `createdBy`,`q`.`sy_id` AS `sy_id` from `quarters` `q` ;

-- --------------------------------------------------------

--
-- Structure for view `quarterlygradepersubject`
--
DROP TABLE IF EXISTS `quarterlygradepersubject`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `quarterlygradepersubject`  AS  select `sag`.`average` AS `average`,`sag`.`control_id` AS `control_id`,`sag`.`sy_id` AS `sy_id`,`sod`.`SubjectTitle` AS `subjectTitle`,`sod`.`SubjecDescription` AS `subjecDescription`,`sod`.`Teacher` AS `Teacher`,`sod`.`faculty_id` AS `faculty_id`,`sod`.`section_id` AS `section_id`,`sod`.`Section` AS `section`,`sod`.`Level` AS `level`,`sod`.`schoolyear` AS `schoolyear`,`sod`.`subj_id` AS `subj_id`,`sod`.`so_id` AS `so_id`,`cf`.`student_id` AS `student_id` from ((`soaveragegrade` `sag` join `subjectofferingdetails` `sod` on((`sag`.`so_id` = `sod`.`so_id`))) join `controlinformation` `cf` on((`sag`.`control_id` = `cf`.`control_id`))) ;

-- --------------------------------------------------------

--
-- Structure for view `remarks_historydetails`
--
DROP TABLE IF EXISTS `remarks_historydetails`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `remarks_historydetails`  AS  select `c`.`control_id` AS `control_id`,`c`.`student_id` AS `student_id`,`c`.`Student` AS `Student`,`c`.`Gender` AS `Gender`,`c`.`LRN` AS `LRN`,`c`.`section_id` AS `section_id`,`c`.`SectionName` AS `SectionName`,`c`.`SchoolYear` AS `SchoolYear`,`c`.`sy_id` AS `sy_id`,`r`.`month_no` AS `month_no`,`r`.`remarks` AS `remarks`,`r`.`reqInformation` AS `reqInformation`,`r`.`dateUpdate` AS `dateUpdate`,`r`.`month_desc` AS `month_desc` from (`controldetails1` `c` join `remarks_history` `r` on((`c`.`control_id` = `r`.`control_id`))) ;

-- --------------------------------------------------------

--
-- Structure for view `report_totalgender`
--
DROP TABLE IF EXISTS `report_totalgender`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `report_totalgender`  AS  select `c`.`SchoolYear` AS `schoolyear`,(select count(0) from `controldetails` where ((`controldetails`.`Gender` = 'Male') and (`controldetails`.`SchoolYear` = `c`.`SchoolYear`))) AS `TotalMale`,(select count(0) from `controldetails` where ((`controldetails`.`Gender` = 'Female') and (`controldetails`.`SchoolYear` = `c`.`SchoolYear`))) AS `TotalFemale` from `controldetails` `c` group by `c`.`SchoolYear` order by `c`.`SchoolYear` ;

-- --------------------------------------------------------

--
-- Structure for view `report_totallearner`
--
DROP TABLE IF EXISTS `report_totallearner`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `report_totallearner`  AS  select count(0) AS `Total`,`controldetails`.`SchoolYear` AS `schoolyear` from `controldetails` group by `controldetails`.`SchoolYear` order by `controldetails`.`SchoolYear` ;

-- --------------------------------------------------------

--
-- Structure for view `report_totalstatus`
--
DROP TABLE IF EXISTS `report_totalstatus`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `report_totalstatus`  AS  select `s`.`SchoolYear` AS `SchoolYear`,(select count(0) from `controldetails` `c` where ((`c`.`control_remarks` = 'T/I') and (`c`.`SchoolYear` = `s`.`SchoolYear`))) AS `TI`,(select count(0) from `controldetails` `c` where ((`c`.`control_remarks` = 'LE') and (`c`.`SchoolYear` = `s`.`SchoolYear`))) AS `LE`,(select count(0) from `controldetails` `c` where ((`c`.`control_remarks` = 'DRP') and (`c`.`SchoolYear` = `s`.`SchoolYear`))) AS `DO`,(select count(0) from `controldetails` `c` where ((`c`.`control_remarks` = 'T/O') and (`c`.`SchoolYear` = `s`.`SchoolYear`))) AS `TO`,(select count(0) from `controldetails` `c` where ((`c`.`control_remarks` <> 'LE') and (`c`.`control_remarks` <> 'DRP') and (`c`.`control_remarks` <> 'T/I') and (`c`.`control_remarks` <> 'T/O') and (`c`.`control_remarks` <> '') and (`c`.`SchoolYear` = `s`.`SchoolYear`))) AS `PTI`,(select count(0) from `controldetails` `c` where ((`c`.`control_remarks` <> 'LE') and (`c`.`control_remarks` <> 'DRP') and (`c`.`control_remarks` <> 'T/I') and (`c`.`control_remarks` <> 'T/O') and (`c`.`control_remarks` = '') and (`c`.`SchoolYear` = `s`.`SchoolYear`))) AS `OTHERS` from `controldetails` `s` group by `s`.`SchoolYear` order by `s`.`SchoolYear` ;

-- --------------------------------------------------------

--
-- Structure for view `sectionoverall`
--
DROP TABLE IF EXISTS `sectionoverall`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `sectionoverall`  AS  select `s`.`section_id` AS `section_id`,(select count(0) from `controldetails` `c` where ((`c`.`control_remarks` = 'T/I') and (`c`.`Gender` = 'Male') and (`c`.`section_id` = `s`.`section_id`))) AS `TIM`,(select count(0) from `controldetails` `c` where ((`c`.`control_remarks` = 'T/I') and (`c`.`Gender` = 'Female') and (`c`.`section_id` = `s`.`section_id`))) AS `TIF`,(select count(0) from `controldetails` `c` where ((`c`.`control_remarks` = 'LE') and (`c`.`Gender` = 'Male') and (`c`.`section_id` = `s`.`section_id`))) AS `LEM`,(select count(0) from `controldetails` `c` where ((`c`.`control_remarks` = 'LE') and (`c`.`Gender` = 'Female') and (`c`.`section_id` = `s`.`section_id`))) AS `LEF`,(select count(0) from `controldetails` `c` where ((`c`.`control_remarks` = 'DRP') and (`c`.`Gender` = 'Male') and (`c`.`section_id` = `s`.`section_id`))) AS `DOM`,(select count(0) from `controldetails` `c` where ((`c`.`control_remarks` = 'DRP') and (`c`.`Gender` = 'Female') and (`c`.`section_id` = `s`.`section_id`))) AS `DOF`,(select count(0) from `controldetails` `c` where ((`c`.`control_remarks` = 'T/O') and (`c`.`Gender` = 'Male') and (`c`.`section_id` = `s`.`section_id`))) AS `TOM`,(select count(0) from `controldetails` `c` where ((`c`.`control_remarks` = 'T/O') and (`c`.`Gender` = 'Female') and (`c`.`section_id` = `s`.`section_id`))) AS `TOF`,(select count(0) from `controldetails` `c` where ((`c`.`control_remarks` <> 'LE') and (`c`.`control_remarks` <> 'DRP') and (`c`.`control_remarks` <> 'T/I') and (`c`.`control_remarks` <> 'T.O') and (`c`.`control_remarks` <> '') and (`c`.`Gender` = 'Male') and (`c`.`section_id` = `s`.`section_id`))) AS `PTIM`,(select count(0) from `controldetails` `c` where ((`c`.`control_remarks` <> 'LE') and (`c`.`control_remarks` <> 'DRP') and (`c`.`control_remarks` <> 'T/I') and (`c`.`control_remarks` <> 'T.O') and (`c`.`control_remarks` <> '') and (`c`.`Gender` = 'Female') and (`c`.`section_id` = `s`.`section_id`))) AS `PTIF`,(select count(0) from `controldetails` `c` where ((`c`.`Gender` = 'Male') and (`c`.`section_id` = `s`.`section_id`))) AS `M`,(select count(0) from `controldetails` `c` where ((`c`.`Gender` = 'Female') and (`c`.`section_id` = `s`.`section_id`))) AS `F` from `controldetails` `s` group by `s`.`section_id` ;

-- --------------------------------------------------------

--
-- Structure for view `sf5remarks`
--
DROP TABLE IF EXISTS `sf5remarks`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `sf5remarks`  AS  select `aa`.`sy_id` AS `sy_id`,`aa`.`section_id` AS `section_id`,`aa`.`GradeLevel` AS `gradelevel`,(select count(0) from `averagedetails` `a` where ((`a`.`average` <= 74) and (`a`.`Gender` = 'Male') and (`a`.`section_id` = `aa`.`section_id`))) AS `MBEGINNING`,(select count(0) from `averagedetails` `a` where ((`a`.`average` <= 74) and (`a`.`Gender` = 'Female') and (`a`.`section_id` = `aa`.`section_id`))) AS `FBEGINNING`,(select count(0) from `averagedetails` `a` where ((`a`.`average` between 75 and 79) and (`a`.`Gender` = 'Male') and (`a`.`section_id` = `aa`.`section_id`))) AS `MDEVELOPING`,(select count(0) from `averagedetails` `a` where ((`a`.`average` between 75 and 79) and (`a`.`Gender` = 'Female') and (`a`.`section_id` = `aa`.`section_id`))) AS `FDEVELOPING`,(select count(0) from `averagedetails` `a` where ((`a`.`average` between 80 and 84) and (`a`.`Gender` = 'Male') and (`a`.`section_id` = `aa`.`section_id`))) AS `MAPPROACHING`,(select count(0) from `averagedetails` `a` where ((`a`.`average` between 80 and 84) and (`a`.`Gender` = 'Female') and (`a`.`section_id` = `aa`.`section_id`))) AS `FAPPROACHING`,(select count(0) from `averagedetails` `a` where ((`a`.`average` between 85 and 89) and (`a`.`Gender` = 'Male') and (`a`.`section_id` = `aa`.`section_id`))) AS `MPROFICIENT`,(select count(0) from `averagedetails` `a` where ((`a`.`average` between 85 and 89) and (`a`.`Gender` = 'Female') and (`a`.`section_id` = `aa`.`section_id`))) AS `FPROFICIENT`,(select count(0) from `averagedetails` `a` where ((`a`.`average` >= 90) and (`a`.`Gender` = 'Male') and (`a`.`section_id` = `aa`.`section_id`))) AS `MADVANCED`,(select count(0) from `averagedetails` `a` where ((`a`.`average` >= 90) and (`a`.`Gender` = 'Female') and (`a`.`section_id` = `aa`.`section_id`))) AS `FADVANCED` from `averagedetails` `aa` group by `aa`.`section_id`,`aa`.`sy_id`,`aa`.`GradeLevel` ;

-- --------------------------------------------------------

--
-- Structure for view `sf5remarksperlevel`
--
DROP TABLE IF EXISTS `sf5remarksperlevel`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `sf5remarksperlevel`  AS  select `sf5remarks`.`sy_id` AS `sy_id`,`sf5remarks`.`gradelevel` AS `gradelevel`,sum(`sf5remarks`.`MBEGINNING`) AS `MBEGINNING`,sum(`sf5remarks`.`FBEGINNING`) AS `FBEGINNING`,sum(`sf5remarks`.`MDEVELOPING`) AS `MDEVELOPING`,sum(`sf5remarks`.`FDEVELOPING`) AS `FDEVELOPING`,sum(`sf5remarks`.`MAPPROACHING`) AS `MAPPROACHING`,sum(`sf5remarks`.`FAPPROACHING`) AS `FAPPROACHING`,sum(`sf5remarks`.`MPROFICIENT`) AS `MPROFICIENT`,sum(`sf5remarks`.`FPROFICIENT`) AS `FPROFICIENT`,sum(`sf5remarks`.`MADVANCED`) AS `MADVANCED`,sum(`sf5remarks`.`FADVANCED`) AS `FADVANCED` from `sf5remarks` group by `sf5remarks`.`sy_id`,`sf5remarks`.`gradelevel` ;

-- --------------------------------------------------------

--
-- Structure for view `soaveragegrade`
--
DROP TABLE IF EXISTS `soaveragegrade`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `soaveragegrade`  AS  select avg(`gradedetails`.`grade_value`) AS `average`,`gradedetails`.`control_id` AS `control_id`,`gradedetails`.`so_id` AS `so_id`,`gradedetails`.`sy_id` AS `sy_id` from `gradedetails` group by `gradedetails`.`control_id`,`gradedetails`.`sy_id`,`gradedetails`.`so_id` ;

-- --------------------------------------------------------

--
-- Structure for view `studentinfo`
--
DROP TABLE IF EXISTS `studentinfo`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `studentinfo`  AS  select `ci`.`control_id` AS `control_id`,`ci`.`student_id` AS `student_id`,`ci`.`student_LRN` AS `student_LRN`,concat(`ci`.`student_fname`,' ',`ci`.`student_mname`,' ',`ci`.`student_lname`) AS `Name`,`ci`.`Sex` AS `sex`,`ci`.`BirthDate` AS `BirthDate`,`ci`.`Age` AS `Age`,`a`.`student_addressProvince` AS `BirthPlace`,`ci`.`MotherTongue` AS `MotherTongue`,`ci`.`IP` AS `IP`,`ci`.`Religion` AS `Religion`,`ci`.`sy_id` AS `sy_id`,`ci`.`sy_year` AS `sy_year`,`ci`.`control_remarks` AS `control_remarks`,`ci`.`section_id` AS `section_id`,`a`.`student_addressHSSP` AS `student_addressHSSP`,`a`.`student_addressBarangay` AS `student_addressBarangay`,`a`.`student_addressMunicipality` AS `student_addressMunicipality`,`a`.`student_addressProvince` AS `student_addressProvince`,(case when (`p`.`parent_Flname` = `ci`.`student_lname`) then `p`.`parent_Flname` when (`p`.`parent_Flname` <> `ci`.`student_lname`) then concat(`p`.`parent_Ffname`,' ',`p`.`parent_Fmname`,' ',`p`.`parent_Flname`) end) AS `FatherName`,concat(`p`.`parent_Mfname`,' ',`p`.`parent_Mmname`,' ',`p`.`parent_Mlname`) AS `Mother`,`p`.`guardian_name` AS `GuardianName`,`p`.`guardian_relationship` AS `Relationship`,`p`.`guardian_phone` AS `GContact`,`p`.`parent_phone` AS `PContact` from ((`controlinformation` `ci` join `address` `a` on((`ci`.`student_id` = `a`.`student_id`))) join `parent` `p` on((`ci`.`student_id` = `p`.`student_id`))) ;

-- --------------------------------------------------------

--
-- Structure for view `studentinfo1`
--
DROP TABLE IF EXISTS `studentinfo1`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `studentinfo1`  AS  select `ci`.`control_id` AS `control_id`,`ci`.`student_id` AS `student_id`,`ci`.`student_LRN` AS `student_LRN`,concat(`ci`.`student_lname`,',',`ci`.`student_fname`,',',`ci`.`student_mname`) AS `Name`,`ci`.`Sex` AS `sex`,date_format(`ci`.`BirthDate`,'%m/%d/%Y') AS `BirthDate`,`ci`.`Age` AS `Age`,`a`.`student_addressProvince` AS `BirthPlace`,`ci`.`MotherTongue` AS `MotherTongue`,`ci`.`IP` AS `IP`,`ci`.`Religion` AS `Religion`,`ci`.`sy_id` AS `sy_id`,`ci`.`sy_year` AS `sy_year`,`ci`.`control_remarks` AS `control_remarks`,`ci`.`section_id` AS `section_id`,`a`.`student_addressHSSP` AS `student_addressHSSP`,`a`.`student_addressBarangay` AS `student_addressBarangay`,`a`.`student_addressMunicipality` AS `student_addressMunicipality`,`a`.`student_addressProvince` AS `student_addressProvince`,concat(`p`.`parent_Flname`,',',`p`.`parent_Ffname`,',',`p`.`parent_Fmname`) AS `Father`,concat(`p`.`parent_Mlname`,',',`p`.`parent_Mfname`,',',`p`.`parent_Mmname`) AS `Mother`,`p`.`guardian_name` AS `GuardianName`,`p`.`guardian_relationship` AS `Relationship`,`p`.`guardian_phone` AS `GContact`,`p`.`parent_phone` AS `PContact`,`ci`.`control_dateCreated` AS `control_dateCreated`,`ci`.`control_dateUpdated` AS `control_dateUpdated` from ((`controlinformation` `ci` join `address` `a` on((`ci`.`student_id` = `a`.`student_id`))) join `parent` `p` on((`ci`.`student_id` = `p`.`student_id`))) ;

-- --------------------------------------------------------

--
-- Structure for view `studentinfo1view`
--
DROP TABLE IF EXISTS `studentinfo1view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `studentinfo1view`  AS  select distinct `studentinfo1`.`student_id` AS `student_id`,`studentinfo1`.`student_LRN` AS `student_LRN`,`studentinfo1`.`Name` AS `Name` from `studentinfo1` ;

-- --------------------------------------------------------

--
-- Structure for view `studentsmalldetail`
--
DROP TABLE IF EXISTS `studentsmalldetail`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `studentsmalldetail`  AS  select `student_account`.`student_id` AS `student_id`,`student_account`.`student_LRN` AS `student_LRN`,(select concat(`student_account`.`student_lname`,', ',`student_account`.`student_fname`,' ',`student_account`.`student_mname`)) AS `Name`,`student_account`.`student_sex` AS `student_sex`,date_format(`student_account`.`student_birthDate`,'%M %e, %Y') AS `DOB`,`student_account`.`student_age` AS `student_age`,`student_account`.`student_motherTongue` AS `student_motherTongue`,`student_account`.`student_IP` AS `student_IP`,`student_account`.`student_religion` AS `student_religion`,`student_account`.`student_picture` AS `student_picture`,(select `facultysmalldetail`.`Name` from `facultysmalldetail` where (`facultysmalldetail`.`faculty_id` = `student_account`.`student_createdBy`)) AS `CreatedBy`,`student_account`.`student_dateCreated` AS `student_dateCreated` from `student_account` ;

-- --------------------------------------------------------

--
-- Structure for view `studentsmalldetail1`
--
DROP TABLE IF EXISTS `studentsmalldetail1`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `studentsmalldetail1`  AS  select `student_account`.`student_id` AS `student_id`,`student_account`.`student_LRN` AS `student_LRN`,concat(`student_account`.`student_fname`,' ',`student_account`.`student_mname`,' ',`student_account`.`student_lname`) AS `Name`,`student_account`.`student_sex` AS `student_sex`,`student_account`.`student_birthDate` AS `DOB`,`student_account`.`student_age` AS `student_age`,`student_account`.`student_pass` AS `student_pass` from `student_account` ;

-- --------------------------------------------------------

--
-- Structure for view `subjectdetails`
--
DROP TABLE IF EXISTS `subjectdetails`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `subjectdetails`  AS  select `s`.`subj_id` AS `subj_id`,`s`.`subj_title` AS `subj_title`,`s`.`subj_description` AS `subj_description`,`s`.`subj_unit` AS `subj_unit`,`s`.`subj_hrs_per_week` AS `subj_hrs_per_week`,(select `y`.`year_lvl_title` from `yearlevel` `y` where (`y`.`year_lvl_id` = `s`.`year_lvl_id`)) AS `Level`,`s`.`subj_dateCreated` AS `subj_dateCreated`,(select `f`.`Name` from `facultysmalldetail` `f` where (`f`.`faculty_id` = `s`.`subj_createdBy`)) AS `CreatedBy`,`s`.`year_lvl_id` AS `year_lvl_id` from `subjects` `s` ;

-- --------------------------------------------------------

--
-- Structure for view `subjectofferingdetails`
--
DROP TABLE IF EXISTS `subjectofferingdetails`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `subjectofferingdetails`  AS  select `so`.`so_id` AS `so_id`,`so`.`so_startTime` AS `so_startTime`,`so`.`so_endTime` AS `so_endTime`,`so`.`so_days` AS `so_days`,`so`.`so_creationDate` AS `so_creationDate`,(select `f`.`Name` from `facultysmalldetail` `f` where (`f`.`faculty_id` = `so`.`so_createdBy`)) AS `CreatedBy`,(select `s`.`subj_title` from `subjects` `s` where (`s`.`subj_id` = `so`.`subj_id`)) AS `SubjectTitle`,(select `s`.`subj_description` from `subjects` `s` where (`s`.`subj_id` = `so`.`subj_id`)) AS `SubjecDescription`,(select `f`.`Name` from `facultysmalldetail` `f` where (`f`.`faculty_id` = `so`.`faculty_id`)) AS `Teacher`,`so`.`faculty_id` AS `faculty_id`,`so`.`section_id` AS `section_id`,(select `c`.`section_name` from `classinformation` `c` where (`c`.`section_id` = `so`.`section_id`)) AS `Section`,(select `c`.`GradeLevel` from `classinformation` `c` where (`c`.`section_id` = `so`.`section_id`)) AS `Level`,(select `c`.`sy_id` from `classinformation` `c` where (`c`.`section_id` = `so`.`section_id`)) AS `schoolyear`,`so`.`subj_id` AS `subj_id` from `subjectoffering` `so` group by `so`.`so_id`,`so`.`subj_id`,`so`.`faculty_id`,`so`.`section_id` ;

-- --------------------------------------------------------

--
-- Structure for view `totaldailyattendance`
--
DROP TABLE IF EXISTS `totaldailyattendance`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `totaldailyattendance`  AS  select `attendancedetails`.`Gender` AS `Gender`,`attendancedetails`.`section_id` AS `section_id`,`attendancedetails`.`month_id` AS `month_id`,`attendancedetails`.`sy_id` AS `sy_id`,sum(`attendancedetails`.`attendance_presentDays`) AS `Total` from `attendancedetails` group by `attendancedetails`.`Gender`,`attendancedetails`.`section_id`,`attendancedetails`.`month_id`,`attendancedetails`.`sy_id` ;

-- --------------------------------------------------------

--
-- Structure for view `totaldailyattendancedetails`
--
DROP TABLE IF EXISTS `totaldailyattendancedetails`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `totaldailyattendancedetails`  AS  select `t`.`Gender` AS `Gender`,`t`.`section_id` AS `section_id`,`t`.`month_id` AS `month_id`,`t`.`sy_id` AS `sy_id`,`t`.`Total` AS `Total`,`c`.`GradeLevel` AS `GradeLevel`,`c`.`year_lvl_id` AS `year_lvl_id`,`c`.`sy_id` AS `sy_year` from (`totaldailyattendance` `t` join `classinformation` `c` on((`t`.`section_id` = `c`.`section_id`))) ;

-- --------------------------------------------------------

--
-- Structure for view `totallearners`
--
DROP TABLE IF EXISTS `totallearners`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `totallearners`  AS  select count(0) AS `Total`,`c`.`gradelevel` AS `gradelevel`,`c`.`sy_year` AS `sy_year`,(select count(0) from `controlinformation` where ((`controlinformation`.`Sex` = 'Male') and (`controlinformation`.`gradelevel` = `c`.`gradelevel`))) AS `TotalMale`,(select count(0) from `controlinformation` where ((`controlinformation`.`Sex` = 'Female') and (`controlinformation`.`gradelevel` = `c`.`gradelevel`))) AS `TotalFemale` from `controlinformation` `c` group by `c`.`gradelevel`,`c`.`sy_year` ;

-- --------------------------------------------------------

--
-- Structure for view `totalunregistered`
--
DROP TABLE IF EXISTS `totalunregistered`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `totalunregistered`  AS  select `unregistered`.`Gender` AS `Gender`,`unregistered`.`section_id` AS `section_id`,`unregistered`.`month_no` AS `MonthNumber`,`unregistered`.`month_desc` AS `Month`,count(0) AS `Total` from `unregistered` group by `unregistered`.`section_id`,`unregistered`.`Gender`,`unregistered`.`sy_id`,`unregistered`.`SchoolYear`,`Month`,`MonthNumber` ;

-- --------------------------------------------------------

--
-- Structure for view `transferredint`
--
DROP TABLE IF EXISTS `transferredint`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `transferredint`  AS  select `remarks_historydetails`.`Gender` AS `Gender`,`remarks_historydetails`.`section_id` AS `section_id`,`remarks_historydetails`.`sy_id` AS `sy_id`,`remarks_historydetails`.`month_desc` AS `Month`,count(0) AS `count(*)` from `remarks_historydetails` where (`remarks_historydetails`.`remarks` = 'T/I') group by `remarks_historydetails`.`Gender`,`remarks_historydetails`.`section_id`,`remarks_historydetails`.`sy_id`,`Month` ;

-- --------------------------------------------------------

--
-- Structure for view `transferredout`
--
DROP TABLE IF EXISTS `transferredout`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `transferredout`  AS  select `remarks_historydetails`.`Gender` AS `Gender`,`remarks_historydetails`.`section_id` AS `section_id`,`remarks_historydetails`.`sy_id` AS `sy_id`,`remarks_historydetails`.`month_desc` AS `Month`,count(0) AS `count(*)` from `remarks_historydetails` where (`remarks_historydetails`.`remarks` = 'T/O') group by `remarks_historydetails`.`Gender`,`remarks_historydetails`.`section_id`,`remarks_historydetails`.`sy_id`,`Month` ;

-- --------------------------------------------------------

--
-- Structure for view `unregistered`
--
DROP TABLE IF EXISTS `unregistered`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `unregistered`  AS  select `remarks_historydetails`.`control_id` AS `control_id`,`remarks_historydetails`.`student_id` AS `student_id`,`remarks_historydetails`.`Student` AS `Student`,`remarks_historydetails`.`Gender` AS `Gender`,`remarks_historydetails`.`LRN` AS `LRN`,`remarks_historydetails`.`section_id` AS `section_id`,`remarks_historydetails`.`SectionName` AS `SectionName`,`remarks_historydetails`.`SchoolYear` AS `SchoolYear`,`remarks_historydetails`.`sy_id` AS `sy_id`,`remarks_historydetails`.`month_no` AS `month_no`,`remarks_historydetails`.`remarks` AS `remarks`,`remarks_historydetails`.`reqInformation` AS `reqInformation`,`remarks_historydetails`.`dateUpdate` AS `dateUpdate`,`remarks_historydetails`.`month_desc` AS `month_desc` from `remarks_historydetails` where ((`remarks_historydetails`.`remarks` = 'DRP') or (`remarks_historydetails`.`remarks` = 'T/O')) ;

-- --------------------------------------------------------

--
-- Structure for view `usangasubjectoffering`
--
DROP TABLE IF EXISTS `usangasubjectoffering`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `usangasubjectoffering`  AS  select `subjectofferingdetails`.`subj_id` AS `subj_id`,`subjectofferingdetails`.`SubjectTitle` AS `SubjectTitle`,`subjectofferingdetails`.`SubjecDescription` AS `SubjecDescription`,`subjectofferingdetails`.`Teacher` AS `Teacher`,`subjectofferingdetails`.`faculty_id` AS `faculty_id`,`subjectofferingdetails`.`Level` AS `Level`,`subjectofferingdetails`.`schoolyear` AS `schoolyear`,`subjectofferingdetails`.`Section` AS `section` from `subjectofferingdetails` group by `subjectofferingdetails`.`Level`,`subjectofferingdetails`.`Teacher`,`subjectofferingdetails`.`subj_id`,`subjectofferingdetails`.`schoolyear`,`subjectofferingdetails`.`faculty_id`,`subjectofferingdetails`.`schoolyear`,`subjectofferingdetails`.`Section` ;

-- --------------------------------------------------------

--
-- Structure for view `yearleveldetails`
--
DROP TABLE IF EXISTS `yearleveldetails`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `yearleveldetails`  AS  select `y`.`year_lvl_id` AS `year_lvl_id`,`y`.`year_lvl_title` AS `year_lvl_title`,`y`.`year_lvl_dateCreated` AS `year_lvl_dateCreated`,(select `f`.`Name` from `facultysmalldetail` `f` where (`f`.`faculty_id` = `y`.`year_lvl_createdBy`)) AS `CreatedBy` from `yearlevel` `y` ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `address`
--
ALTER TABLE `address`
  ADD PRIMARY KEY (`student_id`,`sy_id`),
  ADD KEY `student_id` (`student_id`),
  ADD KEY `sy_id` (`sy_id`);

--
-- Indexes for table `adviser`
--
ALTER TABLE `adviser`
  ADD PRIMARY KEY (`faculty_id`,`section_id`,`adviser_id`),
  ADD KEY `faculty_id` (`faculty_id`),
  ADD KEY `section_id` (`section_id`);

--
-- Indexes for table `attendance`
--
ALTER TABLE `attendance`
  ADD PRIMARY KEY (`control_id`,`month_id`),
  ADD KEY `month_id` (`month_id`),
  ADD KEY `control_id` (`control_id`),
  ADD KEY `faculty_id` (`faculty_id`);

--
-- Indexes for table `book`
--
ALTER TABLE `book`
  ADD PRIMARY KEY (`book_id`),
  ADD KEY `subj_id` (`subj_id`);

--
-- Indexes for table `booksacquired`
--
ALTER TABLE `booksacquired`
  ADD PRIMARY KEY (`control_id`,`book_id`),
  ADD KEY `control_id` (`control_id`),
  ADD KEY `book_id` (`book_id`);

--
-- Indexes for table `control`
--
ALTER TABLE `control`
  ADD PRIMARY KEY (`control_id`),
  ADD KEY `student_id` (`student_id`),
  ADD KEY `section_id` (`section_id`),
  ADD KEY `sy_id` (`sy_id`);

--
-- Indexes for table `faculty_account`
--
ALTER TABLE `faculty_account`
  ADD PRIMARY KEY (`faculty_id`);

--
-- Indexes for table `grade`
--
ALTER TABLE `grade`
  ADD PRIMARY KEY (`quarters_id`,`faculty_id`,`control_id`,`so_id`),
  ADD KEY `so_id` (`so_id`),
  ADD KEY `quarters_id` (`quarters_id`),
  ADD KEY `control_id` (`control_id`),
  ADD KEY `faculty_id` (`faculty_id`);

--
-- Indexes for table `month`
--
ALTER TABLE `month`
  ADD PRIMARY KEY (`month_id`),
  ADD KEY `sy_id` (`sy_id`);

--
-- Indexes for table `parent`
--
ALTER TABLE `parent`
  ADD PRIMARY KEY (`student_id`,`sy_id`),
  ADD KEY `student_id` (`student_id`),
  ADD KEY `sy_id` (`sy_id`);

--
-- Indexes for table `quarters`
--
ALTER TABLE `quarters`
  ADD PRIMARY KEY (`quarters_id`);

--
-- Indexes for table `remarks_history`
--
ALTER TABLE `remarks_history`
  ADD PRIMARY KEY (`month_no`,`control_id`,`remarks`),
  ADD KEY `control_id` (`control_id`);

--
-- Indexes for table `schoolyear`
--
ALTER TABLE `schoolyear`
  ADD PRIMARY KEY (`sy_id`),
  ADD UNIQUE KEY `sy_year` (`sy_year`);

--
-- Indexes for table `section`
--
ALTER TABLE `section`
  ADD PRIMARY KEY (`section_id`),
  ADD KEY `year_lvl_id` (`year_lvl_id`);

--
-- Indexes for table `student_account`
--
ALTER TABLE `student_account`
  ADD PRIMARY KEY (`student_id`),
  ADD UNIQUE KEY `student_LRN_UNIQUE` (`student_LRN`);

--
-- Indexes for table `subjectoffering`
--
ALTER TABLE `subjectoffering`
  ADD PRIMARY KEY (`so_id`),
  ADD KEY `subj_id` (`subj_id`),
  ADD KEY `faculty_id` (`faculty_id`),
  ADD KEY `section_id` (`section_id`),
  ADD KEY `faculty_id_2` (`faculty_id`),
  ADD KEY `section_id_2` (`section_id`);

--
-- Indexes for table `subjects`
--
ALTER TABLE `subjects`
  ADD PRIMARY KEY (`subj_id`),
  ADD KEY `year_lvl_id` (`year_lvl_id`);

--
-- Indexes for table `yearlevel`
--
ALTER TABLE `yearlevel`
  ADD PRIMARY KEY (`year_lvl_id`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `address`
--
ALTER TABLE `address`
  ADD CONSTRAINT `FK_ADDRESS_STUDENT_ACCOUNT` FOREIGN KEY (`student_id`) REFERENCES `student_account` (`student_id`);

--
-- Constraints for table `adviser`
--
ALTER TABLE `adviser`
  ADD CONSTRAINT `FK_ADVISER_FACULTY_ACCOUNT` FOREIGN KEY (`faculty_id`) REFERENCES `faculty_account` (`faculty_id`),
  ADD CONSTRAINT `FK_ADVISER_SECTION` FOREIGN KEY (`section_id`) REFERENCES `section` (`section_id`);

--
-- Constraints for table `book`
--
ALTER TABLE `book`
  ADD CONSTRAINT `FK_BOOK_SUBJECTS` FOREIGN KEY (`subj_id`) REFERENCES `subjects` (`subj_id`);

--
-- Constraints for table `booksacquired`
--
ALTER TABLE `booksacquired`
  ADD CONSTRAINT `FK_BOOKSACQUIRED_BOOK` FOREIGN KEY (`book_id`) REFERENCES `book` (`book_id`),
  ADD CONSTRAINT `FK_BOOKSACQUIRED_CONTROL` FOREIGN KEY (`control_id`) REFERENCES `control` (`control_id`);

--
-- Constraints for table `control`
--
ALTER TABLE `control`
  ADD CONSTRAINT `FK_CONTROL_SCHOOLYEAR` FOREIGN KEY (`sy_id`) REFERENCES `schoolyear` (`sy_id`),
  ADD CONSTRAINT `FK_CONTROL_SECTION` FOREIGN KEY (`section_id`) REFERENCES `section` (`section_id`),
  ADD CONSTRAINT `FK_CONTROL_STUDENT_ACCOUNT` FOREIGN KEY (`student_id`) REFERENCES `student_account` (`student_id`);

--
-- Constraints for table `grade`
--
ALTER TABLE `grade`
  ADD CONSTRAINT `FK_GRADE_CONTROL` FOREIGN KEY (`control_id`) REFERENCES `control` (`control_id`),
  ADD CONSTRAINT `FK_GRADE_FACULTY_ACCOUNT` FOREIGN KEY (`faculty_id`) REFERENCES `faculty_account` (`faculty_id`),
  ADD CONSTRAINT `FK_GRADE_QUARTERS` FOREIGN KEY (`quarters_id`) REFERENCES `quarters` (`quarters_id`),
  ADD CONSTRAINT `FK_GRADE_SUBJECTOFFERING` FOREIGN KEY (`so_id`) REFERENCES `subjectoffering` (`so_id`);

--
-- Constraints for table `parent`
--
ALTER TABLE `parent`
  ADD CONSTRAINT `FK_PARENT_STUDENT_ACCOUNT` FOREIGN KEY (`student_id`) REFERENCES `student_account` (`student_id`);

--
-- Constraints for table `section`
--
ALTER TABLE `section`
  ADD CONSTRAINT `FK_SECTION_YEARLEVEL` FOREIGN KEY (`year_lvl_id`) REFERENCES `yearlevel` (`year_lvl_id`);

--
-- Constraints for table `subjectoffering`
--
ALTER TABLE `subjectoffering`
  ADD CONSTRAINT `FK_SUBJECTOFFERING_FACULTY_ACCOUNT` FOREIGN KEY (`faculty_id`) REFERENCES `faculty_account` (`faculty_id`),
  ADD CONSTRAINT `FK_SUBJECTOFFERING_SECTION` FOREIGN KEY (`section_id`) REFERENCES `section` (`section_id`),
  ADD CONSTRAINT `FK_SUBJECTOFFERING_SUBJECTS` FOREIGN KEY (`subj_id`) REFERENCES `subjects` (`subj_id`);

--
-- Constraints for table `subjects`
--
ALTER TABLE `subjects`
  ADD CONSTRAINT `FK_SUBJECTS_YEARLEVEL` FOREIGN KEY (`year_lvl_id`) REFERENCES `yearlevel` (`year_lvl_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
