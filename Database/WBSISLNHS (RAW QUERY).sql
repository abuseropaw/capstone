delete from control where control_id like '%0%';
delete from parent where parent_id like '%0%';
delete from address where address_id like '%0%';
delete from student_account where student_id like '%0%';



create database WBSISLNHS;
use WBSISLNHS;

create table student_account(
student_id varchar(15) primary key,
student_fname varchar(25) not null,
student_mname varchar(25),
student_lname varchar(25) not null,
student_sex varchar(25),
student_birthDate date not null,
student_age int(5) not null,
student_address varchar(100) not null,
student_motherTongue varchar(25) not null,
student_IP varchar(25),
student_religion varchar(25) not null,
student_picture varchar(100),
student_pass varchar(200));

create table address(
sn varchar(15) not null,
student_addressHSSP varchar(50),
student_addressBarangay varchar(50) not null,
student_addressMunicipality varchar(50) not null,
student_addressProvince varchar(50) not null,
sy_id varchar(15) not null,
INDEX(student_id),
constraint FK_ADDRESS_STUDENT_ACCOUNT foreign key(student_id) 
references STUDENT_ACCOUNT(student_id));



create table parent(
parent_id varchar(15) primary key,
student_id varchar(15) not null,
parent_Ffname varchar(25),
parent_Fmname varchar(25),
parent_Flname varchar(25),
parent_Mfname varchar(25),
parent_Mmname varchar(25),
parent_Mlname varchar(25),
parent_phone varchar(13),
INDEX(student_id),
constraint FK_PARENT_STUDENT_ACCOUNT foreign key(student_id)
references STUDENT_ACCOUNT(student_id));


create table guardian(
guardian_id varchar(15) primary key,
student_id varchar(15) not null,
guardian_name varchar(50) not null,
guardian_phone varchar(13),
guardian_relationship varchar(25) not null,
INDEX(student_id),
constraint FK_GUARDIAN_STUDENT_ACCOUNT foreign key(student_id)
references STUDENT_ACCOUNT(student_id));


create table credential(
cr_id varchar(15) primary key,
cr_title varchar(50) not null,
cr_description varchar(150) not null,
cr_creationDate datetime not null,
cr_createdBy varchar(50) not null
);




create table studentCredential(
student_id varchar(15) not null,
cr_id varchar(15) not null,
sc_remarks varchar(100) not null,
sc_creationDate datetime not null,
sc_createdBy varchar(15) not null,
INDEX(student_id),
INDEX(cr_id),
constraint PK_STUDENTCREDENTIAL primary key(student_id,cr_id),
constraint FK_STUDENTCREDENTIAL_STUDENT_ACCOUNT foreign key(student_id) references STUDENT_ACCOUNT(student_id),
constraint FK_STUDENTCREDENTIAL_CREDENTIAL foreign key(cr_id) references CREDENTIAL(cr_id)
);



create table faculty_account(
faculty_id varchar(15) primary key,
faculty_fname varchar(25) not null,
faculty_mname varchar(25),
faculty_lname varchar(25),
faculty_phone varchar(13),
faculty_address varchar(100) not null,
faculty_religion varchar(50),
faculty_ethnics varchar(50), 
faculty_major varchar(25) not null,
faculty_minor varchar(25) not null,
faculty_dob varchar(20) not null,
faculty_gender varchar(10) not null,
faculty_type varchar(20) not null,
faculty_picture varchar(200),
faculty_pass varchar(25) not null
);

create table yearLevel(
year_lvl_id varchar(15) primary key,
year_lvl_title varchar(15) not null);


create table section(
section_id varchar(15) primary key,
section_name varchar(20) not null,
section_capacity int(5) not null,
section_maxGrade decimal(5,2),
section_minGrade decimal(5,2),
section_createdBy varchar(50) not null,
section_dateCreated datetime not null,
year_lvl_id varchar(15) not null,
index(year_lvl_id),
constraint FK_SECTION_YEARLEVEL foreign key(year_lvl_id) references YEARLEVEL(year_lvl_id)
);


create table adviser(
faculty_id varchar(15) not null,
section_id varchar(15) not null,
INDEX(faculty_id),
INDEX(section_id),
constraint PK_ADVISER primary key(faculty_id,section_id),
constraint FK_ADVISER_FACULTY_ACCOUNT foreign key(faculty_id) references FACULTY_ACCOUNT(faculty_id),
constraint FK_ADVISER_SECTION foreign key(section_id) references SECTION(section_id)
);




create table subjects(
subj_id varchar(15) primary key,
subj_title varchar(25) not null,
subj_description varchar(100) not null,
subj_unit varchar(5) not null,
subj_hrs_per_week varchar(5) not null,
year_lvl_id varchar(15) not null,
INDEX(year_lvl_id),
constraint FK_SUBJECTS_YEARLEVEL foreign key(year_lvl_id) references YEARLEVEL(year_lvl_id)
);





create table schoolyear(
sy_id varchar(15) primary key,
sy_year date not null
);


///////////////////////WALA PANI
create table gradingcriteria(
gc_id int(15) primary key,
gc_title varchar(50) not null,
gc_percent numeric(25,2) DEFAULT 00.00 not null,
year_lvl_id int(15) not null,
faculty_id int(15) not null,
INDEX(year_lvl_id),
INDEX(faculty_id),
constraint FK_GRADINGCRITERIA_YEARLEVEL foreign key(year_lvl_id) references YEARLEVEL(year_lvl_id),
constraint FK_GRADINGCRITERIA_FACULTY_ACCOUNT foreign key(faculty_id) references FACULTY_ACCOUNT(faculty_id)
);




create table subjectOffering(
so_id varchar(15) primary key,
so_startTime datetime,
so_endTime datetime,
so_days int(3) not null,
so_creationDate datetime not null,
so_createdBy varchar(50) not null,
subj_id varchar(15) not null,
faculty_id varchar(15) not null,
section_id varchar(15) not null,
INDEX(subj_id),
INDEX(faculty_id),
INDEX(section_id),
constraint FK_SUBJECTOFFERING_SUBJECTS 
foreign key(subj_id) references SUBJECTS(subj_id),
INDEX(faculty_id),
constraint FK_SUBJECTOFFERING_FACULTY_ACCOUNT 
foreign key(faculty_id) references FACULTY_ACCOUNT(faculty_id),
INDEX(section_id),
constraint FK_SUBJECTOFFERING_SECTION 
foreign key(section_id) references SECTION(section_id)
);

create table quarters(
quarters_id varchar(15) primary key,
quarters_description varchar(25) not null,
quarters_remarks varchar(25) not null,
quarters_dateStarted datetime not null,
quarters_dateEnd  datetime,
quarters_createdBy varchar(15),
sy_id varchar(15)
);


create table control(
control_id varchar(15) primary key,
control_date datetime not null,
control_remarks varchar(25),
control_createdDate datetime not null,
control_createdBy varchar(50) not null,
student_id varchar(15) not null,
section_id varchar(15) not null,
sy_id varchar(15) not null,
INDEX(student_id),
INDEX(section_id),
INDEX(sy_id),
constraint FK_CONTROL_STUDENT_ACCOUNT foreign key(student_id) references STUDENT_ACCOUNT(student_id),
constraint FK_CONTROL_SECTION foreign key(section_id) references SECTION(section_id),
constraint FK_CONTROL_SCHOOLYEAR foreign key(sy_id) references SCHOOLYEAR(sy_id)
);

create table remarks_history(
	month_no varchar(15) not null,
	control_id varchar(15) not null,
	remarks varchar(50) not null,
	reqInformation varchar(5) null,
	dateUpdate date not null,
	month_desc varchar(50) not null,
	index(control_id),
	constraint PK_REMARKS_HISTORY primary key(month_no,control_id),
	constraint FK_REMARKS_HISTORY_CONTROL foreign key(control_id) references CONTROL(control_id)
);


create table grade(
grade_id varchar(15) primary key,
grade_value numeric(25,2) DEFAULT 00.00 not null,
grade_remarks varchar(25),
grade_dateSubmit date not null,
grade_dateUpdate date not null,
quarters_id varchar(15) not null,
faculty_id varchar(15) not null,
control_id varchar(15) not null,
so_id varchar(15) not null,
sy_id varchar(15) not null,
INDEX(so_id),
INDEX(quarters_id),
INDEX(control_id),
INDEX(faculty_id),
constraint FK_GRADE_SUBJECTOFFERING foreign key(so_id) references SUBJECTOFFERING(so_id),
constraint FK_GRADE_FACULTY_ACCOUNT foreign key(faculty_id) references FACULTY_ACCOUNT(faculty_id),
constraint FK_GRADE_QUARTERS foreign key(quarters_id) references QUARTERS(quarters_id),
constraint FK_GRADE_CONTROL foreign key(control_id) references CONTROL(control_id)
);

create table month(
	month_id varchar(15) primary key,
	month_description varchar(25) not null,
	month_totalDays int(5) not null,
	month_status varchar(15),
	sy_id varchar(15),
	INDEX(sy_id),
	constraint FK_MONTH_SCHOOLYEAR foreign key(sy_id) references SCHOOLYEAR(sy_id)
);

create table attendance(
attendance_presentDays int(5) not null,
attendance_absentDays int(5) null,
attendance_tardyDays int(5) null,
attendance_consecutiveAbsent varchar(25) not null,
attendance_dateCreated datetime not null,
month_id varchar(15) not null,
faculty_id varchar(15) not null,
control_id varchar(15) not null,
INDEX(month_id),
INDEX(control_id),
INDEX(faculty_id),
constraint PK_ATTENDANCE primary key(control_id,month_id),
constraint FK_ATTENDANCE_FACULTY_ACCOUNT foreign key(faculty_id) references FACULTY_ACCOUNT(faculty_id),
constraint FK_ATTENDANCE_MONTH foreign key(month_id) references MONTH(month_id),
constraint FK_ATTENDANCE_CONTROL foreign key(control_id) references CONTROL(control_id)
);

create table book(
book_id varchar(15) primary key,
book_title varchar(25) not null,
book_description varchar(50) not null,
book_dateCreated datetime not null,
book_createdBy varchar(15) not null,

subj_id varchar(15) not null,
index(subj_id),

constraint FK_BOOK_SUBJECTS foreign key(subj_id) references SUBJECTS(subj_id)
);


create table booksAcquired(
control_id varchar(15) not null,
book_id varchar(15) not null,
booksAcquired_remarks varchar(50),
date_acquired date not null,
date_received date,
INDEX(control_id),
INDEX(book_id),
constraint PK_BOOKSACQUIRED primary key(control_id,book_id),
constraint FK_BOOKSACQUIRED_CONTROL foreign key(control_id)
references CONTROL(control_id),
constraint FK_BOOKSACQUIRED_BOOK foreign key(book_id)
references BOOK(book_id));














//VIEWS;


create view facultySmallDetail as select faculty_id,concat(faculty_fname,' ',faculty_mname,' ', faculty_lname) as Name,
faculty_type,faculty_major,faculty_minor,faculty_status from faculty_account group by faculty_id;


create view facultySmallDetail1 as select 
faculty_id,concat(faculty_fname,' ', faculty_lname) as Name,
faculty_type,faculty_major,faculty_minor, faculty_pass, faculty_picture,faculty_status
from faculty_account group by faculty_id;




create view Advisory as select a.section_id,(select f.Name from facultysmalldetail f where f.faculty_id = 
a.faculty_id) as Adviser from adviser a;


create view ClassInformation as select s.section_id,s.section_name,s.section_capacity,s.section_maxGrade,
s.section_minGrade,(select f.Name from facultysmalldetail f where f.faculty_id = 
s.section_createdBy) as Cretedby, s.section_dateCreated, (select y.year_lvl_title from 
yearlevel y where y.year_lvl_id = s.year_lvl_id) as GradeLevel, (select a.Adviser from
Advisory a where a.section_id = s.section_id) as Adviser,s.sy_id, year_lvl_id,(select a.faculty_id from
Adviser a where a.section_id = s.section_id) as adviser_id from section s;



create view subjectDetails as select s.subj_id,s.subj_title,s.subj_description,s.subj_unit,s.subj_hrs_per_week,
(Select y.year_lvl_title from yearlevel y where y.year_lvl_id = s.year_lvl_id) as Level,s.subj_dateCreated,
(select f.Name from facultysmalldetail f where f.faculty_id = s.subj_createdBy) as CreatedBy,year_lvl_id from subjects s;


create view bookDetails as select b.book_id,b.book_title, b.book_description, b.book_dateCreated,
(select f.Name from facultysmalldetail
f where f.faculty_id = b.book_createdBy) as CreatedBy, (select s.subj_title
from subjects s where s.subj_id = b.subj_id) as Subject,(select sd.Level
from subjectdetails sd where sd.subj_id = b.subj_id) as Level
from book b;



create view yearlevelDetails as select y.year_lvl_id, y.year_lvl_title, y.year_lvl_dateCreated,
(select f.Name from facultysmalldetail
f where f.faculty_id = y.year_lvl_createdBy) as CreatedBy from yearlevel y;


create view credentialDetails as select c.cr_id, c.cr_title, c.cr_description, c.cr_creationDate, (select f.Name from facultysmalldetail f 
where f.faculty_id = c.cr_createdBy) as createdBy from credential c;

create view quarterDetails as select q.quarters_id, q.quarters_description, q.quarters_remarks,q.quarters_dateStarted,
q.quarters_dateEnd,(select f.Name from facultysmalldetail f where f.faculty_id= q.quarters_createdBy)
 as createdBy,sy_id from quarters q;



create view subjectofferingdetails as select so.so_id,so.so_startTime,so.so_endTime,so.so_days,so.so_creationDate,
(select f.Name from facultysmalldetail f where f.faculty_id= so.so_createdBy) as CreatedBy,
(select s.subj_title from subjects s where s.subj_id = so.subj_id) as SubjectTitle,
(select s.subj_description from subjects s where s.subj_id = so.subj_id) as SubjecDescription,
(select f.Name from facultysmalldetail f where f.faculty_id= so.faculty_id) as Teacher,
so.section_id,(select c.GradeLevel from classinformation c where c.section_id = so.section_id) as Level 
from subjectoffering so group by so.so_id,so.subj_id,so.faculty_id,so.section_id;

////////////////////////////////////////////////////////////////////////////////////
select student_account.student_id,address.address_id,parent.parent_id,guardian.guardian_id
,student_account.student_LRN,student_account.student_fname,student_account.student_mname,
student_account.student_lname,student_account.student_sex,
student_account.student_birthDate,student_account.student_age,student_account.student_motherTongue,
student_account.student_IP,student_account.student_religion,
address.student_addressHSSP,address.student_addressBarangay,address.student_addressMunicipality,
address.student_addressProvince,
(select concat(parent.parent_Ffname,'',parent.parent_Fmname,'',parent.parent_Flname)) as FathersName,
(select concat(parent.parent_Mfname,'',parent.parent_Mmname,'',parent.parent_Mlname)) as MothersName,
parent.parent_phone,
guardian.guardian_name,guardian.guardian_relationship,guardian.guardian_phone
from student_account,address,parent,guardian group by student_id;



create view studentsmalldetail as select student_id,student_LRN,(select concat(student_fname,' ',LEFT(student_mname,1),'. ',student_lname)) as
Name,student_sex,(DATE_FORMAT(student_birthDate, '%M %e, %Y')) as DOB,student_age,student_motherTongue,
student_IP,student_religion,student_picture,(Select Name from facultysmalldetail where faculty_id = 
student_createdBy) as CreatedBy, student_dateCreated from student_account;




create view controlDetails1 as select c.control_id,c.control_remarks,c.control_dateCreated,
(select f.Name from facultysmalldetail f where f.faculty_id = c.control_createdBy)
 as CreatedBy, c.student_id,(select s.Name from studentsmalldetail s where 
 s.student_id = c.student_id) as Student,(select s.student_sex from studentsmalldetail s 
 where s.student_id = c.student_id) as Gender,(select s.student_LRN from studentsmalldetail s 
 where s.student_id = c.student_id) as LRN,c.section_id, (select ci.section_name from
  classinformation ci where ci.section_id = c.section_id) as SectionName,
  (Select sy.sy_year from schoolyear sy where sy.sy_id = c.sy_id) as SchoolYear,MONTHNAME(c.control_dateUpdated) as Month
  ,c.sy_id from
  control c;


create table book(
book_id varchar(15) primary key,
book_title varchar(25) not null,
book_description varchar(50) not null,
book_dateCreated datetime not null,
book_createdBy varchar(15) not null,
year_lvl_id varchar(15) not null,
subj_id varchar(15) not null,
index(subj_id),
index(year_lvl_id),
constraint FK_BOOK_SUBJECT foreign key(subj_id) references SUBJECT(subj_id),
constraint FK_BOOK_YEARLEVEL foreign key(year_lvl_id) references YEARLEVEL(year_lvl_id)
);







create view gradeDetails as 
select g.grade_value,g.grade_remarks,g.grade_dateSubmit,g.grade_dateUpdate,
(select q.quarters_description from quarters q where q.quarters_id = g.quarters_id) as Period,
g.quarters_id,g.faculty_id,(select f.Name from facultysmalldetail f where f.faculty_id = g.faculty_id)
as SubmittedBy,g.control_id,g.sy_id,g.so_id from grade g;





//ChRISTINE QUERY

create view controlInformation as SELECT C.control_id, 
C.control_remarks, C.control_dateCreated, C.control_createdBy, SEC.section_id, SY.sy_id, SY.sy_year, 
SA.student_id, SA.student_LRN, SA.student_fname, SA.student_mname, SA.student_lname, 
(CASE WHEN SA.student_sex = "Male" THEN "M" WHEN SA.student_sex = "Female" THEN "F" END) as Sex, 
SA.student_birthDate as BirthDate, 
year(curdate()) - year(SA.student_birthDate) - (dayofyear(curdate()) < dayofyear(SA.student_birthDate)) as Age, 
SA.student_motherTongue as MotherTongue, 
SA.student_IP as IP, SA.student_religion as Religion, SA.student_age, SEC.gradelevel,c.control_dateUpdated
from(student_account SA inner join control C on SA.student_id=C.student_id) 
inner join classinformation SEC on (C.section_id=SEC.section_id) 
inner join schoolyear SY on (C.sy_id=SY.sy_id);




create view studentInfo as SELECT CI.student_id, CI.student_LRN, 
concat(CI.student_fname,' ',CI.student_mname,' ',CI.student_lname) as Name, CI.sex, CI.BirthDate, CI.Age, 
A.student_addressProvince as BirthPlace, CI.MotherTongue, CI.IP, CI.Religion, CI.sy_id, CI.sy_year, A.student_addressHSSP, 
A.student_addressBarangay, A.student_addressMunicipality, A.student_addressProvince,  
(CASE WHEN P.parent_Flname = CI.student_lname THEN P.parent_Flname WHEN P.parent_Flname != CI.student_lname 
THEN concat(P.parent_Ffname,' ',P.parent_Fmname,' ',P.parent_Flname) END) as FatherName, 
concat(P.parent_Mfname,' ',P.parent_Mmname,' ',P.parent_Mlname) as Mother, G.guardian_name as GuardianName, 
G.guardian_relationship as Relationship, G.guardian_phone as GContact, P.parent_phone as PContact 
from(controlInformation CI inner join address A on CI.student_id = A.student_id) 
inner join parent P on (CI.student_id = P.student_id) 
inner join guardian G on (CI.student_id = G.student_id);


create view studentInfo1 as SELECT CI.control_id,CI.student_id, CI.student_LRN, 
concat(CI.student_lname,',',CI.student_fname,',',CI.student_mname) as Name, CI.sex,
DATE_FORMAT(CI.BirthDate, '%m/%d/%Y') as BirthDate, CI.Age, 
A.student_addressProvince as BirthPlace, CI.MotherTongue, CI.IP, CI.Religion, CI.sy_id, CI.sy_year,
CI.control_remarks,CI.section_id, A.student_addressHSSP, 
A.student_addressBarangay, A.student_addressMunicipality, A.student_addressProvince, 
concat(P.parent_Flname,',',P.parent_Ffname,',',P.parent_Fmname) as Father, 
concat(P.parent_Mlname,',',P.parent_Mfname,',',P.parent_Mmname) as Mother, P.guardian_name as GuardianName, 
P.guardian_relationship as Relationship, P.guardian_phone as GContact, P.parent_phone as PContact,
CI.control_dateCreated, CI.control_dateUpdated
from(controlInformation CI inner join address A on CI.student_id = A.student_id) 
inner join parent P on (CI.student_id = P.student_id) group by CI.sy_id;

create view averageDetails as select c.LRN, c.Student, round(a.average,3) 
as average,a.control_id,a.sy_id,c.section_id,c.Gender,ca.GradeLevel from controlDetails c natural join average a
 natural join classinformation ca;

create view booksAcquiredDetails as SELECT C.control_id, C.Student, C.sectionName, C.SchoolYear, BA.date_acquired, 
BA.date_returned, BD.book_id, BD.Subject, BD.book_description, BA.booksAcquired_remarks, BD.Level, C.Gender,c.section_id
from(controlDetails C inner join booksAcquired BA on C.control_id = BA.control_id) 
inner join bookdetails BD on (BD.book_id = BA.book_id);

create view performance as SELECT C.control_id, C.sectionName, C.Level, SO.so_id, S.subj_id, S.subj_title, 
Q.quarters_id, Q.quarters_description, G.grade_id, G.grade_value, G.grade_remarks,
from(grade G inner join quarters Q on G.quarters_id = Q.quarters_id)
inner join booksacquireddetails C on (C.control_id = G.control_id)
inner join subjectoffering SO on (SO.so_id = Q.so_id) 
inner join subjects S on (S.subj_id = SO.subj_id);


create view SF5Remarks as select aa.sy_id,aa.section_id,aa.gradelevel,
(select count(*) from averageDetails a where a.average <= 74 and a.Gender='Male' and a.section_id=aa.section_id) as MBEGINNING,
(select count(*) from averageDetails a where a.average <= 74 and a.Gender='Female' and a.section_id=aa.section_id) as FBEGINNING,

(select count(*) from averageDetails a where a.average between 75 and 79 and a.Gender='Male' and a.section_id=aa.section_id) as MDEVELOPING,
(select count(*) from averageDetails a where a.average between 75 and 79 and a.Gender='Female' and a.section_id=aa.section_id) as FDEVELOPING,

(select count(*) from averageDetails a where a.average between 80 and 84 and a.Gender='Male' and a.section_id=aa.section_id) as MAPPROACHING,
(select count(*) from averageDetails a where a.average between 80 and 84 and a.Gender='Female' and a.section_id=aa.section_id) as FAPPROACHING,

(select count(*) from averageDetails a where a.average between 85 and 89 and a.Gender='Male' and a.section_id=aa.section_id) as MPROFICIENT,
(select count(*) from averageDetails a where a.average between 85 and 89 and a.Gender='Female' and a.section_id=aa.section_id) as FPROFICIENT,

(select count(*) from averageDetails a where a.average >= 90 and a.Gender='Male' and a.section_id=aa.section_id) as MADVANCED,
(select count(*) from averageDetails a where a.average >= 90 and a.Gender='Female' and a.section_id=aa.section_id) as FADVANCED
from averagedetails aa group by section_id,sy_id,gradelevel;



create view usangaSubjectOffering as select subj_id,subjectTitle,
SubjecDescription,Teacher,faculty_id,level,schoolyear 
from subjectofferingdetails group by level,teacher,subj_id,schoolyear;

create view averageperSubjectOffering as 
select avg(grade_value) as average,control_id,so_id,sy_id from grade group by control_id,so_id,sy_id;

create view TotalLearners as select count(*)as Total,c.gradelevel,c.sy_year,
(select count(*) from controlinformation where sex='Male' and 
gradelevel=c.gradelevel)as
TotalMale,(select count(*) from controlinformation where Sex='Female' and 
gradelevel=c.gradelevel)as TotalFemale from controlinformation c group by gradelevel,sy_year;

create view studentsmalldetail1 as SELECT student_id, student_LRN, 
concat(student_fname,' ',student_mname,' ',student_lname) as Name, student_sex, student_birthDate as DOB, 
student_age, student_pass from student_account;

    
create view average as select avg(average) as average,
control_id,
sy_id
from averageperquarter
group by control_id, sy_id;

	create view usangasubjectoffering as 
	select subj_id,
	SubjectTitle,
	SubjecDescription,
	Teacher,
	faculty_id,
	Level,
	schoolyear,section
	from subjectofferingdetails
	group by Level,Teacher,subj_id,schoolyear,faculty_id,schoolyear,section;





update Password(id varchar(15),pass varchar(200))
update student_account set student_pass=pass where student_id=id;student_accountparentparentparent




create view sectionoverall as 
select s.section_id,(select count(*) from controldetails c where c.control_remarks='T/I' and c.Gender='Male' and c.section_id=s.section_id) as TIM,
				  (select count(*) from controldetails c where c.control_remarks='T/I' and c.Gender='Female' and c.section_id=s.section_id) as TIF,
				  (select count(*) from controldetails c where c.control_remarks='LE' and c.Gender='Male' and c.section_id=s.section_id) as LEM,
				  (select count(*) from controldetails c where c.control_remarks='LE' and c.Gender='Female' and c.section_id=s.section_id) as LEF,
				  (select count(*) from controldetails c where c.control_remarks='DRP' and c.Gender='Male' and c.section_id=s.section_id) as DOM,
				  (select count(*) from controldetails c where c.control_remarks='DRP' and c.Gender='Female' and c.section_id=s.section_id) as DOF,
				  (select count(*) from controldetails c where c.control_remarks='T/O' and c.Gender='Male' and c.section_id=s.section_id) as TOM,
				  (select count(*) from controldetails c where c.control_remarks='T/O' and c.Gender='Female' and c.section_id=s.section_id) as TOF,
				  (select count(*) from controldetails c where NOT c.control_remarks='LE' and NOT c.control_remarks='DRP' and NOT c.control_remarks='T/I' 
				  	and NOT c.control_remarks='T.O' and NOT c.control_remarks='' and c.Gender='Male' and c.section_id=s.section_id) as PTIM,
				  (select count(*) from controldetails c where NOT c.control_remarks='LE' and NOT c.control_remarks='DRP' and NOT c.control_remarks='T/I' 
				  	and NOT c.control_remarks='T.O' and NOT c.control_remarks='' and c.Gender='Female' and c.section_id=s.section_id) as PTIF,
				  (select count(*) from controldetails c where c.Gender='Male' and c.section_id=s.section_id) as M,
				  (select count(*) from controldetails c where c.Gender='Female' and c.section_id=s.section_id) as F
from controldetails s group by s.section_id;



