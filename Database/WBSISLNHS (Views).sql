
-- STUDENTSMALLDETAIL
create view studentsmalldetail 
as select student_id,
(select concat(student_fname,' ',LEFT(student_mname,1),'. ',student_lname)) as Name,
student_sex,(DATE_FORMAT(student_birthDate, '%M %e, %Y')) as DOB,
student_age,student_motherTongue,student_IP,student_religion,student_picture,
(Select Name from facultysmalldetail where faculty_id = 
student_createdBy) as CreatedBy, student_dateCreated from student_account;





select c.control_id, c.control_remarks, c.control_dateCreated,
(select f.Name from facultysmalldetail f where f.faculty_id = c.control_createdBy) as CreatedBy,
c.student_id, s.Name as Student, s.student_sex as Gender, c.section_id,
(select ci.section_name from classinformation ci where ci.section_id = c.section_id) as SectionName,
(Select sy.sy_year from schoolyear sy where sy.sy_id = c.sy_id) as SchoolYear, c.control_dateUpdated
from control c inner join studentsmalldetail s on c.student_id = s.student_id; 


create view averageperquarter as 
select avg(grade_value) as average,quarters_id,control_id,sy_id from grade group by quarters_id,control_id,sy_id;

-- create view averageperquarterdetails as 
-- select a.average,a.quarters_id,a.control_id,a.sy_id,c.section_id,c.Name from averageperquarter a inner join studentinfo1 c
-- on a.control_id=c.control_id;
create view averageperquarterdetails as 
select a.average,a.quarters_id,a.control_id,a.sy_id,c.section_id,concat(c.student_lname,',',
student_fname,',',student_mname) as Name,gradelevel from averageperquarter a inner join controlinformation c
on a.control_id=c.control_id;


create view averageperquarter as 
select avg(grade_value) as average,quarters_id,control_id,so_id,sy_id from grade group by quarters_id,control_id,so_id,sy_id;

create view average as
select avg(average) as average,control_id,sy_id from averageperquarter group by control_id,sy_id;

-- DAAN NGA CONTROLDETAILS
create view controlDetails as select c.control_id,c.control_remarks,c.control_dateCreated,
(select f.Name from facultysmalldetail f where f.faculty_id = c.control_createdBy)
 as CreatedBy, c.student_id,(select s.Name from studentinfo1 s where 
 s.student_id = c.student_id) as Student,(select s.student_sex from studentsmalldetail s 
 where s.student_id = c.student_id) as Gender,(select s.student_LRN from studentsmalldetail s 
 where s.student_id = c.student_id) as LRN,c.section_id, (select ci.section_name from
  classinformation ci where ci.section_id = c.section_id) as SectionName,
  (Select sy.sy_year from schoolyear sy where sy.sy_id = c.sy_id) as SchoolYear,c.control_dateUpdated,
  (select ci.GradeLevel from classinformation ci where ci.section_id = c.section_id) as gradelevel,c.control_reqInformation from
  control c;



-- BAG O NGA CONTROLDETAILS
create view controlDetails as select c.control_id,c.control_remarks,c.control_dateCreated,
(select f.Name from facultysmalldetail f where f.faculty_id = c.control_createdBy)
 as CreatedBy, c.student_id,(select distinct(s.Name) from studentinfo1 s where 
 s.student_id = c.student_id) as Student,(select s.student_sex from studentsmalldetail s 
 where s.student_id = c.student_id) as Gender,(select s.student_LRN from studentsmalldetail s 
 where s.student_id = c.student_id) as LRN,c.section_id, (select ci.section_name from
  classinformation ci where ci.section_id = c.section_id) as SectionName,
  (Select sy.sy_year from schoolyear sy where sy.sy_id = c.sy_id) as SchoolYear,c.control_dateUpdated,
  (select ci.GradeLevel from classinformation ci where ci.section_id = c.section_id) as gradelevel,c.control_reqInformation from
  control c;




create view controlDetails1 as select c.control_id,c.control_remarks,c.control_dateCreated,
(select f.Name from facultysmalldetail f where f.faculty_id = c.control_createdBy)
 as CreatedBy, c.student_id,(select s.Name from studentsmalldetail s where 
 s.student_id = c.student_id) as Student,(select s.student_sex from studentsmalldetail s 
 where s.student_id = c.student_id) as Gender,(select s.student_LRN from studentsmalldetail s 
 where s.student_id = c.student_id) as LRN,c.section_id, (select ci.section_name from
  classinformation ci where ci.section_id = c.section_id) as SectionName,
  (Select sy.sy_year from schoolyear sy where sy.sy_id = c.sy_id) as SchoolYear,MONTHNAME(c.control_dateUpdated) as Month
  ,DAYOFMONTH(c.control_dateUpdated) as Day,MONTH(c.control_dateUpdated) as MonthNumber,c.sy_id from
  control c;	






create view remarks_historyDetails as
  select c.control_id,c.student_id,c.Student,c.Gender,c.LRN,c.section_id,c.SectionName,c.SchoolYear,c.sy_id,
  r.month_no,r.remarks,r.reqInformation,r.dateUpdate,r.month_desc from controldetails1 c inner join remarks_history r on 
  c.control_id = r.control_id;




-- unregistered student
-- create view unregistered as 
-- select * from controldetails1 where control_remarks = 'DRP' OR control_remarks='T/O'; 

create view unregistered as 
select * from remarks_historyDetails where remarks = 'DRP' OR remarks='T/O'; 



-- create view totalUnregistered as 
-- select Gender,section_id,MonthNumber,Month,count(*) as Total from unregistered group by section_id,Gender,sy_id,schoolyear
-- ,Month,MonthNumber;

create view totalUnregistered as 
select Gender,section_id,month_no as MonthNumber,month_desc as Month,count(*) as 
Total from unregistered group by section_id,Gender,sy_id,schoolyear
,Month,MonthNumber;


---------------------------------------------------------------------------------------------------------------------------------------------------
 -- Enrolment as of (1st Friday of June)
 -- create view Enrolmentasof1stFridayofJune as
 -- 	select Gender,section_id,sy_id,count(*) as Total from controldetails1 
 -- 	where NOT control_remarks = 'LE' group by sy_id,section_id,Gender;

  create view Enrolmentasof1stFridayofJune as
  select Gender,section_id,sy_id,count(*) as Total,month_no,month_desc from remarks_historyDetails 
  where NOT remarks = 'LE' group by sy_id,section_id,Gender,month_no,month_desc;



 -- -- Late Enrollment during the month (beyond cut-off)
 -- create view LateEnrollmentduringthemonth as
 -- 	select Gender,section_id,sy_id,Month,count(*) as Total from controlDetails1 where
 --   control_remarks='LE'
 -- 	group by sy_id,section_id,Gender,Month;
   create view LateEnrollmentduringthemonth as
  select Gender,section_id,sy_id,month_desc as Month,count(*) as Total,month_no from remarks_historyDetails where
   remarks='LE' group by sy_id,section_id,Gender,Month,month_no;

 -- Drop out
 create view DropOut as
 select Gender,section_id,sy_id,month_desc as Month,count(*) from remarks_historyDetails
 where remarks='DRP' group by Gender,section_id,sy_id,Month;

 -- Transferred out
 create view TransferredOut as
 select Gender,section_id,sy_id,month_desc as Month,count(*) from remarks_historyDetails
 where remarks='T/O' group by Gender,section_id,sy_id,Month;

 -- Transferred In
 create view TransferredInt as
 select Gender,section_id,sy_id,month_desc as Month,count(*) from remarks_historyDetails
 where remarks='T/I' group by Gender,section_id,sy_id,Month;
---------------------------------------------------------------------------------------------------------------------------------------------------



-- Number of students absent for 5 consecutive days:
 create view NumberofStudentsAbsentfor5ConsecutiveDays as
  select Gender,section_id,sy_id,month_id,count(*) from attendancedetails where 
  attendance_consecutiveAbsent ='Yes' group by Gender,section_id,sy_id,month_id;



create view attendancedetails as 
 select c.control_id,c.Gender,c.section_id,c.control_remarks,c.sy_id,a.attendance_presentDays,
 a.attendance_absentDays,a.attendance_tardyDays,a.attendance_consecutiveAbsent,a.month_id, c.Student
 from controldetails1 c inner join attendance a on c.control_id = a.control_id; 

create view attendancedetails as 
   select c.control_id,c.Gender,c.section_id,c.remarks,c.sy_id,a.attendance_presentDays,
   a.attendance_absentDays,a.attendance_tardyDays,a.attendance_consecutiveAbsent,a.month_id, c.Student
   from remarks_historyDetails c inner join attendance a on c.control_id = a.control_id; 


 -- Average Daily attendance
 create view TotalDailyAttendance as
 select Gender,section_id,month_id,sy_id,sum(attendance_presentDays) as Total 
 from attendancedetails group by Gender,section_id,month_id,sy_id;

create view TotalDailyAttendanceDetails as
select t.Gender,t.section_id,t.month_id,t.sy_id,t.Total,c.GradeLevel,c.year_lvl_id,c.sy_id as sy_year
from TotalDailyAttendance t inner join classinformation c on t.section_id=c.section_id;



 -- Registered Learners as of end of the month
 		-- Get unregistered learner as of end of the month
 			*** LATE ENROLLMENT DURING THE MONTH
 	select cd.Gender,cd.section_id,cd.sy_id,cd.Month,count(*) from controldetails1 cd 
 	where NOT control_remarks='DRP' AND NOT control_remarks='T/O' group by cd.Gender,cd.section_id,cd.sy_id,cd.Month;




 -- Percentage of Enrolment as of end of the month









-- AllLearner per Level
create view alllearnerperlevel as 
select sy_id,sy_year,gradelevel,sex,count(*) as Total from controlinformation
group by sy_id,sy_year,gradelevel,sex;



create view averagepersubjectofferingdetails as 
select a.average,a.control_id,a.so_id,a.sy_id,c.Sex,c.gradelevel,c.section_id from averagepersubjectoffering a inner join 
controlinformation c on a.control_id = c.control_id; 


create view sf5remarksperLevel as 
SELECT sy_id,gradelevel,sum(MBEGINNING) as MBEGINNING, sum(FBEGINNING) as FBEGINNING,
sum(MDEVELOPING) as MDEVELOPING, sum(FDEVELOPING) as FDEVELOPING,
sum(MAPPROACHING) as MAPPROACHING, sum(FAPPROACHING) as FAPPROACHING,
sum(MPROFICIENT) as MPROFICIENT, sum(FPROFICIENT) as FPROFICIENT,
sum(MADVANCED) as MADVANCED, sum(FADVANCED) as FADVANCED from sf5remarks group by sy_id,gradelevel;






-- REPORT

 create view report_TOTALLEARNER as select count(*) as Total, schoolyear from controldetails group by schoolyear order by schoolyear asc;

 create view report_TOTALGENDER as 
 select c.schoolyear,(Select count(*) from controldetails where Gender='Male' AND schoolyear=c.schoolyear) as TotalMale,
 (Select count(*) from controldetails where Gender='Female' AND schoolyear=c.schoolyear) as TotalFemale from controldetails c group by c.schoolyear order by c.schoolyear asc;




create view report_TOTALSTATUS as 
select s.SchoolYear,(select count(*) from controldetails c where c.control_remarks='T/I' and c.SchoolYear=s.SchoolYear) as TI,
				  (select count(*) from controldetails c where c.control_remarks='LE' and c.SchoolYear=s.SchoolYear) as LE,
				  (select count(*) from controldetails c where c.control_remarks='DRP' and c.SchoolYear=s.SchoolYear) as DO,
				  (select count(*) from controldetails c where c.control_remarks='T/O' and c.SchoolYear=s.SchoolYear) as 'TO',
				  (select count(*) from controldetails c where NOT c.control_remarks='LE' and NOT c.control_remarks='DRP' and NOT c.control_remarks='T/I' 
				  	and NOT c.control_remarks='T/O' and NOT c.control_remarks='' and c.SchoolYear=s.SchoolYear) as PTI,
				  (select count(*) from controldetails c where NOT c.control_remarks='LE' and NOT c.control_remarks='DRP' and NOT c.control_remarks='T/I' 
				  	and NOT c.control_remarks='T/O' and c.control_remarks='' and c.SchoolYear=s.SchoolYear) as OTHERS
from controldetails s group by s.SchoolYear order by s.SchoolYear asc;



create view quarterlygradepersubject as select sag.average,sag.control_id,sag.sy_id,
sod.subjectTitle,sod.subjecDescription,sod.Teacher,sod.faculty_id,sod.section_id,sod.section,sod.level,sod.schoolyear,sod.subj_id,sod.so_id, 
cf.student_id 
from ((soaveragegrade sag inner join subjectofferingdetails sod on sag.so_id=sod.so_id) 
inner join controlinformation cf on sag.control_id=cf.control_id);