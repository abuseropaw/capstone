<?php
    include_once 'sessionAdmin.php';
    include_once 'dbconnect.php';
    include 'ajaxJQuery/globalFunction.php';
    function fillSchoolYear($con){
        $output ='';
        $query = mysqli_query($con, "SELECT * from schoolyear");
        while($row = mysqli_fetch_array($query)){
            $output .='<option value="'.$row[1].'">'.$row[1].'</option>';
        }
        return $output;
    }


?>
<!DOCTYPE html>
    <!-- HEAD -->
    <?php include_once 'head.php'; ?> 
    <!-- HEAD   -->
    <body>
        <!-- HEADER -->
        <?php include_once 'header.php'; ?>
        <!-- <style>
        

        /* Link the series colors to axis colors */
        .highcharts-color-0 {
            fill: #00BCD4;
            stroke: #90ed7d;
        }
        .highcharts-axis.highcharts-color-0 .highcharts-axis-line {
            stroke: #90ed7d;
        }
        .highcharts-axis.highcharts-color-0 text {
            fill: #90ed7d;
        }

        .highcharts-color-1 {
            fill: #F44336;
            stroke: #FFEB38;
        }
        .highcharts-axis.highcharts-color-1 .highcharts-axis-line {
            stroke: #FFEB38;
        }
        .highcharts-axis.highcharts-color-1 text {
            fill: #F44336;
        }

        .highcharts-color-2 {
            fill: #FFEB38;
            stroke: #F44336;
        }
        .highcharts-axis.highcharts-color-2 .highcharts-axis-line {
            stroke: #F44336;
        }
        .highcharts-axis.highcharts-color-2 text {
            fill: #FFEB38;
        }

        .highcharts-color-3 {
            fill: #7cb5ec;
            stroke: #7cb5ec;
        }
        .highcharts-axis.highcharts-color-3 .highcharts-axis-line {
            stroke: #7cb5ec;
        }
        .highcharts-axis.highcharts-color-3 text {
            fill: #7cb5ec;
        }


        .highcharts-yaxis .highcharts-axis-line {
            stroke-width: 2px;
        }
        </style> -->
        <!-- HEADER -->

        <section id="main">
            
            <?php 
                $toggle = 'adminHome';
                include_once 'sidebar.php'; 
            ?>
            <section id="content">
                
               
                <div class="p-10 p-b-0">

                    <div class="text-center p-t-25">


                        <h1>STUDENT INFORMATION SYSTEM</h1>
                        <br />
                    </div>

                   <div class="row">
                        <div class="col-sm-5">
                            <div class='card'>                                  
                                <div class="card-body lcb-form">
                                    <div id="chartcontainer"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-7">
                            <div class='card'>                                  
                                <div class="card-body lcb-form">
                                    <div id="chartcontainer1"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class='card'>                                  
                                <div class="card-body lcb-form">
                                    <div id="chartcontainer2"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                  <div class="lockscreen-footer text-center">
                    Copyright &copy; 2018 <b>Mindanao State University Naawan</b><br>
                    IT Services | A.R-C.G-M.Y.
                                
                  </div>
            </div>
                    
                
            </section>
        </section>


        <!-- Javascript Libraries -->
        <?php include_once 'scripts.php'; ?>
        <script type="text/javascript">
            $(document).ready(function(){
            var globalTotal = 0;
               
                $.ajax({
                    url:"ajaxJQuery/registrarAnalytics.php?report=enrolly",
                    method:"get",
                    dataType:"json",
                    success:function(data){
                        var res=[];
                        var na =[];
                        $.each(data, function(index, element) {
                            na.push(element.schoolyear);
                            res.push(parseFloat(element.Total)
                            );
                        });
                        globalTotal = res;

                        $('#chartcontainer').highcharts({
                            chart: {
                                type: 'line'
                            },
                            title: {
                                text: 'School Enrollment'
                            },
                            subtitle: {
                                text: 'Report on School Enrollment'
                            },
                            xAxis: {
                              categories: na,
                              crosshair: false
                          },
                            yAxis: [{
                                className: 'highcharts-color-1',
                                title: {
                                        text: 'Number of registered learner'
                                }
                            }],
                            plotOptions: {
                                column: {
                                    borderRadius: 5
                                },
                                line: {
                                    dataLabels: {
                                        enabled: true
                                    }
                                }
                            },
                            series: [{
                                name:'Learner',
                                data:res,
                              }]
                            

                        });
                    }
                });
               
           
                $.ajax({
                        url:"ajaxJQuery/registrarAnalytics.php?report=gender",
                        method:"get",
                        dataType:"json",
                        success:function(data){
                            var male=[];
                            var female=[];
                            var categ=[];
                            $.each(data, function(index, element) {
                                categ.push(element.schoolyear);
                                male.push(parseFloat(element.male));
                                female.push(parseFloat(element.female));
                            });

                            
                            $('#chartcontainer1').highcharts({
                              chart: {
                                  type: 'column'
                              },
                              title: {
                                  text: 'Learner by Gender'
                              },
                              
                              xAxis: {
                                  categories: categ,
                                  crosshair: true
                              },
                              yAxis: {
                                  min: 0,
                                  title: {
                                      text: 'Total number of learner'
                                  }
                              },
                              tooltip: {
                                  headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                                  pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                      '<td style="padding:0"><b>{point.y:.1f} Learner</b></td></tr>',
                                  footerFormat: '</table>',
                                  shared: true,
                                  useHTML: true
                              },
                              plotOptions: {
                                  column: {
                                      pointPadding: 0.2,
                                      borderWidth: 0,
                                      dataLabels: {
                                        enabled: true
                                    }
                                  }
                              },
                              series: [{
                                  name: 'Male',
                                  data: male

                              }, {
                                  name: 'Female',
                                  data: female
                              }]
                            });
                            
                        }
                    });
               
                
                $.ajax({
                        url:"ajaxJQuery/registrarAnalytics.php?report=status",
                        method:"get",
                        dataType:"json",
                        success:function(data){
                            var categ=[];
                            var ti=[];
                            var le=[];
                            var dod=[];
                            var to=[];
                            var pti=[];
                            var others=[];
                           
                            $.each(data, function(index, element) {
                                categ.push(element.schoolyear);
                                ti.push(parseFloat(element.ti));
                                le.push(parseFloat(element.le));
                                dod.push(parseFloat(element.do));
                                to.push(parseFloat(element.to));
                                pti.push(parseFloat(element.pti));
                                others.push(parseFloat(element.others));
                                

                            });
                            
                            $('#chartcontainer2').highcharts({
                              chart: {
                                  type: 'column'
                              },
                              title: {
                                  text: 'Report on Learners Status'
                              },
                              
                              xAxis: {
                                  categories: categ,
                                  crosshair: true
                              },
                              yAxis: {
                                  min: 0,
                                  title: {
                                      text: 'Total registered learner'
                                  }
                              },
                              tooltip: {
                                  headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                                  pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                      '<td style="padding:0"><b>{point.y:.1f} Learner</b></td></tr>',
                                  footerFormat: '</table>',
                                  shared: true,
                                  useHTML: true
                              },
                              plotOptions: {
                                  column: {
                                      pointPadding: 0.2,
                                      borderWidth: 0,
                                      dataLabels: {
                                        enabled: true
                                    }
                                  }
                              },
                              series: [
                                  {
                                      name: 'Transferred In',
                                      data: ti,

                                  }, {
                                      name: 'Transferred Out',
                                      data: to,
                                  }, {
                                      name: 'Late Enrollment',
                                      data: le,
                                  }, {
                                      name: 'DropOut',
                                      data: dod,
                                  }, {
                                      name: 'Pending T/I',
                                      data: pti,
                                  }, {
                                      name: 'Others',
                                      data: others,
                                  },{
                                        type: 'spline',
                                        name: 'Total',
                                        data: globalTotal,
                                        marker: {
                                            lineWidth: 3,
                                            
                                            fillColor: 'white'
                                        }
                                  }
                              ]
                            });
                            
                        }
                    });
            

                $('#sy').change(function(){
                    var sys = $(this).val();
                    $.ajax({
                        url:"ajaxJQuery/loadDashboard.php",
                        method:"POST",
                        data:{sys:sys},
                        success:function(data){
                            $('#dashboard').html(data);
                        }
                    });
                });        
            });
        </script>
        <!-- Javascript Libraries -->
        <script src="vendors/bower_components/flot/jquery.flot.js"></script>
        <script src="vendors/bower_components/flot/jquery.flot.resize.js"></script>
        <script src="vendors/bower_components/flot.curvedlines/curvedLines.js"></script>
        <script src="vendors/sparklines/jquery.sparkline.min.js"></script>
        <script src="vendors/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js"></script>
        
    </body>

</html>
