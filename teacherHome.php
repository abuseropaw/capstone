<?php
    include_once 'sessionTeacher.php';
    include_once 'dbconnect.php';
?>
<!DOCTYPE html>
    <!-- HEAD -->
    <?php include_once 'head.php'; ?> 
    <!-- HEAD   -->
    <body>
        <!-- HEADER -->
        <?php include_once 'header.php'; ?>
        <!-- HEADER -->

        <section id="main">
            <?php 
                $toggle = 'teacherHome';
                include_once 'sidebarTeacher.php'; 
            ?>
            <section id="content">
                
               
                <div class="p-10 p-b-0">

                    <div class="text-center p-t-25">

                                <!-- <img src="img/deped.png" class="pull-left"> -->
                        <h1>
                            <b>Student Information System</b>
                            <br />
                            <h3>Lugait National High School</h3>

                        </h1>
                        <br />
                    </div>

    <!-- lockscreen credentials (contains the form) -->
                    
    <!-- /.lockscreen credentials -->


  <!-- /.lockscreen-item -->
                  

                  <div class="lockscreen-footer text-center">
                    Copyright &copy; 2018 <b>Mindanao State University Naawan</b><br>
                    IT Services | A.R-C.G-M.Y.
                                
                  </div>
            </div>
                    
                
            </section>
        </section>

        <!-- FOOTER -->
        
        <!-- Javascript Libraries -->
        <?php include_once 'scripts.php'; ?>
        <!-- Javascript Libraries -->


    </body>
  
<!-- Mirrored from byrushan.com/projects/ma/1-7-1/jquery/light/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 01 May 2017 06:33:25 GMT -->
</html>
