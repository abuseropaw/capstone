<?php
    include_once 'sessionAdmin.php';
    include_once 'dbconnect.php';

    $teacherID =mysqli_real_escape_string($con, $_GET['tID']);


    $query = mysqli_query($con, "SELECT * from faculty_account where faculty_id='".$teacherID."'");
    if($row = mysqli_fetch_array($query)){
        $id = $row[0];
        $fname=$row[1];
        $mname=$row[2];
        $lname=$row[3];
        $phone=$row[4];
        $address=$row[5];
        $religion=$row[6];
        $major=$row[7];
        $minor=$row[8];
        $dob=$row[9];
        $gender=$row[10];
        $type=$row[11];
        $picture=$row[12];
        $pass=$row[13];
        $dateCreated=$row[14];
        $createdBy=$row[15];
        $status = $row[16];
    }
    
    
?>
<!DOCTYPE html>
    <!-- HEAD -->
    <?php include_once 'head.php'; ?> 
    <!-- HEAD   -->
    <body>
        <!-- HEADER -->
        <?php include_once 'header.php'; ?>
        <!-- HEADER -->

        <section id="main">
            <ol class="breadcrumb">
                <li><a href="adminHome.php">Home</a></li>
                <li><a href="adminTeacher.php">Teacher</a></li>
                <li class="active">Teacher Profile</li>
            </ol>
            <?php 
                $toggle = 'adminTeacher';
                include_once 'sidebar.php'; 
            ?>
            <section id="content">

                <div class="container">

                    <div class="block-header">
                        <h1><i class="zmdi zmdi-accounts"></i> Teacher Information
                        </h1>

                        
                    </div>
                    <div id="hehe">
                        <?php include_once 'ajaxJQuery/displayTeacherDetails.php' ?>

                    </div>
                    
                    </div>
                
                    </div>
                </div>
            </section>
        </section>
        <div class="modal fade" id="changepic" tabindex="-1" role="dialog" aria-hidden="true" data-keyboard="false">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <form method="post" enctype='multipart/form-data' id='addProfilePic'>
                        <div class="modal-header">
                            <h1 class="modal-title"><b>PROFILE PICTURE</b></h1>
                        </div>
                        <div class="modal-body">
                           
                            <center>
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-preview thumbnail" data-trigger="fileinput"></div>
                                <div>
                                    <span class="btn btn-info btn-xs btn-file">
                                        <span class="fileinput-new">Select image</span>
                                        
                                        <input type="file" name="file" id="file">
                                    </span>
                                    <button type="submit" name="changeprofilepic" id="changeprofilepic" class="btn btn-success btn-xs">CHANGE PICTURE</button>
                                    
                                </div>
                            </div>
                            </center>
                            <div class="form-group fg-line">
                                <input type="hidden" name="ID" id="ID" value="<?php echo $teacherID; ?>">
                            </div>
                        </div>
                        
                        
                    </form>
                </div>
            </div>
        </div>
        <!-- FOOTER -->
        <?php include_once 'footer.php' ?>
        <!-- FOOTER -->

        <!-- Javascript Libraries -->
        <?php include_once 'scripts.php'; ?>
        <!-- Javascript Libraries -->
        <script type="text/javascript" src="js/Notification.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                

                $('#addProfilePic').on('submit', function(e){
                    e.preventDefault();
                    $.ajax({
                        url:"ajaxJQuery/addProfilePic.php",
                        method: "post",
                        data: new FormData(this),
                        contentType:false,
                        cache: false,
                        processData:false,
                        success:function(data){
                            if(data == "not allowed")
                            {
                                var message = "Unsupported format for profile";
                                executeNotif(message,"danger");
                            }
                            else
                            {
                                location.reload();
                            }
                        }
                    });
                });

                //PAG SEARCH
                $('#search').keyup(function(){
                    var txt = $(this).val();
                    if(txt != '')
                    {
                        $.ajax({
                            url:"ajaxJQuery/searchTeacher.php",
                            method:"POST",
                            data:{search:txt},
                            dataType:"text",
                            success:function(data)
                            {
                                $('#resultsaSearch').html(data);
                            }
                        });
                    }
                    
                });



                $('#editForm').on("submit", function(event){
                    event.preventDefault();
                    
                    if($('#fname').val() == '')  
                    {
                            $('#fname').focus();    
                         alert("First name is required and valid");
                         
                    }  
                   
                    else if($('#lname').val() == '')  
                    {
                        $('#lname').focus();
                         alert("Last name is required");  
                    }

                

                    else  
                    {  
                         $.ajax({  
                              url:"ajaxJQuery/updateTeacher.php",  
                              method:"POST",  
                              data:$('#editForm').serialize(),  
                              beforeSend:function(){  
                                   $('#insert1').val("Inserting..");   
                              },  
                              success:function(data){ 
                                   $('#hehe').html(data);
                                   var message = "successfully edited account";
                                    executeNotif(message,"success");
                                   
                              }  

                         });  
                    }
                });

                $('#editPicture').on("submit", function(e){
                    e.preventDefault();
                    var id = "<?php echo $teacherID; ?>";
                });



            } );
        </script>
    </body>

</html>