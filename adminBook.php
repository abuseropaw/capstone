<?php
    include_once 'sessionAdmin.php';
    include_once 'dbconnect.php';
    function check($con)
    {
        $query = mysqli_num_rows(mysqli_query($con, "SELECT * from subjects"));
        if($query > 0){
            return true;
        }
        else
        {
            return false;
        }
    }
?>
<!DOCTYPE html>
    <!-- HEAD -->
    <?php include_once 'head.php'; ?> 
    <!-- HEAD   -->
    <body>
        <!-- HEADER -->
        <?php include_once 'header.php'; ?>
        <!-- HEADER -->

        <section id="main">
            <ol class="breadcrumb">
                <li><a href="adminHome.php">Home</a></li>
                <li class="active">Books</li>
            </ol>
            <?php 
                $toggle = 'adminBook';
                include_once 'sidebar.php'; 
            ?>
            
            <section id="content">
                <div class="container">
                    <!-- Colored Headers -->
                    <div class="block-header">
                        <h1><i class="zmdi zmdi-library"></i>  BOOKS
                        </h1>

                        <div class="actions">
                            <?php
                            if(check($con)){
                            echo '<button href="#modalColor" data-toggle="modal" class="btn btn-default btn-lg"><i class="zmdi zmdi-library"></i><b> New BOOK</b></button>';
                            }
                            else
                            {
                                echo "<button  href='#' data-toggle='tooltip' data-placement='top' title='Subjects must be added in order to add books. Tips: Go to Enrollment Configuration and click Subjects' class='btn btn-default btn-lg'><i class='zmdi zmdi-library'></i> <b>New BOOK</b></button>";
                            }

                            ?>
                        </div>
                        <div class="modal fade" id="modalColor" data-backdrop="static" data-keyboard="false">
                            <div class="modal-dialog modal-sm">
                                <div class="modal-content">
                                    <form id="myForm" method="post">
                                        <div class="modal-header">
                                            <h1 class="modal-title"><b>CREATE NEW BOOK</b></h1>
                                        </div>
        
                                        <div class="modal-body">
                                            <div class='col-sm-12'>
                                                <div class='form-group fg-float'>
                                                    <div class="fg-line">
                                                        <input type='text' name='book_title' id='book_title' class='form-control fg-input' required>
                                                        <label class="fg-label">Book Title </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class='col-sm-12'>
                                                <div class='form-group fg-float'>
                                                    <div class="fg-line">
                                                        <input type='text' name='book_description' id='book_description' class='form-control fg-input'>
                                                        <label class="fg-label">Book description (Optional)</label>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="col-sm-12">
                                                <div class="form-group fg-line">
                                                    <select class="selectpicker form-control" data-live-search="true" name="subject" id="subject" required>
                                                        <option value="">SUBJECT</option>
                                                    </select>
                                                </div>
                                            </div>
                                           
                                            


                                           
                                        </div>
                                        <div class="modal-footer">
                                            
                                            <button type="submit" name="insert1" id="insert1" class="btn btn-success">CREATE BOOK</button>
                                            <button type="button" class="btn btn-warning" data-dismiss="modal">CANCEL
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="modal fade" id="modalColor1" data-backdrop="static" data-keyboard="false">
                            <div class="modal-dialog modal-sm">
                                <div class="modal-content">
                                    <form id="myForm1" method="post">
                                        <div class="modal-header">
                                            <h1 class="modal-title"><b>EDIT BOOK</b></h1>
                                        </div>
                                        
                                        <div class="modal-body">
                                            <div class='col-sm-12'>
                                                <div class='form-group fg-float'>
                                                    <div class="fg-line">
                                                        <input type='text' name='book_title1' id='book_title1' class='form-control fg-input' required>
                                                        <label class="fg-label">Book Title </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class='col-sm-12'>
                                                <div class='form-group fg-float'>
                                                    <div class="fg-line">
                                                    <input type="hidden" name="book_id" id="book_id">
                                                        <input type='text' name='book_description1' id='book_description1' class='form-control fg-input'>
                                                        <label class="fg-label">Book description</label>
                                                    </div>
                                                </div>
                                            </div>
                                            

                                         
                                        </div>
                                        <div class="modal-footer">
                                            
                                            <button type="submit" name="insert1" id="insert1" class="btn btn-success">EDIT BOOK</button>
                                            <button type="button" class="btn btn-warning" data-dismiss="modal">CANCEL
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        
                        <div id="dataModal" class="modal fade" data-backdrop="static" data-keyboard="false">  
                            <div class="modal-dialog">  
                               <div class="modal-content">  
                                    <div class="modal-header">  
                                         <button type="button" class="close" data-dismiss="modal">&times;</button>  
                                         <h4 class="modal-title"><b>BOOK DETAILS</b></h4>  
                                    </div>  
                                    <div class="modal-body" id="Details">  
                                    </div>  
                                    <div class="modal-footer">  
                                         <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>  
                                    </div>  
                               </div>  
                            </div>  
                        </div>
                    </div>
                    <div class="card">
                        <div class="action-header clearfix">
                            <div class="ah-label hidden-xs" data-ma-action="action-header-open"><b>Click here to search for a book</b></div>

                            <div class="ah-search">
                                <input type="text" name='search1' id="search1" placeholder="Start typing..." class="ahs-input" autofocus>
                                <i class="ahs-close" data-ma-action="action-header-close">&times;</i>
                            </div>
                            
                           <ul class="actions">
                                <li>
                                    <a href="#" data-ma-action="action-header-open">
                                        <i class="zmdi zmdi-search"></i>
                                    </a>
                                </li>

                               
                            </ul>
                        </div>

                        <div class="card-body" id='hehe'>
                            <?php 
                                $query1 = mysqli_query($con, "CALL selectBookDetails()");
                                include_once 'ajaxJQuery/displayBook.php'; 
                            ?>   
                           
                        </div>
                    </div>
                    
                    <br/>
                    <br/>
                </div>
                <button href='#modalColor' data-toggle="modal" class="btn btn-float bgm-green m-btn"><i class="zmdi zmdi-plus"></i></button>
            </section>
        </section>
        
        <!-- FOOTER -->
        <?php include_once 'footer.php'; ?>


       
        <?php include_once 'scripts.php'; ?>
        <!-- Javascript Libraries -->   
        <script type="text/javascript" src="js/Notification.js"></script>
        <!-- AJAX Script -->
        <script>
            $(document).ready(function(){
                $('#data-table-basic').DataTable();

                $('#modalColor').on('shown.bs.modal', function() {
                  $('#book_title').focus();
                    $.ajax({
                        url: "ajaxJQuery/getListofSubject.php",
                        dataType: "json",
                        success:function(data){
                            var length = data.length;
                            $("#subject").empty();
                            $("#subject").append("<option value=''>SUBJECT</option>");
                            for(var i = 0; i<length; i++)
                            {
                                $("#subject").append("<option value='"+data[i]['subj_id']+"'>"+data[i]['subj_title']+" - "+data[i]['Level']+"</option>");
                                $('#subject').selectpicker('refresh');
                            }
                        }
                    });
                })
                $('#modalColor1').on('shown.bs.modal', function() {
                  $('#book_title1').focus();
                  $('#book_description1').focus();
                  
                })


                $('[data-dismiss=modal]').on('click', function (e) {
                    $('#myForm')[0].reset();
                    $('#myForm1')[0].reset();
                })


                $('#add').click(function(){     
                     $('#myForm')[0].reset();  
                });


                $('#search1').keyup(function(objEvent){
                    var txt = $('#search1').val();
                    search(txt);

                    
                });

                $('#globalSearch').keyup(function(objEvent){
                    var txt = $('#globalSearch').val();
                    $('#search1').val(txt);
                    search(txt);
                });

                function search(txt)
                {
                    $.ajax({
                        url:"ajaxJQuery/searchBook.php",
                        method:"POST",
                        data:{search:txt},
                        dataType:"text",
                        success:function(data)
                        {
                            if(!data)
                            {
                                $('#hehe').html(

                                    '<div class="card-body card-padding" id="hehe">'
                                        +'Your search - <b>'+txt+'</b> - did not match any books.'
                                        +'<br />'
                                        +'Suggestions:<br />'
                                        +'<ul style="margin-left:1.3em;margin-bottom:2em"><li>Make sure that all words are spelled correctly.</li><li>Try different keywords.</li><li>Try more general keywords.</li></ul>'
                                    +'</div>'

                                    );
                            }
                            else
                            {
                                $('#hehe').html(data);
                                
                            }
                        }
                    });
                }

                $(document).on('click', '.details', function(){
                    var book_id = $(this).attr("id");
                    $.ajax({  
                         url:"ajaxJQuery/viewBook.php",  
                         method:"POST",  
                         data:{book_id:book_id},  
                         success:function(data){
                              $('#Details').html(data);
                              $('#dataModal').modal('show');  
                         }  
                    });  
                });

                $(document).on('click', '.delete_book', function(){
                    var booky_id = $(this).attr("id");
                    var txt = $('#search1').val();
                    swal({   
                        title: "Are you sure?",   
                        text: "You will not be able to recover this data!",   
                        type: "warning",   
                        showCancelButton: true,   
                        confirmButtonText: "Yes, delete it!",
                    }).then(function(){
                        
                        $.ajax({
                            url:"ajaxJQuery/deleteBook.php",
                            method:"POST",
                            data:{booky_id:booky_id},
                            
                            success:function(data){
                                if(data == '0'){
                                    var message = "Error on deleting book.";
                                    executeNotif(message,"danger");
                                }else{

                                    var message = "Successfully deleted book.";
                                    executeNotif(message,"success");
                                    search(txt);
                                }  
                            }
                        });
                    });
                    
                });
                $(document).on('click', '.edit_book', function(){
                    var book_id = $(this).attr("id");
                    $.ajax({  
                         url:"ajaxJQuery/selectBook.php",  
                         method:"GET",  
                         data:{book_id:book_id},  
                         dataType:"json",
                         success:function(data){
                               
                              $('#book_title1').val(data.book_title);  
                              $('#book_description1').val(data.book_description);
                              $('#book_id').val(data.book_id);
                              $('#insert').val("Insert");  
                              $('#modalColor1').modal('show');  
                         }  
                    });  
                });
                //UPDATE BOOK
                $('#myForm1').on("submit", function(event){
                    event.preventDefault();
                    var txt = $('#search1').val();
                     $.ajax({  
                          url:"ajaxJQuery/updateBook.php",  
                          method:"POST",  
                          data:$('#myForm1').serialize(),  
                          
                          success:function(data){ 
                            if(data =='0')
                            {
                                var message = "Error on editing book.";
                                executeNotif(message,"danger");
                            }
                            else
                            {
                                var message = "Successfully edited book.";
                                executeNotif(message,"success");
                                $('#myForm1')[0].reset(),  
                                $('#modalColor1').modal('hide');
                                search(txt);
                            }
                          }  

                     });  
                    
                });

                //INSERT BOOK

                $('#myForm').on("submit", function(event){
                    event.preventDefault();
                   var txt = $('#search1').val();
                     $.ajax({  
                          url:"ajaxJQuery/insertBook.php",  
                          method:"POST",  
                          data:$('#myForm').serialize(),    
                          success:function(data){
                            if(data != '0')
                            {
                                var message = "Successfully added book.";
                                executeNotif(message,"success");
                                $('#myForm')[0].reset();   
                                $('#modalColor').modal('hide');
                                search(txt);
                                
                            }
                            else
                            {
                                var message = "Error on adding new book.";
                                executeNotif(message,"danger");
                                
                            }
                            
                          }  

                     });  
                    
                });
            });



        </script>
    </body>
</html>