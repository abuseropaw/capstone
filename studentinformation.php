<?php
    session_start();

    include_once 'dbconnect.php';



    $studentID =mysqli_real_escape_string($con, $_GET['sID']);
if($_SESSION['faculty_type'] == 'Teacher')
    {
        header("Location: studentinformation1.php?sID=".$studentID."");
    }
    if(isset($_POST['updateAddress']))
    {
        $hssp = mysqli_real_escape_string($con, $_POST['hssp']);
        $barangay = mysqli_real_escape_string($con, $_POST['barangay']);
        $municipality = mysqli_real_escape_string($con, $_POST['municipality']);
        $province = mysqli_real_escape_string($con, $_POST['province']);
        $SY =  getSY($con);

        if(mysqli_query($con, "UPDATE address set student_addressHSSP='".$hssp."',student_addressBarangay='".$barangay."',student_addressMunicipality='".$municipality."',student_addressProvince='".$province."' WHERE student_id='".$student_id."' and sy_id='".$SY."'"))
        {
           header("Location : studentinformation?sID='".$studentID."'");
        }
       
 header("Location : studentinformation?sID='".$studentID."'");
    }
     

    $getMaxControl_id = mysqli_fetch_row(mysqli_query($con, "SELECT * from controlinformation where control_id=(SELECT max(control_id) from control where student_id='".mysqli_real_escape_string($con, $_GET['sID'])."');"));
    $enrolledSY = $getMaxControl_id[6];
    $yrlevel =  $getMaxControl_id[19];
    $rem = $getMaxControl_id[1];

    $query = mysqli_query($con, "SELECT * from studentinfo1 where student_id='".$_GET['sID']."'");
    if($row = mysqli_fetch_array($query)){
        $id = $row['student_id'];
        $lrn = $row['student_LRN'];
        $name=$row['Name'];
        $sex = $row['sex'];

    //$BirthDate=$row['BirthDate'];
        $age = $row['Age'];
        $birthPlace=$row['BirthPlace'];
        $motherTongue=$row['MotherTongue'];
        $IP=$row['IP'];
        $Religion=$row['Religion'];
        $sy_id=$row['sy_id'];
        $sy_year=$row['sy_year'];
        $remarks = $row['control_remarks'];
        $sectionID = $row['section_id'];


        $HSSP=$row['student_addressHSSP'];
        $Barangay=$row['student_addressBarangay'];
        $Municipality=$row['student_addressMunicipality'];
        $Province=$row['student_addressProvince'];
        $Father=$row['Father'];
        $Mother=$row['Mother'];
        $GuardianName=$row['GuardianName'];
        $Relationship=$row['Relationship'];
        $GContact=$row['GContact'];
        $PContact=$row['PContact'];

    }
    $query1 = mysqli_query($con, "SELECT * from student_account where student_id='".$studentID."'");
    if($row = mysqli_fetch_array($query1)){
        $picture = $row['student_picture'];
        $fname = $row[2];
        $mname = $row[3];
        $lname = $row[4];
        $sex = $row[5];
        $age=$row[7];
        $BirthDate = $row['student_birthDate'];


    }
    $query2 = mysqli_query($con, "SELECT * from parent where student_id='".$studentID."'");
    if($row = mysqli_fetch_array($query2)){
        $ffname = $row[1];
        $fmname = $row[2];
        $flname = $row[3];
        $mfname = $row[4];
        $mmname = $row[5];
        $mlname = $row[6];

           
    }
    function getSY($con)
    {
        $q = mysqli_query($con, "SELECT sy_id from schoolyear where sy_remarks='Open'");
        $temp = mysqli_fetch_row($q);
        $temp1 = $temp[0];
        return $temp1;
    }
?>
<!DOCTYPE html>
    <!-- HEAD -->
    <?php include_once 'head.php'; ?> 
    <!-- HEAD   -->
    <body>
        <!-- HEADER -->
        <?php include_once 'header.php'; ?>
        <!-- HEADER -->

        <section id="main">
            
            <section id="content ">

                <div class="container" class="col-sm-12">

                    <div class="block-header">
                     
                        
                    </div>
                        <div class="row" id='sectionList'>

                            <div class="col-sm-12">
                                <div class='card'>
                                    
                                    <div class="card-body">
                                        <div class="card" id="profile-main">
                                            <div class="pm-overview c-overflow">
                                                
                                                <div class="pmo-pic">
                                                    <div class="p-relative">
                                                        <a href="#changepic" data-toggle='modal'>
                                                            <img class="img-responsive" id='profilePicture' src="profile/<?php echo ($picture == null) ? 'default.png':$picture ?>" alt="">
                                                        </a>
                                                        <a href="#changepic" data-toggle='modal' class="pmop-edit">
                                                           <i class="zmdi zmdi-camera"></i> <span class="hidden-xs">Update Profile Picture</span>
                                                        </a>

                                                        
                                                    </div>


                                                        
                                                </div>

                                                <div class="pmo-block pmo-contact hidden-xs">
                                                    
                                                    <h2 class="text-center"><center><b><?php echo $name; ?></b></center></h2>
                                                    <h2 ><center><b>LRN # :</b><?php echo $lrn; ?></center></h2>
                                                    <h2><center><b>Attended SY:</b><?php echo $enrolledSY; ?></center></h2>
                                                    <h2><center><b>Status :</b><?php echo $rem; ?></center></h2>
                                                    <h2><center><b>Level :</b><?php echo $yrlevel; ?></center></h2>
                                                    
                                                </div>

                                            
                                            </div>

                                            <div class="pm-body clearfix">
                                                <div role="tabpanel">
                                                    <ul class="tab-nav tn-justified" role="tablist" data-tab-color="green">
                                                        <li class="active"><a href="#acount" aria-controls="acount" role="tab"
                                                          data-toggle="tab">Profile</a></li>
                                                        <li><a href="#class" aria-controls="class" role="tab" data-toggle="tab">Report Card</a></li>
                                                        
                                                        
                                                        <li><a href="#advisory" aria-controls="advisory" role="tab" data-toggle="tab">Books Acquired</a></li>
                                                       
                                                    </ul>
                                                    <div class="tab-content">

                                                        
                                                        <?php include_once 'studentinformation_profile.php' ?>
                                                        <div role="tabpanel" class="tab-pane" id="class">
                                                            <div class="pmb-block">

                                                                <div class="pmbb-header">
                                                                    <h2><i class="zmdi zmdi-chart m-r-10"></i> <b>Report Cards</b></h2>
                                                                </div>
                                                                <?php

                                                                $querycs = mysqli_query($con, "SELECT * from controlinformation where student_id='".$id."'");


                                                                while ($row4 = mysqli_fetch_array($querycs)) {
                                                                    $controlID = $row4[0];
                                                                    $sectionID = $row4[4];
                                                                    $schoolYearID = $row4['sy_id'];
                                                                    $SY = $row4['sy_year'];

                                                          echo '<div class="pmbb-body p-l-30">
                                                                    <div class="card">
                                                                        <div class="table-responsive">
                                                                            <table class="table table-bordered">
                                                                                <thead>
                                                                                    <th rowspan="2" class="text-center" width="30%"><h2><b>'.$row4[19].' <br />'.$row4['sy_year'].'</b></h2></th>
                                                                                </thead>
                                                                                <tbody>
                                                                                    
                                                                                </tbody>
                                                                            </table>
                                                                            <table class="table table-bordered">
                                                                               
                                                                            <thead>
                                                                                <tr>

                                                                                <th rowspan="2" class="text-center" width="30%"><b>Subject Title</b></th>
                                                                                <th colspan="5" class="text-center" width="70%"><b>Grades</b></th>

                                                                                </tr>

                                                                                <tr>
                                                                                <th width="15%" class="text-center"><small><small><small>1st Quarter</small></small></small></th>
                                                                                <th width="15%" class="text-center"><small><small><small>2nd Quarter</small></small></small></th>
                                                                                <th width="15%" class="text-center"><small><small><small>3rd Quarter</small></small></small></th>
                                                                                <th width="15%" class="text-center"><small><small><small>4th Quarter</small></small></small></th>
                                                                                <th width="15%" class="text-center">Final</th>

                                                                                </tr>
                                                                            </thead>
                                                                                <tbody>';
                                                                                        $getSO = mysqli_query($con, "SELECT * from subjectofferingdetails where section_id='".$sectionID."'");
                                                                                        $countgetSO = mysqli_num_rows(mysqli_query($con, "SELECT * from subjectofferingdetails where section_id='".$sectionID."'"));
                                                                                        if($countgetSO > 0){
                                                                                            while ($row1 = mysqli_fetch_array($getSO)) {
                                                                                                $soID = $row1[0];
                                                                                                echo "
                                                                                                    <tr>
                                                                                                    <td>".$row1[6]."</td>";
                                                                                                    
                                                                                                    $getQuarters = mysqli_query($con, "SELECT * from quarters where sy_id='".$SY."'");
                                                                                                    $countQ = mysqli_num_rows($getQuarters);

                                                                                                    if(mysqli_num_rows($getQuarters) != 0)
                                                                                                    {
                                                                                                        while ($item = mysqli_fetch_array($getQuarters)) {
                                                                                                   
                                                                                                            if($row2 = mysqli_fetch_row(mysqli_query($con, "SELECT * from grade where control_id='".$controlID."' and so_id='".$soID."' and quarters_id='".$item['quarters_id']."'"))){
                                                                                                                if($row2[0] < 75)
                                                                                                                {
                                                                                                                    echo "<td class='c-red'>".$row2[0]."</td>";
                                                                                                                }
                                                                                                                else
                                                                                                                {
                                                                                                                    echo "<td class='c-green'>".$row2[0]."</td>";
                                                                                                                    
                                                                                                                }
                                                                                                            }
                                                                                                            else
                                                                                                            {
                                                                                                                echo "<td></td>";
                                                                                                            }

                                                                                                        }
                                                                                                    }
                                                                                                    else{
                                                                                                        echo "<td></td><td></td><td></td><td></td><td></td>";
                                                                                                    }
                                                                                                    

                                                                                                    if(mysqli_num_rows($getQuarters) == 1){
                                                                                                    echo "<td></td><td></td><td></td>";  
                                                                                                    }else if(mysqli_num_rows($getQuarters) == 2){
                                                                                                        echo "<td></td><td></td>";

                                                                                                    }else if(mysqli_num_rows($getQuarters) == 3){
                                                                                                        echo "<td></td>";

                                                                                                    }else if(mysqli_num_rows($getQuarters) == 4){

                                                                                                    }

                                                                                                    $queryfinal = mysqli_query($con, "SELECT * from soaveragegrade where so_id = '".$soID."' and control_id='".$controlID."'");
                                                                                                    if ($row3 = mysqli_fetch_row($queryfinal)) {
                                                                                                        if($row3[0] < 75)
                                                                                                        {
                                                                                                            echo "<td class='c-red'>".$row3[0]."</td>";
                                                                                                        }
                                                                                                        else
                                                                                                        {
                                                                                                            echo "<td class='c-green'>".$row3[0]."</td>";
                                                                                                            
                                                                                                        }
                                                                                                    }
                                                                                                    else
                                                                                                    {
                                                                                                        echo "<td></td>";
                                                                                                    }

                                                                                                echo "
                                                                                                    </tr>
                                                                                                ";
                                                                                            }
                                                                                        }
                                                                                        else{
                                                                                            echo "<td></td><td></td><td></td><td></td><td></td><td></td>";
                                                                                        }
                                                                            $averageGrade = 0;
                                                                            $getAverageGrade = "SELECT IFNULL(average,0) as average from average where control_id='".$controlID."'";
                                                                            $getAverage = mysqli_fetch_row(mysqli_query($con, $getAverageGrade));
                                                                            if(mysqli_num_rows(mysqli_query($con, $getAverageGrade)) != 0){
                                                                            $averageGrade = $getAverage[0];
                                                                            }
                                                                            echo '
                                                                                    
                                                                                    </tbody>
                                                                                   
                                                                            </table>
                                                                             <table class="table table-bordered">
                                                                                <thead>
                                                                                    <th rowspan="2" class="text-center" width="30%"><b>Total GPA: '.$averageGrade.'</b></th>
                                                                                </thead>
                                                                                <tbody>
                                                                                    
                                                                                </tbody>
                                                                            </table>
                                                                            <table class="table table-bordered">
                                                                               
                                                                            <thead>
                                                                                <tr>
                                                                                <th rowspan="2" class="text-center" width="30%"><b>Total</b></th>
                                                                                <th colspan="12" class="text-center" width="70%"><b>ATTENDANCE</b></th>

                                                                                </tr>

                                                                                <tr>
                                                                                <th class="text-center"><small><small><small>June</small></small></small></th>
                                                                                <th class="text-center"><small><small><small>July</small></small></small></th>
                                                                                <th class="text-center"><small><small><small>Aug</small></small></small></th>
                                                                                <th class="text-center"><small><small><small>Sept</small></small></small></th>
                                                                                <th class="text-center"><small><small><small>Oct</small></small></small></th>
                                                                                <th class="text-center"><small><small><small>Nov</small></small></small></th>
                                                                                <th class="text-center"><small><small><small>Dec</small></small></small></th>
                                                                                <th class="text-center"><small><small><small>Jan</small></small></small></th>
                                                                                <th class="text-center"><small><small><small>Feb</small></small></small></th>
                                                                                <th class="text-center"><small><small><small>Mar</small></small></small></th>
                                                                                <th class="text-center"><small><small><small>Total</small></small></small></th>

                                                                                </tr>
                                                                            </thead>
                                                                                <tbody>';
                                                                                        

                                                                                        $getMonth = mysqli_query($con, "SELECT * from month where sy_id='".$schoolYearID."' order by month_id asc");
                                                                                        $getMonth1 = mysqli_query($con, "SELECT * from month where sy_id='".$schoolYearID."' order by month_id asc");
                                                                                        $getMonth2 = mysqli_query($con, "SELECT * from month where sy_id='".$schoolYearID."' order by month_id asc");
                                                                                        $getMonth3 = mysqli_query($con, "SELECT * from month where sy_id='".$schoolYearID."' order by month_id asc");

                                                                                        $months = array('June','July','August','September','October','November','December','January','February','March');

                                                                                        if(mysqli_num_rows($getMonth) == 0)
                                                                                        {
                                                                                            echo "
                                                                                                <tr>
                                                                                                    <td>Number of days in school year</td>
                                                                                                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>Number of days present</td>
                                                                                                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>Number of days late</td>
                                                                                                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>Number of days absent</td>
                                                                                                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                                                                                                </tr>
                                                                                            ";
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            echo "<t>
                                                                                            <td>Number of days in shool year</td>
                                                                                            ";
                                                                                            for ($i=0; $i < count($months) ; $i++) { 
                                                                                                $find = mysqli_query($con, "SELECT * from month where month_description='".$months[$i]."' and sy_id='".$schoolYearID."'");
                                                                                                $temp = mysqli_fetch_row($find);
                                                                                                if(mysqli_num_rows($find) == 0)
                                                                                                {
                                                                                                    echo "<td>0</td>";
                                                                                                }
                                                                                                else
                                                                                                {
                                                                                                    echo "<td>".$temp[2]."</td>";
                                                                                                }
                                                                                            }
                                                                                            $getTotal_totalDays = mysqli_fetch_row(mysqli_query($con, "SELECT IFNULL(sum(month_totalDays),0) from month where sy_id='".$schoolYearID."'"));

                                                                                            echo "
                                                                                                <td>".$getTotal_totalDays[0]."</td>
                                                                                            </tr>";

                                                                                            //PRESENT
                                                                                            echo "<t>
                                                                                            <td>Number of days present</td>
                                                                                            ";
                                                                                           
                                                                                            for ($i=0; $i < count($months) ; $i++) { 
                                                                                                $find = mysqli_query($con, "SELECT * from month where month_description='".$months[$i]."' and sy_id='".$schoolYearID."'");
                                                                                                $temp = mysqli_fetch_row($find);
                                                                                                if(mysqli_num_rows($find) == 0)
                                                                                                {
                                                                                                    echo "<td>0</td>";
                                                                                                }
                                                                                                else
                                                                                                {
                                                                                                    $query = mysqli_query($con, "SELECT * from attendancedetails where month_id='".$temp[0]."' and control_id='".$controlID."'");
                                                                                                    if(mysqli_num_rows($query) == 0)
                                                                                                    {
                                                                                                        echo "<td>0</td>";
                                                                                                    }
                                                                                                    else
                                                                                                    {
                                                                                                        $temp = mysqli_fetch_row($query);
                                                                                                        echo "<td>".$temp[5]."</td>";
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                            $getTotal_presentDays = mysqli_fetch_row(mysqli_query($con, "SELECT IFNULL(sum(attendance_presentDays),0) from attendance where control_id='".$controlID."'"));
                                                                                            
                                                                                            echo "
                                                                                                <td>".$getTotal_presentDays[0]."</td>
                                                                                            </tr>";

                                                                                            //LATE
                                                                                            echo "<t>
                                                                                            <td>Number of days late</td>
                                                                                            ";
                                                                                            for ($i=0; $i < count($months) ; $i++) { 
                                                                                                $find = mysqli_query($con, "SELECT * from month where month_description='".$months[$i]."' and sy_id='".$schoolYearID."'");
                                                                                                $temp = mysqli_fetch_row($find);
                                                                                                if(mysqli_num_rows($find) == 0)
                                                                                                {
                                                                                                    echo "<td>0</td>";
                                                                                                }
                                                                                                else
                                                                                                {
                                                                                                    $query = mysqli_query($con, "SELECT * from attendancedetails where month_id='".$temp[0]."' and control_id='".$controlID."'");
                                                                                                    if(mysqli_num_rows($query) == 0)
                                                                                                    {
                                                                                                        echo "<td>0</td>";
                                                                                                    }
                                                                                                    else
                                                                                                    {
                                                                                                        $temp = mysqli_fetch_row($query);
                                                                                                        echo "<td>".$temp[7]."</td>";
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                            $getTotal_absentDays = mysqli_fetch_row(mysqli_query($con, "SELECT IFNULL(sum(attendance_tardyDays),0) from attendance where control_id='".$controlID."'"));
                                                                                            echo "
                                                                                                <td>".$getTotal_absentDays[0]."</td>
                                                                                            </tr>";

                                                                                            //ABSENT
                                                                                            echo "<t>
                                                                                            <td>Number of days absent</td>
                                                                                            ";
                                                                                            for ($i=0; $i < count($months) ; $i++) { 
                                                                                                $find = mysqli_query($con, "SELECT * from month where month_description='".$months[$i]."' and sy_id='".$schoolYearID."'");
                                                                                                $temp = mysqli_fetch_row($find);
                                                                                                if(mysqli_num_rows($find) == 0)
                                                                                                {
                                                                                                    echo "<td>0</td>";
                                                                                                }
                                                                                                else
                                                                                                {
                                                                                                    $query = mysqli_query($con, "SELECT * from attendancedetails where month_id='".$temp[0]."' and control_id='".$controlID."'");
                                                                                                    if(mysqli_num_rows($query) == 0)
                                                                                                    {
                                                                                                        echo "<td>0</td>";
                                                                                                    }
                                                                                                    else
                                                                                                    {
                                                                                                        $temp = mysqli_fetch_row($query);
                                                                                                        echo "<td>".$temp[6]."</td>";
                                                                                                    }
                                                                                                }
                                                                                            }

                                                                                            $getTotal_tardyDays = mysqli_fetch_row(mysqli_query($con, "SELECT IFNULL(sum(attendance_absentDays),0) from attendance where control_id='".$controlID."'"));


                                                                                            echo "
                                                                                                <td>".$getTotal_tardyDays[0]."</td>
                                                                                            </tr>";
                                                                                        }
                                                                                        
                                                                                                          
                    
                                                                            echo '
                                                                                    
                                                                                    </tbody>
                                                                                   
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>';

                                                                } 

                                                                ?>                                                               

                                                            </div>
                                                            
                                                        </div>


                                                        
                                                        
                                                        <div role="tabpanel" class="tab-pane" id="advisory">
                                                            <div class="pmb-block">
                                                                <div class="pmbb-header">
                                                                    <h2><i class="zmdi zmdi-book m-r-10"></i> <b>Books Acquired </b></h2>
                                                                </div>
                                                                <div class="pmbb-body p-l-30">
                                                                    <div class="card">
                                                                        <div class="table-responsive">
                                                                            <table id="table-basic" class="table table-bordered table-nowrap dataTable">

                                                                                    <?php
                                                                                    $query1 = mysqli_query($con, "SELECT * from controldetails where student_id='".$id."'");

                                                                                        while($row = mysqli_fetch_array($query1)){
                                                                                            $control_id = $row[0];
                                                                                            $schoolyear = $row[10];

                                                                                            echo "
                                                                                            <thead class='text-center'>
                                                                                                <tr>
                                                                                                <th colspan='6' class='text-center'><b>".$schoolyear.' - '.$row['SectionName']."</b></th>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <th rowspan='2' class='text-center'><b>No.</b></th>
                                                                                                    <th colspan='2' class='text-center'><b>Book</b></th>
                                                                                                    <th colspan='2' class='text-center'><b>Date</b></th>
                                                                                                    <th rowspan='2' class='text-center'><b>Remarks</b></th>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <th class='text-center'><small><small><small>Title</small></small></small></th>
                                                                                                    <th class='text-center'><small><small><small>Description</small></small></small></th>
                                                                                                    <th class='text-center'><small><small><small>Issued</small></small></small></th>
                                                                                                    <th class='text-center'><small><small><small>Returned</small></small></small></th>
                                                                                                    

                                                                                                </tr>
                                                                                            </thead>
                                                                                            <tbody>
                                                                                            ";

                                                                                            $query = mysqli_query($con, "SELECT * from booksacquireddetails where control_id ='".$control_id."' ");
                                                                                            $iter= 1;
                                                                                        while($row = mysqli_fetch_array($query)){
                                                                                            $temp = $row[0];
                                                                                            $queryDateIssued =mysqli_fetch_row(mysqli_query($con, "SELECT DATE_FORMAT(date_acquired, '%b. %e, %Y') from booksacquireddetails where control_id='".$temp."'"));
                                                                                            $issued = $queryDateIssued[0];
                                                                                           

                                                                                            $queryDateReturned = mysqli_fetch_row(mysqli_query($con, "SELECT DATE_FORMAT(date_returned, '%b. %e, %Y') from booksacquireddetails where control_id='".$temp."'"));
                                                                                            $returned = $queryDateReturned[0];
                                                                                            echo "
                                                                                            <tr>
                                                                                                <td>".$iter."</td>
                                                                                                <td>".$row[7]."</td>
                                                                                                <td>".$row[8]."</td>
                                                                                                <td>".$issued."</td>
                                                                                                <td>".$returned."</td>
                                                                                                <td>".$row[9]."</td>
                                                                                            </tr>    
                                                                                            ";
                                                                                            $iter = $iter + 1;
                                                                                        }

                                                                                        }
                                                                                        
                                                                                    ?>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    
                    </div>
                
                    </div>
                </div>
            </section>
        </section>
        <div class="modal fade" id="changepic" tabindex="-1" role="dialog" aria-hidden="true"  data-keyboard="false">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <form id='addProfilePic' method="post" enctype='multipart/form-data'>
                        <div class="modal-header">
                            <h1 class="modal-title"><b>PROFILE PICTURE</b></h1>
                        </div>
                        <div class="modal-body">
                            <center>
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-preview thumbnail" data-trigger="fileinput"></div>
                                <div>
                                    <span class="btn btn-info btn-xs btn-file">
                                        <span class="fileinput-new">Select image</span>
                                        
                                        <input type="file" name="file" id="file">
                                    </span>
                                    <button type="submit" name="changeprofilepic" id="changeprofilepic" class="btn btn-success btn-xs">CHANGE PICTURE</button>
                                    
                                </div>
                            </div>
                            </center>
                            <div class="form-group fg-line">
                                <input type='hidden' name="ID" id="ID" value="<?php echo $studentID; ?>">
                            </div>
                        </div>

                        
                    </form>
                </div>
            </div>
        </div>
        <!-- FOOTER -->
        <?php include_once 'footer.php' ?>
        <!-- FOOTER -->

        <!-- Javascript Libraries -->
        <?php include_once 'scripts.php'; ?>
        <!-- Javascript Libraries -->
        <script type="text/javascript" src="js/Notification.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                

                $('#addProfilePic').on('submit', function(e){
                    e.preventDefault();
                    $.ajax({
                        url:"ajaxJQuery/addProfilePic.php",
                        method: "post",
                        data: new FormData(this),
                        contentType:false,
                        cache: false,
                        processData:false,
                        success:function(data){
                            if(data == "not allowed")
                            {
                                var message = "Unsupported format for profile";
                                executeNotif(message,"danger");
                            }
                            else if(data == "successLearner")
                            {
                                $('#profilePicture').
                            }
                        }
                    });
                });

                //PAG SEARCH
                $('#updateS').on("submit", function(event){
                    event.preventDefault();
                    $.ajax({  
                      url:"ajaxJQuery/updateStudents.php",  
                      method:"POST",  
                      data:$('#updateS').serialize(),  
                    
                      success:function(data){ 
                            if(data == "0")
                            {
                                var message = "Please review the form";
                                executeNotif(message,"danger");
                            }
                            else
                            {   
                                var message = "Successfully edited student account";
                                executeNotif(message,"success");
                                $('#closebutton').click();
                                $('#account').html(data);
                                location.reload();
                            }
                           
                      }  

                 }); 
                    
                });
                $('#updateA').on("submit", function(event){
                    event.preventDefault();

                    $.ajax({  
                      url:"ajaxJQuery/updateAddress.php",  
                      method:"POST",  
                      data:$('#updateA').serialize(),  
                    
                      success:function(data){ 
                           
                           if(data == '0')
                            { alert ('You cannot edit old data of address');
                    }
                           else { location.reload(); }

                      }  

                 }); 
                    
                });
                $('#updateP').on("submit", function(event){
                    event.preventDefault();

                    $.ajax({  
                      url:"ajaxJQuery/updateParent.php",  
                      method:"POST",  
                      data:$('#updateP').serialize(),  
                    
                      success:function(data){ 
                           
                           if(data == '0')
                            { alert ('You cannot edit the old data of parents');
                    }
                           else {  
                            location.reload();
                           }

                      }  

                 }); 
                    
                });


                

            } );
        </script>
    </body>

</html>