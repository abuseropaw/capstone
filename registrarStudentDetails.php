<?php
    session_start();
   if($_SESSION['faculty_id'] == ''){ header("Location: index.php");}else if( $_SESSION['faculty_type'] != 'Head Teacher' && $_SESSION['faculty_type'] != 'Registrar' ){header("Location: javascript:");}
    include_once 'dbconnect.php';
    include 'ajaxJQuery/globalFunction.php';

    $id =mysqli_real_escape_string($con, $_GET['sID']);

    $query = mysqli_fetch_row(mysqli_query($con, "SELECT section_name,sy_id from section where section_id='".$id."'"));
    $name = $query[0];
    $sy = $query[1];

    if($sy != getCurrentSY1($con))
    {
        echo "
        <script>
           

            javascript:window.close()
        </script>";
    }

    

    function checkifLowest($con, $id)
    {
        $query = mysqli_fetch_row(mysqli_query($con, "SELECT year_lvl_id from section where section_id='".$id."'"));
        $yr_id = $query[0];
        if($yr_id == getLevel($con))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    function getLevel($con)
    {
        $query = mysqli_fetch_row(mysqli_query($con, "SELECT min(year_lvl_id) from yearlevel"));
        $id = $query[0];

        return $id;
    }
    
    function getLastSY($con)
    {
        $query = mysqli_fetch_row(mysqli_query($con, "SELECT max(sy_id) from schoolyear"));
        $id = $query[0];
        $id--;
        $query1 = mysqli_fetch_row(mysqli_query($con, "SELECT sy_year from schoolyear where sy_id='".$id."'"));

        return $query1[0];
    }
    function getSectionLevel($con, $id)
    {
        $query = mysqli_fetch_row(mysqli_query($con, "SELECT year_lvl_id from section where section_id='".$id."'"));
        return $query[0];
    }
    function getSectionLevel1($con,$id)
    {   
       
        $query = mysqli_fetch_row(mysqli_query($con, "SELECT GradeLevel from classinformation where section_id='".$id."'"));
        return $query[0];
    }
    $level = getSectionLevel1($con,$id);
    
?>

<!DOCTYPE html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" type="image/x-icon" href="img/lnhs.ico" alt="logo" />
<title>SIS | <?php echo $name; ?></title>

<link href="vendors/bower_components/animate.css/animate.min.css" rel="stylesheet">
<link href="vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css" rel="stylesheet">
<link href="vendors/bower_components/sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
<link href="vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet">
<link href="vendors/bower_components/dropzone/dist/min/dropzone.min.css" rel="stylesheet">
<link href="vendors/bower_components/chosen/chosen.css" rel="stylesheet">
<link href="vendors/bower_components/datatables.net-dt/css/jquery.dataTables.min.css" rel="stylesheet">
<link href="vendors/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">
<link href="vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
<link href="vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet">
<link href="vendors/bower_components/lightgallery/dist/css/lightgallery.min.css" rel="stylesheet">

<link href="css/app_1.min.css" rel="stylesheet">
<link href="css/app_2.min.css" rel="stylesheet">
<link href="css/demo.css" rel="stylesheet">
</head>
    <body>
        <!-- HEADER -->
        <?php include_once 'header.php'; ?>
        <!-- HEADER -->

        <section id="main">
           
            
                <div class="container">
                    <div class="block-header">
                        <h1><i class="zmdi zmdi-accounts"></i> <?php echo $name; ?>
                        </h1>
                        <div class="actions">
                            
                            <?php
                                if(!checkifLowest($con,$id))
                                { 
                                    echo "<button  href='#modalLoad' data-toggle='modal' id='".$id."' class='btn btn-default  loadClass'><i class='zmdi zmdi-account-box'></i> LOAD CLASS</button>";
                                }
                                else
                                {

                                }
                            ?>
                        </div>
                    </div>
                    <div class="card">

                        <div class="card-body card-padding">
                            <div role="tabpanel">
                                <ul class="tab-nav" role="tablist" data-tab-color="green">
                                     <li class="active"><a href="#learner" aria-controls="learner" role="tab"
                                                          data-toggle="tab">Learners</a></li>
                                
                                     <li><a href="#enrollold" aria-controls="enroll" role="tab" data-toggle="tab">Enrollment</a></li>
                                     <li><a href="#enrollnew" aria-controls="enroll" role="tab" data-toggle="tab">Enrollment for Transferee</a></li>
                                   
                                </ul>

                                <!-- GRADE --><!-- GRADE --><!-- GRADE --><!-- GRADE --><!-- GRADE --><!-- GRADE -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="learner">
                                        <div class="card">
                                            <div class="action-header clearfix" data-ma-action="action-header-open">
                                                <div class="ah-label hidden-xs"><b>Click here to search enrolled learner</b></div>

                                                <div class="ah-search">
                                                    <input type="text" name='searchenrolled' id="searchenrolled" placeholder="Start typing..." class="ahs-input" autofocus>
                                                    <i class="ahs-close" data-ma-action="action-header-close">&times;</i>
                                                </div>

                                               <ul class="actions">
                                                    <li>
                                                        <a href="#" data-ma-action="action-header-open">
                                                            <i class="zmdi zmdi-search"></i>
                                                        </a>
                                                    </li>


                                                </ul>
                                            </div>
                                            <div id='hehe' class="card-body">
                                                <?php include_once 'ajaxJQuery/displayregistrarStudentDetails.php'; ?>
                                            </div>
                                        </div>
                                    </div>

                                    <div role="tabpanel" class="tab-pane" id="enrollold">
                                        <div class="card">
                                            <div class="action-header clearfix" data-ma-action="action-header-open">
                                                <div class="ah-label hidden-xs"><b>Click here to search (LRN or Full Name) -- Please hit Enter to to start search</b></div>

                                                <div class="ah-search">
                                                    <input type="text" name='search' id="search" placeholder="Start typing..." class="ahs-input">
                                                    <input type='hidden' name='secID' id='secID' value='<?php echo $id; ?>'>
                                                    <i class="ahs-close" data-ma-action="action-header-close">&times;</i>
                                                </div>
                        
                                                <ul class="actions">
                                                    <li>
                                                        <a href="#" data-ma-action="action-header-open">
                                                            <i class="zmdi zmdi-search"></i>
                                                        </a>
                                                    </li>
                                                    
                                                   
                                                </ul>
                                            </div>

                                            <div class="card-body card-padding">

                                                <div class="contacts clearfix row" id="resultsaSearch">

                                                    
                                     
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div role="tabpanel" class="tab-pane" id="enrollnew">

                                        <form method="POST" id="insert_form">
                        <div class="card-header">
                           <h4><b>STUDENT PERSONAL INFORMATION</b></h4>
                        </div>
                        
                        <div class="card-body">
                            <div class='col-sm-3' id='lrnForm'>
                                <div class='form-group fg-line'>
                                    <input type='text' name='student_LRN' id='student_LRN' class='form-control' placeholder='LRN' autofocus required>
                                </div>
                                <small class="help-block hidden" id='helpMessage'>Learner's LRN already exist</small>
                            </div>
                            <div class='col-sm-3'>
                                <div class='form-group fg-line'>
                                    <input type='text' name='student_fname' id='student_fname' class='form-control' placeholder='First name' autofocus required>
                                </div>
                            </div>
                            <div class='col-sm-3'>
                                <div class='form-group fg-line'>
                                    <input type='text' name='student_mname' id='student_mname' class='form-control' placeholder='Middle name'>

                                </div>
                            </div>
                            <div class='col-sm-3'>
                                <div class='form-group fg-line'>
                                    <input type='text' name='student_lname' id='student_lname' class='form-control' placeholder='Last name' required>

                                </div>
                            </div>

                            <div class="col-sm-2">
                                <div class="form-group fg-line">
                                    <select class="selectpicker form-control" data-live-search="true" name='student_gender' id='student_gender' required>
                                        <option value="">Gender</option>
                                        <option>Male</option>
                                        <option>Female</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-2">
                                <div class="form-group fg-line">
                                    <input type='text' class="form-control date-picker"
                               placeholder="Date of Birth" name="student_birthDate" id="student_birthDate" required>
                                </div>
                            </div>

                            <div class='col-sm-1'>
                                <div class='form-group fg-line'>
                                    <input type='number' name='student_age' id='student_age' class='form-control' placeholder='Age' required>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group fg-line">
                                    <select class="selectpicker form-control" data-live-search="true" name='student_motherTongue' id='student_motherTongue' required>
                                        <option value="">Mother Tongue</option>
                                        <option>Cebuano</option>
                                        <option>Filipino</option>
                                        <option>Sinugbuanong Binisaya</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group fg-line">
                                    <select class="selectpicker form-control" data-live-search="true" name='student_IP' id='student_IP' required>
                                        <option value="">Indigenous People</option>
                                        <option>Christian</option>
                                        <option>Lumad</option>
                                        <option>Muslim</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group fg-line">
                                    <select class="selectpicker form-control" data-live-search="true" name='student_religion' id='student_religion' required>
                                        <option value="">Religion</option>
                                        <option>Christianity</option>
                                        <option>Muslim</option>
                                        
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group fg-line">
                                    <input type="text" class="form-control" name="student_addressHSSP" id="student_addressHSSP" placeholder="House #/ Street/ Purok" required >
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group fg-line">
                                    <input type="text" class="form-control" name="student_addressBarangay" id="student_addressBarangay" placeholder="Barangay" required>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group fg-line">
                                    <select class="selectpicker form-control" data-live-search="true" name='student_addressMunicipality' id='student_addressMunicipality' required>
                                        <option value="">Municipality</option>
                                        <option>Lugait</option>
                                        <option>Malaybalay</option>
                                        <option>Valencia</option>
                                        <option>Baungon</option>
                                        <option>Cabanglasan</option>
                                        <option>Damulog</option>
                                        <option>Dangcagan</option>
                                        <option>Don Carlos</option>
                                        <option>Impasug-ong</option>
                                        <option>Kadingilan</option>
                                        <option>Kibawe</option>
                                        <option>Kitaotao</option>
                                        <option>Lantapan</option>
                                        <option>Malitbog</option>
                                        <option>Manolo Fortich</option>
                                        <option>Maramag</option>
                                        <option>Pangantucan</option>
                                        <option>Quezon</option>
                                        <option>San Fernando</option>
                                        <option>Sumilao</option>
                                        <option>Talakag</option>
                                        <option>Catarman</option>
                                        <option>Guinsiliban</option>
                                        <option>Mahinog</option>
                                        <option>Mambajao</option>
                                        <option>Sagay</option>
                                        <option>Bacolod</option>
                                        <option>Baloi</option>
                                        <option>Baroy</option>
                                        <option>Kapatagan</option>
                                        <option>Kauswagan</option>
                                        <option>Kolambugan</option>
                                        <option>Lala</option>
                                        <option>Linamon</option>
                                        <option>Magsaysay</option>
                                        <option>Maigo</option>
                                        <option>Matungao</option>
                                        <option>Munai</option>
                                        <option>Nunungan</option>
                                        <option>Pantao Ragat</option>
                                        <option>Pantar</option>
                                        <option>Poona Piagapo</option>
                                        <option>Salvador</option>
                                        <option>Sapad</option>
                                        <option>Tagoloan</option>
                                        <option>Tangkal</option>
                                        <option>Tubod</option>
                                        <option>Iligan</option>
                                        <option>Oroquieta</option>
                                        <option>Ozamiz</option>
                                        <option>Tangub</option>
                                        <option>Aloran</option>
                                        <option>Baliangao</option>
                                        <option>Bonifacio</option>
                                        <option>Calamba</option>
                                        <option>Clarin</option>
                                        <option>Concepcion</option>
                                        <option>Don Victoriano</option>
                                        <option>Jimenez</option>
                                        <option>Lopez Jaena</option>
                                        <option>Panaon</option>
                                        <option>Plaridel</option>
                                        <option>Sapang Dalaga</option>
                                        <option>Sinacaban</option>
                                        <option>Tudela</option>
                                        <option>El Salvador</option>
                                        <option>Gingoog</option>
                                        <option>Alubijid</option>
                                        <option>Balingasag</option>
                                        <option>Baligoan</option>
                                        <option>Claveria</option>
                                        <option>Gitagum</option>
                                        <option>Initao</option>
                                        <option>Jasaan</option>
                                        <option>Kinoguitan</option>
                                        <option>Lagonglong</option>
                                        <option>Laguindingan</option>
                                        
                                        <option>Libertad</option>
                                        <option>Magsaysay(Linugos)</option>
                                        <option>Manticao</option>
                                        <option>Medina</option>
                                        <option>Naawan</option>
                                        <option>Opol</option>
                                        <option>Salay</option>
                                        <option>Sugbongcogon</option>
                                        <option>Tagoloan</option>
                                        <option>Talisayan</option>
                                        <option>Villanueva</option>
                                        <option>Cagayan de Oro</option>

                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group fg-line">
                                    <select class="selectpicker form-control" data-live-search="true" name='student_addressProvince' id='student_addressProvince' required>
                                        <option value="">Province</option>
                                        <option>Misamis Oriental</option>
                                        <option>Bukidnon</option>
                                        <option>Camiguin</option>
                                        <option>Lanao Del Norte</option>
                                       
                                        <option>Misamis Occidental</option>

                                    </select>
                                </div>
                            </div>
                            
                            <div class="col-sm-4">
                                <div class="form-group fg-line">
                                    <input type="text" name="parent_Ffname" id="parent_Ffname" class="form-control" placeholder="Father's first name">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group fg-line">
                                    <input type="text" name="parent_Fmname" id="parent_Fmname" class="form-control" placeholder="Father's middle name">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group fg-line">
                                    <input type="text" name="parent_Flname" id="parent_Flname" class="form-control" placeholder="Father's last name">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group fg-line">
                                    <input type="text" name="parent_Mfname" id="parent_Mfname" class="form-control" placeholder="Mother's first name">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group fg-line">
                                    <input type="text" name="parent_Mmname" id="parent_Mmname" class="form-control" placeholder="Mother's middle name">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group fg-line">
                                    <input type="text" name="parent_Mlname" id="parent_Mlname" class="form-control" placeholder="Mother's last name">
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group fg-line">
                                    <input type="text" name="parent_phone" id="parent_phone" class="form-control" placeholder="Parents contact #">
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group fg-line">
                                    <input type="text" name="guardian_name" id="guardian_name" class="form-control" placeholder="Name of guardian">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group fg-line">
                                    <select class="selectpicker" data-search="true" name='guardian_relationship' id='guardian_relationship'>
                                        <option value="">RELATIONSHIP</option>
                                        <option>Aunt</option>
                                        <option>Uncle</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group fg-line">
                                    <input type="text" name="guardian_Phone" id="guardian_Phone" class="form-control" placeholder="Guardian cel #">
                                    <input type="hidden" name="section_id" id="section_id" value="<?php 
                                        echo $id; 
                                    ?>">
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">
                            
                                <button type="submit" name="insert" id="insert" class="btn btn-success">CREATE STUDENT ACCOUNT</button>
                            </button>
                        </div>
                                    </div>
                                       
                                </div>           
                            </div>
                        </div>



                    </form>

                    </div>
                <div id="hehe">
                    
                </div>
            </div>
            
        </section>
        <!-- ADD LEARNER -->
        <div class="modal fade" id="modalColor" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    
                    
                </div>
            </div>
        </div>

        <div class="modal fade" id="modalLoad" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <form id="myForm" method="post">
                        <div class="modal-header">
                            <h2 class="modal-title"><b>LOAD CLASS LEARNER</b></h2>

                        </div>
                       
                        <div class="modal-body">
                            
                            <div class='col-sm-12'>
                                <div class='form-group fg-line'>
                                    <select class="selectpicker" data-search="true" name='section' id='section'>
                                        <option></option>
                                        <?php
                                            $sy = getLastSY($con);
                                            $sLevel = getSectionLevel($con, $id);
                                            $sLevel--;
                                            $query = mysqli_query($con, "SELECT * from section where sy_id='".$sy."' and year_lvl_id='".$sLevel."'");

                                            while($row = mysqli_fetch_array($query))
                                            {
                                                echo '
                                                    <option value="'.$row[0].'">'.$row[1].'</option>
                                                ';
                                            }
                                        ?>
                                    </select>
                                    <input type="hidden" name="section_id" id="section_id" value="<?php echo $id; ?>">
                                </div>
                            </div>
                            <hr> 
                        </div>
                        <div class="modal-footer">
                            
                            <button type="submit" name="insert1" id="insert1" class="btn btn-success btn-lg">LOAD CLASS</button>
                            <button type="button" class="btn btn-warning btn-lg" data-dismiss="modal">CANCEL
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modalRemoveLearner" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <form id="removeLearner" method="post">
                        <div class="modal-header">
                            <h4 class="modal-title"><b>REMOVE LEARNER</b></h4>
                        </div>


                        <div class="modal-body">
                            <div class='col-sm-12'>
                                <div class='form-group fg-line'>
                                    <input type="text" name="student1" id="student1" class="form-control" disabled>
                                    <input type="hidden" name="sid" id="sid" value="<?php echo $id ?>">
                                    <input type="hidden" name="important_id1" id="important_id1">
                                    <input type="hidden" name="student_id" id="student_id">
                                    <input type="hidden" name="level" id="level" value="<?php echo $level ?>">
                                </div>
                            </div>
                            
                        </div>
                        <div class="modal-footer">
                            
                            <button type="submit" name="remove" id="remove" class="btn btn-success btn-lg">YES</button>
                            
                            <button type="button" class="btn btn-warning btn-lg" data-dismiss="modal">NO
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- EDIT LEARNER -->
        <div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form id="editLearner" method="post">
                        <div class="modal-header">
                            <h4 class="modal-title"><b>EDIT LEARNER</b></h4>
                        </div>


                        <div class="modal-body">
                            <div class='col-sm-12'>
                                <div class='form-group fg-line'>
                                    <input type="text" name="student2" id="student2" class="form-control" disabled>
                                    <input type="hidden" name="sid2" id="sid2" value="<?php echo $id ?>">
                                    <input type="hidden" name="important_id2" id="important_id2">
                                    <input type="hidden" name="student_id2" id="student_id2">
                                    <input type="hidden" name="level2" id="level2" value="<?php echo $level ?>">
                                </div>
                            </div>
                            <div class='col-sm-12'>
                                <div class='form-group fg-line'>
                                    <select class="form-control selectpicker" name="remarks1" id="remarks1" >
                                        
                                        <option value="T/I">Transferred In</option>
                                        <option value="T/O">Transferred Out</option>
                                        <option value="LE">Late Enrolment</option>
                                        <option value="DRP">Drop Out</option>
                                        <option value="Pending TI">Pending TI</option>
                                    </select>
                                    
                                </div>
                            </div>
                            <div id='_schoolName' class='col-sm-12'>
                                <div class='form-group fg-line'>
                                    <input type="text" name="school" id="school" class="form-control" placeholder="Reason/School Name">
                                </div>
                            </div>
                            <div id='_reasonID' class='col-sm-12' hidden>
                                <div class='form-group fg-line'>
                                    <select class="selectpicker" name="reason" id="reason">
                                        <option></option>
                                        <optgroup label="Domestic-Related Factors">
                                            <option>Had to take care of siblings</option>
                                            <option>Early marriage/pregnancy</option>
                                            <option>Parent's attitude towards schooling</option>
                                            <option>Family problem</option>
                                        </optgroup>
                                        <optgroup label="Individual-Related Factors">
                                            <option>Illness</option>
                                            <option>Overage</option>
                                            <option>Death</option>
                                            <option>Drug Abuse</option>
                                            <option>Poor academic performance</option>
                                            <option>Lack of interest/Distractions</option>
                                            <option>Hunger/Malnutrition</option>
                                        </optgroup>
                                        <optgroup label="School-Related Factors">
                                            <option>Teacher Factor</option>
                                            <option>Physical condition of classroom</option>
                                            <option>Peer influence</option>
                                        </optgroup>
                                        <optgroup label="Geographic/Environmental">
                                            <option>Distance between home and school</option>
                                            <option>Armed conflict (incl. Tribal wars & clanfeuds)</option>
                                            <option>Calamities/Disasters</option>
                                        </optgroup>
                                        <optgroup label="Financial-Related">
                                            <option>Distance between home and school</option>
                                            <option>Armed conflict (incl. Tribal wars & clanfeuds)</option>
                                            <option>Calamities/Disasters</option>
                                        </optgroup>
                                        <optgroup label="Financial-Related">
                                            <option>Child laber</option>
                                        </optgroup>
                                        <option value="Others">Others</option>
                                    </select>
                                </div>
                            </div>
                            <div id='_others' class='col-sm-12' hidden>
                                <div class='form-group fg-line'>
                                    <input type="text" name="others" id="others" class="form-control" placeholder="Specify reason">
                                </div>
                            </div>
                            <div class='col-sm-4'>
                                <div class="form-group fg-line">
                                    <select class="form-control selectpicker" id="month" name="month">
                                        <option>Select a month</option>
                                    <?php 
                                        $query = mysqli_query($con, "SELECT * from month where sy_id='".getCurrentSY($con)."'");
                                        while ($row = mysqli_fetch_array($query)) {
                                            if($row[3] == 'Closed')
                                            {
                                                echo "<option disabled>".$row[1]."</option>";
                                            }
                                            else
                                            {
                                                echo "<option>".$row[1]."</option>";
                                            }
                                        }

                                    ?>
                                    </select>
                                    <input type='text' class="form-control date-picker" placeholder="Date of remarks" name="tiDate" id="tiDate">
                                </div>
                            </div>
                            <div class='col-sm-4'>
                                <div class="form-group fg-line">
                                    <input type="number" name="day" class="form-control" id="day" placeholder="Day">
                                </div>
                            </div>
                            <div class='col-sm-4'>
                                <div class="form-group fg-line">
                                    <input type="number" name="year" id="year" class="form-control" disabled>
                                    
                                </div>
                            </div>
                            
                        </div>
                        <div class="modal-footer">
                            
                            <button type="submit" name="edit" id="edit" class="btn btn-success btn-lg">SAVE CHANGES</button>
                            
                            <button type="button" class="btn btn-warning btn-lg" data-dismiss="modal">CANCEL
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modalEnrollLearner" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <form id="enroll" method="post">
                        <div class="modal-header">
                            <h4 class="modal-title"><b>ENROLL LEARNER</b></h4>
                        </div>


                        <div class="modal-body">
                            <div class='col-sm-12'>
                                <div class='form-group fg-line'>
                                    <input type="text" name="NameofStudent" id="NameofStudent" class="form-control" disabled>
                                    <input type="hidden" name="sectionIDS" id="sectionIDS">
                                    <input type="hidden" name="studentID" id="studentID">
                                    
                                </div>
                            </div>
                            
                        </div>
                        <div class="modal-footer">
                            
                            <button type="submit" name="remove" id="remove" class="btn btn-success btn-lg">YES</button>
                            
                            <button type="button" class="btn btn-warning btn-lg" data-dismiss="modal">NO
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div id="loading" class="modal fade" data-backdrop="static" data-keyboard="false">  
            <div class="modal-dialog modal-sm">  
               <div class="modal-content">
               <center>   
                    <div class="modal-body" id="Details">
                         
                        <div class="preloader pl-xxl">
                            
                                <svg class="pl-circular" viewBox="25 25 50 50">
                                    <circle class="plc-path" cx="50" cy="50" r="20"/>
                                </svg>
                            
                        </div> 
                       <center><h4>Processing...Please wait</h4></center>
                    </div>  
                </center>
               </div>  
            </div>  
        </div>
        <!-- FOOTER -->
        <?php include_once 'footer.php' ?>
        <!-- FOOTER -->

        <!-- Javascript Libraries -->
        <?php include_once 'scripts.php'; ?>
        <script type="text/javascript" src="js/Notification.js"></script>
        <!-- Javascript Libraries -->
        <script type="text/javascript">
            $(document).ready(function() {

                $('#searchenrolled').keyup(function(){
                    var txt = $('#searchenrolled').val();
                    var id = <?php echo $id; ?>;
                   
                    
                    $.ajax({
                        url:"ajaxJQuery/searchLearnerofSection.php",
                        method:"POST",
                        data:
                        {
                            search:txt,
                            section:id
                        },
                        dataType:"text",
                        success:function(data)
                        {
                            if(data != '')
                            {
                                $('#hehe').html(data);
                            }
                            else
                            {
                                $('#hehe').html(
                                    '<div class="card-body card-padding" id="hehe">'
                                        +'Your search - <b>'+txt+'</b> - did not match any learner.'
                                        +'<br />'
                                        +'Suggestions:<br />'
                                        +'<ul style="margin-left:1.3em;margin-bottom:2em"><li>Make sure that all words are spelled correctly.</li><li>Try different keywords.</li><li>Try more general keywords.</li></ul>'
                                    +'</div>'
                                );
                            }
                        }
                    });
                    
                    
                });


                

                $('#remarks1').on('change', function(){
                    var select = $(this).val();
                    if(select == "DRP")
                    {
                        $('#_reasonID').show();
                         $('#_schoolName').hide();
                    }
                    else if(select == "T/I" | select == "T/O" | select == "LE")
                    {
                        $('#_reasonID').hide();
                        $('#_schoolName').show();
                        $('#_others').hide();

                    }
                    else
                    {
                         $('#_reasonID').hide();
                        $('#_schoolName').hide();
                        $('#_others').hide();
                    }
                    
                });

                $('#reason').on('change', function(){
                    var select1 = $(this).val();
                    if(select1 == "Others")
                    {
                        $('#_others').show();
                    }
                    else
                    {
                        $('#_others').hide();
                    }
                    
                });

                $(document).on('click', '.enroll_learnertoSection', function(){
                    var lrn = $(this).attr("id");
                    var sectionID = $(this).attr("value");

                    $.ajax({
                        url:"ajaxJQuery/selectStudentInfo.php",
                        method:"POST",
                        data:{lrn:lrn,sectionID:sectionID},
                        dataType:"json",
                        success:function(data){
                            $('#NameofStudent').val(data.Name);
                            $('#studentID').val(data.student_id);
                            $('#sectionIDS').val(sectionID);
                            $('#modalEnrollLearner').modal('show');
                        }

                    });
                });

                $('#enroll').on("submit", function(event){
                    event.preventDefault();
                    var txt = $('#search').val();
                    var id = $('#secID').val();
                        $.ajax({  
                              url:"ajaxJQuery/enrollLearner.php",  
                              method:"POST",  
                              data:$('#enroll').serialize(),  
                              success:function(data){
                                   if(data != '0')
                                   { 
                                      
                                       $('#hehe').html(data);
                                       $('#modalEnrollLearner').modal('hide');
                                       $('#enroll')[0].reset();

                                       $.ajax({
                                            url:"ajaxJQuery/searchStudent.php",
                                            method:"POST",
                                            data:
                                            {
                                                search:txt,
                                                section:id
                                            },
                                            dataType:"text",
                                            success:function(data)
                                            {
                                                $('#resultsaSearch').html(data);
                                            }
                                        });
                                       //location.reload();
                                   }
                                   else
                                   {    
                                        
                                        var message = "The learner has been used for other table";
                                        executeNotif(message,"error");  
                                       $('#enroll')[0].reset();
                                       $('#modalRemoveLearner').modal('hide');
                                   }

                                   
                              }  

                         });  
                    
                });

                //SELECT LEARNER TO DELETE
                $(document).on('click', '.delete_learner', function(){
                    var important_id1 = $(this).attr("id");
                    $.ajax({
                        url:"ajaxJQuery/selectControl1.php",
                        method:"POST",
                        data:{important_id1:important_id1},
                        dataType:"json",
                        success:function(data){
                            $('#student1').val(data.Student);
                            $('#student_id').val(data.student_id);
                            $('#important_id1').val(important_id1);
                            $('#modalRemoveLearner').modal('show');
                        }

                    });
                });

                 //SELECT LEARNER TO EDIT
                $(document).on('click', '.edit_learner', function(){
                    var important_id1 = $(this).attr("id");
                    $.ajax({
                        url:"ajaxJQuery/selectControl1.php",
                        method:"POST",
                        data:{important_id1:important_id1},
                        dataType:"json",
                        success:function(data){
                            $('#student2').val(data.Student);
                            $('#remarks1').val(data.control_remarks);
                            $('#student_id2').val(data.student_id);
                            $('#important_id2').val(important_id1);
                            
                            $('#modalEdit').modal('show');
                        }

                    });
                });

                //UN_ENROLL LEARNER
                $('#removeLearner').on("submit", function(event){
                    event.preventDefault();
                    if($('#student1').val() == 'Choose')
                    {
                       
                    }
                    else 
                    {
                        $.ajax({  
                              url:"ajaxJQuery/removeLearner.php",  
                              method:"POST",  
                              data:$('#removeLearner').serialize(),  
                              success:function(data){
                                   if(data != '1')
                                   { 
                                      
                                       $('#removeLearner')[0].reset();
                                       $('#modalRemoveLearner').modal('hide');
                                       $('#hehe').html(data);
                                   }
                                   else
                                   {    
                                        
                                        var message = "The learner has been used for other table";
                                        executeNotif(message,"error");   
                                       $('#removeLearner')[0].reset();
                                       $('#modalRemoveLearner').modal('hide');
                                   }

                                 
                              }  

                         });  
                    }
                });

                //LEARNER EDIT
                $('#editLearner').on("submit", function(event){
                    event.preventDefault();
                    if($('#remarks1').val() == '')
                    {
                        $('#remarks1').focus();
                    }
                    else 
                    {
                        $.ajax({  
                              url:"ajaxJQuery/updateRemarks.php",  
                              method:"POST",  
                              data:$('#editLearner').serialize(),  
                              success:function(data){
                                if(data != '1')
                                { 
                                   //swal("Good job", "You successfully updated learner", "success");  
                                   $('#editLearner')[0].reset();   
                                   $('#modalEdit').modal('hide');
                                   $('#hehe').html(data);
                                }
                                else
                                {
                                   
                                    var message = "Error in editing remarks of a learner";
                                    executeNotif(message,"error");  
                                   $('#editLearner')[0].reset();   
                                   $('#modalEdit').modal('hide');
                                }
                               
                              }  

                         });  
                    }
                });

                //PAG SEARCH
                //PAG SEARCH
                $('#search').keyup(function(objEvent){
                    var txt = $('#search').val();
                    var id = $('#secID').val();
                    (objEvent) ? keycode = objEvent.keyCode : keycode = event.keyCode;
                    console.log(keycode);
                    if(keycode == 13){
                        
                        
                        $.ajax({
                            url:"ajaxJQuery/searchStudent.php",
                            method:"POST",
                            data:
                            {
                                search:txt,
                                section:id
                            },
                            dataType:"text",
                            success:function(data)
                            {
                                if(data != '')
                                {
                                    $('#resultsaSearch').html(data);
                                }
                                else
                                {
                                    $('#resultsaSearch').html(
                                    ''
                                        +'Your search - <b>'+txt+'</b> - did not match any learner.'
                                        +'<br />'
                                        +'Suggestions:<br />'
                                        +'<ul style="margin-left:1.3em;margin-bottom:2em"><li>Make sure that all words are spelled correctly.</li><li>Try different keywords.</li><li>Try more general keywords.</li></ul>'
                                    +''
                                );
                                }
                            }
                        });
                       
                    }
                    
                });

                $('#student_LRN').keyup(function(){
                    var lrn = $('#student_LRN').val();
                    
                        if(lrn != ''){
                            $.ajax({
                                url:"ajaxJQuery/checkLRN.php",
                                method:"POST",
                                data:{search:lrn},
                                dataType:"text",
                                success:function(data)
                                {
                                    if(data == 'naa')
                                    {

                                        $('#insert').hide();
                                        $('#lrnForm').attr('class','col-sm-3 has-warning');                                        
                                        $('#helpMessage').attr('class','help-block');
                                        $('#student_LRN').focus();
                                    }
                                    else
                                    {
                                        $('#helpMessage').attr('class','help-block hidden');
                                        $('#insert').show();
                                       $('#lrnForm').attr('class','col-sm-3');
                                    }
                                }
                            });
                        }
                    
                });

                $('#myForm').on("submit", function(event){
                    event.preventDefault();
                    var e = document.getElementById("section");
                    var sec = e.options[e.selectedIndex].text;
                    if($('#section').val() == '')
                    {
                        
                         alert("Select section");
                    }
                    else  
                    {  
                         $.ajax({  
                              url:"ajaxJQuery/insertLoadStudent.php",  
                              method:"POST",  
                              data:$('#myForm').serialize(),  
                              beforeSend:function(){

                                    $('#modalLoad').modal('hide');
                                   $('#loading').modal('show');  
                              },  
                              success:function(data){
                                if(data == '0' | data == 'naa')
                                {
                                    if(data == '0')
                                    {
                                        $('#loading').modal('hide');  
                                        
                                        var message = "Error in loading class from "+sec+"";
                                        executeNotif(message, "error");
                                    }
                                    else if(data == 'naa')
                                    {
                                        $('#loading').modal('hide');  
                                        
                                        var message = "Class <b>"+sec+"</b> might have been already loaded. Please double check be aware on Loading Classes";
                                        executeNotif(message, "warning");
                                        $('#hehe').html(data);
                                    }
                                    
                                }
                                else
                                {
                                    
                                    $('#loading').modal('hide');  
                                    var message = "Class <b>"+sec+"</b> has been loaded completely.";
                                    executeNotif(message, "success");
                                   
                                   $('#hehe').html(data);
                                   $('#modalLoad').modal('hide');
                                   $('#myForm')[0].reset();   
                                }
                                
                                
                              }  

                         });  
                    }
                });


                 //INSERT LEARNER
                $('#insert_form').on("submit", function(event){
                    event.preventDefault();
                    $('#student_LRN').focus();
                    if($('#student_LRN').val() == '')
                    {
                        $('#student_LRN').focus();    
                         alert("LRN is required and valid");
                    }
                    else if($('#student_fname').val() == '')  
                    {
                            $('#student_fname').focus();    
                         alert("First name is required and valid");
                         
                    }  
                    else if($('#student_mname').val() == '')  
                    {  
                        $('#student_mname').focus();  
                         alert("Middle name is required and valid");  
                    }  
                    else if($('#student_lname').val() == '')  
                    {
                        $('#student_lname').focus();
                         alert("Maximun grade is required");  
                    }
                    else if($('#student_sex').val() == 'GENDER')
                    {
                        $('#student_sex').focus();
                        alert("You must select gender!");
                    }
                    else if($('#student_birthDate').val() == '')
                    {
                        $('#student_birthDate').focus();
                        alert("You must enter birth date!");
                    }
                    else if($('#student_age').val() == '')
                    {
                        $('#student_age').focus();
                        alert("You must enter age!");
                    }
                    else if($('#student_motherTongue').val() == '')
                    {
                        $('#student_motherTongue').focus();
                        alert("You must select language!");
                    }
                    else if($('#student_IP').val() == '')
                    {
                        $('#student_IP').focus();
                        alert("You must select ethnic type!");
                    }
                    else if($('#student_religion').val() == 'RELIGION')
                    {
                        $('#student_religion').focus();
                        alert("You must select religion!");
                    }
                    
                    else if($('#student_addressBarangay').val() == '')
                    {
                        $('#student_addressBarangay').focus();
                        alert("You must enter Barangay");
                    }
                    else if($('#student_addressMunicipality').val() == 'MUNICIPALITY')
                    {
                        $('#student_addressMunicipality').focus();
                        alert("You must enter municipality");
                    }
                    else if($('#student_addressProvince').val() == 'PROVINCE')
                    {
                        $('#student_addressProvince').focus();
                        alert("You must enter province");
                    }
                    else if($('#parent_Mfname').val() == '')
                    {
                        $('#parent_Mfname').focus();
                        alert("You must enter mother's first name");
                    }
                    else if($('#parent_Mmname').val() == '')
                    {
                        $('#parent_Mmname').focus();
                        alert("You must enter mother's middle name");
                    }
                    else if($('#parent_Mlname').val() == '')
                    {
                        $('#parent_Mlname').focus();
                        alert("You must enter mother's last name");
                    }
                    else  
                    {  
                         $.ajax({  
                              url:"ajaxJQuery/insertStudent.php",  
                              method:"POST",  
                              data:$('#insert_form').serialize(),  
                              beforeSend:function(){  
                                   $('#insert1').val("Inserting..");   
                              },  
                              success:function(data){ 
                                if(data == '1' | data == '0'  | data == '2' )
                                {   
                                    var message= "Error on adding learner account";
                                    executeNotif(message, "error");
                                }
                                else
                                {
                                    var message= "Successfulyy added learner account";
                                    executeNotif(message, "success");
                                    $('#modalColor').modal('hide');
                                    $('#insert_form')[0].reset();
                                    $('#hehe').html(data);
                                }
                                   
                              }  

                         });  
                    }
                });





            } );
        </script>
    </body>

</html>