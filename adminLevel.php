<?php
    include_once 'sessionAdmin.php';
    include_once 'dbconnect.php';

?>
<!DOCTYPE html>
    <!-- HEAD -->
    <?php include_once 'head.php'; ?>
    <!-- HEAD   -->
    <body>
        <!-- HEADER -->
        <?php include_once 'header.php'; ?>
        <!-- HEADER -->

        <section id="main">
            <ol class="breadcrumb">
                <li><a href="adminHome.php">Home</a></li>
                <li class="active">Levels</li>
            </ol>
            <?php
                $toggle = 'adminLevel';
                include_once 'sidebar.php';
            ?>
            <section id="content">
                <div class="container">
                    <!-- Colored Headers -->
                    <div class="block-header">
                        <h1><i class="zmdi zmdi-nature-people"></i> Level
                        </h1>
                        
                        
                        <div class="modal fade" id="modalColor" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
                            <div class="modal-dialog modal-sm">
                                <div class="modal-content">
                                    <form id="myForm" method="post" action="insertLevel.php" >
                                        <div class="modal-header">
                                            <h2 class="modal-title">NEW LEVEL</h2>

                                        </div>

                                        <div class="modal-body">

                                            <div class='col-sm-12'>
                                                <div class='form-group fg-float'>
                                                    <div class="fg-line">
                                                        <input type='text' name='year_lvl_title' id='year_lvl_title' class='form-control fg-input input-lg'>
                                                        <label class="fg-label">Level ex. Grade 7</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                        </div>
                                        <div class="modal-footer">

                                            <button type="submit" name="insert1" id="insert1" class="btn btn-success">CREATE LEVEL</button>
                                            <button type="button" class="btn btn-warning" data-dismiss="modal">CANCEL
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="modal fade" id="updateLevel" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
                            <div class="modal-dialog modal-sm">
                                <div class="modal-content">
                                    <form id="myForm1" method="post">
                                        <div class="modal-header">
                                            <h2 class="modal-title">UPDATE LEVEL</h2>
                                        </div>

                                        <div class="modal-body">

                                            <div class='col-sm-12'>
                                                <div class='form-group fg-float'>
                                                    <div class="fg-line">
                                                        <input type='text' name='year_lvl_title1' id='year_lvl_title1' class='form-control fg-input input-lg'>
                                                        <label class="fg-label">Level ex. Grade 7</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                        </div>
                                        <div class="modal-footer">
                                            <input type="hidden" name="year_lvl_id" id="year_lvl_id">
                                            <button type="submit" name="insert1" id="insert1" class="btn btn-success">UPDATE LEVEL</button>
                                            <button type="button" class="btn btn-warning" data-dismiss="modal">CANCEL
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div id="deleteLevel" class="modal fade" data-backdrop="static" data-keyboard="false">
                            <div class="modal-dialog">
                               <div class="modal-content">
                                    <div class="modal-header bgm-lightgreen">
                                         <button type="button" class="close" data-dismiss="modal">&times;</button>
                                         <h3 class="modal-title">Level Details</h3>
                                    </div>
                                    <div class="modal-body" id="Details">
                                    </div>
                                    <div class="modal-footer">
                                         <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
                                    </div>
                               </div>
                            </div>
                        </div>
                    </div>
                    <div id="hehe">
                        <?php include_once 'ajaxJQuery/displayLevel.php'; ?>
                    </div>


                    <br/>
                    <br/>
                </div>
                
            </section>
        </section>

        <!-- FOOTER -->
        <?php include_once 'footer.php' ?>
        <!-- FOOTER -->

        <!-- Javascript Libraries -->
        <?php include_once 'scripts.php'; ?>
        <!-- Javascript Libraries -->
        <script type="text/javascript" src="js/Notification.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){

                $('#modalColor').on('shown.bs.modal', function() {
                  $('#year_lvl_title').focus();
                })
                $('#updateLevel').on('shown.bs.modal', function() {
                  $('#year_lvl_title1').focus();

                })


                $('[data-dismiss=modal]').on('click', function (e) {
                    $('#myForm')[0].reset();
                    $('#myForm1')[0].reset();
                })

                $(document).on('click', '.details', function(){
                    var year_lvl_id = $(this).attr("id");
                    $.ajax({
                        url:"ajaxJQuery/viewLevel.php",
                        method:"POST",
                        data:{year_lvl_id:year_lvl_id},
                        success:function(data){
                            $('#Details').html(data);
                            $('#dataModal').modal('show');
                        }

                    });
                });
                $(document).on('click', '.delete_level', function(){
                    var year_lvl_id = $(this).attr("id");
                    swal({
                        title: "Are you sure?",
                        text: "You will not be able to recover this data!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonText: "Yes, delete it!",
                    }).then(function(){

                        $.ajax({
                            url:"ajaxJQuery/deleteLevel.php",
                            method:"POST",
                            data:{year_lvl_id:year_lvl_id},
                            success:function(data){
                                if(data != '0'){
                                    $('#hehe').html(data);
                                    var message = "Successfully deleted Level!.";
                                    executeNotif(message,"success");

                                }else{

                                    var message = "Unable to delete Level!. Level may already used.";
                                    executeNotif(message,"danger");
                                       //location.reload(); // then reload the page.(3)
                                }

                            }

                        });
                    });

                });
                $(document).on('click', '.edit_level', function(){
                    var year_lvl_id = $(this).attr("id");
                    $.ajax({
                        url:"ajaxJQuery/selectLevel.php",
                        method:"POST",
                        data:{year_lvl_id:year_lvl_id},
                        dataType:"json",
                        success:function(data){
                            $('#year_lvl_title1').val(data.year_lvl_title);
                            $('#year_lvl_id').val(data.year_lvl_id);
                            $('#updateLevel').modal('show');
                        }

                    });
                });


                $('#myForm1').on("submit", function(event){
                    event.preventDefault();
                    if($('#year_lvl_title1').val() == '')
                    {
                        $('#year_lvl_title1').focus();
                         alert("Title is required");
                    }

                    else
                    {
                         $.ajax({
                              url:"ajaxJQuery/updateLevel.php",
                              method:"POST",
                              data:$("#myForm1").serialize(),
                              beforeSend:function(){
                                   $('#insert1').val("Inserting..");
                              },
                              success:function(data){


                                   if(data == '0'){

                                        $('#updateLevel').modal('hide');
                                        var message = "Unable to update "+$('#year_lvl_title1').val()+" Level!!";
                                        executeNotif(message,"danger");
                                        $('#myForm1')[0].reset();

                                    }else{

                                        $('#updateLevel').modal('hide');
                                        $('#hehe').html(data);
                                        var message = "Successfully updated "+$('#year_lvl_title1').val()+" Level!!";
                                        executeNotif(message,"success");
                                        $('#myForm1')[0].reset();
                                    }
                              }

                         });
                    }
                });

                $('#myForm').on("submit", function(event){
                    event.preventDefault();
                    if($('#year_lvl_title').val() == '')
                    {
                         alert("Title is required");
                    }

                    else
                    {
                         $.ajax({
                              url:"ajaxJQuery/insertLevel.php",
                              method:"POST",
                              data:$('#myForm').serialize(),
                              beforeSend:function(){
                                   $('#insert1').val("Inserting..");
                              },
                              success:function(data){
                                   if(data == '0'){

                                    $('#modalColor').modal('hide');
                                    var message = "Error on adding "+$('#year_lvl_title').val()+" Level!!. Level may already exist";

                                    executeNotif(message,"danger");
                                    $('#myForm')[0].reset();

                                    }else{

                                         $('#modalColor').modal('hide');
                                        $('#hehe').html(data);
                                        var message = "Successfully added "+$('#year_lvl_title').val()+" Level!!";
                                        executeNotif(message,"success");
                                        $('#myForm')[0].reset();
                                    }
                              }

                         });
                    }
                });


            });

        </script>

    </body>

</html>
