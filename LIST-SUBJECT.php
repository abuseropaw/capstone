<?php
	session_start();
 require('dbconnect.php');

 require('Report/fpdf.php');

  $level=$_GET['level'];
  if($level == 'Grade7'){
  	$level = 'Grade 7';

  }else if($level == 'Grade8'){
  	$level = 'Grade 8';
  }
  else if($level == 'Grade9'){
  	$level = 'Grade 9';
  }
  else if($level == 'Grade10'){
  	$level = 'Grade 10';
  }else{
  	$level = 'ALL SUBJECT';
  }


 class PDF extends FPDF
 {
 	function Footer()
 	{
 		// Go to 1.5 cm from bottom
 		
 		$this->SetY(-50);
 		// Select Arial italic 8
 		$this->SetFont('Arial','I',8);
 		// Print centered page number
 		$this->Cell(0,25,'Page '.$this->PageNo(),1,0,'C');
 	}
 }
 $selectStudent = mysqli_query($con,"Select * from faculty_account");
 $pdf = new FPDF('P','in',array(8.27,11.69));
 //var_dump(get_class_methods($fpdf));
 
 $pdf->AddPage();
 
	
	$pdf->SetFont("Arial","B","15");
	$pdf->Ln(.5);
	$pdf->SetLeftMargin(.2);
	$pdf->Cell(0,0.20,"SUBJECT LIST",0,1,"C"); 
	$pdf->Ln(.0001);
	$pdf->SetFont("Arial","B","13");
	$pdf->Cell(0,0.15,"Lugait National High School",0,1,"C");
	
	
	$pdf->Ln(.0001);
	$pdf->SetFont("Arial","B","13");
	$pdf->Cell(0,0.15,"January 12, 2017",0,1,"C");
	$pdf->Ln(.0001);
	$pdf->SetFont("Arial","B","13");
	$pdf->Cell(0,0.15,$level." Subjects",0,1,"C");


	$pdf->Image('Report/kne.png',1,.6,1.1,1.1);
	$pdf->Image('Report/deped.png',5.8,.7,2,.8);
	
	
$pdf->Ln(.3);
	
	
	
	
	//New Line
	$pdf->Ln(.1);
	$pdf->SetFont("Arial","B","10");
 	$pdf->Cell(.7,.45,"",0,0,"C");
 	$pdf->Cell(1.7,.45,"TITLE",1,0,"C");
 	$pdf->Cell(3.9,.45,"DESCRIPTION",1,0,"C");
 	$pdf->Cell(.6,.45,"UNIT",1,0,"C");
 	$pdf->Cell(.6,.45,"H/W",1,0,"C");
 	
 	if($level != 'ALL SUBJECT'){
	 	$query = mysqli_query($con, "SELECT * from subjectDetails where Level='".$level."'");
	 	$getTotals = mysqli_query($con, "select sum(subj_unit) as TotalUnit, sum(subj_hrs_per_week) as TotalHrsPerWeek from subjectDetails where Level='".$level."'");
	 	$temp =mysqli_fetch_row($getTotals);
	 	$totUnit = $temp[0];
	 	$totHrsWeek = $temp[1];
	 	$countSubjects = mysqli_num_rows(mysqli_query($con, "SELECT * from subjectDetails where Level='".$level."'"));

	 	while($row = mysqli_fetch_array($query)){
	 		$pdf->Ln();
	 		$pdf->SetFont("Arial","","9");
		 	$pdf->Cell(.7,.30,"",0,0,"C");
		 	$pdf->Cell(1.7,.30,$row[1],1,0,"C");
		 	$pdf->Cell(3.9,.30,$row[2],1,0,"C");
		 	$pdf->Cell(.6,.30,$row[3],1,0,"C");
		 	$pdf->Cell(.6,.30,$row[4],1,0,"C");
	 	}
	 		$pdf->Ln(.5);
	 		$pdf->SetFont("Arial","B","9");
		 	$pdf->Cell(.7,.30,"",0,0,"C");
		 	$pdf->Cell(1.7,.30,"",0,0,"L");
		 	$pdf->Cell(3.9,.30,"TOTAL SUBJECTS:",0,0,"R");
		 	$pdf->Cell(1.2,.30,$countSubjects,0,0,"L");
		 	$pdf->Cell(0,.30,"",0,0,"C");

		 	$pdf->Ln(.2);
	 		$pdf->Cell(.7,.30,"",0,0,"C");
		 	$pdf->Cell(1.7,.30,"",0,0,"L");
		 	$pdf->Cell(3.9,.30,"TOTAL UNITS:",0,0,"R");
		 	$pdf->Cell(1.2,.30,$totUnit,0,0,"L");
		 	$pdf->Cell(0,.30,"",0,0,"C");

		 	$pdf->Ln(.2);
	 		$pdf->Cell(.7,.30,"",0,0,"C");
		 	$pdf->Cell(1.7,.30,"",0,0,"L");
		 	$pdf->Cell(3.9,.30,"TOTAL HRS/WEEK:",0,0,"R");
		 	$pdf->Cell(1.2,.30,$totHrsWeek,0,0,"L");
		 	$pdf->Cell(0,.30,"",0,0,"C");
	}else{
		$query = mysqli_query($con, "SELECT * from subjectDetails");
	 	$getTotals = mysqli_query($con, "select sum(subj_unit) as TotalUnit, sum(subj_hrs_per_week) as TotalHrsPerWeek from subjectDetails");
	 	$temp =mysqli_fetch_row($getTotals);
	 	$totUnit = $temp[0];
	 	$totHrsWeek = $temp[1];
	 	$countSubjects = mysqli_num_rows(mysqli_query($con, "SELECT * from subjectDetails"));
	 	while($row = mysqli_fetch_array($query)){
	 		$pdf->Ln();
	 		$pdf->SetFont("Arial","","9");
		 	$pdf->Cell(.7,.30,"",0,0,"C");
		 	$pdf->Cell(1.7,.30,$row[1],1,0,"C");
		 	$pdf->Cell(3.9,.30,$row[2],1,0,"C");
		 	$pdf->Cell(.6,.30,$row[3],1,0,"C");
		 	$pdf->Cell(.6,.30,$row[4],1,0,"C");
	 	}
	 		$pdf->Ln(.5);
	 		$pdf->SetFont("Arial","B","9");
		 	$pdf->Cell(.7,.30,"",0,0,"C");
		 	$pdf->Cell(1.7,.30,"",0,0,"L");
		 	$pdf->Cell(3.9,.30,"TOTAL SUBJECTS:",0,0,"R");
		 	$pdf->Cell(1.2,.30,$countSubjects,0,0,"L");
		 	$pdf->Cell(0,.30,"",0,0,"C");

		 	$pdf->Ln(.2);
	 		$pdf->Cell(.7,.30,"",0,0,"C");
		 	$pdf->Cell(1.7,.30,"",0,0,"L");
		 	$pdf->Cell(3.9,.30,"TOTAL UNITS:",0,0,"R");
		 	$pdf->Cell(1.2,.30,$totUnit,0,0,"L");
		 	$pdf->Cell(0,.30,"",0,0,"C");

		 	$pdf->Ln(.2);
	 		$pdf->Cell(.7,.30,"",0,0,"C");
		 	$pdf->Cell(1.7,.30,"",0,0,"L");
		 	$pdf->Cell(3.9,.30,"TOTAL HRS/WEEK:",0,0,"R");
		 	$pdf->Cell(1.2,.30,$totHrsWeek,0,0,"L");
		 	$pdf->Cell(0,.30,"",0,0,"C");
	}
	
 $pdf->Output();
?>