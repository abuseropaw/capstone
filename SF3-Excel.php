<?php

// Include PHPExcel library and create its object
session_start();

    include_once 'ajaxJQuery/globalFunction.php';
    include_once 'dbconnect.php';
    
    $id = mysqli_real_escape_string($con, $_GET['id']);
    $getAdviserID = mysqli_fetch_row(mysqli_query($con, "SELECT faculty_id from adviser where section_id='".$id."'"));
    $AdviserID = $getAdviserID[0];

        $getLearners = mysqli_query($con, "SELECT * from controlDetails where section_id='".$id."'");

        $getAdviser = mysqli_fetch_row(mysqli_query($con, "SELECT UPPER(Adviser) as Adviser,GradeLevel from classinformation where section_id='".$id."'"));
        $adviser = $getAdviser[0];
        $level = $getAdviser[1];
        $countBook = mysqli_num_rows(mysqli_query($con, "SELECT * from bookDetails where Level='".$level."'"));
        $getSY = mysqli_fetch_row(mysqli_query($con, "SELECT SchoolYear,SectionName from controlDetails where section_id='".$id."'"));
        $SY = $getSY[0];
        $sectionName = $getSY[1];

        $countMale = mysqli_num_rows(mysqli_query($con, "SELECT * from controlDetails where section_id='".$id."' and Gender='Male'"));
        $countFemale = mysqli_num_rows(mysqli_query($con, "SELECT * from controlDetails where section_id='".$id."' and Gender='Female'"));
        $total = mysqli_num_rows(mysqli_query($con, "SELECT * from controlDetails where section_id='".$id."'"));

        
      

            require_once "PHPExcel-1.8.1/Classes/PHPExcel.php";

            //Get all book
            $getAllBooks = mysqli_query($con, "SELECT * from bookDetails where Level='".$level."'");
            $iteratorB1 = 0;
            $iteratorB2 = 0;
            //Array for books less or equal 8
            $arrayBook1 = array();
            $arrayBookID1 = array();

            //Array of book more than 8
            $arrayBook2 = array();
            $arrayBookID2 = array();

            while ($row = mysqli_fetch_array($getAllBooks)) {
                if($iteratorB1 < 8)
                {
                    $arrayBook1[$iteratorB1] = $row[1];
                    $arrayBookID1[$iteratorB1] = $row[0];
                    $iteratorB1++;
                }
                else
                {
                    $arrayBook2[$iteratorB2] = $row[1];
                    $arrayBookID2[$iteratorB2] = $row[0];
                    $iteratorB2++;
                }
                
            }

            //SETTING up what template to be use
            $countNumberofBook = mysqli_num_rows($getAllBooks);
            $totalSheet = 0;
            if($countNumberofBook <= 8)
            {
                $phpExcel = PHPExcel_IOFactory::load('School Forms Final/School Form 3.xlsx');
                setProperties($phpExcel,$adviser);
                $totalSheet = 1;
            }
            else
            {
                $phpExcel = PHPExcel_IOFactory::load('School Forms Final/School Form 3- 2.xlsx');
                setProperties($phpExcel,$adviser);
                $totalSheet = 2;
            }

            //SETTING up properties for excel file
            function setProperties($phpExcel,$adviser)
            {
                $phpExcel->getProperties()->setCreator($adviser)
                     ->setLastModifiedBy($adviser)
                     ->setTitle("School Form 3")
                     ->setSubject("Books")
                     ->setDescription("Books Issued and Returned")
                     ->setKeywords("SF3")
                     ->setCategory("School Forms");
            }
            


            for ($sheetIndex=0; $sheetIndex <$totalSheet ; $sheetIndex++) {
                $column = array("D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S");
                $colIter = 0;
                $booksIssuedM = array();
                $booksReturnedM = array();
                $booksIssuedF = array();
                $booksReturnedF = array(); 
                if($sheetIndex == 0)
                {

                    $sheet = $phpExcel ->setActiveSheetIndex(0);
                    $start = 12;
                    $index1= 14;
                    $tots = $total + 1;
                    $totalDeductedbyOne = $tots -3;
                    $totalDeductedbyOne1 = $countFemale -2;
                    $index2= $index1 + $totalDeductedbyOne +3;
                    $sheet ->insertNewRowBefore($index1,$totalDeductedbyOne);
                    $totalfoCell = $index1 + $totalDeductedbyOne + 5;
                    $totalFemale =  $index1 + $totalDeductedbyOne + 1;
                    $totalLearner =  $index1 + $totalDeductedbyOne + 2; 
                    for($i = 13; $i<=($totalDeductedbyOne+13); $i++)
                    {
                        $sheet ->mergeCells('B'.$i.':C'.$i.'');
                    }

                    for ($indexofBook=0; $indexofBook < count($arrayBook1); $indexofBook++) { 

                        $sheet ->setCellValue(''.$column[$colIter].'9',$arrayBook1[$indexofBook] );

                        $booksIssuedM[$indexofBook] = mysqli_num_rows(mysqli_query($con, "SELECT * from booksacquireddetails where section_id='".$id."' and book_id='".$arrayBookID1[$indexofBook]."' and Gender='Male'"));
                        $booksReturnedM[$indexofBook] = mysqli_num_rows(mysqli_query($con, "SELECT * from booksacquireddetails where section_id='".$id."' and book_id='".$arrayBookID1[$indexofBook]."' and Gender='Male' and booksAcquired_remarks=''"));
                        $booksIssuedF[$indexofBook] = mysqli_num_rows(mysqli_query($con, "SELECT * from booksacquireddetails where section_id='".$id."' and book_id='".$arrayBookID1[$indexofBook]."' and Gender='Female'"));
                        $booksReturnedF[$indexofBook] = mysqli_num_rows(mysqli_query($con, "SELECT * from booksacquireddetails where section_id='".$id."' and book_id='".$arrayBookID1[$indexofBook]."' and Gender='Female' and booksAcquired_remarks=''"));
                        

                        $colIter= $colIter + 2;
                    }
                    $sheet ->setCellValue('K5', $SY);
                    $sheet ->setCellValue('K7', $level);
                    $sheet ->setCellValue('N7', $sectionName);
                    execute($con,$sheet,$total,$countMale,$countFemale,$countBook,$id,$booksIssuedM,$booksReturnedM,$booksIssuedF,$booksReturnedF,$arrayBook1,$arrayBookID1,$column,$totalFemale,$totalLearner,$totalfoCell,$adviser);
                }
                else
                {
                    $sheet = $phpExcel ->setActiveSheetIndex(1);
                    $start = 12;
                    $index1= 14;
                    $tots = $total + 1;
                    $totalDeductedbyOne = $tots -3;
                    $totalDeductedbyOne1 = $countFemale -2;
                    $index2= $index1 + $totalDeductedbyOne +3;
                    $sheet ->insertNewRowBefore($index1,$totalDeductedbyOne);
                    $totalfoCell = $index1 + $totalDeductedbyOne + 5;
                    $totalFemale =  $index1 + $totalDeductedbyOne + 1;
                    $totalLearner =  $index1 + $totalDeductedbyOne + 2; 
                    for($i = 13; $i<=($totalDeductedbyOne+13); $i++)
                    {
                        $sheet ->mergeCells('B'.$i.':C'.$i.'');
                    }

                    for ($indexofBook=0; $indexofBook < count($arrayBook2); $indexofBook++) { 
                        
                        $sheet ->setCellValue(''.$column[$colIter].'9',$arrayBook2[$indexofBook] );

                        $booksIssuedM[$indexofBook] = mysqli_num_rows(mysqli_query($con, "SELECT * from booksacquireddetails where section_id='".$id."' and book_id='".$arrayBookID2[$indexofBook]."' and Gender='Male'"));
                        $booksReturnedM[$indexofBook] = mysqli_num_rows(mysqli_query($con, "SELECT * from booksacquireddetails where section_id='".$id."' and book_id='".$arrayBookID2[$indexofBook]."' and Gender='Male' and booksAcquired_remarks=''"));
                        $booksIssuedF[$indexofBook] = mysqli_num_rows(mysqli_query($con, "SELECT * from booksacquireddetails where section_id='".$id."' and book_id='".$arrayBookID2[$indexofBook]."' and Gender='Female'"));
                        $booksReturnedF[$indexofBook] = mysqli_num_rows(mysqli_query($con, "SELECT * from booksacquireddetails where section_id='".$id."' and book_id='".$arrayBookID2[$indexofBook]."' and Gender='Female' and booksAcquired_remarks=''"));
                        

                        $colIter= $colIter + 2;
                    }
                    $sheet ->setCellValue('K5', $SY);
                    $sheet ->setCellValue('K7', $level);
                    $sheet ->setCellValue('N7', $sectionName);
                    execute($con,$sheet,$total,$countMale,$countFemale,$countBook,$id,$booksIssuedM,$booksReturnedM,$booksIssuedF,$booksReturnedF,$arrayBook2,$arrayBookID2,$column,$totalFemale,$totalLearner,$totalfoCell,$adviser);
                }
            }


            
            

            function execute($con,$sheet,$total,$countMale,$countFemale,$countBook,$id,$booksIssuedM,$booksReturnedM,$booksIssuedF,$booksReturnedF,$b,$bID,$column,$totalFemale,$totalLearner,$totalfoCell,$adviser)
            {
                $no1 = 1;
                $start1 = 12;
                $no2 = 1;
                $query1 = mysqli_query($con, "SELECT * from controlDetails where section_id='".$id."' order by Gender DESC, Student ASC");


                $totalNumberofMale = -1;

                while($row = mysqli_fetch_array($query1))
                {
                    $totalNumberofMale++;
                    if($countMale == $totalNumberofMale){
                        $no1--;
                        $sheet ->getStyle('A'.$start1.':T'.$start1.'')
                        ->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
                        $sheet ->setCellValue('A'.$start1.'', $no1);
                        $sheet ->setCellValue('B'.$start1.'', '<=== TOTAL FOR MALE | TOTAL COPIES ===>');
                        $newColIter = 0;
                        if($countBook != 0){
                            for($i = 0; $i < count($b); $i++)
                            {
                                
                                $sheet ->getStyle(''.$column[$newColIter].''.$start1.'')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
                                $sheet ->setCellValue(''.$column[$newColIter].''.$start1.'', $booksIssuedM[$i]);
                                $newColIter++;
                                $sheet -> getStyle(''.$column[$newColIter].''.$start1.'')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
                                $sheet ->setCellValue(''.$column[$newColIter].''.$start1.'', $booksReturnedM[$i]);
                                $newColIter++; 
                            }
                        }
                        $start1++;
                        $no1 = 1;
                    }
                    //COLUMN FOR EXCEL
                    
                    $colStart = 0;


                    $sheet ->setCellValue('A'.$start1.'', $no1);
                    $sheet ->setCellValue('B'.$start1.'', $row[5]);
                    $remarks = '';
                    $controlID = $row[0];
                     if($countBook != 0)
                    {
                        for($i = 0; $i <count($bID); $i++ )
                        {
                            $query = mysqli_query($con, "SELECT * from booksacquired where control_id='".$controlID."' and book_id='".$bID[$i]."'");
                            if($row = mysqli_fetch_array($query))
                                {
                                    $date1 = explode('-', $row[3]);
                                    $date2 = explode('-', $row[4]);

                                    $issued = $date1[0].'/'.$date1[1].'/'.$date1[2];
                                    if($row[2] != '')
                                    {
                                        
                                        $returned = $row[2];
                                        switch ($row[2]) {
                                            case 'FM':
                                                    $remarks = 'LLTR';
                                                break;
                                            case 'TDO':
                                                    $remarks = 'TLTR';
                                                break;
                                            case 'NEG':
                                                    $remarks = 'PLT';
                                                break;
                                            
                                            default:
                                                # code...
                                                break;
                                        }
                                    }
                                    else
                                    {
                                        if($row[4] != '')
                                        {
                                            $returned =$date2[0].'-'.$date2[1].'-'.$date2[2];
                                        }
                                        else
                                        {
                                            $returned = '';
                                        }
                                       
                                    }

                                    $sheet ->setCellValue(''.$column[$colStart].''.$start1.'', $row[3]);
                                    $colStart++;
                                    $sheet ->setCellValue(''.$column[$colStart].''.$start1.'', $returned);
                                    $colStart++;

                                }
                        }
                    }
                    
                    $sheet ->setCellValue('T'.$start1.'', $remarks);
                    $start1++;
                    $no1++;
                }
                $no1--;
                $sheet ->setCellValue('A'.$totalFemale.'', $no1);

                $newColIter = 0;
                if($countBook != 0){
                    for($i = 0; $i < count($b); $i++)
                    {
                        
                        $sheet ->getStyle(''.$column[$newColIter].''.$totalFemale.'')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
                        $sheet ->setCellValue(''.$column[$newColIter].''.$totalFemale.'', $booksIssuedF[$i]);
                        $newColIter++;
                        $sheet -> getStyle(''.$column[$newColIter].''.$totalFemale.'')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
                        $sheet ->setCellValue(''.$column[$newColIter].''.$totalFemale.'', $booksReturnedF[$i]);
                        $newColIter++; 
                    }
                    $sheet ->setCellValue('A'.$totalLearner.'', $total);
                    $newColIter = 0;
                    for($i = 0; $i < count($b); $i++)
                    {
                        
                        $sheet ->getStyle(''.$column[$newColIter].''.$totalLearner.'')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
                        $sheet ->setCellValue(''.$column[$newColIter].''.$totalLearner.'', ($booksIssuedF[$i] + $booksIssuedM[$i]));
                        $newColIter++;
                        $sheet -> getStyle(''.$column[$newColIter].''.$totalLearner.'')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
                        $sheet ->setCellValue(''.$column[$newColIter].''.$totalLearner.'', ($booksReturnedF[$i] + $booksReturnedM[$i]));
                        $newColIter++; 
                    }    
                }
                $sheet ->setCellValue('R'.$totalfoCell.'', $adviser);
            }
            
        $writer = PHPExcel_IOFactory::createWriter($phpExcel, "Excel2007");

        //$writer->save('C:/Users/MLD/Documents/School Form 3-'.$sectionName.'-'.$SY.'.xlsx'); 
        $fileName = "";
        switch ($level) {
            case 'Grade 7':
                $fileName = "SF3_".$SY."_".$level." (Year I) - ".$sectionName;
                break;
            case 'Grade 8':
                $fileName = "SF3_".$SY."_".$level." (Year II) - ".$sectionName;
                break;
            case 'Grade 9':
                $fileName = "SF3_".$SY."_".$level." (Year III) - ".$sectionName;
                break;
            case 'Grade 10':
                $fileName = "SF3_".$SY."_".$level." (Year IV) - ".$sectionName;
                break;
            default:
                # code...
                break;
        }

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$fileName.'".xlsx"');
        header('Cache-Control: max-age=0');
        $writer->save('php://output');
        
    ?>