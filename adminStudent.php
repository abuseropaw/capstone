<?php
    include_once 'sessionAdmin.php';
    include_once 'dbconnect.php';

    if(isset($_POST['showall'])){

        $query = mysqli_query($con, "SELECT * from subjectDetails");

    }


?>
<!DOCTYPE html>

    <!-- HEAD -->
    <?php include_once 'head.php'; ?>
    <!-- HEAD   -->
    <body>
        <!-- HEADER -->
        <?php include_once 'header.php'; ?>
        <!-- HEADER -->

        <section id="main">
            <ol class="breadcrumb">
                <li><a href="adminHome.php">Home</a></li>
                <li class="active">Students</li>
            </ol>
            <?php
                $toggle = 'adminStudent';
                include_once 'sidebar.php';
            ?>



            <section id="content">

                <div class="container">

                    <div class="block-header">
                        <h1><i class="zmdi zmdi-account-box"></i> Learners
                        </h1>
                    </div>

                    <!-- Add button -->

                    <div class="card">
                        <div class="action-header clearfix" data-ma-action="action-header-open">
                            <div class="ah-label hidden-xs"><b>Click here to search for a learner</b></div>

                            <div class="ah-search">
                                <input type="text" name='search1' id="search1" placeholder="Start typing..." class="ahs-input" autofocus>
                                <i class="ahs-close" data-ma-action="action-header-close">&times;</i>
                            </div>

                           <ul class="actions">
                                <li>
                                    <a href="#" data-ma-action="action-header-open">
                                        <i class="zmdi zmdi-search"></i>
                                    </a>
                                </li>


                            </ul>
                        </div>

                        <div class="card-body card-padding">

                            <div class="contacts clearfix row" id="resultsaSearch">



                            </div>
                        </div>
                    </div>


                </div>

            </section>
        </section>
        <div class="modal fade" id="changepic" tabindex="-1" role="dialog" aria-hidden="true"  data-keyboard="false">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <form id='addProfilePic' method="post" enctype='multipart/form-data'>
                        <div class="modal-header">
                            <h1 class="modal-title"><b>PROFILE PICTURE</b></h1>
                        </div>
                        <div class="modal-body">
                            <center>
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-preview thumbnail" data-trigger="fileinput"></div>
                                <div>
                                    <span class="btn btn-info btn-xs btn-file">
                                        <span class="fileinput-new">Select image</span>
                                        
                                        <input type="file" name="file" id="file">
                                    </span>
                                    <button type="submit" name="changeprofilepic" id="changeprofilepic" class="btn btn-success btn-xs">CHANGE PICTURE</button>
                                    
                                </div>
                            </div>
                            </center>
                            <div class="form-group fg-line">
                                <input type='hidden' name="ID" id="ID">
                            </div>
                        </div>

                        
                    </form>
                </div>
            </div>
        </div>

        <!-- FOOTER -->
        <?php include_once 'footer.php' ?>
        <!-- FOOTER -->

        <!-- Javascript Libraries -->
        <?php include_once 'scripts.php'; ?>
        <!-- Javascript Libraries -->
        <script type="text/javascript" src="js/Notification.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $('#data-table-basic').DataTable();


                $('#addProfilePic').on('submit', function(e){
                    e.preventDefault();
                    $.ajax({
                        url:"ajaxJQuery/addProfilePic.php",
                        method: "post",
                        data: new FormData(this),
                        contentType:false,
                        cache: false,
                        processData:false,
                        success:function(data){
                            if(data == "not allowed")
                            {
                                var message = "Unsupported format for profile";
                                executeNotif(message,"danger");
                            }
                            else if(data == "successLearner")
                            {
                                $('#changepic').modal('hide');
                                var txt = $('#search1').val();
                                $.ajax({
                                    url:"ajaxJQuery/searchStudent1.php",
                                    method:"POST",
                                    data:{search:txt},
                                    dataType:"text",
                                    success:function(data)
                                    {
                                        if(data == "")
                                        {
                                            $('#resultsaSearch').html(
                                                '<div class="card-body card-padding" id="hehe">'
                                                +'Your search - <b>'+txt+'</b> - did not match any learner account.'
                                                +'<br />'
                                                +'Suggestions:<br />'
                                                +'<ul style="margin-left:1.3em;margin-bottom:2em"><li>Make sure that all words are spelled correctly.</li><li>Try different keywords.</li><li>Try more general keywords.</li></ul>'
                                            +'</div>'
                                                );
                                        }
                                        else
                                        {
                                            
                                            var message = "Successfully added picture to learner";
                                            executeNotif(message,"success");
                                            $('#resultsaSearch').html(data);
                                            
                                        }
                                        
                                    }
                                });
                            }
                        }
                    });
                });

                $(document).on('click', '.change_pic', function(){
                    var sID = $(this).attr("id");
                    $.ajax({
                        url:"ajaxJQuery/searchTeacher.php",
                        method:"POST",
                        data:{sID:sID},
                        success:function(data){
                            $('#ID').val(sID);
                            $('#changepic').modal('show');
                        }

                    });
                });

                $('#search1').keyup(function(objEvent){
                    var txt = $('#search1').val();

                    (objEvent) ? keycode = objEvent.keyCode : keycode = event.keyCode;
                    if(keycode == 13)
                    {
                        $.ajax({
                            url:"ajaxJQuery/searchStudent1.php",
                            method:"POST",
                            data:{search:txt},
                            dataType:"text",
                            success:function(data)
                            {
                                if(data == "")
                                {
                                    $('#resultsaSearch').html(
                                        '<div class="card-body card-padding" id="hehe">'
                                        +'Your search - <b>'+txt+'</b> - did not match any learner account.'
                                        +'<br />'
                                        +'Suggestions:<br />'
                                        +'<ul style="margin-left:1.3em;margin-bottom:2em"><li>Make sure that all words are spelled correctly.</li><li>Try different keywords.</li><li>Try more general keywords.</li></ul>'
                                    +'</div>'
                                        );
                                }
                                else
                                {
                                    $('#resultsaSearch').html(data);
                                }
                            }
                        });
                    }

                });

                $('#globalSearch').keyup(function(objEvent){
                    var txt = $('#globalSearch').val();

                    (objEvent) ? keycode = objEvent.keyCode : keycode = event.keyCode;
                    if(keycode == 13)
                    {
                        $.ajax({
                            url:"ajaxJQuery/searchStudent1.php",
                            method:"POST",
                            data:{search:txt},
                            dataType:"text",
                            success:function(data)
                            {
                                if(data == "")
                                {
                                    $('#resultsaSearch').html(
                                        '<div class="card-body card-padding" id="hehe">'
                                        +'Your search - <b>'+txt+'</b> - did not match any learner account.'
                                        +'<br />'
                                        +'Suggestions:<br />'
                                        +'<ul style="margin-left:1.3em;margin-bottom:2em"><li>Make sure that all words are spelled correctly.</li><li>Try different keywords.</li><li>Try more general keywords.</li></ul>'
                                    +'</div>'
                                        );
                                }
                                else
                                {
                                    $('#resultsaSearch').html(data);
                                }
                            }
                        });

                    }
                });


            } );
        </script>
    </body>

<!-- Mirrored from byrushan.com/projects/ma/1-7-1/jquery/light/widget-templates.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 01 May 2017 06:35:54 GMT -->
</html>
