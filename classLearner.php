<?php
    include_once 'sessionAdmin.php';
    include_once 'dbconnect.php';
    
    $sectionId = $_GET['sID'];
    $sectionID = $_GET['sID'];
 
  

    function checkifCurrent($con, $sectionID)
    {
        $query = mysqli_fetch_row(mysqli_query($con, "SELECT * from section where section_id='".$sectionID."'"));
        $sSY = $query[8];

        $query1= mysqli_fetch_row(mysqli_query($con, "SELECT * from schoolyear where sy_remarks='Open'"));
        $SY = $query1[1];
        if($sSY == $SY)
        {
            return true;
        }
        else{
            return false;
        }

    }

    function checkifFirstTime($con,$sectionId)
    {
        $getMinsyID = mysqli_fetch_row(mysqli_query($con, "SELECT min(sy_id) from schoolyear"));
        $getMinSy = mysqli_fetch_row(mysqli_query($con, "SELECT sy_year from schoolyear where sy_id=(Select min(sy_id) from schoolyear)"));

        $getYearSection = mysqli_fetch_row(mysqli_query($con, "SELECT sy_id from section where section_id='".$sectionId."'"));
        if($getYearSection[0] == $getMinSy[0])
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
?>
<!DOCTYPE html>
    <!-- HEAD -->
    <?php include_once 'head.php'; ?>
    <!-- HEAD   -->
    <body>
        <!-- HEADER -->
        <?php include_once 'header.php'; ?>
        <!-- HEADER -->

        <section id="main">
            <ol class="breadcrumb">
                <li><a href="adminHome.php">Home</a></li>
                <li><a href="adminClass.php">Classes</a></li>
                <li class="active">Class Learner</li>
            </ol>
            <?php
                $toggle = 'adminClass';
                include_once 'sidebar.php';
            ?>


            <section id="content">
                <div class="container">
                    <!-- Colored Headers -->
                    <div class="block-header">
                        <h1><?php
                        		$sectionId = $_GET['sID'];
                        		$query = mysqli_query($con, "SELECT * from classinformation where section_id='".$_GET['sID']."'");
                        		if($row = mysqli_fetch_array($query)){
                        			echo $row['section_name'];
                        		}
                        	?>
                        </h1>
                        <div class="btn-demo actions">
                            <?php
                            if(checkifCurrent($con, $sectionId)){

                                if(checkifFirstTime($con,$sectionId))
                                {

                                    echo "

                                    <button type='submit' name='delete' id='".$sectionId."' data-toggle='tooltip' data-placement='top' title='Details' class='btn bgm-red btn-lg delete_all'><i class='zmdi zmdi-delete'></i><b> DELETE ALL</b></button>
                                    <button  href='#modalImportLearner' data-toggle='modal' class='btn btn-default btn-lg'><i class='zmdi zmdi-account-box'></i> <b>IMPORT SF1</b>
                                    </button>

                                    ";
                                }
                            }

                            ?>
                        </div>

                    </div>


                    <div id="hehe">
                    	<?php include_once 'ajaxJQuery/displayClassLearner.php' ?>

                    </div>



                    <br/>
                    <br/>
                </div>
            </section>
        </section>

        <div class="modal fade" id="modalImportLearner" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    
                    <form method='POST' id='uploadSF1' enctype='multipart/form-data'>
                        <div class='modal-header'>
                            <h4 class='modal-title'><b>IMPORT SF1</b></h4>
                        </div>


                        <div class='modal-body'>

                            <div class='col-sm-12'>
                                <div class='form-group fg-line'>
                                    <input type='file' name='files' id='files' class='form-control' required>
                                    <input type='hidden' name='sectionId' id='sectionId' value='<?php echo $sectionId ?>'>
                                </div>
                            </div>
                            <b>NOTE:</b>
                            <ul>
                                <li>Set your computer's date correctly</li>
                                <li>You must set your computer month in either May or June</li>
                                <li>Your computer's date will be the enrolled date of the learner</li>
                            </ul>
                        </div>
                        
                        <div class='modal-footer'>

                            <button type='submit' id='addLearner' name="addLearner" class='btn btn-success'>CREATE CLASS</button>

                            <button type='button' class='btn btn-danger' data-dismiss='modal'>Close
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div id="loading" class="modal fade" data-backdrop="static" data-keyboard="false">  
            <div class="modal-dialog modal-sm">  
               <div class="modal-content">
               <center>   
                    <div class="modal-body" id="Details">
                         
                        <div class="preloader pl-xxl">
                            
                                <svg class="pl-circular" viewBox="25 25 50 50">
                                    <circle class="plc-path" cx="50" cy="50" r="20"/>
                                </svg>
                            
                        </div> 
                       <center><h4>Processing...Please wait</h4></center>
                    </div>  
                </center>
               </div>  
            </div>  
        </div>
        <!-- FOOTER -->
        <?php include_once 'footer.php' ?>
        <!-- FOOTER -->

        <!-- Javascript Libraries -->
        <?php include_once 'scripts.php'; ?>
        <!-- Javascript Libraries -->
        <script type="text/javascript" src="js/Notification.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){
            //Pace.restart();

                $('.delete_all').on('click', function(){
                    var tae = $(this).attr("id");
                    swal({
                        title: "Are you sure?",
                        text: "You will not be able to recover this data!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonText: "Yes, delete it!",
                    }).then(function(){

                        $.ajax({
                            url:"ajaxJQuery/deleteClassLearner.php",
                            method:"GET",
                            data:{sID:tae},
                            beforeSend:function(){
                            
                                $('#loading').modal('show');
                            },  
                            success:function(data){
                                if(data != '0'){
                                     $('#loading').modal('hide');
                                    var message = "Successfully deleted section.";
                                    executeNotif(message,"success");
                                   $('#hehe').html(data);
                               }
                               else
                               {
                                 $('#loading').modal('hide');
                                    var message = "Error on deleting section. Section has enrolled learners";
                                    executeNotif(message,"danger");
                               }
                            }


                        });
                    });

                });


                $('#uploadSF1').on('submit', function(e){
                   e.preventDefault();
                   var sectionID = "<?php echo $sectionID; ?>";
                    $.ajax({
                        url:"ajaxJQuery/uploadSF1.php",
                        method:"post",
                        data: new FormData(this),
                        contentType:false,
                        cache: false,
                        processData:false,
                        beforeSend:function(){
                            $('#loading').modal('show');
                            $('#modalImportLearner').modal('hide');
                        },
                        success:function(data){
                            if(data == "incorrectMonth")
                            {
                                var message = "Kindly change your PC's date. Set the computer's date correctly";
                                executeNotif(message,"danger");
                                $('#loading').modal('hide');
                            }
                            else
                            {
                                $('#loading').modal('hide');
                                var message = "Successfully loaded learner in section.";
                                executeNotif(message,"success");
                               $('#hehe').html(data);
                            }
                            
                           
                            
                        },
                        error: function (jqXHR, exception) {
                          //alert(jqXHR.status);
                         
                          if (jqXHR.status === 0) {
                             doOrdie(sectionID);

                          } else if (jqXHR.status == 404) {
                             location.reload();
                          } else if (jqXHR.status == 500) {
                                location.reload();
                          } else if (exception === 'parsererror') {
                             location.reload();
                          } else if (exception === 'timeout') {
                            location.reload();
                          } else if (exception === 'abort') {
                             location.reload();
                          } else {
                            doOrdie(sectionID);
                          }
                          
                       }
                    });

                });

                function doOrdie(sectionID)
                {
                    $.ajax({
                        url:"ajaxJQuery/getClassLearner.php?sID="+sectionID+"",
                        method:"get",
                        success:function(data){
                            var message = "Successfully loaded learner in section.";
                            executeNotif(message,"success");
                           $('#hehe').html(data);
                            $('#loading').modal('hide');
                        },
                        error:function(){
                            location.reload();
                        }
                     });
                }

            });
        </script>
    </body>

<!-- Mirrored from byrushan.com/projects/ma/1-7-1/jquery/light/widget-templates.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 01 May 2017 06:35:54 GMT -->
</html>
