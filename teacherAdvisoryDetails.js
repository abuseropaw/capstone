$(document).ready(function() {
                
                $('#datatable-responsive').DataTable();


                $('#remarks1').on('change', function(){
                    var select = $(this).val();
                    if(select == "DRP")
                    {
                        $('#_reasonID').show();
                         $('#_schoolName').hide();
                    }
                    else if(select == "T/I" | select == "T/O" | select == "LE")
                    {
                        $('#_reasonID').hide();
                        $('#_schoolName').show();
                        $('#_others').hide();

                    }
                    else
                    {
                         $('#_reasonID').hide();
                        $('#_schoolName').hide();
                        $('#_others').hide();
                    }
                    
                });

                $('#reason').on('change', function(){
                    var select1 = $(this).val();
                    if(select1 == "Others")
                    {
                        $('#_others').show();
                    }
                    else
                    {
                        $('#_others').hide();
                    }
                    
                });


                $(document).keyup(function(){
                    var lrn = $('#student_LRN').val();
                    
                        if(lrn != ''){
                            $.ajax({
                                url:"ajaxJQuery/checkLRN.php",
                                method:"POST",
                                data:{search:lrn},
                                dataType:"text",
                                success:function(data)
                                {
                                    if(data == 'naa')
                                    {

                                        swal("Oops", "Learner's LRN already exist", "error"); 
                                        $('#student_LRN').focus();
                                    }
                                }
                            });
                        }
                    
                });
                
                

                $(document).on('click', '.enroll_learnertoSection', function(){
                    var lrn = $(this).attr("id");
                    var sectionID = $(this).attr("value");

                    $.ajax({
                        url:"ajaxJQuery/selectStudentInfo.php",
                        method:"POST",
                        data:{lrn:lrn,sectionID:sectionID},
                        dataType:"json",
                        success:function(data){
                            $('#NameofStudent').val(data.Name);
                            $('#studentID').val(data.student_id);
                            $('#sectionIDS').val(sectionID);
                            $('#modalEnrollLearner').modal('show');
                        }

                    });
                });

                $('#enroll').on("submit", function(event){
                    event.preventDefault();
                    
                        $.ajax({  
                              url:"ajaxJQuery/enrollLearner.php",  
                              method:"POST",  
                              data:$('#enroll').serialize(),  
                              success:function(data){
                                   if(data == '1')
                                   { 
                                      
                                       $('#enroll')[0].reset();
                                       $('#modalRemoveLearner').modal('hide');
                                   }
                                   else (data == '0')
                                   {    
                                        swal("Oops", "The learner has been used for other table", "error");  
                                       $('#enroll')[0].reset();
                                       $('#modalRemoveLearner').modal('hide');
                                   }

                                   setTimeout(function()
                                   {// wait for 5 secs(2)
                                        location.reload(); // then reload the page.(3)
                                    }, 800);
                              }  

                         });  
                    
                });
                

                //SEARCH STUDENT
                $('#search').keyup(function(objEvent){
                    var txt = $('#search').val();
                    var id = $('#secID').val();
                    (objEvent) ? keycode = objEvent.keyCode : keycode = event.keyCode;
                    console.log(keycode);
                    if(keycode == 13){
                        
                        $.ajax({
                            url:"ajaxJQuery/searchStudent.php",
                            method:"POST",
                            dataType:"text",
                            data:
                            {
                                search:txt,
                                section:id
                            },
                            
                            success:function(data)
                            {
                                $('#resultsaSearch').html(data);
                            }
                        });
                        
                    }
                });

                //SELECT LEARNER TO DELETE
                $(document).on('click', '.delete_learner', function(){
                    var important_id1 = $(this).attr("id");
                    $.ajax({
                        url:"ajaxJQuery/selectControl1.php",
                        method:"POST",
                        data:{important_id1:important_id1},
                        dataType:"json",
                        success:function(data){
                            $('#student1').val(data.Student);
                            $('#student_id').val(data.student_id);
                            $('#important_id1').val(important_id1);
                            $('#modalRemoveLearner').modal('show');
                        }

                    });
                });

                //SELECT LEARNER TO EDIT
                $(document).on('click', '.edit_learner', function(){
                    var important_id1 = $(this).attr("id");
                    $.ajax({
                        url:"ajaxJQuery/selectControl1.php",
                        method:"POST",
                        data:{important_id1:important_id1},
                        dataType:"json",
                        success:function(data){
                            $('#student2').val(data.Student);
                            $('#remarks1').val(data.control_remarks);
                            $('#student_id2').val(data.student_id);
                            $('#important_id2').val(important_id1);
                            
                            $('#modalEdit').modal('show');
                        }

                    });
                });

                 //ADD ATTENDANCE SELECT
                $(document).on('click', '.add_attendance', function(){
                    var monthID = $(this).attr("id");
                    var sectionID = '<?php echo $id; ?>';
                    $('#monthID').val(monthID);
                    $('#attendancesectionID').val(sectionID);
                    $('#modalAddAttendance').modal('show');
                });

                //UN_ENROLL LEARNER
                $('#removeLearner').on("submit", function(event){
                    event.preventDefault();
                    if($('#student1').val() == 'Choose')
                    {
                       
                    }
                    else 
                    {
                        $.ajax({  
                              url:"ajaxJQuery/removeLearner.php",  
                              method:"POST",  
                              data:$('#removeLearner').serialize(),  
                              success:function(data){
                                   if(data != '1')
                                   { 
                                      
                                       $('#removeLearner')[0].reset();
                                       $('#modalRemoveLearner').modal('hide');
                                       $('#learner').html(data);
                                   }
                                   else
                                   {    
                                        swal("Oops", "The learner has been used for other table", "error");  
                                       $('#removeLearner')[0].reset();
                                       $('#modalRemoveLearner').modal('hide');
                                   }

                                
                              }  

                         });  
                    }
                });

                //LEARNER EDIT
                $('#editLearner').on("submit", function(event){
                    event.preventDefault();
                    if($('#remarks1').val() == '')
                    {
                        $('#remarks1').focus();
                    }
                    else 
                    {
                        $.ajax({  
                              url:"ajaxJQuery/updateRemarks.php",  
                              method:"POST",  
                              data:$('#editLearner').serialize(),  
                              success:function(data){
                                if(data != '1')
                                { 
                                   //swal("Good job", "You successfully updated learner", "success");  
                                   $('#editLearner')[0].reset();   
                                   $('#modalEdit').modal('hide');
                                   $('#learner').html(data);
                                }
                                else
                                {
                                    swal("Oops", "Error in updating remarks of a learner", "error");  
                                   $('#editLearner')[0].reset();   
                                   $('#modalEdit').modal('hide');
                                }
                                
                              }  

                         });  
                    }
                });
                /////////////////////////////////////////////////////////////

                //INSERT BOOKS ACQUIRED
                $('#insert_form2').on("submit", function(event){
                    event.preventDefault();
                    if($('#type').val() == 'Choose')
                    {
                        $('#type').focus();    
                         alert("Choose either return or issue");
                    }
                    else if($('#date').val() == '')
                    {
                        $('#date').focus();    
                         alert("Date is required");
                    }
                    else 
                    {
                        $.ajax({  
                              url:"ajaxJQuery/insertBooksAcquired.php",  
                              method:"POST",  
                              data:$('#insert_form2').serialize(), 
                              beforeSend:function(){
                                    $('#book').modal('hide');  
                                   $('#loading').modal('show');
                              },  
                              success:function(data){ 
                                   //swal("Good job", "You successfully issued books to students", "success");  
                                   $('#insert_form2')[0].reset();   
                                   
                                   //$('#book').modal('hide');
                                   $('#loading').modal('hide'); 
                                   $('#messages11').html(data);
                              }  

                         });  
                    }
                });

                //INSERT ATTENDANCE
                $('#addAttendanceForm').on("submit", function(event){
                    event.preventDefault();
                    
                    $.ajax({  
                          url:"ajaxJQuery/insertAttendance.php",  
                          method:"POST",  
                          data:$('#addAttendanceForm').serialize(), 
                          
                          success:function(data){ 
                            if(data == '0')
                            {
                                
                                var message = "Error on adding attendance to all learners.Dont exceed to the total school days";
                                executeNotif(message,"danger");
                               
                                 $('#modalAddAttendance').modal('hide');  
                               $('#addAttendanceForm')[0].reset(); 
                            }
                            else
                            {
                                $('#modalAddAttendance').modal('hide');  
                               $('#addAttendanceForm')[0].reset();   
                               $('#profile11').html(data); 
                               var message = "Successfully added attendance to registered learners for this Month";
                                executeNotif(message,"success");
                             }  
                          }  

                     });  
                    
                });

                $('#editAttendanceForm').on("submit", function(event){
                    event.preventDefault();
                    
                    $.ajax({  
                          url:"ajaxJQuery/updateAttendance.php",  
                          method:"POST",  
                          data:$('#editAttendanceForm').serialize(), 
                         
                          success:function(data){ 
                            if(data == '0')
                            {
                                 
                                var message = "Error on adding attendance to all learners.Dont exceed to the total school days";
                                executeNotif(message,"danger");
                                 $('#modalAddAttendance').modal('hide');  
                               $('#editAttendanceForm')[0].reset();
                              
                            }
                            else
                            {
                                $('#modalAddAttendance').modal('hide');  
                                $('#editAttendanceForm')[0].reset();   
                                $('#profile11').html(data); 
                                var message = "Successfully updated attendance to registered learners for this Month";
                                executeNotif(message,"success");
                             }  
                          }  

                     });  
                    
                });

                //SELECT LEARNER TO EDIT ATTENDANCE
                $(document).on('click', '.edit_attendance', function(){
                    var id = $(this).attr("id");
                    var sectionID = "<?php echo $id; ?>";
                    var tunga = id.split('/');
                    $.ajax({
                        url:"ajaxJQuery/selectAttendance.php",
                        method:"GET",
                        data:{monthID:tunga[0], controlID:tunga[1]},
                        dataType:"json",
                        success:function(data){
                            $('#StudentName').val(data.Student);
                            $('#monthID1').val(tunga[0]);
                            $('#attendancesectionID1').val(sectionID);
                            $('#present1').val(data.attendance_presentDays);
                            $('#absent1').val(data.attendance_absentDays);
                            $('#tardy1').val(data.attendance_tardyDays);
                            $('#controlID').val(tunga[1]);
                            $('#modalEditAttendance').modal('show');
                        }

                    });
                });


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////  
                //SELECT BOOKS ACQUIRED TO EDIT
                $(document).on('click', '.edit_booksacquired', function(){
                    var important_id1 = $(this).attr("id");
                    $.ajax({
                        url:"ajaxJQuery/selectControl1.php",
                        method:"POST",
                        data:{important_id1:important_id1},
                        dataType:"json",
                        success:function(data){
                            $('#studentID1').val(important_id1);
                            $('#editBookAcquired').modal('show');
                        }

                    });
                });

                //EDIT BOOKS ACQUIRED
                $('#editBook').on("submit", function(event){
                    event.preventDefault();
                    if($('#type1').val() == 'Choose')
                    {
                        $('#type1').focus();    
                         alert("Choose either return or issue");
                    }
                   
                    else 
                    {
                        $.ajax({  
                              url:"ajaxJQuery/updateBooksAcquired.php",  
                              method:"POST",  
                              data:$('#editBook').serialize(),
                                 
                              success:function(data){ 

                                    if(data == '0')
                                    {
                                       swal("Oops", "Error on updating books acquired", "error"); 
                                    }
                                    else
                                    {
                                         
                                       $('#editBook')[0].reset();   
                                      
                                       $('#editBookAcquired').modal('hide');
                                       $('#messages11').html(data);
                                    } 
                              }  

                         });  
                    }
                });
                

                $(document).on('click', '.grade_details', function(){
                    var s_id = $(this).attr("id");
                    $.ajax({
                        url:"ajaxJQuery/viewGradeDetails.php",
                        method:"POST",
                        data:{s_id:s_id},
                        
                        success:function(data){
                            $('#Details').html(data);
                            $('#gradeDetails').modal('show');
                        }
                    });
                });

                //INSERT LEARNER
                $('#insert_form').on("submit", function(event){
                    event.preventDefault();
                    $('#student_LRN').focus();
                    if($('#student_LRN').val() == '')
                    {
                        $('#student_LRN').focus();    
                         alert("LRN is required and valid");
                    }
                    else if($('#student_fname').val() == '')  
                    {
                            $('#student_fname').focus();    
                         alert("First name is required and valid");
                         
                    }  
                    else if($('#student_mname').val() == '')  
                    {  
                        $('#student_mname').focus();  
                         alert("Middle name is required and valid");  
                    }  
                    else if($('#student_lname').val() == '')  
                    {
                        $('#student_lname').focus();
                         alert("Maximun grade is required");  
                    }
                    else if($('#student_sex').val() == 'GENDER')
                    {
                        $('#student_sex').focus();
                        alert("You must select gender!");
                    }
                    else if($('#student_birthDate').val() == '')
                    {
                        $('#student_birthDate').focus();
                        alert("You must enter birth date!");
                    }
                    else if($('#student_age').val() == '')
                    {
                        $('#student_age').focus();
                        alert("You must enter age!");
                    }
                    else if($('#student_motherTongue').val() == '')
                    {
                        $('#student_motherTongue').focus();
                        alert("You must select language!");
                    }
                    else if($('#student_IP').val() == '')
                    {
                        $('#student_IP').focus();
                        alert("You must select ethnic type!");
                    }
                    else if($('#student_religion').val() == 'RELIGION')
                    {
                        $('#student_religion').focus();
                        alert("You must select religion!");
                    }
                    
                    else if($('#student_addressBarangay').val() == '')
                    {
                        $('#student_addressBarangay').focus();
                        alert("You must enter Barangay");
                    }
                    else if($('#student_addressMunicipality').val() == 'MUNICIPALITY')
                    {
                        $('#student_addressMunicipality').focus();
                        alert("You must enter municipality");
                    }
                    else if($('#student_addressProvince').val() == 'PROVINCE')
                    {
                        $('#student_addressProvince').focus();
                        alert("You must enter province");
                    }
                    else if($('#parent_Mfname').val() == '')
                    {
                        $('#parent_Mfname').focus();
                        alert("You must enter mother's first name");
                    }
                    else if($('#parent_Mmname').val() == '')
                    {
                        $('#parent_Mmname').focus();
                        alert("You must enter mother's middle name");
                    }
                    else if($('#parent_Mlname').val() == '')
                    {
                        $('#parent_Mlname').focus();
                        alert("You must enter mother's last name");
                    }
                    else  
                    {  
                         $.ajax({  
                              url:"ajaxJQuery/insertStudent.php",  
                              method:"POST",  
                              data:$('#insert_form').serialize(),  
                              beforeSend:function(){  
                                   $('#insert1').val("Inserting..");   
                              },  
                              success:function(data){ 
                                   swal("Good job", "You successfully created Student account", "success");  
                                   $('#insert_form')[0].reset();   
                                   $('#listofStudents').html(data);
                                    $('#modalColor').modal('hide');
                              }  

                         });  
                    }
                });
                
            } );