<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" type="image/x-icon" href="img/lnhs.ico" alt="logo" />
<title>SIS | LNHS</title>

<link href="vendors/bower_components/animate.css/animate.min.css" rel="stylesheet">
<link href="vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css" rel="stylesheet">
<link href="vendors/bower_components/sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
<link href="vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet">
<link href="vendors/bower_components/dropzone/dist/min/dropzone.min.css" rel="stylesheet">
<link href="vendors/bower_components/chosen/chosen.css" rel="stylesheet">
<link href="vendors/bower_components/datatables.net-dt/css/jquery.dataTables.min.css" rel="stylesheet">
<link href="vendors/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">
<link href="vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
<link href="vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet">
<link href="vendors/bower_components/lightgallery/dist/css/lightgallery.min.css" rel="stylesheet">

<link href="css/app_1.min.css" rel="stylesheet">
<link href="css/app_2.min.css" rel="stylesheet">
<link href="css/demo.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/pace.min.css">
<link href="css/highcharts.css" rel="stylesheet">
</head>