<?php
 require('dbconnect.php');
 
 require('Report/fpdf.php');
 //$schoolYear = $_SESSION['sy_year'];
 //$gradeLevel = $_SESSION['year_lvl_title'];
 //$section = $_SESSION['section_title'];
 class PDF extends FPDF
 {
 	function Footer()
 	{
 		// Go to 1.5 cm from bottom
 		
 		$this->SetY(-50);
 		// Select Arial italic 8
 		$this->SetFont('Arial','I',8);
 		// Print centered page number
 		$this->Cell(0,14,'Page '.$this->PageNo(),1,0,'C');
 	}
 }
 $selectStudent = mysqli_query($con,"Select * from faculty_account");
 $pdf = new FPDF('L','in',array(8.5,14));
 //var_dump(get_class_methods($fpdf));
 
 $pdf->AddPage();
 
	
	$pdf->SetFont("Arial","B","15");
	$pdf->Ln(.5);
	$pdf->SetLeftMargin(.2);
	$pdf->Cell(0,0,"School Form 1 (SF 1) School Register",0,1,"C"); 
	$pdf->Ln(.0001);
	$pdf->SetFont("Arial","I","8");
	$pdf->Cell(0,0.35,"(This replaced Forms 1, Master List & STS Form 2-Family Background and Profile)",0,1,"C"); 
	
	$pdf->Image('Report/kne.png',.17,.8,1.1,1.1);
	$pdf->Image('Report/deped.png',11.9,.8,2,.8);
	
	//Region
	$pdf->SetFont("Arial","","10");
	$pdf->Cell(1.9,0.35,"School ID",0,0,"R");
	
	$pdf->SetFont("Arial","B","12");
	$pdf->Cell(1.5,0.35,"405152",1,0,"C");
	
	
	$pdf->SetFont("Arial","","10");
	$pdf->Cell(1,0.35,"Region",0,0,"R");
	
	$pdf->SetFont("Arial","B","12");
	$pdf->Cell(0.7,0.35,"X",1,0,"C");


	//Division
	$pdf->SetFont("Arial","","10");
	$pdf->Cell(1,0.35,"Division",0,0,"R");
	
	$pdf->SetFont("Arial","B","12");
	$pdf->Cell(2.4,0.35,"MISAMIS ORIENTAL",1,0,"C");

	//District
	$pdf->SetFont("Arial","","10");
	$pdf->Cell(.6,0.35,"District",0,0,"R");
	
	$pdf->SetFont("Arial","B","12");
	$pdf->Cell(2,0.35,"INITAO",1,1,"C");


	
	
	
	//New Line
	$pdf->Ln(.1);
	//School Name
	$pdf->SetFont("Arial","","10");
	$pdf->Cell(1.9,0.35,"School Name",0,0,"R");
	
	$pdf->SetFont("Arial","B","12");
	$pdf->Cell(3.2,0.35,"XAVIER ACADEMY",1,0,"C");
	
	//School Year
	$pdf->SetFont("Arial","","10");
	$pdf->Cell(1,0.35,"School Year",0,0,"R");
	
	$pdf->SetFont("Arial","B","12");
	$pdf->Cell(1.9,0.35,"",1,0,"L");
	
	//Grade Level
	$pdf->SetFont("Arial","","10");
	$pdf->Cell(1.1,0.35,"Grade Level",0,0,"R");
	
	$pdf->SetFont("Arial","B","12");
	$pdf->Cell(1.45,0.35,"",1,0,"L");
	
	//Section
	$pdf->SetFont("Arial","","10");
	$pdf->Cell(0.55,0.35,"Section",0,0,"R");
	
	$pdf->SetFont("Arial","B","12");
	$pdf->Cell(2.5,0.35,"",1,1,"L");
	
	//New Line
	$pdf->Ln(.1);
	$pdf->SetFont("Arial","B","8");
 	$pdf->Cell(.85,.70,"LRN",1,0,"C");
 	$pdf->Cell(1.4,.70,"NAME",1,0,"C");
 	$pdf->Cell(.32,.70,"SEX",1,0,"C");
 	$pdf->Cell(.82,.7,"BIRTH DATE",1,0,"C");
 	$pdf->Cell(.45,.70,"AGE",1,0,"C");
 	$pdf->Cell(.85,.70,"BIRTH PLACE",1,0,"C");
 	$pdf->Cell(.8,.70,"MT",1,0,"C");
 	$pdf->Cell(.55,.70,"IP",1,0,"C");
 	
 	$pdf->Cell(.65,.70,"RELIGION",1,0,"C");
 	$pdf->Cell(2.55,.70,"ADDRESS",1,0,"C");
 	$pdf->Cell(1.6,.70,"NAME OF PARENTS",1,0,"C");
 	$pdf->Cell(1.25,.70,"GUARDIAN",1,0,"C");
 	$pdf->Cell(.85,.70,"CONTACT #",1,0,"C");
 	$pdf->Cell(.7,.70,"REMARKS",1,0,"C");
 	
 	
 	
 	//MALE
 	while($row = mysqli_fetch_array($selectStudent)){
 		$pdf->Ln();
 		$pdf->SetFont("Arial","","6");
 		$pdf->Cell(.85,.30,$row['faculty_id'],1,0,"C");
 		$pdf->Cell(1.4,.30,$row['faculty_lname'],1,0,"C");
 		$pdf->Cell(.32,.30,"SEX",1,0,"C");
 		$pdf->Cell(.82,.3,"BIRTH DATE",1,0,"C");
 		$pdf->Cell(.45,.30,"AGE",1,0,"C");
 		$pdf->Cell(.85,.30,"BIRTH PLACE",1,0,"C");
 		$pdf->Cell(.8,.30,"MT",1,0,"C");
 		$pdf->Cell(.55,.30,"IP",1,0,"C");
 			
 		$pdf->Cell(.65,.30,"RELIGION",1,0,"C");
 		$pdf->Cell(2.55,.30,"ADDRESS",1,0,"C");
 		$pdf->Cell(1.6,.30,"NAME OF PARENTS",1,0,"C");
 		$pdf->Cell(1.25,.30,"GUARDIAN",1,0,"C");
 		$pdf->Cell(.85,.30,"CONTACT #",1,0,"C");
 		$pdf->Cell(.7,.30,"REMARKS",1,0,"C");
 			
 	}
 	$pdf->Ln();
 	$pdf->SetFont("Arial","B","7");
 	$pdf->Cell(.85,.30,"",1,0,"C");
 	$pdf->Cell(1.4,.30,"FEMALE",1,0,"C");
 	$pdf->Cell(.32,.30,"",1,0,"C");
 	$pdf->Cell(.82,.3,"",1,0,"C");
 	$pdf->Cell(.45,.30,"",1,0,"C");
 	$pdf->Cell(.85,.30,"",1,0,"C");
 	$pdf->Cell(.8,.30,"",1,0,"C");
 	$pdf->Cell(.55,.30,"",1,0,"C");
 	
 	$pdf->Cell(.65,.30,"",1,0,"C");
 	$pdf->Cell(2.55,.30,"",1,0,"C");
 	$pdf->Cell(1.6,.30,"",1,0,"C");
 	$pdf->Cell(1.25,.30,"",1,0,"C");
 	$pdf->Cell(.85,.30,"",1,0,"C");
 	$pdf->Cell(.7,.30,"",1,0,"C");
 	
 	
 	//FEMALE
 	for($i = 0; $i<15; $i++){
 		$pdf->Ln();
 		$pdf->SetFont("Arial","","6");
 		$pdf->Cell(.85,.30,$row['faculty_id'],1,0,"C");
 		$pdf->Cell(1.4,.30,$row['faculty_lname'],1,0,"C");
 		$pdf->Cell(.32,.30,"SEX",1,0,"C");
 		$pdf->Cell(.82,.3,"BIRTH DATE",1,0,"C");
 		$pdf->Cell(.45,.30,"AGE",1,0,"C");
 		$pdf->Cell(.85,.30,"BIRTH PLACE",1,0,"C");
 		$pdf->Cell(.8,.30,"MT",1,0,"C");
 		$pdf->Cell(.55,.30,"IP",1,0,"C");
 		
 		$pdf->Cell(.65,.30,"RELIGION",1,0,"C");
 		$pdf->Cell(2.55,.30,"ADDRESS",1,0,"C");
 		$pdf->Cell(1.6,.30,"NAME OF PARENTS",1,0,"C");
 		$pdf->Cell(1.25,.30,"GUARDIAN",1,0,"C");
 		$pdf->Cell(.85,.30,"CONTACT #",1,0,"C");
 		$pdf->Cell(.7,.30,"REMARKS",1,0,"C");
 	}
 	
 	
 	
 	
 	$pdf->Ln();
 	$pdf->SetFont("Arial","B","10");
 	$pdf->Cell(9.44,.30,"List of code of indicators under Remark column",0,0,"C");
 	$pdf->SetFont("Arial","","9");
 	$pdf->Cell(2.1,.30,"Prepared by:",0,0,"L");
 	$pdf->Cell(2.1,.30,"Prepared by:",00,0,"L");
 	
 	$pdf->Ln();
 	$pdf->SetFont("Arial","B","8");
 	$pdf->Cell(.6,.25,"Indicator",0,0,"L");
 	$pdf->Cell(.6,.25,"Code",0,0,"R");
 	$pdf->Cell(2.6,.25,"Required Information",0,0,"L");
 	$pdf->Cell(.6,.25,"Indicator",0,0,"L");
 	$pdf->Cell(.6,.25,"Code",0,0,"R");
 	$pdf->Cell(2.5,.25,"Required Information",0,0,"L");
 	$pdf->SetFillColor(5,51,031);
 	$pdf->Cell(.6,.25,"",1,0,"L",true);
 	
 	$pdf->SetFont("Arial","","8");
 	$pdf->Cell(.6,.25,"BoSY",1,0,"C");
 	$pdf->Cell(.6,.25,"EoSY",1,0,"C");

 	$pdf->Ln();
 	$pdf->SetFont("Arial","B","7");
 	$pdf->Cell(.6,.25,"Transferred Out",0,0,"L");
 	$pdf->Cell(.6,.25,"T/O",0,0,"R");
 	$pdf->Cell(2.6,.25,"Name of Public(P) Private(PR) School & Effectivity Date",0,0,"L");
 	$pdf->Cell(.6,.25,"CCT Recipient",0,0,"L");
 	$pdf->Cell(.6,.25,"CCT",0,0,"R");
 	$pdf->Cell(2.5,.25,"CCT Control/reference number & Effectivity Date",0,0,"L");
 	
 	$pdf->Cell(.6,.25,"MALE",1,0,"C");
 	$pdf->Cell(.6,.25,"",1,0,"C");
 	$pdf->Cell(.6,.25,"",1,0,"C");
 	$pdf->SetFont("Arial","U","7");
 	$pdf->Cell(2.2,.25,"                                                                      ",0,0,"C");
 	$pdf->Cell(2.2,.25,"                                                                     ",0,0,"C");
 	
 	$pdf->Ln();
 	$pdf->SetFont("Arial","B","7");
 	$pdf->Cell(.6,.25,"Transferred In",0,0,"L");
 	$pdf->Cell(.6,.25,"T/I",0,0,"R");
 	$pdf->Cell(2.6,.25,"Name of Public(P) Private(PR) School & Effectivity Date",0,0,"L");
 	$pdf->Cell(.6,.25,"Balik-Aral",0,0,"L");
 	$pdf->Cell(.6,.25,"B/A",0,0,"R");
 	$pdf->Cell(2.5,.25,"Name of school last attended and Year",0,0,"L");
 	
 	$pdf->Cell(.6,.25,"FEMALE",1,0,"C");
 	$pdf->Cell(.6,.25,"",1,0,"C");
 	$pdf->Cell(.6,.25,"",1,0,"C");
 	
 	$pdf->SetFont("Arial","","6.5");
 	$pdf->Cell(2.2,.25,"(Signature of Adviser over Printed Name)",0,0,"C");
 	$pdf->Cell(2.2,.25,"(Signature of Adviser over Printed Name)",0,0,"C");
 	
 	
 	
 	
 	
 	$pdf->Ln();
 	$pdf->SetFont("Arial","B","7");
 	$pdf->Cell(.6,.20,"Dropped",0,0,"L");
 	$pdf->Cell(.6,.20,"DRP",0,0,"R");
 	$pdf->Cell(2.6,.2,"Reason & Effectivity Date",0,0,"L");
 	$pdf->Cell(.6,.20,"Learner With Dissability",0,0,"L");
 	$pdf->Cell(.6,.2,"LWD",0,0,"R");
 	$pdf->Cell(2.5,.2,"Specify",0,0,"L");
 	
 	$pdf->Cell(.6,.40,"TOTAL",1,0,"C");
 	$pdf->Cell(.6,.40,"",1,0,"C");
 	$pdf->Cell(.6,.40,"",1,0,"C");
 	
 	$pdf->SetFont("Arial","B","6.5");
 	$pdf->Cell(2.2,.25,"",0,0,"C");
 	$pdf->Cell(2.2,.25,"",0,0,"C");
 	
 	
 	
 	
 	$pdf->Ln();
 	$pdf->SetFont("Arial","B","7");
 	$pdf->Cell(.6,.20,"Late Enrollment",0,0,"L");
 	$pdf->Cell(.6,.20,"LE",0,0,"R");
 	$pdf->Cell(2.6,.2,"Reason(Enrollment beyond 1st Friday of June)",0,0,"L");
 	$pdf->Cell(.6,.20,"Accelerated",0,0,"L");
 	$pdf->Cell(.6,.2,"ACL",0,0,"R");
 	$pdf->Cell(2.5,.2,"Specify Level & Effectivity Date",0,0,"L");
 	
 	$pdf->Cell(.6,.25,"",0,0,"L");
 	$pdf->Cell(.6,.25,"",0,0,"C");
 	$pdf->Cell(.6,.25,"",0,0,"C");
 	
 	$pdf->SetFont("Arial","U","6.5");
 	$pdf->Cell(.2,.25,"",0,0,"C");
 	$pdf->Cell(.5,.25,"BoSY Date:",0,0,"C");
 	$pdf->Cell(.5,.25,"                     ",0,0,"C");
 	$pdf->Cell(.5,.25,"EoSY Date:",0,0,"C");
 	$pdf->Cell(.5,.25,"                     ",0,0,"C");
 	$pdf->Cell(.2,.25,"",0,0,"C");
 	$pdf->Cell(.6,.25,"BoSY Date:",0,0,"C");
 	$pdf->Cell(.5,.25,"                     ",0,0,"C");
 	$pdf->Cell(.5,.25,"EoSY Date:",0,0,"C");
 	$pdf->Cell(.3,.25,"                     ",0,0,"C");
 	
 	
 	
 	
 	
	
 $pdf->Output();
?>