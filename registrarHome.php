<?php
    session_start();
    if($_SESSION['faculty_id'] == ''){ header("Location: index.php");}else if( $_SESSION['faculty_type'] != 'Head Teacher' && $_SESSION['faculty_type'] != 'Registrar' ){header("Location: javascript:");}
    include_once 'dbconnect.php';

    if(isset($_POST['showall'])){

        $query = mysqli_query($con, "SELECT * from subjectDetails");

    }
    $getCurrentSY = mysqli_query($con, "SELECT sy_year from schoolyear where sy_remarks='Open'");
    $temp = mysqli_fetch_row($getCurrentSY);
    $SY = $temp[0];
    function fillSchoolYear($con){
        $output ='';
        $query = mysqli_query($con, "SELECT * from schoolyear");
        while($row = mysqli_fetch_array($query)){
            $output .='<option value="'.$row[1].'">'.$row[1].'</option>';
        }
        return $output;
    }
?>
<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie9"><![endif]-->
    
<!-- Mirrored from byrushan.com/projects/ma/1-7-1/jquery/light/widget-templates.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 01 May 2017 06:35:54 GMT -->
<!-- HEAD -->
    <?php include_once 'head.php'; ?> 
    <!-- HEAD   -->
    <body>
        <!-- HEADER -->
        <?php include_once 'header.php'; ?>
        <!-- HEADER -->

        <section id="main">
            
            <?php 
                $toggle = 'registrarHome';
                include_once 'registrarSidebar.php'; 
            ?>
            <section id="content">
                
               
                <div class="p-10 p-b-0">

                    <div class="text-center p-t-25">

                                <!-- <img src="img/deped.png" class="pull-left"> -->
                        <h1>STUDENT INFORMATION SYSTEM</h1>
                        <br />
                    </div>

    <!-- lockscreen credentials (contains the form) -->
                    

                    <br />
    <!-- /.lockscreen credentials -->
                    <div class="row">
                        <div class="col-sm-5">
                            <div class='card'>                                  
                                <div class="card-body lcb-form">
                                    <div id="chartcontainer"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-7">
                            <div class='card'>                                  
                                <div class="card-body lcb-form">
                                    <div id="chartcontainer1"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class='card'>                                  
                                <div class="card-body lcb-form">
                                    <div id="chartcontainer2"></div>
                                </div>
                            </div>
                        </div>
                    </div>

  <!-- /.lockscreen-item -->
                  <div class="help-block text-center">
                    Select item from jump menu for quick start
                  </div>

                  <div class="lockscreen-footer text-center">
                    Copyright &copy; 2018 <b>Mindanao State University Naawan</b><br>
                    IT Services | A.R-C.G-M.Y.
                                
                  </div>
            </div>
                    
                
            </section>
        </section>

        <!-- Javascript Libraries -->
        <?php include_once 'scripts.php'; ?>
        <!-- Javascript Libraries -->
        <script type="text/javascript">
            $(document).ready(function() {
                $('#data-table-basic').DataTable();
                
$.ajax({
                    url:"ajaxJQuery/registrarAnalytics.php?report=enrolly",
                    method:"get",
                    dataType:"json",
                    success:function(data){
                        var res=[];
                        var na =[];
                        $.each(data, function(index, element) {
                            na.push(element.schoolyear);
                            res.push(parseFloat(element.Total)
                            );


                        });


                        $('#chartcontainer').highcharts({
                            chart: {
                                type: 'line'
                            },
                            title: {
                                text: 'School Enrollment'
                            },
                            xAxis: {
                              categories: na,
                              crosshair: true
                          },
                            yAxis: [{
                                className: 'highcharts-color-1',
                                title: {
                                        text: 'Number of registered learner'
                                }
                            }, {
                                    className: 'highcharts-color-2',
                                    opposite: true,
                                    
                            }],
                            plotOptions: {
                                column: {
                                    borderRadius: 5
                                },
                                line: {
                                    dataLabels: {
                                        enabled: true
                                    }
                                }
                            },
                            series: [{
                                name:'Learner',
                                data:res,
                              }]
                            

                        });
                    }
                });
               
           
                $.ajax({
                        url:"ajaxJQuery/registrarAnalytics.php?report=gender",
                        method:"get",
                        dataType:"json",
                        success:function(data){
                            var male=[];
                            var female=[];
                            var categ=[];
                            $.each(data, function(index, element) {
                                categ.push(element.schoolyear);
                                male.push(parseFloat(element.male));
                                female.push(parseFloat(element.female));

                               


                            });
                            $('#chartcontainer1').highcharts({
                              chart: {
                                  type: 'column'
                              },
                              title: {
                                  text: 'Learner by Gender'
                              },
                              
                              xAxis: {
                                  categories: categ,
                                  crosshair: true
                              },
                              yAxis: {
                                  min: 0,
                                  title: {
                                      text: 'Total number of learner'
                                  }
                              },
                              tooltip: {
                                  headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                                  pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                      '<td style="padding:0"><b>{point.y:.1f} Learner</b></td></tr>',
                                  footerFormat: '</table>',
                                  shared: true,
                                  useHTML: true
                              },
                              plotOptions: {
                                  column: {
                                      pointPadding: 0.2,
                                      borderWidth: 0,
                                      dataLabels: {
                                        enabled: true
                                    }
                                  }
                              },
                              series: [{
                                  name: 'Male',
                                  data: male

                              }, {
                                  name: 'Female',
                                  data: female
                              }]
                            });
                            
                        }
                    });
               
                
                $.ajax({
                        url:"ajaxJQuery/registrarAnalytics.php?report=status",
                        method:"get",
                        dataType:"json",
                        success:function(data){
                            var categ=[];
                            var ti=[];
                            var le=[];
                            var dod=[];
                            var to=[];
                            var pti=[];
                            var others=[];
                           
                            $.each(data, function(index, element) {
                                categ.push(element.schoolyear);
                                ti.push(parseFloat(element.ti));
                                le.push(parseFloat(element.le));
                                dod.push(parseFloat(element.do));
                                to.push(parseFloat(element.to));
                                pti.push(parseFloat(element.pti));
                                others.push(parseFloat(element.others));
                                

                            });
                            
                            $('#chartcontainer2').highcharts({
                              chart: {
                                  type: 'column'
                              },
                              title: {
                                  text: 'Report on Learners Status'
                              },
                              
                              xAxis: {
                                  categories: categ,
                                  crosshair: true
                              },
                              yAxis: {
                                  min: 0,
                                  title: {
                                      text: 'Total registered learner'
                                  }
                              },
                              tooltip: {
                                  headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                                  pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                      '<td style="padding:0"><b>{point.y:.1f} Learner</b></td></tr>',
                                  footerFormat: '</table>',
                                  shared: true,
                                  useHTML: true
                              },
                              plotOptions: {
                                  column: {
                                      pointPadding: 0.2,
                                      borderWidth: 0,
                                      dataLabels: {
                                        enabled: true
                                    }
                                  }
                              },
                              series: [
                                  {
                                      name: 'Transferred In',
                                      data: ti,

                                  }, {
                                      name: 'Transferred Out',
                                      data: to,
                                  }, {
                                      name: 'Late Enrollment',
                                      data: le,
                                  }, {
                                      name: 'DropOut',
                                      data: dod,
                                  }, {
                                      name: 'Pending T/I',
                                      data: pti,
                                  }, {
                                      name: 'Others',
                                      data: others,
                                  }
                              ]
                            });
                            
                        }
                    });
            });
        </script>
    </body>
  
<!-- Mirrored from byrushan.com/projects/ma/1-7-1/jquery/light/widget-templates.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 01 May 2017 06:35:54 GMT -->
</html>