<?php
	session_start();
	include_once 'dbconnect.php';
    include 'ajaxJQuery/globalFunction.php';
	require_once "PHPExcel-1.8.1/Classes/PHPExcel.php";

	$month = mysqli_real_escape_string($con,$_GET['month']);
	$section = mysqli_real_escape_string($con,$_GET['section']);
    $getSectionDetails = mysqli_fetch_row(mysqli_query($con, "SELECT * from classinformation where section_id ='".$section."'"));
    $level = $getSectionDetails[7];
    $sectionName = $getSectionDetails[1];

     $getMonthDetails = mysqli_fetch_row(mysqli_query($con, "SELECT * from monthdetails where month_id='".$month."'"));
        $monthID = $getMonthDetails[0];
        $month_description = $getMonthDetails[1];
        $month_totalDays = $getMonthDetails[2];
        $month_status = $getMonthDetails[3];
        $sy_id = $getMonthDetails[4];
        $sy_year = $getMonthDetails[5];


    $phpExcel = PHPExcel_IOFactory::load('School Forms Final/School Form 2.xlsx');
    $sheet = $phpExcel ->getActiveSheet();
    

	if(false)
    {
        echo "
            <script>
                alert('NO ENROLLED LEARNER');

                javascript:window.close()
            </script>
        ";
    }
    else{

        setHeaders($sheet,$con, $month, $section);
        $queryM = mysqli_query($con, "SELECT * FROM controldetails where section_id='".$section."' and Gender='Male' order by Gender DESC,Student ASC");
        $queryF = mysqli_query($con, "SELECT * FROM controldetails where section_id='".$section."' and Gender='Female' order by Gender DESC,Student ASC");
        $countLearnerM = mysqli_num_rows(mysqli_query($con, "SELECT * FROM controldetails where section_id='".$section."' and Gender='Male' order by Gender DESC,Student ASC"));

        $countLearnerF = mysqli_num_rows(mysqli_query($con, "SELECT * FROM controldetails where section_id='".$section."' and Gender='Female' order by Gender DESC,Student ASC"));
        if($countLearnerF > 2)
        {
            $sheet ->insertNewRowBefore(18,($countLearnerF -2));
        }
        if($countLearnerM > 2)
        {
        $sheet ->insertNewRowBefore(14,($countLearnerM-2));
        }
        $startMale= 13;
        $i = 1;
        while ($row = mysqli_fetch_array($queryM)) {
            $attendance = mysqli_fetch_row(mysqli_query($con, "SELECT * from attendance where control_id='".$row[0]."' and month_id='".$month."'"));
            $sheet ->setCellValue('A'.$startMale.'', $i);
            $sheet ->setCellValue('B'.$startMale.'', $row['Student']);

            $sheet ->setCellValue('AC'.$startMale.'', $attendance[1]);
            $sheet ->setCellValue('AD'.$startMale.'', $attendance[2]);
            $sheet ->setCellValue('AE'.$startMale.'', $row['control_remarks'] . ' '. $row[13]);
            $startMale++;
            $i++;
        }
        for($i = 14; $i<($startMale); $i++)
        {
            $sheet ->mergeCells('B'.$i.':C'.$i.'');
            $sheet ->mergeCells('AE'.$i.':AJ'.$i.'');
            
        }
                $startFemale1 = $startMale + 3;
        $startFemale = $startMale + 1;
        $j=1;
        while ($row = mysqli_fetch_array($queryF)) {
            $attendance = mysqli_fetch_row(mysqli_query($con, "SELECT * from attendance where control_id='".$row[0]."' and month_id='".$month."'"));
            $sheet ->setCellValue('A'.$startFemale.'', $j);
            $sheet ->setCellValue('B'.$startFemale.'', $row['Student']);
            $sheet ->setCellValue('AC'.$startFemale.'', $attendance[1]);
            $sheet ->setCellValue('AD'.$startFemale.'', $attendance[2]);
            $sheet ->setCellValue('AE'.$startFemale.'', $row['control_remarks']);
            $startFemale++;
            $j++;
        }

        for($d = $startFemale1; $d<($startFemale+1); $d++)
        {
            $sheet ->mergeCells('B'.$d.':C'.$d.'');
            $sheet ->mergeCells('AE'.$d.':AJ'.$d.'');
            
        }

        $startSummary = $startFemale + 4;
        setDatainSummaryTable($sheet,$con,$startSummary,$month,$section);
    }

    function setDatainSummaryTable($sheet,$con,$startSummary,$month,$section)
    {
        $getMonth = mysqli_fetch_row(mysqli_query($con, "SELECT * from monthdetails where month_id='".$month."'"));
        $monthDescription = $getMonth[1];   

        $sheet ->setCellValue('AB'.$startSummary.'', $getMonth[1]);
        $sheet ->setCellValue('AG'.($startSummary-1).'',$getMonth[2]);
        $startSummary++;

        //ENROLMENT AS OF 1st FRIDAY
        $row1M = mysqli_fetch_row(mysqli_query($con, "SELECT Total from Enrolmentasof1stFridayofJune where Gender = 'Male' and section_id='".$section."' and month_no IN ('5', '6')"));
        $row1F = mysqli_fetch_row(mysqli_query($con, "SELECT Total from Enrolmentasof1stFridayofJune where Gender = 'Female' and section_id='".$section."' and month_no IN ('5', '6')"));
        $unaM = ($row1M[0] == '')? 0: $row1M[0];
        $unaF = ($row1F[0] == '')? 0: $row1F[0];
        $sheet ->setCellValue('AH'.$startSummary.'', $unaM);  $sheet ->setCellValue('AI'.$startSummary.'', $unaF);  $sheet ->setCellValue('AJ'.$startSummary.'', ($unaM+$unaF));
        //LATE ENROLMENT
        $startSummary = $startSummary+2;
        $row2M = mysqli_fetch_row(mysqli_query($con, "SELECT Total from LateEnrollmentduringthemonth where Gender='Male' and section_id='".$section."' and Month='".$monthDescription."'"));
        $row2F = mysqli_fetch_row(mysqli_query($con, "SELECT Total from LateEnrollmentduringthemonth where Gender='Female' and section_id='".$section."' and Month='".$monthDescription."'"));
        $duhaM = ($row2M[0]=='')? 0:$row2M[0];
        $duhaF = ($row2F[0]=='')? 0:$row2F[0];
        $sheet ->setCellValue('AH'.$startSummary.'', $duhaM);  $sheet ->setCellValue('AI'.$startSummary.'', $duhaF);  $sheet ->setCellValue('AJ'.$startSummary.'', ($duhaM+$duhaF));
        //REGISTERED LEARNER AS OF THE MONTH

        $rowNumber10M = mysqli_fetch_row(mysqli_query($con, "SELECT * from TransferredInt where section_id='".$section."' and Gender='Male' and Month='".$monthDescription."'"));
        $rowNumber10F = mysqli_fetch_row(mysqli_query($con, "SELECT * from TransferredInt where section_id='".$section."' and Gender='Female' and Month='".$monthDescription."'"));

        $startSummary = $startSummary+2;
        $rowNumber3M = mysqli_fetch_row(mysqli_query($con, "SELECT count(*) from remarks_historydetails where Gender='Male' and section_id='".$section."' and month_desc='June'")); 
        $rowNumber3F =mysqli_fetch_row(mysqli_query($con, "SELECT count(*) from remarks_historydetails where Gender='Female' and section_id='".$section."' and month_desc='June'"));

        $get1stMonth = mysqli_fetch_row(mysqli_query($con, "SELECT min(month_id) as month_id from month where sy_id='".getSYofSection($con, $section)."'"));
        $getlastMonth = mysqli_fetch_row(mysqli_query($con, "SELECT max(month_id) as month_id from month where sy_id='".getSYofSection($con, $section)."'"));

        $totalunregisteredMale = 0;
        $totalunregisteredFemale = 0;
        $totalDeductionMale = 0;
        $totalDeductionFemale = 0;
        for($iterLoop = $month; $iterLoop >= $get1stMonth[0]; $iterLoop--)
        {
            $getDescriptionforMonth = mysqli_fetch_row(mysqli_query($con, "SELECT month_description from month where month_id='".$iterLoop."'"));
            echo '';
            $getUnregisterMale = mysqli_fetch_row(mysqli_query($con, "SELECT Total from totalunregistered where section_id='".$section."' and Gender='Male' and Month='".$getDescriptionforMonth[0]."'"));
            $getUnregisterFemale = mysqli_fetch_row(mysqli_query($con, "SELECT Total from totalunregistered where section_id='".$section."' and Gender='Female' and Month='".$getDescriptionforMonth[0]."'"));

            $totalunregisteredMale = $totalunregisteredMale + $getUnregisterMale[0];
            $totalunregisteredFemale = $totalunregisteredFemale + $getUnregisterFemale[0];
        }
        for($iterLoop1 = ($month +1) ; $iterLoop1 <= $getlastMonth[0]; $iterLoop1++)
        {
            $getDescriptionforMonth1 = mysqli_fetch_row(mysqli_query($con, "SELECT month_description from month where month_id='".$iterLoop1."'"));
            
            ///DIRI PAKO
            $getdeductionMale = mysqli_fetch_row(mysqli_query($con, "SELECT count(*) from controldetails1 where section_id='".$section."' and Gender='Male' and Month='".$getDescriptionforMonth1[0]."' and control_remarks='T/I' and control_remarks='LE'"));
            $getdeductionFemale = mysqli_fetch_row(mysqli_query($con, "SELECT count(*) from controldetails1 where section_id='".$section."' and Gender='Female' and Month='".$getDescriptionforMonth1[0]."' and control_remarks='T/I' and control_remarks='LE'"));

            $totalDeductionMale = $totalDeductionMale + $getdeductionMale[0];
            $totalDeductionFemale = $totalDeductionFemale + $getdeductionMale[0];
        }

        if($month != $get1stMonth[0])
        {
            $tuloM = $rowNumber3M[0] - $totalunregisteredMale - $totalDeductionMale + $rowNumber10M[4] + $duhaM;
            $tuloF = $rowNumber3F[0] - $totalunregisteredFemale - $totalDeductionFemale + $rowNumber10F[4] + $duhaF;
            $tulo = $tuloM + $tuloF;
        }
        else
        {
            $tuloM = $rowNumber3M[0] - $totalunregisteredMale - $totalDeductionMale;
            $tuloF = $rowNumber3F[0] - $totalunregisteredFemale - $totalDeductionFemale;
            $tulo = $tuloM + $tuloF;
        }



        $sheet ->setCellValue('AH'.$startSummary.'', $tuloM);  $sheet ->setCellValue('AI'.$startSummary.'', $tuloF);  $sheet ->setCellValue('AJ'.$startSummary.'', $tulo);

        //PERCENTAGE OF ENROLMENT AS OF THE MONTH
        $startSummary = $startSummary + 2;
        $upatM = ($tuloM / $unaM)*100;
        $upatF = ($tuloF / $unaF)*100;
        $upat = ($upatM + $upatF) /2;
        $sheet ->setCellValue('AH'.$startSummary.'', $upatM);  $sheet ->setCellValue('AI'.$startSummary.'', $upatF);  $sheet ->setCellValue('AJ'.$startSummary.'', $upat);

        //AVERAGE DAILY ATTENDANCE
        $startSummary = $startSummary + 2;
        $rowNumber5M = mysqli_fetch_row(mysqli_query($con, "SELECT * from TotalDailyAttendance where section_id='".$section."' and Gender='Male' and month_id='".$month."'"));
        $rowNumber5F = mysqli_fetch_row(mysqli_query($con, "SELECT * from TotalDailyAttendance where section_id='".$section."' and Gender='Female' and month_id='".$month."'"));
        $limaM = $rowNumber5M[4] / $getMonth[2];
        $limaF= $rowNumber5F[4] / $getMonth[2];
        $lima = $limaM + $limaF;
        $sheet ->setCellValue('AH'.$startSummary.'', $limaM);  $sheet ->setCellValue('AI'.$startSummary.'', $limaF);  $sheet ->setCellValue('AJ'.$startSummary.'', $lima);

        // PERCENTAGE OF ATTENDANCE OF THE MONTH
        $startSummary = $startSummary + 2;
        $unomM = 0;
        $unomF = 0;
        if($tuloM != 0 && $tuloF != 0){
            $unomM = ($limaM / $tuloM) * 100;
            $unomF = ($limaF / $tuloF) * 100;
        }
        $unom = ($unomM + $unomF)/2;
        $sheet ->setCellValue('AH'.$startSummary.'', $unomM);  $sheet ->setCellValue('AI'.$startSummary.'', $unomF);  $sheet ->setCellValue('AJ'.$startSummary.'', $unom);

        // Consecutive Absent
        $startSummary = $startSummary + 2;
        $rowNumber7M = mysqli_fetch_row(mysqli_query($con, "SELECT * from NumberofStudentsAbsentfor5ConsecutiveDays where section_id='".$section."' and Gender='Male' and month_id='".$month."'"));
        $rowNumber7F = mysqli_fetch_row(mysqli_query($con, "SELECT * from NumberofStudentsAbsentfor5ConsecutiveDays where section_id='".$section."' and Gender='Female' and month_id='".$month."'"));
        $pitoM = ($rowNumber7M[4] == '')? 0: $rowNumber7M[4];
        $pitoF = ($rowNumber7F[4] == '')? 0: $rowNumber7F[4];
        $pito = $pitoM + $pitoF;
        $sheet ->setCellValue('AH'.$startSummary.'', $pitoM);  $sheet ->setCellValue('AI'.$startSummary.'', $pitoF);  $sheet ->setCellValue('AJ'.$startSummary.'', $pito);

      
        //DROP OUT
        $startSummary = $startSummary + 2;
        setOthers($sheet,$con,$month,$section,$startSummary);
        // $rowNumber8M = mysqli_fetch_row(mysqli_query($con, "SELECT * from dropout where section_id='".$section."' and Gender='Male' and Month='".$month."'"));
        // $rowNumber8F = mysqli_fetch_row(mysqli_query($con, "SELECT * from dropout where section_id='".$section."' and Gender='Female' and Month='".$month."'"));
        // $totalrowNumber8 = $rowNumber8M[4] + $rowNumber8F[4];
        // $sheet ->setCellValue('AH'.$startSummary.'', ($rowNumber8M[4]=='')? 0: $rowNumber8M[4]);  $sheet ->setCellValue('AI'.$startSummary.'', ($rowNumber8F[4]=='')? 0: $rowNumber8F[4]);  $sheet ->setCellValue('AJ'.$startSummary.'', ($rowNumber8M=='')? 0: $rowNumber8M);

        // //TRANSFERED OUT
        // $startSummary = $startSummary + 2;
        // $rowNumber9M = mysqli_fetch_row(mysqli_query($con, "SELECT * from TransferredOut where section_id='".$section."' and Gender='Male' and Month='".$month."'"));
        // $rowNumber9F = mysqli_fetch_row(mysqli_query($con, "SELECT * from TransferredOut where section_id='".$section."' and Gender='Female' and Month='".$month."'"));
        // $totalrowNumber9 = $rowNumber9M[4] + $rowNumber9F[4];
        // $sheet ->setCellValue('AH'.$startSummary.'', ($rowNumber9M[4]=='')? 0: $rowNumber9M[4]);  $sheet ->setCellValue('AI'.$startSummary.'', ($rowNumber9F[4]=='')? 0: $rowNumber9F[4]);  $sheet ->setCellValue('AJ'.$startSummary.'', ($rowNumber9M=='')? 0: $rowNumber9M);

        // // TRANSFERED IN
        // $startSummary = $startSummary + 2;
        // $rowNumber10M = mysqli_fetch_row(mysqli_query($con, "SELECT * from TransferredInt where section_id='".$section."' and Gender='Male' and Month='".$month."'"));
        // $rowNumber10F = mysqli_fetch_row(mysqli_query($con, "SELECT * from TransferredInt where section_id='".$section."' and Gender='Female' and Month='".$month."'"));
        // $totalrowNumber10 = $rowNumber10M[4] + $rowNumber10F[4];
        //  $sheet ->setCellValue('AH'.$startSummary.'', ($rowNumber10M[4]=='')? 0: $rowNumber10M[4]);  $sheet ->setCellValue('AI'.$startSummary.'', ($rowNumber10F[4]=='')? 0: $rowNumber10F[4]);  $sheet ->setCellValue('AJ'.$startSummary.'', ($rowNumber10M=='')? 0: $rowNumber10M);


         $startSummary = $startSummary+5;
         getAdviser($con,$section,$startSummary,$sheet);
         $startSummary = $startSummary+4;
         getPrincipal($con,$startSummary,$sheet);
    }

    function setOthers($sheet,$con,$month,$section,$startSummary)
    {

       
        if($row = mysqli_fetch_array(mysqli_query($con, "SELECT * from monthdetails where month_id='".$month."'")))
        {
        $rowNumber8M = mysqli_fetch_row(mysqli_query($con, "SELECT * from dropout where section_id='".$section."' and Gender='Male' and Month='".$row['month_description']."'"));
        $rowNumber8F = mysqli_fetch_row(mysqli_query($con, "SELECT * from dropout where section_id='".$section."' and Gender='Female' and Month='".$row['month_description']."'"));
        $totalrowNumber8 = $rowNumber8M[4] + $rowNumber8F[4];
        $sheet ->setCellValue('AH'.$startSummary.'', ($rowNumber8M[4]=='')? 0: $rowNumber8M[4]);  $sheet ->setCellValue('AI'.$startSummary.'', ($rowNumber8F[4]=='')? 0: $rowNumber8F[4]);  $sheet ->setCellValue('AJ'.$startSummary.'', ($totalrowNumber8=='')? 0: $totalrowNumber8);

        //TRANSFERED OUT
        $startSummary = $startSummary + 2;
        $rowNumber9M = mysqli_fetch_row(mysqli_query($con, "SELECT * from TransferredOut where section_id='".$section."' and Gender='Male' and Month='".$row['month_description']."'"));
        $rowNumber9F = mysqli_fetch_row(mysqli_query($con, "SELECT * from TransferredOut where section_id='".$section."' and Gender='Female' and Month='".$row['month_description']."'"));
        $totalrowNumber9 = $rowNumber9M[4] + $rowNumber9F[4];
        $sheet ->setCellValue('AH'.$startSummary.'', ($rowNumber9M[4]=='')? 0: $rowNumber9M[4]);  $sheet ->setCellValue('AI'.$startSummary.'', ($rowNumber9F[4]=='')? 0: $rowNumber9F[4]);  $sheet ->setCellValue('AJ'.$startSummary.'', ($totalrowNumber9=='')? 0: $totalrowNumber9);

        // TRANSFERED IN
        $startSummary = $startSummary + 2;
        $rowNumber10M = mysqli_fetch_row(mysqli_query($con, "SELECT * from TransferredInt where section_id='".$section."' and Gender='Male' and Month='".$row['month_description']."'"));
        $rowNumber10F = mysqli_fetch_row(mysqli_query($con, "SELECT * from TransferredInt where section_id='".$section."' and Gender='Female' and Month='".$row['month_description']."'"));
        $totalrowNumber10 = $rowNumber10M[4] + $rowNumber10F[4];
         $sheet ->setCellValue('AH'.$startSummary.'', ($rowNumber10M[4]=='')? 0: $rowNumber10M[4]);  $sheet ->setCellValue('AI'.$startSummary.'', ($rowNumber10F[4]=='')? 0: $rowNumber10F[4]);  $sheet ->setCellValue('AJ'.$startSummary.'', ($totalrowNumber10=='')? 0: $totalrowNumber10);
         }
    }

    function setHeaders($sheet,$con, $month, $section)
    {
    	$getMonth = mysqli_fetch_row(mysqli_query($con, "SELECT * from monthdetails where month_id='".$month."'"));
    	$getSection = mysqli_fetch_row(mysqli_query($con, "SELECT * from classinformation where section_id='".$section."'"));

        $sheet ->setCellValue('K6', $getMonth[5]);
        $sheet ->setCellValue('X6', $getMonth[1]);
        $sheet ->setCellValue('X8', $getSection[7]);
        $sheet ->setCellValue('AC8', $getSection[1]);
    }
    function getAdviser($con,$section,$startSummary,$sheet)
    {
        $getSection = mysqli_fetch_row(mysqli_query($con, "SELECT * from classinformation where section_id='".$section."'"));
        $sheet ->setCellValue('AD'.$startSummary.'', $getSection[8]);

    }
    function getPrincipal($con,$startSummary,$sheet)
    {
        $query = mysqli_query($con, "SELECT * from facultysmalldetail where faculty_type='Principal'");
        if(mysqli_num_rows($query) == 0)
        {
            $sheet ->setCellValue('AD'.$startSummary.'', '');
        }
        else
        {
            $temp = mysqli_fetch_row($query);
            $sheet ->setCellValue('AD'.$startSummary.'', $temp[1]);
        }
    }

    $writer = PHPExcel_IOFactory::createWriter($phpExcel, "Excel2007");
        //
        // $writer->save('C:/Users/MLD/Documents/School Form 1-'.$sectionName.'-'.$SY.'.xlsx'); 
    $fileName = "";
    switch ($level) {
        case 'Grade 7':
            $fileName = "SF2_".$sy_year."_".$level." (Year I) - ".$sectionName;
            break;
        case 'Grade 8':
            $fileName = "SF2_".$sy_year."_".$level." (Year II) - ".$sectionName;
            break;
        case 'Grade 9':
            $fileName = "SF2_".$sy_year."_".$level." (Year III) - ".$sectionName;
            break;
        case 'Grade 10':
            $fileName = "SF2_".$sy_year."_".$level." (Year IV) - ".$sectionName;
            break;
        default:
            # code...
            break;
        }
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$fileName.'.xlsx"');
        header('Cache-Control: max-age=0');
        $writer->save('php://output'); 
    

        
        //header("Location: index.php");
    
?>