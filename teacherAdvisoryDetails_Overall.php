<div role="tabpanel" class="tab-pane active" id="monitor">
    
    <div class="card-body card-padding">
        <?php
        echo '
        <div class="visitors-stats clearfix">
            <center>
            <div class="col-xs-6">
                <div class="visitors-stats-item">
                    <strong class="c-black"><h1>'.$items[11].'</h1></strong>
                    <h4 class="c-gray">MALE</h4>
                </div>
            </div>
            <div class="col-xs-6">
                <div class="visitors-stats-item">
                    <strong class="c-black"><h1>'.$items[12].'</h1></strong>
                    <h4 class="c-gray">FEMALE</h4>
                </div>
            </div>
            </center>
        </div>
        ';
        ?>

        <div class="row">
            <div class="col-sm-6">
                <div class='card'>                                  
                    <div class="card-body lcb-form">
                        <div id="chartcontainer1"></div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class='card'>                                  
                    <div class="card-body lcb-form">
                        <div id="chartcontainer2"></div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class='card'>                                  
                    <div class="card-body lcb-form">
                        <div id="chartcontainer3"></div>
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
        <?php 
            
            $getQuarters = mysqli_query($con,"SELECT * from quarters where sy_id='".getSYofSection1($con, $id)."'");

            while ($row = mysqli_fetch_array($getQuarters)) {
                $getaverageperquarterdetails = mysqli_query($con, "SELECT * from averageperquarterdetails where quarters_id='".$row[0]."' and section_id='".$id."' order by average desc limit 20");
                echo '

                    <div class="col-sm-3">
                        <div class="card">     
                            <div class="card-header">
                                <br />
                                <h2>'.$row[1].'<small>Top 20 Learner</small></h2>
                                <div class="actions">
                                    <button class="btn btn-danger" onclick=window.open("rankingReport.php?type=quarter&sID='.$id.'&qID='.$row[0].'")><i class="zmdi zmdi-collection-pdf"></i> PRINT</button>
                                </div>

                            </div>        
                            <div class="card-body card-padding lcb-form">
                                <div>
                                   
                                       

                                            <ol>
                ';
                while ($item = mysqli_fetch_array($getaverageperquarterdetails)) {
                echo '
                                    
                                        <li>'.$item['Name'].' - <b>'.$item[0].'</b></li>
                                   
                                ';
                }
                echo '
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                ';

            }


        ?>
        </div>  
    </div>    


</div>