<?php
    include_once 'sessionTeacher.php';
    include_once 'dbconnect.php';
    function fillSchoolYear($con){
        $output ='';
        $query = mysqli_query($con, "SELECT * from schoolyear order by sy_id DESC");
        if(check($con))
        {

        }
        else
        {
            $output .='<option disabled="disabled" selected>NO CURRENT SY</option>';
        }
        while($row = mysqli_fetch_array($query)){
            if($row[4] == 'Open'){
                $output .='<option value="'.$row[1].'">'.$row[1].' - Current</option>';
            }else
            {
                $output .='<option value="'.$row[1].'">'.$row[1].'</option>';
            }
            
        }
        return $output;
    }
    function check($con)
    {
        $query = mysqli_num_rows(mysqli_query($con, "SELECT * from schoolyear where sy_remarks='Open'"));
        if($query > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
?>
<!DOCTYPE html>
    <!-- HEAD -->
    <?php include_once 'head.php'; ?> 
    <!-- HEAD   -->
    <body>
        <!-- HEADER -->
        <?php include_once 'header.php'; ?>
        <!-- HEADER -->

        <section id="main">
            <ol class="breadcrumb">
                <li><a href="teacherHome.php">Home</a></li>
                <li class="active">Classes</li>
            </ol>
            <?php 
                $toggle = 'teacherClass';
                include_once 'sidebarTeacher.php'; 
            ?>

            

            <section id="content">

                <div class="container">
                    <!-- Colored Headers -->
                    <div class="block-header">

                        <h1><i class="zmdi zmdi-calendar-note"></i> Classes

                        </h1>
                        
                        <div class="actions">
                            <div class="btn-group">
                                <?php 
                                    if(fillSchoolYear($con) == '')
                                    {

                                    }else
                                    {
                                        echo ' <select class="btn btn-group btn-lg" id="sy" name="sy" data-toggle="tooltip" data-placement="top" title="" data-original-title="Choose a specific school year for the list of subject handled">';
                                         echo fillSchoolYear($con);
                                    }
                                ?>
                                </select>
                            </div>
                            
                             

                        </div>
                        
                        
                    </div>

                    
                    <div class="row" id='sectionList'>

                        
                            
                            <?php
                            $getCurrentSY = mysqli_query($con, "SELECT sy_year from schoolyear where sy_remarks='Open'");
                            $temp = mysqli_fetch_row($getCurrentSY);
                            $SY = $temp[0];
                                $query = mysqli_query($con, "SELECT * from subjectofferingdetails where faculty_id='".$_SESSION['faculty_id']."' and schoolyear='".$SY."'");
                                while($row = mysqli_fetch_array($query)){
                                    $id = $row[0];
                                    $start = $row[1];
                                    $end = $row[2];
                                    $days = $row[3];
                                    $subjTitle = $row[6];
                                    $subjDescription = $row[7];
                                    $subjID= $row[14];
                                    $Teacher = $row[8];
                                    $TeacherID = $row[9];
                                    $SecID = $row[10];
                                    $Section = $row[11];
                                    $level = $row[12];
                                    $sy = $row[13];

                                    echo "
                                <div class='col-sm-4'>
                                    <div class='card'>
                                        <div class='card-header'><br />
                                            <h2>".$subjTitle. ": " .$subjDescription."</h2>
                                            <h3>".$Section."</h3>
                                            <div class='actions'>
                                                <a href='teacherClassDetails.php?id=".$id."&subjid=".$subjID."&fid=".$TeacherID."&sid=".$SecID."&sname=".$Section."&subjTitle=".$subjTitle."&subjDesc=".$subjDescription."'>
                                                    <button type='submit' name='edit' id='".$id."' class='btn btn-default waves-effect enrol_student' data-toggle='tooltip' data-placement='bottom' title='' data-original-title='More details for this class'><i class='zmdi zmdi-arrow-forward'></i>
                                                    </button>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>";
                                }
                            ?>
                        
                    </div>




                    <br/>
                    <br/>
                </div>
            </section>
        </section>
        
        <!-- FOOTER -->
        <?php include_once 'footer.php' ?>
        <!-- FOOTER -->

        <!-- Javascript Libraries -->
        <?php include_once 'scripts.php'; ?>
        <!-- Javascript Libraries -->


        <script type="text/javascript">
            $(document).ready(function(){
                $('#sy').change(function(){
                    var sys = $(this).val();
                    $.ajax({
                        url:"ajaxJQuery/loadSubjectOffering.php",
                        method:"POST",
                        data:{sys:sys},
                        success:function(data){
                            $('#sectionList').html(data);
                        }
                    });
                });



                $(document).on('click', '.edit_data', function(){
                    var section_id = $(this).attr("id");
                    $.ajax({
                        url:"ajaxJQuery/selectSection.php",
                        method:"POST",
                        data:{section_id:section_id},
                        dataType:"json",
                        success:function(data){
                            $('#section_title1').val(data.section_name);
                            $('#section_capacity1').val(data.section_capacity);
                            $('#section_maxGrade1').val(data.section_maxGrade);
                            $('#section_minGrade1').val(data.section_minGrade);
                            $('#section_adviser1').val(data.Adviser);



                            
                            $('#year_lvl_title1').val(data.GradeLevel);
                             $('#section_id').val(data.section_id);
                            $('#modalColor1').modal('show');
                        }

                    });
                });
                $(document).on('click', '.enrol_student', function(){
                    var section_id = $(this).attr("id");
                    $.ajax({
                        
                        success:function(data){
                            
                             $('#section_id1').val(data.section_id);
                            $('#modalColor2').modal('show');
                        }

                    });
                });

                $('#myForm').on("submit", function(event){
                    event.preventDefault();
                    if($('#section_title').val() == '')  
                    {  
                         alert("Title is required");  
                    }  
                    else if($('#section_capacity').val() == '')  
                    {  
                         alert("Capacity is required");  
                    }  
                    else if($('#section_maxGrade').val() == '')  
                    {  
                         alert("Maximun grade is required");  
                    }  
                    else if($('#section_minGrade').val() == '')  
                    {  
                         alert("Minimum grade is required");  
                    }
                    else if($('#section_adviser').val() == 'ADVISER')  
                    {  
                         alert("You must select adviser");  
                    }
                    else if($('#year_lvl_title').val() == 'LEVEL')  
                    {  
                         alert("You must select year level");  
                    }  
                    else  
                    {  
                         $.ajax({  
                              url:"ajaxJQuery/insertSection.php",  
                              method:"POST",  
                              data:$('#myForm').serialize(),  
                              beforeSend:function(){  
                                   $('#insert1').val("Inserting..");   
                              },  
                              success:function(data){ 
                                   swal("Good job", "You successfully manage the student", "success");  
                                   $('#myForm')[0].reset();  
                                   
                                   $('#sectionList').html(data); 
                              }  

                         });  
                    }
                });
            });



        </script>
    </body>
  
<!-- Mirrored from byrushan.com/projects/ma/1-7-1/jquery/light/widget-templates.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 01 May 2017 06:35:54 GMT -->
</html>