<div role="tabpanel" class="tab-pane" id="home11">
    <div id="listofStudents">

        <div>

                <div class="btn-demo actions row">

                    <?php echo "
                    <button class='btn btn-default btn-lg' onclick=window.open('SF5-Excel.php?id=".$id."') data-toggle='tooltip' data-placement='top' title='' data-original-title='Generate School Form 5 (Report on Promotion & Level of Proficiency) in excel file'><i class='zmdi zmdi-download'> </i> <b> School Form 5</b></button>
                    

                    ";
                    ?>
                </div>
        </div>
        <br />
        <div class="card">
            <div class="table-responsive">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                      <th rowspan="2" class="text-center" width="5%"><small><small><b>Action</b></small></small></th>
                      <th rowspan="2" class="text-center" width="5%"><b>LRN</b></th>
                      <th rowspan="2" class="text-center" width="25%"><b>Name</b></th>
                      <th colspan="4" class="text-center" width="19%"><b>Average Grade</b></th>
                      <th rowspan="2" class="text-center"><b>Final grade</b></th>
                      <th rowspan="2" class="text-center" width="22%"><b>Remarks</b></th>

                    </tr>

                    <tr>
                      <th class="text-center"><small><small><small>1st Quarter</small></small></small></th>
                      <th class="text-center"><small><small><small>2nd Quarter</small></small></small></th>
                      <th class="text-center"><small><small><small>3rd Quarter</small></small></small></th>
                      <th class="text-center"><small><small><small>4th Quarter</small></small></small></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $syQuery = mysqli_query($con, "SELECT * from section where section_id='".$id."'");
                        if($asd = mysqli_fetch_array($syQuery))
                        {
                            $SY=  $asd['sy_id'];
                        }

                        $iter = 0;
                        $qQuery = mysqli_query($con, "SELECT * from quarters where sy_id='".$SY."'");
                        $countQuarters = mysqli_num_rows(mysqli_query($con, "SELECT * from quarters where sy_id='".$SY."'"));

                        while($row1 = mysqli_fetch_array($qQuery))
                        {
                            $qID[$iter] = $row1[0];
                            $iter++;
                        }


                        $query = mysqli_query($con, "SELECT * FROM controldetails where section_id='".$id."' order by Gender DESC,Student ASC");
                        while($row = mysqli_fetch_array($query)){
                            $temp = $row[0];


                            $countGrades = mysqli_num_rows(mysqli_query($con, "SELECT * from averageperquarter where control_id='".$temp."'"));
                            if($row[1] == 'T/O' | $row[1] == 'DRP')
                            {
                                echo "
                                <tr class='c-red'>
                                ";
                            }
                            else
                            {
                                echo "
                                <tr>
                                ";
                            }

                            echo "
                                <td>
                                    <button type='submit' name='details' id=".$temp." class='btn btn-default btn-sm grade_details'><i class='zmdi zmdi-info'></i></button>

                                </td>
                                <td>".$row[7]."</td>
                                <td>".$row[5]."</td>
                                ";
                                if($countQuarters > 0)
                                {
                                    $arrlength = count($qID);
                                    for($i = 0; $i < $arrlength; $i++){
                                        $q = mysqli_query($con, "SELECT round(average,4) from averageperquarter where control_id='".$temp."' and quarters_id='".$qID[$i]."'");
                                        if ($rows = mysqli_fetch_array($q)) {
                                            if($rows[0] < 75)
                                            {
                                                echo "<td class='text-center bgm-red c-white'>".$rows[0]."</td>";
                                            }
                                            else
                                            {
                                                echo "<td class='text-center'>".$rows[0]."</td>";
                                            }
                                        }else{
                                            echo "<td class='text-center'></td>";
                                        }
                                    }
                                    if($arrlength == 1){
                                        echo "<td></td><td></td><td></td>";
                                    }else if($arrlength==2){
                                        echo "<td></td><td></td>";
                                    }else if($arrlength == 3){
                                        echo "<td></td>";
                                    }
                                }
                                else
                                {
                                        echo "<td></td><td></td><td></td><td></td>";
                                }
                                $getAVERAGE = mysqli_query($con, "SELECT round(average,3) from AVERAGE where control_id='".$temp."'");
                                $tempAverage = mysqli_fetch_row($getAVERAGE);
                                echo "<td class='text-center'><b><h4>".$tempAverage[0]."</h4></b></td>
                                <td class='text-center'>".$row[1]." - ".$row[13]."</td>

                            </tr>
                            ";

                        }

                    ?>
                </tbody>
            </table>
            </div>
        </div>
    </div>
</div>
