<?php
session_start();
    include_once 'dbconnect.php';

    // if($_SESSION['type'] != 'Learner')
    // {
    //     header("Location: ../index.php");
    // }

    if(isset($_POST['login'])){
        $username = mysqli_real_escape_string($con, $_POST['username']);
        $password = mysqli_real_escape_string($con, $_POST['password']);   


        $query = "CALL loginStudent('".$username."','".$password."')";
        $result = mysqli_query($con, $query);
        if($row = mysqli_fetch_array($result)){
            $_SESSION['student_id'] = $row['student_id'];
            $_SESSION['Name'] = $row['Name'];
            $_SESSION['student_pass'] = $row['student_pass'];
            $_SESSION['type'] ='Learner';

            header("Location: studentHome.php");


        }else{
            echo "<script> 
                alert('Invalid username and password'); 
            </script>";
        }

    }
?>


<!DOCTYPE html>
<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1"><link rel="shortcut icon" type="image/x-icon" href="../img/lnhs.ico" alt="logo" />
        <title>LNHS | HOME</title>

        <!-- Vendor CSS -->
        <link href="../vendors/bower_components/animate.css/animate.min.css" rel="stylesheet">
         <link href="../vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css" rel="stylesheet">
        
        <!-- CSS -->
        <link href="../css/app_1.min.css" rel="stylesheet">
        <link href="../css/app_2.min.css" rel="stylesheet">
       <style>
            #bg {
              position: fixed; 
              top: -50%; 
              left: -50%; 
              width: 200%; 
              height: 200%;
              background-size: cover;

            }
            #bg img {
              position: absolute; 
              top: 0; 
              left: 0; 
              right: 0; 
              bottom: 0; 
              margin: auto; 
              min-width: 50%;
              min-height: 50%;
              max-width: 50%;
              max-height: 50%;
            }
        </style>
</head>

<body>
    
    <div id="bg">
        <img src="../assets/css/images/asdsdasdsadas.jpg" alt="">
    </div>
   <div class="login-content">
        
        <div class="lc-block lc-block-alt toggled" id="l-lockscreen">
            <div class="lcb-form" style="filter:alpha(opacity=75); opacity:1;">
                <a href="../index.php"><img class="lcb-user pull-right" src="../img/lnhs.ico" alt=""></a>
                <h3 class="c-black"><b>Learner Account</b></h3>
                <p>Online Portal</p>
                <form id="myForm" method="POST" action="studentLogin.php">
                    <div class="input-group m-b-20">
                        <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
                        <div class="fg-line">
                            <input type="text" class="form-control input-lg" style="font-size: calc(.5vw + 1.8vh);" name="username" id="username" placeholder="Username" value='127926090009' required>
                        </div>
                    </div>

                    <div class="input-group m-b-20">
                        <span class="input-group-addon"><i class="zmdi zmdi-male"></i></span>
                        <div class="fg-line">
                            <input type="password" class="form-control input-lg" style="font-size: calc(.5vw + 1.8vh);" name="password" name="password" placeholder="Password" value='127926090009' required>
                        </div>
                    </div>

                    

                    
                    <button type="submit" class="btn btn-login bgm-green btn-float" name="login" id="login"><i class="zmdi zmdi-arrow-forward"></i></button>
                </form>
                <span class="text-danger" id="result"><?php if (isset($checkmsg)) { echo $checkmsg; } ?></span>
                <span class="text-danger" id="result"><?php if (isset($errormsg)) { echo $errormsg; } ?></span>
            </div>
        </div>
         
    </div>
    </div>
    </div>
</div>

    <!-- Javascript Libraries -->
    <script src="../vendors/bower_components/jquery/dist/jquery.min.js"></script>
    <script src="../vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <script src="../vendors/bower_components/Waves/dist/waves.min.js"></script>

    <!-- Placeholder for IE9 -->
    <!--[if IE 9 ]>
        <script src="vendors/bower_components/jquery-placeholder/jquery.placeholder.min.js"></script>
    <![endif]-->

    <script src="../js/app.min.js"></script>
    <script>
        window.onload = function() { document.body.className = ''; }
        window.ontouchmove = function() { return false; }
        window.onorientationchange = function() { document.body.scrollTop = 0; }
    </script>
    
</body>
</html>