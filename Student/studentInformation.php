<?php
    session_start();

    if($_SESSION['student_id'] == ''){ 
        header("Location: ../index.php");
    }
    include_once 'dbconnect.php';

    $studentID =mysqli_real_escape_string($con, $_SESSION['student_id']);

//CALL selectstudentinfo('".$studentID."')
    if($row = mysqli_fetch_array(mysqli_query($con, "SELECT * from studentinfo1 where student_id = '".$studentID."'"))){
        $cid = $row[0];
        $id=$row[1];
        $LRN=$row[2];
        $name=$row[3];
        $sex=$row[4];
        $DOB=$row[5];
        $age=$row[6];
        $birthPlace=$row[7];
        $motherTongue=$row[8];
        $IP=$row[9];
        $religion=$row[10];
        $syid=$row[11];
        $sy_year=$row[12];
        $controlremarks=$row[13];
        $sectionID=$row[14];
        $addressHSSP=$row[15];
        $addressBarangay=$row[16];
        $addressMunicipality=$row[17];
        $addressProvince=$row[18];
        $guardian=$row[19];
        $relationship=$row[20];
        $gcontact=$row[21];
        $pcontact=$row[22];
        $controlDateCreated=$row[23];
        $controlDateUpdated=$row[24];
        $father = $row['Father'];
        $mother = $row['Mother'];
    }
    if($row1 = mysqli_fetch_array(mysqli_query($con, "SELECT * from student_account where student_id='".$studentID."'")))
    {
        $pic = $row1['student_picture'];
    }
    
?>
<!DOCTYPE html>
<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1"><link rel="shortcut icon" type="image/x-icon" href="../img/lnhs.ico" alt="logo" />
        <title>Student | Profile</title>

        <link href="../vendors/bower_components/animate.css/animate.min.css" rel="stylesheet">
        <link href="../vendors/bower_components/sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
        <link href="../vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css" rel="stylesheet">
        <link href="../vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet">
        <link href="../vendors/bower_components/dropzone/dist/min/dropzone.min.css" rel="stylesheet">
        <link href="../vendors/bower_compon../ents/chosen/chosen.css" rel="stylesheet">
        <link href="../css/app_1.min.css" rel="stylesheet">
        <link href="../css/app_2.min.css" rel="stylesheet">
        <link href="../css/demo.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="../css/pace.min.css">

    </head>
    <body>
        <header id="header" class="clearfix" data-ma-theme="green">
            <ul class="h-inner">
                <li class="hi-trigger ma-trigger" data-ma-action="sidebar-open" data-ma-target="#sidebar">
                    <div class="line-wrap">
                        <div class="line top"></div>
                        <div class="line center"></div>
                        <div class="line bottom"></div>
                    </div>
                </li>

                <li class="hi-logo hidden-xs">
                    <a href="index.html">Lugait National High School</a>
                </li>

                <li class="pull-right">
                    <ul class="hi-menu">
                        <li class="dropdown">
                            <a data-toggle="dropdown" href="#"><i class="him-icon zmdi zmdi-caret-down"></i></a>
                                <ul class="dropdown-menu dm-icon pull-right">
                       
                                    <li>
                                        <a href="studentinformation.php"><i class="zmdi zmdi-pin-account"></i> Account</a>
                                    </li>
                                    <li>
                                        <a href="studentChangePassword.php"><i class="zmdi zmdi-lock"></i> Privacy Settings</a>
                                    </li>
                                    <li>
                                        <a href="logout.php"><i class="zmdi zmdi-power"></i>Logout</a>
                                    </li>
                                </ul>
                        </li>
                        
                    </ul>
                </li>
            </ul>

            <!-- Top Search Content -->
            <div class="h-search-wrap">
                <div class="hsw-inner">
                    <i class="hsw-close zmdi zmdi-arrow-left" data-ma-action="search-close"></i>
                    <input type="text">
                </div>
            </div>
        </header>

        <section id="main">
            <aside id="sidebar" class="sidebar c-overflow">
                <div class="s-profile">
                    <a>
                        <div class="sp-pic">
                            <?php
                                if($pic == '')
                                {
                                    echo "<img src='../img/default-profile.png' alt=''>";

                                }else
                                {
                                    echo "<img src='../profile/".$pic."' alt=''>";

                                }

                            ?>
                        </div>
                        <div class="sp-info">
                            <?php echo $_SESSION['Name']; ?>
                        </div>
                    </a>
                </div>
                <ul class="main-menu">
                    <li>
                        <a href="studentHome.php"><i class="zmdi zmdi-home"></i> Home</a>
                    </li>
                    <li class="active">
                        <a href="studentInformation.php"><i class="zmdi zmdi-account"></i> View Performance and Personal Info</a>
                    </li>
                    <li>
                        <a href="studentChangePassword.php"><i class="zmdi zmdi-lock"></i> Change Password</a>
                    </li>
                    <li>
                        <a href="logout.php"><i class="zmdi zmdi-power"></i> Logout</a>
                    </li>
                </ul>
            </aside>
            <section id="content">

                <div class="container">

                    <div class="block-header">
                        

                        
                    </div>
                        <div class="row" id='sectionList'>

                            <div class="col-sm-12">
                                <div class='card'>
                                    
                                    <div class="card-body">
                                        <div class="card" id="profile-main">
                                            <div class="pm-overview c-overflow">
                                                
                                                <div class="pmo-pic">
                                                    <div class="p-relative">
                                                        <a href="#">
                                                            
                                                            <?php
                                                                if($pic == '')
                                                                {
                                                                    echo "<img class='img-responsive' src='../img/default-profile.png' alt=''>";

                                                                }else
                                                                {
                                                                    echo "<img class='img-responsive' src='../profile/".$pic."' alt=''>";

                                                                }

                                                            ?>
                                                        </a>

                                                        
                                                    </div>


                                                        
                                                </div>

                                                <div class="pmo-block pmo-contact hidden-xs">
                                                    <h2 class="text-center"><?php echo $name?></h2>
                                                    <?php
                                                    echo "<ul class='text'>
                                                        <li>    
                                                            <i class='zmdi zmdi-account'></i>
                                                            ".$LRN."
                                                        </li>
                                                        <li>
                                                            <i class='zmdi zmdi-pin'></i>
                                                            <address class='m-b-0 ng-binding'>
                                                                ".$addressHSSP.' '.$addressBarangay.', '.$addressMunicipality.', '.$addressProvince."
                                                            </address>
                                                        </li>
                                                        
                                                    </ul>";
                                                    ?>
                                                </div>

                                            
                                            </div>

                                            <div class="pm-body clearfix">
                                                <div role="tabpanel">
                                                    <ul class="tab-nav tn-justified" role="tablist"  data-tab-color="green">
                                                        <li><a href="#acount" aria-controls="acount" role="tab"
                                                          data-toggle="tab"><b>Account</b></a></li>
                                                        <li class="active"><a href="#class" aria-controls="class" role="tab" data-toggle="tab"><b>Report Cards</b></a></li>
                                                      
                                                        <li><a href="#advisory" aria-controls="advisory" role="tab" data-toggle="tab"><b>Books Acquired</b></a></li>
                                                       
                                                    </ul>
                                                    <div class="tab-content">
                                                        <div role="tabpanel" class="tab-pane" id="acount">
                                                            <div class="pmb-block">
                                                                <div class="pmbb-header">
                                                                    <h2><i class="zmdi zmdi-account m-r-10"></i> <b>Basic Information</b></h2>
                                                                </div>
                                                                <div class="pmbb-body p-l-30">
                                                                    <div class="pmbb-view">
                                                                        <?php
                                                                        echo "
                                                                        <dl class='dl-horizontal'>
                                                                            <dt>LRN</dt>
                                                                            <dd>".$LRN."</dd>
                                                                        </dl>
                                                                        <dl class='dl-horizontal'>
                                                                            <dt>Full Name</dt>
                                                                            <dd>".$name."</dd>
                                                                        </dl>
                                                                        <dl class='dl-horizontal'>
                                                                            <dt>Sex</dt>
                                                                            <dd>".$sex."</dd>
                                                                        </dl>
                                                                        <dl class='dl-horizontal'>
                                                                            <dt>Birth Date</dt>
                                                                            <dd>".$DOB."</dd>
                                                                        </dl>
                                                                          
                                                                        <dl class='dl-horizontal'>
                                                                            <dt>Age</dt>
                                                                            <dd>".$age."</dd>
                                                                        </dl>
                                                                        <dl class='dl-horizontal'>
                                                                            <dt>Birth Place</dt>
                                                                            <dd>".$birthPlace."</dd>
                                                                        </dl> 
                                                                        <dl class='dl-horizontal'>
                                                                            <dt>Mother Tongue</dt>
                                                                            <dd>".$motherTongue."</dd>
                                                                        </dl>
                                                                        <dl class='dl-horizontal'>
                                                                            <dt>Ethnicity</dt>
                                                                            <dd>".$IP."</dd>
                                                                        </dl>
                                                                        <dl class='dl-horizontal'>
                                                                            <dt>Religion</dt>
                                                                            <dd>".$religion."</dd>
                                                                        </dl>
                                                                        <dl class='dl-horizontal'>
                                                                            <dt>School Year</dt>
                                                                            <dd>".$sy_year."</dd>
                                                                        </dl>
                                                                        ";
                                                                        ?>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="pmb-block">
                                                                <div class="pmbb-header">
                                                                    <h2><i class="zmdi zmdi-phone m-r-10"></i> <b>Contact Information</b></h2>
                                                                </div>
                                                                <div class="pmbb-body p-l-30">
                                                                    <div class="pmbb-view">
                                                                        <dl class="dl-horizontal">
                                                                            <dt>Mobile Phone</dt>
                                                                            <dd><?php echo $pcontact; ?></dd>
                                                                            <dd><?php echo $gcontact; ?></dd>
                                                                        </dl>
                                                                        <dl class="dl-horizontal">
                                                                            <dt>Home Address</dt>
                                                                            <dd><?php echo $addressHSSP.' '.$addressBarangay.', '.$addressMunicipality.', '.$addressProvince; ?></dd>
                                                                        </dl>
                                                                        
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="pmb-block">
                                                                <div class="pmbb-header">
                                                                    <h2><i class="zmdi zmdi-accounts m-r-10"></i> <b>Family Background</b></h2>
                                                                </div>
                                                                <div class="pmbb-body p-l-30">
                                                                    <div class="pmbb-view">
                                                                        <dl class="dl-horizontal">
                                                                            <dt>Father</dt>
                                                                            <dd><?php echo $father; ?></dd>
                                                                        </dl>
                                                                        <dl class="dl-horizontal">
                                                                            <dt>Mother</dt>
                                                                            <dd><?php echo $mother; ?></dd>
                                                                        </dl>
                                                                        <dl class="dl-horizontal">
                                                                            <dt>Guardian</dt>
                                                                            <dd><?php echo $guardian; ?></dd>
                                                                                <dt>Relationship</dt>
                                                                                <dd><?php echo $relationship; ?></dd>
                                                                        </dl>

                                                                        
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div role="tabpanel" class="tab-pane active" id="class">
                                                            <div class="pmb-block">

                                                                <div class="pmbb-header">
                                                                    <h2><i class="zmdi zmdi-chart m-r-10"></i> <b>Report Cards</b></h2>
                                                                </div>
                                                                <?php

                                                                $querycs = mysqli_query($con, "SELECT * from controlinformation where student_id='".$id."'");


                                                                while ($row4 = mysqli_fetch_array($querycs)) {
                                                                    $controlID = $row4[0];
                                                                    $sectionID = $row4[4];
                                                                    $schoolYearID = $row4['sy_id'];
                                                                    $SY = $row4['sy_year'];

                                                          echo '<div class="pmbb-body p-l-30">
                                                                    <div class="card">
                                                                        <div class="table-responsive">
                                                                            <table class="table table-bordered">
                                                                                <thead>
                                                                                    <th rowspan="2" class="text-center" width="30%"><h2><b>'.$row4[19].' <br />'.$row4['sy_year'].'</b></h2></th>
                                                                                </thead>
                                                                                <tbody>
                                                                                    
                                                                                </tbody>
                                                                            </table>
                                                                            <table class="table table-bordered">
                                                                               
                                                                            <thead>
                                                                                <tr>

                                                                                <th rowspan="2" class="text-center" width="30%"><b>Subject Title</b></th>
                                                                                <th colspan="5" class="text-center" width="70%"><b>Grades</b></th>

                                                                                </tr>

                                                                                <tr>
                                                                                <th width="15%" class="text-center"><small><small><small><b>1st Quarter</b></small></small></small></th>
                                                                                <th width="15%" class="text-center"><small><small><small><b>2nd Quarter</b></small></small></small></th>
                                                                                <th width="15%" class="text-center"><small><small><small><b>3rd Quarter</b></small></small></small></th>
                                                                                <th width="15%" class="text-center"><small><small><small><b>4th Quarter</b></small></small></small></th>
                                                                                <th width="15%" class="text-center"><b>Final</b></th>

                                                                                </tr>
                                                                            </thead>
                                                                                <tbody>';
                                                                                        $getSO = mysqli_query($con, "SELECT * from subjectofferingdetails where section_id='".$sectionID."'");
                                                                                        $countgetSO = mysqli_num_rows(mysqli_query($con, "SELECT * from subjectofferingdetails where section_id='".$sectionID."'"));
                                                                                        if($countgetSO > 0){
                                                                                            while ($row1 = mysqli_fetch_array($getSO)) {
                                                                                                $soID = $row1[0];
                                                                                                echo "
                                                                                                    <tr>
                                                                                                    <td><b>".$row1[6]."</b></td>";
                                                                                                    
                                                                                                    $getQuarters = mysqli_query($con, "SELECT * from quarters where sy_id='".$SY."'");
                                                                                                    $countQ = mysqli_num_rows($getQuarters);

                                                                                                    if(mysqli_num_rows($getQuarters) != 0)
                                                                                                    {
                                                                                                        while ($item = mysqli_fetch_array($getQuarters)) {
                                                                                                   
                                                                                                            if($row2 = mysqli_fetch_row(mysqli_query($con, "SELECT * from grade where control_id='".$controlID."' and so_id='".$soID."' and quarters_id='".$item['quarters_id']."'"))){
                                                                                                                if($row2[0] < 75)
                                                                                                                {
                                                                                                                    echo "<td class='c-red'>".$row2[0]."</td>";
                                                                                                                }
                                                                                                                else
                                                                                                                {
                                                                                                                    echo "<td class='c-green'>".$row2[0]."</td>";
                                                                                                                    
                                                                                                                }
                                                                                                            }
                                                                                                            else
                                                                                                            {
                                                                                                                echo "<td></td>";
                                                                                                            }

                                                                                                        }
                                                                                                    }
                                                                                                    else{
                                                                                                        echo "<td></td><td></td><td></td><td></td>";
                                                                                                    }
                                                                                                    

                                                                                                    if(mysqli_num_rows($getQuarters) == 1){
                                                                                                    echo "<td></td><td></td><td></td>";  
                                                                                                    }else if(mysqli_num_rows($getQuarters) == 2){
                                                                                                        echo "<td></td><td></td>";

                                                                                                    }else if(mysqli_num_rows($getQuarters) == 3){
                                                                                                        echo "<td></td>";

                                                                                                    }else if(mysqli_num_rows($getQuarters) == 4){

                                                                                                    }

                                                                                                    $queryfinal = mysqli_query($con, "SELECT * from soaveragegrade where so_id = '".$soID."' and control_id='".$controlID."'");
                                                                                                    if ($row3 = mysqli_fetch_row($queryfinal)) {
                                                                                                        if($row3[0] < 75)
                                                                                                        {
                                                                                                            echo "<td class='c-red'>".$row3[0]."</td>";
                                                                                                        }
                                                                                                        else
                                                                                                        {
                                                                                                            echo "<td class='c-green'>".$row3[0]."</td>";
                                                                                                            
                                                                                                        }
                                                                                                    }
                                                                                                    else
                                                                                                    {
                                                                                                        echo "<td></td>";
                                                                                                    }

                                                                                                echo "
                                                                                                    </tr>
                                                                                                ";
                                                                                            }
                                                                                        }
                                                                                        else{
                                                                                            echo "<td></td><td></td><td></td><td></td><td></td><td></td>";
                                                                                        }
                                                                            $averageGrade = 0;
                                                                            $getAverageGrade = "SELECT IFNULL(average,0) as average from average where control_id='".$controlID."'";
                                                                            $getAverage = mysqli_fetch_row(mysqli_query($con, $getAverageGrade));
                                                                            if(mysqli_num_rows(mysqli_query($con, $getAverageGrade)) != 0){
                                                                            $averageGrade = $getAverage[0];
                                                                            }
                                                                            echo '
                                                                                    
                                                                                    </tbody>
                                                                                   
                                                                            </table>
                                                                             <table class="table table-bordered">
                                                                                <thead>
                                                                                    <th rowspan="2" class="text-center" width="30%"><b>Total GPA: '.$averageGrade.'</b></th>
                                                                                </thead>
                                                                                <tbody>
                                                                                    
                                                                                </tbody>
                                                                            </table>
                                                                            <table class="table table-bordered">
                                                                               
                                                                            <thead>
                                                                                <tr>
                                                                                <th rowspan="2" class="text-center" width="30%"><b>Total</b></th>
                                                                                <th colspan="12" class="text-center" width="70%"><b>ATTENDANCE</b></th>

                                                                                </tr>

                                                                                <tr>
                                                                                <th class="text-center"><small><small><small>June</small></small></small></th>
                                                                                <th class="text-center"><small><small><small>July</small></small></small></th>
                                                                                <th class="text-center"><small><small><small>Aug</small></small></small></th>
                                                                                <th class="text-center"><small><small><small>Sept</small></small></small></th>
                                                                                <th class="text-center"><small><small><small>Oct</small></small></small></th>
                                                                                <th class="text-center"><small><small><small>Nov</small></small></small></th>
                                                                                <th class="text-center"><small><small><small>Dec</small></small></small></th>
                                                                                <th class="text-center"><small><small><small>Jan</small></small></small></th>
                                                                                <th class="text-center"><small><small><small>Feb</small></small></small></th>
                                                                                <th class="text-center"><small><small><small>Mar</small></small></small></th>
                                                                                <th class="text-center"><small><small><small>Total</small></small></small></th>

                                                                                </tr>
                                                                            </thead>
                                                                                <tbody>';
                                                                                        

                                                                                        $getMonth = mysqli_query($con, "SELECT * from month where sy_id='".$schoolYearID."' order by month_id asc");
                                                                                        $getMonth1 = mysqli_query($con, "SELECT * from month where sy_id='".$schoolYearID."' order by month_id asc");
                                                                                        $getMonth2 = mysqli_query($con, "SELECT * from month where sy_id='".$schoolYearID."' order by month_id asc");
                                                                                        $getMonth3 = mysqli_query($con, "SELECT * from month where sy_id='".$schoolYearID."' order by month_id asc");

                                                                                        $months = array('June','July','August','September','October','November','December','January','February','March');

                                                                                        if(mysqli_num_rows($getMonth) == 0)
                                                                                        {
                                                                                            echo "
                                                                                                <tr>
                                                                                                    <td><b>Number of days in school year</b></td>
                                                                                                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td><b>Number of days present</b></td>
                                                                                                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td><b>Number of days late</b></td>
                                                                                                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td><b>Number of days absent</b></td>
                                                                                                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                                                                                                </tr>
                                                                                            ";
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            echo "<t>
                                                                                            <td>Number of days in shool year</td>
                                                                                            ";
                                                                                            for ($i=0; $i < count($months) ; $i++) { 
                                                                                                $find = mysqli_query($con, "SELECT * from month where month_description='".$months[$i]."' and sy_id='".$schoolYearID."'");
                                                                                                $temp = mysqli_fetch_row($find);
                                                                                                if(mysqli_num_rows($find) == 0)
                                                                                                {
                                                                                                    echo "<td>0</td>";
                                                                                                }
                                                                                                else
                                                                                                {
                                                                                                    echo "<td>".$temp[2]."</td>";
                                                                                                }
                                                                                            }
                                                                                            $getTotal_totalDays = mysqli_fetch_row(mysqli_query($con, "SELECT IFNULL(sum(month_totalDays),0) from month where sy_id='".$schoolYearID."'"));

                                                                                            echo "
                                                                                                <td>".$getTotal_totalDays[0]."</td>
                                                                                            </tr>";

                                                                                            //PRESENT
                                                                                            echo "<t>
                                                                                            <td>Number of days present</td>
                                                                                            ";
                                                                                           
                                                                                            for ($i=0; $i < count($months) ; $i++) { 
                                                                                                $find = mysqli_query($con, "SELECT * from month where month_description='".$months[$i]."' and sy_id='".$schoolYearID."'");
                                                                                                $temp = mysqli_fetch_row($find);
                                                                                                if(mysqli_num_rows($find) == 0)
                                                                                                {
                                                                                                    echo "<td>0</td>";
                                                                                                }
                                                                                                else
                                                                                                {
                                                                                                    $query = mysqli_query($con, "SELECT * from attendancedetails where month_id='".$temp[0]."' and control_id='".$controlID."'");
                                                                                                    if(mysqli_num_rows($query) == 0)
                                                                                                    {
                                                                                                        echo "<td>0</td>";
                                                                                                    }
                                                                                                    else
                                                                                                    {
                                                                                                        $temp = mysqli_fetch_row($query);
                                                                                                        echo "<td>".$temp[5]."</td>";
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                            $getTotal_presentDays = mysqli_fetch_row(mysqli_query($con, "SELECT IFNULL(sum(attendance_presentDays),0) from attendance where control_id='".$controlID."'"));
                                                                                            
                                                                                            echo "
                                                                                                <td>".$getTotal_presentDays[0]."</td>
                                                                                            </tr>";

                                                                                            //LATE
                                                                                            echo "<t>
                                                                                            <td>Number of days late</td>
                                                                                            ";
                                                                                            for ($i=0; $i < count($months) ; $i++) { 
                                                                                                $find = mysqli_query($con, "SELECT * from month where month_description='".$months[$i]."' and sy_id='".$schoolYearID."'");
                                                                                                $temp = mysqli_fetch_row($find);
                                                                                                if(mysqli_num_rows($find) == 0)
                                                                                                {
                                                                                                    echo "<td>0</td>";
                                                                                                }
                                                                                                else
                                                                                                {
                                                                                                    $query = mysqli_query($con, "SELECT * from attendancedetails where month_id='".$temp[0]."' and control_id='".$controlID."'");
                                                                                                    if(mysqli_num_rows($query) == 0)
                                                                                                    {
                                                                                                        echo "<td>0</td>";
                                                                                                    }
                                                                                                    else
                                                                                                    {
                                                                                                        $temp = mysqli_fetch_row($query);
                                                                                                        echo "<td>".$temp[7]."</td>";
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                            $getTotal_absentDays = mysqli_fetch_row(mysqli_query($con, "SELECT IFNULL(sum(attendance_tardyDays),0) from attendance where control_id='".$controlID."'"));
                                                                                            echo "
                                                                                                <td>".$getTotal_absentDays[0]."</td>
                                                                                            </tr>";

                                                                                            //ABSENT
                                                                                            echo "<t>
                                                                                            <td>Number of days absent</td>
                                                                                            ";
                                                                                            for ($i=0; $i < count($months) ; $i++) { 
                                                                                                $find = mysqli_query($con, "SELECT * from month where month_description='".$months[$i]."' and sy_id='".$schoolYearID."'");
                                                                                                $temp = mysqli_fetch_row($find);
                                                                                                if(mysqli_num_rows($find) == 0)
                                                                                                {
                                                                                                    echo "<td>0</td>";
                                                                                                }
                                                                                                else
                                                                                                {
                                                                                                    $query = mysqli_query($con, "SELECT * from attendancedetails where month_id='".$temp[0]."' and control_id='".$controlID."'");
                                                                                                    if(mysqli_num_rows($query) == 0)
                                                                                                    {
                                                                                                        echo "<td>0</td>";
                                                                                                    }
                                                                                                    else
                                                                                                    {
                                                                                                        $temp = mysqli_fetch_row($query);
                                                                                                        echo "<td>".$temp[6]."</td>";
                                                                                                    }
                                                                                                }
                                                                                            }

                                                                                            $getTotal_tardyDays = mysqli_fetch_row(mysqli_query($con, "SELECT IFNULL(sum(attendance_absentDays),0) from attendance where control_id='".$controlID."'"));


                                                                                            echo "
                                                                                                <td>".$getTotal_tardyDays[0]."</td>
                                                                                            </tr>";
                                                                                        }
                                                                                        
                                                                                                          
                    
                                                                            echo '
                                                                                    
                                                                                    </tbody>
                                                                                   
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>';

                                                                } 

                                                                ?>                                                               

                                                            </div>
                                                            
                                                        </div>
                                                        
                                                        <div role="tabpanel" class="tab-pane" id="advisory">
                                                            <div class="pmb-block">
                                                                <div class="pmbb-header">
                                                                    <h2><i class="zmdi zmdi-book m-r-10"></i> <b>Books Acquired </b></h2>
                                                                </div>
                                                                
                                                                                    <?php
                                                                                    $query1 = mysqli_query($con, "SELECT * from controlinformation where student_id='".$id."'");

                                                                                        while($row = mysqli_fetch_array($query1)){
                                                                                            $control_id = $row[0];
                                                                                            $schoolyear = $row['sy_year'];
                                                                                            echo '
                                                                                            <div class="pmbb-body p-l-30">
                                                                                                <div class="card">
                                                                                                    <div class="table-responsive">
                                                                                                        <table class="table table-bordered">
';
                                                                                            echo "
                                                                                            <thead class='text-center'>
                                                                                                <tr>
                                                                                                <th colspan='6' class='text-center'><b>".$row[19].' <br /> '.$schoolyear."</b></th>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <th rowspan='2' class='text-center'><b>Book No.</b></th>
                                                                                                    <th colspan='2' class='text-center'><b>Book</b></th>
                                                                                                    <th colspan='2' class='text-center'><b>Date</b></th>
                                                                                                    <th rowspan='2' class='text-center'><b>Remarks</b></th>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <th class='text-center'><small><small><small>Title</small></small></small></th>
                                                                                                    <th class='text-center'><small><small><small>Description</small></small></small></th>
                                                                                                    <th class='text-center'><small><small><small>Issued</small></small></small></th>
                                                                                                    <th class='text-center'><small><small><small>Returned</small></small></small></th>
                                                                                                </tr>
                                                                                            </thead>
                                                                                            <tbody>
                                                                                            ";

                                                                                            $query = mysqli_query($con, "SELECT * from booksacquireddetails where control_id ='".$control_id."' ");
                                                                                            while($row = mysqli_fetch_array($query)){
                                                                                                $temp = $row[0];
                                                                                                $queryDateIssued =mysqli_fetch_row(mysqli_query($con, "SELECT DATE_FORMAT(date_acquired, '%b. %e, %Y') from booksacquireddetails where control_id='".$temp."'"));
                                                                                                $issued = $queryDateIssued[0];
                                                                                               

                                                                                                $queryDateReturned = mysqli_fetch_row(mysqli_query($con, "SELECT DATE_FORMAT(date_returned, '%b. %e, %Y') from booksacquireddetails where control_id='".$temp."'"));
                                                                                                $returned = $queryDateReturned[0];
                                                                                                echo "
                                                                                                <tr>
                                                                                                    <td>".$row[6]."</td>
                                                                                                    <td>".$row[7]."</td>
                                                                                                    <td>".$row[8]."</td>
                                                                                                    <td>".$issued."</td>
                                                                                                    <td>".$returned."</td>
                                                                                                    <td>".$row[9]."</td>
                                                                                                </tr>    
                                                                                                ";
                                                                                            }
                                                                                            echo "</tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>";

                                                                                        }
                                                                                        
                                                                                    ?>
                                                                                
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    
                    </div>
                
                    </div>
                </div>
            </section>
        </section>

        <?php include_once '../footer.php' ?>

        <!-- Javascript Libraries -->
        <script src="../vendors/bower_components/jquery/dist/jquery.min.js"></script>

        <script src="../vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

        <script src="../vendors/bower_components/flot/jquery.flot.js"></script>

        <script src="../vendors/bower_components/flot/jquery.flot.resize.js"></script>

        <script src="../vendors/bower_components/flot.curvedlines/curvedLines.js"></script>

        <script src="../vendors/sparklines/jquery.sparkline.min.js"></script>

        <script src="../vendors/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js"></script>

        <script src="../vendors/bower_components/moment/min/moment.min.js"></script>

        <script src="../vendors/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>

        <script src="../vendors/bower_components/simpleWeather/jquery.simpleWeather.min.js"></script>

        <script src="../vendors/bower_components/Waves/dist/waves.min.js"></script>

        <script src="../vendors/bootstrap-growl/bootstrap-growl.min.js"></script>
        <script src="../vendors/bower_components/sweetalert2/dist/sweetalert2.min.js"></script>
        <script src="../vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
        <script src="../vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
        <script src="../vendors/bower_components/dropzone/dist/min/dropzone.min.js"></script>

        <script src="../vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.js"></script>
        <script src="../vendors/bower_components/nouislider/distribute/nouislider.min.js"></script>
        <script src="../vendors/bower_components/typeahead.js/dist/typeahead.bundle.min.js"></script>
        <script src="../vendors/summernote/dist/summernote-updated.min.js"></script>
        <script src="../vendors/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
        <!-- Placeholder for IE9 -->
        <!--[if IE 9 ]>
            <script src="vendors/bower_components/jquery-placeholder/jquery.placeholder.min.js"></script>
        <![endif]-->

        <script src="../vendors/bower_components/chosen/chosen.jquery.js"></script>
        <script src="../vendors/bower_components/jquery-mask-plugin/dist/jquery.mask.min.js"></script>
        <script src="../vendors/fileinput/fileinput.min.js"></script>
        <script src="../vendors/farbtastic/farbtastic.min.js"></script>
        <script src="../js/app.min.js"></script>
        <script type="text/javascript" src="../js/pace.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $('#data-table-basic').DataTable();

                //PAG SEARCH
                $('#search').keyup(function(){
                    var txt = $(this).val();
                    if(txt != '')
                    {
                        $.ajax({
                            url:"ajaxJQuery/searchTeacher.php",
                            method:"POST",
                            data:{search:txt},
                            dataType:"text",
                            success:function(data)
                            {
                                $('#resultsaSearch').html(data);
                            }
                        });
                    }
                    
                });



                $('#insert_form').on("submit", function(event){
                    event.preventDefault();
                    $('#faculty_fname').focus();
                    if($('#faculty_fname').val() == '')  
                    {
                            $('#faculty_fname').focus();    
                         alert("First name is required and valid");
                         
                    }  
                    else if($('#faculty_mname').val() == '')  
                    {  
                        $('#faculty_mname').focus();  
                         alert("Middle name is required and valid");  
                    }  
                    else if($('#faculty_lname').val() == '')  
                    {
                        $('#faculty_lname').focus();
                         alert("Maximun grade is required");  
                    }

                    else if($('#faculty_addressHSSP').val() =='')
                    {
                        $('#faculty_addressHSSP').focus();
                        alert("Home address is required and valid!");
                    }
                    else if($('#faculty_addressBarangay').val() == '')
                    {
                        $('#faculty_addressBarangay').focus();
                        alert("Barangay address is required and valid!");
                    }
                    else if($('#faculty_addressMunicipality').val() == 'MUNICIPALITY')
                    {
                        $('#faculty_addressMunicipality').focus();
                        alert("Municipality address is required and valid!");
                    }
                    else if($('#faculty_addressProvince').val() == 'PROVINCE')
                    {
                        $('#faculty_addressProvince').focus();
                        alert("Province address is required and valid!");
                    }
                    else if($('#faculty_phone').val() == '')  
                    {
                        $('#faculty_phone').focus();  
                         alert("Phone is required and valid");  
                    }

                    else if($('#faculty_religion').val() == 'RELIGION')
                    {
                        $('#faculty_religion').focus();
                        alert("You must select religion!");
                    }
                    else if($('#faculty_dob').val() == '')
                    {
                        $('#faculty_dob').focus();
                        alert("Date of birth is required and valid!");
                    }

                    else if($('#faculty_gender').val() == 'GENDER')
                    {
                        $('#faculty_gender').focus();
                        alert("You must select gender!");
                    }
                    
                    else  
                    {  
                         $.ajax({  
                              url:"ajaxJQuery/insertTeacher.php",  
                              method:"POST",  
                              data:$('#insert_form').serialize(),  
                              beforeSend:function(){  
                                   $('#insert1').val("Inserting..");   
                              },  
                              success:function(data){ 
                                   swal("Good job", "You successfully created Teacher account", "success");  
                                   $('#insert_form')[0].reset();  
                                   $('#modalColor').card('hide');  
                                   $('#sectionList').html(data); 
                              }  

                         });  
                    }
                });





            } );
        </script>
    </body>

</html>