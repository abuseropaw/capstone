<?php
session_start();

if(isset($_SESSION['student_id'])) {
	session_destroy();
	unset($_SESSION['student_id']);
	unset($_SESSION['Name']);


	header("Location: studentLogin.php");
} else {
	header("Location: studentLogin.php");
}
?>