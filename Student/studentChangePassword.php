<?php
    session_start();
    if($_SESSION['student_id'] == ''){
     header("Location: ../index.php");}
    include_once 'dbconnect.php';
    
    $error = false;

    $query = mysqli_query($con, "SELECT * from studentinfo1 where student_id = '".$_SESSION['student_id']."'");
    if ($row = mysqli_fetch_array($query)) {
        $cid = $row[0];
        $id=$row[1];
        $LRN=$row[2];
        $name=$row[3];
        $sex=$row[4];
        $DOB=$row[5];
        $age=$row[6];
        $pass=$row[7];
        
    }
    $query = mysqli_query($con, "SELECT * from student_account where student_id = '".$_SESSION['student_id']."'");
    if ($row = mysqli_fetch_array($query)) {
        $pass=$row[12];
        
    }
    if($row1 = mysqli_fetch_array(mysqli_query($con, "SELECT * from student_account where student_id='".$_SESSION['student_id']."'")))
    {
        $pic = $row1['student_picture'];
    }      

    if(isset($_POST['changepass'])){
        $current = mysqli_real_escape_string($con, $_POST['current']);
        $new = mysqli_real_escape_string($con, $_POST['new']);
        $confirm = mysqli_real_escape_string($con, $_POST['confirm']);

        if(sha1($current) != $pass) {
            $error = true;
            $error_type1 =" has-error has-feedback";
            $current_error = "Incorrect account current password!";
        }
        if($new != $confirm) {
            $error = true;
            $error_type2 =" has-error has-feedback";
            $confirm_error = "New account password and confirm password did not match!";
        }
        if(!$error){
            if(mysqli_query($con, "UPDATE student_account set student_pass='".sha1($new)."' where student_id='".$id."'")) {
                $successmsg = "Successfully updated! :)";
            } else {
                $errormsg = "Error in updating the pass...Please try again later!";
            }
        }
    }

?>
<!DOCTYPE html>
<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1"><link rel="shortcut icon" type="image/x-icon" href="../img/lnhs.ico" alt="logo" />
        <title>Student | Change Password</title>

        <link href="../vendors/bower_components/animate.css/animate.min.css" rel="stylesheet">
        <link href="../vendors/bower_components/sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
        <link href="../vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css" rel="stylesheet">
        <link href="../vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet">
        <link href="../vendors/bower_components/dropzone/dist/min/dropzone.min.css" rel="stylesheet">
        <link href="../vendors/bower_compon../ents/chosen/chosen.css" rel="stylesheet">
        <link href="../css/app_1.min.css" rel="stylesheet">
        <link href="../css/app_2.min.css" rel="stylesheet">
        <link href="../css/demo.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="../css/pace.min.css">
    </head>
    <body>
        <header id="header" class="clearfix" data-ma-theme="green">
            <ul class="h-inner">
                <li class="hi-trigger ma-trigger" data-ma-action="sidebar-open" data-ma-target="#sidebar">
                    <div class="line-wrap">
                        <div class="line top"></div>
                        <div class="line center"></div>
                        <div class="line bottom"></div>
                    </div>
                </li>

                <li class="hi-logo hidden-xs">
                    <a href="index.html">Lugait National High School</a>
                </li>

                <li class="pull-right">
                    <ul class="hi-menu">

                        <li class="dropdown">
                            <a data-toggle="dropdown" href="#"><i class="him-icon zmdi zmdi-caret-down"></i></a>
                                <ul class="dropdown-menu dm-icon pull-right">
                       
                                    <li>
                                        <a href="studentinformation.php"><i class="zmdi zmdi-pin-account"></i> Account</a>
                                    </li>
                                    <li>
                                        <a href="studentChangePassword.php"><i class="zmdi zmdi-lock"></i> Privacy Settings</a>
                                    </li>
                                    <li>
                                        <a href="logout.php"><i class="zmdi zmdi-power"></i>Logout</a>
                                    </li>
                                </ul>
                        </li>

                        
                    </ul>
                </li>
            </ul>

            <!-- Top Search Content -->
            <div class="h-search-wrap">
                <div class="hsw-inner">
                    <i class="hsw-close zmdi zmdi-arrow-left" data-ma-action="search-close"></i>
                    <input type="text">
                </div>
            </div>
        </header>

        <section id="main">
            <aside id="sidebar" class="sidebar c-overflow">
                <div class="s-profile">
                    <a>
                        <div class="sp-pic">
                            <?php
                                if($pic == '')
                                {
                                    echo "<img class='img-responsive' src='../img/default-profile.png' alt=''>";

                                }else
                                {
                                    echo "<img class='img-responsive' src='../profile/".$pic."' alt=''>";

                                }

                            ?>
                        </div>
                        <div class="sp-info">
                            <?php echo $_SESSION['Name']; ?>
                        </div>
                    </a>
                </div>
                <ul class="main-menu">
                    <li>
                        <a href="studentHome.php"><i class="zmdi zmdi-home"></i> Home</a>
                    </li>
                    <li>
                        <a href="studentInformation.php"><i class="zmdi zmdi-account"></i> View Performance and Personal Info</a>
                    </li>
                    <li class="active">
                        <a href="studentChangePassword.php"><i class="zmdi zmdi-lock"></i> Change Password</a>
                    </li>
                    <li>
                        <a href="logout.php"><i class="zmdi zmdi-power"></i> Logout</a>
                    </li>
                </ul>
            </aside>
            <section id="content">
                <div class="container">
                    <div class="block-header">
                        <h1><i class="zmdi zmdi-lock"></i> Change Password
                        </h1> 
                    </div>
                    <div class="row" id='sectionList'>
                        <div class="col-sm-12">
                            <div class='card'>                                  
                                <div class="card-body lcb-form">                                             
                                    <form role="form" class="form-horizontal" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" name="signupform">
                                        <div class="card-body card-padding">
                                            <div class="form-group <?php if (isset($error_type1)) echo $error_type1; ?>">
                                                <label for="current" class="col-sm-2 control-label">Current Password</label>
                                                <div class="col-sm-5">
                                                    <div class="fg-line">
                                                        <input type="password" name="current" class="form-control input-sm" id="current" required value="<?php if($error) echo $current; ?>" placeholder="Current Password">
                                                    </div>
                                                    <span class="text-danger"><?php if (isset($current_error)) echo $current_error; ?></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="new" class="col-sm-2 control-label">New Password</label>
                                                <div class="col-sm-5">
                                                    <div class="fg-line">
                                                        <input type="password" name="new" class="form-control input-sm" id="new" required value="<?php if($error) echo $new; ?>" placeholder="New Password">
                                                    </div>
                                                    <span class="text-danger"><?php if (isset($new_error)) echo $new_error; ?></span>
                                                </div>
                                            </div>
                                            <div class="form-group <?php if (isset($error_type2)) echo $error_type2; ?>">
                                                <label for="confirm" class="col-sm-2 control-label">Re-type New Password</label>
                                                <div class="col-sm-5">
                                                    <div class="fg-line">
                                                        <input type="password" name="confirm" class="form-control input-sm" id="confirm" required value="<?php if($error) echo $confirm; ?>" placeholder="Re-type New Password">
                                                    </div>
                                                    <span class="text-danger"><?php if (isset($confirm_error)) echo $confirm_error; ?></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-offset-2 col-sm-5">
                                                    <button type="submit" name="changepass" id="changepass" class="btn btn-success waves-effect btn-lg">Save Password</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <div class="card-body card-padding">

                                        <span class="text-success"><?php if (isset($successmsg)) { echo $successmsg; echo "<script>alert('Successfully update password!!');</script>"; } ?></span>
                                        <span class="text-danger"><?php if (isset($errormsg)) { echo $errormsg; } ?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </section>

       <?php include_once '../footer.php' ?>

        <!-- Javascript Libraries -->
        <script src="../vendors/bower_components/jquery/dist/jquery.min.js"></script>

        <script src="../vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

        <script src="../vendors/bower_components/flot/jquery.flot.js"></script>

        <script src="../vendors/bower_components/flot/jquery.flot.resize.js"></script>

        <script src="../vendors/bower_components/flot.curvedlines/curvedLines.js"></script>

        <script src="../vendors/sparklines/jquery.sparkline.min.js"></script>

        <script src="../vendors/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js"></script>

        <script src="../vendors/bower_components/moment/min/moment.min.js"></script>

        <script src="../vendors/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>

        <script src="../vendors/bower_components/simpleWeather/jquery.simpleWeather.min.js"></script>

        <script src="../vendors/bower_components/Waves/dist/waves.min.js"></script>

        <script src="../vendors/bootstrap-growl/bootstrap-growl.min.js"></script>
        <script src="../vendors/bower_components/sweetalert2/dist/sweetalert2.min.js"></script>
        <script src="../vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
        <script src="../vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
        <script src="../vendors/bower_components/dropzone/dist/min/dropzone.min.js"></script>

        <script src="../vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.js"></script>
        <script src="../vendors/bower_components/nouislider/distribute/nouislider.min.js"></script>
        <script src="../vendors/bower_components/typeahead.js/dist/typeahead.bundle.min.js"></script>
        <script src="../vendors/summernote/dist/summernote-updated.min.js"></script>
        <script src="../vendors/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
        <!-- Placeholder for IE9 -->
        <!--[if IE 9 ]>
            <script src="vendors/bower_components/jquery-placeholder/jquery.placeholder.min.js"></script>
        <![endif]-->

        <script src="../vendors/bower_components/chosen/chosen.jquery.js"></script>
        <script src="../vendors/bower_components/jquery-mask-plugin/dist/jquery.mask.min.js"></script>
        <script src="../vendors/fileinput/fileinput.min.js"></script>
        <script src="../vendors/farbtastic/farbtastic.min.js"></script>
        <script src="../js/app.min.js"></script>
        <script type="text/javascript" src="../js/pace.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $('#data-table-basic').DataTable();

                //PAG SEARCH
                $('#search').keyup(function(){
                    var txt = $(this).val();
                    if(txt != '')
                    {
                        $.ajax({
                            url:"ajaxJQuery/searchTeacher.php",
                            method:"POST",
                            data:{search:txt},
                            dataType:"text",
                            success:function(data)
                            {
                                $('#resultsaSearch').html(data);
                            }
                        });
                    }
                    
                });



                $('#insert_form').on("submit", function(event){
                    event.preventDefault();
                    $('#faculty_fname').focus();
                    if($('#faculty_fname').val() == '')  
                    {
                            $('#faculty_fname').focus();    
                         alert("First name is required and valid");
                         
                    }  
                    else if($('#faculty_mname').val() == '')  
                    {  
                        $('#faculty_mname').focus();  
                         alert("Middle name is required and valid");  
                    }  
                    else if($('#faculty_lname').val() == '')  
                    {
                        $('#faculty_lname').focus();
                         alert("Maximun grade is required");  
                    }

                    else if($('#faculty_addressHSSP').val() =='')
                    {
                        $('#faculty_addressHSSP').focus();
                        alert("Home address is required and valid!");
                    }
                    else if($('#faculty_addressBarangay').val() == '')
                    {
                        $('#faculty_addressBarangay').focus();
                        alert("Barangay address is required and valid!");
                    }
                    else if($('#faculty_addressMunicipality').val() == 'MUNICIPALITY')
                    {
                        $('#faculty_addressMunicipality').focus();
                        alert("Municipality address is required and valid!");
                    }
                    else if($('#faculty_addressProvince').val() == 'PROVINCE')
                    {
                        $('#faculty_addressProvince').focus();
                        alert("Province address is required and valid!");
                    }
                    else if($('#faculty_phone').val() == '')  
                    {
                        $('#faculty_phone').focus();  
                         alert("Phone is required and valid");  
                    }

                    else if($('#faculty_religion').val() == 'RELIGION')
                    {
                        $('#faculty_religion').focus();
                        alert("You must select religion!");
                    }
                    else if($('#faculty_dob').val() == '')
                    {
                        $('#faculty_dob').focus();
                        alert("Date of birth is required and valid!");
                    }

                    else if($('#faculty_gender').val() == 'GENDER')
                    {
                        $('#faculty_gender').focus();
                        alert("You must select gender!");
                    }
                    
                    else  
                    {  
                         $.ajax({  
                              url:"ajaxJQuery/insertTeacher.php",  
                              method:"POST",  
                              data:$('#insert_form').serialize(),  
                              beforeSend:function(){  
                                   $('#insert1').val("Inserting..");   
                              },  
                              success:function(data){ 
                                   swal("Good job", "You successfully created Teacher account", "success");  
                                   $('#insert_form')[0].reset();  
                                   $('#modalColor').card('hide');  
                                   $('#sectionList').html(data); 
                              }  

                         });  
                    }
                });





            } );
        </script>
    </body>

</html>