<?php
    session_start();
    if($_SESSION['student_id'] == ''){
     header("Location: ../index.php");}
    include_once 'dbconnect.php';

    if($_SESSION['type'] != 'Learner')
    {
        header("Location: studentLogin.php");
    }


    $studentID =mysqli_real_escape_string($con, $_SESSION['student_id']);

    if($row1 = mysqli_fetch_array(mysqli_query($con, "SELECT * from student_account where student_id='".$studentID."'")))
    {
        $pic = $row1['student_picture'];
    }

    $getCurrentSY = mysqli_query($con, "SELECT sy_year from schoolyear where sy_remarks='Open'");
    $temp = mysqli_fetch_row($getCurrentSY);
    $SY = $temp[0];
    function fillSchoolYear($con){
        $output ='';
        $query = mysqli_query($con, "SELECT * from schoolyear order by sy_id DESC");
        if(check($con))
        {

        }
        else
        {
            $output .='<option disabled="disabled" selected>NO CURRENT SY</option>';
        }
        while($row = mysqli_fetch_array($query)){
            if($row[4] == 'Open'){
                $output .='<option value="'.$row[1].'">'.$row[1].' - CURRENT</option>';
            }else
            {
                $output .='<option value="'.$row[1].'">'.$row[1].'</option>';
            }
            
        }
        return $output;
    }
    function check($con)
    {
        $query = mysqli_num_rows(mysqli_query($con, "SELECT * from schoolyear where sy_remarks='Open'"));
        if($query > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    #Getting the total number of students

    // $sql = "SELECT COUNT(student_id) as count FROM studentinfo1 where sex=F";
    $sql = mysqli_query($con, "SELECT average FROM averagedetails where Gradelevel='Grade 7' and LRN=(SELECT student_LRN from student_account where student_id='".$_SESSION['student_id']."')");
    $click = mysqli_fetch_row($sql);
    $click = json_encode($click[0],JSON_NUMERIC_CHECK);

    $sql1 = mysqli_query($con, "SELECT average FROM averagedetails where Gradelevel='Grade 8' and LRN=(SELECT student_LRN from student_account where student_id='".$_SESSION['student_id']."')");
    $click1 = mysqli_fetch_row($sql1);
    $click1 = json_encode($click1[0],JSON_NUMERIC_CHECK);

    $sql2 = mysqli_query($con, "SELECT average FROM averagedetails where Gradelevel='Grade 9' and LRN=(SELECT student_LRN from student_account where student_id='".$_SESSION['student_id']."')");
    $click2 = mysqli_fetch_row($sql2);
    $click2 = json_encode($click2[0],JSON_NUMERIC_CHECK);

    $sql3 = mysqli_query($con, "SELECT average FROM averagedetails where Gradelevel='Grade 10' and LRN=(SELECT student_LRN from student_account where student_id='".$_SESSION['student_id']."')");
    $click3 = mysqli_fetch_row($sql3);
    $click3 = json_encode($click3[0],JSON_NUMERIC_CHECK);

    

    #Grade 7
    $sql4 = mysqli_query($con, "SELECT * from quarterlygradepersubject where student_id='".$_SESSION['student_id']."' and subjectTitle='English' and level='Grade 7'");
    $click4 = mysqli_fetch_row($sql4);
    $click4 = json_encode($click4[0],JSON_NUMERIC_CHECK);

    $sql5 = mysqli_query($con, "SELECT * from quarterlygradepersubject where student_id='".$_SESSION['student_id']."' and subjectTitle='Science' and level='Grade 7'");
    $click5 = mysqli_fetch_row($sql5);
    $click5 = json_encode($click5[0],JSON_NUMERIC_CHECK);

    $sql6 = mysqli_query($con, "SELECT * from quarterlygradepersubject where student_id='".$_SESSION['student_id']."' and subjectTitle='Mathematics' and level='Grade 7'");
    $click6 = mysqli_fetch_row($sql6);
    $click6 = json_encode($click6[0],JSON_NUMERIC_CHECK);

    $sql7 = mysqli_query($con, "SELECT * from quarterlygradepersubject where student_id='".$_SESSION['student_id']."' and subjectTitle='Filipino' and level='Grade 7'");
    $click7 = mysqli_fetch_row($sql7);
    $click7 = json_encode($click7[0],JSON_NUMERIC_CHECK);

    $sql8 = mysqli_query($con, "SELECT * from quarterlygradepersubject where student_id='".$_SESSION['student_id']."' and subjectTitle='Araling Panlipunan' and level='Grade 7'");
    $click8 = mysqli_fetch_row($sql8);
    $click8 = json_encode($click8[0],JSON_NUMERIC_CHECK);

    $sql9 = mysqli_query($con, "SELECT * from quarterlygradepersubject where student_id='".$_SESSION['student_id']."' and subjectTitle='TLE' and level='Grade 7'");
    $click9 = mysqli_fetch_row($sql9);
    $click9 = json_encode($click9[0],JSON_NUMERIC_CHECK);

    $sql10 = mysqli_query($con, "SELECT * from quarterlygradepersubject where student_id='".$_SESSION['student_id']."' and subjectTitle='MAPEH' and level='Grade 7'");
    $click10 = mysqli_fetch_row($sql10);
    $click10 = json_encode($click10[0],JSON_NUMERIC_CHECK);

    $sql11 = mysqli_query($con, "SELECT * from quarterlygradepersubject where student_id='".$_SESSION['student_id']."' and subjectTitle='Edukasyon sa Pagpapakatao' and level='Grade 7'");
    $click11 = mysqli_fetch_row($sql11);
    $click11 = json_encode($click11[0],JSON_NUMERIC_CHECK);


    #Grade 8
    $sql12 = mysqli_query($con, "SELECT * from quarterlygradepersubject where student_id='".$_SESSION['student_id']."' and subjectTitle='English' and level='Grade 8'");
    $click12 = mysqli_fetch_row($sql12);
    $click12 = json_encode($click12[0],JSON_NUMERIC_CHECK);

    $sql13 = mysqli_query($con, "SELECT * from quarterlygradepersubject where student_id='".$_SESSION['student_id']."' and subjectTitle='Science' and level='Grade 8'");
    $click13 = mysqli_fetch_row($sql13);
    $click13 = json_encode($click13[0],JSON_NUMERIC_CHECK);

    $sql14 = mysqli_query($con, "SELECT * from quarterlygradepersubject where student_id='".$_SESSION['student_id']."' and subjectTitle='Mathematics' and level='Grade 8'");
    $click14 = mysqli_fetch_row($sql14);
    $click14 = json_encode($click14[0],JSON_NUMERIC_CHECK);

    $sql15 = mysqli_query($con, "SELECT * from quarterlygradepersubject where student_id='".$_SESSION['student_id']."' and subjectTitle='Filipino' and level='Grade 8'");
    $click15 = mysqli_fetch_row($sql15);
    $click15 = json_encode($click15[0],JSON_NUMERIC_CHECK);

    $sql16 = mysqli_query($con, "SELECT * from quarterlygradepersubject where student_id='".$_SESSION['student_id']."' and subjectTitle='Araling Panlipunan' and level='Grade 8'");
    $click16 = mysqli_fetch_row($sql16);
    $click16 = json_encode($click16[0],JSON_NUMERIC_CHECK);

    $sql17 = mysqli_query($con, "SELECT * from quarterlygradepersubject where student_id='".$_SESSION['student_id']."' and subjectTitle='TLE' and level='Grade 8'");
    $click17 = mysqli_fetch_row($sql17);
    $click17 = json_encode($click17[0],JSON_NUMERIC_CHECK);

    $sql18 = mysqli_query($con, "SELECT * from quarterlygradepersubject where student_id='".$_SESSION['student_id']."' and subjectTitle='MAPEH' and level='Grade 8'");
    $click18 = mysqli_fetch_row($sql18);
    $click18 = json_encode($click18[0],JSON_NUMERIC_CHECK);

    $sql19 = mysqli_query($con, "SELECT * from quarterlygradepersubject where student_id='".$_SESSION['student_id']."' and subjectTitle='Edukasyon sa Pagpapakatao' and level='Grade 8'");
    $click19 = mysqli_fetch_row($sql19);
    $click19 = json_encode($click19[0],JSON_NUMERIC_CHECK);



    #Grade 9
    $sql20 = mysqli_query($con, "SELECT * from quarterlygradepersubject where student_id='".$_SESSION['student_id']."' and subjectTitle='English' and level='Grade 9'");
    $click20 = mysqli_fetch_row($sql20);
    $click20 = json_encode($click20[0],JSON_NUMERIC_CHECK);

    $sql21 = mysqli_query($con, "SELECT * from quarterlygradepersubject where student_id='".$_SESSION['student_id']."' and subjectTitle='Science' and level='Grade 9'");
    $click21 = mysqli_fetch_row($sql21);
    $click21 = json_encode($click21[0],JSON_NUMERIC_CHECK);

    $sql22 = mysqli_query($con, "SELECT * from quarterlygradepersubject where student_id='".$_SESSION['student_id']."' and subjectTitle='Mathematics' and level='Grade 9'");
    $click22 = mysqli_fetch_row($sql22);
    $click22 = json_encode($click22[0],JSON_NUMERIC_CHECK);

    $sql23 = mysqli_query($con, "SELECT * from quarterlygradepersubject where student_id='".$_SESSION['student_id']."' and subjectTitle='Filipino' and level='Grade 9'");
    $click23 = mysqli_fetch_row($sql23);
    $click23 = json_encode($click23[0],JSON_NUMERIC_CHECK);

    $sql24 = mysqli_query($con, "SELECT * from quarterlygradepersubject where student_id='".$_SESSION['student_id']."' and subjectTitle='Araling Panlipunan' and level='Grade 9'");
    $click24 = mysqli_fetch_row($sql24);
    $click24 = json_encode($click24[0],JSON_NUMERIC_CHECK);

    $sql25 = mysqli_query($con, "SELECT * from quarterlygradepersubject where student_id='".$_SESSION['student_id']."' and subjectTitle='TLE' and level='Grade 9'");
    $click25 = mysqli_fetch_row($sql25);
    $click25 = json_encode($click25[0],JSON_NUMERIC_CHECK);

    $sql26 = mysqli_query($con, "SELECT * from quarterlygradepersubject where student_id='".$_SESSION['student_id']."' and subjectTitle='MAPEH' and level='Grade 9'");
    $click26 = mysqli_fetch_row($sql26);
    $click26 = json_encode($click26[0],JSON_NUMERIC_CHECK);

    $sql27 = mysqli_query($con, "SELECT * from quarterlygradepersubject where student_id='".$_SESSION['student_id']."' and subjectTitle='Edukasyon sa Pagpapakatao' and level='Grade 9'");
    $click27 = mysqli_fetch_row($sql27);
    $click27 = json_encode($click27[0],JSON_NUMERIC_CHECK);



    #Grade 10
    $sql28 = mysqli_query($con, "SELECT * from quarterlygradepersubject where student_id='".$_SESSION['student_id']."' and subjectTitle='English' and level='Grade 10'");
    $click28 = mysqli_fetch_row($sql28);
    $click28 = json_encode($click28[0],JSON_NUMERIC_CHECK);

    $sql29 = mysqli_query($con, "SELECT * from quarterlygradepersubject where student_id='".$_SESSION['student_id']."' and subjectTitle='Science' and level='Grade 10'");
    $click29 = mysqli_fetch_row($sql29);
    $click29 = json_encode($click29[0],JSON_NUMERIC_CHECK);

    $sql30 = mysqli_query($con, "SELECT * from quarterlygradepersubject where student_id='".$_SESSION['student_id']."' and subjectTitle='Mathematics' and level='Grade 10'");
    $click30 = mysqli_fetch_row($sql30);
    $click30 = json_encode($click30[0],JSON_NUMERIC_CHECK);

    $sql31 = mysqli_query($con, "SELECT * from quarterlygradepersubject where student_id='".$_SESSION['student_id']."' and subjectTitle='Filipino' and level='Grade 10'");
    $click31 = mysqli_fetch_row($sql31);
    $click31 = json_encode($click31[0],JSON_NUMERIC_CHECK);

    $sql32 = mysqli_query($con, "SELECT * from quarterlygradepersubject where student_id='".$_SESSION['student_id']."' and subjectTitle='Araling Panlipunan' and level='Grade 10'");
    $click32 = mysqli_fetch_row($sql32);
    $click32 = json_encode($click32[0],JSON_NUMERIC_CHECK);

    $sql33 = mysqli_query($con, "SELECT * from quarterlygradepersubject where student_id='".$_SESSION['student_id']."' and subjectTitle='TLE' and level='Grade 10'");
    $click33 = mysqli_fetch_row($sql33);
    $click33 = json_encode($click33[0],JSON_NUMERIC_CHECK);

    $sql34 = mysqli_query($con, "SELECT * from quarterlygradepersubject where student_id='".$_SESSION['student_id']."' and subjectTitle='MAPEH' and level='Grade 10'");
    $click34 = mysqli_fetch_row($sql34);
    $click34 = json_encode($click34[0],JSON_NUMERIC_CHECK);

    $sql35 = mysqli_query($con, "SELECT * from quarterlygradepersubject where student_id='".$_SESSION['student_id']."' and subjectTitle='Edukasyon sa Pagpapakatao' and level='Grade 10'");
    $click35 = mysqli_fetch_row($sql35);
    $click35 = json_encode($click35[0],JSON_NUMERIC_CHECK);


?>
<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie9"><![endif]-->
    
<!-- Mirrored from byrushan.com/projects/ma/1-7-1/jquery/light/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 01 May 2017 06:32:43 GMT -->
<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1"><link rel="shortcut icon" type="image/x-icon" href="../img/lnhs.ico" alt="logo" />
        <title>Home | Student</title>

        <!-- Vendor CSS -->
        <link href="../vendors/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">
        <link href="../vendors/bower_components/animate.css/animate.min.css" rel="stylesheet">
        <link href="../vendors/bower_components/sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
        <link href="../vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css" rel="stylesheet">
        <link href="../vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet">

        <!-- CSS -->
        <link href="../css/app_1.min.css" rel="stylesheet">
        <link href="../css/app_2.min.css" rel="stylesheet">

        <link href="../css/highcharts.css" rel="stylesheet">

        <style>
        #chartcontainer {
            height: 400px;
            max-width: 950px;
            margin: 0 auto;
        }

        /* Link the series colors to axis colors */
        .highcharts-color-0 {
            fill: #90ed7d;
            stroke: #90ed7d;
        }
        .highcharts-axis.highcharts-color-0 .highcharts-axis-line {
            stroke: #90ed7d;
        }
        .highcharts-axis.highcharts-color-0 text {
            fill: #90ed7d;
        }

        .highcharts-color-1 {
            fill: #FFEB38;
            stroke: #FFEB38;
        }
        .highcharts-axis.highcharts-color-1 .highcharts-axis-line {
            stroke: #FFEB38;
        }
        .highcharts-axis.highcharts-color-1 text {
            fill: #FFEB38;
        }

        .highcharts-color-2 {
            fill: #F44336;
            stroke: #F44336;
        }
        .highcharts-axis.highcharts-color-2 .highcharts-axis-line {
            stroke: #F44336;
        }
        .highcharts-axis.highcharts-color-2 text {
            fill: #F44336;
        }

        .highcharts-color-3 {
            fill: #7cb5ec;
            stroke: #7cb5ec;
        }
        .highcharts-axis.highcharts-color-3 .highcharts-axis-line {
            stroke: #7cb5ec;
        }
        .highcharts-axis.highcharts-color-3 text {
            fill: #7cb5ec;
        }


        .highcharts-yaxis .highcharts-axis-line {
            stroke-width: 2px;
        }
        </style>

    </head>
    <body>
        <header id="header" class="clearfix" data-ma-theme="green">
            <ul class="h-inner">
                <li class="hi-trigger ma-trigger" data-ma-action="sidebar-open" data-ma-target="#sidebar">
                    <div class="line-wrap">
                        <div class="line top"></div>
                        <div class="line center"></div>
                        <div class="line bottom"></div>
                    </div>
                </li>
                <li class="hi-logo hidden-xs">
                    <a href="index.html">Student</a>
                </li>
                <li class="pull-right">
                    <ul class="hi-menu">
                        <li class="dropdown">
                            <a data-toggle="dropdown" href="#"><i class="him-icon zmdi zmdi-caret-down"></i></a>
                                <ul class="dropdown-menu dm-icon pull-right">
                       
                                    <li>
                                        <a href="studentinformation.php"><i class="zmdi zmdi-pin-account"></i> Account</a>
                                    </li>
                                    <li>
                                        <a href="studentChangePassword.php"><i class="zmdi zmdi-lock"></i> Privacy Settings</a>
                                    </li>
                                    <li>
                                        <a href="logout.php"><i class="zmdi zmdi-power"></i>Logout</a>
                                    </li>
                                </ul>
                        </li>
                    </ul>
                </li>
            </ul>

            <!-- Top Search Content -->
            <div class="h-search-wrap">
                <div class="hsw-inner">
                    <i class="hsw-close zmdi zmdi-arrow-left" data-ma-action="search-close"></i>
                    <input type="text">
                </div>
            </div>
        </header>

        <section id="main">
            <aside id="sidebar" class="sidebar c-overflow">
                <div class="s-profile">
                    <a>
                        <div class="sp-pic">
                            <?php
                                if($pic == '')
                                {
                                    echo "<img src='../img/default-profile.png' alt=''>";

                                }else
                                {
                                    echo "<img src='../profile/".$pic."' alt=''>";

                                }

                            ?>
                        </div>
                        <div class="sp-info">
                            <?php echo $_SESSION['Name']; ?>
                        </div>
                    </a>
                </div>
                <ul class="main-menu">
                    <li class="active">
                        <a href="studentHome.php"><i class="zmdi zmdi-home"></i> Home</a>
                    </li>
                    <li>
                        <a href="studentInformation.php"><i class="zmdi zmdi-account"></i> View Performance and Personal Info</a>
                    </li>
                    <li>
                        <a href="studentChangePassword.php"><i class="zmdi zmdi-lock"></i> Change Password</a>
                    </li>
                    <li>
                        <a href="logout.php"><i class="zmdi zmdi-power"></i> Logout</a>
                    </li>
                </ul>
            </aside>
            <section id="content">
                <div class="container">
                    <div class="block-header">
                        <h1>
                            Overall Grades
                        </h1>

                       
                            
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class='card'>                                  
                                <div class="card-body lcb-form">
                                    <div id="chartcontainer"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class='card'>                                  
                                <div class="card-body lcb-form">
                                    <div id="chartcontainer2"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                
                </div>
            
            </section>
        </section>

        


        <?php include_once '../footer.php' ?>

        <!-- Page Loader -->
        <!-- <div class="page-loader">
            <div class="preloader pls-blue">
                <svg class="pl-circular" viewBox="25 25 50 50">
                    <circle class="plc-path" cx="50" cy="50" r="20" />
                </svg>

                <p>Please wait...</p>
            </div>
        </div> -->


        

        <!-- Older IE warning message -->
        <!--[if lt IE 9]>
            <div class="ie-warning">
                <h1 class="c-white">Warning!!</h1>
                <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers to access this website.</p>
                <div class="iew-container">
                    <ul class="iew-download">
                        <li>
                            <a href="http://www.google.com/chrome/">
                                <img src="img/browsers/chrome.png" alt="">
                                <div>Chrome</div>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.mozilla.org/en-US/firefox/new/">
                                <img src="img/browsers/firefox.png" alt="">
                                <div>Firefox</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://www.opera.com">
                                <img src="img/browsers/opera.png" alt="">
                                <div>Opera</div>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.apple.com/safari/">
                                <img src="img/browsers/safari.png" alt="">
                                <div>Safari</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                                <img src="img/browsers/ie.png" alt="">
                                <div>IE (New)</div>
                            </a>
                        </li>
                    </ul>
                </div>
                <p>Sorry for the inconvenience!</p>
            </div>
        <![endif]-->

        <!-- Javascript Libraries -->
        <script src="../vendors/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="../vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

        <script src="../vendors/bower_components/flot/jquery.flot.js"></script>
        <script src="..vendors/bower_components/flot/jquery.flot.resize.js"></script>
        <script src="../vendors/bower_components/flot.curvedlines/curvedLines.js"></script>
        <script src="../vendors/sparklines/jquery.sparkline.min.js"></script>
        <script src="../vendors/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js"></script>

        <script src="../vendors/bower_components/moment/min/moment.min.js"></script>
        <script src="../vendors/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
        <script src="../vendors/bower_components/simpleWeather/jquery.simpleWeather.min.js"></script>
        <script src="../vendors/bower_components/Waves/dist/waves.min.js"></script>
        <script src="../vendors/bootstrap-growl/bootstrap-growl.min.js"></script>
        <script src="../vendors/bower_components/sweetalert2/dist/sweetalert2.min.js"></script>
        <script src="../vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>

        <script src="../js/highcharts.js"></script>
        <script src="../js/exporting.js"></script>

        <!-- Placeholder for IE9 -->
        <!--[if IE 9 ]>
            <script src="vendors/bower_components/jquery-placeholder/jquery.placeholder.min.js"></script>
        <![endif]-->

        <script src="../js/app.min.js"></script>

        <script type="text/javascript">
            $(function(){
                var data1 = <?php echo $click ?>;
                var data2 = <?php echo $click1 ?>;
                var data3 = <?php echo $click2 ?>;
                var data4 = <?php echo $click3 ?>;

                var data5 = <?php echo $click4 ?>;
                var data6 = <?php echo $click5 ?>;
                var data7 = <?php echo $click6 ?>;
                var data8 = <?php echo $click7 ?>;
                var data9 = <?php echo $click8 ?>;
                var data10 = <?php echo $click9 ?>;
                var data11 = <?php echo $click10 ?>;
                var data12 = <?php echo $click11 ?>;

                var data13 = <?php echo $click12 ?>;
                var data14 = <?php echo $click13 ?>;
                var data15 = <?php echo $click14 ?>;
                var data16 = <?php echo $click15 ?>;
                var data17 = <?php echo $click16 ?>;
                var data18 = <?php echo $click17 ?>;
                var data19 = <?php echo $click18 ?>;
                var data20 = <?php echo $click19 ?>;

                var data21 = <?php echo $click20 ?>;
                var data22 = <?php echo $click21 ?>;
                var data23 = <?php echo $click22 ?>;
                var data24 = <?php echo $click23 ?>;
                var data25 = <?php echo $click24 ?>;
                var data26 = <?php echo $click25 ?>;
                var data27 = <?php echo $click26 ?>;
                var data28 = <?php echo $click27 ?>;

                var data29 = <?php echo $click28 ?>;
                var data30 = <?php echo $click29 ?>;
                var data31 = <?php echo $click30 ?>;
                var data32 = <?php echo $click31 ?>;
                var data33 = <?php echo $click32 ?>;
                var data34 = <?php echo $click33 ?>;
                var data35 = <?php echo $click34 ?>;
                var data36 = <?php echo $click35 ?>;
                
                $('#chartcontainer').highcharts({
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'Final Average Grade in each Year Level'
                    },
                    yAxis: [{
                        title: {
                                text: 'Average Grade (avg)'
                        }
                    }, {
                            opposite: true,
                            title: {
                                    text: ' '
                            }
                    }],
                    plotOptions: {
                        column: {
                            borderRadius: 5
                        }
                    },
                    series: [
                        {
                            name: 'Grade 7',
                            data: [data1]
                        }, 
                        {
                            name: 'Grade 8',
                            data: [data2]
                        }, 
                        {
                            name: 'Grade 9',
                            data: [data3]
                        }, 
                        {
                            name: 'Grade 10',
                            data: [data4]
                        }
                    ]

                });

                $('#chartcontainer2').highcharts({
                    chart: {
                      type: 'column'
                  },
                  title: {
                      text: 'Average Grade each Subject per Year Level'
                  },
                  xAxis: {
                      categories: [
                          'English',
                          'Science',
                          'Mathematics',
                          'Filipino',
                          'AP',
                          'TLE',
                          'MAPEH',
                          'ESP'
                      ],
                      crosshair: true
                  },
                  yAxis: [{
                      min: 0,
                      title: {
                          text: 'Average Grade (avg)'
                      }
                  }, {
                        opposite: true,
                        title: {
                                text: ' '
                        }
                  }],
                  tooltip: {
                      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                      pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                          '<td style="padding:0"><b>{point.y:.1f} avg</b></td></tr>',
                      footerFormat: '</table>',
                      shared: true,
                      useHTML: true
                  },
                  plotOptions: {
                      column: {
                          pointPadding: 0.2,
                          borderWidth: 0
                      }
                  },
                  series: [{
                      name: 'Grade 7',
                      data: [data5, data6, data7, data8, data9, data10, data11, data12]

                  }, {
                      name: 'Grade 8',
                      data: [data13, data14, data15, data16, data17, data18, data19, data20]

                  }, {
                      name: 'Grade 9',
                      data: [data21, data22, data23, data24, data25, data26, data27, data28]

                  }, {
                      name: 'Grade 10',
                      data: [data29, data30, data31, data32, data33, data34, data35, data36]

                  }]
                });

                $('#sy').change(function(){
                    var sys = $(this).val();
                    $.ajax({
                        url:"ajaxJQuery/loadClass.php",
                        method:"POST",
                        data:{sys:sys},
                        success:function(data){
                            $('#chartcontainer').html(data);
                        }
                    });
                });

            });
        </script>
    </body>
  
<!-- Mirrored from byrushan.com/projects/ma/1-7-1/jquery/light/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 01 May 2017 06:33:25 GMT -->
</html>
