<?php
    session_start();
    include_once 'dbconnect.php';
?>
<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie9"><![endif]-->
    
<!-- Mirrored from byrushan.com/projects/ma/1-7-1/jquery/light/widget-templates.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 01 May 2017 06:35:54 GMT -->
<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Grades</title>

        <!-- Vendor CSS -->
        <link href="../vendors/bower_components/animate.css/animate.min.css" rel="stylesheet">
        <link href="../vendors/bower_components/sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
        <link href="../vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css" rel="stylesheet">
        <link href="../vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet"> 
        <link href="../vendors/bower_components/dropzone/dist/min/dropzone.min.css" rel="stylesheet"> 
        <link href="../vendors/bower_components/chosen/chosen.css" rel="stylesheet">
        <link href="../vendors/bower_components/datatables.net-dt/css/jquery.dataTables.min.css" rel="stylesheet">
        <link href="../css/app_1.min.css" rel="stylesheet">
        <link href="../css/app_2.min.css" rel="stylesheet">
    </head>
    <body>
        <header id="header" class="clearfix" data-ma-theme="green">
            <ul class="h-inner">
                <li class="hi-trigger ma-trigger" data-ma-action="sidebar-open" data-ma-target="#sidebar">
                    <div class="line-wrap">
                        <div class="line top"></div>
                        <div class="line center"></div>
                        <div class="line bottom"></div>
                    </div>
                </li>

                <li class="hi-logo hidden-xs">
                    <a href="index.html">Lugait National High School</a>
                </li>

                <li class="pull-right">
                    <ul class="hi-menu">

                        <li data-ma-action="search-open">
                            <a href="#"><i class="him-icon zmdi zmdi-search"></i></a>
                        </li>
                    </ul>
                </li>
            </ul>

            <!-- Top Search Content -->
            <div class="h-search-wrap">
                <div class="hsw-inner">
                    <i class="hsw-close zmdi zmdi-arrow-left" data-ma-action="search-close"></i>
                    <input type="text">
                </div>
            </div>
        </header>

        <section id="main">
            <aside id="sidebar" class="sidebar c-overflow">
                <div class="s-profile">
                    <a href="#" data-ma-action="profile-menu-toggle">
                        <div class="sp-pic">
                            <img src="img/demo/profile-pics/1.jpg" alt="">
                        </div>

                        <div class="sp-info">
                            <?php echo $_SESSION['Name']; ?>

                            <i class="zmdi zmdi-caret-down"></i>
                        </div>
                    </a>

                    <ul class="main-menu">
                        <li>
                            <a href="profile-about.html"><i class="zmdi zmdi-account"></i> View Profile</a>
                        </li>
                        <li>
                            <a href="#"><i class="zmdi zmdi-lock"></i> Change Password</a>
                        </li>
                        <li>
                            <a href="logout.php"><i class="zmdi zmdi-time-restore"></i> Logout</a>
                        </li>
                    </ul>
                </div>

                <ul class="main-menu">
                    <li>
                        <a href="studentHome.php"><i class=zmdi zmdi-home></i> Home</a>
                    </li>
                    <li>
                        <a href="studentHome.php"><i class="zmdi zmdi-home"></i> Performance</a>
                    </li>
                    <li>
                        <a href="studentBook.php"><i class="zmdi zmdi-book"></i> Books</a>
                    </li> 
                </ul>
            </aside>

            <section id="content">
                <div class="container">
                    <!-- Colored Headers -->
                    <div class="block-header">
                        <h1><i class="zmdi zmdi-collection-text"></i> Performance
                        </h1>
                        <div class="dropdown">
                            <button class="btn btn-default dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">Year Level
                                <span class="caret"></span></button>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                                            <li> <?php
                                                $query = mysqli_query($con, "SELECT * from yearlevel");
                                                while($row = mysqli_fetch_array($query)){
                                                    echo "
                                                        <option>".$row['year_lvl_title']."</option>
                                                    ";
                                                }
                                            ?>
                                
                                            </li>
                            </ul>
                         </div>
                        <div class="dropdown">
                            <div class="actions">

                                                    <select class="selectpicker form-control" name="year_lvl_title" id="year_lvl_title">
                                                        <option>LEVEL</option>
                                            <?php
                                                $query = mysqli_query($con, "SELECT * from yearlevel");
                                                while($row = mysqli_fetch_array($query)){
                                                    echo "
                                                        <option>".$row['year_lvl_title']."</option>
                                                    ";
                                                }
                                            ?>
                                
                                                    </select>
                                                </div>
                        </div>

                        <div class="actions">
                            <button  href="#modalColor" data-toggle="modal" class="btn btn-default btn-lg"><i class="zmdi zmdi-collection-text"></i> Year Level</button>
                            <div class="col-sm-4">
                            </div>
                        </div>
                    </div>

                    
                    <div class="card">
                        <div class="table-responsive">
                            <table id="data-table-basic" class="table table-striped table-bordered table-nowrap dataTable">
                                <thead>
                                    <th>Subject Area</th>
                                    <th>Grade</th>
                                    <th>Remarks</th>
                                </thead>
                                <tbody>
                                    <?php
                                        $query = mysqli_query($con, "SELECT control_id, subj_title, grade_value, grade_remarks from performance");
                                        while($row = mysqli_fetch_array($query)){
                                            $temp = $row[0];
                                            echo "
                                            <tr>
                                                <td>".$row[1]."</td>
                                                <td>".$row[2]."</td>
                                                <td>".$row[3]."</td>
                                            </tr>    
                                            ";
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <br/>
                    <br/>
                </div>
                <button href='#modalColor' data-toggle="modal" class="btn btn-float bgm-green m-btn"><i class="zmdi zmdi-library"></i></button>
            </section>
        </section>
        
        <footer id="footer">
            Copyright &copy; 2015 Material Admin
            
            <ul class="f-menu">
                <li><a href="#">Home</a></li>
                <li><a href="#">Dashboard</a></li>
                <li><a href="#">Reports</a></li>
                <li><a href="#">Support</a></li>
                <li><a href="#">Contact</a></li>
            </ul>
        </footer>
        <!-- Javascript Libraries -->
        <script src="../vendors/bower_components/jquery/dist/jquery.min.js"></script>

        <script src="../vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

        <script src="../vendors/bower_components/flot/jquery.flot.js"></script>

        <script src="../vendors/bower_components/flot/jquery.flot.resize.js"></script>

        <script src="../vendors/bower_components/flot.curvedlines/curvedLines.js"></script>

        <script src="../vendors/sparklines/jquery.sparkline.min.js"></script>

        <script src="../vendors/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js"></script>

        <script src="../vendors/bower_components/moment/min/moment.min.js"></script>

        <script src="../vendors/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>

        <script src="../vendors/bower_components/simpleWeather/jquery.simpleWeather.min.js"></script>

        <script src="../vendors/bower_components/Waves/dist/waves.min.js"></script>

        <script src="../vendors/bootstrap-growl/bootstrap-growl.min.js"></script>
        <script src="../vendors/bower_components/sweetalert2/dist/sweetalert2.min.js"></script>
        <script src="../vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
        <script src="../vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
        <script src="../vendors/bower_components/dropzone/dist/min/dropzone.min.js"></script>

        <script src="../vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.js"></script>
        <script src="../vendors/bower_components/nouislider/distribute/nouislider.min.js"></script>
        <script src="../vendors/bower_components/typeahead.js/dist/typeahead.bundle.min.js"></script>
        <script src="../vendors/summernote/dist/summernote-updated.min.js"></script>
        <script src="../vendors/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
        <!-- Placeholder for IE9 -->
        <!--[if IE 9 ]>
            <script src="vendors/bower_components/jquery-placeholder/jquery.placeholder.min.js"></script>
        <![endif]-->

        <script src="../vendors/bower_components/chosen/chosen.jquery.js"></script>
        <script src="../vendors/bower_components/jquery-placeholder-mask-plugin/dist/jquery.mask.min.js"></script>
        <script src="../vendors/fileinput/fileinput.min.js"></script>
        <script src="../vendors/farbtastic/farbtastic.min.js"></script>
        <script src="../js/app.min.js"></script>
        
        <script type="text/javascript">
            $(document).ready(function(){
                $('#data-table-basic').DataTable();


                $(document).on('click', '.details', function(){
                    var control_id = $(this).attr("id");
                    
                    $.ajax({  
                         url:"ajaxJQuery/viewSubject.php",  
                         method:"POST",  
                         data:{control_id:grade_value},  
                         
                         success:function(data){
                              $('#Details').html(data);
                              $('#dataModal').modal('show');  
                         }  
                    });  
                });


                $(document).on('click', '.edit_subject', function(){
                    var control_id = $(this).attr("id");
                    
                    $.ajax({  
                         url:"ajaxJQuery/selectSubject.php",  
                         method:"POST",  
                         data:{control_id:grade_value},  
                         dataType:"json",
                         success:function(data){
                              $('#subj_title1').val(data.subj_title);
                              $('#grade_value1').val(data.grade_value);
                              $('#modalColor1').modal('show');  
                         }  
                    });  
                });


                $('#myForm').on("submit", function(event){
                    event.preventDefault();
                    if($('#subj_title').val() == '')  
                    {  
                         alert("Title is required");  
                    }  
                    else if($('#subj_description').val() == '')  
                    {  
                         alert("Capacity is required");  
                    }  
                    else if($('#subj_unit').val() == '')  
                    {  
                         alert("Maximun grade is required");  
                    }  
                    else if($('#subj_hrs_per_week').val() == '')  
                    {  
                         alert("Minimum grade is required");  
                    }
                    else if($('#year_lvl_title').val() == 'LEVEL')  
                    {  
                         alert("You must select year level");  
                    }  
                    else  
                    {  
                         $.ajax({  
                              url:"ajaxJQuery/insertSubject.php",  
                              method:"POST",  
                              data:$('#myForm').serialize(),  
                              beforeSend:function(){  
                                   $('#insert1').val("Inserting..");   
                              },  
                              success:function(data){ 
                                   swal("Good job", "You successfully added the subject", "success");  
                                   $('#myForm')[0].reset();    
                                   $('#data-table-basic').html(data); 
                              }  

                         });  
                    }
                });

                $('#myForm1').on("submit", function(event){
                    event.preventDefault();
                    if($('#subj_title1').val() == '')  
                    {  
                        $('#subj_title1').focus();
                         alert("Title is required");  
                    }  
                    else if($('#subj_description1').val() == '')  
                    {
                        $('#subj_description1').focus();  
                         alert("Description is required");  
                    }  
                    else if($('#subj_unit1').val() == '')  
                    {  
                        $('#subj_unit1').focus();
                         alert("Unit is required");  
                    }  
                    else if($('#subj_hrs_per_week1').val() == '')  
                    {  
                        $('#subj_hrs_per_week1').focus();
                         alert("Hours/Week is required");  
                    }
                    else if($('#year_lvl_title1').val() == 'LEVEL')  
                    {  
                        $('#year_lvl_title1').focus();
                         alert("You must select year level");  
                    }  
                    else  
                    {  
                         $.ajax({  
                              url:"ajaxJQuery/updateSubject.php",  
                              method:"POST",  
                              data:$('#myForm1').serialize(),  
                              beforeSend:function(){  
                                   $('#insert1').val("Inserting..");   
                              },  
                              success:function(data){ 
                                   swal("Good job", "You successfully updated the subject", "success");  
                                   $('#myForm1')[0].reset();    
                                   $('#data-table-basic').html(data); 
                              }

                         });  
                    }
                });
            });



        </script>
    </body>

</html> 