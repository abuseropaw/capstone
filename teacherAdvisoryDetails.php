<?php
    session_start();
    include_once 'dbconnect.php';
    include_once 'PHPExcel-1.8.1/Classes/PHPExcel.php';
    include 'ajaxJQuery/globalFunction.php';
    $id = mysqli_real_escape_string($con,$_GET['id']);
    $name1 = mysqli_real_escape_string($con,$_GET['name']);
    $level = mysqli_real_escape_string($con,$_GET['levels']);
    

    $items = mysqli_fetch_row(mysqli_query($con, "SELECT * from sectionoverall where section_id='".$id."'"));


    
    
?>
<!DOCTYPE html>

    <!-- HEAD -->
    <?php include_once 'head.php'; ?>
    <!-- HEAD   -->
    <body >
        <!-- HEADER -->
        <?php include_once 'header.php'; ?>
        <!-- HEADER -->

        <section id="main">
            
           
            
           

                <div class="container">

                    <div class="block-header">
                        <h1><i class="zmdi zmdi-account-box"></i> <?php echo $name1; ?></h1> 
                    </div>

                    <!-- Add button -->
                    
                    <div class="card">
                        

                        <div class="card-body card-padding">
                            <div role="tabpanel">
                                <ul class="tab-nav" role="tablist" data-tab-color="green">
                                    <li class="active"><a href="#monitor" aria-controls="monitor" role="tab" data-toggle="tab"><b>Overall</b></a></li>
                                    
                                    <li><a href="#learner" aria-controls="learner" role="tab"
                                                          data-toggle="tab"><b>Learners</b></a></li>
                                    <li><a href="#home11" aria-controls="home11" role="tab"
                                                          data-toggle="tab"><b>Grades</b></a></li>
                                    <li><a href="#profile11" aria-controls="profile11" role="tab" data-toggle="tab"><b>Attendance</b></a>
                                    </li>
                                    <li><a href="#messages11" aria-controls="messages11" role="tab" data-toggle="tab"><b>Books</b></a>
                                    </li>

                                </ul>

                                <!-- GRADE --><!-- GRADE --><!-- GRADE --><!-- GRADE --><!-- GRADE --><!-- GRADE -->
                                <div class="tab-content">
                                    
                                    <?php
                                        include_once 'teacherAdvisoryDetails_Overall.php';
                                    ?>


                                    <?php include_once 'teacherAdvisoryDetails_Learners.php'; ?>
                                    <!-- GRADE --><!-- GRADE --><!-- GRADE --><!-- GRADE --><!-- GRADE --><!-- GRADE -->
                                    <?php include_once 'teacherAdvisoryDetails_Grades.php'; ?>
                                    
                                    <!-- GRADE --><!-- GRADE --><!-- GRADE --><!-- GRADE --><!-- GRADE --><!-- GRADE -->

                                    <!-- ATTENDANCE --><!-- ATTENDANCE --><!-- ATTENDANCE --><!-- ATTENDANCE -->
                                    <?php include_once 'teacherAdvisoryDetails_Attendance.php'; ?>
                                    <!-- ATTENDANCE --><!-- ATTENDANCE --><!-- ATTENDANCE --><!-- ATTENDANCE -->

                                    <!-- BOOKS --><!-- BOOKS --><!-- BOOKS --><!-- BOOKS --><!-- BOOKS --><!-- BOOKS -->
                                    <?php include_once 'teacherAdvisoryDetails_Books.php'; ?>
                                    <!-- BOOKS --><!-- BOOKS --><!-- BOOKS --><!-- BOOKS --><!-- BOOKS --><!-- BOOKS -->
                                    
                            </div>
                        </div>
                    </div>
                        
                    </div>
                    
                </div>
                
        </section>

        

        <!-- INSERT BOOKSACQUIRED -->
        <div class="modal fade" id="book" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <form method="POST" id="insert_form2">
                        <div class="modal-header">
                            <CENTER>
                                <h3 >Book Acquisition/Return</h3>
                            </CENTER>

                        </div>
                            
                        <div class="modal-body">
                            <div class="col-sm-12">
                                <div class="form-group fg-line">
                                    <select class="selectpicker form-control" data-live-search="true" name='type' id='type' required>
                                        <option value="">Choose</option>
                                        <option>Issue</option>
                                        <option>Return</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="form-group fg-line">
                                    <input type='text' class="form-control date-picker"
                               placeholder="Date issued/return" name="date" id="date">
                                </div>
                            </div>
                            
                            <div class="col-sm-12" hidden>
                                <div class="form-group fg-line">
                                    <input type="text" name="remarksBA" id="remarksBA" class="form-control" placeholder="Remarks">

                                    <input type="hidden" name="section_id" id="section_id" value="<?php 
                                        echo $id; 
                                    ?>">
                                    <input type="hidden" name="level" id="level" value="<?php 
                                        echo $level; 
                                    ?>">
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">
                            
                            <button type="submit" name="insertBook" id="insertBook" class="btn btn-success">SUBMIT</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal">CLOSE
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- UN-ENROLL LEARNER -->
        <div class="modal fade" id="modalRemoveLearner" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <form id="removeLearner" method="post">
                        <div class="modal-header">
                            <h4 class="modal-title">REMOVE LEARNER</h4>
                        </div>


                        <div class="modal-body">
                            <div class='col-sm-12'>
                                <div class='form-group fg-line'>
                                    <input type="text" name="student1" id="student1" class="form-control" disabled>
                                    <input type="hidden" name="sid" id="sid" value="<?php echo $id ?>">
                                    <input type="hidden" name="important_id1" id="important_id1">
                                    <input type="hidden" name="student_id" id="student_id">
                                    <input type="hidden" name="level" id="level" value="<?php echo $level ?>">
                                </div>
                            </div>
                            
                        </div>
                        <div class="modal-footer">
                            
                            <button type="submit" name="remove" id="remove" class="btn btn-success btn-lg">YES</button>
                            
                            <button type="button" class="btn btn-danger btn-lg" data-dismiss="modal">NO
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- EDIT LEARNER -->
        <div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <form id="editLearner" method="post">
                        <div class="modal-header">
                            <h4 class="modal-title">EDIT LEARNER</h4>
                        </div>


                        <div class="modal-body">
                            <div class='col-sm-12'>
                                <div class='form-group fg-line'>
                                    <input type="text" name="student2" id="student2" class="form-control" disabled>
                                    <input type="hidden" name="sid2" id="sid2" value="<?php echo $id ?>">
                                    <input type="hidden" name="important_id2" id="important_id2">
                                    <input type="hidden" name="student_id2" id="student_id2">
                                    <input type="hidden" name="level2" id="level2" value="<?php echo $level ?>">
                                </div>
                            </div>
                            <div class='col-sm-12'>
                                <div class='form-group fg-line'>
                                    <select class="form-control selectpicker" name="remarks1" id="remarks1" >
                                        
                                        <option value="T/I">Transferred In</option>
                                        <option value="T/O">Transferred Out</option>
                                        <option value="LE">Late Enrolment</option>
                                        <option value="DRP">Drop Out</option>
                                        <option value="Pending TI">Pending TI</option>
                                    </select>
                                    
                                </div>
                            </div>
                            <div id='_schoolName' class='col-sm-12'>
                                <div class='form-group fg-line'>
                                    <input type="text" name="school" id="school" class="form-control" placeholder="Reason/School Name">
                                </div>
                            </div>
                            <div id='_reasonID' class='col-sm-12' hidden>
                                <div class='form-group fg-line'>
                                    <select class="selectpicker" name="reason" id="reason">
                                        <option></option>
                                        <optgroup label="Domestic-Related Factors">
                                            <option>Had to take care of siblings</option>
                                            <option>Early marriage/pregnancy</option>
                                            <option>Parent's attitude towards schooling</option>
                                            <option>Family problem</option>
                                        </optgroup>
                                        <optgroup label="Individual-Related Factors">
                                            <option>Illness</option>
                                            <option>Overage</option>
                                            <option>Death</option>
                                            <option>Drug Abuse</option>
                                            <option>Poor academic performance</option>
                                            <option>Lack of interest/Distractions</option>
                                            <option>Hunger/Malnutrition</option>
                                        </optgroup>
                                        <optgroup label="School-Related Factors">
                                            <option>Teacher Factor</option>
                                            <option>Physical condition of classroom</option>
                                            <option>Peer influence</option>
                                        </optgroup>
                                        <optgroup label="Geographic/Environmental">
                                            <option>Distance between home and school</option>
                                            <option>Armed conflict (incl. Tribal wars & clanfeuds)</option>
                                            <option>Calamities/Disasters</option>
                                        </optgroup>
                                        <optgroup label="Financial-Related">
                                            <option>Distance between home and school</option>
                                            <option>Armed conflict (incl. Tribal wars & clanfeuds)</option>
                                            <option>Calamities/Disasters</option>
                                        </optgroup>
                                        <optgroup label="Financial-Related">
                                            <option>Child laber</option>
                                        </optgroup>
                                        <option value="Others">Others</option>
                                    </select>
                                </div>
                            </div>
                            <div id='_others' class='col-sm-12' hidden>
                                <div class='form-group fg-line'>
                                    <input type="text" name="others" id="others" class="form-control" placeholder="Specify reason">
                                </div>
                            </div>
                            <div class='col-sm-12'>
                                <div class="form-group fg-line">
                                    <input type='text' class="form-control date-picker" placeholder="Date of remarks" name="tiDate" id="tiDate" required>
                                </div>
                            </div>
                            
                        </div>
                        <div class="modal-footer">
                            
                            <button type="submit" name="edit" id="edit" class="btn btn-success btn-lg">SAVE CHANGES</button>
                            
                            <button type="button" class="btn btn-danger btn-lg" data-dismiss="modal">DISCARD
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- EDIT BOOKS ACQUIRED -->
        <div class="modal fade" id="editBookAcquired" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <form id="editBook" method="post">
                        <div class="modal-header">
                            <h4 class="modal-title">EDIT BOOK ACQUISITION</h4>
                        </div>


                        <div class="modal-body">
                            <div class='col-sm-12'>
                                <div class='form-group fg-line'>
                                    <select class="selectpicker form-control" name="subject" id="subject">
                                    <?php
                                        $query = mysqli_query($con, "SELECT * from bookDetails where Level='".$level."'");
                                        while($row = mysqli_fetch_array($query)){
                                            echo "<option value='".$row[0]."'>
                                                ".$row[1]."

                                            </option>";
                                        }

                                    ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group fg-line">
                                    <select class="selectpicker form-control" data-live-search="true" name='type1BA' id='type1BA'>
                                        <option>Choose</option>
                                        <option>Issue</option>
                                        <option>Return</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="form-group fg-line">
                                    <input type='text' class="form-control date-picker"
                               placeholder="Date issued/return" name="date1" id="date1" required>
                                </div>
                            </div>
                            
                            <div class="col-sm-12" id="divRemarks">
                                <div class="form-group fg-line">
                                    <input type="hidden" name="studentID1" id="studentID1">
                                    <select class="selectpicker form-control" data-live-search="true" name="remarksBA1" id="remarksBA1">
                                        <option></option>
                                        <option value="FM">Force Majeure</option>
                                        <option value="TDO">Transferred/Dropout</option>
                                        <option value="NEG">Negligence</option>
                                    </select>

                                    <input type="hidden" name="section_id1" id="section_id1" value="<?php 
                                        echo $id; 
                                    ?>">
                                    <input type="hidden" name="level1" id="level1" value="<?php 
                                        echo $level; 
                                    ?>">
                                    
                                </div>
                            </div>
                            
                        </div>
                        <div class="modal-footer">
                            
                            <button type="submit" name="editBookAcquiredsss" id="editBookAcquiredsss" class="btn btn-success btn-lg">SAVE CHANGES</button>
                            
                            <button type="button" class="btn btn-danger btn-lg" data-dismiss="modal">DISCARD
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>


        <div class="modal fade" id="deleteBookAcquired" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <form id="deleteBook" method="post">
                        <div class="modal-header">
                            <h4 class="modal-title"><b>DELETE BOOK ACQUISITION</b></h4>
                        </div>


                        <div class="modal-body">
                            <div class='col-sm-12'>
                                <div class='form-group fg-line'>
                                    <select class="selectpicker form-control" name="subjectDelete" id="subjectDelete">
                                    <?php
                                        $query = mysqli_query($con, "SELECT * from bookDetails where Level='".$level."'");
                                        while($row = mysqli_fetch_array($query)){
                                            echo "<option value='".$row[0]."'>
                                                ".$row[1]."

                                            </option>";
                                        }

                                    ?>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="col-sm-12">
                                <div class="form-group fg-line">
                                    <input type="hidden" name="studentIDDelete" id="studentIDDelete">
                                   

                                    <input type="hidden" name="section_idDelete" id="section_idDelete" value="<?php 
                                        echo $id; 
                                    ?>">
                                    <input type="hidden" name="levelDelete" id="levelDelete" value="<?php 
                                        echo $level; 
                                    ?>">
                                    
                                </div>
                            </div>
                            
                        </div>
                        <div class="modal-footer">
                            
                            <button type="submit" name="editBookAcquiredsss" id="editBookAcquiredsss" class="btn btn-success btn-lg">SAVE CHANGES</button>
                            
                            <button type="button" class="btn btn-danger btn-lg" data-dismiss="modal">DISCARD
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- IMPORT GRADES -->
        


        


        <div id="gradeDetails" class="modal fade" data-backdrop="static" data-keyboard="false">  
            <div class="modal-dialog">  
               <div class="modal-content">  
                    <div class="modal-header">  
                         <button type="button" class="close" data-dismiss="modal">&times;</button>  
                         <h4 class="modal-title"><b>GRADE DETAILS</b></h4>  
                    </div>  
                    <div class="modal-body" id="Details">  
                    </div>  
                    
               </div>  
            </div>  
        </div>



        
        <div id="loading" class="modal fade" data-backdrop="static" data-keyboard="false">  
            <div class="modal-dialog modal-sm">  
               <div class="modal-content">
               <center>   
                    <div class="modal-body" id="Details">
                         
                        <div class="preloader pl-xxl">
                            
                                <svg class="pl-circular" viewBox="25 25 50 50">
                                    <circle class="plc-path" cx="50" cy="50" r="20"/>
                                </svg>
                            
                        </div> 
                       <center><h4>Processing...Please wait</h4></center>
                    </div>  
                </center>
               </div>  
            </div>  
        </div>

        <div class="modal fade" id="modalEnrollLearner" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <form id="enroll" method="post">
                        <div class="modal-header">
                            <h4 class="modal-title">Enroll LEARNER</h4>
                        </div>


                        <div class="modal-body">
                            <div class='col-sm-12'>
                                <div class='form-group fg-line'>
                                    <input type="text" name="NameofStudent" id="NameofStudent" class="form-control" disabled>
                                    <input type="hidden" name="sectionIDS" id="sectionIDS">
                                    <input type="hidden" name="studentID" id="studentID">
                                    
                                </div>
                            </div>
                            
                        </div>
                        <div class="modal-footer">
                            
                            <button type="submit" name="remove" id="remove" class="btn btn-success btn-lg">YES</button>
                            
                            <button type="button" class="btn btn-danger btn-lg" data-dismiss="modal">NO
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modalAddAttendance" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <form id="addAttendanceForm" method="post">
                        <div class="modal-header">
                            <h4 class="modal-title"><b>ADD ATTENDANCE TO ALL</b></h4>
                        </div>
                        <div class="modal-body">
                            <div class='col-sm-12'>
                                <div class='form-group fg-line'>
                                    <input type="number" name="present" id="present" min="0" class="form-control" placeholder="Total Days of Present" required>
                                    <input type="hidden" name="monthID" id="monthID">
                                    <input type="hidden" name="attendancesectionID" id="attendancesectionID">
                                </div>
                            </div>
                            <div class='col-sm-12'>
                                <div class='form-group fg-line'>
                                    <input type="number" name="absent" id="absent" min="0" class="form-control" placeholder="Total Days of Absent">
                                    
                                </div>
                            </div>
                            <div class='col-sm-12'>
                                <div class='form-group fg-line'>
                                    <input type="number" name="tardy" id="tardy" min="0" class="form-control" placeholder="Total Days of Tardy">
                                    
                                </div>
                            </div>

                        </div>
                        
                        <div class="modal-footer">
                            
                            <button type="submit" name="add" id="add" class="btn btn-success btn-lg">YES</button>
                            
                            <button type="button" class="btn btn-danger btn-lg" data-dismiss="modal">NO
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modalEditAttendance" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <form id="editAttendanceForm" method="post">
                        <div class="modal-header">
                            <h4 class="modal-title"><b>EDIT ATTENDANCE</b></h4>
                        </div>
                        <div class="modal-body">
                            <div class='col-sm-12'>
                                <div class='form-group fg-line'>
                                    <input type="text" name="StudentName" id="StudentName" class="form-control" disabled>
                                </div>
                            </div>
                            <div class='col-sm-12'>
                                <div class='form-group fg-line'>
                                    <input type="number" name="present1" id="present1"  class="form-control" placeholder="Total Days of Present" required>
                                    <input type="hidden" name="monthID1" id="monthID1">
                                    <input type="hidden" name="attendancesectionID1" id="attendancesectionID1">
                                    <input type="hidden" name="controlID" id="controlID">
                                </div>
                            </div>
                            <div class='col-sm-12'>
                                <div class='form-group fg-line'>
                                    <input type="number" name="absent1" id="absent1"  class="form-control" placeholder="Total Days of Absent">
                                    
                                </div>
                            </div>
                            <div class='col-sm-12'>
                                <div class='form-group fg-line'>
                                    <input type="number" name="tardy1" id="tardy1" class="form-control" placeholder="Total Days of Tardy">
                                    
                                </div>
                            </div>
                            <div class='col-sm-12'>
                                <div class='form-group fg-line'>
                                    <select class="selectpicker form-control" name="remarks" required>
                                        <option value=""></option>
                                        <option>Yes</option>
                                        <option>No</option>
                                    </select>
                                    
                                </div>
                            </div>
                            

                        </div>
                        
                        <div class="modal-footer">
                            
                            <button type="submit" name="add" id="add" class="btn btn-success btn-lg">YES</button>
                            
                            <button type="button" class="btn btn-danger btn-lg" data-dismiss="modal">NO
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- FOOTER -->
        <?php include_once 'footer.php' ?>
        <!-- FOOTER -->

        <!-- Javascript Libraries -->
        <?php include_once 'scripts.php'; ?>
        <!-- Javascript Libraries -->
        <script type="text/javascript" src="js/Notification.js"></script>


        <script type="text/javascript">
            $(function(){



                var section_id = "<?php echo $id; ?>";
                var section_name = "<?php echo $name1; ?>";


                $.ajax({
                    url:"ajaxJQuery/teacherAnalytics.php?type=status",
                    data:{id:section_id},
                    method:"get",
                    dataType:"json",
                    success:function(data){
                      
                        $('#chartcontainer1').highcharts({
                            chart: {
                              type: 'column'
                          },
                          title: {
                              text: ''+section_name+' Enrollment'
                          },
                          xAxis: {
                              categories: [
                                  'Transferred In',
                                  'Late Enrollment',
                                  'Dropout',
                                  'Transferred Out',
                                  'Pending Transferred In'
                              ],
                              crosshair: true
                          },
                          yAxis: {
                              min: 0,
                              title: {
                                  text: 'Total Learner'
                              }
                          },
                          tooltip: {
                              headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                              pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                  '<td style="padding:0"><b>{point.y:.1f} Learner</b></td></tr>',
                              footerFormat: '</table>',
                              shared: true,
                              useHTML: true
                          },
                          plotOptions: {
                              column: {
                                  pointPadding: 0.2,
                                  borderWidth: 0
                              }
                          },
                          series: [{
                              name: 'Male',
                              data: [data.TIM,data.LEM,data.DOM,data.TOM,data.PTIM]

                          }, {
                              name: 'Female',
                              data: [data.TIF,data.LEF,data.DOF,data.TOF,data.PTIF]

                          }]
                        });
                    }
                });

                $.ajax({
                    url:"ajaxJQuery/teacherAnalytics.php?type=summary",
                    data:{id:section_id},
                    method:"get",
                    dataType:"json",
                    success:function(data){
                      var irreg = data.irregular;
                      var prom = data.promoted;
                      var ret = data.retained;
                     
                        $('#chartcontainer2').highcharts({
                            chart: {
                                plotBackgroundColor: null,
                                plotBorderWidth: null,
                                plotShadow: false,
                                type: 'pie'
                            },
                            title: {
                                text: ''+section_name+' Summary Table'
                            },
                            tooltip: {
                                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                            },
                            plotOptions: {
                                pie: {
                                    allowPointSelect: true,
                                    cursor: 'pointer',
                                    dataLabels: {
                                        enabled: true,
                                        format: '<b>{point.name}</b>: {point.percentage:.1f} Learner',
                                        style: {
                                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                        }
                                    }
                                }
                            },
                          
                            series: [{
                                name: 'Learner',
                                colorByPoint: true,
                                data: [{
                                    name: 'Irregular',
                                    y: irreg
                                    
                                    
                                }, {
                                    name: 'Retained',
                                    y: ret
                                }, {
                                    
                                    name: 'Promoted',
                                    y: prom
                                }]

                            }]
                        });
                    }
                });


                $.ajax({
                    url:"ajaxJQuery/teacherAnalytics.php?type=attendance",
                    data:{id:section_id},
                    method:"get",
                    dataType:"json",
                    success:function(data){
                        var adaM = [];
                        var adaF = [];
                        var adaAverage = [];
                        var categ = [];
                        var totalM = 0;var totalF = 0;
                        var iter = 0
                        $.each(data, function(index, element) {
                            categ.push(element.Month);
                            adaM.push(parseFloat(element.adaM));
                            adaF.push(parseFloat(element.adaF));
                            totalM = totalM + parseFloat(element.adaM);
                            totalF = totalF + parseFloat(element.adaF);
                            adaAverage.push((parseFloat(element.adaM) + (parseFloat(element.adaF)))/2);
                            iter++;
                        });

                        $('#chartcontainer3').highcharts({
                            title: {
                                text: 'Monthly Average Attendance'
                            },
                            xAxis: {
                                categories: categ
                            },
                            yAxis: {
                              min: 0,
                              title: {
                                  text: 'Average'
                              }
                          },
                            tooltip: {
                              headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                              pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                  '<td style="padding:0"><b>{point.y:.1f} Learner</b></td></tr>',
                              footerFormat: '</table>',
                              shared: true,
                              useHTML: true
                          },
                            labels: {
                                items: [{
                                    html: '',
                                    style: {
                                        left: '50px',
                                        top: '18px',
                                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
                                    }
                                }]
                            },
                            series: [{
                                type: 'column',
                                name: 'Male',
                                data: adaM
                            }, {
                                type: 'column',
                                name: 'Female',
                                data: adaF
                            }, {
                                type: 'spline',
                                name: 'Average',
                                data: adaAverage,
                                marker: {
                                    lineWidth: 3,
                                    
                                    fillColor: 'white'
                                }
                            }, {
                                type: 'pie',
                                name: 'Total Average',
                                data: [{
                                    name: 'Male',
                                    y: totalM/iter,
                                   
                                }, {
                                    name: 'Female',
                                    y: totalF/iter,
                                    
                                }],
                                center: [100, 10],
                                size: 70,
                                showInLegend: true,
                                dataLabels: {
                                    enabled: true
                                }
                            }]
                        });
                    }
                });




            });


            $(document).ready(function() {

                $('#remarks1').on('change', function(){
                    var select = $(this).val();
                    if(select == "DRP")
                    {
                        $('#_reasonID').show();
                         $('#_schoolName').hide();
                    }
                    else if(select == "T/I" | select == "T/O" | select == "LE")
                    {
                        $('#_reasonID').hide();
                        $('#_schoolName').show();
                        $('#_others').hide();

                    }
                    else
                    {
                         $('#_reasonID').hide();
                        $('#_schoolName').hide();
                        $('#_others').hide();
                    }
                    
                });

                $('#reason').on('change', function(){
                    var select1 = $(this).val();
                    if(select1 == "Others")
                    {
                        $('#_others').show();
                    }
                    else
                    {
                        $('#_others').hide();
                    }
                    
                });
                
                $('#type1BA').on('change', function(){
                    var select1 = $(this).val();
                    if(select1 == "Issue")
                    {
                        $('#divRemarks').hide();
                    }
                    else
                    {   
                        $('#divRemarks').show();
                    }
                });
                
                

                $(document).on('click', '.enroll_learnertoSection', function(){
                    var lrn = $(this).attr("id");
                    var sectionID = $(this).attr("value");

                    $.ajax({
                        url:"ajaxJQuery/selectStudentInfo.php",
                        method:"POST",
                        data:{lrn:lrn,sectionID:sectionID},
                        dataType:"json",
                        success:function(data){
                            $('#NameofStudent').val(data.Name);
                            $('#studentID').val(data.student_id);
                            $('#sectionIDS').val(sectionID);
                            $('#modalEnrollLearner').modal('show');
                        }

                    });
                });

                $('#enroll').on("submit", function(event){
                    event.preventDefault();
                    
                        $.ajax({  
                              url:"ajaxJQuery/enrollLearner.php",  
                              method:"POST",  
                              data:$('#enroll').serialize(),  
                              success:function(data){
                                   if(data == '1')
                                   { 
                                      
                                       $('#enroll')[0].reset();
                                       $('#modalRemoveLearner').modal('hide');
                                   }
                                   else (data == '0')
                                   {    
                                        swal("Oops", "The learner has been used for other table", "error");  
                                       $('#enroll')[0].reset();
                                       $('#modalRemoveLearner').modal('hide');
                                   }

                                   setTimeout(function()
                                   {// wait for 5 secs(2)
                                        location.reload(); // then reload the page.(3)
                                    }, 800);
                              }  

                         });  
                    
                });
                

                //SEARCH STUDENT
                $('#search').keyup(function(objEvent){
                    var txt = $('#search').val();
                    var id = $('#secID').val();
                    (objEvent) ? keycode = objEvent.keyCode : keycode = event.keyCode;
                    console.log(keycode);
                    if(keycode == 13){
                        
                        $.ajax({
                            url:"ajaxJQuery/searchStudent.php",
                            method:"POST",
                            dataType:"text",
                            data:
                            {
                                search:txt,
                                section:id
                            },
                            
                            success:function(data)
                            {
                                $('#resultsaSearch').html(data);
                            }
                        });
                        
                    }
                });

                //SELECT LEARNER TO DELETE
                $(document).on('click', '.delete_learner', function(){
                    var important_id1 = $(this).attr("id");
                    $.ajax({
                        url:"ajaxJQuery/selectControl1.php",
                        method:"POST",
                        data:{important_id1:important_id1},
                        dataType:"json",
                        success:function(data){
                            $('#student1').val(data.Student);
                            $('#student_id').val(data.student_id);
                            $('#important_id1').val(important_id1);
                            $('#modalRemoveLearner').modal('show');
                        }

                    });
                });

                //SELECT LEARNER TO EDIT
                $(document).on('click', '.edit_learner', function(){
                    var important_id1 = $(this).attr("id");
                    $.ajax({
                        url:"ajaxJQuery/selectControl1.php",
                        method:"POST",
                        data:{important_id1:important_id1},
                        dataType:"json",
                        success:function(data){
                            $('#student2').val(data.Student);
                            $('#remarks1').val(data.control_remarks);
                            $('#student_id2').val(data.student_id);
                            $('#important_id2').val(important_id1);
                            
                            $('#modalEdit').modal('show');
                        }

                    });
                });

                 //ADD ATTENDANCE SELECT
                $(document).on('click', '.add_attendance', function(){
                    var monthID = $(this).attr("id");
                    var sectionID = '<?php echo $id; ?>';
                    $('#monthID').val(monthID);
                    $('#attendancesectionID').val(sectionID);
                    $('#modalAddAttendance').modal('show');
                });

                //UN_ENROLL LEARNER
                $('#removeLearner').on("submit", function(event){
                    event.preventDefault();
                    if($('#student1').val() == 'Choose')
                    {
                       
                    }
                    else 
                    {
                        $.ajax({  
                              url:"ajaxJQuery/removeLearner.php",  
                              method:"POST",  
                              data:$('#removeLearner').serialize(),
                              success:function(data){
                                   if(data != '1')
                                   { 
                                      
                                       // $('#removeLearner')[0].reset();
                                       // $('#modalRemoveLearner').modal('hide');
                                       // $('#learner').html(data);
                                       location.reload();
                                   }
                                   else
                                   {    
                                        swal("Oops", "The learner has been used for other table", "error");  
                                       $('#removeLearner')[0].reset();
                                       $('#modalRemoveLearner').modal('hide');
                                   }

                                
                              }  

                         });  
                    }
                });

                //LEARNER EDIT
                $('#editLearner').on("submit", function(event){
                    event.preventDefault();
                    if($('#remarks1').val() == '')
                    {
                        $('#remarks1').focus();
                    }
                    else 
                    {
                        $.ajax({  
                              url:"ajaxJQuery/updateRemarks.php",  
                              method:"POST",  
                              data:$('#editLearner').serialize(),  
                              success:function(data){
                                if(data != '1')
                                { 
                                   //swal("Good job", "You successfully updated learner", "success");  
                                   // $('#editLearner')[0].reset();   
                                   // $('#modalEdit').modal('hide');
                                   // $('#learner').html(data);
                                   location.reload();
                                }
                                else
                                {
                                    swal("Oops", "Error in updating remarks of a learner", "error");  
                                   $('#editLearner')[0].reset();   
                                   $('#modalEdit').modal('hide');
                                }
                                
                              }  

                         });  
                    }
                });
                /////////////////////////////////////////////////////////////

                //INSERT BOOKS ACQUIRED
                $('#insert_form2').on("submit", function(event){
                    event.preventDefault();
                    if($('#type').val() == 'Choose')
                    {
                        $('#type').focus();    
                         alert("Choose either return or issue");
                    }
                    else if($('#date').val() == '')
                    {
                        $('#date').focus();    
                         alert("Date is required");
                    }
                    else 
                    {
                        $.ajax({  
                              url:"ajaxJQuery/insertBooksAcquired.php",  
                              method:"POST",  
                              data:$('#insert_form2').serialize(), 
                              beforeSend:function(){
                                    $('#book').modal('hide');  
                                   $('#loading').modal('show');
                              },  
                              success:function(data){ 
                                   if(data == "0")
                                   {
                                        var message = "You need to issue book first.";
                                        executeNotif(message,"success");
                                   }
                                   else
                                   {
                                       var message = "Successfully added books acquired";
                                        executeNotif(message,"success");
                                       $('#insert_form2')[0].reset();   
                                   
                                       //$('#book').modal('hide');
                                       $('#loading').modal('hide'); 
                                       $('#messages11').html(data);
                                    }
                              }  

                         });  
                    }
                });

                //INSERT ATTENDANCE
                $('#addAttendanceForm').on("submit", function(event){
                    event.preventDefault();
                    
                    $.ajax({  
                          url:"ajaxJQuery/insertAttendance.php",  
                          method:"POST",  
                          data:$('#addAttendanceForm').serialize(), 
                          
                          success:function(data){ 
                            if(data == '0')
                            {
                                
                                var message = "Error on adding attendance to all learners.Dont exceed to the total school days";
                                executeNotif(message,"danger");
                               
                                 $('#modalAddAttendance').modal('hide');  
                               $('#addAttendanceForm')[0].reset(); 
                            }
                            else if(data == "less")
                            {
                                var message = "Inputs must be valid";
                                executeNotif(message,"danger");
                            }
                            else
                            {
                                $('#modalAddAttendance').modal('hide');  
                               $('#addAttendanceForm')[0].reset();   
                               $('#profile11').html(data); 
                               var message = "Successfully added attendance to registered learners for this Month";
                                executeNotif(message,"success");
                             }  
                          }  

                     });  
                    
                });

                $('#editAttendanceForm').on("submit", function(event){
                    event.preventDefault();
                    
                    $.ajax({  
                          url:"ajaxJQuery/updateAttendance.php",  
                          method:"POST",  
                          data:$('#editAttendanceForm').serialize(), 
                         
                          success:function(data){ 
                            if(data == '0')
                            {
                                 
                                var message = "Error on adding attendance to all learners.Dont exceed to the total school days";
                                executeNotif(message,"danger");
                                 $('#modalAddAttendance').modal('hide');  
                               $('#editAttendanceForm')[0].reset();
                              
                            }
                            else if(data == "less")
                            {
                                var message = "Inputs must be valid";
                                executeNotif(message,"danger");
                            }
                            else
                            {
                                $('#modalAddAttendance').modal('hide');  
                                $('#editAttendanceForm')[0].reset();   
                                $('#profile11').html(data); 
                                var message = "Successfully updated attendance to registered learners for this Month";
                                executeNotif(message,"success");
                                $('#modalEditAttendance').modal('hide');
                             }  
                          }  

                     });  
                    
                });

                //SELECT LEARNER TO EDIT ATTENDANCE
                $(document).on('click', '.edit_attendance', function(){
                    var id = $(this).attr("id");
                    var sectionID = "<?php echo $id; ?>";
                    var tunga = id.split('/');
                    $.ajax({
                        url:"ajaxJQuery/selectAttendance.php",
                        method:"GET",
                        data:{monthID:tunga[0], controlID:tunga[1]},
                        dataType:"json",
                        success:function(data){
                            $('#StudentName').val(data.Student);
                            $('#monthID1').val(tunga[0]);
                            $('#attendancesectionID1').val(sectionID);
                            $('#present1').val(data.attendance_presentDays);
                            $('#absent1').val(data.attendance_absentDays);
                            $('#tardy1').val(data.attendance_tardyDays);
                            $('#controlID').val(tunga[1]);
                            $('#modalEditAttendance').modal('show');
                        }

                    });
                });


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////  
                //SELECT BOOKS ACQUIRED TO EDIT
                $(document).on('click', '.edit_booksacquired', function(){
                    var important_id1 = $(this).attr("id");
                    $.ajax({
                        url:"ajaxJQuery/selectControl1.php",
                        method:"POST",
                        data:{important_id1:important_id1},
                        dataType:"json",
                        success:function(data){
                            $('#studentID1').val(important_id1);
                            $('#editBookAcquired').modal('show');
                        }

                    });
                });

                $(document).on('click', '.delete_booksacquired', function(){
                    var important_id1 = $(this).attr("id");
                    $.ajax({
                        url:"ajaxJQuery/selectControl1.php",
                        method:"POST",
                        data:{important_id1:important_id1},
                        dataType:"json",
                        success:function(data){
                            $('#studentIDDelete').val(important_id1);
                            $('#deleteBookAcquired').modal('show');
                        }

                    });
                });
                $('#deleteBook').on("submit", function(event){
                    event.preventDefault();
                    
                    $.ajax({  
                          url:"ajaxJQuery/deleteBooksAcquired.php",  
                          method:"POST",  
                          data:$(this).serialize(),
                          success:function(data){ 

                                var message = "Successfully updated books acquired";
                                executeNotif(message,"success");
                                $('#deleteBook')[0].reset();   
                              
                                $('#deleteBookAcquired').modal('hide');
                                $('#messages11').html(data);
                          }  

                     });  
                    
                });
                //EDIT BOOKS ACQUIRED
                $('#editBook').on("submit", function(event){
                    event.preventDefault();
                    if($('#type1').val() == 'Choose')
                    {
                        $('#type1').focus();    
                         alert("Choose either return or issue");
                    }
                   
                    else 
                    {
                        $.ajax({  
                              url:"ajaxJQuery/updateBooksAcquired.php",  
                              method:"POST",  
                              data:$('#editBook').serialize(),
                                 
                              success:function(data){ 

                                    if(data == '0')
                                    {
                                       var message = "Error on updating books acquired";
                                        executeNotif(message,"danger");
                                    }
                                    else
                                    {
                                        var message = "Successfully updated books acquired";
                                        executeNotif(message,"success");
                                       $('#editBook')[0].reset();   
                                      
                                       $('#editBookAcquired').modal('hide');
                                       $('#messages11').html(data);
                                    } 
                              }  

                         });  
                    }
                });
                

                $(document).on('click', '.grade_details', function(){
                    var s_id = $(this).attr("id");
                    $.ajax({
                        url:"ajaxJQuery/viewGradeDetails.php",
                        method:"POST",
                        data:{s_id:s_id},
                        
                        success:function(data){
                            $('#Details').html(data);
                            $('#gradeDetails').modal('show');
                        }
                    });
                });

               
                
            } );
        </script>
        
    </body>
  
<!-- Mirrored from byrushan.com/projects/ma/1-7-1/jquery/light/widget-templates.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 01 May 2017 06:35:54 GMT -->
</html>