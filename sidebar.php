<?php 
    
    include_once 'dbconnect.php';
    $getPic=  mysqli_fetch_row(mysqli_query($con, "SELECT faculty_picture from faculty_account where faculty_id='".$_SESSION['faculty_id']."'"));
    $p =$getPic[0];
    echo "

    <aside id='sidebar' class='sidebar c-overflow'>
        <div class='s-profile'>
            <a href='#' data-ma-action='profile-menu-toggle'>
                <div class='sp-pic'>";
                    if($p == '')
                    {
                        echo "<img src='img/default-profile.png' alt=''>";

                    }else
                    {
                        echo "<img src='profile/".$p."' alt=''>";

                    }
                    echo "
                </div>

                <div class='sp-info'>"; echo $_SESSION['Name'];
                echo "
                    <i class='zmdi zmdi-caret-down'></i>
                </div>
            </a>
        
            
            <ul class='main-menu'>
                <li>
                    <a href='adminProfile.php'><i class='zmdi zmdi-account'></i> View Profile</a>
                </li>
                <li>
                    <a href='adminChangePass.php'><i class='zmdi zmdi-lock'></i> Privacy Settings</a>
                </li>
                <li>
                    <a href='logout.php'><i class='zmdi zmdi-power'></i> Logout</a>
                </li>
            </ul>
        
        </div>
        <ul class='main-menu'>";
        if($toggle == 'adminHome'){
            echo "
            <li class='active toggled'>
                <a href='adminHome.php'><i class='zmdi zmdi-balance'></i> Home</a>
            </li>";
        }else{
            echo "
            <li>
                <a href='adminHome.php'><i class='zmdi zmdi-balance'></i> Home</a>
            </li>";
        }

        if($toggle == 'adminClass'){
            echo "
            <li class='active toggled'>
                <a href='adminClass.php'><i class='zmdi zmdi-calendar-check'></i> Class</a>
            </li>";
        }else{
            echo "
            <li>
                <a href='adminClass.php'><i class='zmdi zmdi-calendar-check'></i> Class</a>
            </li>";
        }
        if($toggle == 'adminStudent' | $toggle == 'adminTeacher'){
            echo "
            <li class='sub-menu active toggled'>
                <a href='#' data-ma-action='submenu-toggle'><i class='zmdi zmdi-account-box'></i> Accounts</a>

                <ul>";
                if($toggle == 'adminStudent'){
                    echo "<li><a href='adminStudent.php' class='active'><i class='zmdi zmdi-graduation-cap'></i> Learners</a></li>";
                }else{
                    echo "<li><a href='adminStudent.php'><i class='zmdi zmdi-graduation-cap'></i> Learners</a></li>";
                }
                if($toggle == 'adminTeacher'){
                    echo "<li><a href='adminTeacher.php' class='active'><i class='zmdi zmdi-accounts-alt'></i> Teachers</a></li>";
                }else{
                    echo "<li><a href='adminTeacher.php'><i class='zmdi zmdi-accounts-alt'></i> Teachers</a></li>";
                }
            echo "
                </ul>
            </li>";
        }else
        {
             echo "
            <li class='sub-menu'>
                <a href='#' data-ma-action='submenu-toggle'><i class='zmdi zmdi-account-box'></i> Accounts</a>

                <ul>
                    
                    <li><a href='adminStudent.php'><i class='zmdi zmdi-graduation-cap'></i> Learners</a></li>
                    <li><a href='adminTeacher.php'><i class='zmdi zmdi-accounts-alt'></i> Teachers</a></li>
                   
                </ul>
            </li>";
        }
        
        if($toggle == 'adminSchool' | $toggle == 'adminSubject' | $toggle == 'adminBook' | $toggle == 'adminCredential' | $toggle == 'adminLevel'){
            echo "
            <li class='sub-menu active toggled'>
                <a href='#' data-ma-action='submenu-toggle'><i class='zmdi zmdi-settings'></i> Enrollment Configuration</a>

                <ul>";
                if($toggle == 'adminSchool'){
                    echo "<li><a href='adminSchool.php' class='active'><i class='zmdi zmdi-balance'></i> School Year</a></li>";
                }else{
                    echo "<li><a href='adminSchool.php'><i class='zmdi zmdi-balance'></i> School Year</a></li>";
                }
                
                if($toggle == 'adminSubject'){
                    echo "<li><a href='adminSubject.php' class='active'><i class='zmdi zmdi-collection-text'></i> Subjects</a></li>";
                }else{
                    echo "<li><a href='adminSubject.php'><i class='zmdi zmdi-collection-text'></i> Subjects</a></li>";
                }
                if($toggle == 'adminBook'){
                    echo "<li><a href='adminBook.php' class='active'><i class='zmdi zmdi-library'></i> Books</a></li>";
                }else{
                    echo "<li><a href='adminBook.php'><i class='zmdi zmdi-library'></i> Books</a></li>";
                }
                
               

            echo "    
                </ul>
            </li>";
        }else{
            echo "
            <li class='sub-menu'>
                <a href='#' data-ma-action='submenu-toggle'><i class='zmdi zmdi-settings'></i> Enrollment Configuration</a>

                <ul>
                    <li><a href='adminSchool.php'><i class='zmdi zmdi-balance'></i> School Year</a></li>
                    <li><a href='adminSubject.php'><i class='zmdi zmdi-collection-text'></i> Subjects</a></li>
                    <li><a href='adminBook.php'><i class='zmdi zmdi-library'></i> Books</a></li>
                    
                    
                </ul>
            </li>";
        }

        if($toggle == 'SchoolForm'){
            echo "
            <li class='active toggled'>
                <a href='schoolForms.php'><i class='zmdi zmdi-folder-star'></i> School Forms</a>
            </li>";
        }else{
            echo "
            <li>
                <a href='schoolForms.php'><i class='zmdi zmdi-folder-star'></i> School Forms</a>
            </li>";
        }

        if($toggle == 'enrollment' |$toggle == 'summary' | $toggle == 'ranking' | $toggle == 's7' | $toggle == 's8' | $toggle == 's9' | $toggle == 's10' | $toggle == 'all' | $toggle == 'ss7' | $toggle == 'ss8' | $toggle == 'ss9' | $toggle == 'ss10'){
            echo "
            <li class='sub-menu active toggled'>
                <a href='#' data-ma-action='submenu-toggle'><i class='zmdi zmdi-folder-star'></i> School Report</a>
                    <ul>";

                    if($toggle == 'enrollment'){
                        echo "<li><a href='enrollment.php' class='active'><i class='zmdi zmdi-file-text'></i> Enrollment Report</a></li>";
                    }else{
                        echo "<li><a href='enrollment.php'><i class='zmdi zmdi-file-text'></i> Enrollment Report</a></li>";
                    }
                    
                    if($toggle == 's7' | $toggle == 's8' | $toggle == 's9' | $toggle == 's10'){
                        echo "
                        <li class='sub-menu active toggled'>
                            <a href='#' data-ma-action='submenu-toggle'><i class='zmdi zmdi-file-text'></i> Summary Report</a>
                            <ul>";
                                if($toggle == 's7'){
                                    echo "<li><a href='summary.php?level=Grade 7' class='active'><i class='zmdi zmdi-file-text'></i> Grade 7</a></li>";
                                }else{
                                    echo "<li><a href='summary.php?level=Grade 7'><i class='zmdi zmdi-file-text'></i> Grade 7</a></li>";
                                }
                                if($toggle == 's8'){
                                    echo "<li><a href='summary.php?level=Grade 8' class='active'><i class='zmdi zmdi-file-text'></i> Grade 8</a></li>";
                                }else{
                                    echo "<li><a href='summary.php?level=Grade 8'><i class='zmdi zmdi-file-text'></i> Grade 8</a></li>";
                                }
                                if($toggle == 's9'){
                                    echo "<li><a href='summary.php?level=Grade 9' class='active'><i class='zmdi zmdi-file-text'></i> Grade 9</a></li>";
                                }else{
                                    echo "<li><a href='summary.php?level=Grade 9'><i class='zmdi zmdi-file-text'></i> Grade 9</a></li>";
                                }
                                if($toggle == 's10'){
                                    echo "<li><a href='summary.php?level=Grade 10' class='active'><i class='zmdi zmdi-file-text'></i> Grade 10</a></li>";
                                }else{
                                    echo "<li><a href='summary.php?level=Grade 10'><i class='zmdi zmdi-file-text'></i> Grade 10</a></li>";
                                }
                                
                               
                            
                            echo "
                            </ul>

                        </li>
                        ";


                    }else{
                        echo "
                        <li class='sub-menu'>
                            <a href='#' data-ma-action='submenu-toggle'><i class='zmdi zmdi-file-text'></i> Summary Report</a>
                            <ul>
                                <li><a href='summary.php?level=Grade 7'><i class='zmdi zmdi-file-text'></i> Grade 7</a></li>
                                <li><a href='summary.php?level=Grade 8'><i class='zmdi zmdi-file-text'></i> Grade 8</a></li>
                                <li><a href='summary.php?level=Grade 9'><i class='zmdi zmdi-file-text'></i> Grade 9</a></li>
                                <li><a href='summary.php?level=Grade 10'><i class='zmdi zmdi-file-text'></i> Grade 10</a></li>
                            </ul>

                        </li>";
                    }
                    if($toggle == 'all' | $toggle == 'ss7' | $toggle == 'ss8' | $toggle == 'ss9' | $toggle == 'ss10'){
                        echo "
                        <li class='active toggled sub-menu'>
                            <a href='#' data-ma-action='submenu-toggle'><i class='zmdi zmdi-file-text'></i> Ranking of Learner</a>
                            <ul>";
                                if($toggle == 'all')
                                {
                                    echo "<li><a href='ranking.php?level=Overall' class='active'><i class='zmdi zmdi-file-text'></i> All</a></li>";
                                }
                                else
                                {
                                    echo "<li><a href='ranking.php?level=Overall'><i class='zmdi zmdi-file-text'></i> All</a></li>";
                                }
                                if($toggle == 'ss7')
                                {
                                    echo "<li><a href='ranking.php?level=Grade 7' class='active'><i class='zmdi zmdi-file-text'></i> Grade 7</a></li>";
                                }
                                else
                                {
                                    echo "<li><a href='ranking.php?level=Grade 7'><i class='zmdi zmdi-file-text'></i> Grade 7</a></li>";
                                }
                                if($toggle == 'ss8')
                                {
                                    echo "<li><a href='ranking.php?level=Grade 8' class='active'><i class='zmdi zmdi-file-text'></i> Grade 8</a></li>";
                                }
                                else
                                {
                                    echo "<li><a href='ranking.php?level=Grade 8'><i class='zmdi zmdi-file-text'></i> Grade 8</a></li>";
                                }
                                if($toggle == 'ss9')
                                {
                                    echo "<li><a href='ranking.php?level=Grade 9' class='active'><i class='zmdi zmdi-file-text'></i> Grade 9</a></li>";
                                }
                                else
                                {
                                    echo "<li><a href='ranking.php?level=Grade 9'><i class='zmdi zmdi-file-text'></i> Grade 9</a></li>";
                                }
                                if($toggle == 'ss10')
                                {
                                    echo "<li><a href='ranking.php?level=Grade 10' class='active'><i class='zmdi zmdi-file-text'></i> Grade 10</a></li>";
                                }
                                else
                                {
                                    echo "<li><a href='ranking.php?level=Grade 10'><i class='zmdi zmdi-file-text'></i> Grade 10</a></li>";
                                }

                                
                        echo "
                            </ul>
                        </li>
                        ";
                    }else{
                        echo "
                        <li class='sub-menu'>
                            <a href='#' data-ma-action='submenu-toggle'><i class='zmdi zmdi-file-text'></i> Ranking of Learner</a>
                            <ul>
                                <li><a href='ranking.php?level=Overall'><i class='zmdi zmdi-file-text'></i> All</a></li>
                                <li><a href='ranking.php?level=Grade 7'><i class='zmdi zmdi-file-text'></i> Grade 7</a></li>
                                <li><a href='ranking.php?level=Grade 8'><i class='zmdi zmdi-file-text'></i> Grade 8</a></li>
                                <li><a href='ranking.php?level=Grade 9'><i class='zmdi zmdi-file-text'></i> Grade 9</a></li>
                                <li><a href='ranking.php?level=Grade 10'><i class='zmdi zmdi-file-text'></i> Grade 10</a></li>
                            </ul>

                        </li>
                        ";
                    }
            echo 
                "   </ul>
            </li>";
        }else{
            echo "
            <li class='sub-menu'>
                <a href='#' data-ma-action='submenu-toggle'><i class='zmdi zmdi-folder-star'></i> School Report</a>
                    <ul>
                        <li><a href='enrollment.php'><i class='zmdi zmdi-file-text'></i> Enrollment Report</a></li>
                        <li class='sub-menu'>
                            <a href='#' data-ma-action='submenu-toggle'><i class='zmdi zmdi-file-text'></i> Summary Report</a>
                            <ul>
                                <li><a href='summary.php?level=Grade 7'><i class='zmdi zmdi-file-text'></i> Grade 7</a></li>
                                <li><a href='summary.php?level=Grade 8'><i class='zmdi zmdi-file-text'></i> Grade 8</a></li>
                                <li><a href='summary.php?level=Grade 9'><i class='zmdi zmdi-file-text'></i> Grade 9</a></li>
                                <li><a href='summary.php?level=Grade 10'><i class='zmdi zmdi-file-text'></i> Grade 10</a></li>
                            </ul>

                        </li>
                        <li class='sub-menu'>
                            <a href='#' data-ma-action='submenu-toggle'><i class='zmdi zmdi-file-text'></i> Ranking of Learner</a>
                            <ul>
                                <li><a href='ranking.php?level=Overall'><i class='zmdi zmdi-file-text'></i> All</a></li>
                                <li><a href='ranking.php?level=Grade 7'><i class='zmdi zmdi-file-text'></i> Grade 7</a></li>
                                <li><a href='ranking.php?level=Grade 8'><i class='zmdi zmdi-file-text'></i> Grade 8</a></li>
                                <li><a href='ranking.php?level=Grade 9'><i class='zmdi zmdi-file-text'></i> Grade 9</a></li>
                                <li><a href='ranking.php?level=Grade 10'><i class='zmdi zmdi-file-text'></i> Grade 10</a></li>
                            </ul>

                        </li>
                    </ul>
            </li>";
        }
        

        echo "
        </ul>
        </aside>";




?>
