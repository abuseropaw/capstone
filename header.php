
<header id="header" class="clearfix" data-ma-theme="green">
    <ul class="h-inner">
        <li class="hi-trigger ma-trigger" data-ma-action="sidebar-open" data-ma-target="#sidebar">
            <div class="line-wrap">
                <div class="line top"></div>
                <div class="line center"></div>
                <div class="line bottom"></div>
            </div>
        </li>
        <li class="hi-logo hidden-xs">
        <?php
        switch ($_SESSION['faculty_type'] ) {
            case 'Administrator':
                    echo '
                        <a href="adminHome.php">Lugait National High School</a>
                    ';
                break;
            case 'Teacher':
                    echo '
                        <a href="teacherHome.php">Lugait National High School</a>
                   ';
                break;

            case 'Registrar' || 'Head Teacher':
                    
                    echo '
                        <a href="registrarHome.php">Lugait National High School</a>
                   ';
                break;
            case 'Student':
                    echo '
                        <a href="studentHome.php">Lugait National High School</a>
                   ';
                break; 
            default:
                # code...
                break;
        }

         
        ?>
        </li>
        <li class="pull-right">
            <ul class="hi-menu">

                <li data-ma-action="search-open">
                    <a href="#"><i class="him-icon zmdi zmdi-search"></i></a>
                </li>
                <li class="dropdown">
                    <a data-toggle="dropdown" href="#"><i class="him-icon zmdi zmdi-caret-down"></i></a>
                    <ul class="dropdown-menu dm-icon pull-right">
                        
                        <li class="hidden-xs">
                            <a data-ma-action="fullscreen" href="#"><i class="zmdi zmdi-fullscreen"></i> Toggle Fullscreen</a>
                        </li>
                        <?php

                        if($_SESSION['faculty_type'] == 'Administrator')
                        {
                            echo '
                            <li>
                                <a href="adminProfile.php"><i class="zmdi zmdi-pin-account"></i> Account</a>
                            </li>
                            <li>
                                <a href="adminChangePass.php"><i class="zmdi zmdi-lock"></i> Privacy Settings</a>
                            </li>';
                        }
                        else if($_SESSION['faculty_type'] == 'Teacher')
                        {
                            echo '
                            <li>
                                <a href="teacherProfile.php"><i class="zmdi zmdi-pin-account"></i> Account</a>
                            </li>
                            <li>
                                <a href="teacherChangePass.php"><i class="zmdi zmdi-lock"></i> Privacy Settings</a>
                            </li>';
                        }

                        if($_SESSION['faculty_type'] == 'Head Teacher' | $_SESSION['faculty_type'] == 'Registrar')
                        {
                             echo '
                            <li>
                                <a href="registrarInformation.php"><i class="zmdi zmdi-pin-account"></i> Account</a>
                            </li>
                            <li>
                                <a href="registrarChangePassword.php"><i class="zmdi zmdi-lock"></i> Privacy Settings</a>
                            </li>';
                        }
                        
                        ?>
                        
                        <li>
                            <a href="logout.php"><i class="zmdi zmdi-power"></i>Logout</a>
                        </li>
                    </ul>
                </li>

            </ul>
        </li>

    </ul>

    <!-- Top Search Content -->
    <div class="h-search-wrap">
        <div class="hsw-inner">
            <i class="hsw-close zmdi zmdi-arrow-left" data-ma-action="search-close"></i>
            <input type="text" name='globalSearch' id='globalSearch'>
        </div>
    </div>
</header>