
<?php
    include_once 'sessionTeacher.php';
    include_once 'dbconnect.php';
    include 'ajaxJQuery/globalFunction.php';
?>
<!DOCTYPE html>
    <!-- HEAD -->
    <?php include_once 'head.php'; ?> 
    <!-- HEAD   -->
    <body>
        <!-- HEADER -->
        <?php include_once 'header.php'; ?>
        <!-- HEADER -->

        <section id="main">
            <ol class="breadcrumb">
                <li><a href="teacherHome.php">Home</a></li>
                <li class="active">Advisory</li>
            </ol>
            <?php 
                $toggle = 'teacherAdvisory';
                include_once 'sidebarTeacher.php'; 
            ?>

           
            <section id="content">

                <div class="container">
                    <!-- Colored Headers -->
                    <div class="block-header">

                        <h1><i class="zmdi zmdi-calendar-note"></i> Advisory

                        </h1>
                    </div>

                    
                    <div class="row" id='sectionList'>

                        
                            <?php
                                $getName = mysqli_query($con, "SELECT Name FROM facultysmalldetail WHERE faculty_id='".$_SESSION['faculty_id']."'");
                                $temp = mysqli_fetch_row($getName);
                                $name1 = $temp[0];


                                $query = mysqli_query($con, "SELECT * from classinformation where Adviser='".$name1."'");
                                while($row = mysqli_fetch_array($query)){
                                    $id = $row['section_id'];
                                    $name = $row['section_name'];
                                    $capacity = $row['section_capacity'];
                                    $max = $row['section_maxGrade'];
                                    $min = $row['section_minGrade'];
                                    $createdby = $row['Cretedby'];
                                    $dateCreated = $row['section_dateCreated'];
                                    $level = $row['GradeLevel'];
                                    $adviser = $row['Adviser'];
                                    $schoolYear = $row['sy_id'];

                                    $countGirls = mysqli_num_rows(mysqli_query($con, "SELECT * from controldetails where gender='Female' and section_id='".$id."'"));
                                    $countBoys = mysqli_num_rows(mysqli_query($con, "SELECT * from controldetails where gender='Male' and section_id='".$id."'"));

                                    echo "
                                    <div class='col-sm-4'>
                                    <div class='card'>
                                        <div class='card-header'>
                                            <h1>".$name."
                                          
                                            </h1>
                                            <ul class='actions actions-alt'>
                                                <div class='btn-demo'>";
                                            if($schoolYear == getCurrentSY1($con))
                                            {
                                                echo "
                                                <a href='teacherAdvisoryEnrollment.php?id=".$id."&name=".$name."&levels=".$level."' target='_blank'><button name='but' class='btn btn-default waves-effect' data-toggle='tooltip' data-placement='bottom' title='' data-original-title='Enroll learner to this section'> <b>Enrollment</b> </button></a>";
                                            }


                                                echo "
                                                <a href='teacherAdvisoryDetails.php?id=".$id."&name=".$name."&levels=".$level."'><button name='but' class='btn btn-default waves-effect' data-toggle='tooltip' data-placement='top' title='' data-original-title='More details for this advisory'><i class='zmdi zmdi-arrow-forward'></i></button></a>
                                                </div>
                                            </ul>

                                            <h3><i class='zmdi zmdi-female'> ".$countGirls." </i>  &nbsp&nbsp <i class='zmdi zmdi-male'> ".$countBoys."</i> </h3>";

                                            if($schoolYear == getCurrentSY1($con))
                                            {
                                                echo "<h3><b>Current School Year</b></h3>";
                                            }
                                            else
                                            {
                                                echo "<small><b>School Year: ".$schoolYear."</b></small>";
                                            }
                                            
                                    echo "        
                                        </div>
                                    </div>
                                 </div>";
                                }
                            ?>
                       
                    </div>




                    <br/>
                    <br/>
                </div>
            </section>
        </section>
        <!-- FOOTER -->
        <?php include_once 'footer.php' ?>
        <!-- FOOTER -->

        <!-- Javascript Libraries -->
        <?php include_once 'scripts.php'; ?>
        <!-- Javascript Libraries -->


       
    </body>
  
<!-- Mirrored from byrushan.com/projects/ma/1-7-1/jquery/light/widget-templates.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 01 May 2017 06:35:54 GMT -->
</html>