<?php
    include_once 'sessionAdmin.php';
    include_once 'dbconnect.php';
    $level = mysqli_real_escape_string($con, $_GET['level']);
    $getLevel = mysqli_fetch_row(mysqli_query($con, "SELECT year_lvl_id from yearlevel where year_lvl_title='".$level."'"));



    $getCurrentSY = mysqli_query($con, "SELECT sy_year,sy_id from schoolyear where sy_remarks='Open'");
    $temp = mysqli_fetch_row($getCurrentSY);
    $SY = $temp[0];
    $sy_id= $temp[1];
    $countQuarter = mysqli_num_rows(mysqli_query($con, "SELECT * from quarters where sy_id='".$SY."'"));

    function fillSchoolYear($con){
        $output ='';
        $query = mysqli_query($con, "SELECT * from schoolyear order by sy_id DESC");
        if(check($con))
        {

        }
        else
        {
            $output .='<option disabled="disabled" selected>NO CURRENT SY</option>';
        }
        while($row = mysqli_fetch_array($query)){
            if($row[4] == 'Open'){
                $output .='<option value="'.$row[1].'">'.$row[1].' - CURRENT</option>';
            }else
            {
                $output .='<option value="'.$row[1].'">'.$row[1].'</option>';
            }
            
        }
        return $output;
    }
    function check($con)
    {
        $query = mysqli_num_rows(mysqli_query($con, "SELECT * from schoolyear where sy_remarks='Open'"));
        if($query > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
?>
<!DOCTYPE html>
    <!-- HEAD -->
    <?php include_once 'head.php'; ?> 
    <!-- HEAD   -->
    <body>
        <!-- HEADER -->
        <?php include_once 'header.php'; ?>
        <!-- HEADER -->

        <section id="main">
            <ol class="breadcrumb">
                <li><a href="adminHome.php">Home</a></li>
                <li class="active">Ranking Report</li>
            </ol>
            <?php 

                $toggle = '';
                switch ($level) {
                    case 'Grade 7':
                        $toggle='ss7';
                        break;
                    case 'Grade 8':
                        $toggle='ss8';
                        break;
                    case 'Grade 9':
                        $toggle='ss9';
                        break;
                    case 'Grade 10':
                        $toggle='ss10';
                        break;
                    default:
                        $toggle= 'all';
                        break;
                }

                include_once 'sidebar.php'; 
            ?>
            <section id="content">
                <div class="container">
                    <!-- Colored Headers -->
                    
                    <br />
                    <div class="block-header">
                        <div class="class">
                            <h1><i class="zmdi zmdi-calendar-note"></i> <?php echo $level; ?> <h4>Ranking Report</h4>
                            </h1>


                            <div class="actions">
                                <div class="btn-demo row">
                                <?php if(fillSchoolYear($con) == ''){

                                }else{
                                    echo '
                                    <select class="btn btn-group btn-lg" id="sy" name="sy" >
                                    <optgroup label="School Year">';
                                        
                                        echo fillSchoolYear($con);
                                    echo '</select>
                                        ';
                                } ?>
                                </div>
                            </div>
                        </div>
                        

                    </div>
                    <div id="hehe">
                        <?php include_once 'ajaxJQuery/displayRanking.php'; ?>
                    </div>



                    <br/>
                    <br/>
                </div>
            </section>
        </section>
        <div id="loading" class="modal fade" data-backdrop="static" data-keyboard="false">  
            <div class="modal-dialog modal-sm">  
               <div class="modal-content">
               <center>   
                    <div class="modal-body" id="Details">
                         
                        <div class="preloader pl-xxl">
                            
                                <svg class="pl-circular" viewBox="25 25 50 50">
                                    <circle class="plc-path" cx="50" cy="50" r="20"/>
                                </svg>
                            
                        </div> 
                       <center><h4>Processing...Please wait</h4></center>
                    </div>  
                </center>
               </div>  
            </div>  
        </div>
        <!-- FOOTER -->
        <?php include_once 'footer.php' ?>
        <!-- FOOTER -->

        <!-- Javascript Libraries -->
        <?php include_once 'scripts.php'; ?>
        <!-- Javascript Libraries -->

        <script type="text/javascript" src="js/Notification.js"></script>
        <script type="text/javascript">
            $('#sy').change(function(){
                var sys = $(this).val();
                var level = "<?php echo $level; ?>";
                var countQuarter = "<?php echo $countQuarter; ?>";
                $.ajax({
                    url:"ajaxJQuery/loadRanking.php",
                    method:"GET",
                    data:{sys:sys,level:level,countQuarter:countQuarter},
                    beforeSend:function(){
                        $('#loading').modal('show');
                    },
                    success:function(data){
                        $('#loading').modal('hide');
                        $('#hehe').html(data);
                    }
                });
            });
        </script>
    </body>
</html>