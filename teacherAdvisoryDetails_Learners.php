<div role="tabpanel" class="tab-pane" id="learner">
    <div id="listofStudents">
    <div>
        <div class="btn-demo actions row">
        <?php

        echo "
            <button class='btn btn-default btn-lg' onclick=window.open('SF1-Excel.php?id=".$id."') data-toggle='tooltip' data-placement='top' title='' data-original-title='Generate School Form 1 (School Register) in excel file'><i class='zmdi zmdi-download'> </i>  <b>School Form 1</b></button>
            
            ";

        ?>
      
        </div>
    </div>
        <br />
        <div class="card table-responsive">
        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <?php
                    include_once 'ajaxJQuery/globalFunction.php';
                    if(getSYofSection($con, $id) == getCurrentSY($con))
                    {
                    echo '<th class="text-center " rowspan="2" width="15"><b>Detail/Update/Delete</b></th>';
                    }
                    
                    ?>
                    <th class="text-center" rowspan="2"><b>Remarks</b></th>
                    <th class="text-center" rowspan="2"><b>Reason/<br>School Name</b></th>
                    <th class="text-center" rowspan="2"><b>LRN</b></th>
                    <th class="text-center" colspan="3"><b>Name</b></th>
                    <th class="text-center" rowspan="2"><b>Sex</b></th>

                    <th class="text-center" rowspan="2"><b>Date of Birth</b></th>
                    <th class="text-center" rowspan="2"><b>Age</b></th>
                    <th class="text-center" rowspan="2"><b>Mother Tongue</b></th>
                    <th class="text-center" rowspan="2"><b>Indigenous People</b></th>
                    <th class="text-center" rowspan="2"><b>Religion</b></th>

                    <th class="text-center" colspan="4"><b>Address</b></th>
                    <th class="text-center" rowspan="2"><b>Father's Name</b></th>
                    <th class="text-center" rowspan="2"><b>Mother's Maiden Name</b></th>
                    <th class="text-center" colspan="2"><b>Guardian</b></th>
                    <th class="text-center" rowspan="2"><b>Contact #</b></th>
                </tr>
                <tr>
                    <th class="text-center"><b>Last</b></th>
                    <th class="text-center"><b>First</b></th>
                    <th class="text-center"><b>Middle</b></th>

                    <th class="text-center"><b>House #/ Street/ Sitio</b></th>
                    <th class="text-center"><b>Barangay</b></th>
                    <th class="text-center"><b>Municipality</b></th>
                    <th class="text-center"><b>Province</b></th>

                    <th class="text-center"><b>Name</b></th>
                    <th class="text-center"><b>Relation</b></th>

                </tr>


            </thead>
            <tbody>
                <?php
                    $getLearners = mysqli_query($con, "SELECT * from controldetails where section_id='".$id."' order by Gender DESC,Student ASC");
                    while($row = mysqli_fetch_array($getLearners)){
                        $controlID = $row[0];


                        $getStudentInfo = mysqli_query($con, "SELECT * from studentinfo1 where control_id='".$controlID."' order by Name");
                        if($row1 = mysqli_fetch_array($getStudentInfo)){

                            $temp = explode(',', $row1[3]);
                            if($row1['control_remarks'] == 'DRP' | $row1['control_remarks'] == 'T/O')
                            {
                                echo "
                                <tr class='c-red'>
                                ";
                            }
                            else
                            {
                                echo "
                                <tr>
                                ";
                            }
                            

                                if(getSYofSection($con, $id) == getCurrentSY($con))
                                {
                                    echo "
                                <td>
                                  <div class='btn-demo'>
                                    ";
                                    echo "
                                    <button type='submit' onclick=window.open('studentinformation.php?sID=".$row1[1]."') name='details' id=".$row1[0]." class='btn btn-default btn-sm details'><i class='zmdi zmdi-info'></i></button>
                                    <button type='submit' name='edit' id='".$row[0]."' class='btn btn-default btn-sm edit_learner'><i class='zmdi zmdi-edit'></i></button>
                                    <button type='submit' name='delete' id='".$row[0]."' class='btn btn-default btn-sm delete_learner'><i class='zmdi zmdi-delete'></i></button>
                                    </div>
                                </td>";
                                }
                                echo "
                                  
                                <td>".$row1['control_remarks']." DATE:".$row1['control_dateUpdated']."</td>
                                <td>".$row[13]."</td>
                                <td>".$row1[2]."</td>
                                <td>".$temp[0]."</td>
                                <td>".$temp[1]."</td>
                                <td>".$temp[2]."</td>
                                <td>".$row1['sex']."</td>
                                <td>".$row1['BirthDate']."</td>
                                <td>".$row1['Age']."</td>
                                <td>".$row1['MotherTongue']."</td>
                                <td>".$row1['IP']."</td>
                                <td>".$row1['Religion']."</td>
                                <td>".$row1[15]."</td>
                                <td>".$row1[16]."</td>
                                <td>".$row1[17]."</td>
                                <td>".$row1[18]."</td>



                                <td>".$row1[19]."</td>
                                <td>".$row1[20]."</td>
                                <td>".$row1[21]."</td>
                                <td>".$row1[22]."</td>
                                <td>".$row1[23]."/".$row1[24]."</td>

                            </tr>
                            ";
                        }
                    }
                ?>
            </tbody>
        </table>
    </div>
    </div>
</div>
