<?php
 require('dbconnect.php');
 
 require('Report/fpdf.php');
 //$schoolYear = $_SESSION['sy_year'];
 //$gradeLevel = $_SESSION['year_lvl_title'];
 //$section = $_SESSION['section_title'];
 class PDF extends FPDF
 {
 	function Footer()
 	{
 		// Go to 1.5 cm from bottom
 		
 		$this->SetY(-50);
 		// Select Arial italic 8
 		$this->SetFont('Arial','I',8);
 		// Print centered page number
 		$this->Cell(0,14,'Page '.$this->PageNo(),1,0,'C');
 	}

 	function myCells($w,$h,$x,$t){
 		$height = $h/3;
 		$first = $height+2;
 		$second = $height+$height+$height+3;
 		$len = strlen($t);
 		if($len>15){
 			$txt = str_split($t, 15);
 			$this->SetX($x);
 			$this->Cell($w,$first,$txt[0],'','','');
 			$this->SetX($x);
 			$this->Cell($w, $second,$txt[1],'','','');
 			$this->SetX($x);
 			$this->Cell($w,$h,'','LTRB',0,'L',0);
 		}else{
 			$this->SetX($x);
 			$this->Cell($w,$h,$t,'LTRB',0,'L',0);
 		}
 	}

 }
 $selectStudent = mysqli_query($con,"Select * from faculty_account");
 $pdf = new FPDF('L','in',array(8.5,14));
 //var_dump(get_class_methods($fpdf));
 
 $pdf->AddPage();
 
	
	$pdf->SetFont("Arial","B","15");
	$pdf->Ln(.5);
	$pdf->SetLeftMargin(.2);
	$pdf->Cell(0,0,"School Form 1 (SF 1) School Register",0,1,"C"); 
	$pdf->Ln(.0001);
	$pdf->SetFont("Arial","I","8");
	$pdf->Cell(0,0.35,"(This replaced Forms 1, Master List & STS Form 2-Family Background and Profile)",0,1,"C"); 
	
	$pdf->Image('Report/kne.png',.17,.8,1.1,1.1);
	$pdf->Image('Report/deped.png',11.9,.8,2,.8);
	
	//Region
	$pdf->SetFont("Arial","","10");
	$pdf->Cell(1.9,0.35,"School ID",0,0,"R");
	
	$pdf->SetFont("Arial","B","12");
	$pdf->Cell(1.5,0.35,"405152",1,0,"C");
	
	
	$pdf->SetFont("Arial","","10");
	$pdf->Cell(1,0.35,"Region",0,0,"R");
	
	$pdf->SetFont("Arial","B","12");
	$pdf->Cell(0.7,0.35,"X",1,0,"C");


	//Division
	$pdf->SetFont("Arial","","10");
	$pdf->Cell(1,0.35,"Division",0,0,"R");
	
	$pdf->SetFont("Arial","B","12");
	$pdf->Cell(2.4,0.35,"MISAMIS ORIENTAL",1,0,"C");

	//District
	$pdf->SetFont("Arial","","10");
	$pdf->Cell(.6,0.35,"District",0,0,"R");
	
	$pdf->SetFont("Arial","B","12");
	$pdf->Cell(2,0.35,"INITAO",1,1,"C");


	
	
	
	//New Line
	$pdf->Ln(.1);
	//School Name
	$pdf->SetFont("Arial","","10");
	$pdf->Cell(1.9,0.35,"School Name",0,0,"R");
	
	$pdf->SetFont("Arial","B","12");
	$pdf->Cell(3.2,0.35,"XAVIER ACADEMY",1,0,"C");
	
	//School Year
	$pdf->SetFont("Arial","","10");
	$pdf->Cell(1,0.35,"School Year",0,0,"R");
	
	$pdf->SetFont("Arial","B","12");
	$pdf->Cell(1.9,0.35,"",1,0,"L");
	
	//Grade Level
	$pdf->SetFont("Arial","","10");
	$pdf->Cell(1.1,0.35,"Grade Level",0,0,"R");
	
	$pdf->SetFont("Arial","B","12");
	$pdf->Cell(1.45,0.35,"",1,0,"L");
	
	//Section
	$pdf->SetFont("Arial","","10");
	$pdf->Cell(0.55,0.35,"Section",0,0,"R");
	
	$pdf->SetFont("Arial","B","12");
	$pdf->Cell(2.5,0.35,"",1,1,"L");
	
	//HEADING sa TABLE
	$pdf->Ln(.1);
	$pdf->SetFont("Arial","","8");
	$pdf->Cell(.4,1.2,"NO.",1,0,"C");

	$pdf->SetFont("Arial","B","11");

	$pdf->Cell(2,1.2,"LEARNER'S NAME asdasdasdasdasdasdasdasdasd",1,0,"C");

	$pdf->SetFont("Arial","I","8");
	$pdf->Cell(1.2,.60,"Subject Area & Title",1,0,"C");
	$pdf->Cell(1.2,.60,"Subject Area & Title",1,0,"C");
	$pdf->Cell(1.2,.60,"Subject Area & Title",1,0,"C");
	$pdf->Cell(1.2,.60,"Subject Area & Title",1,0,"C");
	$pdf->Cell(1.2,.60,"Subject Area & Title",1,0,"C");
	$pdf->Cell(1.2,.60,"Subject Area & Title",1,0,"C");
	$pdf->Cell(1.2,.60,"Subject Area & Title",1,0,"C");
	$pdf->Cell(1.2,.60,"Subject Area & Title",1,0,"C");
	$pdf->Cell(1.6,1.2,"REMARK/ACTION TAKEN",1,0,"C");
	$pdf->Cell(0,.6,"",0,1);


	$pdf->SetFont("Arial","","9");
 	$pdf->Cell(2.4,.50,'',0,0);
 	$pdf->Cell(1.2,.30,"Date",1,0,"C");
 	$pdf->Cell(1.2,.30,"Date",1,0,"C");
 	$pdf->Cell(1.2,.30,"Date",1,0,"C");
 	$pdf->Cell(1.2,.30,"Date",1,0,"C");
 	$pdf->Cell(1.2,.30,"Date",1,0,"C");
 	$pdf->Cell(1.2,.30,"Date",1,0,"C");
 	$pdf->Cell(1.2,.30,"Date",1,0,"C");
	$pdf->Cell(1.2,.30,"Date",1,0,"C");
	$pdf->Cell(1.2,.30,"",0,1);

	$pdf->SetFont("Arial","","7");
	$pdf->Cell(2.4,.50,'',0,0);
	$pdf->Cell(.6,.30,"Issued",1,0,"C");
	$pdf->Cell(.6,.30,"Returned",1,0,"C");
	$pdf->Cell(.6,.30,"Issued",1,0,"C");
	$pdf->Cell(.6,.30,"Returned",1,0,"C");
	$pdf->Cell(.6,.30,"Issued",1,0,"C");
	$pdf->Cell(.6,.30,"Returned",1,0,"C");
	$pdf->Cell(.6,.30,"Issued",1,0,"C");
	$pdf->Cell(.6,.30,"Returned",1,0,"C");
	$pdf->Cell(.6,.30,"Issued",1,0,"C");
	$pdf->Cell(.6,.30,"Returned",1,0,"C");
	$pdf->Cell(.6,.30,"Issued",1,0,"C");
	$pdf->Cell(.6,.30,"Returned",1,0,"C");
	$pdf->Cell(.6,.30,"Issued",1,0,"C");
	$pdf->Cell(.6,.30,"Returned",1,0,"C");
	$pdf->Cell(.6,.30,"Issued",1,0,"C");
	$pdf->Cell(.6,.30,"Returned",1,0,"C");
	$pdf->Cell(1.6,.30,"",0,1,"C");

	//CONTENTasdasdasdasdsaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
	
	$pdf->Cell(.4,.3,"NO.",1,0,"C");
	$pdf->Cell(2,.3,"LEARNER'S NAME",1,0,"C");
	$pdf->Image('Report/deped.png',11.9,.8,2,.8);
	$pdf->Cell(.6,.30,"",1,0,"C");
	$pdf->Cell(.6,.30,"",1,0,"C");
	$pdf->Cell(.6,.30,"",1,0,"C");
	$pdf->Cell(.6,.30,"",1,0,"C");
	$pdf->Cell(.6,.30,"",1,0,"C");
	$pdf->Cell(.6,.30,"",1,0,"C");
	$pdf->Cell(.6,.30,"",1,0,"C");
	$pdf->Cell(.6,.30,"",1,0,"C");
	$pdf->Cell(.6,.30,"",1,0,"C");
	$pdf->Cell(.6,.30,"",1,0,"C");
	$pdf->Cell(.6,.30,"",1,0,"C");
	$pdf->Cell(.6,.30,"",1,0,"C");
	$pdf->Cell(.6,.30,"",1,0,"C");
	$pdf->Cell(.6,.30,"",1,0,"C");
	$pdf->Cell(.6,.30,"",1,0,"C");
	$pdf->Cell(.6,.30,"",1,0,"C");
	$pdf->Cell(1.6,.30,"",1,0,"C");
	$pdf->Cell(1.6,.30,"",0,1,"C");




	//TOTAL FOR MALE
	$pdf->Cell(.4,.3,"",1,0,"C");
	$pdf->Cell(2,.3,"TOTAL FOR MALE | TOTAL COPIES",1,0,"C");
	$pdf->Cell(.6,.30,"",1,0,"C");
	$pdf->Cell(.6,.30,"",1,0,"C");
	$pdf->Cell(.6,.30,"",1,0,"C");
	$pdf->Cell(.6,.30,"",1,0,"C");
	$pdf->Cell(.6,.30,"",1,0,"C");
	$pdf->Cell(.6,.30,"",1,0,"C");
	$pdf->Cell(.6,.30,"",1,0,"C");
	$pdf->Cell(.6,.30,"",1,0,"C");
	$pdf->Cell(.6,.30,"",1,0,"C");
	$pdf->Cell(.6,.30,"",1,0,"C");
	$pdf->Cell(.6,.30,"",1,0,"C");
	$pdf->Cell(.6,.30,"",1,0,"C");
	$pdf->Cell(.6,.30,"",1,0,"C");
	$pdf->Cell(.6,.30,"",1,0,"C");
	$pdf->Cell(.6,.30,"",1,0,"C");
	$pdf->Cell(.6,.30,"",1,0,"C");
	$pdf->Cell(1.6,.30,"",1,0,"C");

 $pdf->Output();
?>