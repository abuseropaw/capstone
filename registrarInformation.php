<?php
    session_start();

    if($_SESSION['faculty_id'] == ''){ 
        header("Location: index.php");
    }
    include_once 'dbconnect.php';

    $teacherID =mysqli_real_escape_string($con, $_SESSION['faculty_id']);


    $query = mysqli_query($con, "SELECT * from faculty_account where faculty_id='".$teacherID."'");

    if($row = mysqli_fetch_array($query)){
        $id = $row[0];
        $fname=$row[1];
        $mname=$row[2];
        $lname=$row[3];
        $phone=$row[4];
        $address=$row[5];
        $religion=$row[6];
        $major=$row[7];
        $minor=$row[8];
        $dob=$row[9];
        $gender=$row[10];
        $type=$row[11];
        $picture=$row[12];
        $pass=$row[13];
        $dateCreated=$row[14];
        $createdBy=$row[15];
    }
    
    
?>
<!DOCTYPE html>
<!-- HEAD -->
    <?php include_once 'head.php'; ?> 
    <!-- HEAD   -->
    <body>
        <!-- HEADER -->
        <?php include_once 'header.php'; ?>
        <!-- HEADER -->

        <section id="main">
            <ol class="breadcrumb">
                <li><a href="registrarHome.php">Home</a></li>
                
            </ol>
            <?php 
                $toggle = 'registrarProfile';
                include_once 'registrarSidebar.php'; 
            ?>
            <section id="content">

                <div class="container">

                            <div class="block-header">
                                <h1><i class="zmdi zmdi-accounts"></i> My Profile
                                </h1>

                                
                            </div>
                                <div class="row">

                                    <div class="col-sm-12">
                                        <div class='card'  id='teacherInfo'>
                                            
                                            <div class="card-body">
                                                <div class="card" id="profile-main">
                                                    <div class="pm-overview c-overflow">
                                                        
                                                        <div class="pmo-pic">
                                                            <div class="p-relative">
                                                                <a href="#">
                                                                    <img class="img-responsive" src="profile/<?php echo ($picture == null) ? 'default.png':$picture ?>" alt="">
                                                                </a>

                                                                
                                                            </div>


                                                                
                                                        </div>

                                                        <div class="pmo-block pmo-contact hidden-xs">
                                                            <h2>Contact</h2>
                                                            <?php
                                                            echo "<ul>
                                                                <li><i class='zmdi zmdi-phone'></i>".$fname.' '.$mname.' '.$lname."</li>
                                                                <li>
                                                                    <i class='zmdi zmdi-pin'></i>
                                                                    <address class='m-b-0 ng-binding'>
                                                                        ".$address."
                                                                    </address>
                                                                </li>
                                                            </ul>";
                                                            ?>
                                                        </div>

                                                    
                                                    </div>

                                                    <div class="pm-body clearfix">
                                                        <div role="tabpanel">
                                                            <ul class="tab-nav tn-justified" role="tablist" data-tab-color="green">
                                                                <li class="active"><a href="#acount" aria-controls="acount" role="tab"
                                                                  data-toggle="tab">Account</a></li>
                                                                <li><a href="#class" aria-controls="class" role="tab" data-toggle="tab">Logs</a></li>
                                                                
                                                               
                                                            </ul>
                                                            <div class="tab-content">
                                                                <div role="tabpanel" class="tab-pane active" id="acount">
                                                                    <div class="pmb-block">
                                                                        <div class="pmbb-header">
                                                                            <h2><i class="zmdi zmdi-account m-r-10"></i> Basic Information</h2>

                                                                           
                                                                        </div>
                                                                        <div class="pmbb-body p-l-30">
                                                                            <div class="pmbb-view">
                                                                                <?php
                                                                                echo "
                                                                                <dl class='dl-horizontal'>
                                                                                    <dt>First Name</dt>
                                                                                    <dd>".$fname."</dd>
                                                                                </dl>
                                                                                <dl class='dl-horizontal'>
                                                                                    <dt>Middle Name</dt>
                                                                                    <dd>".$mname."</dd>
                                                                                </dl>
                                                                                <dl class='dl-horizontal'>
                                                                                    <dt>Last Name</dt>
                                                                                    <dd>".$lname."</dd>
                                                                                </dl>
                                                                                <dl class='dl-horizontal'>
                                                                                    <dt>Gender</dt>
                                                                                    <dd>".$gender."</dd>
                                                                                </dl>
                                                                                  
                                                                                <dl class='dl-horizontal'>
                                                                                    <dt>Birthday</dt>
                                                                                    <dd>".$dob."</dd>
                                                                                </dl>
                                                                                <dl class='dl-horizontal'>
                                                                                    <dt>Position</dt>
                                                                                    <dd>".$type."</dd>
                                                                                </dl> 
                                                                                <dl class='dl-horizontal'>
                                                                                    <dt>Minor Subject</dt>
                                                                                    <dd>".$minor."</dd>
                                                                                </dl>
                                                                                <dl class='dl-horizontal'>
                                                                                    <dt>Major Subject</dt>
                                                                                    <dd>".$major."</dd>
                                                                                </dl>

                                                                                <dl class='dl-horizontal'>
                                                                                    <dt>Contant No.</dt>
                                                                                    <dd>".$phone."</dd>
                                                                                </dl>
                                                                                <dl class='dl-horizontal'>
                                                                                    <dt>Home Address</dt>
                                                                                    <dd>".$address."</dd>
                                                                                </dl>
                                                                                ";
                                                                                ?>
                                                                            </div>
                                                                            
                                                                            
                                                                        </div>
                                                                    </div>

                                                                    
                                                                </div>
                                                                <div role="tabpanel" class="tab-pane" id="class">
                                                                    <div class="pmb-block">
                                                                        <div class="pmbb-header">
                                                                            <h2><i class="zmdi zmdi-account m-r-10"></i> Logs</h2>

                                                                            <ul class="actions">
                                                                                <li class="dropdown">
                                                                                    <a href="#" data-toggle="dropdown">
                                                                                        <i class="zmdi zmdi-more-vert"></i>
                                                                                    </a>

                                                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                                                        <li>
                                                                                            <a data-ma-action="profile-edit" href="#">Edit</a>
                                                                                        </li>
                                                                                    </ul>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                        <div class="pmbb-body p-l-30">
                                                                            
                                                                        </div>
                                                                    </div>
                                                                    
                                                                </div>
                                                                
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            
                            </div>
                        
                            </div>
                        </div>
            </section>
        </section>

        <!-- FOOTER -->
        <?php include_once 'footer.php' ?>
        <!-- FOOTER -->

        <!-- Javascript Libraries -->
        <?php include_once 'scripts.php'; ?>
        <!-- Javascript Libraries -->
        <script type="text/javascript">
            $(document).ready(function() {
                $('#data-table-basic').DataTable();

                //PAG SEARCH
                $('#search').keyup(function(){
                    var txt = $(this).val();
                    if(txt != '')
                    {
                        $.ajax({
                            url:"ajaxJQuery/searchTeacher.php",
                            method:"POST",
                            data:{search:txt},
                            dataType:"text",
                            success:function(data)
                            {
                                $('#resultsaSearch').html(data);
                            }
                        });
                    }
                    
                });



                $('#insert_form').on("submit", function(event){
                    event.preventDefault();
                    $('#faculty_fname').focus();
                    if($('#faculty_fname').val() == '')  
                    {
                            $('#faculty_fname').focus();    
                         alert("First name is required and valid");
                         
                    }  
                    else if($('#faculty_mname').val() == '')  
                    {  
                        $('#faculty_mname').focus();  
                         alert("Middle name is required and valid");  
                    }  
                    else if($('#faculty_lname').val() == '')  
                    {
                        $('#faculty_lname').focus();
                         alert("Maximun grade is required");  
                    }

                    else if($('#faculty_addressHSSP').val() =='')
                    {
                        $('#faculty_addressHSSP').focus();
                        alert("Home address is required and valid!");
                    }
                    else if($('#faculty_addressBarangay').val() == '')
                    {
                        $('#faculty_addressBarangay').focus();
                        alert("Barangay address is required and valid!");
                    }
                    else if($('#faculty_addressMunicipality').val() == 'MUNICIPALITY')
                    {
                        $('#faculty_addressMunicipality').focus();
                        alert("Municipality address is required and valid!");
                    }
                    else if($('#faculty_addressProvince').val() == 'PROVINCE')
                    {
                        $('#faculty_addressProvince').focus();
                        alert("Province address is required and valid!");
                    }
                    else if($('#faculty_phone').val() == '')  
                    {
                        $('#faculty_phone').focus();  
                         alert("Phone is required and valid");  
                    }

                    else if($('#faculty_religion').val() == 'RELIGION')
                    {
                        $('#faculty_religion').focus();
                        alert("You must select religion!");
                    }
                    else if($('#faculty_dob').val() == '')
                    {
                        $('#faculty_dob').focus();
                        alert("Date of birth is required and valid!");
                    }

                    else if($('#faculty_gender').val() == 'GENDER')
                    {
                        $('#faculty_gender').focus();
                        alert("You must select gender!");
                    }
                    
                    else  
                    {  
                         $.ajax({  
                              url:"ajaxJQuery/insertTeacher.php",  
                              method:"POST",  
                              data:$('#insert_form').serialize(),  
                              beforeSend:function(){  
                                   $('#insert1').val("Inserting..");   
                              },  
                              success:function(data){ 
                                   swal("Good job", "You successfully created Teacher account", "success");  
                                   $('#insert_form')[0].reset();  
                                   $('#modalColor').card('hide');  
                                   $('#sectionList').html(data); 
                              }  

                         });  
                    }
                });





            } );
        </script>
    </body>

</html>