<?php
    include_once 'sessionAdmin.php';
    include_once 'dbconnect.php';

    $sectionId = mysqli_real_escape_string($con, $_GET['sID']);

    if(isset($_POST['insert'])){
        
        $nameofInstructor = mysqli_real_escape_string($con, $_POST['instructor']);
        $subjectDetails = mysqli_real_escape_string($con, $_POST['subject']);
        $start = mysqli_real_escape_string($con, $_POST['start']);
        $end = mysqli_real_escape_string($con, $_POST['end']);
        $day =mysqli_real_escape_string($con, $_POST['days']);
        $days=implode(',', $_POST['days']);
        

        
        $getInstructorID = mysqli_fetch_row(mysqli_query($con,"SELECT faculty_id from facultysmalldetail where Name='".$nameofInstructor."'"));
        $InstructorID = $getInstructorID[0];
        $getNumberofSubjectOffering = mysqli_query($con, "SELECT * from subjectoffering");
        $count = mysqli_num_rows($getNumberofSubjectOffering);


                                                        
        if($subjectDetails == 'All Subject'){
            $yearofSection = mysqli_query($con, "SELECT year_lvl_id from section where section_id='".$sectionId."'");
            $temp = mysqli_fetch_row($yearofSection);
            $level = $temp[0];
            $gettheSubjects = mysqli_query($con, "SELECT * from subjects where year_lvl_id='".$level."'");
            $numberofSubjects = mysqli_num_rows($gettheSubjects);
            while($row = mysqli_fetch_array($gettheSubjects)){
                $countthenumberofSO = mysqli_num_rows(mysqli_query($con, "SELECT * from subjectoffering"));
                $tempSubjectID = $row['subj_id'];


                $findExisting = mysqli_query($con, "SELECT * from subjectoffering where section_id='".$sectionId."' and subj_id='".$tempSubjectID."'");

                if($row = mysqli_fetch_array($findExisting)){

                }else{
                    if(mysqli_query($con, "INSERT into subjectoffering(so_id,so_startTime,so_endTime,so_days,so_creationDate,so_createdBy,subj_id,faculty_id,section_id) VALUES('1000000','','','',NOW(),'".$_SESSION['faculty_id']."','".$tempSubjectID."',null,'".$sectionId."')")){
                        
                    }else{
                        $ID = getMax($con);
                        $ID++;
                        if(mysqli_query($con, "INSERT into subjectoffering(so_id,so_startTime,so_endTime,so_days,so_creationDate,so_createdBy,subj_id,faculty_id,section_id) VALUES('".$ID."','','','',NOW(),'".$_SESSION['faculty_id']."','".$tempSubjectID."',null,'".$sectionId."')")){
                            
                        }else{
                        echo "<script>
                            alert('Error!!');xattr_get(filename, name)
                            </script>";
                            
                        }
                    }
                }
            }
            
            

        }else{

          


            if(mysqli_query($con, "INSERT INTO subjectoffering(so_id,so_startTime,so_endTime,so_days,so_creationDate,so_createdBy,subj_id,faculty_id,section_id) VALUES('1000000','".$start."','".$end."','".$days."',NOW(),'".$_SESSION['faculty_id']."','".$subjectDetails."','".$InstructorID."','".$sectionId."')")){
                echo "<script>alert('1');</script>";
            }else{
                $ID = getMax($con);
                        $ID++;
                if(mysqli_query($con, "INSERT INTO subjectoffering(so_id,so_startTime,so_endTime,so_days,so_creationDate,so_createdBy,subj_id,faculty_id,section_id) VALUES('".$ID."','".$start."','".$end."','".$days."',NOW(),'".$_SESSION['faculty_id']."','".$subjectDetails."','".$InstructorID."','".$sectionId."')")){
                echo "<script>alert('2');</script>";
                }else{
                    echo "<script>alert('3');</script>";
                }
            }

            echo "<script>alert('4');</script>";
        }

        header('Location: classSchedule.php?sID='.$sectionId.'');
    }


    function checkifCurrent($con, $sectionID)
    {
        $query = mysqli_fetch_row(mysqli_query($con, "SELECT * from section where section_id='".$sectionID."'"));
        $sSY = $query[8];

        $query1= mysqli_fetch_row(mysqli_query($con, "SELECT * from schoolyear where sy_remarks='Open'"));
        $SY = $query1[1];
        if($sSY == $SY)
        {
            return true;
        }
        else{
            return false;
        }

    }


    
    // if(isset($_POST['update'])){
       
    //     $nameofInstructor = mysqli_real_escape_string($con, $_POST['instructor1']);
    //     $so_id = mysqli_real_escape_string($con, $_POST['so_id']);
    //     $start = mysqli_real_escape_string($con, $_POST['start1']);
    //     $end = mysqli_real_escape_string($con, $_POST['end1']);
        
    //     $days=implode(',', $_POST['days1']);

    //     $getInstructorID = mysqli_fetch_row(mysqli_query($con,"SELECT faculty_id from facultysmalldetail where Name='".$nameofInstructor."'"));
    //     $InstructorID = $getInstructorID[0];
        


    //     if(mysqli_query($con, "UPDATE subjectoffering set so_startTime='".$start."', so_endTime='".$end."', so_days='".$days."', faculty_id='".$InstructorID."' where so_id='".$so_id."'")){
            
    //     }

    //     header('Location: classSchedule.php?sID='.$sectionId.'');
    // }

    function getMax($con)
    {
        $query = mysqli_fetch_row(mysqli_query($con, "SELECT max(so_id) from subjectoffering"));
        $tmp = $query[0];
        return $tmp;
    }
	
    


    
?>
<!DOCTYPE html>
    <!-- HEAD -->
    <?php include_once 'head.php'; ?> 
    <!-- HEAD   -->
    <body>
        <!-- HEADER -->
        <?php include_once 'header.php'; ?>
        <!-- HEADER -->

        <section id="main">
            <ol class="breadcrumb">
                <li><a href="adminHome.php">Home</a></li>
                <li><a href="adminClass.php">Classes</a></li>
                <li class="active">Class Schedule</li>
            </ol>
            <?php 
                $toggle = 'adminClass';
                include_once 'sidebar.php'; 
            ?>

            
            <section id="content">
                <div class="container">
                    <!-- Colored Headers -->
                    <div class="block-header">
                        <h1><?php
                        		$sectionId = $_GET['sID'];
                        		$query = mysqli_query($con, "SELECT * from classinformation where section_id='".$_GET['sID']."'");
                        		if($row = mysqli_fetch_array($query)){
                        			echo $row['section_name'];
                        		}
                        	?>
                        </h1>

                        <div class="actions">
                            <div class="btn-demo">
                            <?php
                                $sectionId = $_GET['sID'];
                                $query = mysqli_query($con, "SELECT * from subjectoffering where section_id='".$sectionId."'");
                                $numberofSection = mysqli_num_rows($query);

                                $yearofSection = mysqli_query($con, "SELECT year_lvl_id from section where section_id='".$sectionId."'");
                                $temp = mysqli_fetch_row($yearofSection);
                                $level = $temp[0];
                                $gettheSubjects = mysqli_query($con, "SELECT * from subjects where year_lvl_id='".$level."'");
                                $numberofSubjects = mysqli_num_rows($gettheSubjects);

                                if($numberofSection == $numberofSubjects){
                                    echo "<button  href='#' data-toggle='tooltip' data-placement='bottom' title='No available subject to be added. Add first subject in order to have assign' class='btn btn-default btn-lg'><b>New Schedule</b></button>";
                                }else{
                                    if(checkifCurrent($con,$sectionId)){
                                    echo "<button  href='#modalColor' data-toggle='modal' class='btn btn-default btn-lg'><b>New Schedule</b></button>";
                                    }
                                    else
                                    {
                                        
                                    }
                                }
                            ?>

                            </div>
                        </div>
                        <div class="modal fade" id="modalColor" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                
                                	<form method="post" id='insertSchedule' >
                                
                                        <div class="modal-header">
                                            <h2 class="modal-title"><b>CREATE NEW SCHEDULE</b></h2>
                                        </div>
                                        
                                        <div class="modal-body">
                                            <div class='col-sm-12'>
                                                <div class='form-group fg-line'>
                                                    <select class="selectpicker form-control input-lg" data-live-search="true" name="subject">
                                                    	<option>Subject</option>
                                                        <option value="All Subject">All Subject</option>
                                                    	<?php
                                                    		
                                                    		$yearofSection = mysqli_query($con, "SELECT year_lvl_id from section where section_id='".$sectionId."'");
                                                    		$temp = mysqli_fetch_row($yearofSection);
                                                    		$level = $temp[0];


                                                    		$gettheSubjects = mysqli_query($con, "SELECT * from subjects where year_lvl_id='".$level."'");
                                                    		while ($row = mysqli_fetch_array($gettheSubjects)) {
                                                                $su = $row['subj_id'];

                                                                $checkExist = mysqli_query($con, "SELECT * from subjectoffering where subj_id='".$su."' and section_id='".$_GET['sID']."'");
                                                                if($r = mysqli_fetch_array($checkExist)){
                                                                    
                                                                }
                                                                else{
                                                    			echo "<option value='".$row[0]."'>" .$row['subj_title']. "- " .$row['subj_description']. "</option>";
                                                                }
                                                    		}
                                                    	?>	

                                                    </select>
                                                </div>
                                            </div>
                                            <div class='col-sm-12'>
                                                <div class='form-group fg-line'>
                                                    <select class="selectpicker form-control" data-live-search="true" name="instructor">
                                                    	<option>Subject teacher</option>
                                                    	<?php
                                                    		$gettheInstructor = mysqli_query($con, "SELECT * from facultysmalldetail where faculty_type='Teacher'");
                                                    		while ($row = mysqli_fetch_array($gettheInstructor)) {
                                                    			echo "<option>" .$row['Name']. "</option>";
                                                    		}
                                                    	?>	

                                                    </select>

                                                </div>
                                            </div>
                                            <div class='col-sm-6'>
                                                <div class='form-group fg-line'>
                                                    <input type='text' name="start" class="form-control time-picker" placeholder="Start time">
                                                </div>
                                            </div>
                                            <div class='col-sm-6'>
                                                <div class='form-group fg-line'>
                                                    <input type='text' name="end" class="form-control time-picker" placeholder="End time">
                                                </div>
                                            </div>
                                            <div class='col-sm-12'>
                                                <div class='form-group fg-line'>
                                                    <label class="checkbox checkbox-inline m-r-15">
						                                <input type="checkbox" name="days[]" value="M">
						                                <i class="input-helper"></i>
						                                Monday
						                            </label>
						                            <label class="checkbox checkbox-inline m-r-15">
						                            	<input type="checkbox" name="days[]" value="T">
						                            	<i class="input-helper"></i>
						                                Tuesday
						                            </label>
						                            <label class="checkbox checkbox-inline m-r-15">
						                                <input type="checkbox" name="days[]" value="W">
						                                <i class="input-helper"></i>
						                                Wednesday
						                            </label>
						                            <label class="checkbox checkbox-inline m-r-15">
						                            	<input type="checkbox" name="days[]" value="Th">
						                            	<i class="input-helper"></i>
						                                Thursday
						                            </label>
						                            <label class="checkbox checkbox-inline m-r-15">
						                            	<input type="checkbox" name="days[]" value="F">
						                            	<i class="input-helper"></i>
						                                Friday
						                            </label>
                                                </div>
                                            </div>
                                            


                                            <hr> 
                                        </div>
                                        <div class="modal-footer">
                                            
                                            <button type="submit" name="insert" id="insert" class="btn btn-success">CREATE SCHEDULE</button>
                                            <button type="button" class="btn btn-warning" data-dismiss="modal">CLOSE
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>

                    
                    <div id='hehe'>
                    	<div class="card-body card-padding">
                            <div class="row">
                                <div id='schedule'>
                                    <?php include_once 'ajaxJQuery/displaySchedule.php'; ?>
                                </div>
                                <div class="col-sm-7">
                                    <div class="card">
                                        
                                        <div class="card-body" id="listofStudents">
                                            <div class="table-responsive">
                                                <table class="table table-bordered table-nowrap">
                                                    <thead>
                                                        <tr>
                                                        <th rowspan="2" class="text-center"><b>LRN</b></th>
                                                        <th rowspan="2" class="text-center"><b>Name</b></th>
                                                        <th rowspan="2" class="text-center"><b>Remarks</b></th>
                                                        <th colspan="5" class="text-center"><b>Grade</b></th>
                                                        <th rowspan="2" class="text-center"><b>Details/Edit Grade</b></th>
                                                       
                                                        </tr>

                                                        <tr>
                                                        <th class="text-center"><small><small>1st</small></small></th>
                                                        <th class="text-center"><small><small>2nd</small></small></th>
                                                        <th class="text-center"><small><small>3rd</small></small></th>
                                                        <th class="text-center"><small><small>4th</small></small></th>
                                                        <th class="text-center"><small><small>Average</small></small></th>
                                                        
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    
                    </div>




                    <br/>
                    <br/>
                </div>
            </section>
        </section>
        <div class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                
                    <form method="post" id='updateSchedule'>
                    
                
                        <div class="modal-header">
                            <h2 class="modal-title"><b>EDIT SCHEDULE</b></h2>
                        </div>
                        
                        <div class="modal-body">
                            <div class='col-sm-12'>
                                <div class='form-group fg-line'>
                                    <input type="text" class="form-control" name="subject1" id='subject1' disabled>
                                </div>
                            </div>
                            <div class='col-sm-12'>
                                <div class='form-group fg-line'>
                                    <select class="selectpicker form-control" name="instructor1" id="instructor1">
                                        
                                        <?php
                                            $gettheInstructor = mysqli_query($con, "SELECT * from facultysmalldetail where faculty_type='Teacher'");
                                            while ($row = mysqli_fetch_array($gettheInstructor)) {
                                                echo "<option>" .$row['Name']. "</option>";
                                            }
                                        ?>  

                                    </select>

                                </div>
                            </div>
                            <div class='col-sm-6'>
                                <div class='form-group fg-line'>
                                    <input type='text' name="start1" id="start1" class="form-control time-picker" placeholder="Start time">
                                </div>
                            </div>
                            <div class='col-sm-6'>
                                <div class='form-group fg-line'>
                                    <input type='text' name="end1" id="end1" class="form-control time-picker" placeholder="End time">
                                    <input type="hidden" name="so_id" id="so_id">
                                </div>
                            </div>
                            <div class='col-sm-12'>
                                <div class='form-group fg-line'>
                                    <label class="checkbox checkbox-inline m-r-15">
                                        <input type="checkbox" name="days1[]" value="M">
                                        <i class="input-helper"></i>
                                        Monday
                                    </label>
                                    <label class="checkbox checkbox-inline m-r-15">
                                        <input type="checkbox" name="days1[]" value="T">
                                        <i class="input-helper"></i>
                                        Tuesday
                                    </label>
                                    <label class="checkbox checkbox-inline m-r-15">
                                        <input type="checkbox" name="days1[]" value="W">
                                        <i class="input-helper"></i>
                                        Wednesday
                                    </label>
                                    <label class="checkbox checkbox-inline m-r-15">
                                        <input type="checkbox" name="days1[]" value="Th">
                                        <i class="input-helper"></i>
                                        Thursday
                                    </label>
                                    <label class="checkbox checkbox-inline m-r-15">
                                        <input type="checkbox" name="days1[]" value="F">
                                        <i class="input-helper"></i>
                                        Friday
                                    </label>
                                </div>
                            </div>
                            


                            <hr> 
                        </div>
                        <div class="modal-footer">
                            
                            <button type="submit" name="update" id="update" class="btn btn-success">EDIT SCHEDULE</button>
                            <button type="button" class="btn btn-warning" data-dismiss="modal">CANCEL
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div id="historyGradeModal" class="modal fade" data-backdrop="static" data-keyboard="false">  
            <div class="modal-dialog">  
               <div class="modal-content">  
                    <div class="modal-header">  
                         <button type="button" class="close" data-dismiss="modal">&times;</button>  
                         <h4 class="modal-title"><b>GRADE HISTORY</b></h4>  
                    </div>  
                    <div class="modal-body" id="Details">  
                    </div>  
                    <div class="modal-footer">  
                         <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>  
                    </div>  
               </div>  
            </div>  
        </div>
        <!-- FOOTER -->
        <?php include_once 'footer.php' ?>
        <!-- FOOTER -->

        <!-- Javascript Libraries -->
        <?php include_once 'scripts.php'; ?>
        <!-- Javascript Libraries -->
        <script type="text/javascript" src="js/Notification.js"></script>
        <script type="text/javascript">

            function openModal()
            {
                exec();
            }
            function exec(){
                $('.history_grade').on("click",function(e){
                    e.preventDefault();
                    var soid = $(this).attr("name");
                    var controlID =$(this).attr("id"); 
                    $.ajax({
                        url:"ajaxJQuery/viewHistoryGrades.php",
                        method: "get",
                        data:{controlID:controlID, soid:soid},
                        success:function(data){
                            $('#Details').html(data);
                            $('#historyGradeModal').modal('show');
                        }
                    });
                });
            }
            
            $(document).ready(function(){

                // $('#insertSchedule').on('submit', function(e){
                //     e.preventDefault();
                //     var sectionID = "<?php echo $sectionId; ?>";
                //     $.ajax({
                //         url: "ajaxJQuery/insertSchedule.php?section="+sectionID,
                //         method: "post",
                //         data: $(this).serialize(),
                //         dataType: "html",
                //         success:function(data){
                //             if(data != "0")
                //             {
                //                 // $('#updateModal').modal('hide');
                //                 // $('#schedule').html(data);
                //                 // var message = "Successfully updated schedule";
                //                 // executeNotif(message,"success");
                //                 //location.reload();   
                //             }
                //             else
                //             {
                //                 $('#modalColor').modal('hide');
                //                 var message = "Error on adding schedule";
                //                 executeNotif(message,"danger");
                //             }
                //         }
                //     });
                // });


                $('#updateSchedule').on('submit', function(e){
                    e.preventDefault();
                    var sectionID = "<?php echo $sectionId; ?>";
                    $.ajax({
                        url: "ajaxJQuery/updateSchedule.php?section="+sectionID,
                        method: "post",
                        data: $(this).serialize(),
                        dataType: "html",
                        success:function(data){
                            if(data != "0")
                            {
                                // $('#updateModal').modal('hide');
                                // $('#schedule').html(data);
                                // var message = "Successfully updated schedule";
                                // executeNotif(message,"success");
                                location.reload();   
                            }
                            else
                            {
                                $('#updateModal').modal('hide');
                                var message = "Error on updating schedule";
                                executeNotif(message,"danger");
                            }
                        }
                    });
                });


                $('[data-dismiss=modal]').on('click', function (e) {
                    $('#updateSchedule')[0].reset();
                });


                $(document).on('click', '.edit_Level', function(){
                    var so_id = $(this).attr("id");
                    $.ajax({
                        url:"ajaxJQuery/selectSubjectOffering.php",
                        method:"POST",
                        data:{so_id:so_id},
                        dataType:"json",
                        success:function(data){
                            $('#subject1').val(data.SubjectTitle);

                            if(data.Teacher == null)
                            {
                                $('#instructor1').prepend('<option selected=""></option>');
                            }
                            else
                            {
                                $('#instructor1').prepend('<option selected="">'+data.Teacher+'</option>');
                            }
                            
                            $('#instructor1').selectpicker('refresh');
                            $('#start1').val(data.so_startTime);
                            $('#end1').val(data.so_endTime);
                            
                            var myString = data.so_days;
                            var arr = myString.split(',');
                            for(var i = 0; i < arr.length;i++) {
                                $(':checkbox[value="'+arr[i]+'"]').prop('checked', true);
                            }

                            $('#so_id').val(so_id);
                            
                            $('#updateModal').modal('show');
                        }

                    });
                    $('#updateModal').modal('show');
                });

                $('.monitor').on('click', function(e){
                    e.preventDefault();
                    var soid = $(this).attr("id");
                    var sid ="<?php echo $sectionId; ?>"; 
                    $.ajax({
                        url:"ajaxJQuery/monitorClass.php",
                        method: "get",
                        data:{sid:sid, soid:soid},
                        dataType: "html",
                        success:function(data){
                            $('#listofStudents').html(data);
                        }
                    });
                });


            });
        </script>
    </body>
  
<!-- Mirrored from byrushan.com/projects/ma/1-7-1/jquery/light/widget-templates.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 01 May 2017 06:35:54 GMT -->
</html>