<?php

	include_once 'dbconnect.php';
    require_once "PHPExcel-1.8.1/Classes/PHPExcel.php";

    $level = mysqli_real_escape_string($con, $_GET['level']);
    // switch ($level) {
    // 	case 'Grade%207':
    // 		$level = 'Grade 7';
    // 		break;
    // 	case 'Grade%208':
    // 		$level = 'Grade 8';
    // 		break;
    // 	case 'Grade%209':
    // 		$level = 'Grade 9';
    // 		break;
    // 	case 'Grade%2010':
    // 		$level = 'Grade 10';
    // 		break;
    // 	default:
    // 		# code...
    // 		break;
    // }

    // Load an existing spreadsheet
    $phpExcel = PHPExcel_IOFactory::load('School Forms Final/Class.xlsx');
    $sheet = $phpExcel ->getActiveSheet();

    $iter = 8;
    $counter = 1;

    $sheet ->setCellValue('B4', $level);

    $query = mysqli_query($con, "SELECT * from classinformation where GradeLevel = '".$level."'");
    while($row = mysqli_fetch_array($query))
    {
    	$sheet ->setCellValue('B'.$iter.'', $counter);
    	$sheet ->setCellValue('C'.$iter.'', $row[1]);
    	$sheet ->setCellValue('F'.$iter.'', $row[2]);
    	$sheet ->setCellValue('G'.$iter.'', $row[3]);
    	$sheet ->setCellValue('H'.$iter.'', $row[4]);


    	$counter++;
    }

    $writer = PHPExcel_IOFactory::createWriter($phpExcel, "Excel2007");

    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="Section List - '.$level.'.xlsx"');
    header('Cache-Control: max-age=0');
    $writer->save('php://output'); 

?>