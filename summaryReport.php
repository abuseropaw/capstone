<?php
	session_start();
	include_once 'dbconnect.php';
	require('Report/fpdf.php');
	$pdf = new FPDF('P','in',array(8.27,11.69));
	$pdf->AddPage();
	$type = mysqli_real_escape_string($con, $_GET['type']);
	$level = mysqli_real_escape_string($con, $_GET['level']);
	$sy =  mysqli_real_escape_string($con, $_GET['sy']);


	$pdf->SetFont("Arial","B","15");
	$pdf->Ln(.5);
	$pdf->SetLeftMargin(.2);
	switch ($type) {
		case 'promoted':
			$pdf->Cell(0,0.20,"Report on Promoted Learners   ",0,1,"C"); 
			setParentHeader($pdf,$sy,$level);
			setTableData1($con,$pdf,$level,$sy);
			setFooter($pdf);
			break;
		case 'irregular':
			$pdf->Cell(0,0.20,"Report on Irregular Learners",0,1,"C");
			setParentHeader($pdf,$sy,$level); 
			setTableData2($con,$pdf,$level,$sy);
			setFooter($pdf);
			break;
		case 'retained':
			$pdf->Cell(0,0.20,"Report on Retained Learners",0,1,"C"); 
			setParentHeader($pdf,$sy,$level);
			setTableData3($con,$pdf,$level,$sy);
			setFooter($pdf);
			break;
		default:
			# code...
			break;
	}
	
	
	
	$pdf->Output();



	function setParentHeader($pdf,$sy,$level)
	{
		$pdf->Ln(.0001);
		$pdf->SetFont("Arial","","12");
		$pdf->Cell(0,0.15,"Lugait National High School",0,1,"C");
		
		
		$pdf->Ln(.0001);
		$pdf->SetFont("Arial","","11");
		$pdf->Cell(0,0.15,"School Year: ".$sy,0,1,"C");
		$pdf->Ln(.0001);
		$pdf->SetFont("Arial","","11");
		$pdf->Cell(0,0.15,"Level: ".$level,0,1,"C");
		
		$pdf->Ln(.0001);
		$pdf->SetFont("Arial","","11");
		$pdf->Cell(0,0.15,date('M-d-Y'),0,1,"C");


		$pdf->Image('Report/kne.png',1,.75,.9,.9);
		$pdf->Image('Report/deped.png',6,.82,1.8,.8);

		$pdf->Ln(.3);
		$pdf->Ln(.1);
		
		setTableHeader($pdf);
		
	}



	function setTableData1($con,$pdf,$level,$sy)
	{
		$getLearners = mysqli_query($con, "SELECT * from controlDetails where SchoolYear='".$sy."' and gradelevel='".$level."' order by Gender DESC,Student ASC");
		$i = 1;
        while($row = mysqli_fetch_array($getLearners))
        {
            $id = $row['section_id'];
            $controlId = $row[0];
            $getAverageDetails = mysqli_query($con, "SELECT * from averageDetails where control_id='".$row[0]."'");
            if($row1 = mysqli_fetch_array($getAverageDetails))
            {
                if(($row[1] == 'DRP'))
                {
                }
                else{
                    //$ans = checkLearnersProficiency($con,$id,$controlId);
                    if(checkLearnersProficiency($con,$id,$controlId) == "irregular")
                    {                        
                    }
                    else if(checkLearnersProficiency($con,$id,$controlId) == "promote")
                    {
                        if(($row1[2] < 75))
                        {
                        }
                        else
                        {
                            $pdf->Ln();
							$pdf->SetFont("Arial","","9");
						 	$pdf->Cell(.5,.20,"",0,0,"C");
						 	$pdf->Cell(.4,.20,$i,1,0,"C");
						 	$pdf->Cell(1.3,.20,$row['student_id'],1,0,"L");
						 	$pdf->Cell(4,.20,characterConverter($row['Student']),1,0,"L");
		 				$pdf->Cell(1.4,.20,characterConverter($row['SectionName']),1,0,"L");
						 	$i++;    
                        }
                    }
                    else if(checkLearnersProficiency($con,$id,$controlId) == "retain")
                    { 
                    }
                }
                
            }
            else if($row[1] == 'DRP')
            {
            }
           
        }
	}
	function setTableData2($con,$pdf,$level,$sy)
	{
		$getLearners = mysqli_query($con, "SELECT * from controlDetails where SchoolYear='".$sy."' and gradelevel='".$level."' order by Gender DESC,Student ASC");
		$i = 1;
        while($row = mysqli_fetch_array($getLearners))
        {
            $id = $row['section_id'];
            $controlId = $row[0];
            $getAverageDetails = mysqli_query($con, "SELECT * from averageDetails where control_id='".$row[0]."'");
            if($row1 = mysqli_fetch_array($getAverageDetails))
            {
                
               

                if(($row[1] == 'DRP'))
                {
                }
                else{
                    //$ans = checkLearnersProficiency($con,$id,$controlId);
                    if(checkLearnersProficiency($con,$id,$controlId) == "irregular")
                    {
                    	$pdf->Ln();
						$pdf->SetFont("Arial","","9");
					 	$pdf->Cell(.5,.20,"",0,0,"C");
					 	$pdf->Cell(.4,.20,$i,1,0,"C");
					 	$pdf->Cell(1.3,.20,$row['student_id'],1,0,"L");
					 	$pdf->Cell(4,.20,characterConverter($row['Student']),1,0,"L");
		 				$pdf->Cell(1.4,.20,characterConverter($row['SectionName']),1,0,"L");
					 	$i++;                            
                    }
                    else if(checkLearnersProficiency($con,$id,$controlId) == "promote")
                    {
                        if(($row1[2] < 75))
                        {
                            
                            
                        }
                        else
                        {
                            
                            
                        }
                    }
                    else if(checkLearnersProficiency($con,$id,$controlId) == "retain")
                    {
                        
                        
                    }
                }
                 
            }
            else if($row[1] == 'DRP')
            {
               
                $femaleRetained++;
            }
           
        }
	}

	function setTableData3($con,$pdf,$level,$sy)
	{
		$getLearners = mysqli_query($con, "SELECT * from controlDetails where SchoolYear='".$sy."' and gradelevel='".$level."' order by Gender DESC,Student ASC");
		$i = 1;
        while($row = mysqli_fetch_array($getLearners))
        {
            $id = $row['section_id'];
            $controlId = $row[0];
            $getAverageDetails = mysqli_query($con, "SELECT * from averageDetails where control_id='".$row[0]."'");
            if($row1 = mysqli_fetch_array($getAverageDetails))
            {
                if(($row[1] == 'DRP'))
                {
                	$pdf->Ln();
					$pdf->SetFont("Arial","","9");
				 	$pdf->Cell(.5,.20,"",0,0,"C");
				 	$pdf->Cell(.4,.20,$i,1,0,"C");
				 	$pdf->Cell(1.3,.20,$row['student_id'],1,0,"L");
				 	$pdf->Cell(4,.20,characterConverter($row['Student']),1,0,"L");
	 				$pdf->Cell(1.4,.20,characterConverter($row['SectionName']),1,0,"L");
				 	$i++;
                }
                else{
                    //$ans = checkLearnersProficiency($con,$id,$controlId);
                    if(checkLearnersProficiency($con,$id,$controlId) == "irregular")
                    {
                    	                            
                    }
                    else if(checkLearnersProficiency($con,$id,$controlId) == "promote")
                    {
                        if(($row1[2] < 75))
                        {
                            
                            $pdf->Ln();
							$pdf->SetFont("Arial","","9");
						 	$pdf->Cell(.5,.20,"",0,0,"C");
						 	$pdf->Cell(.4,.20,$i,1,0,"C");
						 	$pdf->Cell(1.3,.20,$row['student_id'],1,0,"L");
						 	$pdf->Cell(4,.20,characterConverter($row['Student']),1,0,"L");
		 					$pdf->Cell(1.4,.20,characterConverter($row['SectionName']),1,0,"L");
						 	$i++;
                        }
                        else
                        {
                            
                            
                        }
                    }
                    else if(checkLearnersProficiency($con,$id,$controlId) == "retain")
                    {
                        $pdf->Ln();
						$pdf->SetFont("Arial","","9");
					 	$pdf->Cell(.5,.20,"",0,0,"C");
					 	$pdf->Cell(.4,.20,$i,1,0,"C");
					 	$pdf->Cell(1.3,.20,$row['student_id'],1,0,"L");
					 	$pdf->Cell(4,.20,characterConverter($row['Student']),1,0,"L");
		 				$pdf->Cell(1.4,.20,characterConverter($row['SectionName']),1,0,"L");
					 	$i++;
                        
                    }
                }
                
            }
            else if($row[1] == 'DRP')
            {
            	$pdf->Ln();
				$pdf->SetFont("Arial","","9");
			 	$pdf->Cell(.5,.20,"",0,0,"C");
			 	$pdf->Cell(.4,.20,$i,1,0,"C");
			 	$pdf->Cell(1.3,.20,$row['student_id'],1,0,"L");
			 	$pdf->Cell(4,.20,characterConverter($row['Student']),1,0,"L");
		 		$pdf->Cell(1.4,.20,characterConverter($row['SectionName']),1,0,"L");
			 	$i++; 
            }
           
        }
	}


	function setTableHeader($pdf)
	{
		$pdf->SetFont("Arial","B","10");
	 	$pdf->Cell(.5,.45,"",0,0,"C");
	 	$pdf->Cell(.4,.45,"NO",1,0,"C");
	 	$pdf->Cell(1.3,.45,"LRN",1,0,"C");
	 	$pdf->Cell(4,.45,"LEARNER'S NAME",1,0,"C");
	 	$pdf->Cell(1.4,.45,"SECTION",1,0,"C");
	 	
	}
	function setFooter($pdf)
	{
		$pdf->Ln(1);
		$pdf->SetFont("Arial","B","12");
		 	$pdf->Cell(.7,.30,"",0,0,"C");
		 	$pdf->Cell(1.7,.30,"",0,0,"L");
		 	$pdf->Cell(3.9,.30,"PREPARED BY:",0,0,"R");
		 $pdf->Ln();

		$pdf->SetFont("Arial","U","12");
		 $pdf->Cell(13,.30,"          ".characterConverter($_SESSION['Name'])."         ",0,0,"C");
		 	
	}
	function checkLearnersProficiency($con,$id,$controlId)
    {
        
        $countSubjectOffer = mysqli_num_rows(mysqli_query($con, "SELECT * from subjectoffering where section_id='".$id."'"));
        $countAverage = mysqli_num_rows(mysqli_query($con, "SELECT * from averagepersubjectoffering where control_id='".$controlId."'"));
        $query = mysqli_query($con, "SELECT * from averagepersubjectoffering where control_id='".$controlId."'");
        $count = 0;
        if($countAverage == $countSubjectOffer){
            while($row = mysqli_fetch_array($query)){
                if($row[0] < 75)
                {
                    $count++;
                }
            }
            if($count > 2)
            {
                return "retain";
            }
            else if($count <= 2 && $count != 0)
            {
                if($countSubjectOffer <= $count)
                {
                    return "retain";
                }
                else
                {
                    return "irregular";
                }
            }
            else
            {
                return "promote";
            }
           
        }else{
            return "retain";
        }
        

    }

    function characterConverter($string)
	{
		$padparan = stripslashes($string);
		$econvert = iconv('UTF-8', 'windows-1252', $padparan);

		return $econvert;
	}
?>