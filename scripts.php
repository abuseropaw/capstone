<script src="vendors/bower_components/jquery/dist/jquery.min.js"></script>

<script src="vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<script src="vendors/bower_components/flot/jquery.flot.js"></script>

<script src="vendors/bower_components/flot/jquery.flot.resize.js"></script>

<script src="vendors/bower_components/moment/min/moment.min.js"></script>

<script src="vendors/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
<script src="vendors/bower_components/Waves/dist/waves.min.js"></script>

<script src="vendors/bootstrap-growl/bootstrap-growl.min.js"></script>
<script src="vendors/bower_components/sweetalert2/dist/sweetalert2.min.js"></script>
<script src="vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<script src="vendors/bower_components/dropzone/dist/min/dropzone.min.js"></script>

<script src="vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.js"></script>
<script src="vendors/bower_components/nouislider/distribute/nouislider.min.js"></script>
<script src="vendors/bower_components/typeahead.js/dist/typeahead.bundle.min.js"></script>
<script src="vendors/summernote/dist/summernote-updated.min.js"></script>
<script src="vendors/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="vendors/bower_components/chosen/chosen.jquery.js"></script>
<script src="vendors/bower_components/jquery-mask-plugin/dist/jquery.mask.min.js"></script>
<script src="vendors/fileinput/fileinput.min.js"></script>
<script src="vendors/fileinput/fileinput.min.js"></script>
<script src="vendors/farbtastic/farbtastic.min.js"></script>
<script src="vendors/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
<script src="vendors/sparklines/jquery.sparkline.min.js"></script>
<script src="vendors/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js"></script>
<script src="js/app.min.js"></script>
<scirpt src="js/Notification.js"></script>
<script type="text/javascript" src="js/pace.min.js"></script>
<script src="js/highcharts.js"></script>
<script src="js/exporting.js"></script>
<script src="js/series-label.js"></script>