<?php
    session_start();
    if($_SESSION['faculty_id'] == ''){ header("Location: index.php");}else if( $_SESSION['faculty_type'] != 'Head Teacher' && $_SESSION['faculty_type'] != 'Registrar' ){header("Location: javascript:");}
    include_once 'dbconnect.php';

    if(isset($_POST['showall'])){

        $query = mysqli_query($con, "SELECT * from subjectDetails");

    }
    $getCurrentSY = mysqli_query($con, "SELECT sy_year from schoolyear where sy_remarks='Open'");
    $temp = mysqli_fetch_row($getCurrentSY);
    $SY = $temp[0];
    function fillSchoolYear($con){
        $output ='';
        $query = mysqli_query($con, "SELECT * from schoolyear");
        while($row = mysqli_fetch_array($query)){
            $output .='<option value="'.$row[1].'">'.$row[1].'</option>';
        }
        return $output;
    }
?>
<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie9"><![endif]-->
    
<!-- Mirrored from byrushan.com/projects/ma/1-7-1/jquery/light/widget-templates.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 01 May 2017 06:35:54 GMT -->
<!-- HEAD -->
    <?php include_once 'head.php'; ?> 
    <!-- HEAD   -->
    <body>
        <!-- HEADER -->
        <?php include_once 'header.php'; ?>
        <!-- HEADER -->
        
        <section id="main">
            
            <?php 
                $toggle = 'enrollment';
                include_once 'registrarSidebar.php'; 
            ?>
            <section id="content">
                <div class="container">
                    <!-- Colored Headers -->
                    
                    <br />
                    <div class="block-header">

                        

                        
                        
                    </div>

                    
                    <div id="hehe">
                        
                        <?php include_once 'ajaxJQuery/displayregistrarEnrollment.php'; ?>
                    </div>




                    <br/>
                    <br/>
                </div>
            </section>
        </section>

        
        <!-- FOOTER -->
        <?php include_once 'footer.php' ?>
        <!-- FOOTER -->

        <!-- Javascript Libraries -->
        <?php include_once 'scripts.php'; ?>
        <!-- Javascript Libraries -->
        <script type="text/javascript">
            $(document).ready(function() {
                $('#data-table-basic').DataTable();
                
                $('#globalSearch').keyup(function(){
                    var search = $('#globalSearch').val();

                    $.ajax({
                        url:"ajaxJQuery/searchSection_registrar.php",
                        method:"POST",
                        data:{search:search},
                        dataType:"text",
                        success:function(data)
                        {
                            $('#hehe').html(data);
                        }

                    });
                });
            } );
        </script>
    </body>
  
<!-- Mirrored from byrushan.com/projects/ma/1-7-1/jquery/light/widget-templates.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 01 May 2017 06:35:54 GMT -->
</html>