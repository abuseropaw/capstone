<?php
    session_start();
    if($_SESSION['faculty_id'] == '')
    { 
        header("Location: index.php");
    }
    else if( $_SESSION['faculty_type'] == 'Teacher')
    {
        header("Location: javascript:");
    }
    include_once 'dbconnect.php';

    $getCurrentSY = mysqli_query($con, "SELECT sy_year,sy_id from schoolyear where sy_remarks='Open'");
    $temp = mysqli_fetch_row($getCurrentSY);
    $SY = $temp[0];
    $sy_id= $temp[1];
    function fillSchoolYear($con){
        $output ='';
        $query = mysqli_query($con, "SELECT * from schoolyear order by sy_id DESC");
        if(check($con))
        {

        }
        else
        {
            $output .='<option disabled="disabled" selected>NO CURRENT SY</option>';
        }
        while($row = mysqli_fetch_array($query)){
            if($row[4] == 'Open'){
                $output .='<option value="'.$row[0].'">'.$row[1].' - CURRENT</option>';
            }else
            {
                $output .='<option value="'.$row[0].'">'.$row[1].'</option>';
            }
            
        }
        return $output;
    }
    function check($con)
    {
        $query = mysqli_num_rows(mysqli_query($con, "SELECT * from schoolyear where sy_remarks='Open'"));
        if($query > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
?>
<!DOCTYPE html>
    <!-- HEAD -->
    <?php include_once 'head.php'; ?> 
    <!-- HEAD   -->
    <body>
        <!-- HEADER -->
        <?php include_once 'header.php'; ?>
        <!-- HEADER -->

        <section id="main">
            <ol class="breadcrumb">
                <li><a href="adminHome.php">Home</a></li>
                <li class="active">Enrollment Report</li>
            </ol>
            <?php
                if($_SESSION['faculty_type'] == 'Administrator')
                { 
                    $toggle = 'enrollment';
                    include_once 'sidebar.php';
                }
                else if($_SESSION['faculty_type'] == 'Registrar' | $_SESSION['faculty_type'] == 'Head Teacher')
                {
                    $toggle = 'report';
                    include_once 'registrarSidebar.php';
                } 
            ?>
            <section id="content">
                <div class="container">
                    <!-- Colored Headers -->
                    
                    <br />
                    <div class="block-header">
                        <div class="class">
                            <h1><i class="zmdi zmdi-calendar-note"></i> Enrollment Report
                            </h1>


                            <div class="actions">
                                <div class="btn-demo row">
                                <?php if(fillSchoolYear($con) == ''){

                                }else{
                                    echo '
                                    <select class="btn btn-group btn-lg" id="sy" name="sy" >
                                    <optgroup label="School Year">';
                                        
                                        echo fillSchoolYear($con);
                                    echo '</select>
                                        ';
                                } ?>
                                </div>
                            </div>
                        </div>
                        

                    </div>
                    <div id="hehe">
                        
                        <?php include_once 'ajaxJQuery/displayEnrollment.php'; ?>
                    </div>



                    <br/>
                    <br/>
                </div>
            </section>
        </section>
        <div id="loading" class="modal fade" data-backdrop="static" data-keyboard="false">  
            <div class="modal-dialog modal-sm">  
               <div class="modal-content">
               <center>   
                    <div class="modal-body" id="Details">
                         
                        <div class="preloader pl-xxl">
                            
                                <svg class="pl-circular" viewBox="25 25 50 50">
                                    <circle class="plc-path" cx="50" cy="50" r="20"/>
                                </svg>
                            
                        </div> 
                       <center><h4>Processing...Please wait</h4></center>
                    </div>  
                </center>
               </div>  
            </div>  
        </div>
        
        <!-- FOOTER -->
        <?php include_once 'footer.php' ?>
        <!-- FOOTER -->

        <!-- Javascript Libraries -->
        <?php include_once 'scripts.php'; ?>
        <!-- Javascript Libraries -->

        <script type="text/javascript" src="js/Notification.js"></script>
        <script type="text/javascript">
            $('#sy').change(function(){
                var sys = $(this).val();
                $.ajax({
                    url:"ajaxJQuery/loadEnrollment.php",
                    method:"POST",
                    data:{sys:sys},
                    beforeSend:function(){
                        $('#loading').modal('show');
                    },
                    success:function(data){
                        $('#loading').modal('hide');
                        $('#hehe').html(data);
                    }
                });
            });
        </script>
    </body>
</html>