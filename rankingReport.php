<?php
	session_start();
	include_once 'dbconnect.php';
	require('Report/fpdf.php');


	$pdf = new FPDF('P','in',array(8.27,11.69));
	$pdf->AddPage();
	$type = mysqli_real_escape_string($con, $_GET['type']);
	class PDF extends FPDF {
		function Header(){
			//header content
		}
		function Footer(){
			date_default_timezone_set('asia/manila');
			$this->SetY(-15);
			$this->SetFont('Arial', 'B', 8);
			$this->Cell(0,10,"Printed by Reymond on ".date("F j, Y")." at ".date("h:i:sa"),0,0);
			$this->Cell(0,10,"Page ".$this->PageNo()." / {pages} ",0,0,'C');
		}
	}
	switch ($type) {
		case 'perQuarter':
			$qID = mysqli_real_escape_string($con, $_GET['qID']);
			$level = mysqli_real_escape_string($con, $_GET['level']);
			$getLevel = mysqli_fetch_row(mysqli_query($con, "SELECT year_lvl_title from yearlevel where year_lvl_id='".$level."'"));
			$getQuarter = mysqli_fetch_row(mysqli_query($con, "SELECT quarters_description,sy_id from quarters where quarters_id='".$qID."'"));
			setHeaders1($pdf,$getQuarter[0],$getLevel[0],$getQuarter[1]);
			setTableData1($con,$pdf,$qID,$getLevel[0]);
			setFooter($pdf);
			$pdf->Output();



			break;
		case 'average':
			$sy = mysqli_real_escape_string($con, $_GET['syID']);
			$level = mysqli_real_escape_string($con, $_GET['level']);

			$getLevel = mysqli_fetch_row(mysqli_query($con, "SELECT year_lvl_title from yearlevel where year_lvl_id='".$level."'"));

			setHeaders2($pdf,$sy,$getLevel[0]);
			setTableData2($con,$pdf,$getLevel[0],$sy);
			setFooter($pdf);
			$pdf->Output();
			break;
		case 'quarter':
			$qID = mysqli_real_escape_string($con, $_GET['qID']);
			$sID = mysqli_real_escape_string($con, $_GET['sID']);
			$getQuarter = mysqli_fetch_row(mysqli_query($con, "SELECT quarters_description,sy_id from quarters where quarters_id='".$qID."'"));
			$getSection = mysqli_fetch_row(mysqli_query($con, "SELECT section_name from classinformation where section_id='".$sID."'"));

			setHeaders3($pdf,$getQuarter[0],$getSection[0],$getQuarter[1]);
			setTableData3($con,$pdf,$qID,$sID);
			setFooter($pdf);
			$pdf->Output();
			break;
		default:
			# code...
			break;
	}


	 
	function setHeaders1($pdf,$qID,$level,$sy)
	{
		$pdf->SetFont("Arial","B","15");
		$pdf->Ln(.5);
		$pdf->SetLeftMargin(.2);
		$pdf->Cell(0,0.20,"Quarterly Ranking for ".$level,0,1,"C"); 
		$pdf->Ln(.0001);
		$pdf->SetFont("Arial","","12");
		$pdf->Cell(0,0.15,"Lugait National High School",0,1,"C");
		
		
		$pdf->Ln(.0001);
		$pdf->SetFont("Arial","","11");
		$pdf->Cell(0,0.15,"School Year: ".$sy,0,1,"C");
		$pdf->Ln(.0001);
		$pdf->SetFont("Arial","","11");
		$pdf->Cell(0,0.15,"Quarter: ".$qID,0,1,"C");
		
		$pdf->Ln(.0001);
		$pdf->SetFont("Arial","","11");
		$pdf->Cell(0,0.15,date('M-d-Y'),0,1,"C");


		$pdf->Image('Report/kne.png',1,.75,.9,.9);
		$pdf->Image('Report/deped.png',6,.82,1.8,.8);

		$pdf->Ln(.3);
		$pdf->Ln(.1);
		
		setTableHeader($pdf);
	}
	function setHeaders2($pdf,$sy,$level)
	{	
		$pdf->SetFont("Arial","B","15");
		$pdf->Ln(.5);
		$pdf->SetLeftMargin(.2);
		$pdf->Cell(0,0.20,"Overall Ranking for ".$level,0,1,"C"); 
		$pdf->Ln(.0001);
		$pdf->SetFont("Arial","","12");
		$pdf->Cell(0,0.15,"Lugait National High School",0,1,"C");
		
		
		
		$pdf->Ln(.0001);
		$pdf->SetFont("Arial","","11");
		$pdf->Cell(0,0.15,"School Year : ".$sy,0,1,"C");
		$pdf->Ln(.0001);
		$pdf->SetFont("Arial","","11");
		$pdf->Cell(0,0.15,date('M-d-Y'),0,1,"C");


		$pdf->Image('Report/kne.png',1,.75,.9,.9);
		$pdf->Image('Report/deped.png',6,.82,1.8,.8);

		$pdf->Ln(.3);
		$pdf->Ln(.1);
		
		setTableHeader($pdf);
	}
	function setHeaders3($pdf,$qID,$sID,$sy)
	{
		$pdf->SetFont("Arial","B","15");
		$pdf->Ln(.5);
		$pdf->SetLeftMargin(.2);
		$pdf->Cell(0,0.20,"Quarterly Ranking for ".$sID,0,1,"C"); 
		$pdf->Ln(.0001);
		$pdf->SetFont("Arial","","12");
		$pdf->Cell(0,0.15,"Lugait National High School",0,1,"C");
		
		
		$pdf->Ln(.0001);
		$pdf->SetFont("Arial","","11");
		$pdf->Cell(0,0.15,"School Year: ".$sy,0,1,"C");
		$pdf->Ln(.0001);
		$pdf->SetFont("Arial","","11");
		$pdf->Cell(0,0.15,"Quarter: ".$qID,0,1,"C");
		
		$pdf->Ln(.0001);
		$pdf->SetFont("Arial","","11");
		$pdf->Cell(0,0.15,date('M-d-Y'),0,1,"C");


		$pdf->Image('Report/kne.png',1,.75,.9,.9);
		$pdf->Image('Report/deped.png',6,.82,1.8,.8);

		$pdf->Ln(.3);
		$pdf->Ln(.1);
		
		setTableHeader($pdf);
	}
	function setTableData3($con,$pdf,$qID,$sID)
	{
		$getaverageperquarterdetails = mysqli_query($con, "SELECT * from averageperquarterdetails where quarters_id='".$qID."' and section_id='".$sID."' order by average desc limit 20");
		$i = 1;
		while ($row = mysqli_fetch_array($getaverageperquarterdetails)) {
			$controlID = explode('-', $row['control_id']);

			$pdf->Ln();
			$pdf->SetFont("Arial","","9");
		 	$pdf->Cell(.5,.20,"",0,0,"C");
		 	$pdf->Cell(.4,.20,$i,1,0,"C");
		 	$pdf->Cell(1.3,.20,$controlID[0],1,0,"L");
		 	$pdf->Cell(4,.20,characterConverter($row['Name']),1,0,"L");
		 	$pdf->Cell(1.4,.20,$row['average'],1,0,"L");
		 	$i++;
		}
	}

	function setTableData2($con,$pdf,$level,$sy)
	{
		$getaveragedetails = mysqli_query($con, "SELECT * from averagedetails where sy_id='".$sy."' and gradelevel='".$level."' order by average desc,Student ASC limit 20");
		$i = 1;
		while ($row = mysqli_fetch_array($getaveragedetails)) {
			$pdf->Ln();
			$pdf->SetFont("Arial","","9");
		 	$pdf->Cell(.5,.20,"",0,0,"C");
		 	$pdf->Cell(.4,.20,$i,1,0,"C");
		 	$pdf->Cell(1.3,.20,$row['LRN'],1,0,"L");
		 	$pdf->Cell(4,.20,characterConverter($row['Student']),1,0,"L");
		 	$pdf->Cell(1.4,.20,$row['average'],1,0,"L");
		 	$i++;
		}
	}
	function setTableData1($con,$pdf,$qID,$level)
	{
		$getaverageperquarterdetails = mysqli_query($con, "SELECT * from averageperquarterdetails where quarters_id='".$qID."' and gradelevel='".$level."' order by average desc,Name ASC limit 20");
		$i = 1;
		while ($row = mysqli_fetch_array($getaverageperquarterdetails)) {
			$controlID = explode('-', $row['control_id']);

			$pdf->Ln();
			$pdf->SetFont("Arial","","9");
		 	$pdf->Cell(.5,.20,"",0,0,"C");
		 	$pdf->Cell(.4,.20,$i,1,0,"C");
		 	$pdf->Cell(1.3,.20,$controlID[0],1,0,"L");
		 	$pdf->Cell(4,.20,characterConverter($row['Name']),1,0,"L");
		 	$pdf->Cell(1.4,.20,$row['average'],1,0,"L");
		 	$i++;
		}
	}
	function setFooter($pdf)
	{
		$pdf->Ln(1);
		$pdf->SetFont("Arial","B","12");
		 	$pdf->Cell(.7,.30,"",0,0,"C");
		 	$pdf->Cell(1.7,.30,"",0,0,"L");
		 	$pdf->Cell(3.9,.30,"PREPARED BY:",0,0,"R");
		 $pdf->Ln();

		$pdf->SetFont("Arial","U","12");
		 $pdf->Cell(13,.30,"          ".characterConverter($_SESSION['Name'])."         ",0,0,"C");
		 	
	}

	function setTableHeader($pdf)
	{
		$pdf->SetFont("Arial","B","10");
	 	$pdf->Cell(.5,.45,"",0,0,"C");
	 	$pdf->Cell(.4,.45,"NO",1,0,"C");
	 	$pdf->Cell(1.3,.45,"LRN",1,0,"C");
	 	$pdf->Cell(4,.45,"LEARNER' NAME",1,0,"C");
	 	$pdf->Cell(1.4,.45,"AVERAGE",1,0,"C");
	 	
	}

	function characterConverter($string)
	{
		$padparan = stripslashes($string);
		$econvert = iconv('UTF-8', 'windows-1252', $padparan);

		return $econvert;
	}
?>