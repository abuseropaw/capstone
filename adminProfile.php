<?php
    include_once 'sessionAdmin.php';
    include_once 'dbconnect.php';

    $teacherID =mysqli_real_escape_string($con, $_SESSION['faculty_id']);


    $query = mysqli_query($con, "SELECT * from faculty_account where faculty_id='".$teacherID."'");
    if($row = mysqli_fetch_array($query)){
        $id = $row[0];
        $fname=$row[1];
        $mname=$row[2];
        $lname=$row[3];
        $phone=$row[4];
        $address=$row[5];
        $religion=$row[6];
        $major=$row[7];
        $minor=$row[8];
        $dob=$row[9];
        $gender=$row[10];
        $type=$row[11];
        $picture=$row[12];
        $pass=$row[13];
        $dateCreated=$row[14];
        $createdBy=$row[15];
    }


    if(isset($_POST['changeprofilepic']))
    {
        $id = $teacherID;
        $pic = $_FILES['file'];

        $fileName= $_FILES['file']['name'];
        $fileTmpName= $_FILES['file']['tmp_name'];
        $fileSize= $_FILES['file']['size'];
        $fileError= $_FILES['file']['error'];
        $fileType= $_FILES['file']['type'];
        $fileExtension = explode('.', $fileName);
        $fileFirstName = $fileExtension[0];
        $fileActualExt = strtolower(end($fileExtension));
        $allowed = array('jpg','jpeg','png','bmp');


        if($fileName == ''){}

        else{
            if(in_array($fileActualExt, $allowed))
            {
              if($fileError === 0)
              {
                if($fileSize < 10000000)
                {
                  $fileNameNew = $id.".".$fileActualExt;
                  $fileDestination = 'profile/'.$fileNameNew;
                  move_uploaded_file($fileTmpName, $fileDestination);

                  if(mysqli_query($con, "UPDATE faculty_account set faculty_picture='".$fileNameNew."' where faculty_id='".$id."'"))
                  {
                    
                  }

                }
              }
            }

        }
        header("Location : adminProfile.php");
    }
    
?>
<!DOCTYPE html>
    <!-- HEAD -->
    <?php include_once 'head.php'; ?> 
    <!-- HEAD   -->
    <body>
        <!-- HEADER -->
        <?php include_once 'header.php'; ?>
        <!-- HEADER -->

        <section id="main">
            <ol class="breadcrumb">
                <li><a href="adminHome.php">Home</a></li>
                
                <li class="active">Teacher Profile</li>
            </ol>
            <?php 
                $toggle = 'adminProfile';
                include_once 'sidebar.php'; 
            ?>
            <section id="content">

                <div class="container">

                    <div class="block-header">
                        <h1><i class="zmdi zmdi-accounts"></i> My Profile
                        </h1>

                        
                    </div>
                        <div class="row">

                            <div class="col-sm-12">
                                <div class='card'  id='teacherInfo'>
                                    
                                    <div class="card-body">
                                        <div class="card" id="profile-main">
                                            <div class="pm-overview c-overflow">
                                                
                                                <div class="pmo-pic">
                                                    <div class="p-relative">

                                                        <a href="#changepic" data-toggle='modal' >

                                                            <img class="img-responsive" src="profile/<?php echo ($picture == null) ? 'default.png':$picture ?>" alt="">
                                                            
                                                        </a>
                                                        <a href="#changepic" data-toggle='modal' class="pmop-edit">
                                                           <i class="zmdi zmdi-camera"></i> <span class="hidden-xs">Update Profile Picture</span>
                                                        </a>
                                                        
                                                    </div>


                                                        
                                                </div>

                                                <div class="pmo-block pmo-contact hidden-xs">
                                                    <h2>Contact</h2>
                                                    <?php
                                                    echo "<ul>
                                                        <li><i class='zmdi zmdi-phone'></i>".$fname.' '.$mname.' '.$lname."</li>
                                                        <li>
                                                            <i class='zmdi zmdi-pin'></i>
                                                            <address class='m-b-0 ng-binding'>
                                                                ".$address."
                                                            </address>
                                                        </li>
                                                    </ul>";
                                                    ?>
                                                </div>

                                            
                                            </div>

                                            <div class="pm-body clearfix">
                                                <div role="tabpanel">
                                                    <ul class="tab-nav tn-justified" role="tablist" data-tab-color="green">
                                                        <li class="active"><a href="#acount" aria-controls="acount" role="tab"
                                                          data-toggle="tab">Account</a></li>
                                                        <li><a href="#class" aria-controls="class" role="tab" data-toggle="tab">Logs</a></li>
                                                        
                                                       
                                                    </ul>
                                                    <div class="tab-content">
                                                        <div role="tabpanel" class="tab-pane active" id="acount">
                                                            <div class="pmb-block">
                                                                <div class="pmbb-header">
                                                                    <h2><i class="zmdi zmdi-account m-r-10"></i> Basic Information</h2>

                                                                    <ul class="actions">
                                                                        <li class="dropdown">
                                                                            <a href="#" data-toggle="dropdown">
                                                                                <i class="zmdi zmdi-more-vert"></i>
                                                                            </a>

                                                                            <ul class="dropdown-menu dropdown-menu-right">
                                                                                <li>
                                                                                    <a data-ma-action="profile-edit" href="#">Edit</a>
                                                                                </li>
                                                                            </ul>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                                <div class="pmbb-body p-l-30">
                                                                    <div class="pmbb-view">
                                                                        <?php
                                                                        echo "
                                                                        <dl class='dl-horizontal'>
                                                                            <dt>First Name</dt>
                                                                            <dd>".$fname."</dd>
                                                                        </dl>
                                                                        <dl class='dl-horizontal'>
                                                                            <dt>Middle Name</dt>
                                                                            <dd>".$mname."</dd>
                                                                        </dl>
                                                                        <dl class='dl-horizontal'>
                                                                            <dt>Last Name</dt>
                                                                            <dd>".$lname."</dd>
                                                                        </dl>
                                                                        <dl class='dl-horizontal'>
                                                                            <dt>Gender</dt>
                                                                            <dd>".$gender."</dd>
                                                                        </dl>
                                                                          
                                                                        <dl class='dl-horizontal'>
                                                                            <dt>Birthday</dt>
                                                                            <dd>".$dob."</dd>
                                                                        </dl>
                                                                        <dl class='dl-horizontal'>
                                                                            <dt>Position</dt>
                                                                            <dd>".$type."</dd>
                                                                        </dl> 
                                                                        <dl class='dl-horizontal'>
                                                                            <dt>Minor Subject</dt>
                                                                            <dd>".$minor."</dd>
                                                                        </dl>
                                                                        <dl class='dl-horizontal'>
                                                                            <dt>Major Subject</dt>
                                                                            <dd>".$major."</dd>
                                                                        </dl>

                                                                        <dl class='dl-horizontal'>
                                                                            <dt>Contant No.</dt>
                                                                            <dd>".$phone."</dd>
                                                                        </dl>
                                                                        <dl class='dl-horizontal'>
                                                                            <dt>Home Address</dt>
                                                                            <dd>".$address."</dd>
                                                                        </dl>
                                                                        ";
                                                                        ?>
                                                                    </div>
                                                                    
                                                                    <div class="pmbb-edit">
                                                                        <form id="editForm" method="post">
                                                                        <dl class="dl-horizontal">
                                                                            <dt class="p-t-10">First Name</dt>
                                                                            <dd>
                                                                                <div class="fg-line">
                                                                                    <input type="text" name='fname' id='fname' class="form-control"
                                                                                           value ="<?php echo $fname ?> ">
                                                                                </div>

                                                                            </dd>
                                                                        </dl>

                                                                        <dl class="dl-horizontal">
                                                                            <dt class="p-t-10">Middle Name</dt>
                                                                            <dd>
                                                                                <div class="fg-line">
                                                                                    <input type="text" id='mname' name='mname' class="form-control"
                                                                                           value ="<?php echo $mname ?>">
                                                                                </div>

                                                                            </dd>
                                                                        </dl>

                                                                        <dl class="dl-horizontal">
                                                                            <dt class="p-t-10">Last Name</dt>
                                                                            <dd>
                                                                                <div class="fg-line">
                                                                                    <input type="text" id='lname' name='lname' class="form-control"
                                                                                           value ="<?php echo $lname ?>">
                                                                                </div>

                                                                            </dd>
                                                                        </dl>
                                                                        <dl class="dl-horizontal">
                                                                            <dt class="p-t-10">Gender</dt>
                                                                            <dd>
                                                                                <div class="fg-line">
                                                                                    <select class="form-control selectpicker" id='gender' name='gender'>
                                                                                        <option><?php echo $gender ?></option>
                                                                                        <option>Male</option>
                                                                                        <option>Female</option>
                                                                                    </select>
                                                                                </div>
                                                                            </dd>
                                                                        </dl>
                                                                        <dl class="dl-horizontal">
                                                                            <dt class="p-t-10">Birthday</dt>
                                                                            <dd>
                                                                                <div class="dtp-container dropdown fg-line">
                                                                                    <input type='text' name="dob" id="dob" class="form-control" value ="<?php echo $dob ?>">
                                                                                </div>
                                                                            </dd>
                                                                        </dl>
                                                                        <?php 
                                                                        if($type !='Administrator')
                                                                        {
                                                                            echo '
                                                                            <dl class="dl-horizontal">
                                                                                <dt class="p-t-10">Position</dt>
                                                                                <dd>
                                                                                    <div class="fg-line">
                                                                                        <select class="form-control selectpicker" name="position" id="position">

                                                                                            <option>'.$type.'</option>
                                                                                            <option>Teacher</option>
                                                                                            <option>Administrator</option>
                                                                                            <option>Head Teacher</option>
                                                                                            <option>Principal</option>
                                                                                            <option>Registrar</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </dd>
                                                                            </dl>';
                                                                        }
                                                                        
                                                                        ?>

                                                                        <dl class="dl-horizontal">
                                                                            <dt class="p-t-10">Minor Subject</dt>
                                                                            <dd>
                                                                                <div class="fg-line">
                                                                                    <select class="form-control selectpicker" name="minor" id="minor">

                                                                                        <option><?php echo $minor; ?></option>
                                                                                        <?php $query= mysqli_query($con, "SELECT * from subjects");
                                                                                            while($row = mysqli_fetch_array($query)){
                                                                                                echo "<option>".$row[1].'-'.$row[2]."</option>";
                                                                                            }
                                                                                        ?>
                                                                                    </select>
                                                                                </div>
                                                                            </dd>
                                                                        </dl>

                                                                        <dl class="dl-horizontal">
                                                                            <dt class="p-t-10">Major Subject</dt>
                                                                            <dd>
                                                                                <div class="fg-line">
                                                                                    <select class="form-control selectpicker" name="major" id="major"> 

                                                                                        <option><?php echo $major; ?></option>
                                                                                        <?php $query= mysqli_query($con, "SELECT * from subjects");
                                                                                            while($row = mysqli_fetch_array($query)){
                                                                                                echo "<option>".$row[1].'-'.$row[2]."</option>";
                                                                                            }
                                                                                        ?>
                                                                                    </select>
                                                                                </div>
                                                                            </dd>
                                                                        </dl>
                                                                        <dl class="dl-horizontal">
                                                                            <dt class="p-t-10">Contact No.</dt>
                                                                            <dd>
                                                                                <div class="fg-line">
                                                                            

                                                                                       <input type='text' name="phone" id="phone" class="form-control" value ="<?php echo $phone ?>">
                                                                                </div>
                                                                            </dd>
                                                                        </dl>
                                                                        <dl class="dl-horizontal">
                                                                            <dt class="p-t-10">Home Address</dt>
                                                                            <dd>
                                                                                <div class="fg-line">
                                                                            

                                                                                       <input type='text' name="address" id="address" class="form-control" value ="<?php echo $address ?>">
                                                                                       <input type="hidden" name="faculty_id" id="faculty_id" value="<?php echo $teacherID ?>">
                                                                                </div>
                                                                            </dd>
                                                                        </dl>
                                                                        <div class="m-t-30">
                                                                            <button type="submit" name="editBasic" id="editBasic" class="btn btn-success btn-lg">SAVE CHANGES</button>

                                                                            <button data-ma-action="profile-edit-cancel" class="btn btn-warning btn-lg">CANCEL</button>
                                                                        </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            
                                                        </div>
                                                        <div role="tabpanel" class="tab-pane" id="class">
                                                            <div class="pmb-block">
                                                                <div class="pmbb-header">
                                                                    <h2><i class="zmdi zmdi-account m-r-10"></i> Logs</h2>

                                                                    <ul class="actions">
                                                                        <li class="dropdown">
                                                                            <a href="#" data-toggle="dropdown">
                                                                                <i class="zmdi zmdi-more-vert"></i>
                                                                            </a>

                                                                            <ul class="dropdown-menu dropdown-menu-right">
                                                                                <li>
                                                                                    <a data-ma-action="profile-edit" href="#">Edit</a>
                                                                                </li>
                                                                            </ul>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                                <div class="pmbb-body p-l-30">
                                                                    
                                                                </div>
                                                            </div>
                                                            
                                                        </div>
                                                        
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    
                    </div>
                
                    </div>
                </div>
            </section>
        </section>
        <div class="modal fade" id="changepic" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" enctype='multipart/form-data'>
                        <div class="modal-header">
                            <h1 class="modal-title"><b>PROFILE PICTURE</b></h1>
                        </div>
                        <div class="modal-body">
                            <center>
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-preview thumbnail" data-trigger="fileinput"></div>
                                <div>
                                    <span class="btn btn-info btn-file btn-xs">
                                        <span class="fileinput-new">Select image</span>
                                        
                                        <input type="file" name="file" id="file">
                                    </span>
                                    <button type="submit" name="changeprofilepic" id="changeprofilepic" class="btn btn-success btn-xs">CHANGE PICTURE</button>
                                    
                                </div>
                            </div>
                            </center>
                            
                            
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- FOOTER -->
        <?php include_once 'footer.php' ?>
        <!-- FOOTER -->

        <!-- Javascript Libraries -->
        <?php include_once 'scripts.php'; ?>
        <!-- Javascript Libraries -->
        
        <script type="text/javascript">
            $(document).ready(function() {
                $('#data-table-basic').DataTable();

                //PAG SEARCH
                $('#search').keyup(function(){
                    var txt = $(this).val();
                    if(txt != '')
                    {
                        $.ajax({
                            url:"ajaxJQuery/searchTeacher.php",
                            method:"POST",
                            data:{search:txt},
                            dataType:"text",
                            success:function(data)
                            {
                                $('#resultsaSearch').html(data);
                            }
                        });
                    }
                    
                });
                


                $('#editForm').on("submit", function(event){
                    event.preventDefault();
                    
                    if($('#fname').val() == '')  
                    {
                            $('#fname').focus();    
                         alert("First name is required and valid");
                         
                    }  
                    else if($('#mname').val() == '')  
                    {  
                        $('#mname').focus();  
                         alert("Middle name is required and valid");  
                    }  
                    else if($('#lname').val() == '')  
                    {
                        $('#lname').focus();
                         alert("Maximun grade is required");  
                    }

                    else if($('#gender').val() =='')
                    {
                        $('#gender').focus();
                        alert("Home address is required and valid!");
                    }
                    else if($('#dob').val() == '')
                    {
                        $('#dob').focus();
                        alert("Barangay address is required and valid!");
                    }
                    else if($('#position').val() == '')
                    {
                        $('#position').focus();
                        alert("Municipality address is required and valid!");
                    }
                    

                    else if($('#phone').val() == '')
                    {
                        $('#phone').focus();
                        alert("You must select religion!");
                    }
                    else if($('#address').val() == '')
                    {
                        $('#address').focus();
                        alert("Date of birth is required and valid!");
                    }

                    else  
                    {  
                         $.ajax({  
                              url:"ajaxJQuery/updateTeacher.php",  
                              method:"POST",  
                              data:$('#editForm').serialize(),  
                              beforeSend:function(){  
                                   $('#insert1').val("Inserting..");   
                              },  
                              success:function(data){ 
                                   swal({

                                        title: "Good job", 
                                        text: "You successfully update the teacher",
                                        type: "success",
                                        timer: 1500
                                    });
                                   $('#editForm')[0].reset();  
                                    
                                      setTimeout(function(){// wait for 5 secs(2)
                                           location.reload(); // then reload the page.(3)
                                      }, 1750); 
                                   
                                   
                              }  

                         });  
                    }
                });





            } );
        </script>
    </body>

</html>