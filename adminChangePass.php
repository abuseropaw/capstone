<?php
    include_once 'sessionAdmin.php';
    include_once 'dbconnect.php';

    $teacherID =mysqli_real_escape_string($con, $_SESSION['faculty_id']);
    $error = false;

    $query = mysqli_query($con, "SELECT * from faculty_account where faculty_id='".$teacherID."'");
    if($row = mysqli_fetch_array($query)){
        $id = $row[0];
        $fname=$row[1];
        $mname=$row[2];
        $lname=$row[3];
        $phone=$row[4];
        $address=$row[5];
        $religion=$row[6];
        $major=$row[7];
        $minor=$row[8];
        $dob=$row[9];
        $gender=$row[5];
        $type=$row[11];
        $picture=$row[12];
        $pass=$row[13];
        $dateCreated=$row[14];
        $createdBy=$row[15];
    }
    if(isset($_POST['changepass'])){
        $current = mysqli_real_escape_string($con, $_POST['current']);
        $new = mysqli_real_escape_string($con, $_POST['new']);
        $confirm = mysqli_real_escape_string($con, $_POST['confirm']);

        if(sha1($current) != $pass) {
            $error = true;
            $error_type1 =" has-error has-feedback";
            $current_error = "Incorrect account current password!";
        }
        if($new != $confirm) {
            $error = true;
            $error_type2 =" has-error has-feedback";
            $confirm_error = "New account password and confirm password did not match!";
        }
        if(!$error){
            if(mysqli_query($con, "UPDATE faculty_account set faculty_pass='".sha1($new)."' where faculty_id='".$id."'")) {
                $successmsg = "Successfully updated! :)";
            } else {
                $errormsg = "Error in updating the pass...Please try again later!";
            }
        }
    }
    
?>
<!DOCTYPE html>
    <!-- HEAD -->
    <?php include_once 'head.php'; ?> 
    <!-- HEAD   -->
    <body>
        <!-- HEADER -->
        <?php include_once 'header.php'; ?>
        <!-- HEADER -->

        <section id="main">
            <ol class="breadcrumb">
                <li><a href="adminHome.php">Home</a></li>
                
                <li class="active">Teacher Profile</li>
            </ol>
            <?php 
                $toggle = 'adminProfile';
                include_once 'sidebar.php'; 
            ?>
            <section id="content">
                <div class="container">
                    <div class="block-header">
                        <h1><i class="zmdi zmdi-lock"></i> Change Password
                        </h1> 
                    </div>
                    <div class="row" id='sectionList'>
                        <div class="col-sm-12">
                            <div class='card'>                                  
                                <div class="card-body lcb-form">                                             
                                    <form role="form" class="form-horizontal" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" name="signupform">
                                        <div class="card-body card-padding">
                                            <div class="form-group <?php if (isset($error_type1)) echo $error_type1; ?>">
                                                <label for="current" class="col-sm-2 control-label">Current Password</label>
                                                <div class="col-sm-5">
                                                    <div class="fg-line">
                                                        <input type="password" name="current" class="form-control" id="current" required value="<?php if($error) echo $current; ?>" placeholder="Current Password">
                                                    </div>
                                                    <span class="text-danger"><?php if (isset($current_error)) echo $current_error; ?></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="new" class="col-sm-2 control-label">New Password</label>
                                                <div class="col-sm-5">
                                                    <div class="fg-line">
                                                        <input type="password" name="new" class="form-control input-sm" id="new" required value="<?php if($error) echo $new; ?>" placeholder="New Password">
                                                    </div>
                                                    <span class="text-danger"><?php if (isset($new_error)) echo $new_error; ?></span>
                                                </div>
                                            </div>
                                            <div class="form-group <?php if (isset($error_type2)) echo $error_type2; ?>">
                                                <label for="confirm" class="col-sm-2 control-label">Re-type New Password</label>
                                                <div class="col-sm-5">
                                                    <div class="fg-line">
                                                        <input type="password" name="confirm" class="form-control input-sm" id="confirm" required value="<?php if($error) echo $confirm; ?>" placeholder="Re-type New Password">
                                                    </div>
                                                    <span class="text-danger"><?php if (isset($confirm_error)) echo $confirm_error; ?></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-offset-2 col-sm-5">
                                                    <button type="submit" name="changepass" id="changepass" class="btn btn-success waves-effect btn-lg">Save Password</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <div class="card-body card-padding">

                                        <span class="text-success"><?php if (isset($successmsg)) { echo $successmsg; } ?></span>
                                        <span class="text-danger"><?php if (isset($errormsg)) { echo $errormsg; } ?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </section>

        <!-- FOOTER -->
        <?php include_once 'footer.php' ?>
        <!-- FOOTER -->

        <!-- Javascript Libraries -->
        <?php include_once 'scripts.php'; ?>
        <!-- Javascript Libraries -->
        
        <script type="text/javascript">
            $(document).ready(function() {
                $('#data-table-basic').DataTable();

                //PAG SEARCH
                $('#search').keyup(function(){
                    var txt = $(this).val();
                    if(txt != '')
                    {
                        $.ajax({
                            url:"ajaxJQuery/searchTeacher.php",
                            method:"POST",
                            data:{search:txt},
                            dataType:"text",
                            success:function(data)
                            {
                                $('#resultsaSearch').html(data);
                            }
                        });
                    }
                    
                });



                

            } );
        </script>
    </body>

</html>