<?php
    session_start();
    include_once 'dbconnect.php';
    include_once 'PHPExcel-1.8.1/Classes/PHPExcel.php';
    include 'ajaxJQuery/globalFunction.php';
    $id = mysqli_real_escape_string($con,$_GET['id']);
    $name1 = mysqli_real_escape_string($con,$_GET['name']);
    $level = mysqli_real_escape_string($con,$_GET['levels']);
	$_SESSION['access'] = 'NOT';    

    $items = mysqli_fetch_row(mysqli_query($con, "SELECT * from sectionoverall where section_id='".$id."'"));
    
    
?>
<!DOCTYPE html>

    <!-- HEAD -->
    <?php include_once 'head.php'; ?>
    <!-- HEAD   -->
    <body >
        <!-- HEADER -->
        <?php include_once 'header.php'; ?>
        <!-- HEADER -->

        <section id="main">
            
           
            
           

                <div class="container">

                    <div class="block-header">
                        <h1><i class="zmdi zmdi-account-box"></i> <?php echo $name1; ?></h1> 
                    </div>

                    <!-- Add button -->
                    
                    <div class="card">
                        

                        <div class="card-body card-padding">
                            <div role="tabpanel">
                                <ul class="tab-nav" role="tablist" data-tab-color="green">
                                    <li class="active"><a href="#old" aria-controls="old" role="tab" data-toggle="tab">Old Learner</a></li>
                                    
                                    <li><a href="#new" aria-controls="new" role="tab"
                                                          data-toggle="tab">Transferee Learner</a></li>
                                    

                                </ul>

                                <!-- GRADE --><!-- GRADE --><!-- GRADE --><!-- GRADE --><!-- GRADE --><!-- GRADE -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="old">
                                        <div class="card">
                                            <div class="action-header clearfix" data-ma-action="action-header-open">
                                                <div class="ah-label hidden-xs"><b>Click here to search (LRN or Full Name) -- Please hit Enter to to start search</b></div>

                                                <div class="ah-search">
                                                    <input type="text" name='search' id="search" placeholder="Start typing..." class="ahs-input">
                                                    <input type='hidden' name='secID' id='secID' value='<?php echo $id; ?>'>
                                                    <i class="ahs-close" data-ma-action="action-header-close">&times;</i>
                                                </div>
                        
                                                <ul class="actions">
                                                    <li>
                                                        <a href="#" data-ma-action="action-header-open">
                                                            <i class="zmdi zmdi-search"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>

                                            <div class="card-body card-padding">

                                                <div class="contacts clearfix row" id="resultsaSearch">

                                                    
                                     
                                                </div>
                                            </div>
                                        </div>
                                    </div>






                                    <div role="tabpanel" class="tab-pane" id="new">

                                        <form method="POST" id="insert_form">
					                        <div class="card-header">
					                           <h4><b>STUDENT PERSONAL INFORMATION</b></h4>
					                        </div>
					                        
					                        <div class="card-body">
					                            <div class='col-sm-3' id='lrnForm'>
								                    <div class='form-group fg-line'>
								                        <input type='text' name='student_LRN' id='student_LRN' class='form-control' placeholder='LRN' autofocus required>
								                    </div>
								                    <small class="help-block hidden" id='helpMessage'>Learner's LRN already exist</small>
								                </div>
					                            <div class='col-sm-3'>
					                                <div class='form-group fg-line'>
					                                    <input type='text' name='student_fname' id='student_fname' class='form-control' placeholder='First name' autofocus required>
					                                </div>
					                            </div>
					                            <div class='col-sm-3'>
					                                <div class='form-group fg-line'>
					                                    <input type='text' name='student_mname' id='student_mname' class='form-control' placeholder='Middle name'>

					                                </div>
					                            </div>
					                            <div class='col-sm-3'>
					                                <div class='form-group fg-line'>
					                                    <input type='text' name='student_lname' id='student_lname' class='form-control' placeholder='Last name' required>

					                                </div>
					                            </div>

					                            <div class="col-sm-2">
					                                <div class="form-group fg-line">
					                                    <select class="selectpicker form-control" data-live-search="true" name='student_gender' id='student_gender' required>
					                                        <option value="">Gender</option>
					                                        <option>Male</option>
					                                        <option>Female</option>
					                                    </select>
					                                </div>
					                            </div>

					                            <div class="col-sm-2">
					                                <div class="form-group fg-line">
					                                    <input type='text' class="form-control date-picker"
					                               placeholder="Date of Birth" name="student_birthDate" id="student_birthDate" required>
					                                </div>
					                            </div>

					                            <div class='col-sm-1'>
					                                <div class='form-group fg-line'>
					                                    <input type='number' name='student_age' id='student_age' class='form-control' placeholder='Age' required>
					                                </div>
					                            </div>
					                            <div class="col-sm-2">
					                                <div class="form-group fg-line">
					                                    <select class="selectpicker form-control" data-live-search="true" name='student_motherTongue' id='student_motherTongue' required>
					                                        <option value="">Mother Tongue</option>
					                                        <option>Cebuano</option>
					                                        <option>Filipino</option>
					                                        <option>Sinugbuanong Binisaya</option>
					                                    </select>
					                                </div>
					                            </div>
					                            <div class="col-sm-2">
					                                <div class="form-group fg-line">
					                                    <select class="selectpicker form-control" data-live-search="true" name='student_IP' id='student_IP' required>
					                                        <option value="">Indigenous People</option>
					                                        <option>Christian</option>
					                                        <option>Lumad</option>
					                                        <option>Muslim</option>
					                                    </select>
					                                </div>
					                            </div>
					                            <div class="col-sm-3">
					                                <div class="form-group fg-line">
					                                    <select class="selectpicker form-control" data-live-search="true" name='student_religion' id='student_religion' required>
					                                        <option value="">Religion</option>
					                                        <option>Christianity</option>
					                                        <option>Muslim</option>
					                                        
					                                    </select>
					                                </div>
					                            </div>
					                            <div class="col-sm-3">
					                                <div class="form-group fg-line">
					                                    <input type="text" class="form-control" name="student_addressHSSP" id="student_addressHSSP" placeholder="House #/ Street/ Purok" required >
					                                </div>
					                            </div>

					                            <div class="col-sm-3">
					                                <div class="form-group fg-line">
					                                    <input type="text" class="form-control" name="student_addressBarangay" id="student_addressBarangay" placeholder="Barangay" required>
					                                </div>
					                            </div>
					                            <div class="col-sm-3">
					                                <div class="form-group fg-line">
					                                    <select class="selectpicker form-control" data-live-search="true" name='student_addressMunicipality' id='student_addressMunicipality' required>
					                                        <option value="">Municipality</option>
					                                        <option>Lugait</option>
					                                        <option>Malaybalay</option>
					                                        <option>Valencia</option>
					                                        <option>Baungon</option>
					                                        <option>Cabanglasan</option>
					                                        <option>Damulog</option>
					                                        <option>Dangcagan</option>
					                                        <option>Don Carlos</option>
					                                        <option>Impasug-ong</option>
					                                        <option>Kadingilan</option>
					                                        <option>Kibawe</option>
					                                        <option>Kitaotao</option>
					                                        <option>Lantapan</option>
					                                        <option>Malitbog</option>
					                                        <option>Manolo Fortich</option>
					                                        <option>Maramag</option>
					                                        <option>Pangantucan</option>
					                                        <option>Quezon</option>
					                                        <option>San Fernando</option>
					                                        <option>Sumilao</option>
					                                        <option>Talakag</option>
					                                        <option>Catarman</option>
					                                        <option>Guinsiliban</option>
					                                        <option>Mahinog</option>
					                                        <option>Mambajao</option>
					                                        <option>Sagay</option>
					                                        <option>Bacolod</option>
					                                        <option>Baloi</option>
					                                        <option>Baroy</option>
					                                        <option>Kapatagan</option>
					                                        <option>Kauswagan</option>
					                                        <option>Kolambugan</option>
					                                        <option>Lala</option>
					                                        <option>Linamon</option>
					                                        <option>Magsaysay</option>
					                                        <option>Maigo</option>
					                                        <option>Matungao</option>
					                                        <option>Munai</option>
					                                        <option>Nunungan</option>
					                                        <option>Pantao Ragat</option>
					                                        <option>Pantar</option>
					                                        <option>Poona Piagapo</option>
					                                        <option>Salvador</option>
					                                        <option>Sapad</option>
					                                        <option>Tagoloan</option>
					                                        <option>Tangkal</option>
					                                        <option>Tubod</option>
					                                        <option>Iligan</option>
					                                        <option>Oroquieta</option>
					                                        <option>Ozamiz</option>
					                                        <option>Tangub</option>
					                                        <option>Aloran</option>
					                                        <option>Baliangao</option>
					                                        <option>Bonifacio</option>
					                                        <option>Calamba</option>
					                                        <option>Clarin</option>
					                                        <option>Concepcion</option>
					                                        <option>Don Victoriano</option>
					                                        <option>Jimenez</option>
					                                        <option>Lopez Jaena</option>
					                                        <option>Panaon</option>
					                                        <option>Plaridel</option>
					                                        <option>Sapang Dalaga</option>
					                                        <option>Sinacaban</option>
					                                        <option>Tudela</option>
					                                        <option>El Salvador</option>
					                                        <option>Gingoog</option>
					                                        <option>Alubijid</option>
					                                        <option>Balingasag</option>
					                                        <option>Baligoan</option>
					                                        <option>Claveria</option>
					                                        <option>Gitagum</option>
					                                        <option>Initao</option>
					                                        <option>Jasaan</option>
					                                        <option>Kinoguitan</option>
					                                        <option>Lagonglong</option>
					                                        <option>Laguindingan</option>
					                                        
					                                        <option>Libertad</option>
					                                        <option>Magsaysay(Linugos)</option>
					                                        <option>Manticao</option>
					                                        <option>Medina</option>
					                                        <option>Naawan</option>
					                                        <option>Opol</option>
					                                        <option>Salay</option>
					                                        <option>Sugbongcogon</option>
					                                        <option>Tagoloan</option>
					                                        <option>Talisayan</option>
					                                        <option>Villanueva</option>
					                                        <option>Cagayan de Oro</option>

					                                    </select>
					                                </div>
					                            </div>
					                            <div class="col-sm-3">
					                                <div class="form-group fg-line">
					                                    <select class="selectpicker form-control" data-live-search="true" name='student_addressProvince' id='student_addressProvince' required>
					                                        <option value="">Province</option>
					                                        <option>Misamis Oriental</option>
					                                        <option>Bukidnon</option>
					                                        <option>Camiguin</option>
					                                        <option>Lanao Del Norte</option>
					                                       
					                                        <option>Misamis Occidental</option>

					                                    </select>
					                                </div>
					                            </div>
					                            
					                            <div class="col-sm-4">
					                                <div class="form-group fg-line">
					                                    <input type="text" name="parent_Ffname" id="parent_Ffname" class="form-control" placeholder="Father's first name">
					                                </div>
					                            </div>
					                            <div class="col-sm-4">
					                                <div class="form-group fg-line">
					                                    <input type="text" name="parent_Fmname" id="parent_Fmname" class="form-control" placeholder="Father's middle name">
					                                </div>
					                            </div>
					                            <div class="col-sm-4">
					                                <div class="form-group fg-line">
					                                    <input type="text" name="parent_Flname" id="parent_Flname" class="form-control" placeholder="Father's last name">
					                                </div>
					                            </div>
					                            <div class="col-sm-4">
					                                <div class="form-group fg-line">
					                                    <input type="text" name="parent_Mfname" id="parent_Mfname" class="form-control" placeholder="Mother's first name">
					                                </div>
					                            </div>
					                            <div class="col-sm-4">
					                                <div class="form-group fg-line">
					                                    <input type="text" name="parent_Mmname" id="parent_Mmname" class="form-control" placeholder="Mother's middle name">
					                                </div>
					                            </div>
					                            <div class="col-sm-4">
					                                <div class="form-group fg-line">
					                                    <input type="text" name="parent_Mlname" id="parent_Mlname" class="form-control" placeholder="Mother's last name">
					                                </div>
					                            </div>

					                            <div class="col-sm-3">
					                                <div class="form-group fg-line">
					                                    <input type="text" name="parent_phone" id="parent_phone" class="form-control" placeholder="Parents contact #">
					                                </div>
					                            </div>

					                            <div class="col-sm-3">
					                                <div class="form-group fg-line">
					                                    <input type="text" name="guardian_name" id="guardian_name" class="form-control" placeholder="Name of guardian">
					                                </div>
					                            </div>
					                            <div class="col-sm-3">
					                                <div class="form-group fg-line">
					                                    <select class="selectpicker" data-search="true" name='guardian_relationship' id='guardian_relationship'>
					                                        <option value="">RELATIONSHIP</option>
					                                        <option>Aunt</option>
					                                        <option>Uncle</option>
					                                    </select>
					                                </div>
					                            </div>
					                            <div class="col-sm-3">
					                                <div class="form-group fg-line">
					                                    <input type="text" name="guardian_Phone" id="guardian_Phone" class="form-control" placeholder="Guardian cel #">
					                                    <input type="hidden" name="section_id" id="section_id" value="<?php 
					                                        echo $id; 
					                                    ?>">
					                                </div>
					                            </div>

					                        </div>
					                        <div class="modal-footer">
					                            
					                                <button type="submit" name="insert" id="insert" class="btn btn-success">CREATE STUDENT ACCOUNT</button>
					                           
					                        </div>
					                    </div>
					                                       
					                                </div>           
					                            </div>
					                        </div>



					                    </form>

					               </div>




                                    
                            </div>
                        </div>
                    </div>
                        
                    </div>
                    
                </div>
                
        </section>
        <div class="modal fade" id="modalEnrollLearner" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <form id="enroll" method="post">
                        <div class="modal-header">
                            <h4 class="modal-title">Enroll LEARNER</h4>
                        </div>


                        <div class="modal-body">
                            <div class='col-sm-12'>
                                <div class='form-group fg-line'>
                                    <input type="text" name="NameofStudent" id="NameofStudent" class="form-control" disabled>
                                    <input type="hidden" name="sectionIDS" id="sectionIDS">
                                    <input type="hidden" name="studentID" id="studentID">
                                    
                                </div>
                            </div>
                            
                        </div>
                        <div class="modal-footer">
                            
                            <button type="submit" name="remove" id="remove" class="btn btn-success btn-lg">YES</button>
                            
                            <button type="button" class="btn btn-danger btn-lg" data-dismiss="modal">NO
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- FOOTER -->
        <?php include_once 'footer.php' ?>
        <!-- FOOTER -->

        <!-- Javascript Libraries -->
        <?php include_once 'scripts.php'; ?>
        <!-- Javascript Libraries -->
        <script type="text/javascript" src="js/Notification.js"></script>


        <script type="text/javascript">
            $(document).ready(function() {
               

            	//SEARCH STUDENT
                $('#search').keyup(function(objEvent){
                    var txt = $('#search').val();
                    var id = $('#secID').val();
                    (objEvent) ? keycode = objEvent.keyCode : keycode = event.keyCode;
                    console.log(keycode);
                    if(keycode == 13){
                        
                        $.ajax({
                            url:"ajaxJQuery/searchStudent2.php",
                            method:"POST",
                            dataType:"text",
                            data:
                            {
                                search:txt,
                                section:id
                            },
                            
                            success:function(data)
                            {
                            	if(!data)
                            	{
                            		$('#resultsaSearch').html(
                                    ''
                                        +'Your search - <b>'+txt+'</b> - did not match any learner.'
                                        +'<br />'
                                        +'Suggestions:<br />'
                                        +'<ul style="margin-left:1.3em;margin-bottom:2em"><li>Make sure that all words are spelled correctly.</li><li>Try different keywords.</li><li>Include comma before whitespace. <b>ex. (Last name, First name, Middle name)</b></li></ul>'
                                    +''
                                );
                            	}
                            	else
                            	{
                            		$('#resultsaSearch').html(data);
                            	}
                                
                            }
                        });
                        
                    }
                });

                $('#student_LRN').keyup(function(){
                    var lrn = $('#student_LRN').val();
                    
                        if(lrn != ''){
                            $.ajax({
                                url:"ajaxJQuery/checkLRN.php",
                                method:"POST",
                                data:{search:lrn},
                                dataType:"text",
                                success:function(data)
                                {
                                    if(data == 'naa')
                                    {

                                        $('#insert').hide();
                                        $('#lrnForm').attr('class','col-sm-3 has-warning');                                        
                                        $('#helpMessage').attr('class','help-block');
                                        $('#student_LRN').focus();
                                    }
                                    else
                                    {
                                        $('#helpMessage').attr('class','help-block hidden');
                                        $('#insert').show();
                                       $('#lrnForm').attr('class','col-sm-3');
                                    }
                                }
                            });
                        }
                    
                });
                
                

                $(document).on('click', '.enroll_learnertoSection', function(){
                    var lrn = $(this).attr("id");
                    var sectionID = $(this).attr("value");

                    $.ajax({
                        url:"ajaxJQuery/selectStudentInfo.php",
                        method:"POST",
                        data:{lrn:lrn,sectionID:sectionID},
                        dataType:"json",
                        success:function(data){
                            $('#NameofStudent').val(data.Name);
                            $('#studentID').val(data.student_id);
                            $('#sectionIDS').val(sectionID);
                            $('#modalEnrollLearner').modal('show');
                        }

                    });
                });

                $('#enroll').on("submit", function(event){
                    event.preventDefault();
                    var txt = $('#search').val();
                    var id = $('#secID').val();
                        $.ajax({  
                              url:"ajaxJQuery/enrollLearner.php",  
                              method:"POST",  
                              data:$('#enroll').serialize(),  
                              success:function(data){
                                   if(data == '1')
                                   { 
                                      
                                       $('#enroll')[0].reset();
                                       $('#modalRemoveLearner').modal('hide');
                                       
                                       $.ajax({
						                    url:"ajaxJQuery/searchStudent2.php",
						                    method:"POST",
						                    dataType:"text",
						                    data:
						                    {
						                        search:txt,
						                        section:id
						                    },
						                    
						                    success:function(data)
						                    {
						                    	if(!data)
						                    	{
						                    		$('#resultsaSearch').html(
						                            ''
						                                +'Your search - <b>'+txt+'</b> - did not match any learner.'
						                                +'<br />'
						                                +'Suggestions:<br />'
						                                +'<ul style="margin-left:1.3em;margin-bottom:2em"><li>Make sure that all words are spelled correctly.</li><li>Try different keywords.</li><li>Include comma before whitespace. <b>ex. (Last name, First name, Middle name)</b></li></ul>'
						                            +''
						                        );
						                    	}
						                    	else
						                    	{
						                    		$('#resultsaSearch').html(data);
						                    	}
						                        
						                    }
						                });
                                   }
                                   else (data == '0')
                                   {    
                                        
                                        var message = "The learner has been used for other table";
                                        executeNotif(message,"error");  
                                       $('#enroll')[0].reset();
                                       $('#modalRemoveLearner').modal('hide');
                                   }

                                 
                              }  

                         });  
                    
                });
               
                //INSERT LEARNER
                $('#insert_form').on("submit", function(event){
                    event.preventDefault();
                    $('#student_LRN').focus();
                    if($('#student_LRN').val() == '')
                    {
                        $('#student_LRN').focus();    
                         alert("LRN is required and valid");
                    }
                    else if($('#student_fname').val() == '')  
                    {
                            $('#student_fname').focus();    
                         alert("First name is required and valid");
                         
                    }  
                    else if($('#student_mname').val() == '')  
                    {  
                        $('#student_mname').focus();  
                         alert("Middle name is required and valid");  
                    }  
                    else if($('#student_lname').val() == '')  
                    {
                        $('#student_lname').focus();
                         alert("Maximun grade is required");  
                    }
                    else if($('#student_sex').val() == 'GENDER')
                    {
                        $('#student_sex').focus();
                        alert("You must select gender!");
                    }
                    else if($('#student_birthDate').val() == '')
                    {
                        $('#student_birthDate').focus();
                        alert("You must enter birth date!");
                    }
                    else if($('#student_age').val() == '')
                    {
                        $('#student_age').focus();
                        alert("You must enter age!");
                    }
                    else if($('#student_motherTongue').val() == '')
                    {
                        $('#student_motherTongue').focus();
                        alert("You must select language!");
                    }
                    else if($('#student_IP').val() == '')
                    {
                        $('#student_IP').focus();
                        alert("You must select ethnic type!");
                    }
                    else if($('#student_religion').val() == 'RELIGION')
                    {
                        $('#student_religion').focus();
                        alert("You must select religion!");
                    }
                    
                    else if($('#student_addressBarangay').val() == '')
                    {
                        $('#student_addressBarangay').focus();
                        alert("You must enter Barangay");
                    }
                    else if($('#student_addressMunicipality').val() == 'MUNICIPALITY')
                    {
                        $('#student_addressMunicipality').focus();
                        alert("You must enter municipality");
                    }
                    else if($('#student_addressProvince').val() == 'PROVINCE')
                    {
                        $('#student_addressProvince').focus();
                        alert("You must enter province");
                    }
                    else if($('#parent_Mfname').val() == '')
                    {
                        $('#parent_Mfname').focus();
                        alert("You must enter mother's first name");
                    }
                    else if($('#parent_Mmname').val() == '')
                    {
                        $('#parent_Mmname').focus();
                        alert("You must enter mother's middle name");
                    }
                    else if($('#parent_Mlname').val() == '')
                    {
                        $('#parent_Mlname').focus();
                        alert("You must enter mother's last name");
                    }
                    else  
                    {  
                         $.ajax({  
                              url:"ajaxJQuery/insertStudent.php",  
                              method:"POST",  
                              data:$('#insert_form').serialize(),  
                              beforeSend:function(){  
                                   $('#insert1').val("Inserting..");   
                              },  
                              success:function(data){ 
                                   
                                   var message = "Successfully enroll transferee learner";
                                   executeNotif(message,"success");
                                   $('#insert_form')[0].reset();   
                                   $('#listofStudents').html(data);
                                    $('#modalColor').modal('hide');
                              }  

                         });  
                    }
                });
                
            } );
        </script>
        
    </body>
  
<!-- Mirrored from byrushan.com/projects/ma/1-7-1/jquery/light/widget-templates.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 01 May 2017 06:35:54 GMT -->
</html>