<?php
session_start();

if(isset($_SESSION['faculty_id'])) {
	session_destroy();
	unset($_SESSION['faculty_id']);
	unset($_SESSION['faculty_type']);
	unset($_SESSION['Name']);


	header("Location: teacherLogin.php");
} else {
	header("Location: teacherLogin.php");
}
?>