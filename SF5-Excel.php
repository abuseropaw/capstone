<?php
    session_start();
    include_once 'dbconnect.php';
    require_once "PHPExcel-1.8.1/Classes/PHPExcel.php";
    include 'ajaxJQuery/globalFunction.php';
    $id = mysqli_real_escape_string($con, $_GET['id']);

    


    //VALIDATE ACCOUNT
    $getAdviserID = mysqli_fetch_row(mysqli_query($con, "SELECT faculty_id from adviser where section_id='".$id."'"));
    $AdviserID = $getAdviserID[0];
    

    $getLearners = mysqli_query($con, "SELECT * from controlDetails where section_id='".$id."' order by Gender DESC,Student ASC");

    $getAdviser = mysqli_fetch_row(mysqli_query($con, "SELECT Adviser,GradeLevel from classinformation where section_id='".$id."'"));
    $adviser = $getAdviser[0];
    $level = $getAdviser[1];

    $getSY = mysqli_fetch_row(mysqli_query($con, "SELECT sy_id,section_name from classinformation where section_id='".$id."'"));
    $SY = $getSY[0];
    $sectionName = $getSY[1];

    $countMale = mysqli_num_rows(mysqli_query($con, "SELECT * from controlDetails where section_id='".$id."' and Gender='Male'"));
    $countFemale = mysqli_num_rows(mysqli_query($con, "SELECT * from controlDetails where section_id='".$id."' and Gender='Female'"));
    $total = mysqli_num_rows(mysqli_query($con, "SELECT * from controlDetails where section_id='".$id."' and NOT control_remarks='DRP' OR NOT control_remarks='T/O'"));

    $countTotalAverage = mysqli_num_rows(mysqli_query($con, "SELECT * from averageDetails where section_id='".$id."'"));
   
    function setHeaders($sheet,$SY,$level,$sectionName)
    {
        $sheet ->setCellValue('C3', 'X');
        $sheet ->setCellValue('C5', '304082');
        $sheet ->setCellValue('C7', 'Lugait NHS');
        $sheet ->setCellValue('E3', 'Misamis Oriental');
        $sheet ->setCellValue('J3', 'Lugait');
        $sheet ->setCellValue('G5', $SY);
        $sheet ->setCellValue('J7', $level);
        $sheet ->setCellValue('L7', $sectionName);


    }
    
    function setDatas($con,$sheet,$getLearners,$start1,$start2,$countMale,$countFemale,$total,$id,$SY)
    {
        $maleRetained = 0;
        $femaleRetained = 0;
        $femalePromoted = 0;
        $femaleIrregular = 0;
        $malePromoted = 0;
        $maleIrregular = 0;




        while($row = mysqli_fetch_array($getLearners))
        {
            $controlId = $row[0];
            $getAverageDetails = mysqli_query($con, "SELECT * from averageDetails where control_id='".$row[0]."'");
            if($row[6] == 'Male')
            {
                $sheet ->getStyle('A'.$start1.'')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
                $sheet ->setCellValue('A'.$start1.'', $row['LRN']);
                $sheet ->setCellValue('B'.$start1.'', $row['Student']);
                if($row1 = mysqli_fetch_array($getAverageDetails))
                {
                    
                    $sheet ->setCellValue('F'.$start1.'', $row1[2]);

                    if(($row[1] == 'DRP'))
                    {
                        $sheet ->setCellValue('G'.$start1.'', 'RETAINED');
                        $maleRetained++;
                    }
                    else{
                        //$ans = checkLearnersProficiency($con,$id,$controlId);
                        if(checkLearnersProficiency($con,$id,$controlId) == "irregular")
                        {
                            
                                $sheet ->setCellValue('G'.$start1.'', 'IRREGULAR');
                                $maleIrregular++;
                            
                        }
                        else if(checkLearnersProficiency($con,$id,$controlId) == "promote")
                        {
                            if(($row1[2] < 75))
                            {
                                $sheet ->setCellValue('G'.$start1.'', 'RETAINED');
                                $maleRetained++;
                            }
                            else
                            {
                                $sheet ->setCellValue('G'.$start1.'', 'PROMOTED');
                                $malePromoted++;
                            }
                        }
                        else if(checkLearnersProficiency($con,$id,$controlId) == "retain")
                        {
                            $sheet ->setCellValue('G'.$start1.'', 'RETAINED');
                            $maleRetained++;
                        }
                    }
                    
                }
                else if($row[1] == 'DRP')
                {
                    $sheet ->setCellValue('F'.$start1.'', 'DRP');
                    $sheet ->setCellValue('G'.$start1.'', 'RETAINED');
                    $maleRetained++;
                }
                $start1++;
            
                
            }
            else
            {
                $sheet ->getStyle('A'.$start2.'')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
                $sheet ->setCellValue('A'.$start2.'', $row['LRN']);
                $sheet ->setCellValue('B'.$start2.'', $row['Student']);
                if($row1 = mysqli_fetch_array($getAverageDetails))
                {
                    
                    $sheet ->setCellValue('F'.$start2.'', $row1[2]);

                    if(($row[1] == 'DRP'))
                    {
                        $sheet ->setCellValue('G'.$start2.'', 'RETAINED');
                        $femaleRetained++;
                    }
                    else{
                        //$ans = checkLearnersProficiency($con,$id,$controlId);
                        if(checkLearnersProficiency($con,$id,$controlId) == "irregular")
                        {
                            
                                $sheet ->setCellValue('G'.$start2.'', 'IRREGULAR');
                                $femaleIrregular++;
                            
                        }
                        else if(checkLearnersProficiency($con,$id,$controlId) == "promote")
                        {
                            if(($row1[2] < 75))
                            {
                                $sheet ->setCellValue('G'.$start2.'', 'RETAINED');
                                $femaleRetained++;
                            }
                            else
                            {
                                $sheet ->setCellValue('G'.$start2.'', 'PROMOTED');
                                $femalePromoted++;
                            }
                        }
                        else if(checkLearnersProficiency($con,$id,$controlId) == "retain")
                        {
                            $sheet ->setCellValue('G'.$start2.'', 'RETAINED');
                            $femaleRetained++;
                        }
                    }
                     
                }
                else if($row[1] == 'DRP')
                {
                    $sheet ->setCellValue('F'.$start1.'', 'DRP');
                    $sheet ->setCellValue('G'.$start1.'', 'RETAINED');
                    $femaleRetained++;
                }
                $start2++;
                
               
            }
        }
       
        // $_SESSION['countMale'] = $maleRetained + $malePromoted + $maleIrregular;
        // $_SESSION['countFemale']=  $femaleRetained + $femalePromoted + $femaleIrregular;
        // $_SESSION['total'] =$_SESSION['countFemale'] + $_SESSION['countMale'];
        setSummaryTable($sheet,$maleRetained,$malePromoted,$maleIrregular,$femaleRetained,$femalePromoted,$femaleIrregular);
        
    }
    function checkLearnersProficiency($con,$id,$controlId)
    {
        
        $countSubjectOffer = mysqli_num_rows(mysqli_query($con, "SELECT * from subjectoffering where section_id='".$id."'"));
        $countAverage = mysqli_num_rows(mysqli_query($con, "SELECT * from averagepersubjectoffering where control_id='".$controlId."'"));
        $query = mysqli_query($con, "SELECT * from averagepersubjectoffering where control_id='".$controlId."'");
        $count = 0;
        if($countAverage == $countSubjectOffer){
            while($row = mysqli_fetch_array($query)){
                if($row[0] < 75)
                {
                    $count++;
                }
            }
            if($count > 2)
            {
                return "retain";
            }
            else if($count <= 2 && $count !=0)
            {
                if($countSubjectOffer <= $count)
                {
                    return "retain";
                }
                else
                {
                    return "irregular";
                }
            }
            else if($count == 0)
            {
                return "promote";
            }
           
        }else{
            return "retain";
        }
        

    }
    function setTotals($sheet,$index1, $index2,$index3, $countMale, $countFemale,$total){
        $sheet ->setCellValue('A'.$index1.'', $countMale);
        $sheet ->setCellValue('A'.$index2.'', $countFemale);
        $sheet ->setCellValue('A'.$index3.'', $total);

        $sheet ->setCellValue('B'.$index1.'', '      <=====      TOTAL MALE');
        $sheet ->setCellValue('C'.$index2.'', '      <=====      TOTAL FEMALE');
        $sheet ->setCellValue('C'.$index3.'', '      <=====      COMBINED');

    }

    function setSummaryTable($sheet,$maleRetained,$malePromoted,$maleIrregular,$femaleRetained,$femalePromoted,$femaleIrregular)
    {
            $i = 15;
            $sheet ->setCellValue('N'.$i.'', $malePromoted);
            $sheet ->setCellValue('O'.$i.'', $femalePromoted);
            $tots1 = $malePromoted + $femalePromoted;
            $sheet ->setCellValue('P'.$i.'', $tots1);

            $i = $i +2;
            $sheet ->setCellValue('N'.$i.'', $maleIrregular);
            $sheet ->setCellValue('O'.$i.'', $femaleIrregular);
            $tots1 = $maleIrregular + $femaleIrregular;
            $sheet ->setCellValue('P'.$i.'', $tots1);
             $i = $i +2;
            $sheet ->setCellValue('N'.$i.'', $maleRetained);
            $sheet ->setCellValue('O'.$i.'', $femaleRetained);
            $tots1 = $maleRetained + $femaleRetained;
            $sheet ->setCellValue('P'.$i.'', $tots1);
    }
    function setLevelofProficiency($con,$start,$id,$sheet)
    {
        $query = mysqli_query($con, "SELECT * from sf5remarks where section_id='".$id."'");
        if($row = mysqli_fetch_array($query)){
           $i = $start;
            $sheet ->setCellValue('N'.$i.'', $row[3]);
            $sheet ->setCellValue('O'.$i.'', $row[4]);
            $tots1 = $row[3] + $row[4];
            $sheet ->setCellValue('P'.$i.'', $tots1);

            $i = $i +2;
            $sheet ->setCellValue('N'.$i.'', $row[5]);
            $sheet ->setCellValue('O'.$i.'', $row[6]);
            $tots1 = $row[5] + $row[6];
            $sheet ->setCellValue('P'.$i.'', $tots1);
             $i = $i +2;
            $sheet ->setCellValue('N'.$i.'', $row[7]);
            $sheet ->setCellValue('O'.$i.'', $row[8]);
            $tots1 = $row[7] + $row[8];
            $sheet ->setCellValue('P'.$i.'', $tots1);
             $i = $i +2;
            $sheet ->setCellValue('N'.$i.'', $row[9]);
            $sheet ->setCellValue('O'.$i.'', $row[10]);
            $tots1 = $row[9] + $row[10];
            $sheet ->setCellValue('P'.$i.'', $tots1);
             $i = $i +2;
            $sheet ->setCellValue('N'.$i.'', $row[11]);
            $sheet ->setCellValue('O'.$i.'', $row[12]);
            $tots1 = $row[11] + $row[12];
            $sheet ->setCellValue('P'.$i.'', $tots1);
            
        }
    }
    //SF 5 20 Male
   
        if($countMale <= 20)
        {
            $start1 = 13; $start2 = 34;
            if($countFemale <=26) // 26 Female
            {
                $phpExcel = PHPExcel_IOFactory::load('School Forms Final/School Form 5/School Form 5 20M/School Form 5 20-26.xlsx');
                $sheet = $phpExcel ->getActiveSheet();
                setHeaders($sheet,$SY,$level,$sectionName);
                setDatas($con,$sheet,$getLearners,$start1,$start2,$countMale,$countFemale,$total,$id,$SY);
                setTotals($sheet,33, 60,61, $countMale, $countFemale,$total);

                setLevelofProficiency($con,24,$id,$sheet);
                $sheet ->setCellValue('M36', $adviser);
                
            }
            else if($countFemale <=30) // 30 Female
            {
                $phpExcel = PHPExcel_IOFactory::load('School Forms Final/School Form 5/School Form 5 20M/School Form 5 20-30.xlsx');
                $sheet = $phpExcel ->getActiveSheet();
                setHeaders($sheet,$SY,$level,$sectionName);
                setDatas($con,$sheet,$getLearners,$start1,$start2,$countMale,$countFemale,$total,$id,$SY);
                setTotals($sheet,33, 64,65, $countMale, $countFemale,$total);

                setLevelofProficiency($con,24,$id,$sheet);
                $sheet ->setCellValue('M36', $adviser);
            }
            else if($countFemale <=35) // 35 Female
            {
                $phpExcel = PHPExcel_IOFactory::load('School Forms Final/School Form 5/School Form 5 20M/School Form 5 20-35.xlsx');
                $sheet = $phpExcel ->getActiveSheet();
                setHeaders($sheet,$SY,$level,$sectionName);
                setDatas($con,$sheet,$getLearners,$start1,$start2,$countMale,$countFemale,$total,$id,$SY);
                setTotals($sheet,33, 68,69, $countMale, $countFemale,$total);

                setLevelofProficiency($con,24,$id,$sheet);
                $sheet ->setCellValue('M36', $adviser);
            }
            else if($countFemale <=40) // 40 Female
            {
                $phpExcel = PHPExcel_IOFactory::load('School Forms Final/School Form 5/School Form 5 20M/School Form 5 20-40.xlsx');
                $sheet = $phpExcel ->getActiveSheet();
                setHeaders($sheet,$SY,$level,$sectionName);
                setDatas($con,$sheet,$getLearners,$start1,$start2,$countMale,$countFemale,$total,$id,$SY);
                setTotals($sheet,33, 74,75, $countMale, $countFemale,$total);

                setLevelofProficiency($con,24,$id,$sheet);
                $sheet ->setCellValue('M36', $adviser);
            }
            else if($countFemale <=43) // 45 Female
            {
                $phpExcel = PHPExcel_IOFactory::load('School Forms Final/School Form 5/School Form 5 20M/School Form 5 20-43.xlsx');
                $sheet = $phpExcel ->getActiveSheet();
                setHeaders($sheet,$SY,$level,$sectionName);
                setDatas($con,$sheet,$getLearners,$start1,$start2,$countMale,$countFemale,$total,$id,$SY);
                setTotals($sheet,33, 77,78, $countMale, $countFemale,$total);

                setLevelofProficiency($con,24,$id,$sheet);
                $sheet ->setCellValue('M36', $adviser);
            }
            else if($countFemale <=50) // 45 Female
            {
                $phpExcel = PHPExcel_IOFactory::load('School Forms Final/School Form 5/School Form 5 20M/School Form 5 20-50.xlsx');
                $sheet = $phpExcel ->getActiveSheet();
                setHeaders($sheet,$SY,$level,$sectionName);
                setDatas($con,$sheet,$getLearners,$start1,$start2,$countMale,$countFemale,$total,$id,$SY);
                setTotals($sheet,33, 82,83, $countMale, $countFemale,$total);

                setLevelofProficiency($con,24,$id,$sheet);
                $sheet ->setCellValue('M36', $adviser);
            }
        }

        // SF 5 26 Male
        else if($countMale <= 26)
        {
            $start1 = 13; $start2 = 40;
            if($countFemale <=26) // 26 Female
            {
                $phpExcel = PHPExcel_IOFactory::load('School Forms Final/School Form 5/School Form 5 26M/School Form 5 26-26.xlsx');
                $sheet = $phpExcel ->getActiveSheet();
                setHeaders($sheet,$SY,$level,$sectionName);
                setDatas($con,$sheet,$getLearners,$start1,$start2,$countMale,$countFemale,$total,$id,$SY);
                setTotals($sheet,39, 66,67, $countMale, $countFemale,$total);

                setLevelofProficiency($con,24,$id,$sheet);
                $sheet ->setCellValue('M42', $adviser);
            }
            else if($countFemale <=30) // 30 Female
            {
                $phpExcel = PHPExcel_IOFactory::load('School Forms Final/School Form 5/School Form 5 26M/School Form 5 26-30.xlsx');
                $sheet = $phpExcel ->getActiveSheet();
                setHeaders($sheet,$SY,$level,$sectionName);
                setDatas($con,$sheet,$getLearners,$start1,$start2,$countMale,$countFemale,$total,$id,$SY);
                setTotals($sheet,39, 70,71, $countMale, $countFemale,$total);

                setLevelofProficiency($con,24,$id,$sheet);
                $sheet ->setCellValue('M42', $adviser);
            }
            else if($countFemale <=35) // 35 Female
            {
                $phpExcel = PHPExcel_IOFactory::load('School Forms Final/School Form 5/School Form 5 26M/School Form 5 26-35.xlsx');
                $sheet = $phpExcel ->getActiveSheet();
                setHeaders($sheet,$SY,$level,$sectionName);
                setDatas($con,$sheet,$getLearners,$start1,$start2,$countMale,$countFemale,$total,$id,$SY);
                setTotals($sheet,39, 74,75, $countMale, $countFemale,$total);

                setLevelofProficiency($con,24,$id,$sheet);
                $sheet ->setCellValue('M42', $adviser);
            }
            else if($countFemale <=40) // 40 Female
            {
                $phpExcel = PHPExcel_IOFactory::load('School Forms Final/School Form 5/School Form 5 26M/School Form 5 26-40.xlsx');
                $sheet = $phpExcel ->getActiveSheet();
                setHeaders($sheet,$SY,$level,$sectionName);
                setDatas($con,$sheet,$getLearners,$start1,$start2,$countMale,$countFemale,$total,$id,$SY);
                setTotals($sheet,39, 79,80, $countMale, $countFemale,$total);

                setLevelofProficiency($con,24,$id,$sheet);
                $sheet ->setCellValue('M42', $adviser);
            }
            else if($countFemale <=45) // 45 Female
            {
                $phpExcel = PHPExcel_IOFactory::load('School Forms Final/School Form 5/School Form 5 26M/School Form 5 26-45.xlsx');
                $sheet = $phpExcel ->getActiveSheet();
                setHeaders($sheet,$SY,$level,$sectionName);
                setDatas($con,$sheet,$getLearners,$start1,$start2,$countMale,$countFemale,$total,$id,$SY);
                setTotals($sheet,39, 84,85, $countMale, $countFemale,$total);

                setLevelofProficiency($con,24,$id,$sheet);
                $sheet ->setCellValue('M42', $adviser);
            }
        }
        // SF 5 30 Male
        else if($countMale <= 30)
        {
            $start1 = 13; $start2 = 44;
            if($countFemale <=26) // 26 Female
            {
                $phpExcel = PHPExcel_IOFactory::load('School Forms Final/School Form 5/School Form 5 30M/School Form 5 30-26.xlsx');
                $sheet = $phpExcel ->getActiveSheet();
                setHeaders($sheet,$SY,$level,$sectionName);
                setDatas($con,$sheet,$getLearners,$start1,$start2,$countMale,$countFemale,$total,$id,$SY);
                setTotals($sheet,43, 70,71, $countMale, $countFemale,$total);

                setLevelofProficiency($con,24,$id,$sheet);
                $sheet ->setCellValue('M46', $adviser);
            }
            else if($countFemale <=30) // 30 Female
            {
                $phpExcel = PHPExcel_IOFactory::load('School Forms Final/School Form 5/School Form 5 30M/School Form 5 30-30.xlsx');
                $sheet = $phpExcel ->getActiveSheet();
                setHeaders($sheet,$SY,$level,$sectionName);
                setDatas($con,$sheet,$getLearners,$start1,$start2,$countMale,$countFemale,$total,$id,$SY);
                setTotals($sheet,43, 74,75, $countMale, $countFemale,$total);

                setLevelofProficiency($con,24,$id,$sheet);
                $sheet ->setCellValue('M46', $adviser);
            }
            else if($countFemale <=35) // 35 Female
            {
                $phpExcel = PHPExcel_IOFactory::load('School Forms Final/School Form 5/School Form 5 30M/School Form 5 30-35.xlsx');
                $sheet = $phpExcel ->getActiveSheet();
                setHeaders($sheet,$SY,$level,$sectionName);
                setDatas($con,$sheet,$getLearners,$start1,$start2,$countMale,$countFemale,$total,$id,$SY);
                setTotals($sheet,43, 78,79, $countMale, $countFemale,$total);

                setLevelofProficiency($con,24,$id,$sheet);
                $sheet ->setCellValue('M46', $adviser);
            }
            else if($countFemale <=40) // 40 Female
            {
                $phpExcel = PHPExcel_IOFactory::load('School Forms Final/School Form 5/School Form 5 30M/School Form 5 30-40.xlsx');
                $sheet = $phpExcel ->getActiveSheet();
                setHeaders($sheet,$SY,$level,$sectionName);
                setDatas($con,$sheet,$getLearners,$start1,$start2,$countMale,$countFemale,$total,$id,$SY);
                setTotals($sheet,43, 83,84, $countMale, $countFemale,$total);

                setLevelofProficiency($con,24,$id,$sheet);
                $sheet ->setCellValue('M46', $adviser);
            }
            else if($countFemale <=45) // 45 Female
            {
                $phpExcel = PHPExcel_IOFactory::load('School Forms Final/School Form 5/School Form 5 30M/School Form 5 30-45.xlsx');
                $sheet = $phpExcel ->getActiveSheet();
                setHeaders($sheet,$SY,$level,$sectionName);
                setDatas($con,$sheet,$getLearners,$start1,$start2,$countMale,$countFemale,$total,$id,$SY);
                setTotals($sheet,43, 88,89, $countMale, $countFemale,$total);

                setLevelofProficiency($con,24,$id,$sheet);
                $sheet ->setCellValue('M46', $adviser);
            }
        }
        //SF 5 35 Male
        else if($countMale <= 35)
        {
            $start1 = 13; $start2 = 49;
            if($countFemale <=26) // 26 Female
            {
                $phpExcel = PHPExcel_IOFactory::load('School Forms Final/School Form 5/School Form 5 35M/School Form 5 35-26.xlsx');
                $sheet = $phpExcel ->getActiveSheet();
                setHeaders($sheet,$SY,$level,$sectionName);
                setDatas($con,$sheet,$getLearners,$start1,$start2,$countMale,$countFemale,$total,$id,$SY);
                setTotals($sheet,48, 75,76, $countMale, $countFemale,$total);
                setLevelofProficiency($con,24,$id,$sheet);
                $sheet ->setCellValue('M51', $adviser);
            }
            else if($countFemale <=30) // 30 Female
            {
                $phpExcel = PHPExcel_IOFactory::load('School Forms Final/School Form 5/School Form 5 35M/School Form 5 35-30.xlsx');
                $sheet = $phpExcel ->getActiveSheet();
                setHeaders($sheet,$SY,$level,$sectionName);
                setDatas($con,$sheet,$getLearners,$start1,$start2,$countMale,$countFemale,$total,$id,$SY);
                setTotals($sheet,48, 79,80, $countMale, $countFemale,$total);
                setLevelofProficiency($con,24,$id,$sheet);
                $sheet ->setCellValue('M51', $adviser);
            }
            else if($countFemale <=35) // 35 Female
            {
                $phpExcel = PHPExcel_IOFactory::load('School Forms Final/School Form 5/School Form 5 35M/School Form 5 35-35.xlsx');
                $sheet = $phpExcel ->getActiveSheet();
                setHeaders($sheet,$SY,$level,$sectionName);
                setDatas($con,$sheet,$getLearners,$start1,$start2,$countMale,$countFemale,$total,$id,$SY);
                setTotals($sheet,48, 83,84, $countMale, $countFemale,$total);
                setLevelofProficiency($con,24,$id,$sheet);
                $sheet ->setCellValue('M51', $adviser);
            }
            else if($countFemale <=40) // 40 Female
            {
                $phpExcel = PHPExcel_IOFactory::load('School Forms Final/School Form 5/School Form 5 35M/School Form 5 35-40.xlsx');
                $sheet = $phpExcel ->getActiveSheet();
                setHeaders($sheet,$SY,$level,$sectionName);
                setDatas($con,$sheet,$getLearners,$start1,$start2,$countMale,$countFemale,$total,$id,$SY);
                setTotals($sheet,48, 88,89, $countMale, $countFemale,$total);
                setLevelofProficiency($con,24,$id,$sheet);
                $sheet ->setCellValue('M51', $adviser);
            }
            else if($countFemale <=45) // 45 Female
            {
                $phpExcel = PHPExcel_IOFactory::load('School Forms Final/School Form 5/School Form 5 35M/School Form 5 35-45.xlsx');
                $sheet = $phpExcel ->getActiveSheet();
                setHeaders($sheet,$SY,$level,$sectionName);
                setDatas($con,$sheet,$getLearners,$start1,$start2,$countMale,$countFemale,$total,$id,$SY);
                setTotals($sheet,48, 93,94, $countMale, $countFemale,$total);
                setLevelofProficiency($con,24,$id,$sheet);
                $sheet ->setCellValue('M51', $adviser);
            }
        }
        // SF 5 40 Male
        else if($countMale <= 40)
        {
            $start1 = 13; $start2 = 54;
            if($countFemale <=26) // 26 Female
            {
                $phpExcel = PHPExcel_IOFactory::load('School Forms Final/School Form 5/School Form 5 40M/School Form 5 40-26.xlsx');
                $sheet = $phpExcel ->getActiveSheet();
                setHeaders($sheet,$SY,$level,$sectionName);
                setDatas($con,$sheet,$getLearners,$start1,$start2,$countMale,$countFemale,$total,$id,$SY);
                setTotals($sheet,53, 80,81, $countMale, $countFemale,$total);
                setLevelofProficiency($con,24,$id,$sheet);
                $sheet ->setCellValue('M56', $adviser);
            }
            else if($countFemale <=30) // 30 Female
            {
                $phpExcel = PHPExcel_IOFactory::load('School Forms Final/School Form 5/School Form 5 40M/School Form 5 40-30.xlsx');
                $sheet = $phpExcel ->getActiveSheet();
                setHeaders($sheet,$SY,$level,$sectionName);
                setDatas($con,$sheet,$getLearners,$start1,$start2,$countMale,$countFemale,$total,$id,$SY);
                setTotals($sheet,53, 84,85, $countMale, $countFemale,$total);
                setLevelofProficiency($con,24,$id,$sheet);
                $sheet ->setCellValue('M56', $adviser);
            }
            else if($countFemale <=35) // 35 Female
            {
                $phpExcel = PHPExcel_IOFactory::load('School Forms Final/School Form 5/School Form 5 40M/School Form 5 40-35.xlsx');
                $sheet = $phpExcel ->getActiveSheet();
                setHeaders($sheet,$SY,$level,$sectionName);
                setDatas($con,$sheet,$getLearners,$start1,$start2,$countMale,$countFemale,$total,$id,$SY);
                setTotals($sheet,53, 88,89, $countMale, $countFemale,$total);
                setLevelofProficiency($con,24,$id,$sheet);
                $sheet ->setCellValue('M56', $adviser);
            }
            else if($countFemale <=40) // 40 Female
            {
                $phpExcel = PHPExcel_IOFactory::load('School Forms Final/School Form 5/School Form 5 40M/School Form 5 40-40.xlsx');
                $sheet = $phpExcel ->getActiveSheet();
                setHeaders($sheet,$SY,$level,$sectionName);
                setDatas($con,$sheet,$getLearners,$start1,$start2,$countMale,$countFemale,$total,$id,$SY);
                setTotals($sheet,53, 93,94, $countMale, $countFemale,$total);
                setLevelofProficiency($con,24,$id,$sheet);
                $sheet ->setCellValue('M56', $adviser);
            }
            else if($countFemale <=45) // 45 Female
            {
                $phpExcel = PHPExcel_IOFactory::load('School Forms Final/School Form 5/School Form 5 40M/School Form 5 40-45.xlsx');
                $sheet = $phpExcel ->getActiveSheet();
                setHeaders($sheet,$SY,$level,$sectionName);
                setDatas($con,$sheet,$getLearners,$start1,$start2,$countMale,$countFemale,$total,$id,$SY);
                setTotals($sheet,53, 98,99, $countMale, $countFemale,$total);
                setLevelofProficiency($con,24,$id,$sheet);
                $sheet ->setCellValue('M56', $adviser);
            }
        }
        
        
        

        $writer = PHPExcel_IOFactory::createWriter($phpExcel, "Excel2007");
        //
        // $writer->save('C:/Users/MLD/Documents/School Form 1-'.$sectionName.'-'.$SY.'.xlsx'); 

        $fileName = "";
        switch ($level) {
            case 'Grade 7':
                $fileName = "SF5_".$SY."_".$level." (Year I) - ".$sectionName;
                break;
            case 'Grade 8':
                $fileName = "SF5_".$SY."_".$level." (Year II) - ".$sectionName;
                break;
            case 'Grade 9':
                $fileName = "SF5_".$SY."_".$level." (Year III) - ".$sectionName;
                break;
            case 'Grade 10':
                $fileName = "SF5_".$SY."_".$level." (Year IV) - ".$sectionName;
                break;
            default:
                # code...
                break;
        }
        
        
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="'.$fileName.'.xlsx"');
            header('Cache-Control: max-age=0');
            $writer->save('php://output'); 
        
        
    
?>